package core.service.master;

import java.util.Map;

import share.AuthRsPicDto;
import share.AuthUserDto;
import share.AuthUserGadgetDto;

public interface MstAuthRsPicSvc {

	public Map<String, Object> loadList(Map<String, Object> input, String search);
	public Map<String, Object> getRsByUser(Map<String, Object> input, String userLogin, String search);
	public Map<String, Object> getUserGadgetByUserLogin(Map<String, Object> input, String userLogin);
	public Map<String, Object> getRsNotIn(Map<String, Object> input);
	
	public int save(AuthRsPicDto mstAuthRsPicDto);
	public int update(AuthUserDto mstAuthUserDto);
	public int save(AuthUserGadgetDto mstAuthUserGadgetDto);
	public int delete(AuthRsPicDto mstAuthRsPicDto);
	
}
