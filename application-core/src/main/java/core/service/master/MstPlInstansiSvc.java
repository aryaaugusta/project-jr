package core.service.master;

import java.math.BigDecimal;
import java.util.Map;

import share.PlInstansiDto;

public interface MstPlInstansiSvc {
	public Map<String, Object> loadList(Map<String, Object> input);

	public abstract Map<String, Object> loadList(Map<String, Object> input, String search, String kodeInstansi);
	
	public int save(PlInstansiDto mstPlInstansiDto);
	
	public abstract int update(PlInstansiDto mstPlInstansiDto);
	
	public abstract Map<String, Object> findKorlantasProvince();
		
	public abstract Map<String, Object> findKorlantasDirtrict(BigDecimal provinceId);
	
	public abstract int delete(PlInstansiDto mstPlInstansiDto);

	Map<String, Object> getOneKorlantasProvince(Map<String, Object> input,
			String kodeInstansi);

	Map<String, Object> findKorlantasDirtrictByDIstricNumber(
			String districtNumber);

}
