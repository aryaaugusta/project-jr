package core.service.master.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.AuthRsPicDto;
import share.AuthUserDto;
import share.AuthUserGadgetDto;
import share.PlRumahSakitDto;
import common.model.UserSessionJR;
import common.spring.Paging;
import common.util.CommonConstants;
import common.util.JsonUtil;
import common.util.StringUtil;
import core.dao.AuthRsPicDao;
import core.dao.AuthUserGadgetDao;
import core.model.AuthRsPic;
import core.model.AuthUser;
import core.model.AuthUserGadget;
import core.service.master.MstAuthRsPicSvc;

@Service
@Transactional
public class MstAuthRsPicSvcImpl implements MstAuthRsPicSvc{

	Paging page = new Paging();
	
	@Autowired
	AuthUserGadgetDao mstAuthUserGadgetDao;
	
	@Autowired
	AuthRsPicDao mstAuthRsPicDao;
	
	@Autowired
	MapperFacade mapperFacade;
	
	@Override
	public Map<String, Object> loadList(Map<String, Object> input,
			String search) {
		
		List<AuthUser> list = mstAuthRsPicDao.getListUser(search);
		List<AuthUserDto> listDto = new ArrayList<>();
		
		for(AuthUser a : list){			
			AuthUserDto dto = new AuthUserDto();
			dto = mapperFacade.map(a, AuthUserDto.class);
			List<Object[]> listRs = mstAuthRsPicDao.getRsByUserLoginJpa(a.getLogin());

			for(Object[] s : listRs){
				System.out.println("test JSON RS "+ JsonUtil.getJson(listRs));
			}
			
			List<PlRumahSakitDto> rsDtos = new ArrayList<>();
			if(listRs != null && listRs.size() >0)
			{
				for(Object[] b : listRs){
					if(b[1] != null && !b[1].toString().isEmpty()){
						AuthRsPic pic = (AuthRsPic) b[0];
						PlRumahSakitDto rsDto = new PlRumahSakitDto();
						rsDto.setKodeNamaRs(b[2] +" - " +b[1]);
						System.out.println();
						rsDtos.add(rsDto);
					}
				}
			}
			dto.setRsDtos(rsDtos);
			listDto.add(dto);
		}
		
		
//		int i = 1;
//		for(Object[] a : list){
//			MstAuthUserDto dto = new MstAuthUserDto();
////			AuthUser user = (AuthUser) a[0];
//			dto.setNo(i++);
//			dto.setLogin((String)a[0]);
//			dto.setUserName((String)a[2]);
//			dto.setDescription((String)a[3]);
//			dto.setAttribute4((String)a[4]);
//			dto.setAttribute2((String)a[5]);
//			dto.setUserId((String)a[6]);
//			List<Object[]> listRs = mstAuthRsPicDao.getRsByUserLogin(dto.getLogin());
//			List<MstPlRumahSakitDto> rsDtos = new ArrayList<>();
//			for(Object[] b : listRs){
//				MstPlRumahSakitDto dtoRs = new MstPlRumahSakitDto();
////				PlRumahSakit obj = (PlRumahSakit) b[1];
//				dtoRs.setKodeNamaRs((String)b[0] +" - "+(String)b[1]);
//				rsDtos.add(dtoRs);
////				dto.setRsDtos(rsDtos);
//			}
////			dto.setNamaRs((String)a[1]);
////			dto.setUserName(user.getUserName().toString());
////			dto.setDescription(user.getDescription().toString());
////			dto.setAttribute4(user.getAttribute4().toString()); //email
//			listDto.add(dto);
//		}
		
		System.err.println("test "+listDto.size());
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		if (listDto.size() > 0) {
			map.put("contentData", listDto);
			return map;
		}
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> getRsByUser(Map<String, Object> input,
			String userLogin, String search) {
		
		List<Object[]> listRs = mstAuthRsPicDao.getRsByUserLogin(userLogin,StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<PlRumahSakitDto> listDto = new ArrayList<>();
		
//		int i = 1;
		for(Object[] a : listRs){
			PlRumahSakitDto dto = new PlRumahSakitDto();
//			dto.setNo(i++);
			dto.setKodeNamaRs((String)a[1]+" - "+(String)a[0]);
			dto.setKodeRumahsakit((String)a[1]);
			dto.setDeskripsi((String)a[0]);			
			listDto.add(dto);			
		}
		System.err.println("test "+listDto.size());
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public int save(AuthRsPicDto mstAuthRsPicDto) {
		try{
			AuthRsPic rsPic = new AuthRsPic();
			rsPic.setUserLogin(mstAuthRsPicDto.getUserLogin());
			rsPic.setKodeRumahsakit(mstAuthRsPicDto.getKodeRumahsakit());
			mstAuthRsPicDao.saveRsPic(rsPic);
			return CommonConstants.OK_REST_STATUS;
		}catch(Exception e){
			return CommonConstants.ERROR_REST_STATUS;						
		}
	}

	@Override
	public int delete(AuthRsPicDto mstAuthRsPicDto) {
		try{
			AuthRsPic rsPic = new AuthRsPic();
			rsPic.setUserLogin(mstAuthRsPicDto.getUserLogin());
			rsPic.setKodeRumahsakit(mstAuthRsPicDto.getKodeRumahsakit());
			mstAuthRsPicDao.deleteRsPic(rsPic);
			return CommonConstants.OK_REST_STATUS;
		}catch(Exception e){
			return CommonConstants.ERROR_REST_STATUS;						
		}
	}

	@Override
	public int save(AuthUserGadgetDto mstAuthUserGadgetDto) {
		try{
			AuthUserGadget userGadget = new AuthUserGadget();
			userGadget.setGNumber(mstAuthUserGadgetDto.getgNumber());
			userGadget.setGSeries(mstAuthUserGadgetDto.getgSeries());
			userGadget.setGStatus(mstAuthUserGadgetDto.getgStatus());
			userGadget.setKeterangan(mstAuthUserGadgetDto.getKeterangan());
			userGadget.setUpdatedBy(mstAuthUserGadgetDto.getUpdatedBy());
			userGadget.setUpdatedDate(mstAuthUserGadgetDto.getUpdatedDate());
			userGadget.setUserLogin(mstAuthUserGadgetDto.getUserLogin());
			userGadget.setUsrId(mstAuthUserGadgetDto.getUsrId());
			mstAuthUserGadgetDao.save(userGadget);
			return CommonConstants.OK_REST_STATUS;
		}catch(Exception e){
			return CommonConstants.ERROR_REST_STATUS;			
		}
	}

	@Override
	public Map<String, Object> getRsNotIn(Map<String, Object> input) {
		Map<String, Object> kkk = new HashMap<>();
		UserSessionJR sess = new UserSessionJR();
		try {
			sess = JsonUtil.mapJsonToSingleObject(input.get("userSession"), UserSessionJR.class);
//			kkk = JsonUtil.mapJsonToHashMapObject(input.get("userSession"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		sess = (UserSessionJR) kkk.get("userSession");
		System.out.println(JsonUtil.getJson(sess));
		String s = "ADMIN PL";
		System.out.println(sess.getLoginDesc());
		String search = "";
		if(s.toUpperCase().contains(sess.getLoginDesc().substring(0, 8).toUpperCase())){
			search = "%";
		}else{
			search = sess.getKantor().substring(0, 2)+"%";
		}
		System.out.println(search);
		List<Object[]> listRs = mstAuthRsPicDao.getListRsByUserLogin(search);
		List<PlRumahSakitDto> listDto = new ArrayList<>();
		
		for(Object[] a : listRs){
			PlRumahSakitDto dto = new PlRumahSakitDto();
			dto.setKodeNamaRs((String)a[0]+" - "+(String)a[1]);
			dto.setKodeRumahsakit((String)a[0]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public int update(AuthUserDto mstAuthUserDto) {
		try{
			AuthUser user = new AuthUser();
			user.setAttribute4(mstAuthUserDto.getAttribute4());
			user.setAttribute2(mstAuthUserDto.getAttribute2());
			user.setLogin(mstAuthUserDto.getLogin());
			mstAuthRsPicDao.updateAuthUser(user);
			return CommonConstants.OK_REST_STATUS;
		}catch(Exception e){
			return CommonConstants.ERROR_REST_STATUS;						
		}
	}

	@Override
	public Map<String, Object> getUserGadgetByUserLogin(
			Map<String, Object> input, String userLogin) {
		
		List<AuthUserGadget> userGadget = mstAuthUserGadgetDao.getListUserGadgetByLogin(userLogin);
		List<AuthUserGadgetDto> listDto = new ArrayList<>();
		
		for(AuthUserGadget a : userGadget){
			AuthUserGadgetDto dto = new AuthUserGadgetDto();
			dto.setgNumber(a.getGNumber());
			dto.setgSeries(a.getGSeries());
			dto.setgStatus(a.getGStatus());
			dto.setKeterangan(a.getKeterangan());
			dto.setUpdatedBy(a.getUpdatedBy());
			dto.setUpdatedDate(a.getUpdatedDate());
			dto.setUserLogin(a.getUserLogin());
			dto.setUsrId(a.getUsrId());
			listDto.add(dto);
		}		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

}
