package core.service.master.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.KorlantasDistrictDto;
import share.KorlantasProvinceDto;
import share.PlInstansiDto;
import share.PlMappingPoldaDto;
import common.spring.Paging;
import common.util.CommonConstants;
import common.util.JsonUtil;
import common.util.StringUtil;
import core.dao.KorlantasDistrictDao;
import core.dao.KorlantasProvinceDao;
import core.dao.PlInstansiDao;
import core.dao.PlMappingPoldaDao;
import core.model.KorlantasDistrict;
import core.model.KorlantasProvince;
import core.model.PlInstansi;
import core.model.PlInstansiPK;
import core.model.PlMappingPolda;
import core.model.PlPengajuanSantunan;
import core.service.master.MstPlInstansiSvc;


@Service
@Transactional
public class MstPlInstansiSvcImpl implements MstPlInstansiSvc{

Paging page = new Paging();
	
	@Autowired
	PlInstansiDao plInstansiDao;
	
	@Autowired
	KorlantasProvinceDao korlantasProvinceDao;
	
	@Autowired
	KorlantasDistrictDao korlantasDistrictDao;
	
	@Autowired
	PlMappingPoldaDao plMappingPoldaDao;
	
	@Autowired
	MapperFacade mapperFacade;
	
	@Override
	public Map<String, Object> loadList(Map<String, Object> input) {
		
//		maps.put("kode", getKode());
//		maps.put("searchIndex", getSearchIndex());
		
		String kodeInstansi = (String) input.get("kode");
		String search = (String) input.get("searchIndex");
		
		List<Object[]> ins1 = plInstansiDao.getList(kodeInstansi, StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
//		List<Object[]> ins = plInstansiDao.getDataList(StringUtil.surroundString(StringUtil.nevl(kodeInstansi, "%%"), ""), StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<PlInstansiDto> listDto = new ArrayList<>();
		
		int i = 1;
		for(Object[] a : ins1){
			PlInstansiDto dto = new PlInstansiDto();
			dto.setNumber(i++);
			dto.setKodeInstansi((String)a[0]);
			dto.setDeskripsi((String)a[1]);
//			dto.setCreatedBy((String)a[2]);
//			dto.setCreationDate((Date)a[3]);
			dto.setFlagEnable(a[2].toString());
//			dto.setNamaPolda((String)a[5]);
			dto.setNamaPolres((String)a[3]);			
			
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		
		return map;
	}
	
	@Override
	public Map<String, Object> loadList(Map<String, Object> input, String search, String kodeInstansi) {

		List<Object[]> ins1 = plInstansiDao.getList(kodeInstansi, StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
//		List<Object[]> ins = plInstansiDao.getDataList(StringUtil.surroundString(StringUtil.nevl(kodeInstansi, "%%"), ""), StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<PlInstansiDto> listDto = new ArrayList<>();
		
		int i = 1;
		for(Object[] a : ins1){
			PlInstansiDto dto = new PlInstansiDto();
			dto.setNumber(i++);
			dto.setKodeInstansi((String)a[0]);
			dto.setDeskripsi((String)a[1]);
//			dto.setCreatedBy((String)a[2]);
//			dto.setCreationDate((Date)a[3]);
			dto.setFlagEnable((String)a[2]);
//			dto.setNamaPolda((String)a[5]);
			dto.setNamaPolres((String)a[3]);			

			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		
		return map;
	}

	@Override
	public int save(PlInstansiDto plInstansiDto) {
					
			PlInstansiPK plInstansiPK = new PlInstansiPK();
			plInstansiPK.setKodeInstansi(plInstansiDto.getKodeInstansi());
			
			PlInstansi instansi = plInstansiDao.getInstansiById(plInstansiDto.getKodeInstansi());
			if(instansi==null){

			try {
				PlInstansi plInstansi = new PlInstansi();
				PlMappingPolda plPolda = new PlMappingPolda();
				
				plInstansi.setKodeInstansi(plInstansiDto.getKodeInstansi());
				plInstansi.setDeskripsi(plInstansiDto.getDeskripsi());
				plInstansi.setCreatedBy(plInstansiDto.getCreatedBy());
				plInstansi.setCreationDate(plInstansiDto.getCreationDate());
				plInstansi.setLastUpdatedBy(plInstansiDto.getLastUpdatedBy());
				plInstansi.setLastUpdatedDate(plInstansiDto.getLastUpdatedDate());
				plInstansi.setFlagEnable(plInstansiDto.getFlagEnable());
				plInstansiDao.save(plInstansi);
				
				plPolda.setKodeInstansi(plInstansiDto.getKodeInstansi());
				plPolda.setDeskripsi(plInstansiDto.getDeskripsi());
				plPolda.setNamaPolda(plInstansiDto.getNamaPolda());
				plPolda.setKodePolda(plInstansiDto.getKodePolda());
				plPolda.setIdDistricts(plInstansiDto.getIdDistrict());
				plPolda.setWilayahKpjr(plInstansiDto.getWilayahKPJR());
				plMappingPoldaDao.save(plPolda);
				return CommonConstants.OK_REST_STATUS;
			} catch (Exception e) {
				e.printStackTrace();
				return CommonConstants.ERROR_REST_STATUS;
			}
		} else {
			return 2;
		}
	}
	@Override
	public int delete(PlInstansiDto plInstansiDto){
		PlInstansi plInstansi = new PlInstansi();
		try {
			
			List<PlPengajuanSantunan> lis = plInstansiDao.getPengajuanFromInstansi(plInstansiDto.getKodeInstansi());
			if(lis.size()>0){
				return 3;
			}else{
				PlInstansiPK pk = new PlInstansiPK();
				pk.setKodeInstansi(plInstansi.getKodeInstansi());
				List<PlInstansi> listIns = plInstansiDao.getInstansiByCode(plInstansiDto.getKodeInstansi()); 
				PlInstansi a = null;
				if(listIns.size()>0){
					a = listIns.get(0);
				}
				
				if(a==null){
					return CommonConstants.ERROR_REST_STATUS;
				}else{
					plInstansiDao.delete(a);
				}
			}
			
//			plInstansi.setKodeInstansi(plInstansiDto.getKodeInstansi());
//			plInstansi.setDeskripsi(plInstansiDto.getDeskripsi());
//			plInstansi.setCreatedBy(plInstansiDto.getCreatedBy());
//			plInstansi.setCreationDate(plInstansiDto.getCreationDate());
//			plInstansi.setLastUpdatedBy(plInstansiDto.getLastUpdatedBy());
//			plInstansi.setLastUpdatedDate(plInstansiDto.getLastUpdatedDate());
//			plInstansi.setFlagEnable(plInstansiDto.getFlagEnable());
//			plInstansiDao.save(plInstansi);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> findKorlantasProvince() {
		List<KorlantasProvince> listData = korlantasProvinceDao.getKorlantasProvinces();
		List<KorlantasProvinceDto> listDto = new ArrayList<>();
		
		listDto = mapperFacade.mapAsList(listData, KorlantasProvinceDto.class);
		System.out.println("List Model : "+JsonUtil.getJson(listData));
		System.out.println("List Dto"+JsonUtil.getJson(listDto));
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	
	
	@Override
	public Map<String, Object> findKorlantasDirtrictByDIstricNumber(
			String districtNumber) {
		
		List<KorlantasDistrict> listData = 
				korlantasDistrictDao.findKorlantasDistrictByDistricNumber(new BigDecimal(districtNumber));
		List<KorlantasDistrictDto> listDto = new ArrayList<>();
		
		listDto = mapperFacade.mapAsList(listData, KorlantasDistrictDto.class);
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}
	@Override
	public Map<String, Object> findKorlantasDirtrict(
			BigDecimal provinceId) {
		
		List<KorlantasDistrict> listData = 
				korlantasDistrictDao.findKorlantasDistrictByProvinceId(provinceId);
		List<KorlantasDistrictDto> listDto = new ArrayList<>();
		
		listDto = mapperFacade.mapAsList(listData, KorlantasDistrictDto.class);
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> getOneKorlantasProvince(
			Map<String, Object> input, String kodeInstansi) {

		PlMappingPolda kor = plMappingPoldaDao.getProvinceByKodeInstansi(kodeInstansi);
		PlMappingPoldaDto dto = new PlMappingPoldaDto();
		
		dto = mapperFacade.map(kor, PlMappingPoldaDto.class);
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", dto);
		return map;
		
	}

	@Override
	public int update(PlInstansiDto plInstansiDto) {
		PlInstansi plInstansi = new PlInstansi();
		PlMappingPolda plPolda = new PlMappingPolda();
		try {
			plInstansi.setKodeInstansi(plInstansiDto.getKodeInstansi());
			plInstansi.setDeskripsi(plInstansiDto.getDeskripsi());
			plInstansi.setCreatedBy(plInstansiDto.getCreatedBy());
			plInstansi.setCreationDate(plInstansiDto.getCreationDate());
			plInstansi.setLastUpdatedBy(plInstansiDto.getLastUpdatedBy());
			plInstansi.setLastUpdatedDate(plInstansiDto.getLastUpdatedDate());
			plInstansi.setFlagEnable(plInstansiDto.getFlagEnable());
			plInstansiDao.save(plInstansi);
			
			plPolda.setKodeInstansi(plInstansiDto.getKodeInstansi());
			plPolda.setDeskripsi(plInstansiDto.getDeskripsi());
			plPolda.setNamaPolda(plInstansiDto.getNamaPolda());
			plPolda.setKodePolda("P"+plInstansiDto.getKodePolda());
			plPolda.setIdDistricts(plInstansiDto.getIdDistrict());
			plPolda.setWilayahKpjr(plInstansiDto.getWilayahKPJR());
			plMappingPoldaDao.save(plPolda);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}


}
