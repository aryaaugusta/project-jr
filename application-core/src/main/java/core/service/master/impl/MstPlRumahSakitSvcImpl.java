package core.service.master.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.annotation.JsonFormat.Value;

import share.FndBankDto;
import share.PlPksRDto;
import share.PlRekeningRDto;
import share.PlRsBpjDto;
import share.PlRsMapBpjDto;
import share.PlRumahSakitDto;
import common.spring.Paging;
import common.util.CommonConstants;
import common.util.JsonUtil;
import common.util.StringUtil;
import core.dao.FndBankDao;
import core.dao.PlPksRDao;
import core.dao.PlRekeningRDao;
import core.dao.PlRsBpjDao;
import core.dao.PlRsMapBpjDao;
import core.dao.PlRumahSakitDao;
import core.model.FndBank;
import core.model.PlPenyelesaianSantunan;
import core.model.PlPksR;
import core.model.PlPksRPK;
import core.model.PlRekeningR;
import core.model.PlRekeningRPK;
import core.model.PlRsBpj;
import core.model.PlRsMapBpj;
import core.model.PlRumahSakit;
import core.service.master.MstPlRumahSakitSvc;

@Service
@Transactional
public class MstPlRumahSakitSvcImpl implements MstPlRumahSakitSvc {

	Paging page = new Paging();

	@Autowired
	PlRumahSakitDao mstPlRumahSakitDao;

	@Autowired
	PlRsMapBpjDao mstPlRsMapBpjDao;

	@Autowired
	PlRekeningRDao mstPlRekeningRDao;

	@Autowired
	PlRsBpjDao mstPlRsBpjDao;

	@Autowired
	FndBankDao mstFndBankDao;

	@Autowired
	PlPksRDao mstPlPksRDao;

	@Autowired
	MapperFacade mapperFacade;

	private PlRumahSakit convertToModel(PlRumahSakitDto obj) {
		return mapperFacade.map(obj, PlRumahSakit.class);
	}
	
	private PlRsMapBpj convertToModelBpjs(PlRsMapBpjDto obj) {
		return mapperFacade.map(obj, PlRsMapBpj.class);
	}

	@Override
	public Map<String, Object> loadList(Map<String, Object> input, String kanWil, String kodeNama,
			String search) {
//		List<PlRumahSakit> rs = mstPlRumahSakitDao.getDataList(kanWil, kodeNama,search);
		List<Object[]> rs = mstPlRumahSakitDao.getDataRS(kanWil, kodeNama,search);
		List<PlRumahSakitDto> listDto = new ArrayList<>();

		int i = 1;

		for(Object[] a : rs){
			PlRumahSakitDto dto = new PlRumahSakitDto();
			/*
			 * "select a.kode_rumahsakit 0, a.deskripsi 1, a.jenis_rs 2, a.creted_by 3, "
				+ "a.creation_date 4, a.last_updated_by 5, a.last_updated_date 6, a.flag_enable 7, "
				+ "a.pic 8, a.no_telp 9, a.alamat 10, b.LOKET_PENANGGUNGJAWAB 11, c.NAMA 12
			 */
			dto.setKodeRumahsakit((String) a[0]);
			dto.setDeskripsi((String) a[1]);
			dto.setJenisRs((String) a[2]);
			dto.setCreatedBy((String) a[3]);
//			dto.setCreationDate((Date) a[4]);
			dto.setLastUpdatedBy((String) a[5]);
//			dto.setLastUpdatedDate((Date) a[6]);
			dto.setFlagEnable(a[7].toString());
			dto.setPic((String) a[8]);
			dto.setNoTelp((String) a[9]);
			dto.setAlamat((String) a[10]);
			dto.setLoketKantor((String) a[12]);
			dto.setLoketKantorKode((String) a[11]);
			dto.setKodeNamaRs(dto.getKodeRumahsakit()+" - "+dto.getDeskripsi());
			listDto.add(dto);
		}
		
//		for (PlRumahSakit prs : rs) {
//			PlRumahSakitDto dto = new PlRumahSakitDto();
//			dto.setNo(i++);
//			dto.setAlamat(prs.getAlamat());
//			dto.setCreatedBy(prs.getCreatedBy());
//			dto.setCreationDate(prs.getCreationDate());
//			dto.setDeskripsi(prs.getDeskripsi());
//			dto.setFlagEnable(prs.getFlagEnable());
//			dto.setJenisRs(prs.getJenisRs());
//			dto.setKodeRumahsakit(prs.getKodeRumahsakit());
//			dto.setLastUpdatedBy(prs.getLastUpdatedBy());
//			dto.setLastUpdatedDate(prs.getLastUpdatedDate());
//			dto.setNoTelp(prs.getNoTelp());
//			dto.setPic(prs.getPic());
//			dto.setKodeNamaRs(prs.getKodeRumahsakit()+" - "+prs.getDeskripsi());
//			listDto.add(dto);
//		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public int save(PlRumahSakitDto mstPlRumahSakitDto) {
		try {
			PlRumahSakit plRumahSakit = convertToModel(mstPlRumahSakitDto);
			mstPlRumahSakitDao.save(plRumahSakit);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}
	
	@Override
	public int delete(PlRumahSakitDto mstPlRumahSakitDto) {
		try {
			PlRumahSakit plRumahSakit = convertToModel(mstPlRumahSakitDto);
			List<PlRekeningR> rek = mstPlRumahSakitDao.getRekeningByRsCode(mstPlRumahSakitDto.getKodeRumahsakit());
			
			if(rek.size()>0){
				return 2;
			}else{
				mstPlRumahSakitDao.delete(plRumahSakit);
			}
			
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int save(PlRsMapBpjDto mstPlRsMapBpjDto) {
		try {
			PlRsMapBpj plRsMapBpj = new PlRsMapBpj();
			plRsMapBpj.setKodeBpjs(mstPlRsMapBpjDto.getKodeBpjs());
			plRsMapBpj.setNamaRsBpjs(mstPlRsMapBpjDto.getNamaRsBpjs());
			plRsMapBpj.setKabKotaRsBpjs(mstPlRsMapBpjDto.getKabKotaRsBpjs());
			plRsMapBpj.setPropinsiRsBpjs(mstPlRsMapBpjDto.getPropinsiRsBpjs());
			plRsMapBpj.setAlamatRsBpjs(mstPlRsMapBpjDto.getAlamatRsBpjs());
			plRsMapBpj.setCreatedBy(mstPlRsMapBpjDto.getCreatedBy());
			plRsMapBpj.setCreationDate(mstPlRsMapBpjDto.getCreationDate());
			plRsMapBpj.setFlagKodeBpjsDobel(mstPlRsMapBpjDto.getFlagKodeBpjsDobel());
			plRsMapBpj.setKodeInstansi(mstPlRsMapBpjDto.getKodeInstansi());
			plRsMapBpj.setKodeRumahsakit(mstPlRsMapBpjDto.getKodeRumahsakit());
			plRsMapBpj.setLastUpdatedBy(mstPlRsMapBpjDto.getLastUpdatedBy());
			plRsMapBpj.setLastUpdatedDate(mstPlRsMapBpjDto.getLastUpdatedDate());
			plRsMapBpj.setLoketPenanggungjawab(mstPlRsMapBpjDto.getLoketPenanggungjawab());
			mstPlRsMapBpjDao.save(plRsMapBpj);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}

	}

	@Override
	public int save(PlRekeningRDto mstPlRekeningRDto) {
		
		if(mstPlRekeningRDto.getMode().equalsIgnoreCase("add")){
			List<PlRekeningR> model = mstPlRekeningRDao.getRekeningByNoRek(mstPlRekeningRDto.getNoRekening());
			if(model!=null){
				if(model.size()>0){
					if(!model.get(0).getKodeRumahsakit().equalsIgnoreCase(mstPlRekeningRDto.getKodeRumahsakit())){
						if(model.get(0).getKodeBank().equalsIgnoreCase(mstPlRekeningRDto.getKodeBank())){
							return 2;
						}
//						return 2;
					}
				}
			}
		}else{
			List<PlRekeningR> model = mstPlRekeningRDao.getRekeningByNoRek(mstPlRekeningRDto.getNoRekening());
			if(model!=null){
				if(model.size()>0){
					if(!model.get(0).getKodeRumahsakit().equalsIgnoreCase(mstPlRekeningRDto.getKodeRumahsakit())){
						if(model.get(0).getKodeBank().equalsIgnoreCase(mstPlRekeningRDto.getKodeBank())){
							return 2;
						}
//						return 2;
					}else{
						List<PlPenyelesaianSantunan> listPeny = mstPlRekeningRDao.getPenyFromRek(mstPlRekeningRDto.getNoRekening());
						if(listPeny!=null){
							if(listPeny.size()>0){
								return 3;
							}
						}
					}
				}
			}
		}
		
		List<PlRekeningR> listRek = mstPlRekeningRDao.getListRekeningByKodeRumahSakit(mstPlRekeningRDto.getKodeRumahsakit(), mstPlRekeningRDto.getKodeBank());
		String idRsRek = null;
		if(mstPlRekeningRDto.getMode().equalsIgnoreCase("add")){
			
			if(listRek.size()!=0){
				for(PlRekeningR a : listRek){
					idRsRek = a.getIdRsRek();
				}			
				if(idRsRek!=null){
					System.out.println("id rek not null : "+ idRsRek);
					String incVal = idRsRek.substring(17,19);
					int countVal = Integer.valueOf(incVal)+1;
					idRsRek = mstPlRekeningRDto.getKodeRumahsakit()+"."+mstPlRekeningRDto.getKodeBank()+"."+String.format("%02d", countVal);			
				}
			}else{
				idRsRek = mstPlRekeningRDto.getKodeRumahsakit()+"."+mstPlRekeningRDto.getKodeBank()+"."+"01";
				System.out.println("IDRSREK BARU : "+idRsRek);
			}
		}
		
		try {
			PlRekeningRPK plRekRPK = new PlRekeningRPK();
			plRekRPK.setIdRsRek(mstPlRekeningRDto.getIdRsRek());
			PlRekeningR findOneRek = mstPlRekeningRDao.getRekById(mstPlRekeningRDto.getIdRsRek());
			if(findOneRek == null){
				PlRekeningR plRekeningR = new PlRekeningR();
				plRekeningR.setIdRsRek(idRsRek);
				plRekeningR.setKodeRumahsakit(mstPlRekeningRDto.getKodeRumahsakit());
				plRekeningR.setKodeBank(mstPlRekeningRDto.getKodeBank());
				plRekeningR.setNoRekening(mstPlRekeningRDto.getNoRekening());
				plRekeningR.setCreatedBy(mstPlRekeningRDto.getCreatedBy());
				plRekeningR.setNamaPemilik(mstPlRekeningRDto.getNamaPemilik());
				plRekeningR.setFlagEnable(mstPlRekeningRDto.getFlagEnable());
				plRekeningR.setKodeKantorJr(mstPlRekeningRDto.getKodeKantorJr());
				plRekeningR.setCreationDate(mstPlRekeningRDto.getCreationDate());
				mstPlRekeningRDao.save(plRekeningR);
			}else{
				findOneRek.setIdRsRek(mstPlRekeningRDto.getIdRsRek());
				findOneRek.setKodeRumahsakit(mstPlRekeningRDto.getKodeRumahsakit());
				findOneRek.setKodeBank(mstPlRekeningRDto.getKodeBank());
				findOneRek.setNoRekening(mstPlRekeningRDto.getNoRekening());
				findOneRek.setNamaPemilik(mstPlRekeningRDto.getNamaPemilik());
				findOneRek.setFlagEnable(mstPlRekeningRDto.getFlagEnable());
				findOneRek.setKodeKantorJr(mstPlRekeningRDto.getKodeKantorJr());
				findOneRek.setCreationDate(mstPlRekeningRDto.getCreationDate());
				mstPlRekeningRDao.save(findOneRek);
			}
			
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}

	}

	@Override
	public Map<String, Object> findAllBank(Map<String, Object> input) {
		List<FndBank> bank = mstFndBankDao.findAll();
		List<FndBankDto> listDto = new ArrayList<>();

		for (FndBank list : bank) {
			FndBankDto dto = new FndBankDto();
			dto.setNamaBank(list.getNamaBank());
			dto.setKodeBank(list.getKodeBank());
			dto.setKodeBank2(list.getKodeBank2());
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		if (listDto.size() > 0) {
			map.put("contentData", listDto);
			return map;
		}
		map.put("contentData", listDto);

		return map;
	}
	
	private Date tambahTahun(Date input, String masaLaku ){
		SimpleDateFormat sdf = new  SimpleDateFormat("dd/MM/yyyy");
		String tglStr[] = sdf.format(input).split("/");
		String dd = tglStr[0];
		String mm = tglStr[1];
		String yyyy = tglStr[2];
//		String days = day.format(input);
//		String monts = month.format(input);
//		String years = year.format(input);
		String newYear = String.valueOf(Integer.parseInt(yyyy)+Integer.parseInt(masaLaku));
		Date baru = new Date();
		try {
			 baru = sdf.parse(dd+"/"+mm+"/"+newYear);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return baru;
	}

	@Override
	public int save(PlPksRDto mstPlPksRDto) {
		
		List<PlPksR> listPks = mstPlPksRDao.getPlPksRByKodeRumahSakitAndKantorJr(mstPlPksRDto.getKodeRumahsakit());
		String idRsPks = null;
		
		if(listPks.size()!=0){
			for(PlPksR a : listPks){
				idRsPks = a.getIdRsPks();
			}			
			if(idRsPks!=null){
				System.out.println("idRsPks not null :\n"+JsonUtil.getJson(idRsPks));
				String incVal = idRsPks.substring(16,18);
				int countVal = Integer.valueOf(incVal)+1;
				idRsPks = mstPlPksRDto.getKantor().substring(0, 7)+"-"+mstPlPksRDto.getKodeRumahsakit()+"-"+String.format("%02d", countVal);
				System.out.println("ID PKS RS BARU : "+idRsPks);
			}
		}else{
//			idRsPks = "0200000"+"-"+mstPlPksRDto.getKodeRumahsakit()+"-"+"01";	
			idRsPks = mstPlPksRDto.getKantor().substring(0, 7)+"-"+mstPlPksRDto.getKodeRumahsakit()+"-"+"01";	
			System.out.println("ID PKS BARU : "+idRsPks);
		}
		try {
			PlPksRPK plPksRPK = new PlPksRPK();
			plPksRPK.setIdRsPks(mstPlPksRDto.getIdRsPks());
			System.out.println("find one pl pks rs");
//			PlPksR findOnePks = mstPlPksRDao.findOne(plPksRPK);
			PlPksR findOnePks = mstPlPksRDao.getPksById(mstPlPksRDto.getIdRsPks());
			if(findOnePks == null){
				System.out.println("PL PKS RS NULL ");
				PlPksR plPksR = new PlPksR();
				plPksR.setIdRsPks(idRsPks);
				plPksR.setNoPksJr(mstPlPksRDto.getNoPksJr());
				plPksR.setNoPksRs(mstPlPksRDto.getNoPksRs());
				plPksR.setTglPks(mstPlPksRDto.getTglPks());
				plPksR.setMasaLaku(mstPlPksRDto.getMasaLaku());
				plPksR.setKeterangan(mstPlPksRDto.getKeterangan());
				plPksR.setKodeRumahsakit(mstPlPksRDto.getKodeRumahsakit());
				plPksR.setCreationDate(mstPlPksRDto.getCreationDate());
				plPksR.setKodeKantorJr(mstPlPksRDto.getKantor());
				plPksR.setCreatedBy(mstPlPksRDto.getCreatedBy());
				plPksR.setFlagAktif(mstPlPksRDto.getFlagAktif());
				plPksR.setTglLakuAkhir(tambahTahun(mstPlPksRDto.getTglPks(),mstPlPksRDto.getMasaLaku()));
				System.out.println("PKS RS YANG DISIMPAN : \n"+JsonUtil.getJson(plPksR));
				mstPlPksRDao.save(plPksR);
				
			}else{
				
				System.out.println("PL PKS RS NOT NULL ");
				findOnePks.setIdRsPks(mstPlPksRDto.getIdRsPks());
				findOnePks.setNoPksJr(mstPlPksRDto.getNoPksJr());
				findOnePks.setNoPksRs(mstPlPksRDto.getNoPksRs());
				findOnePks.setTglPks(mstPlPksRDto.getTglPks());
				findOnePks.setMasaLaku(mstPlPksRDto.getMasaLaku());
				findOnePks.setKeterangan(mstPlPksRDto.getKeterangan());
				findOnePks.setKodeRumahsakit(mstPlPksRDto.getKodeRumahsakit());
//				findOnePks.setCreationDate(mstPlPksRDto.getCreationDate());
				findOnePks.setLastUpdatedBy(mstPlPksRDto.getLastUpdatedBy());
				findOnePks.setLastUpdatedDate(new Date());
				findOnePks.setKodeKantorJr(mstPlPksRDto.getKantor());
				findOnePks.setFlagAktif(mstPlPksRDto.getFlagAktif());
				findOnePks.setTglLakuAkhir(tambahTahun(mstPlPksRDto.getTglPks(),mstPlPksRDto.getMasaLaku()));
				mstPlPksRDao.save(findOnePks);
			}

//			int s = updateTablePlPksRS(mstPlPksRDto.getKodeRumahsakit());
//			listPks = new ArrayList<>();
//			listPks = mstPlPksRDao.getPlPksRByKodeRumahSakitAndKantorJr(mstPlPksRDto.getKodeRumahsakit());
			
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}
	
	@Override
	public int updateTablePlPksRS(String kodeRS){
		int s = mstPlPksRDao.updatePKSRSbyRS(kodeRS);
//		Date tglLaku = mstPlPksRDao.getTglLakuAkhirMax(kodeRS);
//		int d = mstPlPksRDao.updateFlagAktif(kodeRS, tglLaku);
		return s;
	}

	@Override
	public int save(PlRsBpjDto mstPlRsBpjDto) {
		try {
			PlRsBpj plRsBpj = new PlRsBpj();
			plRsBpj.setKodeRsBpjs(mstPlRsBpjDto.getKodeRsBpjs());
			plRsBpj.setNamaRs(mstPlRsBpjDto.getNamaRs());
			plRsBpj.setProvinsi(mstPlRsBpjDto.getProvinsi());
			plRsBpj.setKabKota(mstPlRsBpjDto.getKabKota());
			plRsBpj.setKodeKantorJr(mstPlRsBpjDto.getKodeKantorJr());
			mstPlRsBpjDao.save(plRsBpj);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> findPksByKodeRumahSakit(
			Map<String, Object> input, String kodeRumahsakit, int pageSequence,
			int size, String direction, String orderBy) {
		
		
		orderBy = "idRsPks";
		direction = "asc";

		Page<PlPksR> plPksR = mstPlPksRDao.getPlPksRByKodeRumahSakit(kodeRumahsakit,page.getPageable(pageSequence, size, direction, orderBy));
		List<PlPksRDto> listDto = new ArrayList<>();
		 for(PlPksR a : plPksR){
			 PlPksRDto dto = new PlPksRDto();
			 dto.setCreatedBy(a.getCreatedBy());
			 dto.setCreationDate(a.getCreationDate());
			 dto.setIdRsPks(a.getIdRsPks());
			 dto.setKeterangan(a.getKeterangan());
			 dto.setKodeKantorJr(a.getKodeKantorJr());
			 dto.setKodeRumahsakit(a.getKodeRumahsakit());
			 dto.setLastUpdatedBy(a.getLastUpdatedBy());
			 dto.setLastUpdatedDate(a.getLastUpdatedDate());
			 dto.setMasaLaku(a.getMasaLaku());
			 dto.setNoPksJr(a.getNoPksJr());
			 dto.setNoPksRs(a.getNoPksRs());
			 dto.setTglLakuAkhir(a.getTglLakuAkhir());
			 dto.setTglPks(a.getTglPks());
			 
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Date date = new Date();
				String tglStr[] = sdf.format(dto.getTglPks()).split("/");
				String dd = tglStr[0];
				String mm = tglStr[1];
				String yyyy = tglStr[2];
				
				int tahunTotal = Integer.parseInt(yyyy)+Integer.parseInt(dto.getMasaLaku());
				
				String tglBaruStr = dd+"/"+mm+"/"+tahunTotal;
				dto.setFlagAktif(a.getFlagAktif());
				if(a.getFlagAktif().equalsIgnoreCase("Y")){
					dto.setFlagAktifDesc("Aktif");
				}else{
					dto.setFlagAktifDesc("Tidak Aktif");
				}
				/*try{
					Date tglBaru = sdf.parse(tglBaruStr);
					if(tglBaru.before(date)){
						dto.setFlagAktif("N");
						dto.setFlagAktifDesc("Tidak Aktif");
					}else if(tglBaru.after(date)){
						dto.setFlagAktif("Y");
						dto.setFlagAktifDesc("Aktif");
					}					
				}catch(Exception e){
					e.printStackTrace();
				}*/
			 listDto.add(dto);
//			 listDto.add(mapperFacade.map(a, PlPksRDto.class));
		 }
//		listDto = mapperFacade.mapAsList(plPksR, MstPlPksRDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", plPksR.getTotalElements());
		map.put("contentData", listDto);
		return map;
	}
	@Override
	public Map<String, Object> findPksByKodeRumahSakit(
			Map<String, Object> input, String kodeRumahsakit) {
		
		
//		orderBy = "idRsPks";
//		direction = "asc";
		
		List<PlPksR> plPksR = mstPlPksRDao.getPlPksRByKodeRumahSakitNoPage(kodeRumahsakit);
		List<PlPksRDto> listDto = new ArrayList<>();
		for(PlPksR a : plPksR){
			PlPksRDto dto = new PlPksRDto();
			dto.setCreatedBy(a.getCreatedBy());
			dto.setCreationDate(a.getCreationDate());
			dto.setIdRsPks(a.getIdRsPks());
			dto.setKeterangan(a.getKeterangan());
			dto.setKodeKantorJr(a.getKodeKantorJr());
			dto.setKodeRumahsakit(a.getKodeRumahsakit());
			dto.setLastUpdatedBy(a.getLastUpdatedBy());
			dto.setLastUpdatedDate(a.getLastUpdatedDate());
			dto.setMasaLaku(a.getMasaLaku());
			dto.setNoPksJr(a.getNoPksJr());
			dto.setNoPksRs(a.getNoPksRs());
			dto.setTglLakuAkhir(a.getTglLakuAkhir());
			dto.setTglPks(a.getTglPks());
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date date = new Date();
			String tglStr[] = sdf.format(dto.getTglPks()).split("/");
			String dd = tglStr[0];
			String mm = tglStr[1];
			String yyyy = tglStr[2];
			
			int tahunTotal = Integer.parseInt(yyyy)+Integer.parseInt(dto.getMasaLaku());
			
			String tglBaruStr = dd+"/"+mm+"/"+tahunTotal;
			dto.setFlagAktif(a.getFlagAktif());
			if(a.getFlagAktif().equalsIgnoreCase("Y")){
				dto.setFlagAktifDesc("Aktif");
			}else{
				dto.setFlagAktifDesc("Tidak Aktif");
			}
			/*try{
					Date tglBaru = sdf.parse(tglBaruStr);
					if(tglBaru.before(date)){
						dto.setFlagAktif("N");
						dto.setFlagAktifDesc("Tidak Aktif");
					}else if(tglBaru.after(date)){
						dto.setFlagAktif("Y");
						dto.setFlagAktifDesc("Aktif");
					}					
				}catch(Exception e){
					e.printStackTrace();
				}*/
			listDto.add(dto);
//			 listDto.add(mapperFacade.map(a, PlPksRDto.class));
		}
//		listDto = mapperFacade.mapAsList(plPksR, MstPlPksRDto.class);
		
		Map<String, Object> map = new HashMap<>();
//		map.put("totalRecords", plPksR.getTotalElements());
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findRekByKodeRumahSakit(
			Map<String, Object> input, String kodeRumahsakit, int pageSequence,
			int size, String direction, String orderBy) {

		orderBy = "idRsRek";
		direction = "asc";

		Page<Object[]> plRekR = mstPlRekeningRDao.getRekeningByKodeRumahSakit(
				kodeRumahsakit,
				page.getPageable(pageSequence, size, direction, orderBy));
		List<PlRekeningRDto> listDto = new ArrayList<>();

		for (Object[] a : plRekR) {
			PlRekeningRDto dto = new PlRekeningRDto();
			PlRekeningR rek = (PlRekeningR) a[0];
			dto.setIdRsRek(rek.getIdRsRek());
			dto.setCreatedBy(rek.getCreatedBy());
			dto.setCreationDate(rek.getCreationDate());
			dto.setFlagEnable(rek.getFlagEnable());
			dto.setKodeBank(rek.getKodeBank());
			dto.setKodeKantorJr(rek.getKodeKantorJr());
			dto.setLastUpdatedBy(rek.getLastUpdatedBy());
			dto.setKodeRumahsakit(rek.getKodeRumahsakit());
			dto.setLastUpdatedDate(rek.getLastUpdatedDate());
			dto.setNoRekening(rek.getNoRekening());
			dto.setNamaPemilik(rek.getNamaPemilik());
			dto.setNamaBank((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", plRekR.getTotalElements());
		map.put("contentData", listDto);
		return map;
	}
	@Override
	public Map<String, Object> findRekByKodeRumahSakit(
			Map<String, Object> input, String kodeRumahsakit) {
		
		List<Object[]> plRekR = mstPlRekeningRDao.getRekeningByKodeRumahSakit(
				kodeRumahsakit);
		List<PlRekeningRDto> listDto = new ArrayList<>();
		
		for (Object[] a : plRekR) {
			PlRekeningRDto dto = new PlRekeningRDto();
			PlRekeningR rek = (PlRekeningR) a[0];
			dto.setIdRsRek(rek.getIdRsRek());
			dto.setCreatedBy(rek.getCreatedBy());
			dto.setCreationDate(rek.getCreationDate());
			dto.setFlagEnable(rek.getFlagEnable());
			dto.setKodeBank(rek.getKodeBank());
			dto.setKodeKantorJr(rek.getKodeKantorJr());
			dto.setLastUpdatedBy(rek.getLastUpdatedBy());
			dto.setKodeRumahsakit(rek.getKodeRumahsakit());
			dto.setLastUpdatedDate(rek.getLastUpdatedDate());
			dto.setNoRekening(rek.getNoRekening());
			dto.setNamaPemilik(rek.getNamaPemilik());
			dto.setNamaBank((String) a[1]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", plRekR.size());
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findDistinctMapBpjs(Map<String, Object> input) {

		List<Object[]> plRsMapBpjs = mstPlRsMapBpjDao.getDistinctLoketPj();
		List<PlRsMapBpjDto> listDto = new ArrayList<>();

		for (Object[] a : plRsMapBpjs) {
			PlRsMapBpjDto dto = new PlRsMapBpjDto();
			dto.setKodeNama((String) a[0] + " - " + (String) a[1]);
			dto.setLoketPenanggungjawab((String) a[0]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> findAllMapBpjsByLoket(Map<String, Object> input,
			String loketPenanggungjawab) {
		List<Object[]> plRsMapBpjs = mstPlRsMapBpjDao
				.getBpjsByLoket(loketPenanggungjawab);
		List<PlRsMapBpjDto> listDto = new ArrayList<>();

		for(Object[] a : plRsMapBpjs){
			PlRsMapBpjDto dto = new PlRsMapBpjDto();
			dto.setKodeNama((String) a[7] + " - " + (String) a[11]);
			dto.setLoketPenanggungjawab((String) a[0]);
			dto.setAlamatRsBpjs((String)a[2]);
			dto.setCreatedBy((String) a[3]);
			dto.setCreationDate((Date)a[4]);
			dto.setFlagKodeBpjsDobel((String)a[5]);
			dto.setKabKotaRsBpjs((String)a[6]);
			dto.setKodeBpjs((String)a[7]);
			dto.setKodeInstansi((String)a[8]);
			dto.setLastUpdatedBy((String)a[9]);
			dto.setLastUpdatedDate((Date)a[10]);
			dto.setNamaRsBpjs((String)a[11]);
			dto.setPropinsiRsBpjs((String)a[12]);
			dto.setKodeRumahsakit((String)a[13]);
			listDto.add(dto);
			System.err.println("test "+(String)a[8]);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findDistinctMapBpjsByKodeRumahSakit(
			Map<String, Object> input, String kodeRumahsakit) {
		
		List<Object[]> mapBpjs = mstPlRsMapBpjDao.getDistinctLoketPjByKodeRumahSakit(kodeRumahsakit);
		List<PlRsMapBpjDto> listDto = new ArrayList<>();
		
		for (Object[] a : mapBpjs) {
			PlRsMapBpjDto dto = new PlRsMapBpjDto();
			dto.setKodeNama((String) a[0] + " - " + (String) a[1]);
			dto.setLoketPenanggungjawab((String) a[0]);
			dto.setKodeBpjs((String)a[2]);
			dto.setNamaRsBpjs((String)a[3]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;				
	}

	@Override
	public Map<String, Object> findAllRs(Map<String, Object> input) {
		List<PlRumahSakit> bank = mstPlRumahSakitDao.findAll();
		List<PlRumahSakitDto> listDto = new ArrayList<>();

		for (PlRumahSakit list : bank) {
			PlRumahSakitDto dto = new PlRumahSakitDto();
			dto.setKodeRumahsakit(list.getKodeRumahsakit());
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public int delete(PlPksRDto mstPlPksRDto) {
		try {
			PlPksRPK plPksRPK = new PlPksRPK();
			plPksRPK.setIdRsPks(mstPlPksRDto.getIdRsPks());
//			mstPlPksRDao.delete(plPksRPK);
			mstPlPksRDao.deletePks(mstPlPksRDto.getIdRsPks());
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int delete(PlRekeningRDto mstPlRekeningRDto) {
		try {
			PlRekeningRPK plRekeningRPK = new PlRekeningRPK();
			plRekeningRPK.setIdRsRek(mstPlRekeningRDto.getIdRsRek());
			
			List<PlPenyelesaianSantunan> listPeny = mstPlRekeningRDao.getPenyFromRekDanBank(mstPlRekeningRDto.getNoRekening(), mstPlRekeningRDto.getKodeBank());
			if(listPeny!=null){
				if(listPeny.size()>0){
					return 2;
				}
			}
			
//			mstPlRekeningRDao.delete(plRekeningRPK);
			mstPlRekeningRDao.deleteRek(mstPlRekeningRDto.getIdRsRek());
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int save(PlRumahSakitDto mstPlRumahSakitDto, PlRsMapBpjDto mapBpjDto) {
		try {
			PlRumahSakit plRumahSakit = convertToModel(mstPlRumahSakitDto);
			mstPlRumahSakitDao.save(plRumahSakit);
			
			PlRsMapBpj plRsMapBpj = convertToModelBpjs(mapBpjDto);
			mstPlRsMapBpjDao.save(plRsMapBpj);

			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}

	}
	
	public int checkMasaBerlaku(Date tglPks, String masaLaku){
		try{
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}
}
