package core.service.master.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.spring.Paging;
import common.util.CommonConstants;
import common.util.JsonUtil;
import common.util.StringUtil;
import share.FndCamatDto;
import share.FndKantorJasaraharjaDto;
import core.dao.FndCamatDao;
import core.dao.FndKabKotaDao;
import core.dao.FndProvinsiDao;
import core.model.FndCamat;
import core.model.FndKabkota;
import core.model.FndKantorJasaraharja;
import core.model.FndProvinsi;
import core.model.PlMappingCamat;
import core.service.master.MstFndCamatSvc;

@Service
@Transactional
public class MstFndCamatSvcImpl implements MstFndCamatSvc {

	@Autowired
	private FndCamatDao fndCamatDao;
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private FndProvinsiDao fndProvinsiDao;
	
	@Autowired
	private FndKabKotaDao fndKabKotaDao;
	
	Paging page = new Paging();
	
	private FndCamat convertToModel(FndCamatDto obj) {
		return mapperFacade.map(obj, FndCamat.class);
	}

	@Override
	public Map<String, Object> loadList(Map<String, Object> input, String search) {
		
		List<FndCamat> lokasi = fndCamatDao.getDataListByProvinsi(search);
		List<FndCamatDto> listDto = new ArrayList<>();
		
		for(FndCamat lok : lokasi){
			FndCamatDto dto=new FndCamatDto();
			dto.setCatatan(lok.getCatatan());
			dto.setFlagEnable(lok.getFlagEnable());
			dto.setKodeCamat(lok.getKodeCamat());
			dto.setKodeKabkota(lok.getKodeKabkota());
			dto.setKodeProvinsi(lok.getKodeProvinsi());
			dto.setNamaCamat(lok.getNamaCamat());
			dto.setNamaKabkota(lok.getNamaKabkota());
			dto.setNamaProvinsi(lok.getNamaProvinsi());
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}
	
	@Override
	public Map<String, Object> general(Map<String, Object> input){
		Map<String, Object> out = new HashMap<>();
		out.put("status", 1);
		out.put("message","");
		out.put("content", null);
		out.put("total", 0L);
		String option = "";
		
		try{
			option = (String) input.get("option");
			if(option.equalsIgnoreCase("listKantor")){
				int seq = (int) input.get("seq");
				int size = (int) input.get("size");
				String dir = (String) input.get("direction");
				String orderBy = (String) input.get("orderBy");
				Map<String, Object> map = getListKantorJR(seq, size, dir, orderBy);
				out.put("content", map.get("content"));
				out.put("message", "OK");
				out.put("total", map.get("total")) ;
			}else if("searchKantor".equalsIgnoreCase((String) input.get("option"))){
				out.putAll(searchListKantorJR(input));
			}
		}catch(Exception s){
			s.printStackTrace();
		}
		
		return out;
	}
	
	private Map<String, Object> searchListKantorJR(Map<String, Object> input){
		int seq = (int) input.get("seq");
		int size = (int) input.get("size");
		String dir = (String) input.get("direction");
		String orderBy = (String) input.get("orderBy");
		String search = StringUtil.surroundString((String) input.get("search"), "%");
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		Page<FndKantorJasaraharja> pageModel ;
		try{
			pageModel = fndCamatDao.searchEnabledKantor(search, page.getPageable(seq, size, dir, orderBy));
			List<FndKantorJasaraharja> listModel = pageModel.getContent();
			for(FndKantorJasaraharja m : listModel){
				FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
				dto.setKodeKantor(m.getKodeKantor());
				dto.setKodeKantorJr(m.getKodeKantorJr());
				dto.setNama(m.getNama());
				dto.setKodeNama(m.getKodeKantorJr() + " - " + m.getNama());
				listDto.add(dto);
			}
			
			out.put("total", pageModel.getTotalElements());
			out.put("content", listDto);
		}catch(Exception s){
			s.printStackTrace();
			return new HashMap<>();
		}
//		System.out.println("INSIDE METHOD SEaRCH LIST KANTOR");
//		System.out.println(JsonUtil.getJson(out));
		
		return out;
	}
	private Map<String, Object> getListKantorJR(int pageSequence, int size, String direction, String orderBy){
		
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		Page<FndKantorJasaraharja> pageModel ;
		try{
			pageModel = fndCamatDao.getEnabledKantor(page.getPageable(pageSequence, size, direction, orderBy));
			List<FndKantorJasaraharja> listModel = pageModel.getContent();
			for(FndKantorJasaraharja m : listModel){
				FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
				dto.setKodeKantor(m.getKodeKantor());
				dto.setKodeKantorJr(m.getKodeKantorJr());
				dto.setNama(m.getNama());
				dto.setKodeNama(m.getKodeKantorJr() + " - " + m.getNama());
				listDto.add(dto);
			}
			
			out.put("total", pageModel.getTotalElements());
			out.put("content", listDto);
		}catch(Exception s){
			s.printStackTrace();
			return new HashMap<>();
		}
//		System.out.println("INSIDE METHOD GET LIST KANTOR");
//		System.out.println(JsonUtil.getJson(out));
		
		return out;
	}
	
	@Override
	public Map<String, Object> findLokasi(Map<String, Object> input, String search) {
		
		List<Object[]> fnd = fndCamatDao.getLokasi(StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<FndCamatDto> listDto = new ArrayList<>();
		
		for(Object[] a : fnd){
			FndCamatDto dto = new FndCamatDto();
			dto.setKodeNamaCamat((String)a[0] + "-" + (String)a[1]);
			dto.setKodeCamat((String)a[0]);
			dto.setNamaCamat((String)a[1]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		
		return map;
	}

	@Override
	public Map<String, Object> findProv(Map<String, Object> input, String search) {
		
		List<Object[]> fnd = fndCamatDao.getProvList(StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<FndCamatDto> listDto = new ArrayList<>();
		
		for(Object[] a : fnd){
			FndCamatDto dto = new FndCamatDto();
			dto.setKodeNamaProv((String)a[0] + "-" + (String)a[1]);
			dto.setKodeProvinsi((String)a[0]);
			dto.setNamaProvinsi((String)a[1]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		
		return map;
	}
	
	@Override
	public Map<String, Object> findKodeKabbyProv(Map<String, Object> input, String kodeProvinsi) {
		
		List<Object[]> fnd = fndCamatDao.findKodeKabKotaByProv(kodeProvinsi);
		List<FndCamatDto> listDto = new ArrayList<>();
		
		for(Object[] a : fnd){
			FndCamatDto dto = new FndCamatDto();
			dto.setKodeNamaKabKota((String)a[0] +" - "+ (String)a[1]);
			dto.setKodeKabkota((String)a[0]);
			dto.setNamaKabkota((String)a[1]);
			dto.setKodeProvinsi((String)a[2]);
			dto.setNamaProvinsi((String)a[3]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		
		return map;
	}
	
	@Override
	public int save(FndCamatDto fndCamatDto) {
		try {
			if(fndCamatDto.getMode().equalsIgnoreCase("edit")){
				List<PlMappingCamat> camas = fndCamatDao.getMappingCamatByKodeCamat(fndCamatDto.getKodeCamat());
				if(camas !=null && camas.size()>0){
					return 5;
				}
				List<FndCamat> s = fndCamatDao.getCamatFromKode(fndCamatDto.getKodeCamat());
				if(s.size()>0){
					FndCamat sl = s.get(0);
					if(!sl.getKodeProvinsi().equalsIgnoreCase(fndCamatDto.getKodeProvinsi())){
						return 6;
					}
				}
				
			}
			
			FndCamat fndCamat = convertToModel(fndCamatDto);
//			System.out.println("================================");
//			System.out.println(JsonUtil.getJson(fndCamatDto));
//			System.out.println("--------------------------------");
//			System.out.println(JsonUtil.getJson(fndCamat));
			FndKabkota kab = new FndKabkota();
			FndProvinsi prov = new FndProvinsi();
			FndKabkota kabs = new FndKabkota();
			FndCamat camt = new FndCamat();
			prov = fndProvinsiDao.getProfPk(fndCamat.getKodeProvinsi());
			kab = fndKabKotaDao.getKabKota(fndCamat.getKodeKabkota());
			List<FndCamat> lCmt = fndCamatDao.getCamatFromKode(fndCamat.getKodeCamat()); 
			if(lCmt.size()>0){
				camt = lCmt.get(0); 
			}
			FndProvinsi po = new FndProvinsi();
			
			
			if(prov!=null && prov.getKodeProvinsi()!=null){
				System.out.println("PROV not null");
				if(prov.getKodeProvinsi().equalsIgnoreCase(fndCamat.getKodeProvinsi())){
					System.out.println(prov.getNamaProvinsi());
					System.out.println(fndCamat.getNamaProvinsi());
					if(!prov.getNamaProvinsi().equalsIgnoreCase(fndCamat.getNamaProvinsi())){
						System.out.println("return 3");
						return 3;
					}
				}
			}else{
				prov = new FndProvinsi();
				prov.setCreatedBy(fndCamatDto.getCreatedBy());
				prov.setCreationDate(fndCamatDto.getCreatedDate());
				prov.setFlagEnable(fndCamatDto.getFlagEnable());
				prov.setKodeProvinsi(fndCamatDto.getKodeProvinsi());
				prov.setNamaProvinsi(fndCamatDto.getNamaProvinsi());
			}
			
			if(kab!=null && kab.getKodeKabkota()!=null){
				if(!kab.getNamaKabkota().equalsIgnoreCase(fndCamat.getNamaKabkota())){
//					System.out.println("return 2");
//					return 2;
				}
				
				kab.setNamaKabkota(fndCamatDto.getNamaKabkota());
				kab.setFlagEnable(fndCamatDto.getFlagEnable());
				kab.setKodeProvinsi(fndCamatDto.getKodeProvinsi());
				
			}else{
				kab = new FndKabkota();
				kab.setCreatedBy(fndCamatDto.getCreatedBy());
				kab.setCreationDate(fndCamatDto.getCreatedDate());
				kab.setKodeKabkota(fndCamatDto.getKodeKabkota());
				kab.setNamaKabkota(fndCamatDto.getNamaKabkota());
				kab.setFlagEnable(fndCamatDto.getFlagEnable());
				kab.setKodeProvinsi(fndCamatDto.getKodeProvinsi());
			}
			
			try {
				if(camt!=null && camt.getKodeCamat()!=null){
					System.out.println();
					return 4;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
//				return 4;
			}
			
			fndProvinsiDao.save(prov);
			fndKabKotaDao.save(kab);
			fndCamatDao.saveLokasi(fndCamat);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int update(FndCamatDto fndCamatDto) {
		try {
			FndCamat fndCamat = convertToModel(fndCamatDto);
			System.out.println("================================");
			System.out.println(JsonUtil.getJson(fndCamatDto));
			System.out.println("--------------------------------");
			System.out.println(JsonUtil.getJson(fndCamat));
			FndKabkota kab = new FndKabkota();
			FndProvinsi prov = new FndProvinsi();
			FndKabkota kabs = new FndKabkota();
			prov = fndProvinsiDao.getProfPk(fndCamat.getKodeProvinsi());
			kab = fndKabKotaDao.getKabKota(fndCamat.getKodeKabkota());
			FndProvinsi po = new FndProvinsi();
			
			if(prov!=null && prov.getKodeProvinsi()!=null){
				System.out.println("PROV not null");
				if(prov.getKodeProvinsi().equalsIgnoreCase(fndCamat.getKodeProvinsi())){
					System.out.println(prov.getNamaProvinsi());
					System.out.println(fndCamat.getNamaProvinsi());
					if(!prov.getNamaProvinsi().equalsIgnoreCase(fndCamat.getNamaProvinsi())){
						System.out.println("return 3");
						prov.setNamaProvinsi(fndCamat.getNamaProvinsi());
//						return 3;
					}
				}
			}else{
				 po = new FndProvinsi();
				po.setCreatedBy(fndCamatDto.getCreatedBy());
				po.setCreationDate(fndCamatDto.getCreatedDate());
				po.setFlagEnable("Y");
				po.setKodeProvinsi(fndCamatDto.getKodeProvinsi());
				po.setNamaProvinsi(fndCamatDto.getNamaProvinsi());
			}
			
			if(kab!=null && kab.getKodeKabkota()!=null){
				if(!kab.getNamaKabkota().equalsIgnoreCase(fndCamat.getNamaKabkota())){
					if(fndCamatDto.getMode().equalsIgnoreCase("add")){
						return 2;
//						System.out.println("return 2");
					}else{
						kab.setNamaKabkota(fndCamat.getNamaKabkota());
					}
				}
			}else{
				kabs = new FndKabkota();
				kabs.setCreatedBy(fndCamatDto.getCreatedBy());
				kabs.setCreationDate(fndCamatDto.getCreatedDate());
				kabs.setKodeKabkota(fndCamatDto.getKodeKabkota());
				kabs.setNamaKabkota(fndCamatDto.getNamaKabkota());
				kabs.setFlagEnable(fndCamatDto.getFlagEnable());
				kabs.setKodeProvinsi(fndCamatDto.getKodeProvinsi());
			}
			
			fndProvinsiDao.save(prov);
			fndKabKotaDao.save(kab);
//			fndCamatDao.saveLokasi(fndCamat);
			fndCamatDao.save(fndCamat);
			fndCamatDao.updateKabKotaName(fndCamatDto.getNamaKabkota(), fndCamatDto.getKodeKabkota());
			fndCamatDao.updateProvName(fndCamatDto.getNamaProvinsi(), fndCamatDto.getKodeProvinsi());
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
		/*try {
			FndCamat fndCamat = convertToModel(fndCamatDto);
			fndCamatDao.updateLokasi(fndCamat);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}*/
	}

	@Override
	public int delete(FndCamatDto fndCamatDto) {
		try {
			FndCamat fndCamat = convertToModel(fndCamatDto);
			fndCamatDao.save(fndCamat);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> getDataList(Map<String, Object> input, String kodeNama,
			String search) {
		
			List<FndCamat> lokasi = fndCamatDao.getDataList(StringUtil.surroundString(StringUtil.nevl(kodeNama, "%%"), "%"), StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
			List<FndCamatDto> listDto = new ArrayList<>();
			
			for(FndCamat lok : lokasi){
				FndCamatDto dto=new FndCamatDto();
				dto.setCatatan(lok.getCatatan());
				dto.setFlagEnable(lok.getFlagEnable());
				dto.setKodeCamat(lok.getKodeCamat());
				dto.setKodeKabkota(lok.getKodeKabkota());
				dto.setKodeProvinsi(lok.getKodeProvinsi());
				dto.setNamaCamat(lok.getNamaCamat());
				dto.setNamaKabkota(lok.getNamaKabkota());
				dto.setNamaProvinsi(lok.getNamaProvinsi());
				listDto.add(dto);
			}
			
			Map<String, Object> map = new HashMap<>();
			map.put("totalRecords", new Long(1));
			map.put("contentData", listDto);
			return map;
	}

}
