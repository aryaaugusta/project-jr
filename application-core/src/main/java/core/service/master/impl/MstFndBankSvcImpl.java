package core.service.master.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;






import javax.transaction.Transactional;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import share.FndBankDto;
import share.PlRekeningRDto;
import common.spring.Paging;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.dao.FndBankDao;
import core.dao.PlRekeningRDao;
import core.model.FndBank;
import core.service.master.MstFndBankSvc;

@Service
@Transactional
public class MstFndBankSvcImpl implements MstFndBankSvc{
Paging page = new Paging();
	
	@Autowired
	private FndBankDao mstFndBankDao;
	
	@Autowired
	PlRekeningRDao mstPlRekeningRDao;
	
	@Autowired
	private MapperFacade mapperFacade;
	
	private Paging paging = new Paging();
	
	private FndBank convertToModel(FndBankDto obj) {
		return mapperFacade.map(obj, FndBank.class);
	}


	@Override
	public Map<String, Object> loadlist(Map<String, Object> input, String search){
	
		List<Object[]> fndBank = mstFndBankDao.getListDataBank(StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
		
		List<FndBankDto> listDto = new ArrayList<>();
		int i=1;
		for (Object[] a : fndBank) {
			FndBankDto dto = new FndBankDto();
			dto.setNo(i++);
			dto.setKodeBank((String) a[0]);
			dto.setNamaBank((String) a[1]);
			dto.setJumlahRekening((BigDecimal)a[2]);
			dto.setKodeBank2((String) a[3]);
			dto.setStatus((String) a[4]);
			
			
			if(dto.getStatus().equalsIgnoreCase("Y")){
				dto.setStatusFlag("Aktif");
			}else if (dto.getStatus().equalsIgnoreCase("N")) {
				dto.setStatusFlag("Tidak Aktif");	
			}
			
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}


	@Override
	public Map<String, Object> findRekByKodeBank(Map<String, Object> input,String kodeBank, String search) {


		List<Object[]> plRekR = mstPlRekeningRDao.getRekeningByKodeBank(
				kodeBank, StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<PlRekeningRDto> listDto = new ArrayList<>();
		int i=1;
		for (Object[] a : plRekR) {
			PlRekeningRDto dto = new PlRekeningRDto();
			dto.setNo(i++);
			FndBank bank = (FndBank) a [0];
			dto.setKodeBank(bank.getKodeBank());
			dto.setNamaRumahSakit((String) a[1]);
			dto.setNoRekening((String) a[2]);
			dto.setNamaPemilik((String) a[3]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}


	@Override
	public int save(FndBankDto mstFndBankDto) {
		try {
			FndBank fndBank = convertToModel(mstFndBankDto);
			List<FndBank> a = null;
			List<FndBank> b = null;
			a = mstFndBankDao.getBankFromKode(mstFndBankDto.getKodeBank());
			b = mstFndBankDao.getBankFromKode2(mstFndBankDto.getKodeBank2());
			if(mstFndBankDto.getMode().equalsIgnoreCase("add")){
				if(a != null & a.size()>0){
					return 2;
				}
				if(b != null & b.size()>0){
					return 3;
				}
			}else{
				if(b != null & b.size()>0){
					if(! (b.get(0).getKodeBank2().equalsIgnoreCase(mstFndBankDto.getKodeBank2()))){
						return 3;
					}
				}
			}
			mstFndBankDao.save(fndBank);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}
	

	@Override
	public int update(FndBankDto mstFndBankDto) {
		// TODO Auto-generated method stub
		return 0;
	}

}
