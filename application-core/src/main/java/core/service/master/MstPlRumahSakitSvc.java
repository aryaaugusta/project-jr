package core.service.master;

import java.util.Map;

import share.PlPksRDto;
import share.PlRekeningRDto;
import share.PlRsBpjDto;
import share.PlRsMapBpjDto;
import share.PlRumahSakitDto;

public interface MstPlRumahSakitSvc {

	public abstract Map<String, Object> loadList(Map<String, Object> input, String kanWil, String kodeNama, String search);
	public Map<String, Object> findAllBank(Map<String, Object> input);
	public Map<String, Object> findPksByKodeRumahSakit(Map<String, Object> input,String kodeRumahsakit, int pageSequence, int size, String direction, String orderBy);
	public Map<String, Object> findPksByKodeRumahSakit(Map<String, Object> input,String kodeRumahsakit);
	public Map<String, Object> findRekByKodeRumahSakit(Map<String, Object> input,String kodeRumahsakit, int pageSequence, int size, String direction, String orderBy);
	public Map<String, Object> findRekByKodeRumahSakit(Map<String, Object> input,String kodeRumahsakit);
	public Map<String, Object> findDistinctMapBpjs(Map<String, Object> input);
	public Map<String, Object> findDistinctMapBpjsByKodeRumahSakit(Map<String, Object> input, String kodeRumahsakit);
	public Map<String, Object> findAllMapBpjsByLoket(Map<String, Object> input, String loketPenanggungjawab);
	public Map<String, Object> findAllRs(Map<String, Object> input);
	public int updateTablePlPksRS(String kodeRS);

	public int save(PlRumahSakitDto mstPlRumahSakitDto, PlRsMapBpjDto mapBpjDto);
	public int save(PlRumahSakitDto mstPlRumahSakitDto);
	public int delete(PlRumahSakitDto mstPlRumahSakitDto);
	public int save(PlRsMapBpjDto mstPlRsMapBpjDto);
	public int save(PlRekeningRDto mstPlRekeningRDto);
	public int save(PlPksRDto mstPlPksRDto);
	public int save(PlRsBpjDto mstPlRsBpjDto);
	public int delete(PlPksRDto mstPlPksRDto);
	public int delete(PlRekeningRDto mstPlRekeningRDto);
	
}
