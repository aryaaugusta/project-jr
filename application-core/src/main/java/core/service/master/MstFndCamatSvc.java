package core.service.master;

import java.util.Map;

import share.FndCamatDto;

public interface MstFndCamatSvc {
	
	public abstract Map<String, Object> loadList(Map<String, Object> input, String search);
	public abstract Map<String, Object> getDataList(Map<String, Object> input, String kodeNama,String search);
	
	public int save(FndCamatDto mstLokasiDto);
	public int update(FndCamatDto mstLokasiDto);
	public int delete(FndCamatDto mstLokasiDto);

	public Map<String, Object> findProv(Map<String, Object> input, String search);
	public Map<String, Object> findKodeKabbyProv(Map<String, Object> input, String kodeProvinsi);
	public Map<String, Object> findLokasi(Map<String, Object> input, String search);
	
	public Map<String, Object> general(Map<String, Object> input);

}
