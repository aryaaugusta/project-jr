package core.service.master;

import java.util.Map;

import share.FndBankDto;

public interface MstFndBankSvc {
	public Map<String, Object> loadlist(Map<String, Object> input, String search);
	public int save(FndBankDto mstFndBankDto);
	public int update(FndBankDto mstFndBankDto);	
	
	public Map<String, Object> findRekByKodeBank(Map<String, Object> input,String kodeBank, String search);

}
