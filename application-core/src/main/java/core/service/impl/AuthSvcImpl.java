package core.service.impl;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DistinguishedName;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.common.AuthElementDto;
import share.common.UserMenuDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.util.AppException;
import common.util.RandomString;
import core.dao.common.AuthDao;
import core.model.AuthElement;
import core.model.AuthGroupMember;
import core.model.AuthUser;
import core.model.FndKantorJasaraharja;
import core.service.AuthSvc;

@Service
@Transactional
public class AuthSvcImpl implements AuthSvc {

	@Autowired
	LdapTemplate ldapTemplate;
	
	@Autowired
	MapperFacade mapperFacade;
	
	@Autowired
	AuthDao authDao;
//	private String sessionCode;
	
	private DistinguishedName getDnFrom(String userName) {
		return new DistinguishedName("uid=" + userName);
	}
	
	private String md5Check(String user, String pwd){
		try{
			String HASH_INPUT = "hf656jdsk7ftsahFBDJH";
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] mds = md.digest((user+pwd+HASH_INPUT).getBytes(StandardCharsets.UTF_16LE));
			
			StringBuilder sb = new StringBuilder();
	        for (byte b : mds) {
	            sb.append(String.format("%02x", b));
	        }
	        
	        String pwds = sb.toString().toUpperCase();
	        System.out.println(pwds);
	        int x = pwds.length();
	        String pwd2 = pwds;
	        for(int k = x; k > 0; k--){
	        	if(k != x){
	        		if(k%2==0){
	        			String zx = pwd2.substring(k);
	        			String xzx = pwd2.substring(0,k);
	        			pwd2 = xzx + "-" + zx;
	        		}
	        	}
	        }
	        return pwd2;
		}catch (Exception s){
			s.printStackTrace();
			return null;
		}
	}
	
	@Override
	public UserSessionJR authUser(String userId, String password) {

		// validate user & password from LDAP
		boolean continuez = false;
		boolean passedDB = false;
		boolean passedLDAP = false;
		if(System.getProperty("debug")!= null){
			continuez = true;
		}
		
		UserSessionJR ujr = new UserSessionJR();
		try{
//			this.sessionCode = password;
			ujr = bypassLogin(userId);
			ujr.setSessionCode(password);
		}catch (Exception s){
			s.printStackTrace();
		}
		
		AuthUser auth = new AuthUser();
		auth = authDao.getUser(userId);
		String pwd = md5Check(userId, password);
		
		if(auth.getPassword().equalsIgnoreCase(pwd)){
			passedDB = true;
		}
		
		if(!continuez){
			//sAMAccountName: arviqa.sangia
			//sAMAccountType: 805306368
			//(|(uid=*%s*)(displayName=*%s*)(cn=*%s*)(sn=*%s*))
//			System.out.println("LOGIN : "+userId);
//			System.out.println("sAMAccountName : "+auth.getAttribute5());
//			System.out.println("Password : "+password);
//			if (!ldapTemplate.authenticate("", "(sAMAccountType=" + userId + ")", password)) {
			if (ldapTemplate.authenticate("", "(sAMAccountName=" + auth.getAttribute5() + ")", password)) {
				passedLDAP = true;
			}
//			if (!ldapTemplate.authenticate("", "(uid=" + userId + ")", password)) {
//				throw new AppException("E001");
//			}
		}
		
		if(continuez && !passedLDAP){
			passedLDAP = true;
		}
		/*
		 * 1 : 1 -> 1
		 * 1 : 0 -> 1
		 * 0 : 1 -> 1
		 * 0 : 0 -> 0
		 */
		if(!(passedDB || passedLDAP)){
			throw new AppException("E001");
		}
		
		if(ujr.getLoginID()==null){
			throw new AppException("E001");
		}
		
		
		return ujr;
	}
	
	@Override
	public UserSessionJR bypassLogin(String userLogin) {
		
		AuthUser au = authDao.getUser(userLogin);
		UserSessionJR uJR = new UserSessionJR();
		List<AuthGroupMember> listAG = authDao.getGroupFromLogin(userLogin);
		List<FndKantorJasaraharja> kantor = authDao.getKantorFromLogin(au.getAttribute1());
		
		if (au != null){
			uJR.setLoginID(userLogin);
			uJR.setUserName(au.getUserName());
			uJR.setNamaKantor(kantor.get(0).getNama());
			uJR.setKantor(au.getAttribute1());
			uJR.setPhoneNumber(au.getAttribute2());
			uJR.setSessionID(RandomString.digits + "ACEFGHJKLMNPQRdefhijkprstuvwx");
			uJR.setLoginDesc(au.getDescription());
			uJR.setLevelKantor(kantor.get(0).getLevelKantor());
			uJR.setPerwakilanKantor(au.getAttribute1());
			uJR.setUserLdap(au.getAttribute5());
			if(listAG != null && listAG.size() > 0){
				uJR.setGroupCode(listAG.get(0).getGroupCode());
			}else{
				uJR.setGroupCode(null);
			}
			
			if(au.getExpiryDate().before(new Date())){
				uJR.setExpired(true);
			}else{
				uJR.setExpired(false);
			}
		}
		
		return uJR;
	}
	
	@Override
	public List<UserMenuDto> getMenuByLogin(String login){
		
		List<UserMenuDto> listMenu = new ArrayList<>();
		
		List<AuthElement> listAu = authDao.getElemetsByLogin(login);
		
		try {
			if(listAu != null && listAu.size()>0){
				for(AuthElement au : listAu){
					UserMenuDto menu = new UserMenuDto();
					menu.setMenuID(au.getElementCode());
					menu.setMenuLoc(au.getElementLink());
					menu.setMenuName(au.getDescription());
					
					listMenu.add(menu);
				}
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return listMenu;
	}
	

	@Override
	public void validateToken(UserSessionJR toBeUserSession) {
		// TODO Auto-generated method stub

	}

	@Override
	public void validateToken(String token) {
		// TODO Auto-generated method stub

	}

	@Override
	public RestResponse checkPassword(Map<String, Object> param) {
		// TODO Auto-generated method stub
		return null;
	}

}
