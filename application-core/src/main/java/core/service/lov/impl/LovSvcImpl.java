package core.service.lov.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.model.UserSessionJR;
import common.util.CommonConstants;
import common.util.JsonUtil;
import common.util.StringUtil;
import share.AuthUserDto;
import share.DasiJrRefCodeDto;
import share.FndBankDto;
import share.FndCamatDto;
import share.FndJenisKendaraanDto;
import share.FndKantorJasaraharjaDto;
import share.FndLokasiDto;
import share.PlDataKecelakaanDto;
import share.PlDisposisiDto;
import share.PlInstansiDto;
import share.PlJaminanDto;
import share.PlKorbanKecelakaanDto;
import share.PlMappingCamatDto;
import share.PlMappingPoldaDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import share.PlRsMapBpjDto;
import share.PlRumahSakitDto;
import core.dao.PlDisposisiDao;
import core.dao.common.AuthDao;
import core.dao.lov.LovDao;
import core.model.AuthGroupMember;
import core.model.AuthUser;
import core.model.DasiJrRefCode;
import core.model.FndBank;
import core.model.FndCamat;
import core.model.FndJenisKendaraan;
import core.model.FndKantorJasaraharja;
import core.model.FndLokasi;
import core.model.PlDisposisi;
import core.model.PlInstansi;
import core.model.PlJaminan;
import core.model.PlKorbanKecelakaan;
import core.model.PlMappingCamat;
import core.model.PlPenyelesaianSantunan;
import core.model.PlRegisterSementara;
import core.model.PlRekeningR;
import core.model.PlRsMapBpj;
import core.model.PlRumahSakit;
import core.service.lov.LovSvc;

@Service
@Transactional
public class LovSvcImpl implements LovSvc {

	@Autowired
	LovDao lovDao;
	
	@Autowired
	PlDisposisiDao plDisposisiDao;

	@Autowired
	MapperFacade mapperFacade;

	@Autowired
	AuthDao authDao;

	@Override
	public Map<String, Object> listKodeNamaRs(Map<String, Object> input,
			String kodeWilayah, String search) {

		List<PlRumahSakit> rs = lovDao.lovKodeNamaRs(kodeWilayah,
				StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<PlRumahSakitDto> listDto = new ArrayList<>();

		for (PlRumahSakit a : rs) {
			PlRumahSakitDto dto = new PlRumahSakitDto();
			dto.setKodeNamaRs(a.getKodeRumahsakit() + " - " + a.getDeskripsi());
			dto.setKodeRumahsakit(a.getKodeRumahsakit());
			dto.setDeskripsi(a.getDeskripsi());
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> lovWilayahKantor(Map<String, Object> input,
			String search) {
		
		UserSessionJR inpus = new UserSessionJR();
		try {
			inpus = JsonUtil.mapJsonToSingleObject(input.get("input"), UserSessionJR.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String kantor = inpus.getKantor()==null?"%":inpus.getKantor();

		AuthGroupMember memberGroup = authDao.getGroupFromLogin(inpus.getLoginID()).get(0);

		if (memberGroup != null
				&& (memberGroup.getGroupCode().equalsIgnoreCase(
						"Administrator.Pusat") || memberGroup.getGroupCode()
						.equalsIgnoreCase("Administrator.PL"))) {
			kantor = "%%%%%%%%%%";
		}
		if (memberGroup.getGroupCode().equalsIgnoreCase("Administrator.Pusat")
				|| memberGroup.getGroupCode().equalsIgnoreCase(
						"Administrator.PL")) {
			kantor = "%%%%%%%%%%";
		}
		
		List<FndKantorJasaraharja> fnd = lovDao.lovWilayahKantor(StringUtil
				.surroundString(StringUtil.nevl(search, "%%"), "%"), kantor);
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();

		for (FndKantorJasaraharja a : fnd) {
			FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
			dto.setKodeNama(a.getKodeKantorJr() + " - " + a.getNama());
			dto.setKodeKantorJr(a.getKodeKantorJr());
			dto.setNama(a.getNama());
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> lovWilayahKantor(Map<String, Object> input) {

		Map<String, Object> mapI = input;
		String search = (String) mapI.get("searh");
		String loginId = (String) mapI.get("loginId");
		String kantor = (String) mapI.get("kantor");

		AuthGroupMember memberGroup = authDao.getGroupFromLogin(loginId).get(0);

		if (memberGroup != null
				&& (memberGroup.getGroupCode().equalsIgnoreCase(
						"Administrator.Pusat") || memberGroup.getGroupCode()
						.equalsIgnoreCase("Administrator.PL"))) {
			kantor = "%%";
		}
		if (memberGroup.getGroupCode().equalsIgnoreCase("Administrator.Pusat")
				|| memberGroup.getGroupCode().equalsIgnoreCase(
						"Administrator.PL")) {
			kantor = "%%";
		}

		List<FndKantorJasaraharja> fnd = lovDao.lovWilayahKantor(
				StringUtil.surroundString(StringUtil.nevl(search, ""), "%"),
				kantor);
		// List<FndKantorJasaraharja> fnd =
		// lovDao.lovWilayahKantor(StringUtil.surroundString(StringUtil.nevl(search,
		// "%%"), "%"));
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();

		for (FndKantorJasaraharja a : fnd) {
			FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
			dto.setKodeNama(a.getKodeKantorJr() + " - " + a.getNama());
			dto.setKodeKantorJr(a.getKodeKantorJr());
			dto.setNama(a.getNama());
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> listAsalBerkas(Map<String, Object> input,
			String search) {

		List<FndKantorJasaraharja> listAsalBerkas = lovDao.getKantorAsalBerkas(search);
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();

		for (FndKantorJasaraharja a : listAsalBerkas) {
			FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
			dto.setKodeNama(a.getKodeKantorJr() + " - " + a.getNama());
			dto.setKodeKantorJr(a.getKodeKantorJr());
			dto.setNama(a.getNama());
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;

	}

	@Override
	public Map<String, Object> listSamsat(Map<String, Object> input,
			String search) {
		List<FndKantorJasaraharja> listSamsat = lovDao
				.getKantorSamsat(search);
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();

		for (FndKantorJasaraharja a : listSamsat) {
			FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
			dto.setKodeNama(a.getKodeKantorJr() + " - " + a.getNama());
			dto.setKodeKantorJr(a.getKodeKantorJr());
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listInstansi(Map<String, Object> input,
			String search) {
		List<PlInstansi> listInstansi = lovDao.getAllInstansi(search);
		List<PlInstansiDto> listDto = new ArrayList<>();

		for (PlInstansi a : listInstansi) {
			PlInstansiDto dto = new PlInstansiDto();
			dto.setKodeNama(a.getKodeInstansi() + " - " + a.getDeskripsi());
			dto.setKodeInstansi(a.getKodeInstansi());
			dto.setDeskripsi(a.getDeskripsi());
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listInstansiPembuat(Map<String, Object> input) {
		List<Object[]> listInstansiPembuat = lovDao.getInstansiPembuat();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listInstansiPembuat) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[0]);
			dto.setRvHighValue((String) a[1]);
			dto.setRvLowValue((String)a[2]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listLokasi(Map<String, Object> input,
			String search) {
		List<Object[]> listLokasi = lovDao.listLokasi(StringUtil
				.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<FndLokasiDto> listDto = new ArrayList<>();

		for (Object[] a : listLokasi) {
			FndLokasiDto dto = new FndLokasiDto();
			dto.setKodeDeskripsi((String) a[0] + " - KEC." + (String) a[1]
					+ " " + (String) a[2]);
			dto.setKodeLokasi((String) a[0]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listLingkupJaminan(Map<String, Object> input) {
		List<Object> listLingkupJaminan = lovDao.getLingkupJaminan();
		List<PlJaminanDto> listDto = new ArrayList<>();

		for (Object a : listLingkupJaminan) {
			PlJaminanDto dto = new PlJaminanDto();
			dto.setLingkupJaminan((String) a);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listJenisPertanggungan(Map<String, Object> input) {
		List<PlJaminan> listJenisPertanggungan = lovDao.getJenisPertanggungan();
		List<PlJaminanDto> listDto = new ArrayList<>();
		PlJaminanDto dtoAll = new PlJaminanDto();
		dtoAll.setPertanggungan("-");
		dtoAll.setKodeJaminan("");
		listDto.add(dtoAll);
		for (PlJaminan a : listJenisPertanggungan) {
			PlJaminanDto dto = new PlJaminanDto();

			dto.setKodeJaminan(a.getKodeJaminan());
			dto.setDeskripsi(a.getDeskripsi());
			dto.setLingkupJaminan(a.getLingkupJaminan());
			dto.setKodeLaporan(a.getKodeLaporan());
			dto.setCreatedBy(a.getCreatedBy());
			dto.setCreationDate(a.getCreationDate());
			dto.setLastUpdatedBy(a.getLastUpdatedBy());
			dto.setLastUpdatedDate(a.getLastUpdatedDate());
			dto.setPertanggungan(a.getKodeJaminan() + " - " + a.getDeskripsi());
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listSifatCidera(Map<String, Object> input) {
		List<Object[]> listSifatCidera = lovDao.getKodeSifatCidera();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listSifatCidera) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			dto.setSifatCidera((String) a[0] + " - " + (String) a[1]);
			dto.setRvHighValue((String) a[2]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listSifatKecelakaan(Map<String, Object> input) {
		List<Object[]> listSifatKecelakaan = lovDao.getKodeSifatKecelakaan();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listSifatKecelakaan) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String) a[0]);
			dto.setRvMeaning((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listKasusKecelakaan(Map<String, Object> input) {
		List<Object[]> listKasusKecelakaan = lovDao.getKasusKecelakaan();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listKasusKecelakaan) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			dto.setLowMeaning((String) a[0] + " - " + (String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listStatusKendaraan(Map<String, Object> input) {
		List<Object[]> listStatusKendaraan = lovDao.getStatusKendaraan();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listStatusKendaraan) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listJenisSim(Map<String, Object> input) {
		List<Object[]> listJenisSim = lovDao.getJenisSim();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listJenisSim) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listMerkKendaraan(Map<String, Object> input) {
		List<Object[]> listMerkKendaraan = lovDao.getMerkKendaraan();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listMerkKendaraan) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listIdentitas(Map<String, Object> input) {
		List<Object[]> listIdentitas = lovDao.getJenisId();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listIdentitas) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listJenisPekerjaan(Map<String, Object> input) {
		List<Object[]> listJenisPekerjaan = lovDao.getKodeJenisPekerjaan();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listJenisPekerjaan) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listStatusKorban(Map<String, Object> input) {
		List<Object[]> listStatusKorban = lovDao.getKodeStatusKorban();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listStatusKorban) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listStatusNikah(Map<String, Object> input) {
		List<Object[]> listStatusNikah = lovDao.getKodeStatusNikah();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listStatusNikah) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listProvinsi(Map<String, Object> input,
			String search) {
		List<Object[]> listProvinsi = lovDao.getProvinsi(StringUtil
				.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<FndCamatDto> listDto = new ArrayList<>();

		for (Object[] a : listProvinsi) {
			FndCamatDto dto = new FndCamatDto();
			dto.setKodeNamaProv((String) a[0] + " - " + (String) a[1]);
			dto.setKodeProvinsi((String) a[0]);
			dto.setNamaProvinsi((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listKabkotaByProvinsi(Map<String, Object> input,
			String kodeProvinsi, String search) {
		List<Object[]> listKabkota = lovDao.getKabkotaByProvinsi(kodeProvinsi,
				search);
		List<FndCamatDto> listDto = new ArrayList<>();

		for (Object[] a : listKabkota) {
			FndCamatDto dto = new FndCamatDto();
			dto.setKodeNamaKabKota((String) a[0] + " - " + (String) a[1]);
			dto.setKodeKabkota((String) a[0]);
			dto.setNamaKabkota((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listCamatByKabkota(Map<String, Object> input,
			String kodeKabkota, String search) {
		List<Object[]> listCamat = lovDao.getCamatByKabkota(kodeKabkota,
				StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%"));
		List<FndCamatDto> listDto = new ArrayList<>();

		for (Object[] a : listCamat) {
			FndCamatDto dto = new FndCamatDto();
			dto.setKodeNamaCamat((String) a[0] + " - " + (String) a[1]);
			dto.setKodeCamat((String) a[0]);
			dto.setNamaCamat((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listJenisDokumen(Map<String, Object> input) {
		List<Object[]> listJenisDokumen = lovDao.getJenisDokumen();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listJenisDokumen) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String) a[0]);
			dto.setRvMeaning((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	public Map<String, Object> findAllRS(String search) {
		List<Object[]> listData = lovDao.getAllRSRegister(search);
		List<PlRumahSakitDto> listDto = new ArrayList<>();

		for (Object[] plRumahSakit : listData) {
			PlRumahSakitDto dto = new PlRumahSakitDto();
			dto.setKodeRumahsakit((String) plRumahSakit[0]);
			dto.setDeskripsi((String) plRumahSakit[1]);
			dto.setKodeNamaRs(plRumahSakit[0] + " - " + plRumahSakit[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		if (listDto.size() > 0) {
			map.put("contentData", listDto);
			return map;
		}
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> findAllKantor(String search) {
		List<FndKantorJasaraharja> list = lovDao.lovKantor(search);
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();
		for (FndKantorJasaraharja a : list) {
			FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
			dto.setKodeKantorJr(a.getKodeKantorJr());
			dto.setNama(a.getNama());
			dto.setKodeNama(a.getKodeKantorJr() + " - " + a.getNama());
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}
	@Override
	public Map<String, Object> findAllKantorByLogin(Map<String, Object> input) {
		String search = "%";
		UserSessionJR jr = new UserSessionJR();
		String kantor = "";
		try{
			search = StringUtil.surroundString(StringUtil.nevl((String)input.get("search"), "%"),"%");
			jr = JsonUtil.mapJsonToSingleObject(input.get("userSession"), UserSessionJR.class);
		}catch(Exception p){
			p.printStackTrace();
		}
		List<FndKantorJasaraharja> list = lovDao.lovKantorbyLogin(search, jr.getKantor().substring(0, 2)+"%");
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();
		for (FndKantorJasaraharja a : list) {
			FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
			dto.setKodeKantorJr(a.getKodeKantorJr());
			dto.setNama(a.getNama());
			dto.setKodeNama(a.getKodeKantorJr() + " - " + a.getNama());
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listLoketPj(Map<String, Object> input,
			String search, String kodeKantorJr) {
		List<Object[]> plRsMapBpjs = lovDao.getDistinctLoketPj(search, kodeKantorJr);
		List<PlRsMapBpjDto> listDto = new ArrayList<>();

		for (Object[] a : plRsMapBpjs) {
			PlRsMapBpjDto dto = new PlRsMapBpjDto();
			dto.setKodeNama((String) a[0] + " - " + (String) a[1]);
			dto.setLoketPenanggungjawab((String) a[0]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> listKantor(Map<String, Object> input,
			String userSession) {
		List<FndKantorJasaraharja> kantor = lovDao.findKantor(userSession);
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();

		for (FndKantorJasaraharja list : kantor) {
			FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
			dto.setKodeKantorJr(list.getKodeKantorJr());
			dto.setNama(list.getNama());
			dto.setKodeNama(list.getKodeKantorJr() + "-" + list.getNama());

			listDto.add(dto);
		}
		System.err.println("test " + listDto.size());

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> listStatusProses(Map<String, Object> input) {
		List<Object[]> statusProses = lovDao.getDataProses();

		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : statusProses) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();

			dto.setRvMeaning((String) a[0]);
			dto.setRvLowValue((String) a[1]);
			listDto.add(dto);
			// listDto.add(mapperFacade.map(a, DasiJrRefCodeDto.class));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> ListStatusPenyelesaian(Map<String, Object> input) {
		List<Object[]> penyelesaian = lovDao.getDataPenyelesaian();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : penyelesaian) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();

			dto.setRvMeaning((String) a[0]);
			dto.setRvLowValue((String) a[1]);
			listDto.add(dto);

		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> ListHubunganKorban(Map<String, Object> input) {

		List<Object[]> listHubungan = lovDao.getDataHubungan();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listHubungan) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();

			dto.setRvMeaning((String) a[0]);
			dto.setRvLowValue((String) a[1]);

			listDto.add(dto);

		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> ListId(Map<String, Object> input) {
		List<Object[]> identitas = lovDao.getIdentitas();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : identitas) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			PlKorbanKecelakaan plKorbanKecelakaan = new PlKorbanKecelakaan();
			dto.setRvMeaning((String) a[0]);
			dto.setRvLowValue((String) a[1]);
			listDto.add(dto);

		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		if (listDto.size() > 0) {
			map.put("contentData", listDto);
			return map;
		}
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> listJenisKendaraan(Map<String, Object> input) {
		List<FndJenisKendaraan> jenis = lovDao.getListJenisKendaraan();
		List<FndJenisKendaraanDto> listDto = new ArrayList<>();

		for (FndJenisKendaraan a : jenis) {
			FndJenisKendaraanDto dto = new FndJenisKendaraanDto();
			dto.setKodeNama(a.getKodeJenis() + " - " + a.getDeskripsi());
			dto.setKodeJenis(a.getKodeJenis());
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> getCamatByLokasi(Map<String, Object> input,
			String kodeCamat) {

		PlMappingCamat plMappingCamat = lovDao.getLokasiByCamat(kodeCamat);
		PlMappingCamatDto dto = new PlMappingCamatDto();

		dto.setKodeCamat(plMappingCamat.getKodeCamat());
		dto.setKodeLokasi(plMappingCamat.getKodeLokasi());

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", dto);

		return map;
	}

	@Override
	public Map<String, Object> listJenisPembayaran(Map<String, Object> input) {
		List<Object[]> jenisPembayaran = lovDao.getJenisPembayaran();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : jenisPembayaran) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		if (listDto.size() > 0) {
			map.put("contentData", listDto);
			return map;
		}
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> findAllUser(String search) {
		List<AuthUser> list = lovDao.getAllUSer(search);
		List<AuthUserDto> listDto = new ArrayList<>();
		for (AuthUser a : list) {
			AuthUserDto dto = new AuthUserDto();
			dto = mapperFacade.map(a, AuthUserDto.class);
			dto.setLoginUsername(a.getLogin() + " - " + a.getUserName());
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		if (listDto.size() > 0) {
			map.put("contentData", listDto);
			return map;
		}
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> getKepalaCabangByUser(Map<String, Object> input,
			String kantorUser) {

		List<AuthUser> list = lovDao.getKepalaCabangByUser(kantorUser);
		List<AuthUserDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(list, AuthUserDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> listKesimpulanSementara(Map<String, Object> input) {
		List<Object[]> listKesimpulanSementaran = lovDao.getKesimpulanSementara();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listKesimpulanSementaran) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[0]);
			dto.setRvLowValue((String) a[1]);
			dto.setRvHighValue((String)a[2]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> listOtorisasiFlag(Map<String, Object> input) {
		List<Object[]> listOtorisasiFlag = lovDao.getOtorisasiFlag();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listOtorisasiFlag) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[0]);
			dto.setRvLowValue((String) a[1]);
			dto.setRvHighValue((String)a[2]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	}

	@Override
	public Map<String, Object> listKodeJenisPembayaran(Map<String, Object> input) {
		List<Object[]> listKodeJenisPembayaran = lovDao.getKodeJenisPembayaran();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listKodeJenisPembayaran) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[0]);
			dto.setRvLowValue((String) a[1]);
			dto.setRvHighValue((String)a[2]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
	}

	@Override
	public Map<String, Object> getListDisposisi(Map<String, Object> input,
			String noBerkas, String statusProses, String levelKantor) {
		
		List<Object[]> listKodeDisposisi = lovDao.getKodeDisposisi(statusProses, levelKantor); 
		List<DasiJrRefCodeDto> disposisiRefCodes = new ArrayList<>();
		
		for(Object[] a : listKodeDisposisi){
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String)a[0]);
			dto.setRvHighValue((String)a[1]);
			dto.setRvAbbreviation((String)a[2]);
			dto.setRvMeaning((String)a[3]);
			disposisiRefCodes.add(dto);
		}
		
		List<Object[]> listDisposisi = lovDao.listDisposisi(noBerkas);
		List<PlDisposisiDto> listDto = new ArrayList<>();

		for (Object[] a : listDisposisi) {
			PlDisposisiDto dto = new PlDisposisiDto();
			dto.setDari((String) a[0]);
			dto.setTglDisposisi((Date) a[1]);
			dto.setDisposisi((String)a[2]);
			dto.setIdDisposisi((String)a[3]);
			dto.setDariDesc((String)a[4]);
			for(DasiJrRefCodeDto dtoRefCode : disposisiRefCodes){
				if(dto.getDariDesc().equalsIgnoreCase(dtoRefCode.getRvLowValue())){
					dto.setEditVisible(true);
				}else{
					dto.setEditVisible(false);
				}
			}
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> getOneKantorByKode(String kodeKantorJr) {
		// TODO Auto-generated method stub
		List<Object[]> kantor = lovDao.getOneKantorByKode(kodeKantorJr);
		List<FndKantorJasaraharjaDto> listdto = new ArrayList<>();
		for (Object[] a : kantor) {
			FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
			dto.setNama((String)a[0]);
			dto.setKodeKantorJr((String)a[1]);
			dto.setKodeNama((String)a[1]+" - "+(String)a[0]);
			listdto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listdto);

		return map;
	}

	@Override
	public Map<String, Object> getKodeDisposisi(Map<String, Object> input,String statusProses,
			String levelKantor) {
		List<Object[]> kodeDisposisi = lovDao.getKodeDisposisi(statusProses, levelKantor);
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : kodeDisposisi) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String) a[0]);
			dto.setRvMeaning((String) a[1]);
			dto.setRvHighValue((String)a[2]);
			dto.setRvAbbreviation((String) a[3]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}
	
	//added by luthfi
	@Override
	public Map<String, Object> findOneByKodeKantor(Map<String, Object> input,
			String kodeKantor) {
		List<FndKantorJasaraharja> list = lovDao.getKantorByKodeKantor(kodeKantor);
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(list, FndKantorJasaraharjaDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	//added by luthfi
	@Override
	public Map<String, Object> findOnePenyelesaianSantunanByNoBerkas(
			Map<String, Object> input, String noBerkas) {

		List<PlPenyelesaianSantunan> list = lovDao.getPenyelesaianSantunanByNoBerkas(noBerkas);
		List<PlPenyelesaianSantunan> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(list, PlPenyelesaianSantunan.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	//added by luthfi
	@Override
	public Map<String, Object> listKodeDisposisi(Map<String, Object> input) {
		
		List<Object[]> listKodeDisposisi = lovDao.listKodeDisposisi(); 
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();
		
		for(Object[] a : listKodeDisposisi){
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String)a[0]);
			dto.setRvHighValue((String)a[1]);
			dto.setRvAbbreviation((String)a[2]);
			dto.setRvMeaning((String)a[3]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> getKantorDilimpahkan(Map<String, Object> input,
			String idKorbanKecelakaan) {
		List<Object[]> listKantorDilimpahkan = lovDao.getKantorDilimpahkan(idKorbanKecelakaan); 
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		
		for(Object[] a : listKantorDilimpahkan){
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setIdKorbanKecelakaan((String)a[0]);
			dto.setKodeKantorDilimpahkan((String)a[1]);
			dto.setNamaKantorDilimpahkan((String)a[2]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> getValueLdbp(Map<String, Object> input,
			String noBerkas) {
		List<Object[]> ldbp = lovDao.getValueLdbp(noBerkas); 
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		
		for(Object[] a : ldbp){
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setNoBerkas((String)a[0]);
			dto.setTglPenerimaan((Date)a[1]);
			dto.setTglPenyelesaian((Date)a[2]);
			dto.setNamaKorban((String)a[3]);
			dto.setDeskripsiKecelakaan((String)a[4]);
			dto.setInstansi((String)a[5]);
			dto.setKesimpulanSementara((String)a[6]);
			dto.setKodeJaminan((String)a[7]);
			dto.setKodeJaminanLaporan((String)a[8]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;

	}

	@Override
	public Map<String, Object> findAllBank(String search) {
		List<FndBank> listbank = lovDao.getAllBank(search);
		List<FndBankDto> listdto = new ArrayList<>();
		listdto = mapperFacade.mapAsList(listbank, FndBankDto.class);
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listdto);
		return map;
	}
	
	@Override
	public Map<String, Object> getMetodePembayaran(Map<String, Object> input,
			String noBerkas) {
		List<Object[]> metodePembayaran = lovDao.getMetodePembayaran(noBerkas); 
		List<PlPenyelesaianSantunanDto> listDto = new ArrayList<>();
		
		for(Object[] a : metodePembayaran){
			PlPenyelesaianSantunanDto dto = new PlPenyelesaianSantunanDto();
			dto.setJenisPembayaran((String)a[0]);
			dto.setNamaBank((String)a[1]);
			dto.setNoRekening((String)a[2]);
			dto.setNamaRekening((String)a[3]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}
	
	@Override
	public Map<String, Object> loadJRRefCode(String rvDomain, String flag,String search) {
		// TODO Auto-generated method stub
		List<Object[]> refCodeList = lovDao.getJrRefCode(rvDomain, flag,search);
		List<DasiJrRefCodeDto> refCodeDtoList = new ArrayList<>();
		
		DasiJrRefCodeDto dtoKosong = new DasiJrRefCodeDto();
		dtoKosong.setRvMeaning("");
		dtoKosong.setRvLowValue("");
		refCodeDtoList.add(dtoKosong);
		
		for (Object[] objects : refCodeList) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String)objects[0]);
			dto.setRvHighValue((String)objects[1]);
			dto.setRvMeaning((String)objects[2]);
			dto.setFlagEnable((String)objects[3]);
			dto.setRvAbbreviation((String)objects[4]);
			dto.setOrderSeq((BigDecimal)objects[5]);
			dto.setSifatCidera(dto.getRvLowValue()+" - "+dto.getRvMeaning());
			refCodeDtoList.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", refCodeDtoList);
		return map;
	}

	@Override
	public Map<String, Object> findDisposisiByIdDisposisi(
			Map<String, Object> input, String idDisposisi) {
		
		List<PlDisposisi> disposisi = lovDao.findDisposisiByIdDisposisi(idDisposisi);
		List<PlDisposisiDto> listdto = new ArrayList<>();
		listdto = mapperFacade.mapAsList(disposisi, PlDisposisiDto.class);
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listdto);
		return map;
	}

	@Override
	public int deleteDisposisiById(PlDisposisiDto plDisposisiDto) {
		try {
			PlDisposisi plDisposisi = new PlDisposisi();
			plDisposisi.setIdDisposisi(plDisposisiDto.getIdDisposisi());
			lovDao.deleteDisposisi(plDisposisi);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;						
		}		
	}

	@Override
	public Map<String, Object> instansiLakaIrsms(Map<String, Object> input,
			String kodeInstansi, String search) {
		
		List<Object[]> instansi = lovDao.getInstansiMonitoringIrsms(kodeInstansi, search);
		List<PlMappingPoldaDto> listdto = new ArrayList<>();
		
		for(Object[] a : instansi){
			PlMappingPoldaDto dto = new PlMappingPoldaDto();
			dto.setDeskripsi((String)a[0]);
			dto.setKodeInstansi((String)a[1]);
			listdto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listdto);
		return map;		
		
	}

	@Override
	public Map<String, Object> getJenisLaporan(Map<String, Object> input) {
		List<Object[]> listJenisLaporan = lovDao.getJenisLaporan();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listJenisLaporan) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String) a[0]);
			dto.setRvMeaning((String) a[1]);
			
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
	}

	@Override
	public Map<String, Object> getResponse(Map<String, Object> input) {
		List<Object[]> listResponse = lovDao.getResponse();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listResponse) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String) a[0]);
			dto.setRvMeaning((String) a[1]);
			
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
	}

	@Override
	public Map<String, Object> listCariDataLaka(Map<String, Object> input,
			String kejadianStartDate, String kejadianEndDate,
			String laporanStartDate, String laporanEndDate, String instansi,
			String noLaporan, String namaKorban, String jenisPertanggungan,
			String statusLp, String menu) {
		
		List<Object[]> listDataLaka = lovDao.getListCariDataLaka(kejadianStartDate, kejadianEndDate, laporanStartDate, laporanEndDate, instansi, noLaporan, namaKorban, jenisPertanggungan, statusLp, menu);
		List<PlDataKecelakaanDto> listDto = new ArrayList<>();
		
		for(Object[] o : listDataLaka){
			PlDataKecelakaanDto dto = new PlDataKecelakaanDto();
			dto.setNoLaporanPolisi((String)o[0]);
			dto.setTglKejadian((Date)o[1]);
			dto.setNama((String)o[2]);
			dto.setDeskripsiJaminan((String)o[3]);
			dto.setStatusLaporanPolisi((String)o[4]);
			dto.setIdKecelakaan((String)o[5]);
			dto.setIdKorbanKecelakaan((String)o[6]);
			dto.setTglLaporanPolisi((Date)o[7]);
			dto.setKodeJaminan((String)o[8]);
			dto.setKodeInstansi((String)o[9]);
			dto.setInstansiDesc((String)o[10]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
	}

	@Override
	public Map<String, Object> getKeteranganCoklit(Map<String, Object> input) {
		
		List<Object[]> listKetCoklit = lovDao.getKeteranganCoklit();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listKetCoklit) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[1]);
			dto.setRvLowValue((String) a[0]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listGadget(Map<String, Object> input) {
		List<Object[]> listGadget = lovDao.getGadget();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listGadget) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String) a[0]);
			dto.setRvMeaning((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
	}
	
	@Override
	public Map<String, Object> getPicLakaRs(Map<String, Object> input) {
		List<Object[]> listPicLakaRS = lovDao.getPicLakaRs();
		List<AuthUserDto> listDto = new ArrayList<>();

		for (Object[] a : listPicLakaRS) {
			AuthUserDto dto = new AuthUserDto();
			dto.setLogin((String) a[0]);
			dto.setUserName((String) a[1]);
			dto.setLoginUsername((String) a[0] + " - " +(String) a[1]);
			
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> getAddResponList(Map<String, Object> input) {
		List<Object[]> listAddResponse = lovDao.getAddResponseList();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listAddResponse) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String) a[0]);
			dto.setRvMeaning((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
	}

	@Override
	public Map<String, Object> getKesimpulanSurvey(Map<String, Object> input) {
		List<Object[]> listKesimpulanSurvey = lovDao.getKesimpulanSurvey();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listKesimpulanSurvey) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String) a[0]);
			dto.setRvMeaning((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
	}

	@Override
	public Map<String, Object> getLokasiByCamat(Map<String, Object> input,
			String kodeLokasi) {


		PlMappingCamat plMappingCamat = lovDao.getCamatByLokasi(kodeLokasi);
		PlMappingCamatDto dto = new PlMappingCamatDto();

		dto.setKodeCamat(plMappingCamat.getKodeCamat());
		dto.setKodeLokasi(plMappingCamat.getKodeLokasi());

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", dto);

		return map;

	}

	@Override
	public Map<String, Object> getAllByCamat(String kodeCamat) {
		List<FndCamat> list = lovDao.getAllByCamat(kodeCamat);
		List<FndCamatDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(list, FndCamatDto.class);
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findOneRS(String kodeRumahSakit) {
		List<Object[]> list = lovDao.findOneRs(kodeRumahSakit);
		List<PlRumahSakitDto> listDto = new ArrayList<>();

		for(Object[] a : list){
			PlRumahSakitDto dto = new PlRumahSakitDto();
			dto.setKodeRumahsakit((String)a[0]);
			dto.setDeskripsi((String)a[1]);
			dto.setKodeNamaRs((String)a[0]+" - "+(String)a[1]);
			listDto.add(dto);
		}
//		listDto = mapperFacade.mapAsList(list, PlRumahSakitDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> getBpjsByKodeRs(String kodeRumahSakit) {
		List<PlRsMapBpj> list = lovDao.getBpjsByKodeRs(kodeRumahSakit);
		List<PlRsMapBpjDto>listDto = new ArrayList<>();
		
		for(PlRsMapBpj a : list){
			PlRsMapBpjDto dto = new PlRsMapBpjDto();
			dto.setNamaRsBpjs(a.getNamaRsBpjs());
			dto.setAlamatRsBpjs(a.getNamaRsBpjs());
			dto.setCreatedBy(a.getCreatedBy());
			dto.setCreationDate(a.getCreationDate());
			dto.setFlagKodeBpjsDobel(a.getFlagKodeBpjsDobel());
			dto.setKabKotaRsBpjs(a.getKabKotaRsBpjs());
			dto.setKodeBpjs(a.getKodeBpjs());
			dto.setKodeInstansi(a.getKodeInstansi());
			dto.setKodeNama(a.getKodeBpjs()+" - "+a.getNamaRsBpjs());
			dto.setKodeRumahsakit(a.getKodeRumahsakit());
			dto.setLastUpdatedBy(a.getLastUpdatedBy());
			dto.setLastUpdatedDate(a.getLastUpdatedDate());
			dto.setLoketPenanggungjawab(a.getLoketPenanggungjawab());
			dto.setPropinsiRsBpjs(a.getPropinsiRsBpjs());
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> findOneBank(String kodeBank) {
		List<FndBank> list = lovDao.findOneBank(kodeBank);
		List<FndBankDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(list, FndBankDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;

	}
	
	@Override
	public Map<String, Object> getAllLokasi()
	{
		List<FndLokasi> list = lovDao.getAllLokasi();
		List<FndLokasiDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(list, FndLokasiDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;

	}

	@Override
	public Map<String, Object> getValueLdbpListDisposisi(
			Map<String, Object> input, String levelKantor, String noBerkas) {
		List<Object[]> ldbp = lovDao.getValueLdbpListDisposisi(levelKantor, noBerkas);
		List<PlDisposisiDto> listDto = new ArrayList<>();
		
		for(Object[] a : ldbp){
			PlDisposisiDto dto = new PlDisposisiDto();
			dto.setKodeDisposisi((String)a[0]);
			dto.setDariDesc((String)a[1]);
			dto.setDisposisi((String)a[2]);
			dto.setTglDisposisi((Date)a[3]);
			dto.setCreatedByDesc((String)a[4]);
			dto.setNoBerkasDisposisi((String)a[5]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findOneLokasi(Map<String, Object> input,
			String kodeLokasi) {
		List<FndLokasi> list = lovDao.findOneLokasi(kodeLokasi);
		List<FndLokasiDto> listDto = new ArrayList<>();
		
		for(FndLokasi a : list){
			FndLokasiDto dto = new FndLokasiDto();
			dto.setKodeLokasi(a.getKodeLokasi());
			dto.setDeskripsi(a.getDeskripsi());
			dto.setKodeDeskripsi(a.getKodeLokasi() +" - "+a.getDeskripsi());
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
		
	}

	@Override
	public Map<String, Object> getUserByLogin(Map<String, Object> input,
			String login) {
		
		List<AuthUser> list = lovDao.getUserByLogin(login);
		List<AuthUserDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(list, AuthUserDto.class);
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> listJenisTl(Map<String, Object> input) {
		List<Object[]> listStatusNikah = lovDao.getJenisTl();
		List<DasiJrRefCodeDto> listDto = new ArrayList<>();

		for (Object[] a : listStatusNikah) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvMeaning((String) a[0]);
			dto.setRvLowValue((String) a[1]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;

	}

	@Override
	public Map<String, Object> listLoketKantor(Map<String, Object> input,
			String search) {
		
		List<FndKantorJasaraharja> list = lovDao.listLoketkantor(search);
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();
		
		for(FndKantorJasaraharja a : list){
			FndKantorJasaraharjaDto dto = new FndKantorJasaraharjaDto();
			dto = mapperFacade.map(a, FndKantorJasaraharjaDto.class);
			dto.setKodeNama(a.getKodeKantorJr()+" - "+a.getNama());
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
		
	}

}
