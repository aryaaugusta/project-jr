package core.service.lov;

import java.util.List;
import java.util.Map;

import share.FndCamatDto;
import share.PlDisposisiDto;

public interface LovSvc {
	
	public Map<String, Object> lovWilayahKantor(Map<String, Object> input, String search);
	public Map<String, Object> lovWilayahKantor(Map<String, Object> input);
	
	//combobox for master rumah sakit
	//added by Luthfi
	public Map<String, Object> listKodeNamaRs(Map<String, Object> input, String kodeWilayah, String search);
	public Map<String, Object> listJenisDokumen(Map<String, Object> input);
	public Map<String, Object> listLoketPj(Map<String, Object> input, String search, String kodeKantorJr);
	public Map<String, Object> listJenisKendaraan(Map<String, Object> input);
	public Map<String, Object> getKepalaCabangByUser(Map<String, Object> input, String kantorUser);
	public Map<String, Object> getCamatByLokasi(Map<String, Object> input, String kodeCamat);
	public Map<String, Object> getLokasiByCamat(Map<String, Object> input, String kodeLokasi);
	public Map<String, Object> listKesimpulanSementara(Map<String, Object> input);
	public Map<String, Object> listOtorisasiFlag(Map<String, Object> input);
	public Map<String, Object> listKodeJenisPembayaran(Map<String, Object> input);
	public Map<String, Object> findOneByKodeKantor(Map<String, Object> input, String kodeKantor);
	public Map<String, Object> findOnePenyelesaianSantunanByNoBerkas(Map<String, Object> input, String noBerkas);
	public Map<String, Object> listKodeDisposisi(Map<String, Object>input);
	public Map<String, Object> getKantorDilimpahkan(Map<String, Object> input, String idKorbanKecelakaan);
	public Map<String, Object> findDisposisiByIdDisposisi(Map<String, Object> input, String idDisposisi);
	public int deleteDisposisiById(PlDisposisiDto plDisposisiDto);
	public Map<String, Object> instansiLakaIrsms(Map<String, Object> input, String kodeInstansi, String search);
	public Map<String, Object> listCariDataLaka(Map<String, Object> input, String kejadianStartDate,
			String kejadianEndDate, String laporanStartDate,
			String laporanEndDate, String instansi, String noLaporan,
			String namaKorban, String jenisPertanggungan, String statusLp, String menu);
	public Map<String, Object> getKeteranganCoklit(Map<String, Object> input);

	//List Combobox
	public Map<String, Object> listAsalBerkas(Map<String, Object> input, String search);
	public Map<String, Object> listGadget(Map<String, Object> input);
	public Map<String, Object> listSamsat(Map<String, Object> input, String search);
	public Map<String, Object> listInstansi(Map<String, Object> input, String search);
	public Map<String, Object> listInstansiPembuat(Map<String, Object> input);
	public Map<String, Object> listLokasi(Map<String, Object> input, String search);
	public Map<String, Object> listLingkupJaminan(Map<String, Object> input);
	public Map<String, Object> listJenisPertanggungan(Map<String, Object> input);
	public Map<String, Object> listSifatCidera(Map<String, Object> input);
	public Map<String, Object> listSifatKecelakaan(Map<String, Object> input);
	public Map<String, Object> listKasusKecelakaan(Map<String, Object> input);
	public Map<String, Object> listStatusKendaraan(Map<String, Object> input);
	public Map<String, Object> listJenisSim(Map<String, Object> input);
	public Map<String, Object> listMerkKendaraan(Map<String, Object> input);
	public Map<String, Object> listIdentitas(Map<String, Object> input);
	public Map<String, Object> listJenisPekerjaan(Map<String, Object> input);
	public Map<String, Object> listStatusKorban(Map<String, Object> input);
	public Map<String, Object> listStatusNikah(Map<String, Object> input);
	public Map<String, Object> listJenisTl(Map<String, Object> input);
	public Map<String, Object> listLoketKantor(Map<String, Object>input, String search);
	
	//added by Meilona
	public Map<String, Object> listProvinsi(Map<String, Object> input, String search);
	public Map<String, Object> listKabkotaByProvinsi(Map<String, Object> input, String kodeProvinsi, String search);
	public Map<String, Object> listCamatByKabkota(Map<String, Object> input, String kodeKabkota, String search);

	//for ComboBox PengajuanSantunan
	public Map<String, Object> listKantor(Map<String, Object> input, String userSession);
	public Map<String, Object> listStatusProses(Map<String, Object> input);
	public Map<String, Object> ListStatusPenyelesaian(Map<String, Object> input);
	public Map<String, Object> ListHubunganKorban(Map<String, Object> input);
	public Map<String, Object> ListId(Map<String, Object> input);
	public Map<String, Object> listJenisPembayaran(Map<String, Object>input);

	
	//maryo 
	public Map<String, Object> findAllRS(String search);
	public Map<String, Object> findAllKantor(String search);
	public Map<String, Object> findAllKantorByLogin(Map<String, Object> input);
	public Map<String, Object> findAllUser(String search);
	public Map<String, Object> findAllBank(String search);
	
	//Verifikasi by Meilona
	public Map<String, Object> getListDisposisi(Map <String, Object> input, String noBerkas, String statusProses, String levelKantor);
	public Map<String,Object> getOneKantorByKode(String kodeKantorJr);
	public Map<String,Object> getKodeDisposisi(Map <String, Object> input,String statusProses, String levelKantor);
	public Map<String,Object> getMetodePembayaran(Map<String, Object> input, String noBerkas);
	//Monitoring Laka Belum Tuntas
	public Map<String,Object> getJenisLaporan(Map<String,Object> input);
	//Monitoring Laka RS
	public Map<String, Object> getResponse(Map<String,Object> input);
	public Map<String, Object> getPicLakaRs(Map<String,Object> input);
	public Map<String, Object> getAddResponList(Map<String,Object> input);
	public Map<String, Object> getKesimpulanSurvey(Map<String,Object> input);
	
	public abstract Map<String, Object> loadJRRefCode(String rvDomain,String flag,String search);
	
	public Map<String, Object> getAllByCamat(String kodeCamat);
	public Map<String, Object> findOneRS(String kodeRumahSakit);	
	public Map<String, Object> getBpjsByKodeRs(String kodeRumahSakit);
	
	public Map<String, Object> findOneBank(String kodeBank);
	public Map<String, Object> findOneLokasi(Map<String, Object>input, String kodeLokasi);
	public Map<String, Object> getAllLokasi();


	//for ldbp
	public Map<String, Object> getValueLdbp(Map<String, Object> input, String noBerkas);
	public Map<String, Object> getValueLdbpListDisposisi(Map<String, Object> input,String levelKantor, String noBerkas);
	
	//Auth user
	public Map<String, Object> getUserByLogin(Map<String, Object> input, String login);
	
}
