package core.service.report;

import java.util.Map;

public interface ReportSvc {

	public Map<String, Object> callGeneral(Map<String, Object> input);
}
