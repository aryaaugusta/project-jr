package core.service.report.impl;

import java.awt.geom.GeneralPath;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.DasiJrRefCodeDto;
import share.PlInstansiDto;
import share.PlJaminanDto;
import common.model.UserSessionJR;
import common.util.CommonConstants;
import common.util.JsonUtil;
import common.util.StringUtil;
import core.dao.FndGeneralParamDao;
import core.dao.PlInstansiDao;
import core.dao.PlJaminanDao;
import core.dao.lov.LovDao;
import core.model.DasiJrRefCode;
import core.model.FndGeneralParam;
import core.model.FndKantorJasaraharja;
import core.model.PlInstansi;
import core.model.PlJaminan;
import core.model.PlMappingPolda;
import core.service.report.ReportSvc;

@Service
@Transactional
public class ReportSvcImpl implements ReportSvc {

	@Autowired
	MapperFacade mapperFacade;
	
	@Autowired
	LovDao lovDao;
	
	@Autowired
	PlJaminanDao plJaminanDao;
	
	@Autowired
	PlInstansiDao plInstansiDao;
	
	@Autowired
	FndGeneralParamDao fndGeneralParamDao;
	
	@Override
	public Map<String, Object> callGeneral(Map<String, Object> input) {
		// TODO Call General
		Map<String, Object> out = new HashMap<>();
		out.put("status", CommonConstants.OK_REST_STATUS);
		if(input==null || input.get("option")==null){
			int status = 0;
			status = CommonConstants.ERROR_REST_STATUS;
			out.put("status", status);
			out.put("container", null);
			out.put("total", 0);
			return out;
		}
		
		String option = (String) input.get("option");
		if(option.equalsIgnoreCase("firstLoad")){
			out = getFirstLoad(input);
		}
		if(option.equalsIgnoreCase("loadBulanan")){
			out.putAll(loadBulanan(input));
		}
		if(option.equalsIgnoreCase("instansi")){
			out.putAll(instantsiSearch(input));
		}
		if(out.get("total") == null){
			out.put("total", 0);
		}
		
		return out;
	}
	
	private Map<String, Object> loadBulanan(Map<String, Object> input){
		Map<String, Object> out = new HashMap<>();
		Map<String, Object> genPar = new HashMap<>();
		List<FndGeneralParam> listGeneral = new ArrayList<>();
		List<String> params = new ArrayList<>();
		params.add("PL.RPT.NAMAJABATAN1");
		params.add("PL.RPT.NAMAJABATAN2");
		params.add("PL.RPT.PEJABAT1");
		params.add("PL.RPT.PEJABAT2");
		String crc = (String) input.get("kodeKantor");
		
		try{
			listGeneral = fndGeneralParamDao.getPejabat(params, crc);
			for(FndGeneralParam s : listGeneral){
				genPar.put(s.getKodeParam(),s.getNilaiParam());
			}
			
			String prov = fndGeneralParamDao.getProvinsi(crc);
			genPar.put("provinsi", prov);
			genPar.put("ibukota", getIbuKota(prov));
			
		}catch(Exception s){
			s.printStackTrace();
		}
		out.put("pejabat", genPar);
		return out;
	}
	
	private String getIbuKota(String prov){
		if(prov.equalsIgnoreCase("DKI JAKARTA")){
			return "Jakarta";
		}
		if(prov.equalsIgnoreCase("JAWA BARAT")){
			return "Bandung";
		}
		if(prov.equalsIgnoreCase("ACEH")){
			return "Banda Aceh";
		}
		if(prov.equalsIgnoreCase("Sumatera Utara")){
			return "Medan";
		}
		if(prov.equalsIgnoreCase("Sumatera Barat")){
			return "Padang";
		}
		if(prov.equalsIgnoreCase("riau")){
			return "Pekanbaru";
		}
		if(prov.equalsIgnoreCase("Kepulauan Riau") || 
			prov.equalsIgnoreCase("kep riau") || 
			prov.equalsIgnoreCase("kep. riau")){
			return "Tanjung Pinang";
		}
		if(prov.equalsIgnoreCase("Jambi")){
			return "Jambi";
		}
		if(prov.equalsIgnoreCase("Bengkulu")){
			return "Bengkulu";
		}
		if(prov.equalsIgnoreCase("Sumatera Selatan")){
			return "Palembang";
		}
		if(prov.equalsIgnoreCase("Kepulauan Bangka Belitung") || 
				prov.equalsIgnoreCase("kep Bangka Belitung") || 
				prov.equalsIgnoreCase("kep Bangka") || 
				prov.equalsIgnoreCase("kep. Bangka") || 
				prov.equalsIgnoreCase("kep. Bangka Belitung")){
			return "Pangkal Pinang";
		}
		if(prov.equalsIgnoreCase("Lampung")){
			return "Bandar Lampung";
		}
		if(prov.equalsIgnoreCase("Banten")){
			return "Serang";
		}
		if(prov.equalsIgnoreCase("Jawa Tengah")){
			return "Semarang";
		}
		if(prov.equalsIgnoreCase("DI Yogyakarta")){
			return "Yogyakarta";
		}
		if(prov.equalsIgnoreCase("Jawa Timur")){
			return "Surabaya";
		}
		if(prov.equalsIgnoreCase("Bali")){
			return "Denpasar";
		}
		if(prov.equalsIgnoreCase("Nusa Tenggara Barat")){
			return "Mataram";
		}
		if(prov.equalsIgnoreCase("Nusa Tenggara Timur")){
			return "Kupang";
		}
		if(prov.equalsIgnoreCase("Kalimantan Utara")){
			return "Tanjungselor";
		}
		if(prov.equalsIgnoreCase("Kalimantan Barat")){
			return "Pontianak";
		}
		if(prov.equalsIgnoreCase("Kalimantan Tengah")){
			return "Palangkaraya";
		}
		if(prov.equalsIgnoreCase("Kalimantan Selatan")){
			return "Banjarmasin";
		}
		if(prov.equalsIgnoreCase("Kalimantan Timur")){
			return "Samarinda";
		}
		if(prov.equalsIgnoreCase("Gorontalo")){
			return "Gorontalo";
		}
		if(prov.equalsIgnoreCase("Sulawesi Utara")){
			return "Manado";
		}
		if(prov.equalsIgnoreCase("Sulawesi Barat")){
			return "Mamuju";
		}
		if(prov.equalsIgnoreCase("Sulawesi Tengah")){
			return "Palu";
		}
		if(prov.equalsIgnoreCase("Sulawesi Selatan")){
			return "Makasar";
		}
		if(prov.equalsIgnoreCase("Sulawesi Tenggara")){
			return "Kendari";
		}
		if(prov.equalsIgnoreCase("Maluku Utara")){
			return "Sofifi";
		}
		if(prov.equalsIgnoreCase("Maluku")){
			return "Ambon";
		}
		if(prov.equalsIgnoreCase("Papua Barat")){
			return "Manokwari";
		}
		if(prov.equalsIgnoreCase("Papua")){
			return "Jayapura";
		}
		else{
			return "";
		}
	}
	
	private Map<String, Object> instantsiSearch(Map<String, Object> input){
		Map<String, Object> out = new HashMap<>();
		String search = (String) input.get("search");
		System.out.println("LOGGER : search instansi by "+search);
		List<PlInstansiDto> listInst = cariInstansi(search);
		out.put("instansi", listInst);
		return out;
	}
	
	private List<PlInstansiDto> cariInstansi(String search){
		List<PlInstansiDto> listDto = new ArrayList<>();
		try{
			List<FndKantorJasaraharja> listInstansi = lovDao.lovKantor(StringUtil.surroundString(search,"%"));
			for (FndKantorJasaraharja a : listInstansi) {
				PlInstansiDto dto = new PlInstansiDto();
				dto.setKodeNama(a.getKodeKantorJr() + " - " + a.getNama());
				dto.setKodeInstansi(a.getKodeKantorJr());
				dto.setDeskripsi(a.getNama());
				listDto.add(dto);
			}
			
		}catch(Exception s){
			System.out.println("Pencarian Kantor JasaRaharja kosong");
		}
		
		return listDto;
	}
	
	private Map<String, Object> getFirstLoad(Map<String, Object> input){
		Map<String, Object> out = new HashMap<>();
		List<String> thnPenerimaan = new ArrayList<>();
		List<String> thnPenyelesaian = new ArrayList<>();
		List<String> thnPengajuan = new ArrayList<>();
		List<DasiJrRefCodeDto> listSifatKecelakaan = new ArrayList<>();
		List<DasiJrRefCodeDto> listKasusKecelakaan = new ArrayList<>();
		List<DasiJrRefCodeDto> listStatusProses = new ArrayList<>();
		List<PlJaminanDto> listJaminan= new ArrayList<>();
		List<PlInstansiDto> listInstansi = new ArrayList<>();
		String isAdmin = (String) input.get("admin");
		int status = 0;
		try{
			
			UserSessionJR session = JsonUtil.mapJsonToSingleObject(input.get("userSession"), UserSessionJR.class) ;
			String kodeKantorJr = session.getKantor();
			status = CommonConstants.OK_REST_STATUS;
			thnPenerimaan = lovDao.getTahunPenerimaanFromPengajuanSantunan();
			thnPenyelesaian = lovDao.getTahunPenyelesaianFromPengajuanSantunan();
			thnPengajuan = lovDao.getTahunPengajuanFromPengajuanSantunan();
			List<DasiJrRefCode> listSifatLaka = lovDao.getRefByRvDomain("KODE SIFAT KECELAKAAN");
			List<DasiJrRefCode> listKasustLaka = lovDao.getRefByRvDomain("PL KASUS KECELAKAAN");
			List<DasiJrRefCode> listStatus = lovDao.getRefByRvDomain("KODE STATUS PROSES");
			List<PlJaminan> lJaminan = plJaminanDao.findAll();
			List<Object[]> objs = plInstansiDao.getInstansi(kodeKantorJr.substring(0, 2)+'%');
			for(Object[] s : objs ){
				PlInstansiDto dto = mapperFacade.map(s[0], PlInstansiDto.class);
				PlMappingPolda mp = (PlMappingPolda) s[1];
				dto.setNamaPolda(mp.getNamaPolda());
				listInstansi.add(dto);
			}
			listSifatKecelakaan = mapperFacade.mapAsList(listSifatLaka, DasiJrRefCodeDto.class);
			listKasusKecelakaan = mapperFacade.mapAsList(listKasustLaka, DasiJrRefCodeDto.class);
			listStatusProses = mapperFacade.mapAsList(listStatus, DasiJrRefCodeDto.class);
			listJaminan = mapperFacade.mapAsList(lJaminan, PlJaminanDto.class);
			out.put("sifatLaka", listSifatKecelakaan);
			out.put("kasusLaka", listKasusKecelakaan);
			out.put("statusProses", listStatusProses);
			out.put("jaminan", listJaminan);
			out.put("instansi", listInstansi);
			out.put("thnPenerimaan", thnPenerimaan);
			out.put("thnPenyelesaian", thnPenyelesaian);
			out.put("thnPengajuan", thnPengajuan);
			out.put("message", null);
			out.put("isAdmin", isAdmin);
		}catch(Exception s){
			status = CommonConstants.ERROR_REST_STATUS;
			s.printStackTrace();
			out.put("message", s.getMessage());
		}
		out.put("status", status);
		
		return out;
	}

}
