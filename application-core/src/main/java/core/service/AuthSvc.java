package core.service;

import java.util.List;
import java.util.Map;

import share.common.UserMenuDto;

import common.model.RestResponse;
import common.model.UserSessionJR;

public interface AuthSvc {

		//used for authenticate user
		public UserSessionJR authUser(String userId, String password);
		
		public UserSessionJR bypassLogin(String userLogin);
		
		//used for authorized user token with user id, company id and token
		public void validateToken(UserSessionJR toBeUserSession);
		
		//used for authorized user token with token only
		public void validateToken(String token);
		
		
		public RestResponse checkPassword(Map<String, Object> param);

		public List<UserMenuDto> getMenuByLogin(String login);
	
}
