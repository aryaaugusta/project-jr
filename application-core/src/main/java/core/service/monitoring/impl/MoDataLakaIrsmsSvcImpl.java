package core.service.monitoring.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.KorlantasAccidentDto;
import share.PlKorbanKecelakaanDto;
import core.dao.PlDataKecelakaanDao;
import core.dao.PlPengajuanSantunanDao;
import core.dao.monitoring.MonitoringDao;
import core.service.monitoring.MoDataLakaIrsmsSvc;

@Service
@Transactional
public class MoDataLakaIrsmsSvcImpl implements MoDataLakaIrsmsSvc {

	@Autowired
	MonitoringDao monitoringDao;
	
	@Autowired
	PlDataKecelakaanDao plDataKecelakaanDao;
	
	@Override
	public Map<String, Object> getListIndex(Map<String, Object> input, String instansi, String kejadianStartDate, String kejadianEndDate, 
			String laporanStartDate,String laporanEndDate, String namaKorban, String noLaporan, String jenisData) {
		
		List<Object[]> listIndex = monitoringDao.getListDataLakaIrsms(instansi, kejadianStartDate, kejadianEndDate, 
				laporanStartDate,laporanEndDate, namaKorban,noLaporan, jenisData);
		List<KorlantasAccidentDto> listDto = new ArrayList<>();
		
		for(Object[] a : listIndex){
			KorlantasAccidentDto dto = new KorlantasAccidentDto();
			dto.setId((BigDecimal)a[0]);
			dto.setInstansiDesc((String)a[1]);
			dto.setTglKejadian((Date)a[2]);
			dto.setTglLaporanPolisi((Date)a[3]);
			dto.setNoLaporan((String)a[4]);
			dto.setLokasi((String)a[5]);
			dto.setKorban((String)a[6]);
			dto.setMappingStatus((String)a[7]);
			dto.setPoliceDetails((String)a[8]);
			if(dto.getMappingStatus().equalsIgnoreCase("Y")){
				dto.setMappingVisible(true);
				dto.setUnMappingVisible(false);
			}else if(dto.getMappingStatus().equalsIgnoreCase("N")){
				dto.setUnMappingVisible(true);
				dto.setMappingVisible(false);
			}
			dto.setPoliceDetails((String)a[8]);
			dto.setIdLakaJr((String)a[10]);
			dto.setIdKorbanJr((String)a[11]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;		
	}

	@Override
	public Map<String, Object> findByIdKejadian(Map<String, Object> input,
			String idKejadian, String instansi) {
		List<Object[]> listIndex = monitoringDao.findOneDataLakaIrsms(idKejadian,instansi);
		List<KorlantasAccidentDto> listDto = new ArrayList<>();
		
		for(Object[] a : listIndex){
			KorlantasAccidentDto dto = new KorlantasAccidentDto();
			dto.setId((BigDecimal)a[0]);
			dto.setInstansiDesc((String)a[1]);
			dto.setTglKejadian((Date)a[2]);
			dto.setTglLaporanPolisi((Date)a[3]);
			dto.setNoLaporan((String)a[4]);
			dto.setLokasi((String)a[5]);
			dto.setKorban((String)a[6]);
			dto.setMappingStatus((String)a[7]);
			dto.setPoliceDetails((String)a[8]);
			dto.setKodeInstansi((String)a[9]);
			dto.setIdLakaJr((String)a[10]);
			dto.setIdKorbanJr((String)a[11]);
			
			List<Object[]> korbanLaka = plDataKecelakaanDao.getKorbanByIdKorban(dto.getIdKorbanJr());
			for(Object[] o : korbanLaka){
				dto.setNamaCidera(o[1]+" - "+o[8]);
			}
			
			
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;		
	}

}
