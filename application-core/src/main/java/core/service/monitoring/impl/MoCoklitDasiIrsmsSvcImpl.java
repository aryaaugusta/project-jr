package core.service.monitoring.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.KorlantasDistrictDto;
import share.KorlantasProvinceDto;
import share.PlDataKecelakaanDto;
import share.PlMappingPoldaDto;
import core.dao.monitoring.MonitoringDao;
import core.model.KorlantasDistrict;
import core.model.KorlantasProvince;
import core.model.PlMappingPolda;
import core.service.monitoring.MoCoklitDasiIrsmsSvc;

@Service
@Transactional
public class MoCoklitDasiIrsmsSvcImpl implements MoCoklitDasiIrsmsSvc{
	
	@Autowired
	MonitoringDao monitoringDao;
	
	@Autowired
	MapperFacade mapperFacade;

	@Override
	public Map<String, Object> getListProvince(Map<String, Object> input,
			String search) {
		
		List<KorlantasProvince> listProvince = monitoringDao.getListProvince(search);
		List<KorlantasProvinceDto> listDto = new ArrayList<>();
		
		listDto = mapperFacade.mapAsList(listProvince, KorlantasProvinceDto.class);
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;		
	}

	@Override
	public Map<String, Object> getListDistrictByProvinceId(
			Map<String, Object> input, String provinceId, String search) {
		
		List<KorlantasDistrict> listProvince = monitoringDao.getListDistrictByProvinceId(provinceId, search);
		List<KorlantasDistrictDto> listDto = new ArrayList<>();
		
		listDto = mapperFacade.mapAsList(listProvince, KorlantasDistrictDto.class);
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;		
	}

	@Override
	public Map<String, Object> getListIndex(Map<String, Object> input,
			String kodeInstansi, String startDate, String endDate) {
		
		List<Object[]> listIndex = monitoringDao.getIndexCoklitDasiIrsms(kodeInstansi, startDate, endDate);
		List<PlDataKecelakaanDto> listDto = new ArrayList<>();
		
		for(Object[] o : listIndex){
			PlDataKecelakaanDto dto = new PlDataKecelakaanDto();
			dto.setTglKejadian((Date)o[0]);
			dto.setNoLaporanPolisi((String)o[1]);
			dto.setTglLaporanPolisi((Date)o[2]);
			dto.setNamaKorban((String)o[3]);
			dto.setCideraHighValue((String)o[4]);
			dto.setTglKejadianIrsms((Date)o[5]);
			dto.setNoLaporanPolisiIrsms((String)o[1]);
			dto.setTglLaporanPolisiIrsms((Date)o[6]);
			dto.setNamaKorbanIrsms((String)o[7]);
			dto.setCideraKorbanIrsms((String)o[8]);
			dto.setIdKecelakaan((String)o[9]);
			dto.setVisibleKetCoklit(false);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;		

	}

	@Override
	public Map<String, Object> getMappingPolda(Map<String, Object> input,
			String idDistrict) {
		List<PlMappingPolda> listPolda = monitoringDao.getPoldaByDistrict(idDistrict);
		List<PlMappingPoldaDto> listDto = new ArrayList<>();
		
		listDto = mapperFacade.mapAsList(listPolda, PlMappingPoldaDto.class);
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;	
	}
	
	

}
