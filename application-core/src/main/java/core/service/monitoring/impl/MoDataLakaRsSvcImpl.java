package core.service.monitoring.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.PlKorbanRsDto;
import core.dao.monitoring.MonitoringDao;
import core.service.monitoring.MoDataLakaRsSvc;

@Service
@Transactional
public class MoDataLakaRsSvcImpl implements MoDataLakaRsSvc {
	
	 @Autowired
	 MonitoringDao monitoringDao;

	@Override
	public Map<String, Object> getDataLakaRs(Map<String, Object> input,
			String namaKantor, String kodeRs, String startDate, String endDate,
			String pilihSumberData, String namaKorban, String statusResponse, String search) {
		// TODO Auto-generated method stub
		List<Object[]> lakaRs = monitoringDao.getLakaRs(namaKantor, kodeRs, startDate,endDate, pilihSumberData, namaKorban, statusResponse, search);
		List<PlKorbanRsDto> listDto= new ArrayList<>();
		
		for(Object[] a: lakaRs){
			PlKorbanRsDto dto= new PlKorbanRsDto();
			dto.setNamaRs((String) a[0]);
			dto.setTglKejadian((Date) a[1]);
			dto.setTglMasukRs((Date) a[2]);
			dto.setCreatedDate((Date) a[3]);
			dto.setBatasKunjungan((String) a[4]);
			dto.setBatasKesimpulan((String) a[5]);
			dto.setKodeKejadian((String) a[6]);
			dto.setNamaKorban((String) a[7]);
			dto.setStatus((String) a[8]);
			dto.setKodeRumahSakit((String) a[9]);
		
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> getLakaRsbyId(Map<String, Object> input,
			String kodeRumahSakit, String kodeKejadian) {
		// TODO Auto-generated method stub
		List<Object[]> lakaRsById = monitoringDao.getLakaRsById(kodeRumahSakit,kodeKejadian);
		List<PlKorbanRsDto> listDto= new ArrayList<>();
		
		for(Object[] a: lakaRsById){
			PlKorbanRsDto dto= new PlKorbanRsDto();
			dto.setKodeKejadian((String) a[0]);
			dto.setNamaRs((String) a[1]);
			dto.setTglMasukRs((Date) a[2]);
			dto.setRuangan((String) a[3]);
			dto.setTglKejadian((Date) a[4]);
			dto.setNamaKorban((String) a[5]);
			dto.setAlamat((String) a[6]);
			dto.setPic((String) a[7]);
			dto.setCreatedDate((Date) a[8]);
			dto.setKodeInstansi((String) a[9]);
			dto.setTglResponse((Date)a[10]);
			dto.setPetugas((String) a[11]);
			dto.setStatus((String) a[12]);
			
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}
	 
	 

}
