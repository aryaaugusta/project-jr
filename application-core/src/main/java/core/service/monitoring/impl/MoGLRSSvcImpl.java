package core.service.monitoring.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.util.CommonConstants;
import share.PlKorbanKecelakaanDto;
import share.PlTlRDto;
import core.dao.PlTlRDao;
import core.model.PlKorbanKecelakaan;
import core.model.PlNihilKecelakaan;
import core.model.PlTlR;
import core.service.monitoring.MoGLRSSvc;

@Service
@Transactional
public class MoGLRSSvcImpl implements MoGLRSSvc{

	@Autowired
	PlTlRDao plTlRsDao;
	
	@Autowired
	MapperFacade mapperFacade;
	
	private PlTlR convertToModel(PlTlRDto obj) {
		return mapperFacade.map(obj, PlTlR.class);
	}

	
	@Override
	public Map<String, Object> findJmlJaminanGLRS(String kodeKantor) {
		// TODO Auto-generated method stub
		List<Object[]> data = plTlRsDao.findJmlJaminanGLRS(kodeKantor);
		List<PlTlRDto> listDto = new ArrayList<>();
		for (Object[] a : data) {
			PlTlRDto dto = new PlTlRDto();
			dto.setJmlJaminan((BigDecimal)a[0]);
			dto.setCountJaminan((Long)a[1]);
			listDto.add(dto);
		}
		
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> findJmlJaminanBayarGLRS(String kodeKantor) {
		List<Object[]> data = plTlRsDao.findJmlJaminanBayarGLRS(kodeKantor);
		List<PlTlRDto> listDto = new ArrayList<>();
		for (Object[] a : data) {
			PlTlRDto dto = new PlTlRDto();
			if (a[0]==null) {
				dto.setJmlJaminan(new BigDecimal(0));

			}else {
				dto.setJmlJaminan((BigDecimal)a[0]);

			}
			dto.setCountJaminan((Long)a[1]);
			listDto.add(dto);
		}
		
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> findJmlJaminanHutangGLRS(String kodeKantor) {
		// TODO Auto-generated method stub
		List<Object[]> data = plTlRsDao.findJmlJaminanHutangGLRS(kodeKantor);
		List<PlTlRDto> listDto = new ArrayList<>();
		for (Object[] a : data) {
			PlTlRDto dto = new PlTlRDto();
			dto.setJmlJaminan((BigDecimal)a[0]);
			dto.setCountJaminan((Long)a[1]);
			listDto.add(dto);
		}
		
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> findJmlJaminanBatalGLRS(String kodeKantor) {
		List<Long> data = plTlRsDao.findJmlJaminanBatalGLRS(kodeKantor);
		List<PlTlRDto> listDto = new ArrayList<>();
		for (Long a : data) {
			PlTlRDto dto = new PlTlRDto();
			dto.setCountJaminan(a);
			listDto.add(dto);
		}
		
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;

	}

	@Override
	public Map<String, Object> plTlRsByIdJaminan(Map<String, Object> input,
			String idJaminan) {
		List<PlTlR> list = plTlRsDao.getPlTlRsByIdJaminan(idJaminan);
		List<PlTlRDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(list, PlTlRDto.class);
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;			
	}

	@Override
	public int savePlTlRs(PlTlRDto plTlRDto) {
		try {
			PlTlR plTlR = convertToModel(plTlRDto);
			plTlRsDao.save(plTlR);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}
	}


	@Override
	public Map<String, Object> getMaxNoSurat(Map<String,Object> mapInput,String noRegister) {
			List<String> list = plTlRsDao.getMaxNoSuratJaminan(noRegister);
			List<PlTlRDto> listDto = new ArrayList<>();
			for(String o : list){
				PlTlRDto dto = new PlTlRDto();
				dto.setNoSuratJaminan((String)o);
				listDto.add(dto);
			}
			
			Map<String, Object> map = new HashMap<>();
			map.put("totalRecords", new Long(1));
			map.put("contentData", listDto);
			return map;			
	}


	@Override
	public Map<String, Object> getSize(Map<String, Object> mapInput,
			String noRegister) {
		List<String> list = plTlRsDao.getSize(noRegister);
		List<PlTlRDto> listDto = new ArrayList<>();
		for(String o : list){
			PlTlRDto dto = new PlTlRDto();
			dto.setNoRegister((String)o);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;			
	}
}
