package core.service.monitoring.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.PlPengajuanSantunanDto;
import core.dao.PlPengajuanSantunanDao;
import core.dao.monitoring.MonitoringDao;
import core.service.monitoring.MonitoringPelimpahanSvc;

@Service
@Transactional
public class MonitoringPelimpahanSvcImpl implements MonitoringPelimpahanSvc{
	
	@Autowired
	MonitoringDao monitoringDao;

	@Override
	public Map<String, Object> getListMonitoring(Map<String, Object> input,
			String noBerkas, String namaKorban, String tglKejadian,
			String tglPenerimaan, String diajukanDi, String dilimpahkanKe,
			String search) {
		// TODO Auto-generated method stub
		List<Object[]> pelimpahan = monitoringDao.getListMonitoring(noBerkas, namaKorban, tglKejadian, 
				tglPenerimaan, diajukanDi, dilimpahkanKe, search);
		List<PlPengajuanSantunanDto> listDto= new ArrayList<>();
		
		for(Object[] a: pelimpahan){
			PlPengajuanSantunanDto dto= new PlPengajuanSantunanDto();
			dto.setNoBerkas((String) a[0]);
			dto.setDiajukanDi((String) a[2]);
			dto.setNamaKantorDiajukanDi((String) a[3]);
			dto.setDilimpahkanKe((String) a[4]);
			dto.setNamaKantorDilimpahkan((String) a[5]);
			dto.setJeda((BigDecimal) a[6]);
			dto.setNamaKorban((String) a[7]);
			dto.setTglPelimpahan((String) a[8]);
			dto.setTglTerjadi((String) a[9]);
			
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}
	

}	
			
				
			
			
			
