package core.service.monitoring.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.PlPengajuanSantunanDto;
import core.dao.monitoring.MonitoringDao;
import core.service.monitoring.MoLakaBelumTuntasSvc;


@Service
@Transactional
public class MoLakaBelumTuntasSvcImpl implements MoLakaBelumTuntasSvc {
	
	@Autowired
	MonitoringDao monitoringDao;

	@Override
	public Map<String, Object> getLakaBelumTuntas(Map<String, Object> input,
			String startDate, String endDate,  String tipePeriode, String jenisLaporan,
			String search) {
		// TODO Auto-generated method stub
		List<Object[]> lakaBelumTuntas = monitoringDao.getLakaBelumTuntas(startDate,endDate, tipePeriode, jenisLaporan, search);
		List<PlPengajuanSantunanDto> listDto= new ArrayList<>();
		
		for(Object[] a: lakaBelumTuntas){
			PlPengajuanSantunanDto dto= new PlPengajuanSantunanDto();
			dto.setTglKejadian((Date) a[0]);
			dto.setNamaKorban((String) a[1]);
			dto.setCideraDes((String) a[2]);
			dto.setDeskripsiInstansi((String) a[3]);
			dto.setKasusKecelakaan((String) a[4]);
			dto.setTglTindakLanjut((Date) a[5]);
			dto.setTindakLanjutDesc((String) a[6]);
			
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	
	}
	
	
