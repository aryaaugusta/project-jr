package core.service.monitoring;

import java.util.Map;

public interface MoCoklitDasiIrsmsSvc {
	
	
	public Map<String,Object> getListProvince(Map<String, Object> input, String search);
	public Map<String,Object> getListDistrictByProvinceId(Map<String, Object> input,String provinceId, String search);
	public Map<String,Object> getListIndex(Map<String, Object> input, String kodeInstansi, String startDate, String endDate);
	
	public Map<String,Object> getMappingPolda(Map<String, Object> input, String idDistrict);	
}
