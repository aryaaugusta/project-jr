package core.service.monitoring;

import java.util.Map;

import share.PlTlRDto;

public interface MoGLRSSvc {

	public Map<String, Object> findJmlJaminanGLRS(String kodeKantor);
	public Map<String, Object> findJmlJaminanBayarGLRS(String kodeKantor);
	public Map<String, Object> findJmlJaminanHutangGLRS(String kodeKantor);
	public Map<String, Object> findJmlJaminanBatalGLRS(String kodeKantor);

	public Map<String, Object> plTlRsByIdJaminan(Map<String, Object>input, String idJaminan);
	
	public int savePlTlRs(PlTlRDto plTlRDto);

	public Map<String, Object> getMaxNoSurat(Map<String,Object> mapInput,String noRegister);
	public Map<String, Object> getSize(Map<String,Object> mapInput,String noRegister);

}
