package core.service.monitoring;

import java.util.Map;

public interface MoDataLakaIrsmsSvc {
	
	public Map<String,Object> getListIndex(Map<String, Object> input, String instansi, String kejadianStartDate, String kejadianEndDate, 
			String laporanStartDate,String laporanEndDate, String namaKorban, String noLaporan, String jenisData);

	public Map<String,Object> findByIdKejadian(Map<String, Object> input, String idKejadian, String instansi);

}
