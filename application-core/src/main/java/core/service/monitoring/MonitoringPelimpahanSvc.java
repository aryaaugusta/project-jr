package core.service.monitoring;

import java.util.Map;

public interface MonitoringPelimpahanSvc {
	public Map<String,Object> getListMonitoring(Map<String, Object> input,
			String noBerkas, String namaKorban,String tglKejadian, String tglPenerimaan, 
			String diajukanDi,String dilimpahkanKe, String search);

}
