package core.service.monitoring;

import java.util.Map;

public interface MoLakaBelumTuntasSvc {
	public Map<String,Object> getLakaBelumTuntas(Map<String, Object> input,
			String startDate,String endDate, String tipePeriode,String jenisLaporan,String search);

}
