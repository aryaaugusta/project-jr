package core.service.monitoring;

import java.util.Map;

public interface MoDataLakaRsSvc {
	public Map<String,Object> getDataLakaRs(Map<String, Object> input,
			String namaKantor,
			String kodeRs,
			String startDate,
			String endDate,
			String pilihSumberData,
			String namaKorban,
			String statusResponse,
			String search);
	
	public Map<String, Object> getLakaRsbyId(Map<String, Object> input, String kodeRumahSakit, String kodeKejadian);
}
