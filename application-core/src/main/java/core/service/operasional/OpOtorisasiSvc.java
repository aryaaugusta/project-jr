package core.service.operasional;

import java.util.Map;

public interface OpOtorisasiSvc {

	public Map<String, Object>  getDataList(Map<String, Object> input,String pilihPengajuan,String tglPenerimaan, String noBerkas, String search);
	
}
