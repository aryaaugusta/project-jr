package core.service.operasional;

import java.util.List;
import java.util.Map;

import share.PlAngkutanKecelakaanDto;
import share.PlDataKecelakaanDto;
import share.PlKorbanKecelakaanDto;
import share.PlNihilKecelakaanDto;
import core.model.PlDataKecelakaan;

public interface OpPlDataKecelakaanSvc {

	public Map<String, Object> getDataList(Map<String, Object> input, 
			String kejadianStartDate,
			String kejadianEndDate,
			String asalBerkas,
			String samsat, 
			String laporanStartDate,
			String laporanEndDate,
			String instansi,
			String instansiPembuat,
			String noLaporan,
			String lokasi,
			String namaKorban,
			String noIdentitas,
			String lingkupJaminan,
			String jenisPertanggungan,
			String sifatCidera,
			String sifatKecelakaan,
			String kecelakaanKatostrop,
			String perusahaanPenerbangan,
			String perusahaanOtobus, 
			String search);
	
	public Map<String, Object>  getDataByIdKecelakaan(Map<String, Object> input,String idKecelakaan);
	public Map<String, Object> getAngkutanByIdLaka(Map<String, Object> input, String idKecelakaan, String search);
	public Map<String, Object> getKorbanByIdLaka(Map<String, Object> input, String idKecelakaan, String search);

	public Map<String, Object> getAngkutanByIdAngkutan(Map<String, Object> input, String idAngkutan);
	public Map<String, Object> getKorbanByIdKorban(Map<String, Object> input, String idKorban);

	public int saveDataLaka(PlDataKecelakaanDto plDataKecelakaanDto);
	public int saveAngkutanKecelakaan(PlAngkutanKecelakaanDto plAngkutanKecelakaanDto);
	public int saveKorban(PlKorbanKecelakaanDto plKorbanKecelakaanDto);
	public int saveNihilKecelakaan(PlNihilKecelakaanDto plNihilKecelakaanDto);

	public int deleteAngkutan(PlAngkutanKecelakaanDto plAngkutanKecelakaanDto);
	public int deleteKorban(PlKorbanKecelakaanDto plKorbanKecelakaanDto);
	public int deleteKorbanByIdLaka(PlDataKecelakaanDto plDataKecelakaanDto);
	
	public Map<String, Object>  getKodeNoLaporan(Map<String, Object> input,String kodeInstansi);
	public Map<String, Object>  cekData(Map<String, Object> input,String tglKejadian, String tglLaporan, String kodeInstansi);
	
	public Map<String, Object> getDataForPrint(Map<String, Object> map);
	public Map<String, Object> reportDataLaka1(Map<String, Object> input, String idKec);
	public Map<String, Object> reportDataLaka2(Map<String, Object> input, String idKec);
	public Map<String, Object> reportDataLaka3(Map<String, Object> input, String idKec);
	
}
