package core.service.operasional;

import java.util.List;
import java.util.Map;

import share.PlAdditionalDescDto;
import share.PlBerkasPengajuanDto;
import share.PlDisposisiDto;
import share.PlPengajuanRsDto;
import share.PlPengajuanSantunanDto;


public interface OpPlPengajuanSantunanSvc {
	public Map<String, Object> getDataList(Map<String, Object> input, 
			String diajukanDi, 
			String dilimpahkanKe, 
			String namaKorban, 
			String pengajuanDay, 
			String pengajuanMonth, 
			String pengajuanYear,  
			String lakaDay, 
			String lakaMonth, 
			String lakaYear, 
			String kodeInstansi,
			String noLaporan, 
			String noBerkas, 
			String statusProses, 
			String penyelesaian, 
			String namaPemohon, 
			String kodeRs, 
			String search);
	
	public abstract Map<String,Object> findById(Map<String, Object> input,String idKecelakaan);

	public Map<String,Object> findByNoBerkas(Map<String, Object> input,String noBerkas);
	public Map<String,Object> findPengajuanRsByNoBerkas(Map<String, Object> input,String noBerkas);
	public Map<String,Object> findBerkasPengajuanByNoBerkas(Map<String, Object> input,String noBerkas);

	public Map<String,Object> getListRs(Map<String,Object> input);

	public int saveSantunan(PlPengajuanSantunanDto plPengajuanSantunanDDto);
	public int saveSantunanOnly(PlPengajuanSantunanDto plPengajuanSantunanDto);
	public int saveAdditional(PlAdditionalDescDto plAdditionalDescDto);
	public int savePengajuanRs(PlPengajuanRsDto plPengajuanRsDto);
//	public int save(PlPengajuanSantunanDto plPengajuanSantunanDDto);
	public int saveDisposisi(PlDisposisiDto plDisposisiDto);
	public int saveBerkasPengajuan(List<PlBerkasPengajuanDto> plBerkasPengajuanDtos);
	public int deleteBerkasPengajuan(List<PlBerkasPengajuanDto> plBerkasPengajuanDtos);
	
	public Map<String,Object> findByNoPengajuan(Map<String, Object> input, String noPengajuan);
	public Map<String, Object> getListDisposisiById(Map<String, Object> input, String noBerkas);
	public Map<String,Object> getDataPengajuanByNoBerkas(Map<String, Object> input,String noBerkas);
	public Map<String, Object> findOnePenyelesaian(Map<String, Object> input,String noBerkas);
	public Map<String, Object> findAdditionalDescByNoBerkas(Map<String, Object> input,String noBerkas);

	//added by luthfi
	public int updateSantunanOtorisasi(PlPengajuanSantunanDto plPengajuanSantunanDto);
	public int updateStatusProses(PlPengajuanSantunanDto plPengajuanSantunanDto);
	public Map<String,Object> getSantunanByIdKorban(Map<String, Object> input, String idKorban);
	public Map<String,Object> getIndexHapusPengajuan(Map<String, Object> input, String noBerkas);
	public int deletePengajuan(PlPengajuanSantunanDto plPengajuanSantunanDto);

	int saveSantunanNoTrigger(PlPengajuanSantunanDto dto);

}
