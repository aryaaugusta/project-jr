package core.service.operasional;

import java.util.Map;

import share.PlRegisterSementaraDto;
import share.PlRequestPerubahanDto;

public interface PermintaanPerubahanDataPengajuanSvc {
	public Map<String,Object> getListIndexPPDataPengajuan(Map<String, Object> input,
			String noBerkas, String status, String search);
	public Map<String,Object> findByNoBerkas(Map<String, Object> input,String noBerkas);
	public int savePermintaan(PlRequestPerubahanDto plRequestPerubahanDto);
	public int deletePermintaan(PlRequestPerubahanDto plRequestPerubahanDto);

}
