package core.service.operasional;

import java.util.Date;
import java.util.Map;

import org.springframework.data.repository.query.Param;

import share.PlRegisterSementaraDto;
import share.PlTindakLanjutDto;
import share.PlTlRDto;

public interface OpRegisterSementaraSvc {
	
	public abstract Map<String, Object> loadJRRefCode(String rvDomain,String flag,String search);
	
	public abstract Map<String, Object> loadRegisterSementara(
			String kodeKantorJr,
			String regDay,
			String regMonth,
			String regYear,
			String namaKorban,
			String lakaDay,
			String lakaMonth,
			String lakaYear,
			String noLaporan,
			String noRegister,
			String asalBerkasFlag,
			String tindakLanjutFlag,
			String cideraKorban,
			String namaInstansi,
			String rvDomainCidera,
			String rvDomainTindakLanjut,
			String search);
	
	public abstract Map<String,Object> loadDataLaka(String kejadianStartDate,
			String kejadianEndDate,
			String namaKantorInstansi,
			String laporStartDate,
			String laporEndDate,
			String noLaporPolisi,
			String namaKorbanLaka,
			String kodeJaminan,
			String statusLP,
			String search,
			String idKorban,
			String idKecelakaan,
			String tipe);
		
	public Map<String, Object> saveRegister(PlRegisterSementaraDto dto);
	public int saveRegisterPS(PlRegisterSementaraDto dto);
	public abstract Map<String,Object> loadTindakLanjut(String noRegister);
	public int saveTindakLanjut(PlTindakLanjutDto dto);
	public int deleteTindakLanjut(String idTLReg);
	public Map<String, Object> saveTLforRS(PlRegisterSementaraDto dto);
	public Map<String, Object> findOneRegister(Map<String, Object> input, String noReg);
	
	public int saveRegister2(PlRegisterSementaraDto dto);
	
	public int saveRegisterSementara(PlRegisterSementaraDto dto);
	
	//added by luthfi
	public Map<String, Object> findRegisterByIdKorban(Map<String, Object> input, String idKecelakaan);
	public Map<String, Object> findRegisterByNoRegister(Map<String, Object> input, String noReg);
	public Map<String, Object> findTindakLanjutByNoRegister(Map<String, Object> input, String noReg);
	public Map<String, Object> findTindakLanjutByIdTl(Map<String, Object> input, String idTl);

	public int deleteRegister(PlRegisterSementaraDto plRegisterSementaraDto);
	
	public Map<String, Object> findIndexGLRS(String kodeKantor,String jenisTgl,String tglAwal,String tglAkhir,String kodeRS,String flagBayar);
	
	public Map<String, Object> plTlRsByNoSuratJaminan(Map<String, Object> input, String noSuratJaminan);
	public Map<String, Object> plTlRsByNoRegister(Map<String, Object> input, String noRegister);
	
	public Map<String, Object> getIndexGLRS(String kodeKantor,String jenisTgl,String tglAwal,String tglAkhir,String kodeRS,String flagBayar);
	
	public Map<String, Object> dataCetakTl1(Map<String, Object> input, String noRegister);

	
}
