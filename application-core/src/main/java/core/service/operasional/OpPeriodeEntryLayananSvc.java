package core.service.operasional;

import java.util.Map;

public interface OpPeriodeEntryLayananSvc {

	public Map<String, Object> getDataList(Map<String, Object> input, String kodeKantorJr,
			String periodeBulan, String periodeTahun);

	public Map<String, Object> general(Map<String, Object> input);

	public void updateTable();
}
