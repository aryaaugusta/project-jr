package core.service.operasional.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.util.CommonConstants;
import share.PlPengajuanSantunanDto;
import share.PlRequestPerubahanDto;
import core.dao.PlRequestPerubahanDao;
import core.model.PlPengajuanSantunan;
import core.model.PlRegisterSementara;
import core.model.PlRequestPerubahan;
import core.service.operasional.PermintaanPerubahanDataPengajuanSvc;

@Service
@Transactional
public class PermintaanPerubahanDataPengajuanSvcImpl implements
		PermintaanPerubahanDataPengajuanSvc {

	@Autowired
	PlRequestPerubahanDao plRequestPerubahanDao;

	@Autowired
	MapperFacade mapperFacade;

	private PlRequestPerubahan convertToModel(PlRequestPerubahanDto obj) {
		return mapperFacade.map(obj, PlRequestPerubahan.class);
	}

	@Override
	public Map<String, Object> getListIndexPPDataPengajuan(
			Map<String, Object> input, String noBerkas, String status, String search) {
		List<Object[]> ppDataPengajuan = plRequestPerubahanDao
				.getListPPDataPengajuan(noBerkas, status, search);
		List<PlRequestPerubahanDto> listDto = new ArrayList<>();

		for (Object[] a : ppDataPengajuan) {
			PlRequestPerubahanDto dto = new PlRequestPerubahanDto();
			dto.setNamaKantor((String) a[0]);
			dto.setNoBerkas((String) a[1]);
			dto.setNamaKorban((String) a[2]);
			dto.setCreatedDate((Date) a[3]);
			dto.setStatusRequest((String) a[4]);
			dto.setIdRequest((String) a[5]);
			if((Date)a[6]!=null){
				dto.setVisibleAlert(true);
			}else if((Date)a[6]==null){
				dto.setVisibleAlert(false);
			}
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}
	
	@Override
	public Map<String, Object> findByNoBerkas(Map<String, Object> input,
			String noBerkas) {
		List<Object[]> findData = plRequestPerubahanDao.findByNoBerkas(noBerkas);
		List<PlRequestPerubahanDto> listDto = new ArrayList<>();

		for (Object[] a : findData) {
			PlRequestPerubahanDto dto = new PlRequestPerubahanDto();
			dto.setCreatedBy((String) a[0]);
			dto.setDescription((String) a[1]);
			dto.setCreatedByDesc((String) a[0] + "-"+ (String) a[1]);
			dto.setKodeKantorJr((String) a[2]);
			dto.setNamaKantor((String) a[3]);
			dto.setKodeNamaKantor((String) a[2] + "-" + (String) a[3]);
			dto.setNoBerkas((String) a[4]);
			dto.setCreatedDate((Date) a[5]);
			dto.setKeterangan((String) a[6]);
			dto.setUpdatedBy((String) a[7]);
			dto.setTglUpdate((Date) a[8]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public int savePermintaan(PlRequestPerubahanDto plRequestPerubahanDto) {
		try {
			PlRequestPerubahan plRequestPerubahan = convertToModel(plRequestPerubahanDto);
			plRequestPerubahanDao.save(plRequestPerubahan);

			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int deletePermintaan(PlRequestPerubahanDto plRequestPerubahanDto) {
		try {
			PlRequestPerubahan plRequestPerubahan = new PlRequestPerubahan();
			plRequestPerubahan.setIdRequest(plRequestPerubahanDto.getIdRequest());
			plRequestPerubahanDao.deletePermintaan(plRequestPerubahan);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;						
		}		
	}
	
	
	}

	


