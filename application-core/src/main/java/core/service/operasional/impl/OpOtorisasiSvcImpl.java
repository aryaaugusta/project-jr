package core.service.operasional.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.PlPengajuanSantunanDto;
import core.dao.PlPengajuanSantunanDao;
import core.service.operasional.OpOtorisasiSvc;

@Service
@Transactional
public class OpOtorisasiSvcImpl implements OpOtorisasiSvc {

	@Autowired
	PlPengajuanSantunanDao plPengajuanSantunanDao;
	
	@Override
	public Map<String, Object> getDataList(Map<String, Object> input, String pilihPengajuan,
			String tglPenerimaan, String noBerkas, String search) {

		List<Object[]> otorisasi = plPengajuanSantunanDao.getIndexOtorisasi(pilihPengajuan,tglPenerimaan, noBerkas, search);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		
		for(Object[] a : otorisasi){
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setIdKecelakaan((String)a[0]);
			dto.setNoBerkas((String)a[1]);
			dto.setStatusProsesDesc((String)a[2]);
			dto.setTglPenerimaan((Date)a[3]);
			dto.setNama((String)a[4]);
			dto.setAlamatKorban((String)a[5]);
			dto.setStatusProses((String)a[7]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

}
