package core.service.operasional.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.util.CommonConstants;
import share.PlBerkasEcmHDto;
import core.dao.PlBerkasEcmsHDao;
import core.model.PlBerkasEcmsContain;
import core.model.PlBerkasEcmsH;
import core.service.operasional.PlBerkasEcmHSvc;

@Service
@Transactional
public class PlBerkasEcmHSvcImpl implements PlBerkasEcmHSvc {

	@Autowired
	PlBerkasEcmsHDao plBerkasEcmHDao;
	
	@Override
	public int saveBerkasEcmH(PlBerkasEcmHDto plBerkasEcmsHDto) {
		try {
			PlBerkasEcmsH plBerkasEcmsH = new PlBerkasEcmsH();
			plBerkasEcmsH.setNoRegEcms(plBerkasEcmsHDto.getNoRegEcms());
			plBerkasEcmsH.setIdKorbanKecelakaan(plBerkasEcmsHDto.getIdKorbanKecelakaan());
			plBerkasEcmHDao.save(plBerkasEcmsH);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

}
