package core.service.operasional.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.util.CommonConstants;
import core.dao.PlBerkasEcmDao;
import core.model.PlBerkasEcm;
import core.service.operasional.OpPlBerkasEcmSvc;
import share.PlBerkasEcmDto;

/*
 * @author: Tri
 * 
 * */
@Service
@Transactional
public class OpPlBerkasEcmSvcImpl implements OpPlBerkasEcmSvc{

	@Autowired
	PlBerkasEcmDao plBerkasEcmDao;
	
	@Override
	public int saveData(PlBerkasEcmDto plBerkasEcmDto) {
		try {
			PlBerkasEcm plBerkasEcm = new PlBerkasEcm();
			plBerkasEcm.setNoRegEcms(plBerkasEcmDto.getNoRegEcms());
			plBerkasEcm.setNamafile(plBerkasEcmDto.getNamafile());
			plBerkasEcm.setCreatedBy(plBerkasEcmDto.getCreatedBy());
			plBerkasEcm.setCreatedDate(plBerkasEcmDto.getCreatedDate());
			plBerkasEcm.setDdocname(plBerkasEcmDto.getDdocname());
			plBerkasEcm.setFilepath(plBerkasEcmDto.getFilepath());
			plBerkasEcmDao.save(plBerkasEcm);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

}
