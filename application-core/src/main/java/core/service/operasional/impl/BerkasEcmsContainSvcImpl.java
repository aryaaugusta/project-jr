package core.service.operasional.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.util.CommonConstants;

import share.PlBerkasEcmsContainDto;
import core.dao.PlBerkasEcmsContainsDao;
import core.model.PlBerkasEcmsContain;
import core.service.operasional.BerkasEcmsContainSvc;

@Service
@Transactional
public class BerkasEcmsContainSvcImpl implements BerkasEcmsContainSvc{

	@Autowired
	PlBerkasEcmsContainsDao plBerkasEcmsContainDao;
	
	@Override
	public int saveBerkasEcmsContain(PlBerkasEcmsContainDto plBerkasEcmsContainDto) {
		// TODO Auto-generated method stub
		try {
			PlBerkasEcmsContain plBerkasEcmsContain = new PlBerkasEcmsContain();
			plBerkasEcmsContain.setNoRegEcms(plBerkasEcmsContainDto.getNoRegEcms());
			plBerkasEcmsContain.setDdocname(plBerkasEcmsContainDto.getdDocname());
			plBerkasEcmsContain.setKodeBerkas(plBerkasEcmsContainDto.getKodeBerkas());
			plBerkasEcmsContainDao.save(plBerkasEcmsContain);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

}
