package core.service.operasional.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.util.CommonConstants;
import common.util.JsonUtil;
import share.DukcapilWinDto;
import share.KKDukcapilDto;
import core.dao.DukcapilWinDao;
import core.model.DukcapilRefCode;
import core.model.DukcapilWn;
import core.model.DukcapilWnFam;
import core.service.operasional.OpNikKKDukcapilSvc;

@Service
@Transactional
public class OpNikKKDukcapilSvcImpl implements OpNikKKDukcapilSvc {

	@Autowired
	DukcapilWinDao dukcapilWinDao;
	
	@Autowired
	MapperFacade mapper;
	
	@Override
	public Map<String, Object> general(Map<String, Object> input) {
		String option = "";
		Map<String, Object> out = new HashMap<>();
		try {
			option = (String) input.get("option");
			if(option != null && option.equalsIgnoreCase("save")){
				out = save(input);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return out;
	}

	private Map<String, Object> save(Map<String, Object> input){
		
		List<KKDukcapilDto> kks = new ArrayList<>();
		List<DukcapilWinDto> dukWin = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		int status = 0;
		String message = "";
		List<DukcapilRefCode> listDuk = dukcapilWinDao.getDukcapil();
		System.out.println("lust ref code : \n"+ JsonUtil.getJson(listDuk));
		try{
			int x = 0;
			kks = JsonUtil.mapJsonToListObject(input.get("kk"), KKDukcapilDto.class);
			for(KKDukcapilDto kk : kks){
				String hubKel = "";
				for(DukcapilRefCode ref  :listDuk){
					if(kk.getsTATHBKEL().equalsIgnoreCase(ref.getRvMeaning())){
						hubKel = ref.getRfLowValue();;
					}
				}
				
				DukcapilWinDto dto = new DukcapilWinDto(kk);
				dukWin.add(dto);
				DukcapilWn model = mapper.map(dto, DukcapilWn.class);
				System.out.println("DTO : \n"+JsonUtil.getJson(dto));
				System.out.println("MODEL : \n"+JsonUtil.getJson(model));
				try{
					System.out.println("NIK : "+model.getNik());
					dukcapilWinDao.saveDukcapil(model);
					DukcapilWnFam fam = new DukcapilWnFam();
					try{
						fam.setNik(model.getNik());
						fam.setNoKk(model.getNoKk());
						System.out.println("STATUS KELUARGA : "+kk.getsTATHBKEL());
						fam.setKodeHubKel(hubKel);
						fam.setLastSyncDate(new Date());
						dukcapilWinDao.saveDukcapilFam(fam);
					}catch(Exception s){
						s.printStackTrace();
					}
					
					x = x+1;
					message = "OK";
				}catch(Exception s){
					s.printStackTrace();
					message = "ERROR";
				}
			}
			if(x>0){
				status = x;
			}else{
				message = "ERROR";
				status = CommonConstants.ERROR_REST_STATUS;
			}
		}catch(Exception s){
			s.printStackTrace();
			status = CommonConstants.ERROR_REST_STATUS;
		}
		out.put("status", status);
		out.put("message", message);
		out.put("total", 0);
		System.out.println("==============\nOUT : "+JsonUtil.getJson(out));
		
		
		return out;
	}
}
