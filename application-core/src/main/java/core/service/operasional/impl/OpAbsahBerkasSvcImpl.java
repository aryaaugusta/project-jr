package core.service.operasional.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.model.UserSessionJR;
import common.util.CommonConstants;
import common.util.JsonUtil;
import share.DasiJrRefCodeDto;
import share.PlBerkasPengajuanDto;
import share.PlKorbanKecelakaanDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import core.dao.PlBerkasPengajuanDao;
import core.dao.PlPengajuanSantunanDao;
import core.dao.PlPenyelesaianSantunanCustomDao;
import core.dao.PlPenyelesaianSantunanDao;
import core.dao.lov.LovDao;
import core.model.DasiJrRefCode;
import core.model.PlBerkasPengajuan;
import core.model.PlBerkasPengajuanPK;
import core.model.PlKorbanKecelakaan;
import core.model.PlPengajuanSantunan;
import core.model.PlPenyelesaianSantunan;
import core.service.operasional.OpAbsahBerkasSvc;

@Service
@Transactional
public class OpAbsahBerkasSvcImpl implements OpAbsahBerkasSvc {

	@Autowired
	PlPengajuanSantunanDao plPengajuanSantunanDao;
	
	@Autowired
	PlBerkasPengajuanDao plBerkasPengajuanDao;
	
	@Autowired
	PlPenyelesaianSantunanDao  plPenyelesaianSantunanDao;
	
	@Autowired
	PlPenyelesaianSantunanCustomDao plPenyelesaianSantunanCustomDao;
	
	@Autowired
	LovDao lovDao;
	
	@Autowired
	MapperFacade mapperFacade;
	
	@Override
	public Map<String, Object> getListAbsah(Map<String, Object> input) {
		// TODO Auto-generated method stub
		
		String tglPenerimaan = (String) input.get("tglDiterima");
		String noBerkas = (String) input.get("noBerkas");
		String statusProses = (String) input.get("option");
		String filter = (String) input.get("filter");
		
		String nomBerkas = "";
		if(noBerkas!=null){
			nomBerkas = noBerkas.replaceAll("-", "");
			nomBerkas = nomBerkas.replaceAll("_", "");
		}
		
		if (nomBerkas != null){
			if (!nomBerkas.equalsIgnoreCase("") || 
					!nomBerkas.isEmpty() || 
					!nomBerkas.equalsIgnoreCase("ALL") || 
					nomBerkas!="ALL" || nomBerkas!=""){
				noBerkas = "ALL";
			}
		}
		
		List<Object[]> list = new ArrayList<>();
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		Map<String, Object> out = new HashMap<>();
		String status="";
		try{
			list = plPengajuanSantunanDao.getListAbsah(noBerkas, tglPenerimaan, statusProses, filter);
			for(Object[] o:list){
				PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
				dto.setNoBerkas((String) o[0]);
				dto.setStatusProsesMeaning((String) o[1]);
				dto.setTglPenerimaan((Date) o[2]);
				dto.setNamaKorban((String) o[3]);
				dto.setAlamatKorban((String) o[4]);
				dto.setIdKorbanKecelakaan((String) o[5]);
				dto.setStatusProses((String) o[6]);
				listDto.add(dto);
			}
			status = CommonConstants.OK_REST_STATUS+"";
		}catch (Exception s){
			s.printStackTrace();
			status=CommonConstants.ERROR_REST_STATUS+"";
		}
		if(listDto!=null && listDto.size()>0){
			out.put("total",listDto.size());
		}else{
			out.put("total", 0);
		}
		
		out.put("status", status);
		out.put("listDto", listDto);
		
		return out;
	}

	@Override
	public Map<String, Object> callGeneral(Map<String, Object> input) {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<>();
		String option = (String) input.get("option");
		
		if(option.equalsIgnoreCase("getPegajuanByNoBerkas")){
			map = getPengajuanByNoBerkas(input);
		}
		
		if(option.equalsIgnoreCase("getPengajuanByIdKec")){
			map = getPengajuanByIdKec(input);
		}
		
		if(option.equalsIgnoreCase("nextStep")){
			map = processToOtorisasi(input);
		}
		
		if(option.equalsIgnoreCase("disposisi")){
			map = getDisposisi(input);
		}
		if(option.equalsIgnoreCase("save")){
			map = saveData(input);
		}
		
		return map;
	}

	private Map<String, Object> getDisposisi(Map<String, Object> input){
		Map<String, Object> out = new HashMap<>();
		int status = 100;
		List<DasiJrRefCodeDto> listRef = new ArrayList<>();
		int proses = 4;
		try{
			UserSessionJR userSess = (UserSessionJR) JsonUtil.mapJsonToSingleObject(input.get("userSess"), UserSessionJR.class);
			List<DasiJrRefCode> listRefM = new ArrayList<>();
			listRefM = lovDao.getRefByRvDomainRvHigh("KODE DISPOSISI", userSess.getLevelKantor());
			System.out.println("================================================================================");
			System.out.println(JsonUtil.getJson(listRefM));
			List<DasiJrRefCode> lRef = new ArrayList<>();
			DasiJrRefCode disp = new DasiJrRefCode();
			for(DasiJrRefCode d : listRefM){
				if(d.getOrderSeq()!=null){
					if(d.getOrderSeq().compareTo(new BigDecimal(proses))==0){
						lRef.add(d);
						disp = d;
					}
				}
			}
			listRef = mapperFacade.mapAsList(listRefM, DasiJrRefCodeDto.class);
		}catch(Exception s){
			status = CommonConstants.ERROR_REST_STATUS;
			s.printStackTrace();
		}
		out.put("status", status);
		out.put("disposisi", listRef);
		out.put("total", 0);
		return out;
	}
	
	private Map<String, Object> generic(Map<String, Object> input){
		Map<String, Object> out = new HashMap<>();
		int status = 100;
		try{
		}catch(Exception s){
			status = CommonConstants.ERROR_REST_STATUS;
			s.printStackTrace();
		}
		out.put("status", status);
		out.put("OK", "OK");
		out.put("total", 0);
		return out;
	}
	
	private Map<String, Object> processToOtorisasi(Map<String, Object> input){
		
		PlPengajuanSantunanDto pengajuan = new PlPengajuanSantunanDto();
		UserSessionJR userSess = new UserSessionJR();
		try {
			pengajuan = JsonUtil.mapJsonToSingleObject(input.get("plPengajuanSantunanDto"), PlPengajuanSantunanDto.class);
			userSess = JsonUtil.mapJsonToSingleObject(input.get("userSess"), UserSessionJR.class);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String, Object> out = new HashMap<>();
		
		
		int status = 100;
		try{
			
			PlPengajuanSantunan model = plPengajuanSantunanDao.findOneByNoBerkas(pengajuan.getNoBerkas());
			model.setJumlahPengajuanLukaluka(pengajuan.getJumlahPengajuanLukaluka());
			model.setJumlahPengajuanMeninggal(pengajuan.getJumlahPengajuanMeninggal());
			model.setJumlahPengajuanPenguburan(pengajuan.getJumlahPengajuanPenguburan());
			model.setJmlPengajuanAmbl(pengajuan.getJmlPengajuanAmbl());
			model.setJmlPengajuanP3k(pengajuan.getJmlPengajuanP3k());
			model.setAbsahFlag("Y");
			model.setStatusProses("BA");
			model.setLastUpdatedBy(userSess.getLoginID());
			model.setLastUpdatedDate(new Date());
			System.out.println("processToOtorisasi");
			System.out.println(JsonUtil.getJson(model));
			plPengajuanSantunanDao.save(model);
			
			
			
			status = CommonConstants.OK_REST_STATUS;
		}catch(Exception s){
			status = CommonConstants.ERROR_REST_STATUS;
			s.printStackTrace();
		}
		out.put("status", status);
		out.put("OK", "OK");
		out.put("total", 0L);
		return out;
	}
	
	private Map<String, Object> getPengajuanByIdKec(Map<String, Object> input){
		String idKec = (String) input.get("idKec");
		Map<String, Object> out = new HashMap<>();
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		int status = 100;
		try{
			List<String> listNoBerkas = new ArrayList<>();
			List<Object[]> listObject = new ArrayList<>();
			listNoBerkas = plPengajuanSantunanDao.getPengajuanByIdKec(idKec);
			listObject = plPengajuanSantunanDao.getListPengajuanByNoBerkas(listNoBerkas);
			
			for(Object[] o : listObject){
				PlPengajuanSantunan model = (PlPengajuanSantunan) o[0];
				PlPengajuanSantunanDto dto = mapperFacade.map(model, PlPengajuanSantunanDto.class);
				PlKorbanKecelakaan korban = (PlKorbanKecelakaan) o[1];
				DasiJrRefCode ref = (DasiJrRefCode) o[2];
				dto.setStatusProsesMeaning(ref.getRvMeaning());
				dto.setNamaKorban(korban.getNama());
				listDto.add(dto);
			}
			status = CommonConstants.OK_REST_STATUS;
		}catch(Exception s){
			status = CommonConstants.ERROR_REST_STATUS;
			s.printStackTrace();
		}
		out.put("status", status);
		out.put("PlPengajuanSantunanDtos", listDto);
		out.put("total", listDto.size());
		return out;
	}
	
	private Map<String, Object> getPengajuanByNoBerkas(Map<String, Object> input){
		
		String noBerkas = (String) input.get("noBerkas");
		Map<String, Object> out = new HashMap<>();
		
		List<PlPengajuanSantunan> lObp = new ArrayList<>();
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		List<Object[]> listObject = new ArrayList<>();
		int status = 100;
		try{
//			lObp = plPengajuanSantunanDao.findByNoBerkas(noBerkas);
			listObject = plPengajuanSantunanDao.getPengajuanByNoBerkas(noBerkas);
			for (Object[] o : listObject){
				PlPengajuanSantunan model = (PlPengajuanSantunan) o[0];
				PlPengajuanSantunanDto dto = mapperFacade.map(model, PlPengajuanSantunanDto.class);
				PlKorbanKecelakaan korban = (PlKorbanKecelakaan) o[1];
				if(o[2]!=null){
					DasiJrRefCode ref = (DasiJrRefCode) o[2];
					dto.setStatusProsesMeaning(ref.getRvMeaning());
				}else{
					List<DasiJrRefCode> listRef = lovDao.getRef(model.getStatusProses(), "KODE STATUS PROSES");
					List<DasiJrRefCode> listRefs = lovDao.getAllProses();
				}
				dto.setNamaKorban(korban.getNama());
				listDto.add(dto);
			}
			status = CommonConstants.OK_REST_STATUS;
		}catch(Exception s){
			status = CommonConstants.ERROR_REST_STATUS;
			s.printStackTrace();
		}
		
		out.put("status", status);
		out.put("PlPengajuanSantunanDtos", listDto);
		out.put("total", listDto.size());
		return out;
	}

	@Override
	public Map<String, Object> getDataList(Map<String, Object> input,
			String pilihPengajuan, String tglPenerimaan, String noBerkas,
			String search) {
		
		List<Object[]> otorisasi = plPengajuanSantunanDao.getIndexIdentifikasi(pilihPengajuan,tglPenerimaan, noBerkas, search);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		System.err.println("luthfi91 "+pilihPengajuan);
		
		for(Object[] a : otorisasi){
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setIdKecelakaan((String)a[0]);
			dto.setNoBerkas((String)a[1]);
			dto.setStatusProsesDesc((String)a[2]);
			dto.setTglPenerimaan((Date)a[3]);
			dto.setNama((String)a[4]);
			dto.setAlamatKorban((String)a[5]);
			dto.setStatusProses((String)a[6]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;

	}
	
	private Map<String, Object> saveData(Map<String, Object> input){
		
		List<PlBerkasPengajuanDto> berkas = new ArrayList<>();
		PlPenyelesaianSantunanDto penyelesaianSantunanDto = new PlPenyelesaianSantunanDto();
		PlPenyelesaianSantunan penyelesaianSantunan = new PlPenyelesaianSantunan();
		int status = 100;
		try {
			berkas = JsonUtil.mapJsonToListObject(input.get("berkasPengajuanDtos"), PlBerkasPengajuanDto.class);
			penyelesaianSantunanDto = JsonUtil.mapJsonToSingleObject(input.get("penyelesaianSantunanDto"), PlPenyelesaianSantunanDto.class);
			System.err.println("luthfi11 1 "+penyelesaianSantunanDto.getNoBerkas());
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("luthfi11 2 ");
		}

		try{
			for(PlBerkasPengajuanDto a : berkas){
				
				PlBerkasPengajuan b = new PlBerkasPengajuan(); 
				b = mapperFacade.map(a, PlBerkasPengajuan.class);
				System.out.println(JsonUtil.getJson(b));
				plBerkasPengajuanDao.save(b);
				// plBerkasPengajuanDao.updateBerkas(b);
				PlBerkasPengajuan c = plBerkasPengajuanDao.findBerkas(b.getKodeBerkas(), b.getNoBerkas());
			}
			
			penyelesaianSantunan.setNoBerkas(penyelesaianSantunanDto.getNoBerkas());
			penyelesaianSantunan.setTglProses(penyelesaianSantunanDto.getTglProses());
			penyelesaianSantunan.setJumlahDibayarMeninggal(penyelesaianSantunanDto.getJumlahDibayarMeninggal());
			penyelesaianSantunan.setJumlahDibayarLukaluka(penyelesaianSantunanDto.getJumlahDibayarLukaluka());
			penyelesaianSantunan.setCreatedBy(penyelesaianSantunanDto.getCreatedBy());
			penyelesaianSantunan.setCreationDate(penyelesaianSantunanDto.getCreationDate());
			penyelesaianSantunan.setJmlByrAmbl(penyelesaianSantunanDto.getJmlByrAmbl());
			penyelesaianSantunan.setJmlByrP3k(penyelesaianSantunanDto.getJmlByrP3k());
			System.err.println("process save luthfi");
			System.out.println(JsonUtil.getJson(penyelesaianSantunan));
			plPenyelesaianSantunanDao.save(penyelesaianSantunan);
			status = CommonConstants.OK_REST_STATUS;
			System.err.println("luthfi11 3 "+penyelesaianSantunanDto.getNoBerkas());
		}catch(Exception e){
			status = CommonConstants.ERROR_REST_STATUS;
			e.printStackTrace();
			System.err.println("luthfi11 4 "+penyelesaianSantunanDto.getNoBerkas());

		}
		
		Map<String, Object> out = new HashMap<>();
		out.put("status", status);
		out.put("ok", "OK");
		out.put("total", 0L);
		return out;
		
	}
	
}
