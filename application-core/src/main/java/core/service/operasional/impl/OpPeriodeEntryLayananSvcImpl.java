package core.service.operasional.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.util.CommonConstants;

import share.PlPeriodeLockDto;
import core.dao.PlPeriodeLockDao;
import core.model.PlPeriodeLock;
import core.service.operasional.OpPeriodeEntryLayananSvc;

@Service
@Transactional
public class OpPeriodeEntryLayananSvcImpl implements OpPeriodeEntryLayananSvc{

	@Autowired
	PlPeriodeLockDao plPeriodeLockDao;
	
	@Autowired
	MapperFacade mapperFacade;

	
	
	@Override
	public Map<String, Object> getDataList(Map<String, Object> input,  String kodeKantorJr,
			String periodeBulan, String periodeTahun) {
		
		List<PlPeriodeLock> periodeLock = plPeriodeLockDao.getListIndex(kodeKantorJr, periodeBulan, periodeTahun);
		List<PlPeriodeLockDto> listDto = new ArrayList<>();
		
		
		for(PlPeriodeLock a : periodeLock){
			PlPeriodeLockDto dto = new PlPeriodeLockDto();
			String bulan = null;
			dto.setKodeKantorJr(a.getKodeKantorJr());
			if(a.getPeriodeBulan().equalsIgnoreCase("01")||a.getPeriodeBulan().equalsIgnoreCase("1")){
				bulan = "Januari";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("02")||a.getPeriodeBulan().equalsIgnoreCase("2")){
				bulan = "Februari";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("03")||a.getPeriodeBulan().equalsIgnoreCase("3")){
				bulan = "Maret";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("04")||a.getPeriodeBulan().equalsIgnoreCase("4")){
				bulan = "April";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("05")||a.getPeriodeBulan().equalsIgnoreCase("5")){
				bulan = "Mei";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("06")||a.getPeriodeBulan().equalsIgnoreCase("6")){
				bulan = "Juni";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("07")||a.getPeriodeBulan().equalsIgnoreCase("7")){
				bulan = "Juli";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("08")||a.getPeriodeBulan().equalsIgnoreCase("8")){
				bulan = "Agustus";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("09")||a.getPeriodeBulan().equalsIgnoreCase("9")){
				bulan = "September";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("10")){
				bulan = "Oktober";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("11")){
				bulan = "November";
			}else if(a.getPeriodeBulan().equalsIgnoreCase("12")){
				bulan = "Desember";
			}
			dto.setPeriode(bulan+" - "+a.getPeriodeTahun());
			dto.setStatus(a.getStatus());
			dto.setCreationDate(a.getCreationDate());
			
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;

		
	}



	@Override
	public Map<String, Object> general(Map<String, Object> input) {
		// TODO Auto-generated method stub
		Map<String, Object> out = new HashMap<>();
		if("update".equalsIgnoreCase((String) input.get("option"))){
			out = updateByPeriode(input);
		}
		return out;
	}


	private Map<String,Object> updateByPeriode(Map<String, Object> input){
		
		PlPeriodeLockDto dto = new PlPeriodeLockDto();
		dto = (PlPeriodeLockDto) input.get("periode");
		Map<String, Object> out= new HashMap<>();
		try {
//			plPeriodeLockDao.updateByLock(dto.getKodeKantorJr(), dto.getPeriodeBulan(), dto.getPeriodeTahun(), dto.getStatus(), dto.getUpdatedBy(), dto.getUpdatedDate());
			out.put("message", "OK");
			out.put("contentData", null);
			out.put("status", CommonConstants.OK_REST_STATUS);
		} catch (Exception e) {
			out.put("message", "ERR");
			out.put("contentData", null);
			out.put("status", CommonConstants.ERROR_REST_STATUS);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return out;
	}

	@Override
	public void updateTable() {
		// TODO Auto-generated method stub
//		PlPeriodeLock pl = new PlPeriodeLock();
		List<String> listCode = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		try{
			listCode = plPeriodeLockDao.getAllInstansiCode();
			for(String a : listCode){
				try{
					plPeriodeLockDao.updateLock(a);
				}catch(Exception s){
					s.printStackTrace();
				}
				PlPeriodeLock period = new PlPeriodeLock();
				period.setCreatedBy("SYSTEM");
				period.setCreationDate(new Date());
				period.setKodeKantorJr(a);
				period.setPeriodeBulan(sdf.format(new Date()));
				period.setStatus("OPEN");
				plPeriodeLockDao.save(period);
			}
		}catch(Exception s){
			s.printStackTrace();
			
		}
		
	}

}
