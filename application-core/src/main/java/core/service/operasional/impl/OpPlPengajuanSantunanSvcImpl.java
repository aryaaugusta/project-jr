package core.service.operasional.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import common.util.CommonConstants;
import share.PlPengajuanSantunanDto;
import core.dao.PlAdditionalDescDao;
import core.dao.PlBerkasPengajuanDao;
import core.dao.PlDisposisiDao;
import core.dao.PlPengajuanRsDao;
import core.dao.PlPengajuanSantunanDao;
import core.dao.PlPenyelesaianSantunanDao;
import core.dao.PlRumahSakitDao;
import core.dao.lov.LovDao;
import core.model.DasiJrRefCode;
import core.model.FndCamat;
import core.model.PlAdditionalDesc;
import core.model.PlBerkasPengajuan;
import core.model.PlDisposisi;
import core.model.PlPengajuanR;
import core.model.PlPengajuanSantunan;
import core.model.PlPenyelesaianSantunan;
import core.model.PlRumahSakit;
import core.service.operasional.OpPlPengajuanSantunanSvc;
import share.DasiJrRefCodeDto;
import share.PlAdditionalDescDto;
import share.PlBerkasPengajuanDto;
import share.PlDisposisiDto;
import share.PlPengajuanRsDto;
import share.PlPenyelesaianSantunanDto;
import share.PlRumahSakitDto;
import share.PlDataKecelakaanDto;

@Service
@Transactional
public class OpPlPengajuanSantunanSvcImpl implements OpPlPengajuanSantunanSvc {

	@Autowired
	PlPengajuanSantunanDao plPengajuanSantunanDao;

	@Autowired
	PlRumahSakitDao plRumahSakitDao;

	@Autowired
	PlAdditionalDescDao plAdditionalDescDao;

	@Autowired
	PlPengajuanRsDao plPengajuanRsDao;

	@Autowired
	PlDisposisiDao plDisposisiDao;

	@Autowired
	PlPenyelesaianSantunanDao plPenyelesaianSantunanDao;
	
	@Autowired
	PlBerkasPengajuanDao plBerkasPengajuanDao;

	@Autowired
	LovDao lovDao;

	@Autowired
	MapperFacade mapperFacade;

	private PlPengajuanSantunan convertToModel(PlPengajuanSantunanDto obj) {
		return mapperFacade.map(obj, PlPengajuanSantunan.class);
	}
	
	private PlBerkasPengajuan convertToModel(PlBerkasPengajuanDto obj) {
		return mapperFacade.map(obj, PlBerkasPengajuan.class);
	}


	private PlAdditionalDesc convertToModel(PlAdditionalDescDto obj) {
		return mapperFacade.map(obj, PlAdditionalDesc.class);
	}

	private PlPengajuanR convertToModel(PlPengajuanRsDto obj) {
		return mapperFacade.map(obj, PlPengajuanR.class);
	}

	private PlDisposisi convertToModel(PlDisposisiDto obj) {
		return mapperFacade.map(obj, PlDisposisi.class);
	}

	@Override
	public Map<String, Object> getDataList(Map<String, Object> input,
			String diajukanDi, String dilimpahkanDi, String namaKorban,
			String pengajuanDay, String pengajuanMonth, String pengajuanYear,
			String lakaDay, String lakaMonth, String lakaYear,
			String kodeInstansi, String noLaporan, String noBerkas,
			String statusProses, String penyelesaian, String namaPemohon,
			String kodeRs, String search) {
		List<Object[]> dataSantunan = plPengajuanSantunanDao.getListIndex(
				diajukanDi, dilimpahkanDi, namaKorban, pengajuanDay,
				pengajuanMonth, pengajuanYear, lakaDay, lakaMonth, lakaYear,
				kodeInstansi, noLaporan, noBerkas, statusProses, penyelesaian,
				namaPemohon, kodeRs, search);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();

		for (Object[] a : dataSantunan) {
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			// PlPengajuanSantunan santunan=(PlPengajuanSantunan) a[0];
			// dto.setDiajukanDi(santunan.getDiajukanDi());
			// dto.setNoBerkas(santunan.getNoBerkas());
			// dto.setTglPengajuan(santunan.getTglPengajuan());
			dto.setDiajukanDi((String) a[0]);
			dto.setNoBerkas((String) a[1]);
			dto.setTglPengajuan((Date) a[2]);
			dto.setNama((String) a[3]);
			dto.setCideraKorban((String) a[4]);
			dto.setNamaPemohon((String) a[5]);
			dto.setStatusProsesDesc((String) a[6]);
			dto.setPenyelesaian((String) a[7]);
			dto.setIdKecelakaan((String) a[8]);
			dto.setIdKorbanKecelakaan((String) a[9]);
			dto.setStatusProses((String) a[17]);
			// dto.setTglKejadian((Date) a[8]);
			// dto.setDeskripsiInstansi((String) a[9]);
			// dto.setDeskripsiRs((String) a[10]);

			// dto.setIdKorbanKecelakaan((String) a[11]);
			// dto.setIdKecelakaan((String) a[12]);
			// dto.setAlamatPemohon((String) a[13]);
			// dto.setStatusHubunganDes((String) a[14]);
			// dto.setNoIdentitas((String) a[15]);
			// dto.setCideraDes((String) a[16]);
			// dto.setKodeCidera((String) a[17]);
			// dto.setKodeCideraDes((String) a[17] + " - " + (String) a[16]);
			// dto.setKodeRs((String) a[18]);
			// dto.setDeskripsiRs((String) a[19]);
			// dto.setKodeDesRs((String) a[18] + " - " + (String) a[19]);
			// dto.setPembayaranDes((String) a[20]);
			// dto.setNamaKantor((String) a[21]);
			// dto.setKodeNamaKantor((String) a[0] + " - " + (String) a[21]);
			// dto.setJenisIdentitas((String) a[22]);
			// dto.setIdGuid((String) a[23]);
			// dto.setTglPenyelesaian((Date) a[24]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findById(Map<String, Object> input,
			String idKecelakaan) {
		List<Object[]> list = plPengajuanSantunanDao.findById2(idKecelakaan);

		List<PlDataKecelakaanDto> listDto = new ArrayList<>();

		for (Object[] objects : list) {
			PlDataKecelakaanDto dto = new PlDataKecelakaanDto();
			dto.setNoLaporanPolisi((String) objects[0]);
			dto.setTglKejadian((Date) objects[1]);
			dto.setNamaKorban((String) objects[2]);
			dto.setJaminan((String) objects[3]);
			dto.setStatusLaporanPolisi((String) objects[4]);
			dto.setUmur((BigDecimal) objects[5]);
			dto.setJenisKelamin((String) objects[6]);
			dto.setCidera((String) objects[7]);
			dto.setPenjamin((String) objects[8] + "-" + objects[9]);
			dto.setKasus((String) objects[10]);
			dto.setDeskripsiKecelakaan((String) objects[11]);
			dto.setStatus((String) objects[12]);
			dto.setNamaInstansi((String) objects[13]);
			dto.setIdKecelakaan((String) objects[14]);
			dto.setDeskripsiLokasi((String) objects[15]);
			dto.setNoIdentitas((String) objects[16]);
			dto.setJenisIdentitas((String) objects[17]);
			dto.setAlamat((String) objects[18]);
			dto.setStatusNikah((String) objects[19]);
			dto.setNoTelp((String) objects[20]);
			dto.setKodeKantorJr((String) objects[21]);
			dto.setIdKorbanKecelakaan((String) objects[22]);
			dto.setKodeSifatCidera((String) objects[23]);
			dto.setKodeJaminan((String) objects[24]);
			dto.setLingkupJaminan((String) objects[25]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> getListRs(Map<String, Object> input) {
		// TODO Auto-generated method stub

		List<PlRumahSakit> rs = plRumahSakitDao.findAll();
		List<PlRumahSakitDto> listDto = new ArrayList<>();

		for (PlRumahSakit list : rs) {
			PlRumahSakitDto dto = new PlRumahSakitDto();
			dto.setKodeRumahsakit(list.getKodeRumahsakit());
			dto.setDeskripsi(list.getDeskripsi());
			dto.setKodeNamaRs(list.getKodeRumahsakit() + "-"
					+ list.getDeskripsi());

			listDto.add(dto);
		}
		System.err.println("test " + listDto.size());

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		if (listDto.size() > 0) {
			map.put("contentData", listDto);
			return map;
		}
		map.put("contentData", listDto);

		return map;

	}
	
	@Override
	public int saveSantunanNoTrigger(PlPengajuanSantunanDto dto) {
		try {
			// PlPengajuanSantunan plPengajuanSantunan = new
			// PlPengajuanSantunan();
			// plPengajuanSantunan.setAlamatPemohon(dto.getAlamatPemohon());
			// plPengajuanSantunan.setCideraKorban(dto.getCideraKorban());
			// plPengajuanSantunan.setDiajukanDi(dto.getDiajukanDi());
			// plPengajuanSantunan.setKodeHubunganKorban(dto.getKodeHubunganKorban());
			// plPengajuanSantunan.setKodeJaminan(dto.getKodeJaminan());
			// plPengajuanSantunan.setNamaPemohon(dto.getNamaPemohon());
			// plPengajuanSantunan.setNoBerkas(dto.getNoBerkas());
			// plPengajuanSantunan.setTglPenerimaan(dto.getTglPenerimaan());
			// plPengajuanSantunan.setTransisiFlag("Y");
			// plPengajuanSantunan.setJenisIdentitas(dto.getJenisIdentitas());
			// plPengajuanSantunan.setIdKecelakaan(dto.getIdKecelakaan());
			// plPengajuanSantunan.setIdKorbanKecelakaan(dto.getIdKorbanKecelakaan());
			// plPengajuanSantunan.setStatusProses(dto.getStatusProses());
			// plPengajuanSantunan.setFlagHasilSurvey(dto.getFlagHasilSurvey());
			// plPengajuanSantunan.setKodePengajuan(dto.getKodePengajuan());

			PlPengajuanSantunan plPengajuanSantunan = new PlPengajuanSantunan();
			PlAdditionalDesc plAdditionalDesc = new PlAdditionalDesc();
			PlPengajuanR plPengajuanR = new PlPengajuanR();
			PlBerkasPengajuan plBerkasPengajuan = new PlBerkasPengajuan();
			
			plPengajuanSantunan = convertToModel(dto);
			plPengajuanSantunanDao.delete(plPengajuanSantunan);
			plPengajuanSantunanDao.save(plPengajuanSantunan);

			// pl additional desc
			System.err.println("luthfi11 "+dto.getKodeLokasiPemohonAdd());
			plAdditionalDesc.setNoBerkas(dto.getNoBerkas());
			plAdditionalDesc.setCreatedBy(dto.getCreatedBy());
			plAdditionalDesc.setCreatedDate(dto.getCreationDate());
			plAdditionalDesc.setLastUpdatedBy(dto.getLastUpdatedBy());
			plAdditionalDesc.setLastUpdatedDate(dto.getLastUpdatedDate());
			plAdditionalDesc.setTglMd(dto.getTglMdAdd());
			plAdditionalDesc.setTglRawatAwal(dto.getTglRawatAwalAdd());
			plAdditionalDesc.setTglRawatAkhir(dto.getTglRawatAkhirAdd());
			plAdditionalDesc.setKodeLokasiPemohon(dto.getKodeLokasiPemohonAdd());
			plAdditionalDescDao.save(plAdditionalDesc);

			// pl pengajuan rs
			plPengajuanR.setIdPengajuanRs(dto.getIdPengajuanRs());
			plPengajuanR.setKodeRumahsakit(dto.getKodeRumahsakitPengajuanRs());
			plPengajuanR.setNoBerkas(dto.getNoBerkas());
			plPengajuanR.setCreatedBy(dto.getCreatedBy());
			plPengajuanR.setCreationDate(dto.getCreationDate());
			plPengajuanRsDao.save(plPengajuanR);
			
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int saveSantunan(PlPengajuanSantunanDto dto) {
		try {
			// PlPengajuanSantunan plPengajuanSantunan = new
			// PlPengajuanSantunan();
			// plPengajuanSantunan.setAlamatPemohon(dto.getAlamatPemohon());
			// plPengajuanSantunan.setCideraKorban(dto.getCideraKorban());
			// plPengajuanSantunan.setDiajukanDi(dto.getDiajukanDi());
			// plPengajuanSantunan.setKodeHubunganKorban(dto.getKodeHubunganKorban());
			// plPengajuanSantunan.setKodeJaminan(dto.getKodeJaminan());
			// plPengajuanSantunan.setNamaPemohon(dto.getNamaPemohon());
			// plPengajuanSantunan.setNoBerkas(dto.getNoBerkas());
			// plPengajuanSantunan.setTglPenerimaan(dto.getTglPenerimaan());
			// plPengajuanSantunan.setTransisiFlag("Y");
			// plPengajuanSantunan.setJenisIdentitas(dto.getJenisIdentitas());
			// plPengajuanSantunan.setIdKecelakaan(dto.getIdKecelakaan());
			// plPengajuanSantunan.setIdKorbanKecelakaan(dto.getIdKorbanKecelakaan());
			// plPengajuanSantunan.setStatusProses(dto.getStatusProses());
			// plPengajuanSantunan.setFlagHasilSurvey(dto.getFlagHasilSurvey());
			// plPengajuanSantunan.setKodePengajuan(dto.getKodePengajuan());

			PlPengajuanSantunan plPengajuanSantunan = new PlPengajuanSantunan();
			PlAdditionalDesc plAdditionalDesc = new PlAdditionalDesc();
			PlPengajuanR plPengajuanR = new PlPengajuanR();
			PlBerkasPengajuan plBerkasPengajuan = new PlBerkasPengajuan();
			
			plPengajuanSantunan = convertToModel(dto);
			plPengajuanSantunanDao.save(plPengajuanSantunan);

			// pl additional desc
			System.err.println("luthfi11 "+dto.getKodeLokasiPemohonAdd());
			plAdditionalDesc.setNoBerkas(dto.getNoBerkas());
			plAdditionalDesc.setCreatedBy(dto.getCreatedBy());
			plAdditionalDesc.setCreatedDate(dto.getCreationDate());
			plAdditionalDesc.setLastUpdatedBy(dto.getLastUpdatedBy());
			plAdditionalDesc.setLastUpdatedDate(dto.getLastUpdatedDate());
			plAdditionalDesc.setTglMd(dto.getTglMdAdd());
			plAdditionalDesc.setTglRawatAwal(dto.getTglRawatAwalAdd());
			plAdditionalDesc.setTglRawatAkhir(dto.getTglRawatAkhirAdd());
			plAdditionalDesc.setKodeLokasiPemohon(dto.getKodeLokasiPemohonAdd());
			plAdditionalDescDao.save(plAdditionalDesc);

			// pl pengajuan rs
			plPengajuanR.setIdPengajuanRs(dto.getIdPengajuanRs());
			plPengajuanR.setKodeRumahsakit(dto.getKodeRumahsakitPengajuanRs());
			plPengajuanR.setNoBerkas(dto.getNoBerkas());
			plPengajuanR.setCreatedBy(dto.getCreatedBy());
			plPengajuanR.setCreationDate(dto.getCreationDate());
			plPengajuanRsDao.save(plPengajuanR);
			
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	/*
	 * @Override public int save(PlPengajuanSantunanDto plPengajuanSantunanDto)
	 * { try { PlPengajuanSantunan plPengajuanSantunan =
	 * convertToModel(plPengajuanSantunanDto);
	 * plPengajuanSantunanDao.save(plPengajuanSantunan); return
	 * CommonConstants.OK_REST_STATUS; } catch (Exception e) { return
	 * CommonConstants.ERROR_REST_STATUS; } }
	 */

	@Override
	public int savePengajuanRs(PlPengajuanRsDto plPengajuanRsDto) {
		try {
			PlPengajuanR plPengajuanR = convertToModel(plPengajuanRsDto);
			plPengajuanRsDao.save(plPengajuanR);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int saveAdditional(PlAdditionalDescDto plAdditionalDescDto) {
		try {
			PlAdditionalDesc plAdditionalDesc = convertToModel(plAdditionalDescDto);
			plAdditionalDescDao.save(plAdditionalDesc);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> findByNoPengajuan(Map<String, Object> input,
			String noPengajuan) {
		List<Object[]> list = plPengajuanSantunanDao
				.findByNoPengajuan(noPengajuan);

		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();

		for (Object[] a : list) {
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setNoBerkas((String) a[0]);
			dto.setDiajukanDi((String) a[1]);
			dto.setTglPengajuan((Date) a[2]);
			dto.setTglPenyelesaian((Date) a[3]);
			dto.setStatusProses((String) a[4]);
			dto.setPenyelesaian((String) a[5]);
			dto.setKodeNamaKantor((String) a[1] + " - " + (String) a[6]);
			dto.setDilimpahkanKe((String) a[7]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findByNoBerkas(Map<String, Object> input,
			String noBerkas) {

		List<PlPengajuanSantunan> pengajuanSantunan = plPengajuanSantunanDao
				.findByNoBerkas(noBerkas);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(pengajuanSantunan,
				PlPengajuanSantunanDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public int saveDisposisi(PlDisposisiDto plDisposisiDto) {

		try {
			PlDisposisi plDisposisi = convertToModel(plDisposisiDto);
			plDisposisiDao.save(plDisposisi);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> getListDisposisiById(Map<String, Object> input,
			String noBerkas) {
		List<PlDisposisi> disposisi = plDisposisiDao.getListbyId(noBerkas);
		List<PlDisposisiDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(disposisi, PlDisposisiDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> getDataPengajuanByNoBerkas(
			Map<String, Object> input, String noBerkas) {

		List<Object[]> list = plPengajuanSantunanDao
				.getDataPengajuanSantunanByNoBerkas(noBerkas);

		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();

		for (Object[] a : list) {
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setKodeNamaKantor((String) a[4] + " - " + (String) a[0]);
			dto.setStatusHubunganDes((String) a[1]);
			dto.setStatusProsesDesc((String) a[2]);
			dto.setPenyelesaian((String) a[3]);
			dto.setDilimpahkanKe((String) a[5]);
			dto.setNamaKantor((String) a[6]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;

	}

	@Override
	public int updateSantunanOtorisasi(
			PlPengajuanSantunanDto plPengajuanSantunanDto) {
		try {
			PlPengajuanSantunan plPengajuanSantunan = convertToModel(plPengajuanSantunanDto);
			plPengajuanSantunanDao
					.updatePengajuanSantunanOtorisasi(plPengajuanSantunan);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int updateStatusProses(PlPengajuanSantunanDto plPengajuanSantunanDto) {
		try {
			PlPengajuanSantunan plPengajuanSantunan = convertToModel(plPengajuanSantunanDto);
			plPengajuanSantunanDao.updateProsesSelanjutnya(plPengajuanSantunan);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> findOnePenyelesaian(Map<String, Object> input,
			String noBerkas) {

		List<PlPenyelesaianSantunan> penyelesaianSantunan = plPenyelesaianSantunanDao
				.findOneByNoBerkas(noBerkas);
		List<PlPenyelesaianSantunanDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(penyelesaianSantunan,
				PlPenyelesaianSantunanDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	// added by Luthfi
	@Override
	public Map<String, Object> getSantunanByIdKorban(Map<String, Object> input,
			String idKorban) {

		List<PlPengajuanSantunan> pengajuanSantunan = plPengajuanSantunanDao
				.getPengajuanByIdKorban(idKorban);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(pengajuanSantunan,
				PlPengajuanSantunanDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	// added by Luthfi
	@Override
	public Map<String, Object> getIndexHapusPengajuan(
			Map<String, Object> input, String noBerkas) {

		List<Object[]> pengajuan = plPengajuanSantunanDao
				.getIndexHapusPengajuan(noBerkas);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();

		for (Object[] a : pengajuan) {
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setNoBerkas((String) a[0]);
			dto.setTglPenerimaan((Date) a[1]);
			dto.setNamaKorban((String) a[2]);
			dto.setNamaPemohon((String) a[3]);
			dto.setStatusProsesDesc((String) a[4]);
			dto.setOtorisasiFlag((String) a[5]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public int deletePengajuan(PlPengajuanSantunanDto plPengajuanSantunanDto) {
		try {
			PlPengajuanSantunan plPengajuanSantunan = new PlPengajuanSantunan();
			plPengajuanSantunan.setNoBerkas(plPengajuanSantunanDto
					.getNoBerkas());
			plPengajuanSantunanDao.deletePengajuan(plPengajuanSantunan);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> findAdditionalDescByNoBerkas(
			Map<String, Object> input, String noBerkas) {
		List<PlAdditionalDesc> plAdditionalDescs = plAdditionalDescDao.getOnebyNoberkas(noBerkas);
		List<PlAdditionalDescDto> listDto = new ArrayList<>();
		for(PlAdditionalDesc a : plAdditionalDescs){
			PlAdditionalDescDto dto = new PlAdditionalDescDto();
			dto.setCreatedBy(a.getCreatedBy());
			dto.setCreatedDate(a.getCreatedDate());
			dto.setNoBerkas(a.getNoBerkas());
			dto.setLastUpdatedBy(a.getLastUpdatedBy());
			dto.setLastUpdateDatePenyelesaian(a.getLastUpdatedDate());
			dto.setTglMd(a.getTglMd());
			dto.setTglTerimaLimpah(a.getTglTerimaLimpah());
			dto.setNamaRekening(a.getNamaRekening());
			dto.setJnsRekening(a.getJnsRekening());
			dto.setTglRawatAwal(a.getTglRawatAwal());
			dto.setTglRawatAkhir(a.getTglRawatAkhir());
			dto.setIdRekRs(a.getIdRekRs());
			dto.setKodeLokasiPemohon(a.getKodeLokasiPemohon());
			
			System.err.println("luthfi12 "+dto.getKodeLokasiPemohon());
			listDto.add(dto);
			
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;

	}

	@Override
	public Map<String, Object> findPengajuanRsByNoBerkas(
			Map<String, Object> input, String noBerkas) {
		
		List<PlPengajuanR> pengajuanRs = plPengajuanSantunanDao.findPengajuanRsByNoBerkas(noBerkas);
		List<PlPengajuanRsDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(pengajuanRs,	PlPengajuanRsDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findBerkasPengajuanByNoBerkas(
			Map<String, Object> input, String noBerkas) {
		
		List<PlBerkasPengajuan> list = plPengajuanSantunanDao.findBerkasPengajuanByNoBerkas(noBerkas);
		List<PlBerkasPengajuanDto> listDto = new ArrayList<>();
		List<Object[]> listJenisDokumen = lovDao.getJenisDokumen();
		
		for(PlBerkasPengajuan o : list){
			PlBerkasPengajuanDto dto = new PlBerkasPengajuanDto();
			dto =mapperFacade.map(o, PlBerkasPengajuanDto.class);
			for(Object[] a : listJenisDokumen){
				if(dto.getKodeBerkas().equalsIgnoreCase((String)a[0])){
					dto.setKodeBerkasDesc((String)a[1]);
				}
			}
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public int saveBerkasPengajuan(List<PlBerkasPengajuanDto> plBerkasPengajuanDtos) {
		try {
			for(PlBerkasPengajuanDto plBerkasPengajuanDto : plBerkasPengajuanDtos){
				PlBerkasPengajuan plBerkasPengajuan = convertToModel(plBerkasPengajuanDto);
				plBerkasPengajuanDao.save(plBerkasPengajuan);
			}
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}

	}

	@Override
	public int deleteBerkasPengajuan(
			List<PlBerkasPengajuanDto> plBerkasPengajuanDtos) {
		try {
			for(PlBerkasPengajuanDto plBerkasPengajuanDto : plBerkasPengajuanDtos){
				PlBerkasPengajuan plBerkasPengajuan = convertToModel(plBerkasPengajuanDto);
				plBerkasPengajuanDao.delete(plBerkasPengajuan);
			}
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int saveSantunanOnly(PlPengajuanSantunanDto plPengajuanSantunanDto) {
		try {
				PlPengajuanSantunan pengajuanSantunan = convertToModel(plPengajuanSantunanDto);
				plPengajuanSantunanDao.save(pengajuanSantunan);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}

	}

}
