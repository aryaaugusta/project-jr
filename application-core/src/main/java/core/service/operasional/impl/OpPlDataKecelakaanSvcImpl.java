package core.service.operasional.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.SetFactoryBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.AuthUserDto;
import share.PlAngkutanKecelakaanDto;
import share.PlDataKecelakaanDto;
import share.PlKorbanKecelakaanDto;
import share.PlMappingPoldaDto;
import share.PlNihilKecelakaanDto;
import common.util.CommonConstants;
import common.util.JsonUtil;
import common.util.StringUtil;
import core.dao.PlAngkutanKecelakaanDao;
import core.dao.PlDataKecelakaanDao;
import core.dao.PlKorbanKecelakaanDao;
import core.dao.PlNihilKecelakaanDao;
import core.dao.PlPengajuanSantunanDao;
import core.dao.PlRegisterSementaraDao;
import core.model.AuthUser;
import core.model.DasiJrRefCode;
import core.model.FndCamat;
import core.model.FndKantorJasaraharja;
import core.model.PlAngkutanKecelakaan;
import core.model.PlDataKecelakaan;
import core.model.PlKorbanKecelakaan;
import core.model.PlMappingPolda;
import core.model.PlNihilKecelakaan;
import core.model.PlPengajuanSantunan;
import core.model.PlRegisterSementara;
import core.service.operasional.OpPlDataKecelakaanSvc;

@Service
@Transactional
public class OpPlDataKecelakaanSvcImpl implements OpPlDataKecelakaanSvc {
	
	@Autowired
	PlDataKecelakaanDao plDataKecelakaanDao;
	
	@Autowired
	PlAngkutanKecelakaanDao plAngkutanKecelakaanDao;

	@Autowired
	PlKorbanKecelakaanDao plKorbanKecelakaanDao;
	
	@Autowired
	PlNihilKecelakaanDao plNihilKecelakaanDao;
	
	@Autowired
	PlPengajuanSantunanDao plPengajuanSantunanDao;
	
	@Autowired
	PlRegisterSementaraDao plRegisterSementaraDao;
	
	@Autowired
	MapperFacade mapperFacade;
	
	private PlKorbanKecelakaan convertToModel(PlKorbanKecelakaanDto obj) {
		return mapperFacade.map(obj, PlKorbanKecelakaan.class);
	}
	
	private PlNihilKecelakaan convertToModel(PlNihilKecelakaanDto obj) {
		return mapperFacade.map(obj, PlNihilKecelakaan.class);
	}

	@Override
	public Map<String, Object> getDataList(Map<String, Object> input,
			String kejadianStartDate, String kejadianEndDate,
			String asalBerkas, String samsat, String laporanStartDate,
			String laporanEndDate, String instansi, String instansiPembuat,
			String noLaporan, String lokasi, String namaKorban,
			String noIdentitas, String lingkupJaminan,
			String jenisPertanggungan, String sifatCidera,
			String sifatKecelakaan, String kecelakaanKatostrop,
			String perusahaanPenerbangan, String perusahaanOtobus, String search) {
		
		search = StringUtil.surroundString(StringUtil.nevl(search, "%%"), "%");
		
		List<Object[]> dataLaka = plDataKecelakaanDao.getDataList(
				kejadianStartDate, kejadianEndDate, asalBerkas, samsat, laporanStartDate, 
				laporanEndDate, instansi, instansiPembuat, noLaporan, lokasi, namaKorban, 
				noIdentitas, lingkupJaminan, jenisPertanggungan, sifatCidera, sifatKecelakaan, 
				kecelakaanKatostrop, perusahaanPenerbangan, perusahaanOtobus, search);
		List<PlDataKecelakaanDto> listDto = new ArrayList<>();
		
		for(Object[] a : dataLaka){
			PlDataKecelakaanDto dto = new PlDataKecelakaanDto();
			dto.setTglKejadian((Date) a[0]);
			dto.setLokasiDesc((String)a[1]);
			dto.setNamaInstansi((String)a[2]);
			dto.setNoLaporanPolisi((String)a[3]);
			dto.setNamaKorban((String)a[4]);
			dto.setCidera((String)a[5]);
			dto.setSifatKecelakaanDesc((String)a[6]);
			dto.setJumlahMeninggal((BigDecimal)a[7]);
			dto.setStatusLaporanPolisi((String)a[8]);			
			dto.setIdKecelakaan((String)a[9]);
			dto.setAsalBerkas((String)a[10]);
			dto.setTglLaporanPolisi((Date)a[11]);
			dto.setKodeInstansi((String)a[12]);
			dto.setNoIdentitas((String)a[13]);
			dto.setKodeSifatCidera((String)a[14]);
			dto.setLingkupJaminan((String)a[15]);
			dto.setDeskripsiJaminan((String)a[16]);
			dto.setCideraHighValue((String)a[17]);
			dto.setKodeJaminan((String)a[18]);
			dto.setSifatKecelakaanDesc((String)a[19]);
			dto.setIdKorbanKecelakaan((String)a[20]);
			dto.setKodeWilayah((String)a[21]);
			
			List<PlPengajuanSantunan> listPengajuan = plPengajuanSantunanDao.findPengajuanByIdKorban((String)a[20]);
			List<PlRegisterSementara> listRegister = plRegisterSementaraDao.findRegisterByIdKorban((String)a[20]);
			dto.setFlagDelete(listPengajuan.size()==0&&listRegister.size()==0?false:true);
			
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", (long) listDto.size());
		map.put("contentData", listDto);

		return map;
	}
	
	@Override
	public Map<String, Object> getDataForPrint(Map<String, Object> map){
		/**
		 * map.put("idKec",idKec);
		map.put("login", getCurrentUserSessionJR().getLoginID());
		map.put("kantor", getCurrentUserSessionJR().getKantor());
		 */
		String idLaka = "";
		String login = "";
		String kantor = "";
		if(map.get("idKec")!=null) 
			idLaka = (String) map.get("idKec");
		
		if(map.get("login")!=null)
			login = (String) map.get("login");
		
		if(map.get("kantor")!=null)
			kantor = (String) map.get("kantor");
		//Start model
		PlDataKecelakaan modelDataLaka = new PlDataKecelakaan();
		List<Object[]> modelDataLaka2 = new ArrayList<>();
		DasiJrRefCode refCode = new DasiJrRefCode();
		Map<String, Object> temp = new HashMap<>();
		FndCamat modelCamat = new FndCamat();
		FndKantorJasaraharja modelKantor = new FndKantorJasaraharja();
		//end model
		
		//start DTO
		PlDataKecelakaanDto dataLaka = new PlDataKecelakaanDto();
		List<PlAngkutanKecelakaanDto> listKend = new ArrayList<>();
		List<PlKorbanKecelakaanDto> listKorban = new ArrayList<>();
		AuthUserDto pinCab = new AuthUserDto();
		AuthUserDto petugas = new AuthUserDto();
		try{
			modelDataLaka2 = plDataKecelakaanDao.getDataById(idLaka);
//			dataLaka = mapperFacade.map(modelDataLaka, PlDataKecelakaanDto.class);
			for(Object[] a : modelDataLaka2){
		//		modelDataLaka
			}
			List<Object[]> ob = plAngkutanKecelakaanDao.getKendaraanByLaka(idLaka);
			for(Object[] o : ob){
				PlAngkutanKecelakaanDto dto = mapperFacade.map(o[0], PlAngkutanKecelakaanDto.class);
				dto.setStatusDesc((String) o[1]);
				listKend.add(dto);
			}
			
			List<Object[]> oc = plDataKecelakaanDao.getKorbanFromLaka(idLaka);
			System.out.println("------------- data korban ");
			for(Object[] s : oc){
				System.out.println(JsonUtil.getJson(s));
				System.out.println(s[0]);
				PlKorbanKecelakaanDto korban = new PlKorbanKecelakaanDto();
				korban.setIdKorbanKecelakaan((String)s[7]);
				korban.setNamaJkUmur((String)s[0]);
				korban.setAlamat((String)s[1]);
				korban.setCideraDesc((String)s[2]);
				korban.setStatusKorbanDesc((String)s[3]);
				korban.setIdKorbanKecelakaan((String)s[7]);
				
				List<Object[]> ow = plDataKecelakaanDao.getKorbanByIdKorban(korban.getIdKorbanKecelakaan());
				for(Object[] a : ow){
					korban.setAngkutanKecelakaanDesc((String)a[10]+" - "+(String)a[11]);
					korban.setAngkutanPenanggungKecelakaanDesc((String)a[8]+" - "+(String)a[9]);					
					korban.setJaminanDesc((String)a[12]);
					listKorban.add(korban);
				}
				System.out.println("test "+korban.getAngkutanKecelakaanDesc());
				listKorban.add(korban);
			}
			//KodeKasusKecelakaan
			List<DasiJrRefCode> tempRef = new ArrayList<>();
			try{
				tempRef = plDataKecelakaanDao.getRvMeaningByRvLowValue(modelDataLaka.getKodeKasusKecelakaan(), "PL KASUS KECELAKAAN");
			}catch(Exception l){
				l.printStackTrace();
			}
			
			if(tempRef!=null && tempRef.size()>0 && !tempRef.isEmpty()){
				refCode = tempRef.get(0);
			}
			
			if(refCode!=null && refCode.getRvMeaning()!=null && !refCode.equals(new DasiJrRefCode())){
				temp.put("kasusLaka", refCode.getRvMeaning());
				dataLaka.setKasusKecelakaanDesc(refCode.getRvMeaning());
			}else{
				temp.put("kasusLaka","");
				dataLaka.setKasusKecelakaanDesc("");
			}
			//Kode sifat kecelakaan
			refCode = new DasiJrRefCode();
			tempRef = new ArrayList<>();
			try{
				tempRef = plDataKecelakaanDao.getRvMeaningByRvLowValue(modelDataLaka.getSifatKecelakaan(), "KODE SIFAT KECELAKAAN");
			}catch(Exception l){
				l.printStackTrace();
			}
			if(tempRef!=null && tempRef.size()>0 && !tempRef.isEmpty())
				refCode = tempRef.get(0);
			
			if(refCode!=null && refCode.getRvMeaning()!=null && !refCode.equals(new DasiJrRefCode())){
				temp.put("sifatLaka", refCode.getRvMeaning());
				dataLaka.setSifatKecelakaanDesc(refCode.getRvMeaning());
			}else{
				temp.put("sifatLaka","");
				dataLaka.setSifatKecelakaanDesc("");
			}
			
			List<FndCamat> camatTemp = new ArrayList<>();
			List<FndKantorJasaraharja> kantorTemp = new ArrayList<>();
			try{
				camatTemp = plDataKecelakaanDao.getCamatByLoc(modelDataLaka.getKodeLokasi());
				kantorTemp = plDataKecelakaanDao.getKantorFromLokasi(modelDataLaka.getKodeWilayah());
			}catch(Exception koplak){
				koplak.printStackTrace();
			}
			
			if(camatTemp!=null && camatTemp.size()>0 && !camatTemp.isEmpty())
				modelCamat = camatTemp.get(0);
			
			if(kantorTemp!=null && kantorTemp.size()>0 && !kantorTemp.isEmpty())
				modelKantor = kantorTemp.get(0);
			dataLaka.setKabKotaDesc(modelCamat.getNamaKabkota());
			dataLaka.setCamatDesc(modelCamat.getNamaCamat());
			dataLaka.setProvinsiDesc(modelCamat.getNamaProvinsi());
			List<AuthUser> auTemp = new ArrayList<>();
			AuthUser kaCab = new AuthUser();
			try{
				kaCab = plDataKecelakaanDao.getKepalaCabang(kantor).get(0);
				pinCab = mapperFacade.map(kaCab, AuthUserDto.class);
			}catch(Exception laper){
				laper.printStackTrace();
			}
			map.put("dataKec", dataLaka);
			map.put("listKend", listKend);
			map.put("listKorban", listKorban);
			map.put("kepalaCabang", pinCab==null?new AuthUserDto():pinCab.getLogin()!=null?pinCab:new AuthUserDto());
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return map;
	}

	@Override
	public Map<String, Object> getDataByIdKecelakaan(Map<String, Object> input,String idKecelakaan) {
		
		List<Object[]> dataLaka = plDataKecelakaanDao.getDataById(idKecelakaan);
		List<PlDataKecelakaanDto> listDto = new ArrayList<>();

		for(Object[] b : dataLaka){
			PlDataKecelakaanDto dto = new PlDataKecelakaanDto();					
			dto.setTglLaporanPolisi((Date)b[0]);
			dto.setNamaPetugas((String)b[1]);
			dto.setTglKejadian((Date)b[2]);
			dto.setDeskripsiLokasi((String)b[3]);
			dto.setDeskripsiKecelakaan((String)b[4]);
			dto.setGpsLs((String)b[5]);
			dto.setGpsLu((String)b[6]);
			
			dto.setAsalBerkasDesc((String)b[7]);
			dto.setInstansiDesc((String)b[8]);
			dto.setProvinsiDesc((String)b[9]);
			dto.setKabKotaDesc((String)b[10]);	
			dto.setCamatDesc((String)b[11]);	
			dto.setSamsatDesc((String)b[12]);
			dto.setKasusKecelakaanDesc((String)b[13]);
			dto.setSifatKecelakaanDesc((String)b[14]);
			dto.setIdKecelakaan((String)b[15]);
			dto.setNoLaporanPolisi((String)b[16]);
			
			dto.setAsalBerkas((String)b[17]);
			dto.setKodeInstansi((String)b[18]);
			dto.setKodeWilayah((String)b[19]);
			dto.setKodeProvinsiLokasi((String)b[20]);
			dto.setKodeKabkotaLokasi((String)b[21]);
			dto.setKodeCamatLokasi((String)b[22]);
			dto.setKodeKasusKecelakaan((String)b[23]);
			dto.setSifatKecelakaan((String)b[24]);
			
			dto.setNoUrut((BigDecimal) b[25]);
			dto.setStatusLaporanPolisi((String)b[26]);
			dto.setStatusTransisi((String)b[27]);
			dto.setKodeLokasi((String)b[28]);

			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> getAngkutanByIdLaka(Map<String, Object> input, String idKecelakaan, String search) {
		
		List<Object[]> angkutanLaka = plDataKecelakaanDao.getAngkutanByIdLaka(idKecelakaan, search);
		List<PlAngkutanKecelakaanDto> listDtos = new ArrayList<>();
		
		for(Object[] a : angkutanLaka){
//			PlAngkutanKecelakaan b = (PlAngkutanKecelakaan) a[0];
			PlAngkutanKecelakaanDto dto = new PlAngkutanKecelakaanDto();			
//			dto.setAlamatPemilik(b.getAlamatPemilik());
//			dto.setAlamatPengemudi(b.getAlamatPengemudi());
			dto.setIdAngkutanKecelakaan((String)a[7]);
//			dto.setIdKecelakaan(b.getIdKecelakaan());
//			dto.setKode(b.getKode());
//			dto.setKodeGolongan(b.getKodeGolongan());
			dto.setKodeJenis((String)a[1]);
//			dto.setKodeJenisSim(b.getKodeJenisSim());
//			dto.setKodeMerk(b.getKodeMerk());
//			dto.setKodePo(b.getKodePo());
//			dto.setMasaBerlakuSim(b.getMasaBerlakuSim());
//			dto.setNamaPemilik(b.getNamaPemilik());
			dto.setNamaPengemudi((String)a[2]);
			dto.setNoPolisi((String)a[0]);
//			dto.setNoSimPengemudi(b.getNoSimPengemudi());
//			dto.setStatusKendaraan(b.getStatusKendaraan());
//			dto.setTahunPembuatan(b.getTahunPembuatan());
			
			dto.setJenisDesc((String) a[3]);
			dto.setStatusDesc((String) a[4]);
			dto.setSimDesc((String)a[5]);
			dto.setMerkDesc((String)a[6]);
			dto.setNoPlatNamaPengemudiDesc((String)a[0]+" - "+(String)a[2]);
			List<Object[]> list = plDataKecelakaanDao.getAngkutanByKorban(dto.getIdAngkutanKecelakaan());
			if(list.size()>0){
				dto.setFlagUsed(true);
			}else{
				dto.setFlagUsed(false);
			}
			listDtos.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDtos);

		return map;
	}

	@Override
	public int saveDataLaka(PlDataKecelakaanDto plDataKecelakaanDto) {
		
		try {
			PlDataKecelakaan plDataKecelakaan = new PlDataKecelakaan();
			plDataKecelakaan.setAsalBerkas(plDataKecelakaanDto.getAsalBerkas());
			plDataKecelakaan.setCreatedBy(plDataKecelakaanDto.getCreatedBy());
			plDataKecelakaan.setCreationDate(plDataKecelakaanDto.getCreationDate());
			plDataKecelakaan.setDeskripsiKecelakaan(plDataKecelakaanDto.getDeskripsiKecelakaan());
			plDataKecelakaan.setDeskripsiLokasi(plDataKecelakaanDto.getDeskripsiLokasi());
			plDataKecelakaan.setGpsLs(plDataKecelakaanDto.getGpsLs());
			plDataKecelakaan.setGpsLu(plDataKecelakaanDto.getGpsLu());
			plDataKecelakaan.setIdGuid(plDataKecelakaanDto.getIdGuid());
			plDataKecelakaan.setIdKecelakaan(plDataKecelakaanDto.getIdKecelakaan());
			plDataKecelakaan.setKodeInstansi(plDataKecelakaanDto.getKodeInstansi());
			plDataKecelakaan.setKodeKantorJr(plDataKecelakaanDto.getKodeKantorJr());
			plDataKecelakaan.setKodeKasusKecelakaan(plDataKecelakaanDto.getKodeKasusKecelakaan());
			plDataKecelakaan.setKodeLintasan(plDataKecelakaanDto.getKodeLintasan());
			plDataKecelakaan.setKodeLokasi(plDataKecelakaanDto.getKodeLokasi());
			plDataKecelakaan.setKodeWilayah(plDataKecelakaanDto.getKodeWilayah());
			plDataKecelakaan.setNamaPetugas(plDataKecelakaanDto.getNamaPetugas());
			plDataKecelakaan.setNoLaporanPolisi(plDataKecelakaanDto.getNoLaporanPolisi());
			plDataKecelakaan.setNoUrut(plDataKecelakaanDto.getNoUrut());
			plDataKecelakaan.setSifatKecelakaan(plDataKecelakaanDto.getSifatKecelakaan());
			plDataKecelakaan.setStatusLaporanPolisi(plDataKecelakaanDto.getStatusLaporanPolisi());
			plDataKecelakaan.setStatusTransisi(plDataKecelakaanDto.getStatusTransisi());
			plDataKecelakaan.setTglKejadian(plDataKecelakaanDto.getTglKejadian());
			plDataKecelakaan.setTglLaporanPolisi(plDataKecelakaanDto.getTglLaporanPolisi());
			
			plDataKecelakaanDao.save(plDataKecelakaan);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int saveAngkutanKecelakaan(PlAngkutanKecelakaanDto plAngkutanKecelakaanDto) {
		
		System.out.println("test "+plAngkutanKecelakaanDto.getIdAngkutanKecelakaan());
		try {
			PlAngkutanKecelakaan plAngkutanKecelakaan = new PlAngkutanKecelakaan();
			plAngkutanKecelakaan.setAlamatPemilik(plAngkutanKecelakaanDto.getAlamatPemilik());
			plAngkutanKecelakaan.setAlamatPengemudi(plAngkutanKecelakaanDto.getAlamatPengemudi());
			plAngkutanKecelakaan.setCreatedBy(plAngkutanKecelakaanDto.getCreatedBy());
			plAngkutanKecelakaan.setCreationDate(plAngkutanKecelakaanDto.getCreationDate());
			plAngkutanKecelakaan.setIdAngkutanKecelakaan(plAngkutanKecelakaanDto.getIdAngkutanKecelakaan());
//			plAngkutanKecelakaan.setIdGuid(plAngkutanKecelakaanDto.getIdGuid());
			plAngkutanKecelakaan.setIdKecelakaan(plAngkutanKecelakaanDto.getIdKecelakaan());
//			plAngkutanKecelakaan.setKode(kode);
//			plAngkutanKecelakaan.setKodeGolongan(kodeGolongan);
			plAngkutanKecelakaan.setKodeJenis(plAngkutanKecelakaanDto.getKodeJenis());
			plAngkutanKecelakaan.setKodeJenisSim(plAngkutanKecelakaanDto.getKodeJenisSim());
			plAngkutanKecelakaan.setKodeMerk(plAngkutanKecelakaanDto.getKodeMerk());
			plAngkutanKecelakaan.setKodePo(plAngkutanKecelakaanDto.getKodePo());
			plAngkutanKecelakaan.setMasaBerlakuSim(plAngkutanKecelakaanDto.getMasaBerlakuSim());
			plAngkutanKecelakaan.setNamaPemilik(plAngkutanKecelakaanDto.getNamaPemilik());
			plAngkutanKecelakaan.setNamaPengemudi(plAngkutanKecelakaanDto.getNamaPengemudi());
			plAngkutanKecelakaan.setNoPolisi(plAngkutanKecelakaanDto.getNoPolisi());
			plAngkutanKecelakaan.setStatusKendaraan(plAngkutanKecelakaanDto.getStatusKendaraan());
			plAngkutanKecelakaan.setNoSimPengemudi(plAngkutanKecelakaanDto.getNoSimPengemudi());
			plAngkutanKecelakaan.setTahunPembuatan(plAngkutanKecelakaanDto.getTahunPembuatan());
			
			plAngkutanKecelakaanDao.save(plAngkutanKecelakaan);
			return CommonConstants.OK_REST_STATUS;

		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int saveKorban(PlKorbanKecelakaanDto plKorbanKecelakaanDto) {
		try {
			PlKorbanKecelakaan plKorbanKecelakaan = convertToModel(plKorbanKecelakaanDto);
			PlKorbanKecelakaan korban = new PlKorbanKecelakaan();
			List<PlKorbanKecelakaan> Lkorban = new ArrayList<>();
			
			try {
				System.out.println("Start Get one korban kecelakaan");
				Lkorban = plKorbanKecelakaanDao.getKorbanDariID(plKorbanKecelakaan.getIdKorbanKecelakaan());
				System.out.println("end get one korban kecelakaan");
				System.out.println(Lkorban == null);
				System.out.println(Lkorban.size());
				if(Lkorban.size()>0){
					korban = Lkorban.get(0);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(Lkorban.size()==0){
				System.out.println("failed to get idkorban kecelakaan (new korban)");
				plKorbanKecelakaanDao.save(plKorbanKecelakaan);
			}else{
				System.out.println("korban kecelakaan found");
				korban.setAlamat(plKorbanKecelakaan.getAlamat()==null?korban.getAlamat():plKorbanKecelakaan.getAlamat());
				korban.setCreatedBy(plKorbanKecelakaan.getCreatedBy()==null?korban.getCreatedBy():plKorbanKecelakaan.getCreatedBy());
				korban.setCreationDate(plKorbanKecelakaan.getCreationDate()==null?korban.getCreationDate():plKorbanKecelakaan.getCreationDate());
				korban.setIdAngkutanKecelakaan(plKorbanKecelakaan.getIdAngkutanKecelakaan()==null?korban.getIdAngkutanKecelakaan():plKorbanKecelakaan.getIdAngkutanKecelakaan());
				korban.setIdAngkutanPenanggung(plKorbanKecelakaan.getIdAngkutanPenanggung()==null?korban.getIdAngkutanPenanggung():plKorbanKecelakaan.getIdAngkutanPenanggung());
				korban.setIdGuid(plKorbanKecelakaan.getIdGuid()==null?korban.getIdGuid():plKorbanKecelakaan.getIdGuid());
				korban.setIdKecelakaan(plKorbanKecelakaan.getIdKecelakaan()==null?korban.getIdKecelakaan():plKorbanKecelakaan.getIdKecelakaan());
				korban.setIdKorbanKecelakaan(plKorbanKecelakaan.getIdKorbanKecelakaan()==null?korban.getIdKorbanKecelakaan():plKorbanKecelakaan.getIdKorbanKecelakaan());
				korban.setJenisIdentitas(plKorbanKecelakaan.getJenisIdentitas()==null?korban.getJenisIdentitas():plKorbanKecelakaan.getJenisIdentitas());
				korban.setJenisKelamin(plKorbanKecelakaan.getJenisKelamin()==null?korban.getJenisKelamin():plKorbanKecelakaan.getJenisKelamin());
				korban.setKodeJaminan(plKorbanKecelakaan.getKodeJaminan()==null?korban.getKodeJaminan():plKorbanKecelakaan.getKodeJaminan());
				korban.setKodePekerjaan(plKorbanKecelakaan.getKodePekerjaan()==null?korban.getKodePekerjaan():plKorbanKecelakaan.getKodePekerjaan());
				korban.setKodeSifatCidera(plKorbanKecelakaan.getKodeSifatCidera()==null?korban.getKodeSifatCidera():plKorbanKecelakaan.getKodeSifatCidera());
				korban.setKodeStatusKorban(plKorbanKecelakaan.getKodeStatusKorban()==null?korban.getKodeStatusKorban():plKorbanKecelakaan.getKodeStatusKorban());
				korban.setLastUpdatedBy(plKorbanKecelakaan.getLastUpdatedBy()==null?korban.getLastUpdatedBy():plKorbanKecelakaan.getLastUpdatedBy());
				korban.setLastUpdatedDate(plKorbanKecelakaan.getLastUpdatedDate()==null?korban.getLastUpdatedDate():plKorbanKecelakaan.getLastUpdatedDate());
				korban.setNama(plKorbanKecelakaan.getNama()==null?korban.getNama():plKorbanKecelakaan.getNama());
				korban.setNoIdentitas(plKorbanKecelakaan.getNoIdentitas()==null?korban.getNoIdentitas():plKorbanKecelakaan.getNoIdentitas());
				korban.setNoTelp(plKorbanKecelakaan.getNoTelp()==null?korban.getNoTelp():plKorbanKecelakaan.getNoTelp());
				korban.setStatusNikah(plKorbanKecelakaan.getStatusNikah()==null?korban.getStatusNikah():plKorbanKecelakaan.getStatusNikah());
				korban.setUmur(plKorbanKecelakaan.getUmur()==null?korban.getUmur():plKorbanKecelakaan.getUmur());
				plKorbanKecelakaanDao.save(korban);
			}
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> getKorbanByIdLaka(Map<String, Object> input,
			String idKecelakaan, String search) {
		List<Object[]> korbanLaka = plDataKecelakaanDao.getKorbanByIdLaka(idKecelakaan, search);
		List<PlKorbanKecelakaanDto> listDtos = new ArrayList<>();
		
		for(Object[] a : korbanLaka){
//			PlKorbanKecelakaan b = (PlKorbanKecelakaan) a[0];
			PlKorbanKecelakaanDto dto = new PlKorbanKecelakaanDto();
			
//			dto.setAlamat(b.getAlamat());
//			dto.setCreatedBy(b.getCreatedBy());
//			dto.setCreationDate(b.getCreationDate());
//			dto.setIdAngkutanKecelakaan(b.getIdAngkutanKecelakaan());
//			dto.setIdAngkutanPenanggung(b.getIdAngkutanPenanggung());
//			dto.setIdGuid(b.getIdGuid());
//			dto.setIdKecelakaan(b.getIdKecelakaan());
//			dto.setIdKorbanKecelakaan(b.getIdKorbanKecelakaan());
//			dto.setJenisIdentitas(b.getJenisIdentitas());
//			dto.setJenisKelamin(b.getJenisKelamin());
//			dto.setKodeJaminan(b.getKodeJaminan());
//			dto.setKodePekerjaan(b.getKodePekerjaan());
//			dto.setKodeSifatCidera(b.getKodeSifatCidera());
//			dto.setKodeStatusKorban(b.getKodeStatusKorban());
			dto.setNama((String)a[0]);
//			dto.setNoIdentitas(b.getNoIdentitas());
//			dto.setNoTelp(b.getNoTelp());
//			dto.setStatusNikah(b.getStatusNikah());
			dto.setUmur((BigDecimal) a[1]);
			
			dto.setCideraHighDesc((String)a[2]);
			dto.setStatusKorbanDesc((String)a[3]);
			dto.setPertanggunganDesc((String)a[4]);
			dto.setPekerjaanDesc((String)a[5]);
			dto.setCideraDesc((String)a[6]);
			dto.setIdKorbanKecelakaan((String)a[7]);
			dto.setNamaCidera((String)a[0]+" - "+(String)a[6]);
			listDtos.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDtos);

		return map;
	}

	@Override
	public int saveNihilKecelakaan(PlNihilKecelakaanDto plNihilKecelakaanDto) {
		try {
			PlNihilKecelakaan plNihilKecelakaan = convertToModel(plNihilKecelakaanDto);
			plNihilKecelakaanDao.save(plNihilKecelakaan);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			return CommonConstants.ERROR_REST_STATUS;
		}	}

	@Override
	public Map<String, Object> getAngkutanByIdAngkutan(
			Map<String, Object> input, String idAngkutan) {
		List<Object[]> angkutan = plDataKecelakaanDao.getAngkutanByIdAngkutan(idAngkutan);
		List<PlAngkutanKecelakaanDto> listDto = new ArrayList<>();
		
		for(Object[] a:angkutan){
			PlAngkutanKecelakaanDto dto = new PlAngkutanKecelakaanDto();
			
			
			dto.setNoPolisi((String) a[0]);
			dto.setNamaPengemudi((String)a[1]);
			dto.setNamaPemilik((String)a[2]);
			dto.setAlamatPengemudi((String)a[3]);
			dto.setAlamatPemilik((String)a[4]);
			dto.setTahunPembuatan((BigDecimal)a[5]);
			dto.setNoSimPengemudi((String)a[6]);
			dto.setMasaBerlakuSim((Date)a[7]);
			dto.setIdAngkutanKecelakaan((String)a[8]);
			
			dto.setJenisDesc((String)a[9]+" - "+(String)a[10]);
			dto.setStatusDesc((String)a[11]);
			dto.setSimDesc((String)a[12]);
			dto.setMerkDesc((String)a[13]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> getKorbanByIdKorban(Map<String, Object> input,
			String idKorban) {
		
		List<Object[]> korban = plDataKecelakaanDao.getKorbanByIdKorban(idKorban);
		List<PlKorbanKecelakaanDto> listDto = new ArrayList<>();
		
		for(Object[] a : korban){
			PlKorbanKecelakaanDto dto = new PlKorbanKecelakaanDto();
			dto.setNoIdentitas((String)a[0]);
			dto.setNama((String)a[1]);
			dto.setJenisKelamin((String)a[2]);
			dto.setUmur((BigDecimal)a[3]);
			dto.setAlamat((String)a[4]);
			dto.setNoTelp((String)a[5]);
			dto.setJenisIdentitasDesc((String)a[6]);
			dto.setPekerjaanDesc((String)a[7]);
			dto.setCideraDesc((String)a[8]);
			dto.setPenjaminDesc((String)a[9]+" - "+(String)a[10]);
			dto.setPosisiDesc((String)a[11]+" - "+(String)a[12]);
			dto.setStatusKorbanDesc((String)a[13]);
			dto.setStatusNikahDesc((String)a[14]);
			dto.setPertanggunganDesc((String)a[15]);
			dto.setKodeJaminan((String)a[16]);
			dto.setKodeProvinsi((String)a[17]);
			dto.setNamaProvinsi((String)a[18]);
			dto.setKodeKabkota((String)a[19]);
			dto.setNamaKabkota((String)a[20]);
			dto.setKodeCamat((String)a[21]);
			dto.setNamaCamat((String)a[22]);
			dto.setIdKecelakaan((String)a[23]);
			dto.setCideraHighDesc((String)a[24]);
			dto.setKodeSifatCidera((String)a[25]);
			dto.setPenjaminDesc((String)a[26]+" - "+(String)a[27]);
			dto.setJenisIdentitas((String)a[28]);
			dto.setIdAngkutanKecelakaan((String)a[29]);
			dto.setIdAngkutanPenanggung((String)a[30]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public int deleteAngkutan(PlAngkutanKecelakaanDto plAngkutanKecelakaanDto) {
		try{
			PlAngkutanKecelakaan plAngkutanKecelakaan = new PlAngkutanKecelakaan();
			plAngkutanKecelakaan.setIdAngkutanKecelakaan(plAngkutanKecelakaanDto.getIdAngkutanKecelakaan());
			plDataKecelakaanDao.deleteAngkutanByIdAngkutan(plAngkutanKecelakaan);
			return CommonConstants.OK_REST_STATUS;
		}catch(Exception e){
			return CommonConstants.ERROR_REST_STATUS;						
		}
	}

	@Override
	public int deleteKorban(PlKorbanKecelakaanDto plKorbanKecelakaanDto) {
		try{
			PlKorbanKecelakaan plKorbanKecelakaan = new PlKorbanKecelakaan();
			plKorbanKecelakaan.setIdKorbanKecelakaan(plKorbanKecelakaanDto.getIdKorbanKecelakaan());
			plDataKecelakaanDao.deleteKorbanByIdKorban(plKorbanKecelakaan);
			return CommonConstants.OK_REST_STATUS;
		}catch(Exception e){
			return CommonConstants.ERROR_REST_STATUS;						
		}
	}

	@Override
	public Map<String, Object> getKodeNoLaporan(Map<String, Object> input,
			String kodeInstansi) {
		
		List<String> kodeNoLap = plDataKecelakaanDao.getKodeNoLaporan(kodeInstansi);
		List<PlMappingPoldaDto> listDto = new ArrayList<>();
		
		for(String a : kodeNoLap){
			PlMappingPoldaDto dto = new PlMappingPoldaDto();
			dto.setKodeNoLaporan(a);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;
	}

	@Override
	public Map<String, Object> reportDataLaka1(Map<String, Object> input, String idKec) {
		List<Object[]> list = plDataKecelakaanDao.reportDataLaka1(idKec);
		List<PlDataKecelakaanDto> listDto = new ArrayList<>();
		
		for(Object[] o : list){
			PlDataKecelakaanDto dto = new PlDataKecelakaanDto();
			dto.setAsalBerkasDesc((String)o[0]);
			dto.setNamaPetugas((String)o[1]);
			dto.setTglLaporanPolisi((Date)o[2]);
			dto.setTglKejadian((Date)o[3]);
			dto.setDeskripsiLokasi((String)o[4]);
			dto.setKasusKecelakaanDesc((String)o[5]);
			dto.setDeskripsiKecelakaan((String)o[6]);
			dto.setCreatedBy((String)o[7]);
			dto.setLastUpdatedBy((String)o[8]);
			dto.setCreationDate((Date)o[9]);
			dto.setLastUpdatedDate((Date)o[10]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
		
	}

	@Override
	public Map<String, Object> reportDataLaka2(Map<String, Object> input,
			String idKec) {
		
		List<Object[]> list = plDataKecelakaanDao.reportDataLaka2(idKec);
		List<PlAngkutanKecelakaanDto> listDto = new ArrayList<>();
		for(Object[] o :list){
			PlAngkutanKecelakaanDto dto = new PlAngkutanKecelakaanDto();
			dto.setNoPolisi((String)o[0]);
			dto.setKodeJenis((String)o[1]);
			dto.setNamaPengemudi((String)o[2]);
			dto.setNamaPemilik((String)o[3]);
			dto.setStatusDesc((String)o[4]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
			
	}

	@Override
	public Map<String, Object> reportDataLaka3(Map<String, Object> input,
			String idKec) {
		
		List<Object[]> list = plDataKecelakaanDao.getKorbanFromLaka(idKec);
		List<PlKorbanKecelakaanDto> listDto = new ArrayList<>();
		
		for(Object[] o : list){
			PlKorbanKecelakaanDto dto = new PlKorbanKecelakaanDto();
			dto.setNamaJkUmur((String)o[0]);
			dto.setAlamat((String)o[1]);
			dto.setCideraDesc((String)o[2]);
			dto.setStatusKorbanDesc((String)o[3]);
			dto.setAngkutanKecelakaanDesc((String)o[4]);
			dto.setAngkutanPenanggungKecelakaanDesc((String)o[5]);
			dto.setJaminanDesc((String)o[6]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);

		return map;	
	}

	@Override
	public int deleteKorbanByIdLaka(PlDataKecelakaanDto plDataKecelakaanDto) {
		try{
			PlDataKecelakaan plDataKecelakaan = new PlDataKecelakaan();
			plDataKecelakaan.setIdKecelakaan(plDataKecelakaanDto.getIdKecelakaan());
			plDataKecelakaanDao.deleteKorbanByIdLaka(plDataKecelakaan);
			return CommonConstants.OK_REST_STATUS;
		}catch(Exception e){
			return CommonConstants.ERROR_REST_STATUS;						
		}
	}

	@Override
	public Map<String, Object> cekData(Map<String, Object> input,
			String tglKejadian, String tglLaporan, String kodeInstansi) {
			List<Object[]> list = plDataKecelakaanDao.cekData(tglKejadian, tglLaporan, kodeInstansi);
			List<PlDataKecelakaanDto> listDto = new ArrayList<>();
			for(Object[] o : list){
				PlDataKecelakaanDto dto = new PlDataKecelakaanDto();
				dto.setIdKecelakaan((String)o[0]);
				listDto.add(dto);
			}
			Map<String, Object> map = new HashMap<>();
			map.put("totalRecords", new Long(1));
			map.put("contentData", listDto);
			return map;	
	}
}
