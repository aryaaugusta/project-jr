package core.service.operasional.impl;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.DasiJrRefCodeDto;
import share.PlDataKecelakaanDto;
import share.PlNihilKecelakaanDto;
import share.PlRegisterSementaraDto;
import share.PlTindakLanjutDto;
import share.PlTlRDto;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.dao.PlDataKecelakaanDao;
import core.dao.PlInstansiDao;
import core.dao.PlJaminanDao;
import core.dao.PlPengajuanSantunanDao;
import core.dao.PlRegisterSementaraDao;
import core.dao.PlRumahSakitDao;
import core.dao.PlTindakLanjutDao;
import core.dao.PlTlRDao;
import core.dao.lov.LovDao;
import core.model.PlNihilKecelakaan;
import core.model.PlPengajuanSantunan;
import core.model.PlRegisterSementara;
import core.model.PlTindakLanjut;
import core.model.PlTlR;
import core.service.operasional.OpRegisterSementaraSvc;

@Service
@Transactional
public class OpRegisterSementaraSvcImpl implements OpRegisterSementaraSvc {

	@Autowired
	LovDao lovDao;

	@Autowired
	PlDataKecelakaanDao plDataKecelakaanDao;

	@Autowired
	PlRegisterSementaraDao plRegisterSementaraDao;

	@Autowired
	PlJaminanDao plJaminanDao;

	@Autowired
	PlInstansiDao mstPlInstansiDao;

	@Autowired
	PlRumahSakitDao mstPlRumahSakitDao;

	@Autowired
	PlTindakLanjutDao plTindakLanjutDao;

	@Autowired
	PlPengajuanSantunanDao plPengajuanSantunanDao;
	
	@Autowired
	PlTlRDao plTlRDao;

	@Autowired
	MapperFacade mapperFacade;

	private PlRegisterSementara convertToModel(PlRegisterSementaraDto obj) {
		return mapperFacade.map(obj, PlRegisterSementara.class);
	}
	
	private PlTindakLanjut convertToModel(PlTindakLanjutDto obj) {
		return mapperFacade.map(obj, PlTindakLanjut.class);
	}
	
	private PlTlR convertToModel(PlTlRDto obj) {
		return mapperFacade.map(obj, PlTlR.class);
	}


	DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	String noReg;

	@Override
	public Map<String, Object> loadJRRefCode(String rvDomain, String flag,
			String search) {
		// TODO Auto-generated method stub
		List<Object[]> refCodeList = lovDao
				.getJrRefCode(rvDomain, flag, search);
		List<DasiJrRefCodeDto> refCodeDtoList = new ArrayList<>();

		for (Object[] objects : refCodeList) {
			DasiJrRefCodeDto dto = new DasiJrRefCodeDto();
			dto.setRvLowValue((String) objects[0]);
			dto.setRvHighValue((String) objects[1]);
			dto.setRvMeaning((String) objects[2]);
			dto.setFlagEnable((String) objects[3]);
			dto.setRvAbbreviation((String) objects[4]);
			dto.setOrderSeq((BigDecimal) objects[5]);
			dto.setSifatCidera(dto.getRvLowValue() + " - " + dto.getRvMeaning());
			refCodeDtoList.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", refCodeDtoList);
		return map;
	}

	@Override
	public Map<String, Object> loadRegisterSementara(String kodeKantorJr,
			String regDay, String regMonth, String regYear, String namaKorban,
			String lakaDay, String lakaMonth, String lakaYear,
			String noLaporan, String noRegister, String asalBerkasFlag,
			String tindakLanjutFlag, String cideraKorban, String namaInstansi,
			String rvDomainCidera, String rvDomainTindakLanjut, String search) {
		List<Object[]> listData = plRegisterSementaraDao.getListData(
				kodeKantorJr, regDay, regMonth, regYear, namaKorban, lakaDay,
				lakaMonth, lakaYear, noLaporan, noRegister, asalBerkasFlag,
				tindakLanjutFlag, cideraKorban, namaInstansi, rvDomainCidera,
				rvDomainTindakLanjut, search);
		List<PlRegisterSementaraDto> listDto = new ArrayList<>();

		for (Object[] objects : listData) {
			PlRegisterSementaraDto dto = new PlRegisterSementaraDto();

			dto.setKodeKantorJr((String) objects[0]);
			dto.setNoRegister((String) objects[1]);
			dto.setTglKecelakaan((Date) objects[2]);
			dto.setTglRegister((Date) objects[3]);
			dto.setNamaKorban((String) objects[4]);
			dto.setKodeSifatCidera((String) objects[5]);
			dto.setNamaPemohon((String) objects[6]);
			dto.setTindakLanjut((String) objects[7]);
			dto.setTglTindakLanjutTL((Date) objects[8]);
			dto.setTlTerakhir((String) "(" + df.format(objects[8]) + ")-"
					+ objects[7]);
			dto.setIdKorbanKecelakaan((String) objects[9]);
			dto.setIdKecelakaanPS((String) objects[10]);
			dto.setAsalBerkas((String) objects[11]);
			dto.setHubunganKorban((String) objects[12]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> loadDataLaka(String kejadianStartDate,
			String kejadianEndDate, String namaKantorInstansi,
			String laporStartDate, String laporEndDate, String noLaporPolisi,
			String namaKorbanLaka, String kodeJaminan, String statusLP,
			String search, String idKorban, String idKecelakaan, String tipe) {
		// TODO Auto-generated method stub
		List<Object[]> list = plDataKecelakaanDao.getRegisterSementara(
				kejadianStartDate, kejadianEndDate, namaKantorInstansi,
				laporStartDate, laporEndDate, noLaporPolisi, namaKorbanLaka,
				kodeJaminan, statusLP, search, idKorban, idKecelakaan, tipe);
		List<PlDataKecelakaanDto> listDto = new ArrayList<>();

		for (Object[] objects : list) {
			PlDataKecelakaanDto dto = new PlDataKecelakaanDto();
			dto.setNoLaporanPolisi((String) objects[0]);
			dto.setTglKejadian((Date) objects[1]);
			dto.setNamaKorban((String) objects[2]);
			dto.setJaminan((String) objects[3]);
			dto.setStatusLaporanPolisi((String) objects[4]);
			dto.setUmur((BigDecimal) objects[5]);
			dto.setJenisKelamin((String) objects[6]);
			dto.setCidera((String) objects[7]);
			dto.setPenjamin((String) objects[8] + "-" + objects[9]);
			dto.setKendaraanPenjamin((String) objects[8]);
			dto.setKasus((String) objects[10]);
			dto.setDeskripsiKecelakaan((String) objects[11]);
			dto.setStatus((String) objects[12]);
			dto.setNamaInstansi((String) objects[13]);
			dto.setIdKecelakaan((String) objects[14]);
			dto.setDeskripsiLokasi((String) objects[15]);
			dto.setNoIdentitas((String) objects[16]);
			dto.setJenisIdentitas((String) objects[17]);
			dto.setAlamat((String) objects[18]);
			dto.setStatusNikah((String) objects[19]);
			dto.setNoTelp((String) objects[20]);
			dto.setKodeKantorJr((String) objects[21]);
			dto.setIdKorbanKecelakaan((String) objects[22]);
			dto.setKodeSifatCidera((String) objects[23]);
			dto.setKodeJaminan((String) objects[24]);
			dto.setLingkupJaminan((String) objects[25]);
			dto.setProvinsiDesc((String) objects[26]);
			dto.setKabKotaDesc((String) objects[27]);
			dto.setCamatDesc((String) objects[28]);
			dto.setKodeSifatCidera((String) objects[29]);
			dto.setCideraHighValue((String) objects[30]);
			dto.setTglLaporanPolisi((Date) objects[31]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> saveRegister(PlRegisterSementaraDto dto) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat thn = new SimpleDateFormat("YYYY");
		cal.setTime(dto.getTglRegister());
		String tahun = thn.format(dto.getTglRegister());
		String tl;
		String noRegist;
		String idTLReg = "";
		String noRegSearch = "";
		Map<String, Object> map = new HashMap<>();
		if (dto.getAsalBerkasFlag().equalsIgnoreCase("2")) {
			tl = "01";
		} else {
			tl = "00";
		}
		if (dto.getNoRegister() == null || dto.getNoRegister().isEmpty()
				|| dto.getNoRegister().equalsIgnoreCase("")) {
			System.out.println("aaaaaaaa  " + dto.getKodeKantorJr());
			System.out.println("bbbbbbbbbb    " + tahun);
			System.out.println("ccccccc    " + tl);
			noRegSearch = dto.getKodeKantorJr().substring(0, 2)
					+ dto.getKodeKantorJr().substring(3, 5) + "-" + tahun + "-"
					+ tl;
			try {
				List<PlRegisterSementara> listReg = plRegisterSementaraDao
						.findByKode(StringUtil.surroundString(
								StringUtil.nevl(noRegSearch, "%%"), "%"));
				System.out.println("size " + listReg.size());
				if (listReg.size() != 0) {
					for (PlRegisterSementara a : listReg) {
						if (a.getNoRegister() != null) {
							noRegist = a.getNoRegister();
							System.out.println("reg   " + a.getNoRegister());
							int count = Integer.valueOf(noRegist.substring(12,
									16)) + 1;
							setNoReg(noRegSearch + String.format("%04d", count));
						}
					}
				} else {
					setNoReg(noRegSearch + "0001");
				}
				idTLReg = getNoReg() + ".1";
			} catch (Exception e) {
				e.printStackTrace();
			}
			try {

				System.out.println(" no : " + idTLReg);
				PlRegisterSementara plReg = new PlRegisterSementara();
				plReg.setNoRegister(getNoReg());
				plReg.setKodeKantorJr(dto.getKodeKantorJr());
				plReg.setIdKorbanKecelakaan(dto.getIdKorbanKecelakaan());
				plReg.setAsalBerkasFlag(dto.getAsalBerkasFlag());
				plReg.setTglRegister(dto.getTglRegister());
				plReg.setNoRegister(dto.getNoRegister());
				plReg.setNamaPemohon(dto.getNamaPemohon());
				plReg.setKodeHubunganKorban(dto.getKodeHubunganKorban());
				plReg.setJenisIdentitas(dto.getJenisIdentitas());
				plReg.setNoIdentitas(dto.getNoIdentitas());
				plReg.setAlamatPemohon(dto.getAlamatPemohon());
				plReg.setTelpPemohon(dto.getTelpPemohon());
				plReg.setCideraKorban(dto.getCideraKorban());
				plReg.setKodeRumahSakit(dto.getKodeRumahSakit());
				plReg.setCreatedBy(dto.getCreatedBy());
				plReg.setCreatedDate(dto.getCreatedDate());
				plReg.setStatusKrbnRs(dto.getStatusKrbnRs());
				plReg.setJumlahPengajuan1(dto.getJumlahPengajuan1());
				plReg.setJumlahPengajuan2(dto.getJumlahPengajuan2());
				plReg.setJmlAmbl(dto.getJmlAmbl());
				plReg.setJmlP3k(dto.getJmlP3k());
				// plReg.setCideraKorban(dto.getKodeSifatCidera());
				plRegisterSementaraDao.save(plReg);

				PlTindakLanjut plTL = new PlTindakLanjut();
				plTL.setIdTlRegister(idTLReg);
				plTL.setIdTlRegister(dto.getIdTlRegisterTL());
				plTL.setNoRegister(getNoReg());
				// plTL.setNoRegister(dto.getNoRegisterTL());
				plTL.setTglTindakLanjut(dto.getTglTindakLanjutTL());
				plTL.setTindakLanjutFlag(dto.getTindakLanjutFlagTL());
				plTL.setCatatanTindakLanjut(dto.getCatatanTindakLanjutTL());
				plTL.setCreatedBy(dto.getCreatedByTL());
				plTL.setCreatedDate(dto.getCreatedDateTL());
				plTL.setPetugasSurvey(dto.getPetugasSurveyTL());
				plTL.setDilimpahkanKe(dto.getDilimpahkanKeTL());
				plTL.setNoSuratPanggilan(dto.getNoSuratPanggilanTL());
				plTL.setNoSuratJaminan(dto.getNoSuratJaminanTL());
				plTindakLanjutDao.save(plTL);

				map.put("status", CommonConstants.OK_REST_STATUS);
				map.put("noRegister", getNoReg());
				return map;
			} catch (Exception e) {
				e.printStackTrace();
				return new HashMap<>();
			}
		} else {
			setNoReg(StringUtil.surroundString(
					StringUtil.nevl((String) dto.getNoRegister(), "%%"), "%"));
			List<PlTindakLanjut> listTL = plTindakLanjutDao.findById2(noReg);
			System.out.println("size = " + listTL.size());
			if (listTL.size() != 0) {
				// for (PlTindakLanjut b : listTL) {
				//
				// if (b.equals(listTL.get(listTL.size()-1))) {
				// idTLReg = b.getIdTlRegister();
				// int count = Integer.valueOf(idTLReg.substring(17,18))+1;
				// idTLReg = b.getNoRegister()+"."+ count;
				// }
				// }
				PlTindakLanjut a = listTL.get(listTL.size() - 1);
				int count = Integer.valueOf(a.getIdTlRegister().substring(17,
						18)) + 1;
				idTLReg = a.getNoRegister() + "." + count;
			} else {
				idTLReg = dto.getNoRegister() + "." + "1";
			}
			try {
				// PlRegisterSementara plReg = new PlRegisterSementara();
				// plReg.setNoRegister(getNoReg());
				// plReg.setKodeKantorJr(dto.getKodeKantorJr());
				// plReg.setIdKorbanKecelakaan(dto.getIdKorbanKecelakaan());
				// plReg.setAsalBerkasFlag(dto.getAsalBerkasFlag());
				// plReg.setTglRegister(dto.getTglRegister());
				// // plReg.setNoRegister(dto.getNoRegister());
				// // plReg.setNamaPemohon(dto.getNamaPemohon());
				// // plReg.setKodeHubunganKorban(dto.getKodeHubunganKorban());
				// // plReg.setJenisIdentitas(dto.getJenisIdentitas());
				// // plReg.setNoIdentitas(dto.getNoIdentitas());
				// // plReg.setAlamatPemohon(dto.getAlamatPemohon());
				// // plReg.setTelpPemohon(dto.getTelpPemohon());
				// // plReg.setCideraKorban(dto.getCideraKorban());
				// // plReg.setKodeRumahSakit(dto.getKodeRumahSakit());
				// plReg.setCreatedBy(dto.getCreatedBy());
				// plReg.setCreatedDate(dto.getCreatedDate());
				// // plReg.setStatusKrbnRs(dto.getStatusKrbnRs());
				// plReg.setCideraKorban(dto.getKodeSifatCidera());
				// plRegisterSementaraDao.save(plReg);

				PlTindakLanjut plTL = new PlTindakLanjut();
				plTL.setIdTlRegister(idTLReg);
				plTL.setNoRegister(dto.getNoRegister());
				plTL.setTglTindakLanjut(dto.getTglTindakLanjutTL());
				plTL.setTindakLanjutFlag(dto.getTindakLanjutFlagTL());
				plTL.setCatatanTindakLanjut(dto.getCatatanTindakLanjutTL());
				plTL.setNoSuratPanggilan(null);
				plTL.setCreatedBy(dto.getCreatedByTL());
				plTL.setCreatedDate(dto.getCreatedDateTL());
				plTindakLanjutDao.save(plTL);

				map.put("status", CommonConstants.OK_REST_STATUS);
				map.put("noRegister", dto.getNoRegister());
				return map;
			} catch (Exception e) {
				e.printStackTrace();
				return new HashMap<>();
			}
		}

	}

	public String getNoReg() {
		return noReg;
	}

	public void setNoReg(String noReg) {
		this.noReg = noReg;
	}

	public void printTL() throws Exception {
		// XWPFDocument document = new XWPFDocument();
	}

	@Override
	public int saveRegisterPS(PlRegisterSementaraDto dto) {
		DateFormat month = new SimpleDateFormat("MM");
		DateFormat year = new SimpleDateFormat("YYYY");
		Calendar cal = Calendar.getInstance();
		cal.setTime(dto.getTglRegister());
		String tl;
		String noRegist;
		String noBerkas = "";
		String pertanggungan = "";
		String suplesi;
		int a = 0;
		String z = "";
		// set no berkas

		List<PlPengajuanSantunan> listPengajuan = plPengajuanSantunanDao
				.findPengajuanByKorban(dto.getIdKecelakaanPS(),
						dto.getIdKorbanKecelakaanPS());
		if (listPengajuan.size() != 0) {
			if (listPengajuan.size() > 0 && listPengajuan.size() < 10) {
				suplesi = "0" + listPengajuan.size();
			} else {
				suplesi = Integer.toString(listPengajuan.size());
			}
		} else {
			suplesi = "00";
		}
		if (dto.getLingkupJaminan().equalsIgnoreCase("33")) {
			pertanggungan = "1";
		} else if (dto.getLingkupJaminan().equalsIgnoreCase("34")) {
			pertanggungan = "2";
		}
		String urutan = "";

		String cek = pertanggungan + "-%-" + suplesi + "-"
				+ dto.getDiajukanDiPS().substring(0, 2) + "-00"
				+ month.format(dto.getTglPengajuanPS()) + "-"
				+ year.format(dto.getTglPengajuanPS());
		try {
			List<Object> cekNoBerkas = plPengajuanSantunanDao.findMaxId(cek);
			if (cekNoBerkas.size() == 0 || cekNoBerkas.get(0) == null) {
				z = "000";
			} else {
				for (Object objects : cekNoBerkas) {
					String no;
					no = (String) objects;
					a = Integer.valueOf(no.substring(2, 5) + 1);
					z = Integer.toString(a);

				}
			}
			noBerkas = pertanggungan + "-" + z + "-" + suplesi + "-"
					+ dto.getDiajukanDiPS().substring(0, 2) + "-00"
					+ month.format(dto.getTglPengajuanPS()) + "-"
					+ year.format(dto.getTglPengajuanPS());
			System.out.println("noBerkas = " + noBerkas);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// set no register
		if (dto.getAsalBerkasFlag().equalsIgnoreCase("2")) {
			tl = "01";
		} else {
			tl = "00";
		}
		String noRegSearch = dto.getKodeKantorJr().substring(0, 2)
				+ dto.getKodeKantorJr().substring(3, 5) + "-"
				+ year.format(dto.getTglRegister()) + "-" + tl;
		System.out.println("tglReg = " + dto.getTglRegister());

		System.out.println("noreg = " + noRegSearch);

		List<PlRegisterSementara> listReg = plRegisterSementaraDao
				.findByKode(noRegSearch);

		if (listReg.size() != 0) {
			for (PlRegisterSementara ac : listReg) {
				noRegist = ac.getNoRegister();
				if (noRegist != null) {
					int count = Integer.valueOf(noRegist.substring(12, 16)) + 1;
					setNoReg(noRegSearch + count);

				}
			}
		} else {
			setNoReg(noRegSearch + "0001");
		}

		try {
			PlRegisterSementara plReg = new PlRegisterSementara();
			plReg.setNoRegister(getNoReg());
			plReg.setKodeKantorJr(dto.getKodeKantorJr());
			plReg.setIdKorbanKecelakaan(dto.getIdKorbanKecelakaan());
			plReg.setAsalBerkasFlag(dto.getAsalBerkasFlag());
			plReg.setTglRegister(dto.getTglRegister());
			plReg.setNamaPemohon(dto.getNamaPemohon());
			plReg.setKodeHubunganKorban(dto.getKodeHubunganKorban());
			plReg.setJenisIdentitas(dto.getJenisIdentitas());
			plReg.setNoIdentitas(dto.getNoIdentitas());
			plReg.setAlamatPemohon(dto.getAlamatPemohon());
			plReg.setTelpPemohon(dto.getTelpPemohon());
			plReg.setKodeRumahSakit(dto.getKodeRumahSakit());
			plReg.setCreatedBy(dto.getCreatedBy());
			plReg.setCreatedDate(dto.getCreatedDate());
			plReg.setStatusKrbnRs(dto.getStatusKrbnRs());
			plReg.setCideraKorban(dto.getCideraKorban());
			plReg.setStatusKrbnRs(dto.getStatusKrbnRs());
			plReg.setJumlahPengajuan1(dto.getJumlahPengajuan1());
			plReg.setJumlahPengajuan2(dto.getJumlahPengajuan2());
			plReg.setJmlAmbl(dto.getJmlAmbl());
			plReg.setJmlP3k(dto.getJmlP3k());
			plRegisterSementaraDao.save(plReg);

			// pengajuan santunan
			PlPengajuanSantunan plPS = new PlPengajuanSantunan();
			plPS.setNoBerkas(noBerkas);
			plPS.setIdKecelakaan(dto.getIdKecelakaanPS());
			plPS.setIdKorbanKecelakaan(dto.getIdKorbanKecelakaanPS());
			plPS.setKodeJaminan(dto.getKodeJaminanPS());
			plPS.setDiajukanDi(dto.getDiajukanDiPS());
			plPS.setNamaPemohon(dto.getNamaPemohonPS());
			plPS.setAlamatPemohon(dto.getAlamatPemohonPS());
			plPS.setNoIdentitas(dto.getNoIdentitasPS());
			plPS.setJenisIdentitas(dto.getJenisIdentitasPS());
			plPS.setJumlahPengajuanLukaluka(dto.getJumlahPengajuanLukalukaPS());
			plPS.setJumlahPengajuanMeninggal(dto
					.getJumlahPengajuanMeninggalPS());
			plPS.setJumlahPengajuanPenguburan(dto
					.getJumlahPengajuanPenguburanPS());
			plPS.setCideraKorban(dto.getCideraKorbanPS());
			plPS.setKodeHubunganKorban(dto.getKodeHubunganKorbanPS());
			plPS.setTglPengajuan(dto.getTglPengajuanPS());
			plPS.setStatusProses("TL");
			plPS.setTglPenerimaan(new Date());
			plPS.setTransisiFlag("N");
			plPS.setFlagHasilSurvey("N");
			plPengajuanSantunanDao.save(plPS);

			return CommonConstants.OK_REST_STATUS;

		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}

	}

	@Override
	public Map<String, Object> loadTindakLanjut(String noRegister) {
		List<Object[]> list = new ArrayList<>();
		List<PlTindakLanjutDto> listDto = new ArrayList<>();
		list = plTindakLanjutDao.findByNoRegister(noRegister);
		int i = 1;
		for (Object[] a : list) {
			PlTindakLanjutDto dto = new PlTindakLanjutDto();

			dto = mapperFacade.map(a[0], PlTindakLanjutDto.class);
			dto.setNoUrut(i++);
			dto.setDeskripsiTL((String) a[1]);
			
			List<Object[]> b = plTindakLanjutDao.findLastTlByNoRegister(noRegister);
			for(Object[] o : b){
				if(dto.getIdTlRegister().equalsIgnoreCase((String)o[0])){
					dto.setEditButtonVisible(true);
				}else{
					dto.setEditButtonVisible(false);
				}
			}
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;

	}

	@Override
	public int saveTindakLanjut(PlTindakLanjutDto dto){
		try {
			PlTindakLanjut plTL = convertToModel(dto);
			plTindakLanjutDao.save(plTL);
			
//			if(plTL.getTindakLanjutFlag().equalsIgnoreCase("5")){
//				PlTlR plTlRs = new PlTlR();
//				plTlRs.setIdJaminan(dto.getIdJaminanTlRs()==null?"":dto.getIdJaminanTlRs());
//				plTlRs.setNoRegister(dto.getNoRegisterTlRs()==null?"":dto.getNoRegisterTlRs());
//				plTlRs.setKodeKantorJr(dto.getKodeKantorJrTlRs()==null?"":dto.getKodeKantorJrTlRs());
//				plTlRs.setNoSuratJaminan(dto.getNoSuratJaminanTlRs()==null?"":dto.getNoSuratJaminanTlRs());
//				plTlRs.setKodeRumahSakit(dto.getKodeRumahSakitTlRs()==null?"":dto.getKodeRumahSakitTlRs());
//				plTlRs.setTglMasukRs(dto.getTglMasukRsTlRs()==null?null:dto.getTglMasukRsTlRs());
//				plTlRs.setJumlahPengajuan1(dto.getJumlahPengajuan1TlRs()==null?new BigDecimal(0):dto.getJumlahPengajuan1TlRs());
//				plTlRs.setJmlAmbl(dto.getJmlAmblTlRs()==null?new BigDecimal(0):dto.getJmlAmblTlRs());
//				plTlRs.setJmlP3k(dto.getJmlP3kTlRs()==null?new BigDecimal(0):dto.getJmlP3kTlRs());
//				plTlRs.setJmlPengajuanSementara(dto.getJmlPengajuanSementaraTlRs()==null?new BigDecimal(0):dto.getJmlPengajuanSementaraTlRs());
//				plTlRs.setJmlAmblSementara(dto.getJmlAmblSementaraTlRs()==null?new BigDecimal(0):dto.getJmlAmblSmtTlRs());
//				plTlRs.setJmlP3kSementara(dto.getJmlP3kSementaraTlRs()==null?new BigDecimal(0):dto.getJmlP3kSementaraTlRs());
//				plTlRs.setFlagBayar(dto.getFlagBayarTlRs()==null?"":dto.getFlagBayarTlRs());
//				plTlRs.setFlagBayarAwal(dto.getFlagBayarAwalTlRs()==null?"":dto.getFlagBayarAwalTlRs());
//				plTlRs.setJenisTagihan(dto.getJenisTagihanTlRs()==null?"":dto.getJenisTagihanTlRs());
//				plTlRs.setOtorisasiAwal(dto.getOtorisasiAwalTlRs()==null?"":dto.getOtorisasiAwalTlRs());
//				plTlRs.setOtorisasiAjuSmt(dto.getOtorisasiAjuSmtTlRs()==null?"":dto.getOtorisasiAjuSmtTlRs());
//				plTlRs.setFlagLanjutan(dto.getFlagLanjutanTlRs()==null?"":dto.getFlagLanjutanTlRs());
//				plTlRs.setFlagLimpahan(dto.getFlagLimpahanTlRs()==null?"":dto.getFlagLimpahanTlRs());
//				plTlRs.setCreatedBy(dto.getCreatedByTlRs()==null?"":dto.getCreatedByTlRs());
//				plTlRs.setCreatedDate(dto.getCreatedDateTlRs()==null?null:dto.getCreatedDateTlRs());
//				plTlRDao.save(plTlRs);
//			}
			
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public int deleteTindakLanjut(String idTLReg) {
		try {
			plTindakLanjutDao.deleteByID(idTLReg);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> saveTLforRS(PlRegisterSementaraDto dto) {
		return null;
	}

	// added by luthfi
	@Override
	public Map<String, Object> findRegisterByIdKorban(
			Map<String, Object> input, String idKecelakaan) {

		List<PlRegisterSementara> list = plRegisterSementaraDao
				.findRegisterByIdKorban(idKecelakaan);
		List<PlRegisterSementaraDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(list, PlRegisterSementaraDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	// added by Luthfi
	@Override
	public Map<String, Object> findRegisterByNoRegister(
			Map<String, Object> input, String noReg) {
		List<Object[]> registerSementara = plRegisterSementaraDao
				.findOneByNoRegister(noReg);
		List<PlRegisterSementaraDto> listDto = new ArrayList<>();

		for (Object[] a : registerSementara) {
			PlRegisterSementaraDto dto = new PlRegisterSementaraDto();
			dto.setKodeKantorJr((String) a[0]);
			dto.setNoRegister((String) a[1]);
			dto.setTglKecelakaan((Date) a[2]);
			dto.setTglRegister((Date) a[3]);
			dto.setCideraDesc((String) a[4]);
			dto.setNamaKorban((String) a[5]);
			dto.setNamaPemohon((String) a[6]);
			dto.setTglTindakLanjutTL((Date) a[7]);
			dto.setTindakLanjut((String) a[8]);
			listDto.add(dto);
		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public int deleteRegister(PlRegisterSementaraDto plRegisterSementaraDto) {
		try {
			PlRegisterSementara plRegisterSementara = new PlRegisterSementara();
			plRegisterSementara.setNoRegister(plRegisterSementaraDto
					.getNoRegister());
			plRegisterSementaraDao.deleteRegister(plRegisterSementara);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> findIndexGLRS(String kodeKantor,
			String jenisTgl, String tglAwal, String tglAkhir, String kodeRS,
			String flagBayar) {
		List<Object[]> listData = plRegisterSementaraDao.findMonitoringGLRS(
				kodeKantor, jenisTgl, tglAwal, tglAkhir, kodeRS, flagBayar);
		List<PlRegisterSementaraDto> listDto = new ArrayList<>();
		for (Object[] a : listData) {
			PlRegisterSementaraDto dto = new PlRegisterSementaraDto();
			dto.setKodeKantorJr((String) a[0]);
			dto.setNoRegister((String) a[1]);
			dto.setTglKejadian((String) a[2]);
			dto.setTglRegisterGL((String) a[3]);
			dto.setNamaKorban((String) a[4]);
			dto.setCideraDesc((String) a[5]);
			dto.setNoSuratJaminan((String) a[6]);
			dto.setJmlPengajuan((BigDecimal) a[7]);
			dto.setBerkasSS((String) a[8]);
			dto.setNamaRS((String) a[9]);
			dto.setAlamatRS((String) a[10]);
			dto.setIdKorbanKecelakaan((String) a[11]);
			dto.setIdJaminan((String) a[12]);
			dto.setFlagBayar((String) a[13]);
			listDto.add(dto);

		}

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findTindakLanjutByNoRegister(
			Map<String, Object> input, String noReg) {
		List<PlTindakLanjut> list = plRegisterSementaraDao
				.findTlByNoRegister(noReg);
		List<PlTindakLanjutDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(list, PlTindakLanjutDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;

	}

	@Override
	public int saveRegister2(PlRegisterSementaraDto dto) {

		try {
			PlRegisterSementara plReg = new PlRegisterSementara();
			plReg.setNoRegister(dto.getNoRegister());
			plReg.setKodeKantorJr(dto.getKodeKantorJr());
			plReg.setIdKorbanKecelakaan(dto.getIdKorbanKecelakaan());
			plReg.setAsalBerkasFlag(dto.getAsalBerkasFlag());
			plReg.setTglRegister(dto.getTglRegister());
			plReg.setNoRegister(dto.getNoRegister());
			plReg.setNamaPemohon(dto.getNamaPemohon());
			plReg.setKodeHubunganKorban(dto.getKodeHubunganKorban());
			plReg.setJenisIdentitas(dto.getJenisIdentitas());
			plReg.setNoIdentitas(dto.getNoIdentitas());
			plReg.setAlamatPemohon(dto.getAlamatPemohon());
			plReg.setTelpPemohon(dto.getTelpPemohon());
			plReg.setCideraKorban(dto.getCideraKorban());
			plReg.setKodeRumahSakit(dto.getKodeRumahSakit());
			plReg.setCreatedBy(dto.getCreatedBy());
			plReg.setCreatedDate(dto.getCreatedDate());
			plReg.setStatusKrbnRs(dto.getStatusKrbnRs());
			plReg.setJumlahPengajuan1(dto.getJumlahPengajuan1());
			plReg.setJumlahPengajuan2(dto.getJumlahPengajuan2());
			plReg.setJmlAmbl(dto.getJmlAmbl());
			plReg.setJmlP3k(dto.getJmlP3k());
			// plReg.setCideraKorban(dto.getKodeSifatCidera());
			plRegisterSementaraDao.save(plReg);

			PlTindakLanjut plTL = new PlTindakLanjut();
			plTL.setIdTlRegister(dto.getIdTlRegisterTL());
			plTL.setIdTlRegister(dto.getIdTlRegisterTL());
			plTL.setNoRegister(dto.getNoRegisterTL());
			// plTL.setNoRegister(dto.getNoRegisterTL());
			plTL.setTglTindakLanjut(dto.getTglTindakLanjutTL());
			plTL.setTindakLanjutFlag(dto.getTindakLanjutFlagTL());
			plTL.setCatatanTindakLanjut(dto.getCatatanTindakLanjutTL());
			plTL.setCreatedBy(dto.getCreatedByTL());
			plTL.setCreatedDate(dto.getCreatedDateTL());
			plTL.setPetugasSurvey(dto.getPetugasSurveyTL());
			plTL.setDilimpahkanKe(dto.getDilimpahkanKeTL());
			plTL.setNoSuratPanggilan(dto.getNoSuratPanggilanTL());
			plTL.setNoSuratJaminan(dto.getNoSuratJaminanTL());
			plTindakLanjutDao.save(plTL);
			
			if(dto.getTindakLanjutFlagTL().equalsIgnoreCase("5")){
				PlTlR plTlRs = new PlTlR();
				plTlRs.setIdJaminan(dto.getIdJaminanTlRs()==null?"":dto.getIdJaminanTlRs());
				plTlRs.setNoRegister(dto.getNoRegisterTlRs()==null?"":dto.getNoRegisterTlRs());
				plTlRs.setKodeKantorJr(dto.getKodeKantorJrTlRs()==null?"":dto.getKodeKantorJrTlRs());
				plTlRs.setNoSuratJaminan(dto.getNoSuratJaminanTlRs()==null?"":dto.getNoSuratJaminanTlRs());
				plTlRs.setKodeRumahSakit(dto.getKodeRumahSakitTlRs()==null?"":dto.getKodeRumahSakitTlRs());
				plTlRs.setTglMasukRs(dto.getTglMasukRsTlRs()==null?null:dto.getTglMasukRsTlRs());
				plTlRs.setJumlahPengajuan1(dto.getJumlahPengajuan1TlRs()==null?new BigDecimal(0):dto.getJumlahPengajuan1TlRs());
				plTlRs.setJmlAmbl(dto.getJmlAmblTlRs()==null?new BigDecimal(0):dto.getJmlAmblTlRs());
				plTlRs.setJmlP3k(dto.getJmlP3kTlRs()==null?new BigDecimal(0):dto.getJmlP3kTlRs());
				plTlRs.setJmlPengajuanSementara(dto.getJmlPengajuanSementaraTlRs()==null?new BigDecimal(0):dto.getJmlPengajuanSementaraTlRs());
				plTlRs.setJmlAmblSementara(dto.getJmlAmblSementaraTlRs()==null?new BigDecimal(0):dto.getJmlAmblSmtTlRs());
				plTlRs.setJmlP3kSementara(dto.getJmlP3kSementaraTlRs()==null?new BigDecimal(0):dto.getJmlP3kSementaraTlRs());
				plTlRs.setFlagBayar(dto.getFlagBayarTlRs()==null?"":dto.getFlagBayarTlRs());
				plTlRs.setFlagBayarAwal(dto.getFlagBayarAwalTlRs()==null?"":dto.getFlagBayarAwalTlRs());
				plTlRs.setJenisTagihan(dto.getJenisTagihanTlRs()==null?"":dto.getJenisTagihanTlRs());
				plTlRs.setOtorisasiAwal(dto.getOtorisasiAwalTlRs()==null?"":dto.getOtorisasiAwalTlRs());
				plTlRs.setOtorisasiAjuSmt(dto.getOtorisasiAjuSmtTlRs()==null?"":dto.getOtorisasiAjuSmtTlRs());
				plTlRs.setFlagLanjutan(dto.getFlagLanjutanTlRs()==null?"":dto.getFlagLanjutanTlRs());
				plTlRs.setFlagLimpahan(dto.getFlagLimpahanTlRs()==null?"":dto.getFlagLimpahanTlRs());
				plTlRs.setCreatedBy(dto.getCreatedByTlRs()==null?"":dto.getCreatedByTlRs());
				plTlRs.setCreatedDate(dto.getCreatedDateTlRs()==null?null:dto.getCreatedDateTlRs());
				plTlRDao.save(plTlRs);
			}
			
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> findOneRegister(Map<String, Object> input,
			String noReg) {
		
		List<PlRegisterSementara> list = plRegisterSementaraDao.findByKode(noReg);
		List<PlRegisterSementaraDto> listDto = new ArrayList<>();

		listDto = mapperFacade.mapAsList(list, PlRegisterSementaraDto.class);

		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public int saveRegisterSementara(PlRegisterSementaraDto dto) {
		try {
			PlRegisterSementara plReg = new PlRegisterSementara();
			plReg.setNoRegister(dto.getNoRegister());
			plReg.setKodeKantorJr(dto.getKodeKantorJr());
			plReg.setIdKorbanKecelakaan(dto.getIdKorbanKecelakaan());
			plReg.setAsalBerkasFlag(dto.getAsalBerkasFlag());
			plReg.setTglRegister(dto.getTglRegister());
			plReg.setNoRegister(dto.getNoRegister());
			plReg.setNamaPemohon(dto.getNamaPemohon());
			plReg.setKodeHubunganKorban(dto.getKodeHubunganKorban());
			plReg.setJenisIdentitas(dto.getJenisIdentitas());
			plReg.setNoIdentitas(dto.getNoIdentitas());
			plReg.setAlamatPemohon(dto.getAlamatPemohon());
			plReg.setTelpPemohon(dto.getTelpPemohon());
			plReg.setCideraKorban(dto.getCideraKorban());
			plReg.setKodeRumahSakit(dto.getKodeRumahSakit());
			plReg.setTglMasukRs(dto.getTglMasukRs());
			plReg.setCreatedBy(dto.getCreatedBy());
			plReg.setCreatedDate(dto.getCreatedDate());
			plReg.setLastUpdatedBy(dto.getLastUpdatedBy());
			plReg.setLastUpdatedDate(dto.getLastUpdatedDate());
			plReg.setStatusKrbnRs(dto.getStatusKrbnRs());
			plReg.setJumlahPengajuan1(dto.getJumlahPengajuan1());
			plReg.setJumlahPengajuan2(dto.getJumlahPengajuan2());
			plReg.setJmlAmbl(dto.getJmlAmbl());
			plReg.setJmlP3k(dto.getJmlP3k());
			plRegisterSementaraDao.save(plReg);

			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}

	@Override
	public Map<String, Object> plTlRsByNoSuratJaminan(
			Map<String, Object> input, String noSuratJaminan) {
		
		List<PlTlR> list = plTlRDao.getPlTlRsByNoSuratJaminan(noSuratJaminan);
		List<PlTlRDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(list, PlTlRDto.class);
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;		
	}

	@Override
	public Map<String, Object> plTlRsByNoRegister(Map<String, Object> input,
			String noRegister) {
		List<PlTlR> list = plTlRDao.getPlTlRsByNoRegister(noRegister);
		List<PlTlRDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(list, PlTlRDto.class);
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;			}

	@Override
	public Map<String, Object> getIndexGLRS(String kodeKantor, String jenisTgl,
			String tglAwal, String tglAkhir, String kodeRS, String flagBayar) {
		
		List<Object[]> list = plRegisterSementaraDao.getMonitoringGLRS(kodeKantor, jenisTgl, tglAwal, tglAkhir, kodeRS, flagBayar);
		List<PlTlRDto> listDto = new ArrayList<>(); 
		
		for(Object[] o : list){
			PlTlRDto dto = new PlTlRDto();
			dto.setKodeKantorJr((String)o[0]);
			dto.setNamaRs((String)o[1]);
			dto.setNoRegister((String)o[2]);
			dto.setTglLaka((Date)o[3]);
			dto.setNamaKorban((String)o[4]);
			dto.setNoSuratJaminan((String)o[5]);
			dto.setTglSuratJaminan((Date)o[6]);
			dto.setJmlPengajuan((BigDecimal)o[7]==null?new BigDecimal(0):(BigDecimal)o[7]);
			dto.setJmlDigunakan((BigDecimal)o[8]==null?new BigDecimal(0):(BigDecimal)o[8]);
			dto.setStatusJaminan((String)o[9]);
			dto.setIdJaminan((String)o[10]);
			dto.setIdKorbanKecelakaan((String)o[11]);
			dto.setIdLaka((String)o[12]);
			dto.setAsalBerkasDesc((String)o[13]);
			dto.setLokasiPengajuanDesc((String)o[14]);
			dto.setTglRegister((Date)o[15]);
			dto.setFlagBayar((String)o[16]);
			dto.setOtorisasiAwal((String)o[17]);
			dto.setOtorisasiAjuSmt((String)o[18]);
			if(dto.getOtorisasiAwal().equalsIgnoreCase("1")){
				dto.setCetakVisible(true);
			}else{
				dto.setCetakVisible(false);
			}
			if(dto.getFlagBayar().equalsIgnoreCase("0")){
				dto.setJaminanOption("Selesaikan Jaminan");
			}else{
				dto.setJaminanOption("Buat Jaminan Lanjutan");
			}
			System.err.println("luthfi88 "+dto.isCetakVisible());
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;			}

	@Override
	public Map<String, Object> findTindakLanjutByIdTl(
			Map<String, Object> input, String idTl) {
		
		List<PlTindakLanjut> list = plTindakLanjutDao.findById2(idTl);
		List<PlTindakLanjutDto> listDto = new ArrayList<>();
		
		for(PlTindakLanjut a : list){
			PlTindakLanjutDto dto = new PlTindakLanjutDto();
			dto = mapperFacade.map(a, PlTindakLanjutDto.class);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> dataCetakTl1(Map<String, Object> input,
			String noRegister) {
		
		List<Object[]> list = plRegisterSementaraDao.dataCetakTl1(noRegister);
		List<PlRegisterSementaraDto> listDto = new ArrayList<>();
		for(Object[] o : list){
			PlRegisterSementaraDto dto = new PlRegisterSementaraDto();
			dto.setNoRegister((String)o[0]);
			dto.setTglRegister((Date)o[1]);
			dto.setNamaKorban((String)o[2]);
			dto.setCideraKorban((String)o[3]);
			dto.setAsalBerkas((String)o[4]);
			dto.setNamaPemohon((String)o[5]);
			dto.setHubunganKorban((String)o[6]);
			dto.setTglKejadianLaka((Date)o[7]);
			dto.setTglLaporanPolisiLaka((Date)o[8]);
			dto.setNoLaporanPolisiLaka((String)o[9]);
			dto.setNamaInstansi((String)o[10]);
			dto.setKasusLaka((String)o[11]);
			dto.setStatusKorbanLaka((String)o[12]);
			dto.setPenjamin((String)o[13]);
			dto.setDeskripsiLokasiLaka((String)o[14]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	}
