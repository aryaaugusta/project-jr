package core.service.operasional.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import share.PlPengajuanSantunanDto;
import core.dao.PlPengajuanSantunanDao;
import core.service.operasional.OpVerifikasiSvc;

@Service
@Transactional
public class OpVerifikasiSvcImpl implements OpVerifikasiSvc {
	
	@Autowired
	PlPengajuanSantunanDao plPengajuanSantunanDao;

	@Override
	public Map<String, Object> getListIndexVerifikasi(
			Map<String, Object> input, String pilihPengajuan, String tglPengajuan, String noBerkas,
			String search) {

		List<Object[]> verifikasi = plPengajuanSantunanDao.getListVerifikasi(
				pilihPengajuan,tglPengajuan, noBerkas, search);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();

		for (Object[] a : verifikasi) {
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setNoBerkas((String) a[0]);
			dto.setStatusProses((String) a[1]);
			dto.setTglPenerimaan((Date) a[2]);
			dto.setTglPengajuan((Date) a[3]);
			dto.setNama((String) a[4]);
			dto.setAlamatKorban((String) a[5]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

}
