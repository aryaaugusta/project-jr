package core.service.operasional.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.util.CommonConstants;
import share.PlBerkasPengajuanDto;
import core.dao.PlBerkasPengajuanDao;
import core.model.PlBerkasEcmsH;
import core.model.PlBerkasPengajuan;
import core.service.operasional.PlBerkasPengajuanSvc;

@Service
@Transactional
public class PlBerkasPengajuanSvcImpl implements PlBerkasPengajuanSvc{
	
	@Autowired
	PlBerkasPengajuanDao plBerkasPengajuanDao;

	@Override
	public int saveBerkasPengajuan(PlBerkasPengajuanDto plBerkasPengajuanDto) {
		try {
			PlBerkasPengajuan plBerkasPengajuan = new PlBerkasPengajuan();
			plBerkasPengajuan.setNoBerkas(plBerkasPengajuanDto.getNoBerkas());
			plBerkasPengajuan.setKodeBerkas(plBerkasPengajuanDto.getKodeBerkas());
			plBerkasPengajuan.setAbsahFlag(plBerkasPengajuanDto.getAbsahFlag());
			plBerkasPengajuan.setCreatedBy(plBerkasPengajuanDto.getCreatedBy());
			plBerkasPengajuan.setCreationDate(plBerkasPengajuanDto.getCreationDate());
			plBerkasPengajuan.setDiterimaFlag(plBerkasPengajuanDto.getDiterimaFlag());
			plBerkasPengajuan.setIdGuid(plBerkasPengajuanDto.getIdGuid());
			plBerkasPengajuan.setLastUpdatedBy(plBerkasPengajuanDto.getLastUpdatedBy());
			plBerkasPengajuan.setLastUpdatedDate(plBerkasPengajuanDto.getLastUpdatedDate());
			plBerkasPengajuan.setTglTerimaBerkas(plBerkasPengajuanDto.getTglTerimaBerkas());
			plBerkasPengajuanDao.save(plBerkasPengajuan);
			return CommonConstants.OK_REST_STATUS;
		} catch (Exception e) {
			e.printStackTrace();
			return CommonConstants.ERROR_REST_STATUS;
		}
	}
	
	

}
