package core.service.operasional.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import common.util.CommonConstants;
import share.FndBankDto;
import share.PlAdditionalDescDto;
import share.PlPengajuanRsDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import share.PlRekeningRDto;
import core.dao.PlAdditionalDescDao;
import core.dao.PlPengajuanRsDao;
import core.dao.PlPengajuanSantunanDao;
import core.dao.PlPenyelesaianRSDao;
//import core.dao.PlPenyelesaianRSDao;
import core.dao.PlPenyelesaianSantunanDao;
import core.dao.lov.LovDao;
import core.model.PlAdditionalDesc;
import core.model.PlPengajuanR;
import core.model.PlPengajuanSantunan;
import core.model.PlPenyelesaianR;
import core.model.PlPenyelesaianSantunan;
import core.model.PlRekeningR;
import core.service.operasional.OpPenyelesaianPengajuanSvc;

@Service
@Transactional
public class OpPenyelesaianPengajuanSvcImpl implements OpPenyelesaianPengajuanSvc{

	@Autowired
	PlPengajuanSantunanDao pengajuanSantunanDao;
	
	@Autowired
	PlPenyelesaianSantunanDao penyelesaianSantunanDao;
	
	@Autowired
	PlPenyelesaianRSDao penyelesaianRSDao;
	
	@Autowired
	PlPengajuanRsDao plPengajuanRsDao;

	@Autowired
	LovDao lovDao;
	
	@Autowired
	PlAdditionalDescDao additionalDescDao;
	
	@Autowired
	MapperFacade mapperFacade;

	@Override
	public Map<String, Object> indexPenyelesaianPengajuan(String tglPengajuan,
			String noBerkas, String search, String jenisPengajuan, String dateFlag) {
		// TODO Auto-generated method stub
		List<Object[]> listPengajuan = pengajuanSantunanDao.findForPenyelesaianPengajuan(tglPengajuan, noBerkas, search, jenisPengajuan, dateFlag);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		for (Object[] objects : listPengajuan) {
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setNoBerkas((String)objects[0]);
			dto.setNamaKorban((String)objects[1]);
			dto.setCideraKorban((String)objects[2]);
			dto.setKodePengajuan((String)objects[3]);
			dto.setTglPenyelesaianStr((String)objects[4]);
			dto.setStatusLaporanPolisi((String)objects[5]);
			dto.setTglKejadianStr((String)objects[6]);
			dto.setKesimpulanSementara((String)objects[7]);
			dto.setKesimpulanSementaraDesc((String)objects[8]);
			dto.setJaminanPembayaran((String)objects[9]);
			dto.setJaminaanPembayaranDesc((String)objects[10]);
			dto.setOtorisasiFlag((String)objects[11]);
			dto.setOtorisasiDesc((String)objects[12]);
			dto.setStatusProses((String)objects[13]);
			dto.setUrutProses((String)objects[14]);
			dto.setStatusProsesDesc((String)objects[15]);
			dto.setKodeKantorDilimpahkan((String)objects[16]);
			dto.setNamaKantorDilimpahkan((String)objects[17]);
			dto.setHubKorbanDesc((String)objects[18]);
			dto.setJenisPembayaranPenyelesaian((String)objects[19]);
			dto.setTglProsesPenyelesaian((Date)objects[20]);
			dto.setJumlahDibayarLukalukaPenyelesaian((BigDecimal)objects[21]);
			dto.setJmlByrAmblPenyelesaian((BigDecimal)objects[22]);
			dto.setJmlByrP3kPenyelesaian((BigDecimal)objects[23]);
			dto.setNoBpk((BigDecimal)objects[24]);
			dto.setTglPembuatanBpkPenyelesaian((Date)objects[25]);
			dto.setNoSuratPenyelesaianPenyelesaian((String)objects[26]);
			dto.setKodeBank((String)objects[27]);
			dto.setNoRekeningPenyelesaian((String)objects[28]);
			dto.setNamaRekeningAdd((String)objects[29]);
			dto.setJenisRek((String)objects[30]);
			dto.setPembayaranDes((String)objects[31]);
			dto.setKodeRs((String)objects[32]);
			dto.setIdRekRsAdd((String)objects[33]);
			dto.setAlamatKorban((String)objects[34]);
			dto.setTglPengajuan((Date)objects[35]);
			dto.setJumlahDibayarMeninggalPenyelesaian((BigDecimal)objects[36]);
			dto.setDeskripsiLokasi((String)objects[37]);
			dto.setDiajukanDi((String)objects[38]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> detailPenyelesaianPengajuan(String tglPengajuan,
			String noBerkas, String search, String jenisPengajuan, String dateFlag) {
		// TODO Auto-generated method stub
		List<Object[]> list = pengajuanSantunanDao.findForPenyelesaianPengajuanDetail(tglPengajuan, noBerkas, search, jenisPengajuan, dateFlag);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();

		for (Object[] a : list) {
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setNoBerkas((String)a[0]);
			dto.setNamaKorban((String)a[1]);
			dto.setStatusProsesMeaning((String)a[2]);
			dto.setPenyelesaian((String)a[3]);
			dto.setKesimpulan((String)a[4]);
			dto.setPembayaranDes((String)a[5]);
			dto.setTglPengajuan((Date)a[6]);
			dto.setJumlahPengajuanMeninggal((BigDecimal)a[7]);
			dto.setJumlahPengajuanLukaluka((BigDecimal)a[8]);
			dto.setJumlahPengajuanPenguburan((BigDecimal)a[9]);
			dto.setJmlPengajuanAmbl((BigDecimal)a[10]);
			dto.setJmlPengajuanP3k((BigDecimal)a[11]);
			dto.setAlamatKorban((String)a[12]);
			dto.setCideraKorban((String)a[13]);
			dto.setTglPenyelesaian((Date)a[14]);
			dto.setDilimpahkanKe((String)a[15]);
			dto.setStatusProses((String)a[16]);
			dto.setOtorisasiFlag((String)a[17]);
			dto.setKesimpulanSementara((String)a[18]);
			dto.setJaminanPembayaran((String)a[19]);
			dto.setNamaKantorDilimpahkan((String)a[20]);
			
			dto.setJaminanDesc((String)a[21]);
			dto.setTglKejadian((Date)a[22]);
			dto.setDeskripsiLokasi((String)a[23]);
			dto.setNoPolisi((String)a[24]);
			dto.setJenisKendaraanDesc((String)a[25]);
			dto.setUmur((BigDecimal)a[26]);
			dto.setNamaPemohon((String)a[27]);
			dto.setAlamatPemohon((String)a[28]);
			dto.setHubKorbanDesc((String)a[29]);
			dto.setCideraDes((String)a[30]);
			dto.setNamaInstansi((String)a[31]);
			dto.setTglLaporanPolisi((Date)a[32]);
			dto.setNoLaporanPolisi((String)a[33]);
			dto.setKasusKecelakaan((String)a[34]);
			dto.setStatusKroban((String)a[35]);
			dto.setSifatLakaDesc((String)a[36]);
			dto.setGolonganKendaraan((String)a[37]);
			dto.setNoIdentitasPemohon((String)a[38]);
			dto.setNoTelpPemohon((String)a[39]);
			dto.setNoIdentitasKorban((String)a[40]);
			dto.setNoTelpKorban((String)a[41]);
			dto.setKodeRs((String)a[42]);
			dto.setJumlahPengajuanLuka2RS((BigDecimal)a[43]);
			dto.setNamaRS((String)a[44]);
			dto.setIdKorbanKecelakaan((String)a[45]);
			dto.setAlamatRS((String)a[46]);
			if (dto.getStatusProsesMeaning().equalsIgnoreCase("Berkas Selesai")) {
				dto.setBerkasSelesai(true);
			} else {
				dto.setBerkasSelesai(false);
			}
			listDto.add(dto);

		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;	}

	@Override
	public Map<String, Object> savePenyelesaian(PlPengajuanSantunanDto dto) {
		Map<String, Object> map = new HashMap<>();
		PlPengajuanSantunan pengajuan = new PlPengajuanSantunan();
		PlPenyelesaianSantunan penyelesaian= new PlPenyelesaianSantunan();
		PlAdditionalDesc additional = new PlAdditionalDesc();
		pengajuan = pengajuanSantunanDao.findOneByNoBerkas(dto.getNoBerkas());
		PlPenyelesaianR penyelesaianRS = new PlPenyelesaianR();
		PlPenyelesaianR getPenyelesaianRS = penyelesaianRSDao.getOnebyId(dto.getKodeRs()+"."+dto.getNoBerkas());
		try {
			//pengajuan
			pengajuan.setNoBerkas(dto.getNoBerkas());
			pengajuan.setStatusProses(dto.getStatusProses());
			pengajuan.setTglPenyelesaian(dto.getTglPenyelesaian());
			pengajuan.setLastUpdatedBy(dto.getLastUpdatedBy());
			pengajuan.setLastUpdatedDate(dto.getLastUpdatedDate());
			pengajuan.setOtorisasiFlag(dto.getOtorisasiFlag());
			pengajuan.setKesimpulanSementara(dto.getKesimpulanSementara());
			pengajuan.setJaminanPembayaran(dto.getJaminanPembayaran());
			pengajuan.setTglPengajuan(dto.getTglPengajuan());
			pengajuanSantunanDao.save(pengajuan);
			
//			BigDecimal totals = dto.getJmlByrAmblPenyelesaian().add(dto.getJmlByrP3kPenyelesaian()).add(dto.getJumlahDibayarLukalukaPenyelesaian())
//					.add(dto.getJumlahDibayarMeninggalPenyelesaian()).add(dto.getJumlahDibayarPenguburanPenyelesaian());
			//penyelesaian
			penyelesaian.setNoBerkas(dto.getNoBerkasPenyelesaian());
			penyelesaian.setTglProses(dto.getTglProsesPenyelesaian());
			penyelesaian.setJumlahDibayarMeninggal(dto.getJumlahDibayarMeninggalPenyelesaian());
			penyelesaian.setJumlahDibayarLukaluka(dto.getJumlahDibayarLukalukaPenyelesaian());
			penyelesaian.setJumlahDibayarPenguburan(dto.getJumlahDibayarPenguburanPenyelesaian());
			penyelesaian.setNoSuratPenyelesaian(dto.getNoSuratPenyelesaianPenyelesaian());
			penyelesaian.setNoRekening(dto.getNoRekeningPenyelesaian());
			penyelesaian.setCreatedBy(dto.getCreatedByPenyelesaian());
			penyelesaian.setCreationDate(dto.getCreationDatePenyelesaian());
			penyelesaian.setJmlByrAmbl(dto.getJmlByrAmblPenyelesaian());
			penyelesaian.setJmlByrP3k(dto.getJmlByrP3kPenyelesaian());
			penyelesaian.setJenisPembayaran(dto.getJenisPembayaranPenyelesaian());
			penyelesaian.setNoSuratPenyelesaian(dto.getNoSuratPenyelesaianPenyelesaian());
			penyelesaian.setNoRekening(dto.getNoRekeningPenyelesaian());
			penyelesaian.setIdGuid(dto.getIdGuidPenyelesaian());
			penyelesaianSantunanDao.save(penyelesaian);
			
			//additional desc
			additional.setNoBerkas(dto.getNoBerkasPenyelesaian());
			additional.setCreatedBy(dto.getCreatedByPenyelesaian());
			additional.setCreatedDate(dto.getCreationDatePenyelesaian());
			additional.setNamaRekening(dto.getNamaRekeningAdd());
			additional.setJnsRekening(dto.getJenisPembayaranPenyelesaian());
			additionalDescDao.save(additional);
			
			//penyelesaianRS
//			if (getPenyelesaianRS != null) {
//				getPenyelesaianRS.setLastUpdatedBy(dto.getCreatedByPenyelesaian());
//				getPenyelesaianRS.setLastUpdatedDate(new Date());
//				getPenyelesaianRS.setJumlahDibayarLukaluka(dto.getJumlahPengajuanLuka2RS());
//				penyelesaianRSDao.save(getPenyelesaianRS);
//			} else {
//				penyelesaianRS.setKodeRumahsakit(dto.getKodeRs());
//				penyelesaianRS.setNoBerkas(dto.getNoBerkas());
//				penyelesaianRS.setJumlahDibayarLukaluka(dto.getJumlahPengajuanLuka2RS());
//				penyelesaianRS.setCreatedBy(dto.getCreatedByPenyelesaian());
//				penyelesaianRS.setCreationDate(new Date());
//				penyelesaianRS.setIdPenyelesaianRs(dto.getKodeRs()+"."+dto.getNoBerkas());
//				System.out.println("aaaaaaaaaaaaaaaaaaaaa"+dto.getKodeRs()+"     "+dto.getNoBerkas());
//				penyelesaianRSDao.save(penyelesaianRS);
//			}
			map.put("status", CommonConstants.OK_REST_STATUS);
			return map;
			
		} catch (Exception e) {
			return new HashMap<>();
		}
		
	}

	@Override
	public Map<String, Object> cetakKuitansi(Map<String, Object> input,
			String noBerkas) {
		
		List<Object[]> kuitansi = pengajuanSantunanDao.cetakKuitansi(noBerkas);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		
		for(Object[] a : kuitansi){
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setNoBerkas((String)a[0]);
			dto.setSifatLakaDesc((String)a[1]);
			dto.setNamaPemohon((String)a[2]);
			dto.setAlamatPemohon((String)a[3]);
			dto.setTglKejadian((Date)a[4]);
			dto.setDeskripsiLokasi((String)a[5]);
			dto.setNoPolisi((String)a[6]);
			dto.setJenisKendaraanDesc((String)a[7]);
			dto.setNamaKorban((String)a[8]);
			dto.setUmur((BigDecimal)a[9]);
			dto.setAlamatKorban((String)a[10]);
			dto.setCideraDes((String)a[11]);
			dto.setHubKorbanDesc((String)a[12]);
			dto.setJumlahDibayarMeninggalPenyelesaian((BigDecimal)a[13]);
			dto.setJumlahDibayarLukalukaPenyelesaian((BigDecimal)a[14]);
			dto.setJmlByrAmblPenyelesaian((BigDecimal)a[15]);
			dto.setJmlByrP3kPenyelesaian((BigDecimal)a[16]);
			dto.setNoBpkPenyelesaian((String)a[17]);
			dto.setLingkupJaminan((String)a[18]);
			listDto.add(dto);
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> getOnePenyelesaianByNoBerkas(String noBerkas) {
		// TODO Auto-generated method stub
		List<PlPenyelesaianSantunan> list = penyelesaianSantunanDao.findOneByNoBerkas(noBerkas);
		List<PlPenyelesaianSantunanDto> listdto = new ArrayList<>();
		listdto = mapperFacade.mapAsList(list, PlPenyelesaianSantunanDto.class);
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listdto);
		return map;	}

	@Override
	public Map<String, Object> saveKuitansi(PlAdditionalDescDto dto) {
		// TODO Auto-generated method stub
		PlAdditionalDesc obj = new PlAdditionalDesc();
		List<PlAdditionalDesc> getOne = additionalDescDao.getOnebyNoberkas(dto.getNoBerkas());
		List<PlPenyelesaianSantunan> getPenyelesaian = penyelesaianSantunanDao.findOneByNoBerkas(dto.getNoBerkas());
		PlPenyelesaianSantunan onePenyelesaian = getPenyelesaian.get(0);
		Map<String, Object> map = new HashMap<>();
		try {
			if (getOne.size() == 0) {
				obj.setNoBerkas(dto.getNoBerkas());
				obj.setCreatedBy(dto.getCreatedBy());
				obj.setCreatedDate(new Date());
				obj.setTglMd(dto.getTglMd());
				obj.setTglTerimaLimpah(dto.getTglTerimaLimpah());
				obj.setNamaRekening(dto.getNamaRekening());
				obj.setJnsRekening(dto.getJnsRekening());
				obj.setTglRawatAwal(dto.getTglRawatAwal());
				obj.setTglRawatAkhir(dto.getTglRawatAkhir());
				obj.setIdRekRs(dto.getIdRekRs());
				obj.setLastUpdatedBy("");
				obj.setLastUpdatedDate(null);
				additionalDescDao.save(obj);
			} else {
				PlAdditionalDesc one = getOne.get(0);
				one.setLastUpdatedDate(new Date());
				one.setLastUpdatedBy(dto.getLastUpdatedBy());
				one.setNamaRekening(dto.getNamaRekening());
				one.setTglMd(dto.getTglMd());
				one.setTglTerimaLimpah(dto.getTglTerimaLimpah());
				one.setNamaRekening(dto.getNamaRekening());
				one.setJnsRekening(dto.getJnsRekening());
				one.setTglRawatAwal(dto.getTglRawatAwal());
				one.setTglRawatAkhir(dto.getTglRawatAkhir());
				one.setIdRekRs(dto.getIdRekRs());	
				additionalDescDao.save(one);
				
			}
			onePenyelesaian.setJenisPembayaran(dto.getJenisPembayaranPenyelesaian());
			onePenyelesaian.setNoRekening(dto.getNoRekPenyelesaian());
			onePenyelesaian.setIdGuid(dto.getIdGUIDPenyelesaian());
			onePenyelesaian.setLastUpdatedBy(dto.getLastUpdateByPenyelesaian());
			onePenyelesaian.setLastUpdatedDate(new Date());
			penyelesaianSantunanDao.save(onePenyelesaian);
			
			map.put("status", CommonConstants.OK_REST_STATUS);
			return map;
			
		} catch (Exception e) {
			return new HashMap<>();
		}
	
		
	}

	@Override
	public Map<String, Object> akumulasiPembayaran(String idkorban) {
		List<PlPenyelesaianSantunan> listObj = pengajuanSantunanDao.getAkumulasiPembayaran(idkorban);
		List<PlPenyelesaianSantunanDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(listObj, PlPenyelesaianSantunanDto.class);
		
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> getDataList(Map<String, Object> input,
			String pilihPengajuan, String tglPenerimaan, String noBerkas,
			String search) {
		
		List<Object[]> penyelesaianSantunan = pengajuanSantunanDao.getIndexPenyelesaian(pilihPengajuan,tglPenerimaan, noBerkas, search);
		List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
		
		for(Object[] a : penyelesaianSantunan){
			PlPengajuanSantunanDto dto = new PlPengajuanSantunanDto();
			dto.setIdKecelakaan((String)a[0]);
			dto.setNoBerkas((String)a[1]);
			dto.setStatusProsesDesc((String)a[2]);
			dto.setTglPengajuan((Date)a[3]);
			dto.setNama((String)a[4]);
			dto.setAlamatKorban((String)a[5]);
			dto.setStatusProses((String)a[6]);
			dto.setKodeRumahsakitPengajuanRs((String)a[7]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findPengajuanRsByNoBerkas(
			Map<String, Object> input, String noBerkas) {
		List<PlPengajuanR> list = plPengajuanRsDao.findPengajuanRsByNoBerkas(noBerkas);
		List<PlPengajuanRsDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(list, PlPengajuanRsDto.class);
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;

	}

	@Override
	public Map<String, Object> findBankByKodeRs(Map<String, Object> input,
			String kodeRumahsakit, String search) {
		
		List<Object[]> list = lovDao.getBankByKodeRs(kodeRumahsakit, search);
		List<FndBankDto> listDto = new ArrayList<>();
		for(Object[] o : list){
			FndBankDto dto = new FndBankDto();
			dto.setNamaBank((String)o[0]);
			dto.setKodeBank((String)o[1]);
			listDto.add(dto);
		}
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

	@Override
	public Map<String, Object> findRekByKodeRsKodeBank(
			Map<String, Object> input, String kodeRumahsakit, String kodeBank) {

		List<PlRekeningR> list = lovDao.listRekening(kodeBank, kodeRumahsakit);
		List<PlRekeningRDto> listDto = new ArrayList<>();
		listDto = mapperFacade.mapAsList(list, PlRekeningRDto.class);
		Map<String, Object> map = new HashMap<>();
		map.put("totalRecords", new Long(1));
		map.put("contentData", listDto);
		return map;
	}

}
