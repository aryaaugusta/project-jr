package core.service.operasional;

import java.util.Map;

import share.PlAdditionalDescDto;
import share.PlPengajuanSantunanDto;

public interface OpPenyelesaianPengajuanSvc {

	public abstract Map<String, Object> indexPenyelesaianPengajuan(String tglPengajuan,
			String noBerkas, String search, String jenisPengajuan, String dateFlag);

	public abstract Map<String,Object> detailPenyelesaianPengajuan(String tglPengajuan,
			String noBerkas, String search, String jenisPengajuan, String dateFlag);
	
	public Map<String, Object> savePenyelesaian (PlPengajuanSantunanDto dto);
	
	public Map<String,Object> cetakKuitansi(Map<String, Object> input, String noBerkas);
	
	public Map<String, Object> getOnePenyelesaianByNoBerkas(String noBerkas);

	public Map<String, Object> saveKuitansi(PlAdditionalDescDto dto);
	
	public Map<String, Object> akumulasiPembayaran(String idkorban);
	
	public Map<String, Object>  getDataList(Map<String, Object> input,String pilihPengajuan,String tglPenerimaan, String noBerkas, String search);
	
	public Map<String, Object>  findPengajuanRsByNoBerkas(Map<String, Object> input,String noBerkas);
	public Map<String, Object>  findBankByKodeRs(Map<String, Object> input,String kodeRumahsakit, String search);
	public Map<String, Object>  findRekByKodeRsKodeBank(Map<String, Object> input,String kodeRumahsakit, String kodeBank);

}
