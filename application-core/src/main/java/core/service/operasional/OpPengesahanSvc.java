package core.service.operasional;

import java.util.Map;

public interface OpPengesahanSvc {

	public Map<String,Object> getListIndexVerifikasi(Map<String, Object> input,
			String pilihPengajuan, String tglPengajuan, String noBerkas, String search);
	
}
