package core.service.operasional;

import java.util.Map;

public interface OpAbsahBerkasSvc {
	public Map<String, Object> getListAbsah(Map<String, Object> input);
	public Map<String, Object> callGeneral(Map<String, Object> input);
	public Map<String, Object>  getDataList(Map<String, Object> input,String pilihPengajuan,String tglPenerimaan, String noBerkas, String search);
}
