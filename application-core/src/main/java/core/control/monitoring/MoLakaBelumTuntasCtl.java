package core.control.monitoring;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.monitoring.MoLakaBelumTuntasSvc;

@RestController
@RequestMapping("/LakaBelumTuntas")
public class MoLakaBelumTuntasCtl {
	
	@Autowired
	MoLakaBelumTuntasSvc moLakaBelumTuntasSvc;
	
	
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndex(
		@RequestBody Map<String, Object> mapInput){
		
		String startDate = (String) mapInput.get("startDate");
		String endDate = (String) mapInput.get("endDate");
		String tipePeriode = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("tipePeriode"), "%%"),"%");
		String jenisLaporan = (String) mapInput.get("jenisLaporan") +"%";
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = moLakaBelumTuntasSvc.getLakaBelumTuntas(mapInput, startDate, endDate, tipePeriode, jenisLaporan, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

}
