package core.control.monitoring;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.monitoring.MoCoklitDasiIrsmsSvc;

@RestController
@RequestMapping("/MoCoklitDasiIrsms")
public class MoCoklitDasiIrsmsCtl {
	
	@Autowired
	MoCoklitDasiIrsmsSvc moCoklitDasiIrsmsSvc;
	
	@RequestMapping(value="/getListProvince", method = RequestMethod.POST)
	public RestResponse getListProvince(
		@RequestBody Map<String, Object> mapInput){
				
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = moCoklitDasiIrsmsSvc.getListProvince(mapInput, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/getListDistrictByProvince", method = RequestMethod.POST)
	public RestResponse getListDistrictByProvince(
		@RequestBody Map<String, Object> mapInput){
		String provinceId = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("provinceId"), "%%"),"%");				
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = moCoklitDasiIrsmsSvc.getListDistrictByProvinceId(mapInput, provinceId, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndex(
		@RequestBody Map<String, Object> mapInput){

		String startDate = (String) mapInput.get("startDate");
		String endDate = (String) mapInput.get("endDate");
		String kodeInstansi = (String) mapInput.get("kodeInstansi");
		Map<String, Object> map = moCoklitDasiIrsmsSvc.getListIndex(mapInput, kodeInstansi, startDate, endDate);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	@RequestMapping(value="/getPoldaByDistrict", method = RequestMethod.POST)
	public RestResponse getPoldaByDistrict(
		@RequestBody Map<String, Object> mapInput){

		String idDistrict = (String) mapInput.get("idDistrict");
		Map<String, Object> map = moCoklitDasiIrsmsSvc.getMappingPolda(mapInput, idDistrict);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
}
