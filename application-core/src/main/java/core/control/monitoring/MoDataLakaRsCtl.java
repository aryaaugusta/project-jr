package core.control.monitoring;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;

import core.service.monitoring.MoDataLakaRsSvc;

@RestController
@RequestMapping("/LakaRs")
public class MoDataLakaRsCtl {
	
	@Autowired
	MoDataLakaRsSvc moDataLakaRsSvc;
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndex(
		@RequestBody Map<String, Object> mapInput){
		
		String namaKantor = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaKantor"), "%%"),"%");
		String kodeRs = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeRs"), "%%"),"%");
		String startDate = (String) mapInput.get("startDate");
		String endDate = (String) mapInput.get("endDate");
		String pilihSumberData = (String) mapInput.get("pilihSumberData");
		String namaKorban = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaKorban"), "%%"),"%");
		String statusResponse = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("statusResponse"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = moDataLakaRsSvc.getDataLakaRs(mapInput, namaKantor, kodeRs, startDate,endDate, pilihSumberData, namaKorban, statusResponse, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/getLakaRsById", method = RequestMethod.POST)
	public RestResponse getDataById(
			@RequestBody Map<String, Object> mapInput){
		String kodeRumahSakit= StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeRumahSakit"),"%%"),"%");
		String kodeKejadian= StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeKejadian"),"%%"),"%");
		
		Map<String, Object> map = moDataLakaRsSvc.getLakaRsbyId(mapInput, kodeRumahSakit, kodeKejadian);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
			

}
