package core.control.monitoring;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.PlDataKecelakaanDto;
import share.PlTlRDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.monitoring.MoGLRSSvc;
import core.service.operasional.OpRegisterSementaraSvc;

@RestController
@RequestMapping("/MonitoringGlRs")
public class MoGlRsCtl {

	@Autowired
	MoGLRSSvc moGlRSSvc;
	
	@Autowired
	OpRegisterSementaraSvc opRegisterSementaraSvc;
	
	@RequestMapping(value="/jaminanAll", method = RequestMethod.POST)
	public RestResponse jaminanAll(
		@RequestBody Map<String, Object> mapInput){
				
		String kodeKantor = (String) mapInput.get("kodeKantor");
		Map<String, Object> map = moGlRSSvc.findJmlJaminanGLRS(kodeKantor);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/jaminanBayar", method = RequestMethod.POST)
	public RestResponse jaminanBayar(
		@RequestBody Map<String, Object> mapInput){
				
		String kodeKantor = (String) mapInput.get("kodeKantor");
		Map<String, Object> map = moGlRSSvc.findJmlJaminanBayarGLRS(kodeKantor);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/jaminanHutang", method = RequestMethod.POST)
	public RestResponse jaminanHutang(
		@RequestBody Map<String, Object> mapInput){
				
		String kodeKantor = (String) mapInput.get("kodeKantor");
		Map<String, Object> map = moGlRSSvc.findJmlJaminanHutangGLRS(kodeKantor);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	@RequestMapping(value="/jaminanBatal", method = RequestMethod.POST)
	public RestResponse jaminanBatal(
		@RequestBody Map<String, Object> mapInput){
				
		String kodeKantor = (String) mapInput.get("kodeKantor");
		Map<String, Object> map = moGlRSSvc.findJmlJaminanBatalGLRS(kodeKantor);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	//monitoring GL RS
	@RequestMapping(value = "/getIndexMonitoringGlRs",method = RequestMethod.POST)
	public RestResponse monitoringGLRSIndex(@RequestBody Map<String, Object> mapInput){
		
		String kodeKantor = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("kodeKantor"), "%%"),"%");
		String jenisTgl = (String) mapInput.get("jenisTgl");
		String tglAwal = (String)mapInput.get("tglAwal");
		String tglAkhir = (String)mapInput.get("tglAkhir");
		String kodeRS = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("kodeRS"), "%%"),"%");
		String flagBayar = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("flagBayar"), "%%"),"%");
		
		Map<String, Object> map = opRegisterSementaraSvc.getIndexGLRS(kodeKantor, jenisTgl, tglAwal, tglAkhir, kodeRS, flagBayar);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/plTlByIdJaminan",method = RequestMethod.POST)
	public RestResponse plTlRsByIdJaminan(@RequestBody Map<String, Object> mapInput){
		
		String idJaminan = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("idJaminan"), "%%"),"%");
		
		Map<String, Object> map = moGlRSSvc.plTlRsByIdJaminan(mapInput, idJaminan);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/savePlTlRs",method = RequestMethod.POST)
	public RestResponse save(@RequestBody PlTlRDto plTlRDto){	
		int status;
		String message;
		try {
			moGlRSSvc.savePlTlRs(plTlRDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value = "/getMaxNoSurat",method = RequestMethod.POST)
	public RestResponse getMaxNoSurat(@RequestBody Map<String, Object> mapInput){
		
		String noRegister = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("noRegister"), "%%"),"%");
		
		Map<String, Object> map = moGlRSSvc.getMaxNoSurat(mapInput, noRegister);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	@RequestMapping(value = "/getSizePlTlRs",method = RequestMethod.POST)
	public RestResponse getMaxIdJaminan(@RequestBody Map<String, Object> mapInput){
		
		String noRegister = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("noRegister"), "%%"),"%");
		
		Map<String, Object> map = moGlRSSvc.getSize(mapInput, noRegister);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

}
