package core.control.monitoring;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.monitoring.MonitoringPelimpahanSvc;

@RestController
@RequestMapping("/MonitoringPelimpahan")
public class MonitoringPelimpahanCtl {
	
	@Autowired
	MonitoringPelimpahanSvc monitoringPelimpahanSvc;
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndex(
		@RequestBody Map<String, Object> mapInput){
		
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		String namaKorban = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaKorban"), "%%"),"%");
			
		String tglKejadian = (String) mapInput.get("tglKejadian");
		if(mapInput.get("tglKejadian")==null){
			tglKejadian = "%";
		}
		String tglPenerimaan = (String) mapInput.get("tglPenerimaan");
		if(mapInput.get("tglPenerimaan")==null){
			tglPenerimaan = "%";
		}	
		String diajukanDi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("diajukanDi"), "%%"),"%");
		String dilimpahkanKe = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("dilimpahkanKe"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = monitoringPelimpahanSvc.getListMonitoring(mapInput, noBerkas, namaKorban, tglKejadian, tglPenerimaan, diajukanDi, dilimpahkanKe, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

}
