package core.control.monitoring;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.monitoring.MoDataLakaIrsmsSvc;

@RestController
@RequestMapping("/MoLakaIrsms")
public class MoDataLakaIrsmsCtl {
	
	@Autowired
	MoDataLakaIrsmsSvc moDataLakaIrsmsSvc;
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndex(
		@RequestBody Map<String, Object> mapInput){
				
		String kejadianStartDate = (String) mapInput.get("kejadianStartDate");
		String kejadianEndDate = (String) mapInput.get("kejadianEndDate");
		String laporanStartDate = (String) mapInput.get("laporanStartDate");
		String laporanEndDate = (String) mapInput.get("laporanEndDate");
		String instansi = (String) mapInput.get("instansi");
		String namaKorban = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaKorban"), "%%"),"%");
		String noLaporan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noLaporan"), "%%"),"%");
		String jenisData = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("jenisData"), "%%"),"%");
		
		Map<String, Object> map = moDataLakaIrsmsSvc.getListIndex(mapInput, instansi, kejadianStartDate, kejadianEndDate, laporanStartDate, laporanEndDate, namaKorban, noLaporan, jenisData);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/findByIdKejadian", method = RequestMethod.POST)
	public RestResponse findByIdKejadian(
		@RequestBody Map<String, Object> mapInput){
				
		String idKejadian = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKejadian"), "%%"),"%");
		String instansi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("instansi"), "%%"),"%");
		
		Map<String, Object> map = moDataLakaIrsmsSvc.findByIdKejadian(mapInput, idKejadian, instansi);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
}
