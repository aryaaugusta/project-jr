package core.control.common;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.common.LoginData;
import share.common.UserMenuDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.util.AppException;
import core.service.AuthSvc;



/**
 * This controller for authentication and authorization user application
 * 
 * @author None of ur bsns
 * 
 */
@RestController
@RequestMapping("/auth")
public class AuthCtl {

	@Autowired
	private AuthSvc authSvc;

	/**
	 * Check whether user is authenticated by system or not. And get all
	 * information about current logged in user.
	 * 
	 * @param loginData
	 * @return
	 */
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public RestResponse authUser(@RequestBody LoginData loginData) {
		try {
			return new RestResponse(0, null,
					authSvc.authUser(loginData.getLogin(), loginData.getPassword()));
		} catch (AppException e) {
			return new RestResponse(1,"Login gagal", null);
		}
	}

	@RequestMapping(value = "checkPassword", method = RequestMethod.POST)
	public RestResponse checkPassword(@RequestBody Map<String, Object> param) {
		try {
			return authSvc.checkPassword(param);
		} catch (Exception e) {
			e.printStackTrace();
			return new RestResponse(1,
					e.getMessage(), null);
		}
	}

	@RequestMapping(value = "getMenuByLogin/{login}", method = RequestMethod.POST)
	public RestResponse getMenuByLogin(
		@PathVariable("login") String login,
		@RequestBody LoginData logins){
		RestResponse rest = new RestResponse();
//		Map<String, Object> map = new HashMap<>();
		Integer sts = null;
		String msg = "";
		try{
//			UserSessionJR ujr = authSvc.bypassLogin(login);
			List<UserMenuDto> listM = authSvc.getMenuByLogin(login);
//			map.put("user", ujr);
//			map.put("menu", listM );
			rest.setContents(listM);
			sts = 0;
			msg = "OKE";
		}catch (Exception s){
			sts = 1;
			msg = "KESALAHAN";
			s.printStackTrace();
		}
		rest.setMessage(msg);
		rest.setStatus(sts);
		
		return rest;
	}
	
	@RequestMapping(value = "getUserInfo", method = RequestMethod.POST)
	public RestResponse getUserInfo(
			@RequestBody LoginData logins){
		RestResponse rest = new RestResponse();
//		Map<String, Object> map = new HashMap<>();
		Integer sts = null;
		String login = logins.getLogin();
		String msg = "";
		try{
			UserSessionJR ujr = authSvc.bypassLogin(login);
//			List<UserMenuDto> listM = authSvc.getMenuByLogin(login);
//			map.put("user", ujr);
//			map.put("menu", listM );
			sts = 0;
			msg = "OKE";
			rest.setContents(ujr);
		}catch (Exception s){
			sts = 1;
			msg = "KESALAHAN";
			s.printStackTrace();
		}
		rest.setMessage(msg);
		rest.setStatus(sts);
		
		return rest;
	}
	
	@RequestMapping(value = "getMenu", method = RequestMethod.GET)
	public RestResponse getMenuTest(
		@PathVariable("login") String login,
		@RequestBody LoginData logins){
		RestResponse rest = new RestResponse();
//		Map<String, Object> map = new HashMap<>();
		Integer sts = null;
		String msg = "";
		try{
//			UserSessionJR ujr = authSvc.bypassLogin(login);
			List<UserMenuDto> listM = authSvc.getMenuByLogin(login);
//			map.put("user", ujr);
//			map.put("menu", listM );
			rest.setContents(listM);
			sts = 0;
			msg = "OKE";
		}catch (Exception s){
			sts = 1;
			msg = "KESALAHAN";
			s.printStackTrace();
		}
		rest.setMessage(msg);
		rest.setStatus(sts);
		
		return rest;
	}
		
}