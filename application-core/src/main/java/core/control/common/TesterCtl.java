package core.control.common;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.AppException;

@RestController
@RequestMapping("/test")
public class TesterCtl {
	@RequestMapping(method = RequestMethod.GET)
	public RestResponse authUser() {
		try {
			return new RestResponse(0,"OKE","OKE DONG");
		} catch (AppException e) {
			return new RestResponse(1,
					e.getCodeMessage(), null);
		}
	}
}
