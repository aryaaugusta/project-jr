package core.control.operasional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.AuthRsPicDto;
import share.PlAngkutanKecelakaanDto;
import share.PlDataKecelakaanDto;
import share.PlKorbanKecelakaanDto;
import share.PlNihilKecelakaanDto;
import share.PlRumahSakitDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.operasional.OpPlDataKecelakaanSvc;


@RestController
@RequestMapping("/OpDataKecelakaan")
public class OpPlDataKecelakaanCtl {
	
	@Autowired
	OpPlDataKecelakaanSvc opPlDataKecelakaanSvc;
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse loadList(
			@RequestBody Map<String, Object>mapInput){
		
		String kejadianStartDate = (String) mapInput.get("kejadianStartDate");
		String kejadianEndDate = (String) mapInput.get("kejadianEndDate");
		String asalBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("asalBerkas"), "%%"),"%");
		String samsat = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("samsat"), "%%"),"%");
		String laporanStartDate = (String) mapInput.get("laporanStartDate");
		String laporanEndDate = (String) mapInput.get("laporanEndDate");
		String instansi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("instansi"), "%%"),"%");
		if(instansi.contains("-")){
			instansi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("instansi"), "%%").substring(0,7),"%");
		}
		String instansiPembuat = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("instansiPembuat"), "%%"),"%");
		String noLaporan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noLaporan"), "%%"),"%");
		String lokasi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("lokasi"), "%%"),"%");
		if(lokasi.contains("-")){
			lokasi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("lokasi"), "%%").substring(0,7),"%");
		}
		String namaKorban = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaKorban"), "%%"),"%");
		String noIdentitas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noIdentitas"), "%%"),"%");
		
		String lingkupJaminan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("lingkupJaminan"), "%%"),"%");
		if(lingkupJaminan.contains("-")){
			lingkupJaminan = "%%";
		}
		
		String jenisPertanggungan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("jenisPertanggungan"), "%%"),"%");		
		String sifatCidera = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("sifatCidera"), "%%").substring(0,2),"%");
		
		String sifatLaka = (String)mapInput.get("sifatKecelakaan");
		if (sifatLaka != null && "NR".equals(sifatLaka)) {
			sifatLaka = "NORMAL";
		} else if (sifatLaka != null && "TL".equals(sifatLaka)) {
			sifatLaka = "TABRAK LARI";
		}
		String sifatKecelakaan = StringUtil.surroundString(StringUtil.nevl(sifatLaka, "%%"),"%");
		String kecelakaanKatostrop = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kecelakaanKatostrop"), "%%"),"%");
		String perusahaanPenerbangan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("perusahaanPenerbangan"), "%%"),"%");
		String perusahaanOtobus = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("perusahaanOtobus"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");

		Map<String, Object> map = opPlDataKecelakaanSvc.getDataList(mapInput, kejadianStartDate, kejadianEndDate, 
				asalBerkas, samsat, laporanStartDate, laporanEndDate, instansi, instansiPembuat, noLaporan, 
				lokasi, namaKorban, noIdentitas, lingkupJaminan, jenisPertanggungan, sifatCidera, sifatKecelakaan, 
				kecelakaanKatostrop, perusahaanPenerbangan, perusahaanOtobus, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	@RequestMapping(value="/dataLakaById", method = RequestMethod.POST)
	public RestResponse getDataById(
			@RequestBody Map<String, Object> mapInput){
		
		String idKecelakaan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKecelakaan"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.getDataByIdKecelakaan(mapInput, idKecelakaan);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/angkutanLakaById", method = RequestMethod.POST)
	public RestResponse getAngkutanById(
			@RequestBody Map<String, Object> mapInput){
				
		String idKecelakaan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKecelakaan"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.getAngkutanByIdLaka(mapInput, idKecelakaan, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/angkutanLakaByIdAngkutan", method = RequestMethod.POST)
	public RestResponse getAngkutanByIdAngkutan(
			@RequestBody Map<String, Object> mapInput){
				
		String idAngkutan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idAngkutan"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.getAngkutanByIdAngkutan(mapInput, idAngkutan);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/korbanLakaById", method = RequestMethod.POST)
	public RestResponse getKorbanById(
			@RequestBody Map<String, Object> mapInput){
				
		String idKecelakaan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKecelakaan"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.getKorbanByIdLaka(mapInput, idKecelakaan, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/korbanLakaByIdKorban", method = RequestMethod.POST)
	public RestResponse getKorbanByIdKorban(
			@RequestBody Map<String, Object> mapInput){
				
		String idKorban = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKorban"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.getKorbanByIdKorban(mapInput, idKorban);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/kodeNoLaporan", method = RequestMethod.POST)
	public RestResponse getKodeNoLaporan(
			@RequestBody Map<String, Object> mapInput){
				
		String kodeInstansi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeInstansi"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.getKodeNoLaporan(mapInput, kodeInstansi);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/saveDataLaka",method = RequestMethod.POST)
	public RestResponse save(@RequestBody PlDataKecelakaanDto plDataKecelakaanDto){	
		int status;
		String message;
		try {
			opPlDataKecelakaanSvc.saveDataLaka(plDataKecelakaanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value="/saveAngkutanKecelakaan",method = RequestMethod.POST)
	public RestResponse saveAngkutan(@RequestBody PlAngkutanKecelakaanDto plAngkutanKecelakaanDto){	
		int status;
		String message;
		try {
			opPlDataKecelakaanSvc.saveAngkutanKecelakaan(plAngkutanKecelakaanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value="/saveKorbanKecelakaan",method = RequestMethod.POST)
	public RestResponse saveKorban(@RequestBody PlKorbanKecelakaanDto plKorbanKecelakaanDto){	
		int status;
		String message;
		try {
			opPlDataKecelakaanSvc.saveKorban(plKorbanKecelakaanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};

	@RequestMapping(value="/saveNihilKecelakaan",method = RequestMethod.POST)
	public RestResponse saveKorban(@RequestBody PlNihilKecelakaanDto plNihilKecelakaanDto){	
		int status;
		String message;
		try {
			opPlDataKecelakaanSvc.saveNihilKecelakaan(plNihilKecelakaanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value = "/deleteAngkutan", method = RequestMethod.POST)
	public RestResponse deleteAngkutan(@RequestBody PlAngkutanKecelakaanDto plAngkutanKecelakaanDto){
		int status;
		String message;
		try {
			opPlDataKecelakaanSvc.deleteAngkutan(plAngkutanKecelakaanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value = "/deleteKorban", method = RequestMethod.POST)
	public RestResponse deleteKorban(@RequestBody PlKorbanKecelakaanDto plKorbanKecelakaanDto){
		int status;
		String message;
		try {
			opPlDataKecelakaanSvc.deleteKorban(plKorbanKecelakaanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value = "/deleteLaka", method = RequestMethod.POST)
	public RestResponse deleteLaka(@RequestBody PlDataKecelakaanDto plDataKecelakaanDto){
		int status;
		String message;
		try {
			opPlDataKecelakaanSvc.deleteKorbanByIdLaka(plDataKecelakaanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}

	
	@RequestMapping(value = "/getPrintData", method = RequestMethod.POST)
	public RestResponse getPrint(@RequestBody Map<String,Object> map){
		int status;
		String message;
		Map<String, Object> out = new HashMap<>();
		try {
			out = opPlDataKecelakaanSvc.getDataForPrint(map);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, out);
	}
	
	@RequestMapping(value="/reportDataLaka", method = RequestMethod.POST)
	public RestResponse getReportDataLaka1(
			@RequestBody Map<String, Object> mapInput){
				
		String idKec = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKec"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.reportDataLaka1(mapInput, idKec);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/reportDataKendaraan", method = RequestMethod.POST)
	public RestResponse getReportDataLaka2(
			@RequestBody Map<String, Object> mapInput){
				
		String idKec = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKec"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.reportDataLaka2(mapInput, idKec);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/reportDataKorban", method = RequestMethod.POST)
	public RestResponse getReportDataLaka3(
			@RequestBody Map<String, Object> mapInput){
				
		String idKec = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKec"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.reportDataLaka3(mapInput, idKec);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/cekData", method = RequestMethod.POST)
	public RestResponse cekData(
			@RequestBody Map<String, Object> mapInput){
				
		String tglKejadian = (String) mapInput.get("tglKejadian");
		String tglLaporan = (String) mapInput.get("tglLaporan");
		String kodeInstansi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeInstansi"), "%%"),"%");
		
		Map<String, Object> map = opPlDataKecelakaanSvc.cekData(mapInput, tglKejadian, tglLaporan, kodeInstansi);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}


}
