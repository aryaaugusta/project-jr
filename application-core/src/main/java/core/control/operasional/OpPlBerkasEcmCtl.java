package core.control.operasional;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import core.service.operasional.BerkasEcmsContainSvc;
import core.service.operasional.OpPlBerkasEcmSvc;
import core.service.operasional.PlBerkasEcmHSvc;
import core.service.operasional.PlBerkasPengajuanSvc;
import share.PlBerkasEcmDto;
import share.PlBerkasEcmHDto;
import share.PlBerkasEcmsContainDto;
import share.PlBerkasPengajuanDto;

@RestController
@RequestMapping("/OpPlBerkasEcm")
public class OpPlBerkasEcmCtl {
	
	@Autowired
	OpPlBerkasEcmSvc opPlBerkasEcmSvc;
	
	@Autowired
	BerkasEcmsContainSvc berkasEcmsContainSvc;
	
	@Autowired
	PlBerkasEcmHSvc plBerkasEcmSvc;
	
	@Autowired
	PlBerkasPengajuanSvc plBerkasPengajuanSvc;
	
	@RequestMapping(value="/saveBerkasEcmsContain",method = RequestMethod.POST)
	public RestResponse save(@RequestBody PlBerkasEcmsContainDto plBerkasEcmsContainDto){	
		int status;
		String message;
		try {
			berkasEcmsContainSvc.saveBerkasEcmsContain(plBerkasEcmsContainDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value="/saveDataPlBerkasEcm", method=RequestMethod.POST)
	public RestResponse save(@RequestBody PlBerkasEcmDto plBerkasEcmDto){
		int status;
		String message;
		try {
			opPlBerkasEcmSvc.saveData(plBerkasEcmDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value="/saveBerkasEcmH", method=RequestMethod.POST)
	public RestResponse save(@RequestBody PlBerkasEcmHDto plBerkasEcmHDto){
		int status;
		String message;
		try {
			plBerkasEcmSvc.saveBerkasEcmH(plBerkasEcmHDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value="/saveBerkasPengajuan", method=RequestMethod.POST)
	public RestResponse save(@RequestBody PlBerkasPengajuanDto plBerkasPengajuanDto){
		int status;
		String message;
		try {
			plBerkasPengajuanSvc.saveBerkasPengajuan(plBerkasPengajuanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
}
