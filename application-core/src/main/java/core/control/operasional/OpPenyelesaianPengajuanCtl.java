package core.control.operasional;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.PlAdditionalDescDto;
import share.PlPengajuanSantunanDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.operasional.OpPenyelesaianPengajuanSvc;

@RestController
@RequestMapping("/OperasionalPenyelesaianPengajuan")
public class OpPenyelesaianPengajuanCtl {

	@Autowired
	OpPenyelesaianPengajuanSvc penyelesaianPengajuanSvc;
	
	@RequestMapping(value = "/index",method = RequestMethod.POST)
	public RestResponse loadIndex(@RequestBody Map<String, Object> mapInput){
//		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
//		String jenisPengajuan = (String)mapInput.get("jenisPengajuan");
//		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
//		String tglPengajuan;
//		String dateFlag = (String)mapInput.get("dateFlag");
//		if ((String)mapInput.get("tglPengajuan") == null || mapInput.get("tglPengajuan").equals("")) {
//			tglPengajuan = "1/1/1900";
//		} else {
//			tglPengajuan = (String)mapInput.get("tglPengajuan");
//		}
//		Map<String, Object> map = penyelesaianPengajuanSvc.indexPenyelesaianPengajuan(tglPengajuan, noBerkas, search, jenisPengajuan,dateFlag);
//		
//		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
//				map.get("contentData"), (long) map.get("totalRecords"));
		
		String tglPenerimaan = (String) mapInput.get("tglPenerimaan");
		if(mapInput.get("tglPenerimaan")==null){
			tglPenerimaan = "%";
		}
		String pilihPengajuan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("pilihPengajuan"), "%%"),"%");
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = penyelesaianPengajuanSvc.getDataList(mapInput, pilihPengajuan, tglPenerimaan, noBerkas, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));

	}
	
	@RequestMapping(value = "/detail",method = RequestMethod.POST)
	public RestResponse loadDetail(@RequestBody Map<String, Object> mapInput){
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		String jenisPengajuan = (String)mapInput.get("jenisPengajuan");
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		String tglPengajuan;
		String dateFlag = (String)mapInput.get("dateFlag");
		if ((String)mapInput.get("tglPengajuan") == null || mapInput.get("tglPengajuan").equals("")) {
			tglPengajuan = "1/1/1900";
		} else {
			tglPengajuan = (String)mapInput.get("tglPengajuan");
		}
		Map<String, Object> map = penyelesaianPengajuanSvc.detailPenyelesaianPengajuan(tglPengajuan, noBerkas, search, jenisPengajuan,dateFlag);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/save",method = RequestMethod.POST)
	public RestResponse savePenyelesaian(@RequestBody PlPengajuanSantunanDto dto){
		int status;
		String message;
		try {
			penyelesaianPengajuanSvc.savePenyelesaian(dto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	//added by luthfi
	@RequestMapping(value="/cetakKuitansi", method=RequestMethod.POST)
	public RestResponse penyelesaianSantunan(
			@RequestBody Map<String, Object> mapInput){
		String noBerkas = (String)mapInput.get("noBerkas");
		Map<String, Object> map = penyelesaianPengajuanSvc.cetakKuitansi(mapInput, noBerkas);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/onePenyelesaian",method=RequestMethod.POST)
	public RestResponse getOnePenyelesaian(@RequestBody Map<String, Object> mapinput){
		String noBerkas = (String)mapinput.get("noBerkas");
		Map<String, Object> map = penyelesaianPengajuanSvc.getOnePenyelesaianByNoBerkas(noBerkas);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		

	}
	@RequestMapping(value = "/saveKuitansi",method = RequestMethod.POST)
	public RestResponse saveKuitansi(@RequestBody PlAdditionalDescDto dto){
		int status;
		String message;
		try {
			penyelesaianPengajuanSvc.saveKuitansi(dto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value = "/akumulasiPembayaran",method = RequestMethod.POST)
	public RestResponse akumulasiPembayaran(@RequestBody Map<String, Object> mapInput){
		String idKorban = (String)mapInput.get("idKorban");
		Map<String, Object> map = penyelesaianPengajuanSvc.akumulasiPembayaran(idKorban);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/findBankByKodeRs",method = RequestMethod.POST)
	public RestResponse getBankByKodeRs(@RequestBody Map<String, Object> mapInput){
		String kodeRumahsakit = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeRumahsakit"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = penyelesaianPengajuanSvc.findBankByKodeRs(mapInput, kodeRumahsakit,search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/findRekByKodeRsKodeBank",method = RequestMethod.POST)
	public RestResponse getRekByKodeRsKodeBank(@RequestBody Map<String, Object> mapInput){
		String kodeRumahsakit = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeRumahsakit"), "%%"),"%");
		String kodeBank = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeBank"), "%%"),"%");
		Map<String, Object> map = penyelesaianPengajuanSvc.findRekByKodeRsKodeBank(mapInput, kodeRumahsakit, kodeBank);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	

}
