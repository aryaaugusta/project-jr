package core.control.operasional;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.operasional.OpAbsahBerkasSvc;
import core.service.operasional.OpOtorisasiSvc;

@RestController
@RequestMapping("/AbsahBerkas")
public class OpAbsahBerkasCtl {
	
	@Autowired
	OpAbsahBerkasSvc opAbsahBerkasSvc;
	
	@RequestMapping(value="/search", method = RequestMethod.POST)
	public RestResponse getListIndex(
			@RequestBody Map<String, Object> input){
		Map<String, Object> map = opAbsahBerkasSvc.getListAbsah(input);		
		return new RestResponse(Integer.parseInt((String) map.get("status")), null,
				map, (Integer) map.get("total"));
	}
	
	@RequestMapping(value="/general", method = RequestMethod.POST)
	public RestResponse callGeneral(
			@RequestBody Map<String, Object> input){
		Map<String, Object> map = opAbsahBerkasSvc.callGeneral(input);
		return new RestResponse( (int)map.get("status"), null,
				map, Integer.valueOf(String.valueOf(map.get("total"))));
	}
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndexIdentifikasi(
			@RequestBody Map<String, Object> mapInput){
		
		String tglPenerimaan = (String) mapInput.get("tglPenerimaan");
		String pilihPengajuan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("pilihPengajuan"), "%%"),"%");
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		System.err.println("luthfi92 "+pilihPengajuan);
		Map<String, Object> map = opAbsahBerkasSvc.getDataList(mapInput, pilihPengajuan, tglPenerimaan, noBerkas, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

}
