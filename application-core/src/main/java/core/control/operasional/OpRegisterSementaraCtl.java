package core.control.operasional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;

import share.PlAngkutanKecelakaanDto;
import share.PlRegisterSementaraDto;
import share.PlTindakLanjutDto;
import share.PlTlRDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.dao.lov.LovDao;
import core.service.operasional.OpRegisterSementaraSvc;

@RestController
@RequestMapping("/OperasionalRegisterSementara")
public class OpRegisterSementaraCtl {

	@Autowired
	OpRegisterSementaraSvc registerSementaraSvc;
	
	@RequestMapping(value = "/refCode", method = RequestMethod.POST)
	public RestResponse loadRefCode(@RequestBody Map<String, Object> mapInput){
		String flag;
		String rvDomain;
		String search;
		search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		rvDomain = (String)mapInput.get("rvDomain");
		if (mapInput.get("flag") == null || mapInput.get("flag").equals("")) {
			flag = "%%";
		} else {
			flag = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("flag"), "%%"),"%");
		}
		
		Map<String, Object> map = registerSementaraSvc.loadJRRefCode(rvDomain,flag,search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/saveTL",method = RequestMethod.POST)
	public RestResponse saveTL(@RequestBody PlRegisterSementaraDto dto){
		int status;
		Map<String, Object> map = new HashMap<>();
		Map<String, Object> mapNo = new HashMap<>();
		String message;
		String noregister;
		try {
			map = registerSementaraSvc.saveRegister(dto);
			status = (int)map.get("status");
			noregister = (String)map.get("noRegister");
			mapNo.put("noRegister", noregister);
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, mapNo);
	}
	
	@RequestMapping(value = "/saveTindakLanjut",method = RequestMethod.POST)
	public RestResponse saveTindakLanjut(@RequestBody PlTindakLanjutDto dto){
		int status;
		String message;
		try {
			registerSementaraSvc.saveTindakLanjut(dto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}

	@RequestMapping(value = "/saveRegisterTl",method = RequestMethod.POST)
	public RestResponse saveRegisterTl(@RequestBody PlRegisterSementaraDto dto){
		int status;
		String message;
		
		try {
			registerSementaraSvc.saveRegister2(dto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value = "/saveRegisterSementara",method = RequestMethod.POST)
	public RestResponse saveRegisterSementara(@RequestBody PlRegisterSementaraDto dto){
		int status;
		String message;
		try {
			registerSementaraSvc.saveRegisterSementara(dto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value = "/savePS",method = RequestMethod.POST)
	public RestResponse savePS(@RequestBody PlRegisterSementaraDto dto){
		int status;
		String message;
		try {
			registerSementaraSvc.saveRegisterPS(dto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.POST)
	public RestResponse loadRegisterSementara(@RequestBody Map<String, Object> mapInput){
		
		String kodeKantorJr = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeKantorJr"), "%%"),"%");
		String regDay;
		String regMonth;
		String regYear;
		String lakaDay;
		String lakaMonth;
		String lakaYear;
		if (mapInput.get("regDay") == null || mapInput.get("regDay").equals("")) {
			regDay = "%%";
		} else {
			regDay = StringUtil.surroundString((String)mapInput.get("regDay"), "%%");
		}
		if (mapInput.get("regMonth") == null || mapInput.get("regMonth").equals("")) {
			regMonth = "%%";
		} else {
			regMonth = StringUtil.surroundString((String)mapInput.get("regMonth"), "%%");
		}
		if (mapInput.get("regYear") == null || mapInput.get("regYear").equals("")) {
			regYear = "%%";
		} else {
			regYear = StringUtil.surroundString((String)mapInput.get("regYear"), "%%");
		}
		
		String namaKorban = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaKorban"), "%%"),"%");
		
		if (mapInput.get("lakaDay") == null || mapInput.get("lakaDay").equals("")) {
			lakaDay = "%%";
		} else {
			lakaDay = StringUtil.surroundString((String)mapInput.get("lakaDay"), "%%");
		}
		if (mapInput.get("lakaMonth") == null || mapInput.get("lakaMonth").equals("")) {
			lakaMonth = "%%";
		} else {
			lakaMonth = StringUtil.surroundString((String)mapInput.get("lakaMonth"), "%%");
		}
		if (mapInput.get("lakaYear") == null || mapInput.get("lakaYear").equals("")) {
			lakaYear = "%%";
		} else {
			lakaYear = StringUtil.surroundString((String)mapInput.get("lakaYear"), "%%");
		}
		String noLaporan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noLaporan"), "%%"),"%");
		String noRegister = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noRegister"), "%%"),"%");
		String asalBerkasFlag = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("asalBerkasFlag"), "%%"),"%");
		String tindakLanjutFlag = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("tindakLanjutFlag"), "%%"),"%");
		String cideraKorban = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("cideraKorban"), "%%"),"%");
		String namaInstansi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaInstansi"), "%%"),"%");
		String rvDomainCidera = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("rvDomainCidera"), "%%"),"%");
		String rvDomainTindakLanjut = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("rvDomainTindakLanjut"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");

		Map<String, Object> map = registerSementaraSvc.loadRegisterSementara(
				kodeKantorJr, regDay, regMonth, regYear, namaKorban, lakaDay, 
				lakaMonth, lakaYear, noLaporan, noRegister, asalBerkasFlag, tindakLanjutFlag, 
				cideraKorban, namaInstansi, rvDomainCidera, rvDomainTindakLanjut,search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));	
		}
	
	
	@RequestMapping(value = "/dataLaka",method = RequestMethod.POST)
	public RestResponse getDataLaka(@RequestBody Map<String, Object> mapInput){
		String kejadianStartDate;
		String kejadianEndDate;
		String laporStartDate;
		String laporEndDate;
		String tipe;
		if (mapInput.get("kejadianStartDate") == null) {
			kejadianStartDate = "1/1/1700";
		} else {
			kejadianStartDate = (String)mapInput.get("kejadianStartDate");
		}
		if (mapInput.get("kejadianEndDate") == null) {
			kejadianEndDate = "31/12/2300";
		} else {
			kejadianEndDate = (String)mapInput.get("kejadianEndDate");
		}
		if (mapInput.get("laporStartDate") == null) {
			laporStartDate = "1/1/1700";
		} else {
			laporStartDate = (String)mapInput.get("laporStartDate");
		}
		if (mapInput.get("laporEndDate") == null) {
			laporEndDate = "31/12/2300";
		} else {
			laporEndDate = (String)mapInput.get("laporEndDate");
		}
		
		
		String namaKantorInstansi;
		String noLaporPolisi;
		String namaKorbanLaka;
		String kodeJaminan;
		String statusLP;
		String search = "";
		String idKorban;
		String idKecelakaan;
		
		if (mapInput.get("namaKantorInstansi") == null || mapInput.get("namaKantorInstansi").equals("")) {
			namaKantorInstansi = "%%";
		} else {
			namaKantorInstansi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaKantorInstansi"), "%%"),"%");
		}
		if (mapInput.get("namaKorbanLaka") == null || mapInput.get("namaKorbanLaka").equals("")) {
			namaKorbanLaka = "%%";
		} else {
			namaKorbanLaka = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaKorbanLaka"), "%%"),"%");
		}
		if (mapInput.get("noLaporPolisi") == null || mapInput.get("noLaporPolisi").equals("")) {
			noLaporPolisi = "%%";
		} else {
			noLaporPolisi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noLaporPolisi"), "%%"),"%");
		}
		if (mapInput.get("kodeJaminan") == null || mapInput.get("kodeJaminan").equals("")) {
			kodeJaminan = "%%";
		} else {
			kodeJaminan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeJaminan"), "%%"),"%");
		}
		if (mapInput.get("statusLP") == null || mapInput.get("statusLP").equals("")) {
			statusLP = "%%";
		} else {
			statusLP = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("statusLP"), "%%"),"%");
		}
//		if (mapInput.get("search") == null || mapInput.get("search").equals("")) {
//			search = "%%";
//		} else {
//			
//		}
		search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		if (mapInput.get("idKorban") == null || mapInput.get("idKorban").equals("")) {
			idKorban = "%%";
		} else {
			idKorban = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKorban"), "%%"),"%");
		}
		if (mapInput.get("idKecelakaan") == null || mapInput.get("idKecelakaan").equals("")) {
			idKecelakaan = "%%";
		} else {
			idKecelakaan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKecelakaan"), "%%"),"%");
		}
		tipe = (String)mapInput.get("tipe");
		
		Map<String, Object> map = registerSementaraSvc.loadDataLaka(kejadianStartDate, kejadianEndDate, 
				namaKantorInstansi, laporStartDate, laporEndDate, noLaporPolisi, namaKorbanLaka, 
				kodeJaminan, statusLP, search,idKorban,idKecelakaan,tipe);
		return new RestResponse(CommonConstants.OK_REST_STATUS,null,
				map.get("contentData"), (long) map.get("totalRecords"));	
		}
	
	@RequestMapping(value = "/tindakLanjut", method = RequestMethod.POST)
	public RestResponse loadTindakLanjut(@RequestBody Map<String, Object> mapInput){
		String noRegister = (String)mapInput.get("noRegister");
		
		Map<String, Object> map = registerSementaraSvc.loadTindakLanjut(noRegister);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/deleteTL", method = RequestMethod.POST)
	public RestResponse deleteTL(@RequestBody Map<String, Object> map){
		int status;
		String message;
		try {
			registerSementaraSvc.deleteTindakLanjut((String)map.get("idTlReg"));
			System.out.println("tes : "+(String)map.get("idTlReg") );
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			e.printStackTrace();
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value = "/getRegisterByIdKorban", method = RequestMethod.POST)
	public RestResponse getRegisterByIdKorban(
			@RequestBody Map<String, Object> mapInput) {

		String idKecelakaan = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("idKecelakaan"), "%%"),"%");

		Map<String, Object> map = registerSementaraSvc.findRegisterByIdKorban(mapInput, idKecelakaan);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	//addded by luthfi
	@RequestMapping(value = "/getRegisterByNoRegister", method = RequestMethod.POST)
	public RestResponse getRegisterByNoRegister(
			@RequestBody Map<String, Object> mapInput) {

		String noReg = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("noReg"), "%%"),"%");

		Map<String, Object> map = registerSementaraSvc.findRegisterByNoRegister(mapInput, noReg);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/findOneRegister", method = RequestMethod.POST)
	public RestResponse findOneRegister(
			@RequestBody Map<String, Object> mapInput) {

		String noReg = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("noReg"), "%%"),"%");

		Map<String, Object> map = registerSementaraSvc.findOneRegister(mapInput, noReg);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	//added by luthfi
	@RequestMapping(value = "/deleteRegisterSementara", method = RequestMethod.POST)
	public RestResponse deleteAngkutan(@RequestBody PlRegisterSementaraDto plRegisterSementaraDto){
		int status;
		String message;
		try {
			registerSementaraSvc.deleteRegister(plRegisterSementaraDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	//monitoring GL RS
	@RequestMapping(value = "/monitoringGLRSIndex",method = RequestMethod.POST)
	public RestResponse monitoringGLRSIndex(@RequestBody Map<String, Object> mapInput){
		
		String kodeKantor = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("kodeKantor"), "%%"),"%");
		String jenisTgl = (String) mapInput.get("jenisTgl");
		String tglAwal = (String)mapInput.get("tglAwal");
		String tglAkhir = (String)mapInput.get("tglAkhir");
		String kodeRS = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("kodeRS"), "%%"),"%");
		String flagBayar = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("flagBayar"), "%%"),"%");
		
		Map<String, Object> map = registerSementaraSvc.findIndexGLRS(kodeKantor, jenisTgl, tglAwal, tglAkhir, kodeRS, flagBayar);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/noSuratJaminan",method = RequestMethod.POST)
	public RestResponse plTlRsByNoSuratJaminan(@RequestBody Map<String, Object> mapInput){
		
		String noSuratJaminan = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("noSuratJaminan"), "%%"),"%");
		
		Map<String, Object> map = registerSementaraSvc.plTlRsByNoSuratJaminan(mapInput, noSuratJaminan);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/plTlByNoRegister",method = RequestMethod.POST)
	public RestResponse plTlRsByNoRegister(@RequestBody Map<String, Object> mapInput){
		
		String noRegister = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("noRegister"), "%%"),"%");
		
		Map<String, Object> map = registerSementaraSvc.plTlRsByNoRegister(mapInput, noRegister);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/plTindakLanjutByIdTl",method = RequestMethod.POST)
	public RestResponse plTindakLanjutByIdTl(@RequestBody Map<String, Object> mapInput){
		
		String idTl = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("idTl"), "%%"),"%");
		
		Map<String, Object> map = registerSementaraSvc.findTindakLanjutByIdTl(mapInput, idTl);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	@RequestMapping(value = "/dataCetakTl1",method = RequestMethod.POST)
	public RestResponse getDataCetakTl1(@RequestBody Map<String, Object> mapInput){
		
		String noRegister = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("noRegister"), "%%"),"%");
		
		Map<String, Object> map = registerSementaraSvc.dataCetakTl1(mapInput, noRegister);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}


}
