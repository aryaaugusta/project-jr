package core.control.operasional;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.operasional.OpOtorisasiSvc;

@RestController
@RequestMapping("/OpOtorisasi")
public class OpOtorisasiCtl {
	
	@Autowired
	OpOtorisasiSvc opOtorisasiSvc; 
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndex(
			@RequestBody Map<String, Object> mapInput){
		String tglPenerimaan = (String) mapInput.get("tglPenerimaan");
		if(mapInput.get("tglPenerimaan")==null){
			tglPenerimaan = "%";
		}
		String pilihPengajuan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("pilihPengajuan"), "%%"),"%");
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = opOtorisasiSvc.getDataList(mapInput, pilihPengajuan, tglPenerimaan, noBerkas, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

}
