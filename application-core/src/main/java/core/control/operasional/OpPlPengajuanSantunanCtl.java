package core.control.operasional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.PlAdditionalDescDto;
import share.PlBerkasPengajuanDto;
import share.PlDisposisiDto;
import share.PlPengajuanRsDto;
import share.PlPengajuanSantunanDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.operasional.OpPlPengajuanSantunanSvc;

@RestController
@RequestMapping("/OpPengajuanSantunan")
public class OpPlPengajuanSantunanCtl {
	@Autowired
	OpPlPengajuanSantunanSvc opPlPengajuanSantunanSvc;

	@RequestMapping(value = "/all", method = RequestMethod.POST)
	public RestResponse loadList(@RequestBody Map<String, Object> mapInput) {

		String diajukanDi = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("diajukanDi"), "%%"), "%");
		String dilimpahkanKe = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("dilimpahkanKe"), "%%"),"%");
		String namaKorban = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("namaKorban"), "%%"), "%");
		String pengajuanDay = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("pengajuanDay"), "%%"),"%");
		String pengajuanMonth = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("pengajuanMonth"), "%%"),"%");
		String pengajuanYear = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("pengajuanYear"), "%%"),"%");
		String lakaDay = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("lakaDay"), "%%"), "%");
		String lakaMonth = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("lakaMonth"), "%%"), "%");
		String lakaYear = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("lakaYear"), "%%"), "%");
		String kodeInstansi = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("kodeInstansi"), "%%"),"%");
		String noLaporan = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("noLaporan"), "%%"), "%");
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("noBerkas"), "%%"), "%");
		String statusProses = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("statusProses"), "%%"),"%");
		String penyelesaian = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("penyelesaian"), "%%"),"%");
		String namaPemohon = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("namaPemohon"), "%%"),"%");
		String kodeRs = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("kodeRs"), "%%"), "%");
		String search = StringUtil.surroundString(StringUtil.nevl((String) mapInput.get("search"), "%%"), "%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.getDataList(
				mapInput, diajukanDi, dilimpahkanKe, namaKorban, pengajuanDay,
				pengajuanMonth, pengajuanYear, lakaDay, lakaMonth, lakaYear,
				kodeInstansi, noLaporan, noBerkas, statusProses, penyelesaian,
				namaPemohon, kodeRs, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	@RequestMapping(value = "/findbyId", method = RequestMethod.POST)
	public RestResponse getDataById(
	// @PathVariable("idKorbanKecelakaan") String idKorbanKecelakaan,

			@RequestBody Map<String, Object> mapInput) {

		String idKecelakaan = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("idKecelakaan"), "%%"),
				"%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.findById(mapInput,
				idKecelakaan);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/getAdditionalByNoBerkas", method = RequestMethod.POST)
	public RestResponse getAdditionalByNoBerkas(
			@RequestBody Map<String, Object> mapInput) {

		String noBerkas = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("noBerkas"), "%%"),
				"%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.findAdditionalDescByNoBerkas(mapInput, noBerkas);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	@RequestMapping(value = "/getBerkasPengajuan", method = RequestMethod.POST)
	public RestResponse getBerkasPengajuan(
			@RequestBody Map<String, Object> mapInput) {

		String noBerkas = (String) mapInput.get("noBerkas");

		Map<String, Object> map = opPlPengajuanSantunanSvc.findBerkasPengajuanByNoBerkas(mapInput, noBerkas);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/deleteBerkasPengajuan", method = RequestMethod.POST)
	public RestResponse deleteBerkas(
			@RequestBody List<PlBerkasPengajuanDto> plBerkasPengajuanDtos) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.deleteBerkasPengajuan(plBerkasPengajuanDtos);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};



	@RequestMapping(value = "/getListRs", method = RequestMethod.POST)
	public RestResponse listRs(@RequestBody Map<String, Object> mapInput) {
		Map<String, Object> map = opPlPengajuanSantunanSvc.getListRs(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	@RequestMapping(value = "/saveSantunan", method = RequestMethod.POST)
	public RestResponse saveSantunan(
			@RequestBody PlPengajuanSantunanDto plPengajuanSantunanDto) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.saveSantunan(plPengajuanSantunanDto);
			// opPlPengajuanSantunanSvc.saveSantunanNoTrigger(plPengajuanSantunanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value = "/saveSantunanOnly", method = RequestMethod.POST)
	public RestResponse saveSantunanOnly(
			@RequestBody PlPengajuanSantunanDto plPengajuanSantunanDto) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.saveSantunanOnly(plPengajuanSantunanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};

	
	@RequestMapping(value = "/saveBerkasPengajuan", method = RequestMethod.POST)
	public RestResponse saveBerkasPengajuan(
			@RequestBody List<PlBerkasPengajuanDto> plBerkasPengajuanDto) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.saveBerkasPengajuan(plBerkasPengajuanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};


	@RequestMapping(value = "/saveAdditional", method = RequestMethod.POST)
	public RestResponse saveAdditional(
			@RequestBody PlAdditionalDescDto plAdditionalDescDto) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.saveAdditional(plAdditionalDescDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};

	@RequestMapping(value = "/savePengRs", method = RequestMethod.POST)
	public RestResponse savePengajuanRs(
			@RequestBody PlPengajuanRsDto plPengajuanRsDto) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.savePengajuanRs(plPengajuanRsDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};

	@RequestMapping(value = "/findByNoPengajuan", method = RequestMethod.POST)
	public RestResponse getDataByNoPengajuan(
	// @PathVariable("idKorbanKecelakaan") String idKorbanKecelakaan,

			@RequestBody Map<String, Object> mapInput) {

		String noPengajuan = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("noPengajuan"), "%%"),
				"%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.findByNoPengajuan(
				mapInput, noPengajuan);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/findByNoBerkas", method = RequestMethod.POST)
	public RestResponse getDataByNoBerkas(

			@RequestBody Map<String, Object> mapInput) {

		String noBerkas = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("noBerkas"), "%%"),
				"%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.findByNoBerkas(mapInput, noBerkas);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/getDataPengajuanByNoBerkas", method = RequestMethod.POST)
	public RestResponse getDataPengajuanByNoBerkas(

			@RequestBody Map<String, Object> mapInput) {

		String noBerkas = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("noBerkas"), "%%"),
				"%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.getDataPengajuanByNoBerkas(mapInput, noBerkas);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/saveDisposisi", method = RequestMethod.POST)
	public RestResponse saveDisposisi(
			@RequestBody PlDisposisiDto plDisposisiDto) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.saveDisposisi(plDisposisiDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};

	@RequestMapping(value = "/getDisposisiById", method = RequestMethod.POST)
	public RestResponse getListDisposisiById(

			@RequestBody Map<String, Object> mapInput) {

		String noBerkas = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("noBerkas"), "%%"),
				"%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.getListDisposisiById(mapInput, noBerkas);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	//added by luthfi
	@RequestMapping(value = "/saveSantunanOtorisasi", method = RequestMethod.POST)
	public RestResponse saveSantunanOtorisasi(
			@RequestBody PlPengajuanSantunanDto plPengajuanSantunanDto) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.updateSantunanOtorisasi(plPengajuanSantunanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	//added by luthfi
	@RequestMapping(value = "/saveSantunanProsesSelanjutnya", method = RequestMethod.POST)
	public RestResponse saveSantunanProsesSelanjutnya(
			@RequestBody PlPengajuanSantunanDto plPengajuanSantunanDto) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.updateStatusProses(plPengajuanSantunanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	//added by Meilona
	@RequestMapping(value = "/findOnePenyelesaian", method = RequestMethod.POST)
	public RestResponse getDataPenyelesaian(

			@RequestBody Map<String, Object> mapInput) {

		String noBerkas = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("noBerkas"), "%%"),
				"%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.findOnePenyelesaian(mapInput, noBerkas);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/getPengajuanByIdKorban", method = RequestMethod.POST)
	public RestResponse getPengajuanByIdKorban(@RequestBody Map<String, Object> mapInput) {

		String idKorban= StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("idKorban"), "%%"),"%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.getSantunanByIdKorban(mapInput, idKorban);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	
	//added by luthfi
	//index hapus pengajuan
	@RequestMapping(value = "/getIndexHapusPengajuan", method = RequestMethod.POST)
	public RestResponse getIndexHapusPengajuan(@RequestBody Map<String, Object> mapInput) {

		String noBerkas= (String) mapInput.get("noBerkas");

		Map<String, Object> map = opPlPengajuanSantunanSvc.getIndexHapusPengajuan(mapInput, noBerkas);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	//added by luthfi
	//delete pengajuan
	@RequestMapping(value = "/deletePengajuan", method = RequestMethod.POST)
	public RestResponse deletePengajuan(
			@RequestBody PlPengajuanSantunanDto plPengajuanSantunanDto) {
		int status;
		String message;
		try {
			opPlPengajuanSantunanSvc.deletePengajuan(plPengajuanSantunanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value = "/findPengajuanRsByNoBerkas", method = RequestMethod.POST)
	public RestResponse getDataPengajuanRsByNoBerkas(

			@RequestBody Map<String, Object> mapInput) {

		String noBerkas = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("noBerkas"), "%%"),
				"%");

		Map<String, Object> map = opPlPengajuanSantunanSvc.findPengajuanRsByNoBerkas(mapInput, noBerkas);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}


	
}
