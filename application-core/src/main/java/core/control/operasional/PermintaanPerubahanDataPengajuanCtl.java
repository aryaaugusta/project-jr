package core.control.operasional;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.PlPengajuanSantunanDto;
import share.PlRegisterSementaraDto;
import share.PlRequestPerubahanDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.operasional.PermintaanPerubahanDataPengajuanSvc;

@RestController
@RequestMapping("/PermintaanPerubahanDataPengajuan")
public class PermintaanPerubahanDataPengajuanCtl {
	
	@Autowired
	PermintaanPerubahanDataPengajuanSvc ppDataPengajuanSvc;
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndex(
		@RequestBody Map<String, Object> mapInput){
		
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		String status =StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("status"), "%%"),"%");
		
		Map<String, Object> map = ppDataPengajuanSvc.getListIndexPPDataPengajuan(mapInput, noBerkas, status, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/findByNoBerkas", method = RequestMethod.POST)
	public RestResponse getDataByNoBerkas(

			@RequestBody Map<String, Object> mapInput) {

		String noBerkas = StringUtil.surroundString(
				StringUtil.nevl((String) mapInput.get("noBerkas"), "%%"),
				"%");

		Map<String, Object> map = ppDataPengajuanSvc.findByNoBerkas(mapInput, noBerkas);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/savePermintaan", method = RequestMethod.POST)
	public RestResponse savePermintaan(
			@RequestBody PlRequestPerubahanDto plRequestPerubahanDto) {
		int status;
		String message;
		try {
			ppDataPengajuanSvc.savePermintaan(plRequestPerubahanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value = "/deletePermintaan", method = RequestMethod.POST)
	public RestResponse deletePermintaan(@RequestBody PlRequestPerubahanDto plRequestPerubahanDto){
		int status;
		String message;
		try {
			ppDataPengajuanSvc.deletePermintaan(plRequestPerubahanDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
			e.printStackTrace();
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}


}
