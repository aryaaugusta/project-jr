package core.control.operasional;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;

import core.service.operasional.OpNikKKDukcapilSvc;
@RestController
@RequestMapping("/dukcapil")
public class OpNikKKDukcapilCtl {
@Autowired
OpNikKKDukcapilSvc dukcapilSvc;

@RequestMapping(value="/save", method = RequestMethod.POST)
public RestResponse getListIndex(
		@RequestBody Map<String, Object> input){
	
	Map<String, Object> map = dukcapilSvc.general(input);
	
	return new RestResponse((Integer) ( map.get("status")), null,
			map, (Integer) map.get("total"));
}
}
