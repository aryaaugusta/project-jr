package core.control.operasional;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.operasional.OpPengesahanSvc;

@RestController
@RequestMapping("/OpPengesahan")
public class OpPengesahanCtl {

	@Autowired
	OpPengesahanSvc opPengesahanSvc; 
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndex(
		@RequestBody Map<String, Object> mapInput){
		
		String pilihPengajuan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("pilihPengajuan"), "%%"),"%");
		String tglPengajuan = (String) mapInput.get("tglPengajuan");
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = opPengesahanSvc.getListIndexVerifikasi(mapInput, pilihPengajuan, tglPengajuan, noBerkas, search);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	
}
