package core.control.operasional;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.operasional.OpPeriodeEntryLayananSvc;

@RestController
@RequestMapping("/OpPeriodeEntryLayanan")
public class OpPeriodeEntryLayananCtl {
	
	@Autowired
	OpPeriodeEntryLayananSvc opPeriodeEntryLayananSvc;
	
	@RequestMapping(value="/general", method = RequestMethod.POST)
	public RestResponse general(
			@RequestBody Map<String, Object> mapInput){
		
		
		Map<String, Object> map = opPeriodeEntryLayananSvc.general(mapInput);
		
		return new RestResponse((int) map.get("status"), (String) map.get("message"),
				map, 0L);
	}
	
	@RequestMapping(value="/all", method = RequestMethod.POST)
	public RestResponse getListIndex(
		@RequestBody Map<String, Object> mapInput){
		
		String kodeKantorJr = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeKantorJr"), "%%"),"%");
		String periodeBulan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("periodeBulan"), "%%"),"%");
		String periodeTahun = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("periodeTahun"), "%%"),"%");
		
		Map<String, Object> map = opPeriodeEntryLayananSvc.getDataList(mapInput, kodeKantorJr, periodeBulan, periodeTahun);

		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}


}
