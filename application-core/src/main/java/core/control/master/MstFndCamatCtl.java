package core.control.master;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import share.FndCamatDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import core.service.master.MstFndCamatSvc;

@RestController
@RequestMapping("/MstLokasi")
public class MstFndCamatCtl {

	@Autowired
	MstFndCamatSvc mstFndCamatSvc;

	@RequestMapping(value = "/allProv/{search}", method = RequestMethod.POST)
	public RestResponse listLoad(
			@PathVariable("search")String search,
			@RequestBody Map<String, Object> mapInput) {

		Map<String, Object> map = mstFndCamatSvc.loadList(mapInput, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/allProv/", method = RequestMethod.POST)
	public RestResponse listLoadx(
			@RequestBody Map<String, Object> mapInput) {
		String search = (String) mapInput.get("search");
		Map<String, Object> map = mstFndCamatSvc.loadList(mapInput, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/all/{kodeNama}/{search}", method = RequestMethod.POST)
	public RestResponse getDataList(
			@PathVariable("kodeNama")String kodeNama,
			@PathVariable("search")String search,
			@RequestBody Map<String, Object> mapInput) {

		Map<String, Object> map = mstFndCamatSvc.getDataList(mapInput, kodeNama, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/findLokasi",method = RequestMethod.POST)
	public RestResponse listLoadLokasi(
		@RequestParam(value = CommonConstants.SEARCH, required = false) String search,
		@RequestBody Map<String, Object> mapInput) {
		Map<String, Object> map = mstFndCamatSvc.findLokasi(mapInput,search);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/findProv",method = RequestMethod.POST)
	public RestResponse listLoadProv(
		@RequestParam(value = CommonConstants.SEARCH, required = false) String search,
		@RequestBody Map<String, Object> mapInput) {
		Map<String, Object> map = mstFndCamatSvc.findProv(mapInput,search);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/findKota/{kodeProvinsi}",method = RequestMethod.POST)
	public RestResponse listLoadKota(
		@PathVariable("kodeProvinsi") String kodeProvinsi,
		@RequestParam(value = CommonConstants.SEARCH, required = false) String search,
		@RequestBody Map<String, Object> mapInput) {
		Map<String, Object> map = mstFndCamatSvc.findKodeKabbyProv(mapInput, kodeProvinsi);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	
	@RequestMapping(method = RequestMethod.POST)
	public RestResponse save(
			@RequestBody FndCamatDto fndCamatDto){
		RestResponse restResponse = new RestResponse();
		int i = mstFndCamatSvc.save(fndCamatDto);
		restResponse.setStatus(i);
		if(i == CommonConstants.OK_REST_STATUS){
			restResponse.setMessage("I001");
		}else if(i == 2){
			restResponse.setMessage("E155");
			restResponse.setStatus(CommonConstants.ERROR_REST_STATUS);
		}else if(i == 3){
			restResponse.setMessage("E154");
			restResponse.setStatus(CommonConstants.ERROR_REST_STATUS);
		}else if(i == 4){
			restResponse.setMessage("E156");
			restResponse.setStatus(CommonConstants.ERROR_REST_STATUS);
		}else{
			restResponse.setMessage("E003");
			restResponse.setStatus(CommonConstants.ERROR_REST_STATUS);
		}
		return restResponse;
	}
	
	@RequestMapping(value="/update", method = RequestMethod.POST)
	public RestResponse update(
			@RequestBody FndCamatDto fndCamatDto){
		RestResponse restResponse = new RestResponse();
		int i = mstFndCamatSvc.update(fndCamatDto);
		restResponse.setStatus(i);
		if(i == CommonConstants.OK_REST_STATUS){
			restResponse.setMessage("I001");
		}else{
			restResponse.setMessage("E003");
		}
		return restResponse;
	}
	
	@RequestMapping(value = "/delete",method = RequestMethod.POST)
	public RestResponse removeAreaHse(
			@RequestBody FndCamatDto fndCamatDto) {
		RestResponse restRespone = new RestResponse();
		int i = mstFndCamatSvc.delete(fndCamatDto);
		restRespone.setStatus(i);
		if (i == CommonConstants.OK_REST_STATUS) {
			restRespone.setMessage("I001");
		} else {
			restRespone.setMessage("E003");
		}
		return restRespone;
	}
	
	@RequestMapping(value = "/general",method = RequestMethod.POST)
	public RestResponse general(
			@RequestBody Map<String, Object> input) {
		RestResponse rest = new RestResponse();
		Map<String, Object> out = new HashMap<>();
		try {
			out = mstFndCamatSvc.general(input);
			rest.setStatus((int) out.get("status"));
			rest.setMessage((String)out.get("message"));
			rest.setContents(out.get("content"));
			rest.setTotalRecords((long) out.get("total"));
		} catch (Exception e) {
			rest.setStatus(CommonConstants.ERROR_REST_STATUS);
			rest.setMessage("Get Data Failed");
			rest.setContents(null);
			rest.setTotalRecords(0L);
			e.printStackTrace();
		}
		
		return rest;
	}
	
	
}
