package core.control.master;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.PlInstansiDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import core.service.master.MstPlInstansiSvc;

@RestController
@RequestMapping("/MasterInstansi")
public class MstPlInstansiCtl {

	@Autowired
	MstPlInstansiSvc mstPlInstansiSvc;

	@RequestMapping(value = "/all/{kodeInstansi}/{search}", method = RequestMethod.POST)
	public RestResponse listLoad(
			@RequestBody Map<String, Object> mapInput,
			@PathVariable("kodeInstansi") String kodeInstansi,
			@PathVariable("search")String search) {

		Map<String, Object> map = mstPlInstansiSvc.loadList(mapInput,
				search, kodeInstansi);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/all/", method = RequestMethod.POST)
	public RestResponse listLoad(
			@RequestBody Map<String, Object> mapInput) {
		
		Map<String, Object> map = mstPlInstansiSvc.loadList(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/korlantasProvince", method = RequestMethod.POST)
	public RestResponse listKorlantasProvince(){
		Map<String, Object> map = mstPlInstansiSvc.findKorlantasProvince();
		return new RestResponse(CommonConstants.OK_REST_STATUS,null,map.get("contentData"),
				(long)map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/korlantasDistrict/{provinceNumber}", method = RequestMethod.POST)
	public RestResponse listKorlantasDistrictByProvinceNumber(@PathVariable("provinceNumber") BigDecimal provinceNumber){
		Map<String, Object> map = mstPlInstansiSvc.findKorlantasDirtrict(provinceNumber);
		return new RestResponse(CommonConstants.OK_REST_STATUS,null,map.get("contentData"),
				(long)map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/korlantasDistrictId/{districtNumber}", method = RequestMethod.POST)
	public RestResponse listKorlantasDistrictByDistrictNumber(@PathVariable("districtNumber") String districtNumber){
		Map<String, Object> map = mstPlInstansiSvc.findKorlantasDirtrictByDIstricNumber(districtNumber);
		return new RestResponse(CommonConstants.OK_REST_STATUS,null,map.get("contentData"),
				(long)map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/korlantasProvince/{kodeInstansi}", method = RequestMethod.POST)
	public RestResponse korlantasProvinceByKodeInstansi(
			@RequestBody Map<String, Object> input,
			@PathVariable("kodeInstansi") String kodeInstansi){
		Map<String, Object> map = mstPlInstansiSvc.getOneKorlantasProvince(input, kodeInstansi);
		return new RestResponse(CommonConstants.OK_REST_STATUS,null,map.get("contentData"),
				(long)map.get("totalRecords"));
	}
	
	
	@RequestMapping(method = RequestMethod.POST)
	public RestResponse save(@RequestBody PlInstansiDto mstPlInstansiDto){
		int status;
		String message;
		try {
			mstPlInstansiSvc.save(mstPlInstansiDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());

	}
	
	@RequestMapping(value="/update",method = RequestMethod.POST)
	public RestResponse update(@RequestBody PlInstansiDto mstPlInstansiDto){
		int status;
		String message;
		try {
			mstPlInstansiSvc.update(mstPlInstansiDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());

	}
	
	@RequestMapping(value="/delete",method = RequestMethod.POST)
	public RestResponse deleteInstansi(@RequestBody PlInstansiDto mstPlInstansiDto){	
		int status;
		String message;
		try {
			status = mstPlInstansiSvc.delete(mstPlInstansiDto);
//			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
}
