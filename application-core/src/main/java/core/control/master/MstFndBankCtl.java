package core.control.master;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.FndBankDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import core.service.master.MstFndBankSvc;

@RestController
@RequestMapping("/fndBank")
public class MstFndBankCtl {

		@Autowired
		MstFndBankSvc mstFndBankSvc;
		
		@RequestMapping(value = "/all/{search}", method = RequestMethod.POST)
		public RestResponse listLoad(
				@PathVariable("search") String search,
				@RequestBody Map<String, Object> mapInput){

			Map<String, Object> map = mstFndBankSvc.loadlist(mapInput, search);
					
			return new RestResponse(CommonConstants.OK_REST_STATUS, null,
					map.get("contentData"), (long) map.get("totalRecords"));

		}
		
		@RequestMapping(value = "/findRek/{kodeBank}/{search}", method = RequestMethod.POST)
		public RestResponse listLoadRek(
				@PathVariable("search") String search,
				@PathVariable("kodeBank") String kodeBank,
				@RequestBody Map<String, Object> mapInput) {

			Map<String, Object> map = mstFndBankSvc.findRekByKodeBank(mapInput, kodeBank, search);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null,
					map.get("contentData"), (long) map.get("totalRecords"));

		}
		
		@RequestMapping(method = RequestMethod.POST)
		public RestResponse save(@RequestBody FndBankDto mstFndBankDto){	
			int status;
			String message;
			try {
				mstFndBankSvc.save(mstFndBankDto);
				status = CommonConstants.OK_REST_STATUS;
				message = "I001";
			} catch (Exception e) {
				status = CommonConstants.ERROR_REST_STATUS;
				message = "E003";
			}
			return new RestResponse(status, message, new HashMap<String, Object>());
		};
		
		
}
