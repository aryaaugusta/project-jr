package core.control.master;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import share.PlPksRDto;
import share.PlRekeningRDto;
import share.PlRsBpjDto;
import share.PlRsMapBpjDto;
import share.PlRumahSakitDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.master.MstPlRumahSakitSvc;

@RestController
@RequestMapping("/MasterRumahSakit")
public class MstPlRumahSakitCtl {

	@Autowired
	MstPlRumahSakitSvc mstPlRumahSakitSvc;

	@RequestMapping(value = "/all", method = RequestMethod.POST)
	public RestResponse listLoad(
			@RequestBody Map<String, Object> mapInput) {

		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		String kanWil = (String) mapInput.get("kanWil")+"%";
		String kodeNama = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeNamaRs"), "%%"),"%");
		

		Map<String, Object> map = mstPlRumahSakitSvc.loadList(mapInput, kanWil, kodeNama, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));

	}
	
	@RequestMapping(value="/findAllBank",method = RequestMethod.POST)
	public RestResponse listLoadBank(@RequestBody Map<String, Object> mapInput){
		
		Map<String, Object> map = mstPlRumahSakitSvc.findAllBank(mapInput);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/findBpjsByLoket",method = RequestMethod.POST)
	public RestResponse listLoadBpjs(@RequestBody Map<String, Object> mapInput){
		
		String loketPenanggungjawab = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("loketPenanggungjawab"), "%%"),"%");
		Map<String, Object> map = mstPlRumahSakitSvc.findAllMapBpjsByLoket(mapInput, loketPenanggungjawab);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	
	@RequestMapping(value="/findPj",method = RequestMethod.POST)
	public RestResponse findPj(@RequestBody Map<String, Object> mapInput){
		
		Map<String, Object> map = mstPlRumahSakitSvc.findDistinctMapBpjs(mapInput);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/findPj/{kodeRumahsakit}",method = RequestMethod.POST)
	public RestResponse findPjByKodeRumahSakit(
			@PathVariable("kodeRumahsakit")String kodeRumahsakit,
			@RequestBody Map<String, Object> mapInput){
		
		Map<String, Object> map = mstPlRumahSakitSvc.findDistinctMapBpjsByKodeRumahSakit(mapInput, kodeRumahsakit);
		
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}

	
	@RequestMapping(method = RequestMethod.POST)
	public RestResponse save(@RequestBody PlRumahSakitDto mstPlRumahSakitDto){	
		int status;
		String message;
		try {
			mstPlRumahSakitSvc.save(mstPlRumahSakitDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value="/delete",method = RequestMethod.POST)
	public RestResponse deleteRumahSakit(@RequestBody PlRumahSakitDto mstPlRumahSakitDto){	
		int status;
		String message;
		try {
			int s = mstPlRumahSakitSvc.delete(mstPlRumahSakitDto);
			status = s;
//			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value="/saveRsBpj", method = RequestMethod.POST)
	public RestResponse savePksRs(@RequestBody PlRsBpjDto mstPlRsBpjDto){	
		int status;
		String message;
		try {
			mstPlRumahSakitSvc.save(mstPlRsBpjDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};

	
	@RequestMapping(value="/savePksRs", method = RequestMethod.POST)
	public RestResponse savePksRs(@RequestBody PlPksRDto mstPlPksRDto){	
		int status;
		String message;
		try {
			mstPlRumahSakitSvc.save(mstPlPksRDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value="/deletePksRs", method = RequestMethod.POST)
	public RestResponse deletePksRs(@RequestBody PlPksRDto mstPlPksRDto){	
		int status;
		String message;
		try {
			mstPlRumahSakitSvc.delete(mstPlPksRDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};

	
	@RequestMapping(value="/saveRekRs", method = RequestMethod.POST)
	public RestResponse savePksRs(@RequestBody PlRekeningRDto mstPlRekeningRDto){	
		int status;
		String message;
		try {
			status = mstPlRumahSakitSvc.save(mstPlRekeningRDto);
//			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};
	
	@RequestMapping(value="/deleteRekRs", method = RequestMethod.POST)
	public RestResponse deletePksRs(@RequestBody PlRekeningRDto mstPlRekeningRDto){	
		int status;
		String message;
		try {
//			status = CommonConstants.OK_REST_STATUS;
			status = mstPlRumahSakitSvc.delete(mstPlRekeningRDto);
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};

	
	@RequestMapping(value="/saveRsMapBpj", method = RequestMethod.POST)
	public RestResponse saveRsMapBpj(@RequestBody PlRsMapBpjDto mstPlRsMapBpjDto){	
		int status;
		String message;
		try {
			mstPlRumahSakitSvc.save(mstPlRsMapBpjDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	};

	
	@RequestMapping(value = "/findPks/{kodeRumahsakit}/{pageSequence}/{size}", method = RequestMethod.POST)
	public RestResponse listLoadPks(
			@RequestParam(value = CommonConstants.DIRECTION, required = false) String direction,
			@RequestParam(value = CommonConstants.ORDER_BY, required = false) String orderBy,
			@PathVariable("pageSequence") int pageSequence,
			@PathVariable("size") int size,
			@PathVariable("kodeRumahsakit") String kodeRumahsakit,
			@RequestBody Map<String, Object> mapInput) {

		Map<String, Object> map = mstPlRumahSakitSvc.findPksByKodeRumahSakit(mapInput, kodeRumahsakit, pageSequence, size, direction, orderBy);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));

	}
	@RequestMapping(value = "/findPks/{kodeRumahsakit}", method = RequestMethod.POST)
	public RestResponse listLoadPksNoPage(
			@PathVariable("kodeRumahsakit") String kodeRumahsakit,
			@RequestBody Map<String, Object> mapInput) {
		
		Map<String, Object> map = mstPlRumahSakitSvc.findPksByKodeRumahSakit(mapInput, kodeRumahsakit);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), 1L);
		
	}
	
	@RequestMapping(value="/updatePlPksRs/{kodeRumahSakit}", method = RequestMethod.POST)
	public RestResponse updatePKSRS(@PathVariable("kodeRumahSakit")String kodeRS,
			@RequestBody Map<String, Object> input){
		try{
			int s = mstPlRumahSakitSvc.updateTablePlPksRS(kodeRS);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, s, 1L);
		}catch(Exception s){
			s.printStackTrace();
			return new RestResponse(CommonConstants.ERROR_REST_STATUS, null, null, 1L);
		}
		
	}
	
	@RequestMapping(value = "/findRek/{kodeRumahsakit}/{pageSequence}/{size}", method = RequestMethod.POST)
	public RestResponse listLoadRek(
			@RequestParam(value = CommonConstants.DIRECTION, required = false) String direction,
			@RequestParam(value = CommonConstants.ORDER_BY, required = false) String orderBy,
			@PathVariable("pageSequence") int pageSequence,
			@PathVariable("size") int size,
			@PathVariable("kodeRumahsakit") String kodeRumahsakit,
			@RequestBody Map<String, Object> mapInput) {

		Map<String, Object> map = mstPlRumahSakitSvc.findRekByKodeRumahSakit(mapInput, kodeRumahsakit, pageSequence, size, direction, orderBy);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));

	}
	
	@RequestMapping(value = "/findRek/{kodeRumahsakit}", method = RequestMethod.POST)
	public RestResponse listLoadReknoPage(
			@PathVariable("kodeRumahsakit") String kodeRumahsakit,
			@RequestBody Map<String, Object> mapInput) {
		
		Map<String, Object> map = mstPlRumahSakitSvc.findRekByKodeRumahSakit(mapInput, kodeRumahsakit);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), 1L);
		
	}
}


