package core.control.master;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.AuthRsPicDto;
import share.AuthUserDto;
import share.AuthUserGadgetDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.service.master.MstAuthRsPicSvc;

@RestController
@RequestMapping("/MasterRsPic")
public class MstAuthRsPicCtl {

	@Autowired
	MstAuthRsPicSvc mstAuthRsPicSvc;

	@RequestMapping(value = "/all", method = RequestMethod.POST)
	public RestResponse listLoad(
			@RequestBody Map<String, Object> mapInput) {
		
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");

		Map<String, Object> map = mstAuthRsPicSvc.loadList(mapInput, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));

	}

	@RequestMapping(value = "/getListRsByUser/{userLogin}/{search}", method = RequestMethod.POST)
	public RestResponse listUser(
			@PathVariable("userLogin") String userLogin,
			@PathVariable("search")String search,
			@RequestBody Map<String, Object> mapInput) {
		Map<String, Object> map = mstAuthRsPicSvc.getRsByUser(mapInput,
				userLogin, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/getUserGadget/{userLogin}", method = RequestMethod.POST)
	public RestResponse userGadget(@PathVariable("userLogin") String userLogin,
			@RequestBody Map<String, Object> mapInput) {
		
		Map<String, Object> map = mstAuthRsPicSvc.getUserGadgetByUserLogin(mapInput, userLogin);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/getRsNotIn", method = RequestMethod.POST)
	public RestResponse rsNotIn(@RequestBody Map<String, Object> mapInput) {
		
		Map<String, Object> map = mstAuthRsPicSvc.getRsNotIn(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}


	@RequestMapping(value = "/savePicRs", method = RequestMethod.POST)
	public RestResponse savePicRs(@RequestBody AuthRsPicDto mstAuthRsPicDto){
		int status;
		String message;
		try {
			mstAuthRsPicSvc.save(mstAuthRsPicDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value = "/saveUserGadget", method = RequestMethod.POST)
	public RestResponse saveUserGadget(@RequestBody AuthUserGadgetDto mstAuthUserGadgetDto){
		int status;
		String message;
		try {
			mstAuthRsPicSvc.save(mstAuthUserGadgetDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}
	
	@RequestMapping(value = "/updateAuthUser", method = RequestMethod.POST)
	public RestResponse updateAuthUser(@RequestBody AuthUserDto mstAuthUserDto){
		int status;
		String message;
		try {
			mstAuthRsPicSvc.update(mstAuthUserDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}


	@RequestMapping(value = "/deletePicRs", method = RequestMethod.POST)
	public RestResponse deletePicRs(@RequestBody AuthRsPicDto mstAuthRsPicDto){
		int status;
		String message;
		try {
			mstAuthRsPicSvc.delete(mstAuthRsPicDto);
			status = CommonConstants.OK_REST_STATUS;
			message = "I001";
		} catch (Exception e) {
			status = CommonConstants.ERROR_REST_STATUS;
			message = "E003";
		}
		return new RestResponse(status, message, new HashMap<String, Object>());
	}

}
