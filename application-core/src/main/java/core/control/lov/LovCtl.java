package core.control.lov;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import share.PlDisposisiDto;
import share.PlRegisterSementaraDto;
import common.model.RestResponse;
import common.util.CommonConstants;
import common.util.StringUtil;
import core.dao.lov.LovDao;
import core.service.lov.LovSvc;

@RestController
@RequestMapping("/Lov")
public class LovCtl {
	
	@Autowired
	LovSvc lovSvc;
	
	@RequestMapping(value = "/wilayahKantor", method = RequestMethod.POST)
	public RestResponse listLoad(
			@RequestBody Map<String, Object> mapInput) {
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = lovSvc.lovWilayahKantor(mapInput, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/wilayahKantor/", method = RequestMethod.POST)
	public RestResponse laodInstansi(
			@RequestBody Map<String, Object> mapInput) {
		
		Map<String, Object> map = lovSvc.lovWilayahKantor(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value = "/kodeNamaRs/{kodeWilayah}", method = RequestMethod.POST)
	public RestResponse listKodeNamaRs(
			@PathVariable("kodeWilayah")String kodeWilayah,
			@RequestBody Map<String, Object> mapInput) {

		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = lovSvc.listKodeNamaRs(mapInput, kodeWilayah, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/getAsalBerkas", method=RequestMethod.POST)
	public RestResponse listAsalBerkas(
			@RequestBody Map<String, Object> mapInput){
		
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = lovSvc.listAsalBerkas(mapInput, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListSamsat", method=RequestMethod.POST)
	public RestResponse listSamsat(
			@RequestBody Map<String, Object> mapInput){
		
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");

		Map<String, Object> map = lovSvc.listSamsat(mapInput, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListInstansi", method=RequestMethod.POST)
	public RestResponse listInstansi(
			@RequestBody Map<String, Object> mapInput){
		
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = lovSvc.listInstansi(mapInput, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}

	@RequestMapping(value="/getListInstansiPembuat", method=RequestMethod.POST)
	public RestResponse listInstansiPembuat(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listInstansiPembuat(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}

	@RequestMapping(value="/getListLingkupJaminan", method=RequestMethod.POST)
	public RestResponse listLingkupJaminan(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listLingkupJaminan(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListJenisPertanggungan", method=RequestMethod.POST)
	public RestResponse listJenisPertanggungan(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listJenisPertanggungan(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListSifatCidera", method=RequestMethod.POST)
	public RestResponse listSifatCidera(
			@RequestBody Map<String, Object> mapInput){
		
		
		Map<String, Object> map = lovSvc.listSifatCidera(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListSifatKecelakaan", method=RequestMethod.POST)
	public RestResponse listSifatKecelakaan(
			@RequestBody Map<String, Object> mapInput){
		
		
		Map<String, Object> map = lovSvc.listSifatKecelakaan(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListLokasi", method=RequestMethod.POST)
	public RestResponse listLokasi(
			@RequestBody Map<String, Object> mapInput){
		
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		
		Map<String, Object> map = lovSvc.listLokasi(mapInput, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getAllLokasi", method=RequestMethod.POST)
	public RestResponse getAllLokasi(){
		Map<String, Object> map = lovSvc.getAllLokasi();
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/findOneLokasi", method=RequestMethod.POST)
	public RestResponse findOneLokasi(
			@RequestBody Map<String, Object> mapInput){
		
		String kodeLokasi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeLokasi"), "%%"),"%");
		
		Map<String, Object> map = lovSvc.findOneLokasi(mapInput, kodeLokasi);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListKasusKecelakaan", method=RequestMethod.POST)
	public RestResponse listKasusKecelakaan(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listKasusKecelakaan(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListStatusKendaraan", method=RequestMethod.POST)
	public RestResponse listStatusKendaraan(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listStatusKendaraan(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListJenisSim", method=RequestMethod.POST)
	public RestResponse listJenisSim(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listJenisSim(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListMerkKendaraan", method=RequestMethod.POST)
	public RestResponse listMerkKendaraan(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listMerkKendaraan(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListIdentitas", method=RequestMethod.POST)
	public RestResponse listIdentitas(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listIdentitas(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListJenisPekerjaan", method=RequestMethod.POST)
	public RestResponse listJenisPekerjaan(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listJenisPekerjaan(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListStatusKorban", method=RequestMethod.POST)
	public RestResponse listStatusKorban(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listStatusKorban(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListStatusNikah", method=RequestMethod.POST)
	public RestResponse listStatusNikah(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listStatusNikah(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListProvinsi", method=RequestMethod.POST)
	public RestResponse listProvinsi(
			@RequestBody Map<String, Object> mapInput){
		
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		if(search.contains("-")){
			search = StringUtil.nevl((String)mapInput.get("search"), "").substring(0,2);
		}
		
		Map<String, Object> map = lovSvc.listProvinsi(mapInput, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListKabkota/{kodeProvinsi}", method=RequestMethod.POST)
	public RestResponse listKabkota(
			@PathVariable("kodeProvinsi") String kodeProvinsi,
			@RequestBody Map<String, Object> mapInput){
		
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		if(search.contains("-")){
			search = StringUtil.nevl((String)mapInput.get("search"), "").substring(0,4)+"%";
		}
		
		Map<String, Object> map = lovSvc.listKabkotaByProvinsi(mapInput, kodeProvinsi, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListCamat/{kodeKabkota}", method=RequestMethod.POST)
	public RestResponse listCamat(
			@PathVariable("kodeKabkota")String kodeKabkota,
			@RequestBody Map<String, Object> mapInput){
		
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		if(search.contains("-")){
			search = StringUtil.nevl((String)mapInput.get("search"), "").substring(0,4)+"%";
		}
		
		Map<String, Object> map = lovSvc.listCamatByKabkota(mapInput, kodeKabkota, search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListJenisDokumen", method=RequestMethod.POST)
	public RestResponse listCamat(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listJenisDokumen(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	//maryo find all rs
	@RequestMapping(value="/findAllRS",method = RequestMethod.POST)
	public RestResponse loadAllRS(@RequestBody Map<String, Object> mapInput){
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = lovSvc.findAllRS(search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/findAllKantor",method = RequestMethod.POST)
	public RestResponse loadAlKantor(@RequestBody Map<String, Object> mapInput){
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = lovSvc.findAllKantor(search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	@RequestMapping(value="/findAllKantorByLogin",method = RequestMethod.POST)
	public RestResponse loadAlKantorByLogin(@RequestBody Map<String, Object> mapInput){
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = lovSvc.findAllKantorByLogin(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null,
				map.get("contentData"), (long) map.get("totalRecords"));
	}
	
	//added by luthfi
	@RequestMapping(value="/getListLoketPj", method=RequestMethod.POST)
	public RestResponse listLoketPj(
			@RequestBody Map<String, Object> mapInput){
		
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		String kodeKantorJr = (String)mapInput.get("kodeKantorJr");

		if(search.contains("-")){
			search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%").substring(0,7),"%");
		}
		
		Map<String, Object> map = lovSvc.listLoketPj(mapInput, search, kodeKantorJr);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListJenisKendaraan", method=RequestMethod.POST)
	public RestResponse listJenisKendaraan(
			@RequestBody Map<String, Object> mapInput){
				
		Map<String, Object> map = lovSvc.listJenisKendaraan(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}

	@RequestMapping(value="/getListKantor/{userSession}", method=RequestMethod.POST)
	public RestResponse listKantor(
			@PathVariable ("userSession") String userSession,
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listKantor(mapInput,userSession);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
		@RequestMapping(value="/getListStatusProses", method=RequestMethod.POST)
		public RestResponse listStatusProses(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listStatusProses(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
		
		@RequestMapping(value="/getListStatusPenyelesaian", method=RequestMethod.POST)
		public RestResponse listStatusPenyelesaian(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.ListStatusPenyelesaian(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
		
		@RequestMapping(value="/getListStatusHubungan", method=RequestMethod.POST)
		public RestResponse listStatusHubungan(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.ListHubunganKorban(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
		
		
		@RequestMapping(value="/getListId", method=RequestMethod.POST)
		public RestResponse listjenisIdentitas(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.ListId(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
		
	@RequestMapping(value="/getLokasiByCamat/{kodeCamat}", method=RequestMethod.POST)
	public RestResponse listLokasiByCamat(
		@PathVariable("kodeCamat")String kodeCamat,
		@RequestBody Map<String, Object> mapInput){
	Map<String, Object> map = lovSvc.getCamatByLokasi(mapInput, kodeCamat);
	return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getCamatByLokasi/{kodeLokasi}", method=RequestMethod.POST)
	public RestResponse listCamatByLokasi(
		@PathVariable("kodeLokasi")String kodeLokasi,
		@RequestBody Map<String, Object> mapInput){
	Map<String, Object> map = lovSvc.getLokasiByCamat(mapInput, kodeLokasi);
	return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListJenisPembayaran", method=RequestMethod.POST)
	public RestResponse listJenisPembayaran(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listJenisPembayaran(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getKepalaCabangByUser", method=RequestMethod.POST)
	public RestResponse getKepalaCabangByUser(
			@RequestBody Map<String, Object> mapInput){
		
		String kantorUser = (String) mapInput.get("kantorUser"+"%");

		Map<String, Object> map = lovSvc.getKepalaCabangByUser(mapInput, kantorUser);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/allUser",method=RequestMethod.POST)
	public RestResponse listAllUser(@RequestBody Map<String, Object> mapInput){
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = lovSvc.findAllUser(search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));
	}
	
	//added by luthfi
	//list keseimpulan sementara
	@RequestMapping(value="/getListKesimpulanSementara", method=RequestMethod.POST)
	public RestResponse listKesimpulanSementara(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listKesimpulanSementara(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListOtorisasiFlag", method=RequestMethod.POST)
	public RestResponse listOtorisasiFlag(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listOtorisasiFlag(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListKodeJenisPembayaran", method=RequestMethod.POST)
	public RestResponse listKodeJenisPembayaran(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listKodeJenisPembayaran(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getListDisposisi", method=RequestMethod.POST)
	public RestResponse listDisposisi(
			@RequestBody Map<String, Object> mapInput){
		
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		String statusProses = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("statusProses"), "%%"),"%");
		String levelKantor = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("levelKantor"), "%%"),"%");

		Map<String, Object> map = lovSvc.getListDisposisi(mapInput,noBerkas, statusProses, levelKantor);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/kantorByKode",method=RequestMethod.POST)
	public RestResponse kantorByKode(@RequestBody Map<String, Object> mapInput){
		String kodeKantorJr = (String)mapInput.get("kodeKantorJr");
		Map<String, Object> map = lovSvc.getOneKantorByKode(kodeKantorJr);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		

	}
	
	@RequestMapping(value="/getKodeDisposisi", method=RequestMethod.POST)
	public RestResponse kodeDisposisi(
			@RequestBody Map<String, Object> mapInput){
		String statusProses = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("statusProses"), "%%"),"%");
		String levelKantor = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("levelKantor"), "%%"),"%");
		Map<String, Object> map = lovSvc.getKodeDisposisi(mapInput, statusProses, levelKantor);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getKantorByKodeKantor", method=RequestMethod.POST)
	public RestResponse listKantor(
			@RequestBody Map<String, Object> mapInput){
		String kodeKantor = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeKantor"), "%%"),"%");
		Map<String, Object> map = lovSvc.findOneByKodeKantor(mapInput, kodeKantor);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/findOneRs", method=RequestMethod.POST)
	public RestResponse findOneRs(
			@RequestBody Map<String, Object> mapInput){
		String kodeRumahSakit = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeRumahSakit"), "%%"),"%");
		Map<String, Object> map = lovSvc.findOneRS(kodeRumahSakit);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}

	//added by luthfi
	@RequestMapping(value="/getPenyelesaianSantunanByNoBerkas", method=RequestMethod.POST)
	public RestResponse penyelesaianSantunan(
			@RequestBody Map<String, Object> mapInput){
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		Map<String, Object> map = lovSvc.findOnePenyelesaianSantunanByNoBerkas(mapInput, noBerkas);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	//added by luthfi 
	@RequestMapping(value="/getListKodeDisposisi", method=RequestMethod.POST)
	public RestResponse listKodeDisposisi(
			@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.listKodeDisposisi(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	//added by luthfi
	//get kantor dilimpahkan otorisasi
	@RequestMapping(value="/getKantorDilimpahkan", method=RequestMethod.POST)
	public RestResponse getKantorDilimpahkan(
			@RequestBody Map<String, Object> mapInput){
		String idKorbanKecelakaan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idKorbanKecelakaan"), "%%"),"%");
		Map<String, Object> map = lovSvc.getKantorDilimpahkan(mapInput, idKorbanKecelakaan);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	//added by luthfi
	//get value ldbp
	@RequestMapping(value="/getValueLdbp", method=RequestMethod.POST)
	public RestResponse getValueLdbp(
			@RequestBody Map<String, Object> mapInput){
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		Map<String, Object> map = lovSvc.getValueLdbp(mapInput, noBerkas);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/getValueLdbpList", method=RequestMethod.POST)
	public RestResponse getValueLdbpList(
			@RequestBody Map<String, Object> mapInput){
		String levelKantor = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("levelKantor"), "%%"),"%");
		String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
		Map<String, Object> map = lovSvc.getValueLdbpListDisposisi(mapInput, levelKantor, noBerkas);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	@RequestMapping(value="/allBank", method=RequestMethod.POST)
	public RestResponse findAllBank(
			@RequestBody Map<String, Object> mapInput){
		String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
		Map<String, Object> map = lovSvc.findAllBank(search);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
	}
	
	//added by Meilona
		//metode Pembayaran verifikasi dan pengesahan
		@RequestMapping(value="/getMetodePembayaran", method=RequestMethod.POST)
		public RestResponse getMetodePembayaran(
				@RequestBody Map<String, Object> mapInput){
			String noBerkas = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noBerkas"), "%%"),"%");
			Map<String, Object> map = lovSvc.getMetodePembayaran(mapInput, noBerkas);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		@RequestMapping(value = "/refCode", method = RequestMethod.POST)
		public RestResponse loadRefCode(@RequestBody Map<String, Object> mapInput){
			String flag;
			String rvDomain;
			String search;
			search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");
			rvDomain = (String)mapInput.get("rvDomain");
			if (mapInput.get("flag") == null || mapInput.get("flag").equals("")) {
				flag = "%%";
			} else {
				flag = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("flag"), "%%"),"%");
			}
			
			Map<String, Object> map = lovSvc.loadJRRefCode(rvDomain,flag,search);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null,
					map.get("contentData"), (long) map.get("totalRecords"));
		}
		
		//added by luthfi 
		@RequestMapping(value="/getDisposisiById", method=RequestMethod.POST)
		public RestResponse getDispisiById(
				@RequestBody Map<String, Object> mapInput){
			String idDisposisi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("idDisposisi"), "%%"),"%");

			Map<String, Object> map = lovSvc.findDisposisiByIdDisposisi(mapInput, idDisposisi);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		//added by luthfi
		//delete disposisi
		@RequestMapping(value = "/deleteDisposisi", method = RequestMethod.POST)
		public RestResponse deleteDisposisi(@RequestBody PlDisposisiDto plDisposisiDto){
			int status;
			String message;
			try {
				lovSvc.deleteDisposisiById(plDisposisiDto);
				status = CommonConstants.OK_REST_STATUS;
				message = "I001";
			} catch (Exception e) {
				status = CommonConstants.ERROR_REST_STATUS;
				message = "E003";
				e.printStackTrace();
			}
			return new RestResponse(status, message, new HashMap<String, Object>());
		}
		
		//added by luthfi 
		//isntansi monitoring irsms
		@RequestMapping(value="/getInstansiIrsms", method=RequestMethod.POST)
		public RestResponse getInstansiIrsms(
				@RequestBody Map<String, Object> mapInput){
			String kodeInstansi = (String) mapInput.get("kodeInstansi");
			String search = mapInput.get("search")+"%";

			Map<String, Object> map = lovSvc.instansiLakaIrsms(mapInput, kodeInstansi, search);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		//added by Meilona
		//Laka Belum Tuntas
		@RequestMapping(value="/getJenisLaporan", method=RequestMethod.POST)
		public RestResponse listJenisLaporan(
				@RequestBody Map<String, Object> mapInput){
			Map<String, Object> map = lovSvc.getJenisLaporan(mapInput);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		//Laka Rs
		@RequestMapping(value="/getResponse", method=RequestMethod.POST)
		public RestResponse listResponse(
				@RequestBody Map<String, Object> mapInput){
			Map<String, Object> map = lovSvc.getResponse(mapInput);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		//cari data laka
		@RequestMapping(value="/getListCariDataLaka", method = RequestMethod.POST)
		public RestResponse loadList(
				@RequestBody Map<String, Object>mapInput){
			
			String kejadianStartDate = (String) mapInput.get("kejadianStartDate");
			String kejadianEndDate = (String) mapInput.get("kejadianEndDate");
			String laporanStartDate = (String) mapInput.get("laporanStartDate");
			String laporanEndDate = (String) mapInput.get("laporanEndDate");
			String instansi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("instansi"), "%%"),"%");
			if(instansi.contains("-")){
				instansi = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("instansi"), "%%").substring(0,7),"%");
			}
			String noLaporan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("noLaporan"), "%%"),"%");
			String namaKorban = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("namaKorban"), "%%"),"%");
			String jenisPertanggungan = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("jenisPertanggungan"), "%%"),"%");		
			String statusLp = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("statusLp"), "%%"),"%");	
			String menu = (String) mapInput.get("menu");
			Map<String, Object> map = lovSvc.listCariDataLaka(mapInput, kejadianStartDate, kejadianEndDate, laporanStartDate, laporanEndDate, instansi, noLaporan, namaKorban, jenisPertanggungan, statusLp, menu);

			return new RestResponse(CommonConstants.OK_REST_STATUS, null,
					map.get("contentData"), (long) map.get("totalRecords"));
		}
		
		//added by luthfi
		//List keterangan coklit
		@RequestMapping(value="/getListKetCoklit", method=RequestMethod.POST)
		public RestResponse listKetCoklit(@RequestBody Map<String, Object> mapInput){
			Map<String, Object> map = lovSvc.getKeteranganCoklit(mapInput);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		//get list gadget
		@RequestMapping(value="/getListGadget", method=RequestMethod.POST)
		public RestResponse listGadget(@RequestBody Map<String, Object> mapInput){
			Map<String, Object> map = lovSvc.listGadget(mapInput);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		//added by Meilona
				//list Pic Laka RS
		@RequestMapping(value="/getPicLakaRs", method=RequestMethod.POST)
		public RestResponse listPicLakaRs(@RequestBody Map<String, Object> mapInput){
		Map<String, Object> map = lovSvc.getPicLakaRs(mapInput);
		return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
				}
				
		//AddListResponse monitoring data laka Rs
		@RequestMapping(value="/getAddResponseList", method=RequestMethod.POST)
		public RestResponse addListResponse(@RequestBody Map<String, Object> mapInput){
			Map<String, Object> map = lovSvc.getAddResponList(mapInput);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		//AddListResponse monitoring data laka Rs
		@RequestMapping(value="/getListKesimpulanSurvey", method=RequestMethod.POST)
		public RestResponse listKesimpulanSurvey(@RequestBody Map<String, Object> mapInput){
			Map<String, Object> map = lovSvc.getKesimpulanSurvey(mapInput);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		//get All prov, kota by kode camat
		@RequestMapping(value="/getAllByCamat", method=RequestMethod.POST)
		public RestResponse gettAllByCamat(@RequestBody Map<String, Object> mapInput){
			
			String kodeCamat = (String) mapInput.get("kodeCamat");

			Map<String, Object> map = lovSvc.getAllByCamat(kodeCamat);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		//get map bpjs by kode rs
		@RequestMapping(value="/getBpjsByKodeRs",method=RequestMethod.POST)
		public RestResponse getBpjsByKodeRs(@RequestBody Map<String, Object> mapInput){
			String kodeRumahsakit = (String)mapInput.get("kodeRumahsakit");
			Map<String, Object> map = lovSvc.getBpjsByKodeRs(kodeRumahsakit);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		@RequestMapping(value="/findOneBank", method=RequestMethod.POST)
		public RestResponse findOneBank(
				@RequestBody Map<String, Object> mapInput){
			String kodeBank = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("kodeBank"), "%%"),"%");
			Map<String, Object> map = lovSvc.findOneBank(kodeBank);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		@RequestMapping(value="/getUserByLogin", method=RequestMethod.POST)
		public RestResponse getUserByLogin(
				@RequestBody Map<String, Object> mapInput){
			String login = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("login"), "%%"),"%");
			Map<String, Object> map = lovSvc.getUserByLogin(mapInput, login);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}
		
		@RequestMapping(value="/getListJenisTl", method=RequestMethod.POST)
		public RestResponse listJenisTl(
				@RequestBody Map<String, Object> mapInput){
			Map<String, Object> map = lovSvc.listJenisTl(mapInput);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}

		@RequestMapping(value="/getListLoketKantor", method=RequestMethod.POST)
		public RestResponse getListLoketKantor(
				@RequestBody Map<String, Object> mapInput){
			String search = StringUtil.surroundString(StringUtil.nevl((String)mapInput.get("search"), "%%"),"%");

			Map<String, Object> map = lovSvc.listLoketKantor(mapInput, search);
			return new RestResponse(CommonConstants.OK_REST_STATUS, null, map.get("contentData"),(long)map.get("totalRecords"));		
		}

}

