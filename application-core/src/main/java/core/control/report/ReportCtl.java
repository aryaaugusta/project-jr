package core.control.report;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import common.model.RestResponse;
import core.service.report.ReportSvc;

@RestController
@RequestMapping("/report")
public class ReportCtl {

	@Autowired
	ReportSvc reportSvc;
	
	@RequestMapping(value="/general", method = RequestMethod.POST)
	public RestResponse callGeneral(
			@RequestBody Map<String, Object> input){
		Map<String, Object> map = reportSvc.callGeneral(input);
		return new RestResponse( (int)map.get("status"), null,
				map, (Integer) map.get("total"));
		
		
	}
	
}
