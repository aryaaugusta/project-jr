package common.ui;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTVMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;
import org.zkoss.zk.ui.WrongValuesException;

public class DocumentUtil {

	protected String getHari(Date tgl){
		if (tgl == null){
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("E", Locale.ENGLISH);
		String ha = sdf.format(tgl);
		return 	ha.equalsIgnoreCase("Mon")?"Senin":
				ha.equalsIgnoreCase("Tue")?"Selasa":
				ha.equalsIgnoreCase("Wed")?"Rabu":
				ha.equalsIgnoreCase("Thu")?"Kamis":
				ha.equalsIgnoreCase("Fri")?"Jum'at":
				ha.equalsIgnoreCase("Sat")?"Sabtu":
				ha.equalsIgnoreCase("Sun")?"Minggu":"";
	}
	
	protected String getBulan(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String month = sdf.format(tgl);
		return month.equalsIgnoreCase("01")?"Januari": 
			   month.equalsIgnoreCase("1")?"Januari":
			   month.equalsIgnoreCase("2")?"Februari":
			   month.equalsIgnoreCase("3")?"Maret":
			   month.equalsIgnoreCase("4")?"April":
			   month.equalsIgnoreCase("5")?"Mei":
			   month.equalsIgnoreCase("6")?"Juni":
			   month.equalsIgnoreCase("7")?"Juli":
			   month.equalsIgnoreCase("8")?"Agustus":
			   month.equalsIgnoreCase("9")?"September":
			   month.equalsIgnoreCase("02")?"Februari":
			   month.equalsIgnoreCase("03")?"Maret":
			   month.equalsIgnoreCase("04")?"April":
			   month.equalsIgnoreCase("05")?"Mei":
			   month.equalsIgnoreCase("06")?"Juni":
			   month.equalsIgnoreCase("07")?"Juli":
			   month.equalsIgnoreCase("08")?"Agustus":
			   month.equalsIgnoreCase("09")?"September":
			   month.equalsIgnoreCase("10")?"Oktober":
			   month.equalsIgnoreCase("11")?"Nopember":
			   month.equalsIgnoreCase("12")?"Desember":"";
	}
	
	protected String getJam(Date tgl){
		if(tgl==null){
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(tgl);
	}
	
	protected String getTanggal(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("DD", Locale.ENGLISH);
		return sdf.format(tgl);
	}
	
	protected String getTahun(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
		return sdf.format(tgl);
	}
	
	protected String getFormatTanggal(Date tgl){
		return getHari(tgl)+" "+getBulan(tgl)+" "+getTahun(tgl);
	}
	
	protected String getDateFormat(Date tgl){
		if(tgl==null){
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/YYYY");
		return sdf.format(tgl);
	}
	/**
	 * set border of table
	 * <pre>
	 * setTableBorder(tableName) or setTableBorder(tableName, 0)
	 * </pre>
	 * will set border none to current table
	 * <pre>
	 * setTableBorder(tableName, 0, 1)
	 * </pre>
	 * will set inner border of table,
	 * @param table
	 * @param option
	 */
	protected void setTableBorder(XWPFTable table, int...option){
		CTTblPr tblpro = table.getCTTbl().getTblPr();
		CTTblWidth tblW = tblpro.addNewTblW();
		CTTblBorders border = tblpro.addNewTblBorders();
		int len = option.length;
		
		if(len>0 && len==1){
			if(option[0]==0){
				border.addNewBottom().setVal(STBorder.NONE);
				border.addNewLeft().setVal(STBorder.NONE);
				border.addNewRight().setVal(STBorder.NONE);
				border.addNewTop().setVal(STBorder.NONE);
				border.addNewInsideH().setVal(STBorder.NONE);
				border.addNewInsideV().setVal(STBorder.NONE);
			}else if(option[0]==1){
				border.addNewBottom().setVal(STBorder.BASIC_THIN_LINES);
				border.addNewLeft().setVal(STBorder.BASIC_THIN_LINES);
				border.addNewRight().setVal(STBorder.BASIC_THIN_LINES);
				border.addNewTop().setVal(STBorder.BASIC_THIN_LINES);
				border.addNewInsideH().setVal(STBorder.BASIC_THIN_LINES);
				border.addNewInsideV().setVal(STBorder.BASIC_THIN_LINES);
			}
		}else if(len>0 && len<=2){
			if(option[0]==0){
				border.addNewBottom().setVal(STBorder.NONE);
				border.addNewLeft().setVal(STBorder.NONE);
				border.addNewRight().setVal(STBorder.NONE);
				border.addNewTop().setVal(STBorder.NONE);
				if(option[1]==0){
					border.addNewInsideH().setVal(STBorder.NONE);
					border.addNewInsideV().setVal(STBorder.NONE);
				}else if(option[1]==1){
					border.addNewInsideH().setVal(STBorder.BASIC_THIN_LINES);
					border.addNewInsideV().setVal(STBorder.BASIC_THIN_LINES);
				}
			}else if(option[0]==1){
				border.addNewBottom().setVal(STBorder.BASIC_THIN_LINES);
				border.addNewLeft().setVal(STBorder.BASIC_THIN_LINES);
				border.addNewRight().setVal(STBorder.BASIC_THIN_LINES);
				border.addNewTop().setVal(STBorder.BASIC_THIN_LINES);
				if(option[1]==1){
					border.addNewInsideH().setVal(STBorder.BASIC_THIN_LINES);
					border.addNewInsideV().setVal(STBorder.BASIC_THIN_LINES);
				}else if(option[1]==0){
					border.addNewInsideH().setVal(STBorder.NONE);
					border.addNewInsideV().setVal(STBorder.NONE);
				}
			}
		}else{
			border.addNewBottom().setVal(STBorder.NONE);
			border.addNewLeft().setVal(STBorder.NONE);
			border.addNewRight().setVal(STBorder.NONE);
			border.addNewTop().setVal(STBorder.NONE);
			border.addNewInsideH().setVal(STBorder.NONE);
			border.addNewInsideV().setVal(STBorder.NONE);
		}
	}
	
	/**
	 * will create new row, depending how many parameter take,
	 *  it will create column as many as input param
	 * @param table
	 * @param a
	 */
	protected void addRow(XWPFTable table, String...a){
		if (a.length>0){
			XWPFTableRow row = table.createRow();
			for(int x = 0; x<a.length; x++){
				if(x>0 && (row.getCell(x)==null)){
					row.addNewTableCell().setText(a[x]);
				}else{
					row.getCell(x).setText(a[x]);
				}
			}
		}
	}
	
	protected void addFirstRow(XWPFTable table, String...a){
		if (a.length>0){
			XWPFTableRow row = table.createRow();
			row.getCell(0).setText(a[0]);
			for(int x = 1; x<a.length; x++){
				row.addNewTableCell().setText(a[x]);
			}
		}
	}
	
	protected void setCellWidth(XWPFTable tabel, int column, long value){
		tabel.getRow(0).getCell(column).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(value));
	}
	
	protected void setCellWidth(XWPFTableRow baris, int column, long value){
		baris.getCell(column).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(value));
	}
	
	public void setTableAlign(XWPFTable table,ParagraphAlignment align) {
	    CTTblPr tblPr = table.getCTTbl().getTblPr();
	    CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
	    STJc.Enum en = STJc.Enum.forInt(align.getValue());
	    jc.setVal(en);
	}
	
	/**
	 *  set whether alignment for each paragraph using {@code Apache Poi}.
	 * <pre>
	 *    setAlignment(paragraph, 1);
	 * </pre>
	 * will make align Center
	 * 
	 * @param paragraph
	 * @param align 
	 *         a value less than {@code 0} if {@code x < y}; and
	 *         a value greater than {@code 0} if {@code x > y}
	 * 
	 * 1 for align Center, 
	 * 2 for align Left, 
	 * 3 for align Right,
	 * 4 for justify
	 */
	public void setAlignment(XWPFParagraph paragraph, int align){
		if(align == 1)
			paragraph.setAlignment(ParagraphAlignment.CENTER);
		else if(align == 2)
			paragraph.setAlignment(ParagraphAlignment.LEFT);
		else if(align == 3)
			paragraph.setAlignment(ParagraphAlignment.RIGHT);
		else if(align == 4)
			paragraph.setAlignment(ParagraphAlignment.BOTH);
	}
	
	/**
	 *  Set font family to Calibri, then use option to make it Bold, Italic, or Capitalized. 
	 *  Uses:
	 * <pre>
	 *    setRunFontText(run) to set font text to calibri,
	 *    setRunFontText(run, 1) to add option to bold (use other number instead of 1 to disable bold)
	 *    SetRunFontText(run, 1, 1) to add other option to Italic (use other number instead of 1 to disable italic)
	 *    SetRunFontText(run, 1, 1, 1) to add other option to Capitalized font (use other number instead of 1 to disable capitalized)
	 * 
	 * @param run
	 * @param option 
	 * 
	 */
	protected void setRunFontText(XWPFRun run, int...option){
		run.setFontFamily("calibri");
		if(option.length>0){
			run.setBold(option[0]==1?true:false);
			if(option.length>1){
				run.setItalic(option[1]==1?true:false);
				if(option.length>2){
					run.setCapitalized(option[2]==1?true:false);
				}
			}
		}
	}
	
	protected void setHMerge(XWPFTable tabel, int row, int startFrom, int untilColumn ) throws Exception{
		CTHMerge hMerge = CTHMerge.Factory.newInstance();
		hMerge.setVal(STMerge.RESTART);
		getTcPr(tabel.getRow(row).getCell(startFrom)).setHMerge(hMerge);
		if(untilColumn>startFrom){
			for(int i = startFrom+1; i<=untilColumn; i++){
				CTHMerge hMerges = CTHMerge.Factory.newInstance();
				hMerges.setVal(STMerge.CONTINUE);
				getTcPr(tabel.getRow(row).getCell(i)).setHMerge(hMerges);
			}
		}else{
			for(int i = untilColumn; i < startFrom ;i++){
				CTHMerge hMerges = CTHMerge.Factory.newInstance();
				hMerges.setVal(STMerge.CONTINUE);
				getTcPr(tabel.getRow(row).getCell(startFrom-i)).setHMerge(hMerges);
			}
		}
	}
	
	protected void setVMerge(XWPFTable table, int column, int startFrom, int untilRow ) throws WrongValuesException{
		CTVMerge vMerge = CTVMerge.Factory.newInstance();
		vMerge.setVal(STMerge.RESTART);
		getTcPr(table.getRow(startFrom).getCell(column)).setVMerge(vMerge);
		if(untilRow>startFrom){
			for(int i = startFrom+1; i<=untilRow; i++){
				CTVMerge vMerges = CTVMerge.Factory.newInstance();
				vMerges.setVal(STMerge.CONTINUE);
				getTcPr(table.getRow(i).getCell(column)).setVMerge(vMerges);
			}
		}else{
			CTVMerge vMerges = CTVMerge.Factory.newInstance();
			for(int i = untilRow; i < startFrom; i++){
				vMerges.setVal(STMerge.CONTINUE);
				getTcPr(table.getRow(startFrom-i).getCell(column)).setVMerge(vMerges);
			}
		}
	}
	
	protected void spanCellsAcrossRow(XWPFTable table, int rowNum, int colNum, int span) {
	    XWPFTableCell  cell = table.getRow(rowNum).getCell(colNum);
	    if (cell.getCTTc().getTcPr() == null) 
	    	cell.getCTTc().addNewTcPr();
	    
	    cell.getCTTc().getTcPr().addNewGridSpan();
	    cell.getCTTc().getTcPr().getGridSpan().setVal(BigInteger.valueOf((long)span));
	}
	
	protected CTTcPr getTcPr(XWPFTableCell cell){
		CTTcPr tcPr;
		if(cell.getCTTc().getTcPr()!=null){
			tcPr = cell.getCTTc().getTcPr();
		}else{
			tcPr = cell.getCTTc().addNewTcPr();
		}
		/*	try{
			return cell.getCTTc().getTcPr();
		}catch(NullPointerException e){
			return cell.getCTTc().addNewTcPr();
		}
		 */	
		return tcPr;
	}
	
}
