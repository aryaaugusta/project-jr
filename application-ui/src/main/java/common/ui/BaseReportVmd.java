package common.ui;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.annotation.Init;

@Init(superclass=true)
public class BaseReportVmd extends BaseVmd implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Map<String, Object> params = new HashMap<>();
	//tipeTgl: tgl_kejadian, tgl_pengajuan, tgl_penyelesaian
	private String tglKejadian = "tgl_kejadian";
	private String tglPengajuan = "tgl_pengajuan";
	private String tglPenyelesaian = "tgl_penyelesaian";
	private String modeTglKej = "KEJADIAN";
	private String modeTglEnt = "ENTRY";
	
	public String getModeTglKej(){
		return modeTglKej;
	}
	
	public String getModeTglEnt(){
		return modeTglEnt;
	}
	
	public String getModTglKejadian(){
		return tglKejadian;
	}
	
	public String getModTglPengajuan(){
		return tglPengajuan;
	}
	
	public String getModTglPenyelesaian(){
		return tglPenyelesaian;
	}
	
	protected Map<String, Object> getTipeLaporan(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("TipeLaporan", input);
		map.put("TIPE_LAPORAN", input);
		map.put("Jenis_Laporan", input);
		map.put("jenis_laporan", input);
		map.put("JnsLaporan", input);
		map.put("jenislaporan", input);
		map.put("JENIS_LAPORAN", input);
		return map;
	}
	
	protected Map<String, Object> getKodeKantor(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("KdKantor", input);
		map.put("kodekantor", input);
		map.put("inKodeKantor", input);
		map.put("inkodekantor", input);
		map.put("inKdKantor", input);
		map.put("KODE_KANTOR", input);
		return map;
	}
	
	protected Map<String, Object> getTipeTanggal(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("tipetgl", input);
		map.put("tipeTgl", input);
		map.put("TanggalTipe", input);
		map.put("TANGGAL_TIPE", input);
		map.put("TANGGALTIPE", input);
		map.put("TIPE_TANGGAL", input);
		return map;
	}

	protected Map<String, Object> getTglAwal(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("tglAwal", input);
		map.put("TGL_AWAL", input);
		map.put("dtTglAwal", input);
		map.put("periode_awal", input);
		map.put("Periode_Awal", input);
		map.put("periodeawal", input);
		map.put("PERIODE_AWAL", input);
		map.put("DtTglAwal", input);
		map.put("dtTglAwalA", input);
		map.put("dtTglAwalB", input);
		return map;
	}
	
	protected Map<String, Object> getTglAkhir(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("tglAkhir", input);
		map.put("dtTglAkhirA", input);
		map.put("dtTglAkhirB", input);
		map.put("DtTglAkhir", input);
		map.put("TGL_AKHIR", input);
		map.put("dtTglAkhir", input);
		map.put("periode_akhir", input);
		map.put("Periode_Akhir", input);
		map.put("periodeakhir", input);
		map.put("PERIODE_AKHIR", input);
		return map;
	}
	
	protected Map<String, Object> getSifatCidera(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("sifatCidera", input);
		map.put("sifatcidera", input);
		map.put("SIFAT_CIDERA", input);
		map.put("SIFATCIDERA", input);
		return map;
	}
	
	protected Map<String, Object> getKesimpulan(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("kesimpulan", input);
		map.put("KESIMPULAN", input);
		return map;
	}
	
	protected Map<String, Object> getPilihan(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("pilihan", input);
		map.put("PILIHAN", input);
		return map;
	}
	
	protected Map<String, Object> getKdCidera(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("kdCidera", input);
		map.put("kdcidera", input);
		map.put("KODE_CIDERA", input);
		map.put("KODECIDERA", input);
		map.put("KD_CIDERA", input);
		map.put("KDCIDERA", input);
		return map;
	}

	protected Map<String, Object> getKodeCidera(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("kode_cidera", input);
		map.put("Kode_Cidera", input);
		map.put("KODE_CIDERA", input);
		map.put("KODECIDERA", input);
		map.put("kode_", input);
		return map;
	}

	protected Map<String, Object> getCidera(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("cidera", input);
		map.put("Cidera", input);
		map.put("CIDERA", input);
		return map;
	}
	
	protected Map<String, Object> getSimpulSementara(Object input){
		Map<String, Object> map = new HashMap<>();
		map.put("simpul_smntr", input);
		map.put("simpul_smntr", input);
		map.put("SIMPUL_SMNTR", input);
		return map;
	}
	
	
	//Rekapitulasi Pembayaran Limpahan
	//jenislaporan : Nasional, Cabang, Perwakilan, Loket
	
	//JnsEG : ALL, 
	/*
	 * 3->ExGratia2KendLebih, 
	 * 4->EGMurniLakaTunggal, 
	 * 5->ExGratiaAwakAngkUmum, 
	 * 6->ExGratiaKedaluarsa, 
	 * 7->EG MurniLainnya
	 */
	
	/*
	 * SEMUA
01 - MENINGGAL
02 - LUKA-LUKA
04 - CACAT TETAP
05 - MENINGGAL &amp; LUKA-LUKA
06 - LUKA-LUKA &amp; CACAT TETAP
07 - PENGUBURAN
08 - LUKA-LUKA &amp; PENGUBURAN
09 - MENINGGAL DAN MENINGGAL &amp; LUKA-LUKA
	 */
	
	
	
}
