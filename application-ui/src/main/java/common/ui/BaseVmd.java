package common.ui;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.imageio.ImageIO;














//import sun.misc.BASE64Encoder;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zul.Include;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

import share.DukcapilSentObject;
import share.DukcapilWinDto;
import share.KKDukcapilDto;
import Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdlProxy;
import Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdl_PortType;
import Bpm.Ecms.queryByNoBerkas.DirectoryName;
import Bpm.Ecms.queryByNoBerkas.Response;
import Bpm.Ecms.upload.AttachmentResultType;
import Bpm.Ecms.upload.AttachmentsType;
import Bpm.Ecms.upload.Bpm_ecms_upload_wsdlProxy;
import Bpm.Ecms.upload.Bpm_ecms_upload_wsdl_PortType;
import common.model.RestResponse;
import common.model.RestResponseDukcapil;
import common.model.UserSessionJR;
import common.util.CommonConstants;
import common.util.JsonUtil;
import common.util.StringUtil;

/**
 * Base view model class. All view model that used in application must extend
 * this base class.
 * 
 * @author Leo Sendra
 * 
 */
public abstract class BaseVmd {

	private static Logger logger = LoggerFactory.getLogger(BaseVmd.class);

	private int pageSize = Integer.parseInt(Labels.getLabel("pageSize"));;
	private long totalSize = 0;
	private int activePage = 0;

	private String search = "";
	private String search1 = "";
	private String search2 = "";
	private String search3 = "";
	private String search4 = "";
	private String search5 = "";
	protected String orderBy = ""; // order by entity model column
	protected String currentOrderDirection = CommonConstants.ASC; // desc / asc
																	// order
																	// private
																	// static
																	// final
																	// String[]
																	// BASE64_STRING_IMAGES
																	// = new
																	// String[]
																	// { PNG,
																	// JPG, GIF
																	// };

	private final String WS_URL = Labels.getLabel("wsUrl");
	private final String WS_BPM_URL = Labels.getLabel("wsSOAUrl");
	private final String WS_BPM = "/JR-BPM/BPM/ProxyServices/bpm_get_dukcapil_ps";
	
	private boolean showAdvSearch = false;

	@Init
	public void baseInit() {

		logger.debug("baseInit in BaseVmd is called!");
		if (getPageInfo().isListMode()) {
			onList();
		} else if (getPageInfo().isEditMode()) {
			onEdit();
		} else if (getPageInfo().isAddMode()) {
			onAdd();
		} else if (getPageInfo().isViewMode()) {
			onView();
		} else if (getPageInfo().isTaskMode()) {
			onTask();
		} else if (getPageInfo().isBarcodeMode()) {
			onBarcode();
		} else if (getPageInfo().isEditDetailMode()) {
			onEditDetail();
		} else if (getPageInfo().isAddDetailMode()) {
			onAddDetail();
		} else if (getPageInfo().isViewDetailMode()) {
			onViewDetail();
		} else if (getPageInfo().isDeleteDetailMode()) {
			onDeleteDetail();
		}
	}

	/**
	 * Invoked first time when page in list mode. Child class can be override
	 * this method for further custom.
	 */
	protected void onList() {
		loadList();
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page in add mode
	 */
	protected void onAdd() {
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page in edit mode
	 */
	protected void onEdit() {
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page in view mode
	 */
	protected void onView() {
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page opened via task / inbox from BPM.
	 */
	protected void onTask() {
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page in barcode mode
	 */
	protected void onBarcode() {
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page in add detail mode
	 */
	protected void onAddDetail() {
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page in edit detail mode
	 */
	protected void onEditDetail() {
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page in delete detail mode
	 */
	protected void onDeleteDetail() {
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page in view detail mode
	 */
	protected void onViewDetail() {
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * first time when page in sub detail mode
	 */
	protected void onSubDetail() {
	}

	protected void loadList() {
	}

	/**
	 * Method that will be executed when do sorting / paging. It will do query
	 * with some criteria start from 1st page.
	 */
	@Command("sortAndSearch")
	@NotifyChange("totalSize")
	public void sortAndSearch() {
		setActivePage(0);
	}

	@Command("sort")
	public void sort(@BindingParam("col") String toBeSortedColumn) {
		if (orderBy.equals(toBeSortedColumn)) {
			if (currentOrderDirection.equals(CommonConstants.ASC)) {
				currentOrderDirection = CommonConstants.DESC;
			} else {
				currentOrderDirection = CommonConstants.ASC;
			}
		} else {
			currentOrderDirection = CommonConstants.ASC;
		}

		orderBy = toBeSortedColumn;
		sortAndSearch();
	}

	/**
	 * 
	 * @param popup
	 *            URI LOV popup (ZUL page)
	 * @param globalHandleMethodName
	 *            global method name to be invoked when user choose data in LOV
	 *            popup
	 */
	@Command("showLov")
	public void showLov(
			@BindingParam("popup") String popup,
			@Default(UIConstants.LOV_HANDLER) @BindingParam(UIConstants.LOV_HANDLER) String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();

		// put global command method name that will be invoked after user choose
		// data in LOV popup
		args.put(UIConstants.LOV_HANDLER, globalHandleMethodName);

		if (!beforeCallLov(args, popup))
			return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	/**
	 * Template method to be implemented / override in child class. Invoked
	 * before LOV popup be showed
	 * 
	 * @param args
	 *            HashMap argument to be sent to popup LOV
	 * @param popup
	 *            LOV popup name. Using this variable if we need a filter /
	 *            selection when fill @param args.
	 * 
	 * @return true if proceed to show LOV popup. false to terminate process.
	 */
	protected boolean beforeCallLov(Map<String, Object> args, String popup) {
		return true;
	}

	protected RestResponse callCustomWs(String url, Object sentObject,
			HttpMethod httpMethod) {
		return executeWebService(WS_URL + url, sentObject, httpMethod);
	}

	protected RestResponse callWs(String url, Object sentObject,
			HttpMethod httpMethod) {
		return executeWebService(WS_URL + url, sentObject, httpMethod);
	}

	protected RestResponse callWsWithPaging(String url, Object sentObject,
			HttpMethod httpMethod) {
		return executeWebService(
				WS_URL + url + "/" + getActivePage() + "/" + getPageSize()
						+ "?" + CommonConstants.SEARCH + "="
						+ StringUtil.nevl(getSearch(), "") + "&"
						+ CommonConstants.DIRECTION + "="
						+ StringUtil.nevl(currentOrderDirection, "") + "&"
						+ CommonConstants.ORDER_BY + "="
						+ StringUtil.nevl(orderBy, ""), sentObject, httpMethod);
	}

	// protected RestResponse callWsWithPagingSearch(String url, Object
	// sentObject,
	// HttpMethod httpMethod) {
	// return executeWebService(WS_URL + url
	// + "/" + getActivePage()
	// + "/" + getPageSize()
	// + "?" + CommonConstants.SEARCH + "=" + StringUtil.nevl(getSearch(), "")
	// + "?" + CommonConstants.SEARCH1 + "=" + StringUtil.nevl(getSearch1(), "")
	// + "?" + CommonConstants.SEARCH2 + "=" + StringUtil.nevl(getSearch2(), "")
	// + "?" + CommonConstants.SEARCH3 + "=" + StringUtil.nevl(getSearch3(), "")
	// + "?" + CommonConstants.SEARCH4 + "=" + StringUtil.nevl(getSearch4(), "")
	// + "&" + CommonConstants.DIRECTION + "=" +
	// StringUtil.nevl(currentOrderDirection, "")
	// + "&" + CommonConstants.ORDER_BY + "=" + StringUtil.nevl(orderBy, ""),
	// sentObject, httpMethod);
	// }

	private RestResponse executeWebService(String url, Object sentObject,
			HttpMethod httpMethod) {
		url = url.replace("\\", "");

		logger.info("JSON Object : {} ", JsonUtil.getJson(sentObject));
		logger.info("Invoke web service with URL : {}", url);

		RestTemplate restTemplate = new RestTemplate();
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		final HttpEntity<Object> requestEntity = new HttpEntity<Object>(
				sentObject, headers);
		RestResponse restResponse = null;

		ResponseEntity<RestResponse> reponseEntity = null;
		try {
			reponseEntity = restTemplate.exchange(url, httpMethod,
					requestEntity, RestResponse.class);
			restResponse = reponseEntity.getBody();
			if (restResponse != null
					&& restResponse.getStatus() == CommonConstants.ERROR_BY_PAGE_ERROR) {
				throw new RuntimeException(restResponse.getMessage());
			}
		} catch (Exception e) {
			if (reponseEntity != null) {
				try {
					HttpStatus httpStatus = reponseEntity.getStatusCode();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			restResponse = new RestResponse(CommonConstants.ERROR_REST_STATUS,
					"E150", null);
			e.printStackTrace();

		}

		return restResponse;
	}

	@Command("navigate")
	public void navigate(@BindingParam("uri") String locationUri) {
		Include include = (Include) getCurrentExecution().getDesktop()
				.getPage(UIConstants.PAGE_INDEX_ID)
				.getFellow(UIConstants.PAGE_INCLUDE_ID);
		include.setSrc(locationUri);
	}

	protected void showSmartMsgBox(String keyMessage, String... param) {
		String code = keyMessage.substring(0, 1);
		switch (code) {
		case "E":
			showErrorMsgBox(keyMessage, param);
			break;
		case "I":
			showInfoMsgBox(keyMessage, param);
			break;
		case "W":
			showWarningMsgBox(keyMessage, param);
			break;
		default:
			showErrorMsgBox(keyMessage, param);
			break;
		}
	}

	/**
	 * Show error message box with i18n feature
	 * 
	 * @param keyMessage
	 *            key code message
	 */
	protected void showErrorMsgBox(String keyMessage, String... param) {
		String message = Labels.getLabel(keyMessage, param);
		showMessageBox(message == null || message.isEmpty() ? keyMessage
				: message, Labels.getLabel("error"), Messagebox.ERROR);
	}

	/**
	 * Show info message box with i18n feature
	 * 
	 * @param keyMessage
	 *            key code message
	 */
	protected void showInfoMsgBox(String keyMessage, String... param) {
		String message = Labels.getLabel(keyMessage, param);
		showMessageBox(message == null || message.isEmpty() ? keyMessage
				: message, Labels.getLabel("info"), Messagebox.INFORMATION);
	}

	/**
	 * Show warning message box with i18n feature
	 * 
	 * @param keyMessage
	 *            key code message
	 */
	protected void showWarningMsgBox(String keyMessage, String... param) {
		String message = Labels.getLabel(keyMessage, param);
		showMessageBox(message == null || message.isEmpty() ? keyMessage
				: message, Labels.getLabel("warning"), Messagebox.EXCLAMATION);
	}

	private void showMessageBox(String message, String title, String type) {
		Messagebox.show(message, title,
				new Messagebox.Button[] { Messagebox.Button.OK },
				new String[] { Labels.getLabel("ok") }, type,
				Messagebox.Button.OK, null);
	}

	@Command("showDirty")
	public void showDirtyMsgBox() {
		Messagebox.show(Labels.getLabel("W002"), Labels.getLabel("warning"),
				new Messagebox.Button[] { Messagebox.Button.YES,
						Messagebox.Button.OK }, Messagebox.EXCLAMATION,
				Messagebox.Button.NO, new EventListener<ClickEvent>() {
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							back();
						}
					}
				});
	}

	public void back() {
	}

	protected Session getCurrentSession() {
		return Sessions.getCurrent();
	}

	protected Execution getCurrentExecution() {
		return Executions.getCurrent();
	}

	protected UserSessionJR getCurrentUserSessionJR() {
		return (UserSessionJR) getCurrentSession().getAttribute("user");
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public long getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}

	public int getActivePage() {
		return activePage;
	}

	public void setActivePage(int activePage) {
		this.activePage = activePage;
		loadList();
		BindUtils.postNotifyChange(null, null, this, "activePage");
	}

	public PageInfo getPageInfo() {
		return (PageInfo) Executions.getCurrent().getDesktop()
				.getAttribute(UIConstants.PAGE_INFO_ID);
	}

	@Command("toggleAdvSearch")
	public void toggleAdvSearch() {
		setShowAdvSearch(!isShowAdvSearch());
		BindUtils.postNotifyChange(null, null, this, "showAdvSearch");
		System.out.println("filter " + showAdvSearch);

	}

	public String getSearch1() {
		return search1;
	}

	public void setSearch1(String search1) {
		this.search1 = search1;
	}

	public String getSearch2() {
		return search2;
	}

	public void setSearch2(String search2) {
		this.search2 = search2;
	}

	public String getSearch3() {
		return search3;
	}

	public void setSearch3(String search3) {
		this.search3 = search3;
	}

	public String getSearch4() {
		return search4;
	}

	public void setSearch4(String search4) {
		this.search4 = search4;
	}

	public String getSearch5() {
		return search5;
	}

	public void setSearch5(String search5) {
		this.search5 = search5;
	}

	public Map<String, Boolean> getUserButton() {
		return getPageInfo().getUserButton();
	}

	public Map<String, Boolean> getMenuButton() {
		return getPageInfo().getMenuButton();
	}

	public boolean isShowAdvSearch() {
		return showAdvSearch;
	}

	public void setShowAdvSearch(boolean showAdvSearch) {
		this.showAdvSearch = showAdvSearch;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	/**
	 * Get user session locale
	 * 
	 * @return user session locale
	 */
	protected Locale getCurrentLocale() {
		Locale locale = new Locale("en"); // default locale

		Object obj = getCurrentSession().getAttribute(
				Attributes.PREFERRED_LOCALE);
		if (obj != null) {
			locale = (Locale) obj;
		}

		return locale;
	}

	/**
	 * Decode string to image
	 *
	 * @param imageString
	 *            The string to decode
	 * @return decoded image
	 */
	@SuppressWarnings("static-access")
	public static BufferedImage decodeToImage(String imageString)
			throws IOException {
		BufferedImage image = null;
		byte[] imageByte;
		Base64 decode = new Base64();
		imageByte = decode.decodeBase64(imageString);
		ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
		image = ImageIO.read(bis);
		bis.close();
		return image;
	}

	/**
	 * Encode image to Base64
	 *
	 * @param imageFile
	 *            The image to encode
	 * @param type
	 *            jpeg, bmp, gif, png
	 * @return encoded string
	 */
	public static String encodeToString(File imageFile, String type)
			throws IOException {
		String imageString = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		BufferedImage image = ImageIO.read(imageFile);
		ImageIO.write(image, type, bos);
		byte[] imageBytes = bos.toByteArray();

		// BASE64Encoder encoder = new BASE64Encoder();
		Base64 encode = new Base64();
		imageString = encode.encodeToString(imageBytes);
		// imageString = encoder.encode(imageBytes);

		bos.close();
		return imageString;
	}

	/**
	 * To make base64 string decoded properly, We need to remove the base64
	 * header from a base64 string.
	 *
	 * @param base64
	 *            The Base64 string of an image.
	 * @return Base64 string without header.
	 */
	public static String removeBase64Header(String base64) {
		if (base64 == null)
			return null;
		return base64.trim()
				.replaceFirst("data[:]image[/]([a-z])+;base64,", "");
	}

	/**
	 * Get the image type.
	 *
	 * @param base64
	 *            The base64 string of an image.
	 * @return jpg, png, gif
	 */
	public static String getImageType(String base64) {
		String[] header = base64.split("[;]");
		if (header == null || header.length == 0)
			return null;
		return header[0].split("[/]")[1];
	}

	// private void s(){
	// InputStream finput = new FileInputStream(file);
	// byte[] imageBytes = new byte[(int)file.length()];
	// finput.read(imageBytes, 0, imageBytes.length);
	// finput.close();
	// String imageStr = Base64.encodeBase64String(imageBytes);
	// }

	/**
	 * encode upload event to single line base64 encoded String
	 * 
	 * @param org
	 *            .zkoss.zk.ui.event.UploadEvent
	 * @return base64 String
	 */
	public String encodeToString(UploadEvent uE) {
		byte[] s = uE.getMedia().getByteData();
		String base64 = org.apache.commons.codec.binary.Base64
				.encodeBase64String(s);
		return base64;
	}

	/**
	 * encode Media to single line base64 encoded String
	 * 
	 * @param org
	 *            .zkoss.util.media.Media
	 * @return base64 String
	 */
	public String encodeToString(Media media) {
		byte[] s = media.getByteData();
		String base64 = org.apache.commons.codec.binary.Base64
				.encodeBase64String(s);
		return base64;
	}

	private String getLabel() {
		String label = "dd/MM/yyyy";
		try {
			label = Labels.getLabel("dateFormat");
		} catch (Exception o) {
			o.printStackTrace();
		}
		return label;
	}

	private String getLabel(String param) {
		String label = "dd/MM/yyyy";
		if (param.equalsIgnoreCase("time")) {
			label = "HH:mm";
			try {
				label = Labels.getLabel("timeFormat");
			} catch (Exception o) {
				o.printStackTrace();
			}
		} else if (param.equalsIgnoreCase("dateTime")) {
			label = "dd/MM/yyyy HH:mm";
			try {
				label = Labels.getLabel("dateTimeFormat");
			} catch (Exception o) {
				o.printStackTrace();
			}
		}

		return label;
	}

	/**
	 * Return date format base on application properties by default dd/MM/yyyy
	 * 
	 * @param date
	 * @return String
	 */
	public String dateToString(Date date) {
		try{
			SimpleDateFormat sdf = new SimpleDateFormat(getLabel());
			return sdf.format(date);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Return date based on formatted date using pattern in
	 * application.properties [dateFormat] by default dd/MM/yyyy
	 * 
	 * @param formattedDate
	 * @return
	 */
	public Date stringToDate(String formattedDate) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(getLabel());
			return sdf.getCalendar().getTime();
		} catch (NullPointerException n) {
			n.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * convert Date into formatted time String by default HH:mm
	 * 
	 * @param date
	 * @return
	 */
	public String dateToHour(Date date) {
		try{
			SimpleDateFormat sdf = new SimpleDateFormat(getLabel("time"));
			return sdf.format(date);
		}catch(NullPointerException e){
			e.printStackTrace();
			return null;
		}catch(Exception s){
			s.printStackTrace();
			return null;
		}
	}

	/**
	 * convert formatted String dateTime to Date format must use the one in
	 * application.properties [dateTimeFormat]
	 * 
	 * @param formattedDateTime
	 * @return
	 */
	public Date stringWithTimeToDate(String formattedDateTime) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(getLabel("dateTime"));
			return sdf.getCalendar().getTime();
		} catch (NullPointerException n) {
			n.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public AttachmentResultType[] hitserviesBpmEcmsUpload(String noBerkas, AttachmentsType[] attachments) throws RemoteException{
		Bpm_ecms_upload_wsdlProxy service = new Bpm_ecms_upload_wsdlProxy();
		Bpm_ecms_upload_wsdl_PortType port = service.getBpm_ecms_upload_wsdl_PortType();
		return port.bpm_ecms_upload(noBerkas, attachments);
	}
	
	public Bpm.Ecms.uploadByDirectory.AttachmentResultType[] uploadByDirectory(String noBerkas, String docPath, Bpm.Ecms.uploadByDirectory.AttachmentsType[] attachments) throws RemoteException{
		Bpm.Ecms.uploadByDirectory.Bpm_ecms_upload_wsdlProxy service = new Bpm.Ecms.uploadByDirectory.Bpm_ecms_upload_wsdlProxy();
		Bpm.Ecms.uploadByDirectory.Bpm_ecms_upload_wsdl_PortType port = service.getBpm_ecms_upload_wsdl_PortType();
		return port.bpm_ecms_upload(noBerkas, docPath, attachments);
	}
	
	public Response hitserviesBpmEcmsByNoBerkas(DirectoryName parameters) throws RemoteException{
		Bpm_ecms_query_bynoberkas_ps_wsdlProxy service = new Bpm_ecms_query_bynoberkas_ps_wsdlProxy();
		Bpm_ecms_query_bynoberkas_ps_wsdl_PortType port = service.getBpm_ecms_query_bynoberkas_ps_wsdl_PortType();
		return port.get_ecms_id(parameters);
	}
	
	
	@Command
	public void onProgress(){
		Messagebox.show("Sedang dalam perbaikan");
	}

	public void doAfterCompose(Component comp) throws Exception {
		// TODO Auto-generated method stub

	}
	
	@Command
	public void changePageSize(){
		setPageSize(getPageSize());
		onList();
		BindUtils.postNotifyChange(null, null, this, "pageSize");
	}
	
	protected RestResponseDukcapil callWsDukcapil(String url, Object sentObject,
			HttpMethod httpMethod) {
		return executeWebServiceDukcapil(WS_BPM_URL + url, sentObject, httpMethod);
	}
	
	private RestResponseDukcapil executeWebServiceDukcapil(String url, Object sentObject,
			HttpMethod httpMethod) {
		url = url.replace("\\", "");

		logger.info("JSON Dukcapil Object : {} ", JsonUtil.getJson(sentObject));
		logger.info("Invoke SOA service with URL : {}", url);

		RestTemplate restTemplate = new RestTemplate();
		final HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
//		headers.setContentType(MediaType.TEXT_PLAIN);
		final HttpEntity<Object> requestEntity = new HttpEntity<Object>(
				sentObject, headers);
		RestResponseDukcapil restResponse = null;

//		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();        
//		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
//		converter.setSupportedMediaTypes(Collections.singletonList(MediaType.TEXT_HTML));         
//		messageConverters.add(converter);  
//		restTemplate.setMessageConverters(messageConverters);
		
		ResponseEntity<RestResponseDukcapil> reponseEntity = null;
		try {
			//
			MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = 
                    new MappingJackson2HttpMessageConverter();
			mappingJackson2HttpMessageConverter.setSupportedMediaTypes(
                    Arrays.asList(
                       MediaType.TEXT_HTML,
                       MediaType.TEXT_PLAIN,
                       MediaType.APPLICATION_JSON, 
                       MediaType.APPLICATION_OCTET_STREAM));
			restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);
			//
			reponseEntity = restTemplate.exchange(url, httpMethod,
					requestEntity, RestResponseDukcapil.class);
			restResponse = reponseEntity.getBody();
			if (restResponse == null ) {
				throw new RuntimeException();
			}
		} catch (Exception e) {
			if (reponseEntity != null) {
				try {
					HttpStatus httpStatus = reponseEntity.getStatusCode();
				} catch (Exception e2) {
					e2.printStackTrace();
				}
			}
			restResponse = new RestResponseDukcapil();
			e.printStackTrace();

		}

		return restResponse;
	}
	
	protected DukcapilWinDto getDataFromDukcapil(String noKTP){
		DukcapilWinDto dto = new DukcapilWinDto();
		RestResponseDukcapil rest = new RestResponseDukcapil();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
//			Long nik = Long.parseLong(noKTP);
//			String nikRequest = noKTP;
			String alamat = "";
			DukcapilSentObject obj = new DukcapilSentObject();
			obj.setNik(noKTP);
			obj.setIpUser(Labels.getLabel("wsSOAIP"));
			obj.setPassword(Labels.getLabel("wsSOAPwd"));
			obj.setUserId(Labels.getLabel("wsSOAUser"));
			
			rest = callWsDukcapil(WS_BPM, obj, HttpMethod.POST);
			System.out.println("==================================");
			System.out.println(JsonUtil.getJson(rest));
			
			KKDukcapilDto kkDucapilDto = null;
			List<KKDukcapilDto> listKKDukcapilDto = JsonUtil.mapJsonToListObject(rest.getContent(), KKDukcapilDto.class);
			try{
				kkDucapilDto = listKKDukcapilDto.get(0);
			}catch(Exception s){
				Messagebox.show("Data Tidak Ditemukan");
			}
			if (kkDucapilDto.getRespon()!= null){
				listKKDukcapilDto = new ArrayList<>();
				Messagebox.show(kkDucapilDto.getRespon());
				return null;
			}
			if(kkDucapilDto.getaLAMAT()!= null || !kkDucapilDto.getaLAMAT().isEmpty()){
				alamat = kkDucapilDto.getaLAMAT()+"\n"+
						(kkDucapilDto.getnORT()==null?"":"RT. "+kkDucapilDto.getnORT()+" RW. "+kkDucapilDto.getnORW()+"\n")+
						kkDucapilDto.getkELNAME()+", "+kkDucapilDto.getkECNAME()+", "+kkDucapilDto.getkABNAME()+"\n"+
						kkDucapilDto.getpROPNAME();
			}else{
				listKKDukcapilDto = new ArrayList<>();
			}
			for(KKDukcapilDto a: listKKDukcapilDto){
				if(noKTP.equalsIgnoreCase(a.getnIK())){
					dto.setAgama(a.getaGAMA());
					dto.setAlamat(alamat);
					dto.setAlamatDusun(a.getnAMALGKP());
					dto.setAlamatKodepos(null);
					dto.setAlamatRt(a.getnORT());
					dto.setAlamatRw(a.getnORW());
					dto.setGolDarah(a.getgOLDARAH());
					dto.setJnsKelamin(a.getjENISKLMIN());
					dto.setKodeKabupaten(a.getnOKAB());
					dto.setKodeKecamatan(a.getnOKEC());
					dto.setKodeKelurahan(a.getnOKEL());
					dto.setKodePropinsi(a.getnOPROP());
					dto.setKodePekerjaan(null);
					dto.setNamaAyah(a.getnAMALGKPAYAH());
					dto.setNamaIbu(a.getnAMALGKPIBU());
					dto.setNamaLengkap(a.getnAMALGKP());
					dto.setNik(a.getnIK());
					dto.setNoKk(a.getnOKK());
					dto.setPendAkhir(a.getpDDKAKH());
					dto.setStatusNikah(a.getsTATUSKAWIN());
					dto.setTglLahir(sdf.parse(a.gettGLLHR()));
					dto.setTmpLahir(a.gettMPTLHR());
					dto.setNamaKabupaten(a.getkABNAME());
					dto.setNamaKecamatan(a.getkECNAME());
					dto.setNamaKelurahan(a.getkELNAME());
					dto.setNamaProvinsi(a.getpROPNAME());
					
					return dto;
				}
			}
			
			return dto;
			
		}catch(Exception s){
			s.printStackTrace();
			return null;
		}
	}
	
	public boolean isAdmin(){
		String s = "ADMIN PL";
		UserSessionJR p = getCurrentUserSessionJR();
		if(s.toUpperCase().contains(p.getLoginDesc().substring(0, 8).toUpperCase())){
			return true;
		}else{
			return false;
		}
	}
	
}