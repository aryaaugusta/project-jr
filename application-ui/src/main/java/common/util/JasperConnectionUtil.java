package common.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.zkoss.util.resource.Labels;

public class JasperConnectionUtil {
	public static Connection getOracleConnection() throws ClassNotFoundException, SQLException{
//		String host="192.168.1.30";
//		String host="localhost";
//		String sid="orcl";
//		String userName = "jr_user";
//		String password = "jr_user";
		String host=Labels.getLabel("hostReport");
		String sid=Labels.getLabel("sidReport");
		String userName = Labels.getLabel("usernameReport");
		String port = Labels.getLabel("portReport");
		String password = Labels.getLabel("passwordReport");
		System.out.println(host + " " + sid + " " + userName + " " );
		return getOracleConnection(host, port, sid, userName, password);
	}
	
	public static Connection getOracleConnection(String host, String sid, String user, String password) throws ClassNotFoundException, SQLException{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		
		String connectionUrl = "jdbc:oracle:thin:@"+host+":"+"1521"+":"+sid;
//		String connectionUrl = "jdbc:oracle:thin:@"+host+":"+Labels.getLabel("port")+":"+sid;
		Connection conn = DriverManager.getConnection(connectionUrl, user, password);
		return conn;
	}
	
	public static Connection getOracleConnection(String host, String port, String sid, String user, String password) throws ClassNotFoundException, SQLException{
		Class.forName("oracle.jdbc.driver.OracleDriver");
		//java.sql.DriverManager.getConnection("jdbc:oracle:thin:@198.168.1.30:1521:jr", "jr_user", "jr_user")

		String connectionUrl = "jdbc:oracle:thin:@"+host+":"+port+":"+sid;
//		String connectionUrl = "jdbc:oracle:thin:@"+host+":"+Labels.getLabel("port")+":"+sid;
		Connection conn = DriverManager.getConnection(connectionUrl, user, password);
		return conn;
	}
}
