package common.util;

public class NumberUtil {

	public String terbilang(int satuan)
	  {
	    String[] huruf = { "", "SATU", "DUA", "TIGA", "EMPAT", "LIMA", "ENAM", "TUJUH", "DELAPAN", "SEMBILAN", "SEPULUH", "SEBELAS" };
	    
	    String hasil = "";
	    if (satuan < 12) {
	      hasil = hasil + huruf[satuan];
	    } else if (satuan < 20) {
	      hasil = hasil + terbilang(satuan - 10) + " BELAS";
	    } else if (satuan < 100) {
	      hasil = hasil + terbilang(satuan / 10) + " PULUH " + terbilang(satuan % 10);
	    } else if (satuan < 200) {
	      hasil = hasil + "SERATUS " + terbilang(satuan - 100);
	    } else if (satuan < 1000) {
	      hasil = hasil + terbilang(satuan / 100) + " RATUS " + terbilang(satuan % 100);
	    } else if (satuan < 2000) {
	      hasil = hasil + "SERIBU " + terbilang(satuan - 1000);
	    } else if (satuan < 1000000) {
	      hasil = hasil + terbilang(satuan / 1000) + " RIBU " + terbilang(satuan % 1000);
	    } else if (satuan < 1000000000) {
	      hasil = hasil + terbilang(satuan / 1000000) + " JUTA " + terbilang(satuan % 1000000);
	    } else if (satuan >= 1000000000) {
	      hasil = "Angka terlalu besar, harus kurang dari 1 milyar!";
	    }
	    return hasil;
	  }
}
