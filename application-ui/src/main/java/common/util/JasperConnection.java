package common.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.zkoss.util.resource.Labels;
import org.zkoss.zul.Label;

public class JasperConnection {

	public static Connection getConnection() throws SQLException, ClassNotFoundException{
		return JasperConnectionUtil.getOracleConnection();
	}
	
	public static void main(String[] args) throws SQLException, ClassNotFoundException{
		System.out.println("getting connection");
		
		Connection conn = JasperConnection.getConnection();
		
		System.out.println("Connection : "+conn);
		
		conn.close();
		System.out.println("connected");
		
		System.out.println(new NumberUtil().terbilang(213912192));
	}
}
