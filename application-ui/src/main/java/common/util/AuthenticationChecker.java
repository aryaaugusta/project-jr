package common.util;

import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.zkoss.util.Locales;
import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Initiator;

import share.common.LoginData;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;

public class AuthenticationChecker extends BaseVmd implements Initiator{
	private static Logger logger = LoggerFactory.getLogger(AuthenticationChecker.class);
	@Override
	public void doInit(Page page, Map<String, Object> args) throws Exception {
		if (System.getProperty("debug") != null && getCurrentUserSessionJR()==null) {
			if(System.getProperty("login")!=null){
				String login = System.getProperty("login");
				
				LoginData loginData = new LoginData();
				
				loginData.setLogin(login);
//				System.out.println("Loign data : ");
				System.out.println(JsonUtil.getJson(loginData));
//			syso
				RestResponse restUser = callCustomWs("/auth/getUserInfo", loginData, HttpMethod.POST);
				
				try {
					
					UserSessionJR userSessionJR = JsonUtil.mapJsonToSingleObject(restUser.getContents(), UserSessionJR.class);
//				List<UserMenuDto> listMenu = JsonUtil.mapJsonToListObject(rest.getContents(), UserMenuDto.class);
					
					Locale locale = Locales.getLocale(userSessionJR.getUserLocale());
					getCurrentSession().setAttribute(Attributes.PREFERRED_LOCALE, locale);
					
					getCurrentSession().setAttribute("user", userSessionJR);
					System.out.println("Set USER SESSION");
					
					Executions.sendRedirect("/index.zul");
				} catch (Exception e) {
					e.printStackTrace();
					showSmartMsgBox("Login Failed");
				}
			}else{
				Executions.sendRedirect("/login.zul");
			}
		}
		if (getCurrentUserSessionJR() == null) {
			Executions.sendRedirect("/login.zul");
		}
		
	}

}
