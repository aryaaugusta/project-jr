package common.util;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.Initiator;

import common.ui.UIConstants;

public class loginChecker implements Initiator {
	
	@Override
	public void doInit(Page page, Map<String, Object> args) throws IOException{
		Session sess = Sessions.getCurrent();

		// if user is already login, then redirect to proper home page
		if (sess.hasAttribute(UIConstants.SESS_LOGIN_ID)) {
			Execution exec = Executions.getCurrent();
			HttpServletResponse response = (HttpServletResponse) exec
					.getNativeResponse();
			response.sendRedirect(response.encodeRedirectURL(exec
					.getContextPath() + "/index.zul"));

			exec.setVoided(true);
		}
	}
}
