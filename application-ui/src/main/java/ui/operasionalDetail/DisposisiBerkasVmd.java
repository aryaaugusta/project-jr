package ui.operasionalDetail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import share.DasiJrRefCodeDto;
import share.PlAngkutanKecelakaanDto;
import share.PlDataKecelakaanDto;
import share.PlDisposisiDto;
import share.PlKorbanKecelakaanDto;
import share.PlPengajuanSantunanDto;
import ui.operasional.dokumen.CetakLdpb;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

public class DisposisiBerkasVmd extends BaseVmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private PlPengajuanSantunanDto plPengajuanSantunanDto = new PlPengajuanSantunanDto();
	private List<PlPengajuanSantunanDto> listPengajuanSantunanDto = new ArrayList<>();

	private DasiJrRefCodeDto disposisiRefCode = new DasiJrRefCodeDto();
	private List<DasiJrRefCodeDto> listDisposisiRefCode = new ArrayList<>();

	private PlDisposisiDto plDisposisiDto = new PlDisposisiDto();
	private List<PlDisposisiDto> listDisposisiDto = new ArrayList<>();

	private final String WS_URI = "/OpOtorisasi";
	private final String WS_URI_SANTUNAN = "/OpPengajuanSantunan";
	private final String WS_URI_LOV = "/Lov";

	@Wire("#popupDisposisi")
	private Window winLov;

	private String popUpHandler;
	private UserSessionJR ujr = new UserSessionJR();
	private String id;
	private Date hari;
	private Date bulan;
	private Date tahun;
	private Date tanggal;
	private String caller;
	private DasiJrRefCodeDto disp;

	private boolean showListDisposisi = true;
	private boolean showFormDisposisi = false;

	private boolean editVisible;
	private boolean addVisible;

	private boolean checkDisposisi;

	private boolean flagTambahDisposisi;
	private AMedia fileContent;

	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent()
			.getAttribute(UIConstants.SESS_LOGIN_ID);

	/**
	 * args.put("plPengajuanSantunanDto", pengajuan2); args.put("caller",
	 * "absahBerkas"); if(listDisposisi.size()>0 && listDisposisi.size()>1){
	 * for(DasiJrRefCodeDto j : listDisposisi){
	 * if(j.getRvAbbreviation().equalsIgnoreCase("BV")){ args.put("disposisi",
	 * j);
	 * 
	 * @param popUpHandler
	 * @param pengajuanSantunanDto
	 * @param view
	 */
	@NotifyChange({ "plPengajuanSantunanDto", "id" })
	@Init
	public void baseInit(
			@ExecutionArgParam("popUpHandler") String popUpHandler,
			@ExecutionArgParam("caller") String caller,
			@ExecutionArgParam("disposisi") DasiJrRefCodeDto disposisi,
			@ExecutionArgParam("plPengajuanSantunanDto") PlPengajuanSantunanDto pengajuanSantunanDto,
			@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		this.popUpHandler = popUpHandler;
		plPengajuanSantunanDto = pengajuanSantunanDto;

		if (caller != null) {
			this.caller = caller;
		}

		if (disposisi != null) {
			this.disp = disposisi;
		}

		listDisposisi();
		System.out.println("Base popup init called");
	}

	public void onView() {
	}

	@Command
	public void close(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}

	private void close() {
		if (winLov == null)
			throw new RuntimeException("id popUp tidak sama dengan viewModel");
		winLov.detach();
	}

	@Command
	public void back() {
		close();
	}

	@GlobalCommand("viewDisposisi")
	public void view(@BindingParam("noBerkas") String noBerkas) {
		plPengajuanSantunanDto = new PlPengajuanSantunanDto();
		plPengajuanSantunanDto.setNoBerkas(noBerkas);

		Map<String, Object> map = new HashMap<>();
		map.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse restSantunan = callWs(WS_URI + "/findByNoBerkas", map,
				HttpMethod.POST);
		try {
			listPengajuanSantunanDto = JsonUtil.mapJsonToListObject(
					restSantunan.getContents(), PlPengajuanSantunanDto.class);
			for (PlPengajuanSantunanDto a : listPengajuanSantunanDto) {
				plPengajuanSantunanDto.setIdKecelakaan(a.getIdKecelakaan());
				plPengajuanSantunanDto.setIdKorbanKecelakaan(a.getIdKorbanKecelakaan());
				plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
				plPengajuanSantunanDto.setNoPengajuan(a.getNoPengajuan());
				plPengajuanSantunanDto.setNamaPemohon(a.getNamaPemohon());
				plPengajuanSantunanDto.setAlamatPemohon(a.getAlamatPemohon());
				plPengajuanSantunanDto.setTglPengajuan(a.getTglPengajuan());
				plPengajuanSantunanDto.setTglPenyelesaian(a.getTglPenyelesaian());
				plPengajuanSantunanDto.setOtorisasiFlag(a.getOtorisasiFlag());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "flagRingkasanSantunan");
		BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
		Executions.getCurrent().setAttribute("obj", plPengajuanSantunanDto);
		getPageInfo().setViewMode(true);
		onView();
	}

	@Command
	public void listDisposisi() {
		Map<String, Object> mapInput = new HashMap<>();
		if (plPengajuanSantunanDto.isMenuPengajuan()==true) {
			mapInput.put("statusProses", "TL");
		} else if (plPengajuanSantunanDto.isMenuIdentifikasi()==true) {
			mapInput.put("statusProses", "BA");
		} else if (plPengajuanSantunanDto.isMenuOtorisasi()==true) {
			mapInput.put("statusProses", "O");
		}
		mapInput.put("levelKantor", userSession.getLevelKantor());
		mapInput.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse restDisposisi = callWs(WS_URI_LOV + "/getListDisposisi",
				mapInput, HttpMethod.POST);
		listKodeDisposisi();
		try {
			listDisposisiDto = JsonUtil.mapJsonToListObject(
					restDisposisi.getContents(), PlDisposisiDto.class);
				for (PlDisposisiDto a : listDisposisiDto) {
					plDisposisiDto.setDari(a.getDari());
					plDisposisiDto.setTglDisposisi(a.getTglDisposisi());
					plDisposisiDto.setDisposisi(a.getDisposisi());
					plDisposisiDto.setDariDesc(a.getDariDesc());
					plDisposisiDto.setIdDisposisi(a.getIdDisposisi());
					plDisposisiDto.setEditVisible(a.isEditVisible());
				}
			BindUtils.postNotifyChange(null, null, this, "plDisposisiDto");
			BindUtils.postNotifyChange(null, null, this, "listDisposisiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	@NotifyChange("plDisposisiDto")
	public void editDisposisi(@BindingParam("item")PlDisposisiDto selectedDto) {
		if (plDisposisiDto.getIdDisposisi() == null) {
			showInfoMsgBox("Id Disposisi null !", "");
		} else {
			listKodeDisposisi();
			setShowFormDisposisi(true);
			BindUtils.postNotifyChange(null, null, this, "showFormDisposisi");
			setShowListDisposisi(false);
			BindUtils.postNotifyChange(null, null, this, "showListDisposisi");
			Map<String, Object> map = new HashMap<>();
			map.put("idDisposisi", selectedDto.getIdDisposisi());
			System.err.println("luthfi990 "+selectedDto.getIdDisposisi());
			RestResponse rest = callWs(WS_URI_LOV + "/getDisposisiById", map,
					HttpMethod.POST);
			try {
				listDisposisiDto = JsonUtil.mapJsonToListObject(
						rest.getContents(), PlDisposisiDto.class);
				System.err.println("luthfi990 "+listDisposisiDto.size());
				for (PlDisposisiDto a : listDisposisiDto) {
					plDisposisiDto = new PlDisposisiDto();
					plDisposisiDto.setCreatedBy(a.getCreatedBy());
					plDisposisiDto.setCreationDate(a.getCreationDate());
					plDisposisiDto.setDari(a.getDari());
					plDisposisiDto.setDisposisi(a.getDisposisi());
					plDisposisiDto.setIdDisposisi(a.getIdDisposisi());
					plDisposisiDto.setIdGuid(a.getIdGuid());
					plDisposisiDto.setJumlahDisetujui(a.getJumlahDisetujui());
					plDisposisiDto.setLevelCabangDisp(a.getLevelCabangDisp());
					plDisposisiDto.setNoBerkas(a.getNoBerkas());
					plDisposisiDto.setTglDisposisi(a.getTglDisposisi());
				}
				System.err.println("luthfi "+JsonUtil.getJson(plDisposisiDto));
//				getDataDisposisi();
				Map<String, Object> map2 = new HashMap<>();
				if (plPengajuanSantunanDto.isMenuPengajuan()==true) {
					map2.put("statusProses", "TL");
				} else if (plPengajuanSantunanDto.isMenuIdentifikasi()==true) {
					map2.put("statusProses", "BA");
				} else if (plPengajuanSantunanDto.isMenuOtorisasi()==true) {
					map2.put("statusProses", "O");
				}
				map2.put("levelKantor", userSession.getLevelKantor());
				RestResponse rest2 = callWs(WS_URI_LOV + "/getKodeDisposisi", map2,
						HttpMethod.POST);
				try {
					listDisposisiRefCode = JsonUtil.mapJsonToListObject(
							rest2.getContents(), DasiJrRefCodeDto.class);
					for (DasiJrRefCodeDto a : listDisposisiRefCode) {
						disposisiRefCode.setRvLowValue(a.getRvLowValue());
						disposisiRefCode.setRvHighValue(a.getRvHighValue());
						disposisiRefCode.setRvAbbreviation(a.getRvAbbreviation());
						disposisiRefCode.setRvMeaning(a.getRvMeaning());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				for (DasiJrRefCodeDto kodeDisposisi : listDisposisiRefCode) {
					if (disposisiRefCode.getRvLowValue().equalsIgnoreCase(
							kodeDisposisi.getRvLowValue())
							&& disposisiRefCode.getRvAbbreviation().equalsIgnoreCase(
									kodeDisposisi.getRvAbbreviation())
							&& disposisiRefCode.getRvHighValue().equalsIgnoreCase(
									kodeDisposisi.getRvHighValue())) {
						disposisiRefCode = new DasiJrRefCodeDto();
						disposisiRefCode = kodeDisposisi;
					}
				}
				BindUtils.postNotifyChange(null, null, this, "disposisiRefCode");

				System.err.println("luthfi2 "+JsonUtil.getJson(plDisposisiDto));
				BindUtils.postNotifyChange(null, null, this, "plDisposisiDto");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Command
	public void deleteDisposisi(@BindingParam("item") final PlDisposisiDto plDisposisiDto) {
		if (plDisposisiDto.getIdDisposisi() == null) {
			showInfoMsgBox("Id Disposisi null !", "");
		} else {
			Messagebox.show(Labels.getLabel("C001"),
					Labels.getLabel("confirmation"), new Button[] { Button.YES,
							Button.NO }, Messagebox.QUESTION, Button.NO,
					new EventListener<ClickEvent>() {
						@Override
						public void onEvent(ClickEvent evt) throws Exception {
							if (Messagebox.ON_YES.equals(evt.getName())) {
								RestResponse restRespone;
								restRespone = callWs(WS_URI_LOV
										+ "/deleteDisposisi", plDisposisiDto,
										HttpMethod.POST);
								if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
									showInfoMsgBox("Data successfully deleted");
									setAddVisible(true);
									listDisposisi();
								} else {
									showErrorMsgBox(restRespone.getMessage());
								}
							}
						}
					});
		}
	}

	@Command("tambahDisposisi")
	public void tambahDisposisi() {
		setShowFormDisposisi(true);
		setFlagTambahDisposisi(true);
		BindUtils.postNotifyChange(null, null, this, "showFormDisposisi");
		setShowListDisposisi(false);
		BindUtils.postNotifyChange(null, null, this, "showListDisposisi");
		setAddVisible(false);
		listKodeDisposisi();
		plDisposisiDto = new PlDisposisiDto();
		// =========================================GET DATA DISPOSISI=============================
		plDisposisiDto.setTglDisposisi(new Date());
		getDataDisposisi();
		BindUtils.postNotifyChange(null, null, this, "flagTambahDisposisi");
		BindUtils.postNotifyChange(null, null, this, "addVisible");
		BindUtils.postNotifyChange(null, null, this, "plDisposisiDto");
	}

	public void getDataDisposisi() {
		Map<String, Object> map = new HashMap<>();
		if (plPengajuanSantunanDto.getStatusProses().equalsIgnoreCase("DT")) {
			map.put("statusProses", "TL");
		} else if (plPengajuanSantunanDto.getStatusProses().equalsIgnoreCase(
				"BL")) {
			map.put("statusProses", "BA");
		} else if (plPengajuanSantunanDto.getStatusProses().equalsIgnoreCase(
				"BA")) {
			map.put("statusProses", "O");
		}
		map.put("levelKantor", userSession.getLevelKantor());
		RestResponse rest = callWs(WS_URI_LOV + "/getKodeDisposisi", map,
				HttpMethod.POST);
		try {
			listDisposisiRefCode = JsonUtil.mapJsonToListObject(
					rest.getContents(), DasiJrRefCodeDto.class);
			for (DasiJrRefCodeDto a : listDisposisiRefCode) {
				disposisiRefCode.setRvLowValue(a.getRvLowValue());
				disposisiRefCode.setRvHighValue(a.getRvHighValue());
				disposisiRefCode.setRvAbbreviation(a.getRvAbbreviation());
				disposisiRefCode.setRvMeaning(a.getRvMeaning());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		for (DasiJrRefCodeDto kodeDisposisi : listDisposisiRefCode) {
			if (disposisiRefCode.getRvLowValue().equalsIgnoreCase(
					kodeDisposisi.getRvLowValue())
					&& disposisiRefCode.getRvAbbreviation().equalsIgnoreCase(
							kodeDisposisi.getRvAbbreviation())
					&& disposisiRefCode.getRvHighValue().equalsIgnoreCase(
							kodeDisposisi.getRvHighValue())) {
				disposisiRefCode = new DasiJrRefCodeDto();
				disposisiRefCode = kodeDisposisi;
			}
		}
		BindUtils.postNotifyChange(null, null, this, "disposisiRefCode");
	}

	@Command("cetakDisposisi")
	public void cetakDisposisi() throws IOException {
		CetakLdpb cetak = new CetakLdpb();
		cetak.cetakDisposisi(plPengajuanSantunanDto);
	}

	// =====================================SHOW LIST DISPOSISI=============================
	@Command("showDisposisi")
	public void showListDisposisi() {
		setShowFormDisposisi(false);
		BindUtils.postNotifyChange(null, null, this, "showFormDisposisi");
		setShowListDisposisi(true);
		BindUtils.postNotifyChange(null, null, this, "showListDisposisi");
		listDisposisi();
	}

	@Command("test")
	public void test() {
		String str = plPengajuanSantunanDto.getNoBerkas();
		String a = str.replace("-", "");

		Date today = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
		String c = formatter.format(today);

		System.out.println("test " + disposisiRefCode.getRvLowValue());
		System.out.println("test " + plDisposisiDto.getTglDisposisi());
		System.out.println("test " + a + "." + disposisiRefCode.getRvLowValue()
				+ "." + c);
		System.out.println("test " + plDisposisiDto.getDisposisi());
		System.out.println("test " + disposisiRefCode.getRvHighValue());
		System.out.println("test " + plPengajuanSantunanDto.getNoBerkas());
		System.out.println("test " + userSession.getLoginID());
	}

	// ======================================SAVE
	// DISPOSISI================================
	@Command("saveDisposisi")
	public void saveDisposisi() {
		RestResponse restResponse = null;
		System.err.println("luthfi5 "+JsonUtil.getJson(plDisposisiDto));
		if (flagTambahDisposisi == true) {
			// for id disposisi
			String str = plPengajuanSantunanDto.getNoBerkas();
			String a = str.replace("-", "");

			Date today = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyHHmmss");
			String c = formatter.format(today);

			plDisposisiDto.setDari(disposisiRefCode.getRvLowValue());
			plDisposisiDto.setTglDisposisi(plDisposisiDto.getTglDisposisi());
			plDisposisiDto.setDisposisi(plDisposisiDto.getDisposisi());
			plDisposisiDto.setIdDisposisi(a + "."
					+ disposisiRefCode.getRvLowValue() + "." + c);
			plDisposisiDto
					.setLevelCabangDisp(disposisiRefCode.getRvHighValue());
			plDisposisiDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
			plDisposisiDto.setCreatedBy(userSession.getLoginID());
			plDisposisiDto.setCreationDate(new Date());
			System.err.println("luthfi4 "+JsonUtil.getJson(plDisposisiDto));
			restResponse = callWs(WS_URI_SANTUNAN + "/saveDisposisi",
					plDisposisiDto, HttpMethod.POST);

		} else {
			Map<String, Object> map = new HashMap<>();
			map.put("idDisposisi", plDisposisiDto.getIdDisposisi());
			RestResponse rest = callWs(WS_URI_LOV + "/getDisposisiById", map,
					HttpMethod.POST);
			try {
				listDisposisiDto = JsonUtil.mapJsonToListObject(
						rest.getContents(), PlDisposisiDto.class);
				for (PlDisposisiDto a : listDisposisiDto) {
					plDisposisiDto.setCreatedBy(a.getCreatedBy());
					plDisposisiDto.setCreationDate(a.getCreationDate());
					plDisposisiDto.setDari(a.getDari());
					plDisposisiDto.setIdDisposisi(a.getIdDisposisi());
					plDisposisiDto.setIdGuid(a.getIdGuid());
					plDisposisiDto.setJumlahDisetujui(a.getJumlahDisetujui());
					plDisposisiDto.setLevelCabangDisp(a.getLevelCabangDisp());
					plDisposisiDto.setNoBerkas(a.getNoBerkas());
					plDisposisiDto.setTglDisposisi(a.getTglDisposisi());
				}
				plDisposisiDto.setDisposisi(plDisposisiDto.getDisposisi());
				plDisposisiDto.setTglDisposisi(plDisposisiDto.getTglDisposisi());
				plDisposisiDto.setLastUpdatedBy(userSession.getLoginID());
				plDisposisiDto.setLastUpdatedDate(new Date());
				System.err.println("luthfi6 "+JsonUtil.getJson(plDisposisiDto));
				restResponse = callWs(WS_URI_SANTUNAN + "/saveDisposisi",
						plDisposisiDto, HttpMethod.POST);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
			showListDisposisi();

		} else {
			showErrorMsgBox(restResponse.getMessage());
		}

	}

	public void listKodeDisposisi() {
		Map<String, Object> map = new HashMap<>();
		RestResponse rest = callWs(WS_URI_LOV + "/getListKodeDisposisi", map,
				HttpMethod.POST);
		try {
			listDisposisiRefCode = JsonUtil.mapJsonToListObject(
					rest.getContents(), DasiJrRefCodeDto.class);
			for (DasiJrRefCodeDto a : listDisposisiRefCode) {
				disposisiRefCode.setRvAbbreviation(a.getRvAbbreviation());
				disposisiRefCode.setRvLowValue(a.getRvLowValue());
				disposisiRefCode.setRvHighValue(a.getRvHighValue());
				disposisiRefCode.setRvMeaning(a.getRvMeaning());
			}
			BindUtils.postNotifyChange(null, null, this, "disposisiRefCode");
			BindUtils
					.postNotifyChange(null, null, this, "listDisposisiRefCode");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean checkDisposisi(PlPengajuanSantunanDto plPengajuanSantunanDto,
			UserSessionJR userSession) {
		boolean cek = false;
//		=====================================CHECK LIST DISPOSISI==============================
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse restDisposisi = callWs(WS_URI_LOV + "/getListDisposisi",
				mapInput, HttpMethod.POST);
		listKodeDisposisi();
		setAddVisible(true);
		try {
			listDisposisiDto = JsonUtil.mapJsonToListObject(restDisposisi.getContents(), PlDisposisiDto.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "addVisible");

//		===================================CHECK DISPOSISI========================================
		Map<String, Object> map = new HashMap<>();
		if (plPengajuanSantunanDto.getStatusProses().equalsIgnoreCase("DT")) {
			map.put("statusProses", "TL");
		} else if (plPengajuanSantunanDto.getStatusProses().equalsIgnoreCase(
				"BL")) {
			map.put("statusProses", "BA");
		} else if (plPengajuanSantunanDto.getStatusProses().equalsIgnoreCase(
				"BA")) {
			map.put("statusProses", "O");
		}
		map.put("levelKantor", userSession.getLevelKantor());
		RestResponse rest = callWs(WS_URI_LOV + "/getKodeDisposisi", map,
				HttpMethod.POST);
		try {
			listDisposisiRefCode = JsonUtil.mapJsonToListObject(
					rest.getContents(), DasiJrRefCodeDto.class);
			for (DasiJrRefCodeDto a : listDisposisiRefCode) {
				disposisiRefCode.setRvLowValue(a.getRvLowValue());
				disposisiRefCode.setRvHighValue(a.getRvHighValue());
				disposisiRefCode.setRvAbbreviation(a.getRvAbbreviation());
				disposisiRefCode.setRvMeaning(a.getRvMeaning());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	
		for(PlDisposisiDto check : listDisposisiDto){
			if(disposisiRefCode.getRvLowValue().equalsIgnoreCase(check.getDariDesc())){
				cek = true;
			}
		}
		return cek;
	}
	
	@SuppressWarnings("deprecation")
	@Command("cetak")
	public void cete(@BindingParam("popup")String popup,@BindingParam("item") PlPengajuanSantunanDto selectedDto) throws ClassNotFoundException, SQLException, IOException, JRException, Exception{
		try{
			Map<String, Object> parameters = new HashMap<String, Object>();
			String kode = null;
			PlPengajuanSantunanDto disposisi = new PlPengajuanSantunanDto();
			List<PlPengajuanSantunanDto> disposisiPengajuan = new ArrayList<>();
			List<PlDisposisiDto> disposisiDtos = new ArrayList<>();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");

//			=================================GET DISPOSISI======================================
			Map<String, Object> mapPengajuan = new HashMap<>();
			mapPengajuan.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
			RestResponse restDisposisi = callWs(WS_URI_LOV+ "/getValueLdbp", mapPengajuan,
					HttpMethod.POST);
			try{
				disposisiPengajuan = JsonUtil.mapJsonToListObject(restDisposisi.getContents(), PlPengajuanSantunanDto.class);
				for(PlPengajuanSantunanDto a : disposisiPengajuan){
					disposisi.setNoBerkas(a.getNoBerkas());
					disposisi.setTglPenerimaan(a.getTglPenerimaan());
					disposisi.setTglPenyelesaian(a.getTglPenyelesaian());
					disposisi.setNamaKorban(a.getNamaKorban());
					disposisi.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
					disposisi.setInstansi(a.getInstansi());
					disposisi.setKesimpulanSementara(a.getKesimpulanSementara());;
					disposisi.setKodeJaminan(a.getKodeJaminan());
					disposisi.setKodeJaminanLaporan(a.getKodeJaminanLaporan());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
//			=================================GET DATA KENDARAAN======================================
			Map<String, Object> mapDisposisi = new HashMap<>();
			mapDisposisi.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
			mapDisposisi.put("levelKantor", userSession.getLevelKantor());
			RestResponse restListDisposisi = callWs(WS_URI_LOV+ "/getValueLdbpList", mapDisposisi,
					HttpMethod.POST);
			try{
				disposisiDtos = JsonUtil.mapJsonToListObject(restListDisposisi.getContents(), PlDisposisiDto.class);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(disposisi.getKodeJaminan().equalsIgnoreCase("111") 
					|| disposisi.getKodeJaminan().equalsIgnoreCase("112")
					|| disposisi.getKodeJaminan().equalsIgnoreCase("121")
					|| disposisi.getKodeJaminan().equalsIgnoreCase("131")
					|| disposisi.getKodeJaminan().equalsIgnoreCase("132")
					|| disposisi.getKodeJaminan().equalsIgnoreCase("141")
					){
				kode = "UU No. 33/1964 "+disposisi.getKodeJaminanLaporan();
			}else if(disposisi.getKodeJaminan().equalsIgnoreCase("211")
					|| disposisi.getKodeJaminan().equalsIgnoreCase("212")
					|| disposisi.getKodeJaminan().equalsIgnoreCase("213")
					){
				kode = "UU No. 34/1964 "+disposisi.getKodeJaminanLaporan();
			}

			JRBeanCollectionDataSource listDisposisi  = new JRBeanCollectionDataSource(disposisiDtos);

			parameters.put("PARAM_KESIMPULAN", disposisi.getKesimpulanSementara());
			parameters.put("PARAM_KODE", kode);
			parameters.put("PARAM_TGL_PENYELESAIAN", disposisi.getTglPenyelesaian());
			parameters.put("PARAM_ISI_RINGKAS", disposisi.getDeskripsiKecelakaan());
			parameters.put("PARAM_ASAL", disposisi.getInstansi());
			parameters.put("PARAM_TGL_TERIMA", disposisi.getTglPenerimaan());
			parameters.put("PARAM_NO_BERKAS", disposisi.getNoBerkas());
			parameters.put("PARAM_NAMA_KORBAN", disposisi.getNamaKorban());
			parameters.put("LIST_DISPOSISI", listDisposisi);
			
			File temp = File.createTempFile("BERKAS LDPB", ".pdf");
			FileOutputStream outs = new FileOutputStream(temp);
			File tempJsp = File.createTempFile("Jasper", ".jrxml");
			JasperReport jasper = null;
			try{
				ByteArrayOutputStream baos = getByteArrayOutputStream("ReportLDPB.jrxml");
				try (OutputStream outputStream = new FileOutputStream(tempJsp)) {
					baos.writeTo(outputStream);
					outputStream.close();
				} catch (Exception x) {
					x.printStackTrace();
				}
				jasper = JasperCompileManager.compileReport(tempJsp
						.getAbsolutePath());
			}catch(Exception s){
				jasper = JasperCompileManager.compileReport("/home/glassfish/report/ReportLDPB.jrxml");
			}
			JasperPrint jasperReport = JasperFillManager.fillReport(jasper, parameters, new JREmptyDataSource(1));
			jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql"
	                ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
		
			// PDF-Export
			JRPdfExporter exportPdf = new JRPdfExporter();
			System.out.println(temp.getAbsolutePath());
			exportPdf.setParameter(JRExporterParameter.JASPER_PRINT, jasperReport);
			exportPdf.setParameter(JRExporterParameter.OUTPUT_STREAM, outs);
			exportPdf.exportReport();
		
			// show exported pdf 
			File f = new File(temp.getPath());
			byte[] buffer = new byte[(int) f.length()];
			FileInputStream fs = new FileInputStream(f);
			fs.read(buffer);
			fs.close();
			ByteArrayInputStream is = new ByteArrayInputStream(buffer);
			this.fileContent = new AMedia("report", "pdf", "application/pdf", is);


		}catch(Exception p){
			p.printStackTrace();
		}
		Map<String, Object> args = new HashMap<>();
		args.put("media", fileContent);
		((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
//		System.out.println(JsonUtil.getJson(map));
		
	}
	
	private ByteArrayOutputStream getByteArrayOutputStream(String... param)
			throws IOException, FileNotFoundException {
		URL url;
		String reportName = "";
		if(param.length>0){
			url = new URL(getLoc() + "/" + param[0]);
		}else{
			url = new URL(getLoc() + "/" + param[0]);
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			is = url.openStream();
			byte[] byteChunk = new byte[4096];
			int n;
			while ((n = is.read(byteChunk)) > 0) {
				baos.write(byteChunk, 0, n);
			}
		} catch (IOException e) {
			System.err.printf("Failed while reading bytes from %s: %s",
					url.toExternalForm(), e.getMessage());
			e.printStackTrace();
		} finally {
			if (is != null) {
				is.close();
			}
		}
	
		return baos;
	}
	
	private String getLoc() {
		String port = (Executions.getCurrent().getServerPort() == 80) ? ""
				: (":" + Executions.getCurrent().getServerPort());
		/*
		 * String url = Executions.getCurrent().getScheme() + "://" +
		 * Executions.getCurrent().getServerName() + port +
		 * Executions.getCurrent().getContextPath() +
		 * Executions.getCurrent().getDesktop().getRequestPath();
		 */
		String x = "";
		if(System.getProperty("location")!=null){
			if(System.getProperty("location").equalsIgnoreCase("linux")){
				x = "/home/glassfish/report";
			} else{
				x = Executions.getCurrent().getScheme() + "://"
						+ Executions.getCurrent().getServerName() + port
						+ Executions.getCurrent().getContextPath()+"/report";
			}
		}else{
			x = Executions.getCurrent().getScheme() + "://"
					+ Executions.getCurrent().getServerName() + port
					+ Executions.getCurrent().getContextPath()+"/report";
		}
		return x;
	}

	public PlPengajuanSantunanDto getPlPengajuanSantunanDto() {
		return plPengajuanSantunanDto;
	}

	public void setPlPengajuanSantunanDto(
			PlPengajuanSantunanDto plPengajuanSantunanDto) {
		this.plPengajuanSantunanDto = plPengajuanSantunanDto;
	}

	public List<PlPengajuanSantunanDto> getListPengajuanSantunanDto() {
		return listPengajuanSantunanDto;
	}

	public void setListPengajuanSantunanDto(
			List<PlPengajuanSantunanDto> listPengajuanSantunanDto) {
		this.listPengajuanSantunanDto = listPengajuanSantunanDto;
	}

	public PlDisposisiDto getPlDisposisiDto() {
		return plDisposisiDto;
	}

	public void setPlDisposisiDto(PlDisposisiDto plDisposisiDto) {
		this.plDisposisiDto = plDisposisiDto;
	}

	public List<PlDisposisiDto> getListDisposisiDto() {
		return listDisposisiDto;
	}

	public void setListDisposisiDto(List<PlDisposisiDto> listDisposisiDto) {
		this.listDisposisiDto = listDisposisiDto;
	}

	public boolean isShowListDisposisi() {
		return showListDisposisi;
	}

	public void setShowListDisposisi(boolean showListDisposisi) {
		this.showListDisposisi = showListDisposisi;
	}

	public boolean isShowFormDisposisi() {
		return showFormDisposisi;
	}

	public void setShowFormDisposisi(boolean showFormDisposisi) {
		this.showFormDisposisi = showFormDisposisi;
	}

	public DasiJrRefCodeDto getDisposisiRefCode() {
		return disposisiRefCode;
	}

	public void setDisposisiRefCode(DasiJrRefCodeDto disposisiRefCode) {
		this.disposisiRefCode = disposisiRefCode;
	}

	public List<DasiJrRefCodeDto> getListDisposisiRefCode() {
		return listDisposisiRefCode;
	}

	public void setListDisposisiRefCode(
			List<DasiJrRefCodeDto> listDisposisiRefCode) {
		this.listDisposisiRefCode = listDisposisiRefCode;
	}

	public boolean isEditVisible() {
		return editVisible;
	}

	public void setEditVisible(boolean editVisible) {
		this.editVisible = editVisible;
	}

	public boolean isAddVisible() {
		return addVisible;
	}

	public void setAddVisible(boolean addVisible) {
		this.addVisible = addVisible;
	}

	public boolean isCheckDisposisi() {
		return checkDisposisi;
	}

	public void setCheckDisposisi(boolean checkDisposisi) {
		this.checkDisposisi = checkDisposisi;
	}

	public boolean isFlagTambahDisposisi() {
		return flagTambahDisposisi;
	}

	public void setFlagTambahDisposisi(boolean flagTambahDisposisi) {
		this.flagTambahDisposisi = flagTambahDisposisi;
	}

}
