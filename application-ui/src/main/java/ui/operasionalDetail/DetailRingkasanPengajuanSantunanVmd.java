package ui.operasionalDetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.metamodel.domain.Superclass;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import share.DasiJrRefCodeDto;
import share.FndKantorJasaraharjaDto;
import share.PlDataKecelakaanDto;
import share.PlJaminanDto;
import share.PlKorbanKecelakaanDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.JsonUtil;


public class DetailRingkasanPengajuanSantunanVmd extends BaseVmd implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String RINGKASAN_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DetailRingkasanPengajuan.zul";
	
	private final String INDEX_SANTUNAN_PAGE_PATH =UIConstants.BASE_PAGE_PATH
			+"/operasional/OpPengajuanSantunan/_index.zul";
	
	private final String INDEX_VERIFIKASI_PAGE_PATH =UIConstants.BASE_PAGE_PATH
			+"/operasional/OpVerifikasi/_index.zul";

	private final String WS_URI = "/OpPengajuanSantunan";
	private final String WS_URI_LAKA = "/OpDataKecelakaan";
	private final String WS_URI_LOV = "/Lov";

	private PlPengajuanSantunanDto plPengajuanSantunanDto = new PlPengajuanSantunanDto();
	private List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
	private PlPengajuanSantunanDto selectedPengajuan = new PlPengajuanSantunanDto();

	private List<FndKantorJasaraharjaDto> listKantorDto = new ArrayList<>();
	private FndKantorJasaraharjaDto fndKantorJasaraharjaDto = new FndKantorJasaraharjaDto();

	private List<PlDataKecelakaanDto> listDataLakaDto = new ArrayList<>();
	private PlDataKecelakaanDto plDataKecelakaanDto = new PlDataKecelakaanDto();
	private PlDataKecelakaanDto selectedKecelakaan = new PlDataKecelakaanDto();

	private DasiJrRefCodeDto dasiJrRefCodeDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listStatusHubungan = new ArrayList<>();
	private DasiJrRefCodeDto statusHubunganDto = new DasiJrRefCodeDto();

	List<DasiJrRefCodeDto> listJenisIdentitasDto = new ArrayList<>();
	private DasiJrRefCodeDto identitasDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listSifatCidera = new ArrayList<>();
	private DasiJrRefCodeDto sifatCideraDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listStatusProses = new ArrayList<>();
	private DasiJrRefCodeDto statusProsesDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listPenyelesaian = new ArrayList<>();
	private DasiJrRefCodeDto statusPenyelesainDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listJenisPembayaran = new ArrayList<>();
	private DasiJrRefCodeDto jenisPembayaranDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listJenisDokumenDto = new ArrayList<>();
	private DasiJrRefCodeDto jenisDokumenDto = new DasiJrRefCodeDto();

	private List<PlJaminanDto> listPertanggungan = new ArrayList<>();
	private PlJaminanDto plJaminanDto = new PlJaminanDto();

	private List<PlKorbanKecelakaanDto> listKorban = new ArrayList<>();
	private PlKorbanKecelakaanDto plKorbanKecelakaanDto = new PlKorbanKecelakaanDto();
	private PlKorbanKecelakaanDto selectedKorban = new PlKorbanKecelakaanDto();

	private List<PlDataKecelakaanDto> listPlDataKecelakaanDtos = new ArrayList<>();
	
	private PlPenyelesaianSantunanDto plPenyelesaianSantunanDto = new PlPenyelesaianSantunanDto();
	private List<PlPenyelesaianSantunanDto> listPenyelesaianDto = new ArrayList<>();
	


	private Date tglKejadian;
	private Date jamKejadian;
	private String statusLaporan;
	private String laporan1;
	private String laporan2;
	private String laporan3;

	private boolean flagRingkasanSantunan;
	private boolean flagPenyelesaian = false;
	
	private BigDecimal biaya1;
	private BigDecimal biaya2;
	private String cideraKode;
	private String cideraKode2;
	
	private boolean visibleJumlahPengajuan;
	private boolean visibleJumlahPembayaran;
	private boolean visibleCidera2;
	
	@Wire("#popupRingkasan")
	private Window winLov;
	
	private String popUpHandler;
	private UserSessionJR ujr = new UserSessionJR();
	private String id;
	
	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent().getAttribute(
			UIConstants.SESS_LOGIN_ID);


	@NotifyChange({"plPengajuanSantunanDto","id"})
	@Init
	public void baseInit(
			@ExecutionArgParam("popUpHandler") String popUpHandler,
			@ExecutionArgParam("plPengajuanSantunanDto") PlPengajuanSantunanDto pengajuanSantunanDto,
			@ContextParam(ContextType.VIEW) Component view) {
		System.out.println("BASE init called");
		System.out.println(pengajuanSantunanDto.toString());
		if(pengajuanSantunanDto!=null){
			System.out.println("=============");
			System.out.println(JsonUtil.getJson(pengajuanSantunanDto));
		}
		Selectors.wireComponents(view, this, false);
		this.popUpHandler = popUpHandler;
		if(pengajuanSantunanDto != null){
			plPengajuanSantunanDto = pengajuanSantunanDto;			
			viewRingkasan(plPengajuanSantunanDto.getNoBerkas());
			showDataPengajuanSantunan();
			showHistory();
		}
		
		System.out.println("Base popup init called");
	}
	
	
	@Command
	public void close(@BindingParam("window") Window win){
		if(win!=null)
		win.detach();
	}
	
	private void close(){
		if (winLov == null)
			throw new RuntimeException(
					"id popUp tidak sama dengan viewModel");
		winLov.detach();
	}

	

	@GlobalCommand("viewRingkasan")
	public void viewRingkasan(@BindingParam("noBerkas") String noBerkas) {
		//setFlagRingkasanSantunan(true);
		plPengajuanSantunanDto = new PlPengajuanSantunanDto();
		plPengajuanSantunanDto.setNoBerkas(noBerkas);
		//setFlagPenyelesaian(penyelesaianFlag);
		Map<String, Object> map = new HashMap<>();
		map.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse restSantunan = callWs(WS_URI + "/findByNoBerkas", map,
				HttpMethod.POST);
		try {
			listDto = JsonUtil.mapJsonToListObject(restSantunan.getContents(),
					PlPengajuanSantunanDto.class);
			for (PlPengajuanSantunanDto a : listDto) {
				plPengajuanSantunanDto.setIdKecelakaan(a.getIdKecelakaan());
				plPengajuanSantunanDto.setIdKorbanKecelakaan(a.getIdKorbanKecelakaan());
				plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
				plPengajuanSantunanDto.setNoPengajuan(a.getNoPengajuan());
				plPengajuanSantunanDto.setNamaPemohon(a.getNamaPemohon());
				plPengajuanSantunanDto.setAlamatPemohon(a.getAlamatPemohon());
				plPengajuanSantunanDto.setTglPengajuan(a.getTglPengajuan());
				plPengajuanSantunanDto.setTglPenyelesaian(a.getTglPenyelesaian());
				plPengajuanSantunanDto.setOtorisasiFlag(a.getOtorisasiFlag());
				plPengajuanSantunanDto.setCideraKorban(a.getCideraKorban());
				plPengajuanSantunanDto.setJumlahPengajuanMeninggal(a
						.getJumlahPengajuanMeninggal());
				plPengajuanSantunanDto.setJumlahPengajuanLukaluka(a
						.getJumlahPengajuanLukaluka());
				plPengajuanSantunanDto.setJmlPengajuanAmbl(a
						.getJmlPengajuanAmbl());
				plPengajuanSantunanDto.setJmlPengajuanP3k(a
						.getJmlPengajuanP3k());
			}
			
			sifatCideraDto.setRvLowValue(plPengajuanSantunanDto.getCideraKorban());
		} catch (Exception e) {
			e.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "flagRingkasanSantunan");
		BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
		BindUtils.postNotifyChange(null, null, this, "flagPenyelesaian");

		Executions.getCurrent().setAttribute("obj", plPengajuanSantunanDto);
		getPageInfo().setViewMode(true);		
		onView();
	}

	// @Command("detailRingkasanPengajuan")
	// public void detailRingkasanPengajuan(
	// @BindingParam("item") PlPengajuanSantunanDto selectedPengajuan) {
	// if (selectedPengajuan == null
	// || selectedPengajuan.getNoBerkas() == null) {
	//
	// showSmartMsgBox("W001");
	// return;
	//
	// }
	//
	// Executions.getCurrent().setAttribute("obj", selectedPengajuan);
	// getPageInfo().setViewMode(true);
	// navigate(RINGKASAN_PAGE_PATH);
	// }

	public void onView() {
		plPengajuanSantunanDto = (PlPengajuanSantunanDto) Executions
				.getCurrent().getAttribute("obj");
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", plPengajuanSantunanDto.getIdKecelakaan());
		RestResponse rest = callWs(WS_URI_LAKA + "/dataLakaById", map,
				HttpMethod.POST);
		//Messagebox.show("TEST "+plPengajuanSantunanDto.getIdKecelakaan());
		
		plPengajuanSantunanDto.setTglPenyelesaian(plPengajuanSantunanDto
				.getTglPenyelesaian());

		try {
			listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(
					rest.getContents(), PlDataKecelakaanDto.class);
			// Messagebox.show("DATA NYA BERAPA ? "+listPlDataKecelakaanDtos.size());
			for (PlDataKecelakaanDto a : listPlDataKecelakaanDtos) {
				SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String tgl = date.format(a.getTglKejadian());
				String jam = time.format(a.getTglKejadian());
				Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(tgl);
				Date date2 = new SimpleDateFormat("HH:mm").parse(jam);
				setTglKejadian(date1);
				setJamKejadian(date2);
//				String string = a.getNoLaporanPolisi();
//				String[] parts = string.split("/");
//				String part1 = null;
//				String part2 = null;
//				String part3 = null;
//				String part4 = null;
//				String part5 = null;
//				String part6 = null;
//
//				if(parts.length==5){
//					part1 = parts[0];
//					part2 = parts[1];
//					part3 = parts[2];
//					part4 = parts[3];
//					part5 = parts[4];
//					part6 = parts[5];					
//					setLaporan1(part1 + "/" + part2 + "/");
//					setLaporan2(part3);
//					setLaporan3("/" + part4 + "/" + part5 + "/" + part6);
//				}else if(parts.length==4){
//					
//				}

				plDataKecelakaanDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
				plDataKecelakaanDto.setNamaPetugas(a.getNamaPetugas());
				// Messagebox.show("ADA NAMA PETUGAS");
				plDataKecelakaanDto.setTglLaporanPolisi(a.getTglLaporanPolisi());
				plDataKecelakaanDto.setKasus(a.getKasusKecelakaanDesc());
				plDataKecelakaanDto.setSifatKecelakaanDesc(a.getSifatKecelakaanDesc());
				plDataKecelakaanDto.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
				plDataKecelakaanDto.setTglKejadian(date1);
				plDataKecelakaanDto.setDeskripsiLokasi(a.getDeskripsiLokasi());
				// onEdit();

				BindUtils.postNotifyChange(null, null, this, "laporan1");
				BindUtils.postNotifyChange(null, null, this, "laporan2");
				BindUtils.postNotifyChange(null, null, this, "laporan3");
				BindUtils.postNotifyChange(null, null, this,
						"kasusKecelakaanDto");
				BindUtils.postNotifyChange(null, null, this,
						"plDataKecelakaanDto");
				BindUtils.postNotifyChange(null, null, this,
						"sifatKecelakaanDto");
				BindUtils.postNotifyChange(null, null, this, "tglKejadian");
				BindUtils.postNotifyChange(null, null, this, "date1");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void showDataPengajuanSantunan() {
		Map<String, Object> map = new HashMap<>();
		map.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse restHistory = callWs(WS_URI + "/getDataPengajuanByNoBerkas", map,
				HttpMethod.POST);
		sifatCidera();
		try {			
			listDto = JsonUtil.mapJsonToListObject(restHistory.getContents(),
					PlPengajuanSantunanDto.class);
			for (PlPengajuanSantunanDto p : listDto) {
				plPengajuanSantunanDto.setKodeNamaKantor(p.getKodeNamaKantor());
				plPengajuanSantunanDto.setStatusProsesDesc(p.getStatusProsesDesc());
				plPengajuanSantunanDto.setStatusHubunganDes(p.getStatusHubunganDes());	
				
				if(plPengajuanSantunanDto.getOtorisasiFlag().equalsIgnoreCase("3")){
					plPengajuanSantunanDto.setPenyelesaian(p.getPenyelesaian()+" Ke "+p.getDilimpahkanKe()+" - "+p.getNamaKantor() );
				}else{
					plPengajuanSantunanDto.setPenyelesaian(p.getPenyelesaian());					
				}
			}
			for (DasiJrRefCodeDto sifatCidera : listSifatCidera) {
				if (sifatCideraDto.getRvLowValue().equalsIgnoreCase(
						sifatCidera.getRvLowValue())) {
					sifatCideraDto = new DasiJrRefCodeDto();
					sifatCideraDto = sifatCidera;
				}
			}
			
			
			rupiahPengajuan();
			if(sifatCideraDto.getRvLowValue() !=null){
				setVisibleJumlahPembayaran(true);
				setVisibleJumlahPengajuan(true);
			}
			
			BindUtils.postNotifyChange(null, null, this,"plPengajuanSantunanDto");
			BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");
			

		} catch (Exception e) {
			e.printStackTrace();
		}

		// ================GET DATA KORBAN========================
		Map<String, Object> mapInput = new HashMap<>();
		// Messagebox.show("test "+plPengajuanSantunanDto.getIdKorbanKecelakaan());
		mapInput.put("idKorban", plPengajuanSantunanDto.getIdKorbanKecelakaan());
		RestResponse rest2 = callWs(WS_URI_LAKA + "/korbanLakaByIdKorban",
				mapInput, HttpMethod.POST);
		
		try {
			listKorban = JsonUtil.mapJsonToListObject(rest2.getContents(),
					PlKorbanKecelakaanDto.class);

			for (PlKorbanKecelakaanDto a : listKorban) {
				plDataKecelakaanDto.setNamaKorban(a.getNama());
				plDataKecelakaanDto.setCidera(a.getCideraDesc());
				plDataKecelakaanDto.setJenisIdentitas(a.getJenisIdentitasDesc());
				plDataKecelakaanDto.setNoIdentitas(a.getNoIdentitas());
				plDataKecelakaanDto.setStatus(a.getStatusKorbanDesc());
				plDataKecelakaanDto.setProvinsiDesc(a.getNamaProvinsi());
				plDataKecelakaanDto.setKabKotaDesc(a.getNamaKabkota());
				plDataKecelakaanDto.setCamatDesc(a.getNamaCamat());
				plDataKecelakaanDto.setAlamat(a.getAlamat());
				plDataKecelakaanDto.setNoTelp(a.getNoTelp());
				plDataKecelakaanDto.setJaminan(a.getPertanggunganDesc());
			
			}
			BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//====================== Data Penyelesaian===================================================
		Map<String, Object> mapPenyelesaian = new HashMap<>();
		mapPenyelesaian.put("noBerkas", plPenyelesaianSantunanDto.getNoBerkas());
		RestResponse restPenyelesaian = callWs(WS_URI + "/findOnePenyelesaian", mapPenyelesaian,
				HttpMethod.POST);
		try {
			listPenyelesaianDto = JsonUtil.mapJsonToListObject(restPenyelesaian.getContents(),
					PlPenyelesaianSantunanDto.class);
			for (PlPenyelesaianSantunanDto a : listPenyelesaianDto) {
				plPenyelesaianSantunanDto.setNoBerkas(a.getNoBerkas());;
				plPenyelesaianSantunanDto.setJmlByrAmbl(a.getJmlByrAmbl());
				plPenyelesaianSantunanDto.setJmlByrP3k(a.getJmlByrP3k());
				plPenyelesaianSantunanDto.setJumlahDibayarMeninggal(a.getJumlahDibayarMeninggal());
				plPenyelesaianSantunanDto.setJumlahDibayarLukaluka(a.getJumlahDibayarLukaluka());
				plPenyelesaianSantunanDto.setJumlahDibayarPenguburan(a.getJumlahDibayarPenguburan());
				
			}
			
			sifatCideraDto.setRvLowValue(plPengajuanSantunanDto.getCideraKorban());
			BindUtils.postNotifyChange(null, null, this, "plPenyelesaianSantunanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Command("showHistory")
	public void showHistory() {
		Map<String, Object> map = new HashMap<>();
		map.put("noPengajuan", plPengajuanSantunanDto.getNoPengajuan());
		RestResponse restHistory = callWs(WS_URI + "/findByNoPengajuan", map,
				HttpMethod.POST);
		try {			
			listDto = JsonUtil.mapJsonToListObject(restHistory.getContents(),
					PlPengajuanSantunanDto.class);
			for (PlPengajuanSantunanDto p : listDto) {
				plPengajuanSantunanDto.setNoBerkas(p.getNoBerkas());
				plPengajuanSantunanDto.setKodeNamaKantor(p.getKodeNamaKantor());
				plPengajuanSantunanDto.setTglPengajuan(p.getTglPengajuan());
				plPengajuanSantunanDto.setTglPenyelesaian(p.getTglPenyelesaian());
				plPengajuanSantunanDto.setStatusProses(p.getStatusProses());
				plPengajuanSantunanDto.setPenyelesaian(p.getPenyelesaian());				
			}
			BindUtils.postNotifyChange(null, null, this,
					"plPengajuanSantunanDto");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Command
	public void back(){
		close();
	}
	
	public void sifatCidera() {
		RestResponse restCidera = callWs(WS_URI_LOV + "/getListSifatCidera",
				new HashMap<String, Object>(), HttpMethod.POST);
		
		try {
			listSifatCidera = JsonUtil.mapJsonToListObject(
					restCidera.getContents(), DasiJrRefCodeDto.class);
			
			BindUtils.postNotifyChange(null, null, this, "listSifatCidera");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@NotifyChange({ "cideraKode", "cideraKode2","visibleCidera2" })
	@Command
	public void rupiahPengajuan() {
		
		if (sifatCideraDto.getRvLowValue() == "01"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("01")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setVisibleCidera2(false);
		} else if (sifatCideraDto.getRvLowValue() == "02"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("02")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setVisibleCidera2(false);

		} else if (sifatCideraDto.getRvLowValue() == "04"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("04")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setVisibleCidera2(false);

		} else if (sifatCideraDto.getRvLowValue() == "05"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("05")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setVisibleCidera2(true);

		} else if (sifatCideraDto.getRvLowValue() == "06"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("06")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setVisibleCidera2(true);

		} else if (sifatCideraDto.getRvLowValue() == "07"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("07")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setVisibleCidera2(false);
		} else if (sifatCideraDto.getRvLowValue() == "08"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("08")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setVisibleCidera2(true);
		}
	}
	
	
//	@Command("back")
//	public void keluar() {
//		if (flagPenyelesaian == true) {
//			getPageInfo().setListMode(true);
//			navigate(UIConstants.BASE_PAGE_PATH
//			+ "/operasional/OpPenyelesaianPengajuan/_index.zul");
//		} else {
//			getPageInfo().setListMode(true);
//			navigate(INDEX_SANTUNAN_PAGE_PATH);
//		}
//		
//	}


	public PlPengajuanSantunanDto getPlPengajuanSantunanDto() {
		return plPengajuanSantunanDto;
	}

	public void setPlPengajuanSantunanDto(
			PlPengajuanSantunanDto plPengajuanSantunanDto) {
		this.plPengajuanSantunanDto = plPengajuanSantunanDto;
	}

	public List<PlPengajuanSantunanDto> getListDto() {
		return listDto;
	}

	public void setListDto(List<PlPengajuanSantunanDto> listDto) {
		this.listDto = listDto;
	}

	public PlPengajuanSantunanDto getSelectedPengajuan() {
		return selectedPengajuan;
	}

	public void setSelectedPengajuan(PlPengajuanSantunanDto selectedPengajuan) {
		this.selectedPengajuan = selectedPengajuan;
	}

	public List<FndKantorJasaraharjaDto> getListKantorDto() {
		return listKantorDto;
	}

	public void setListKantorDto(List<FndKantorJasaraharjaDto> listKantorDto) {
		this.listKantorDto = listKantorDto;
	}

	public FndKantorJasaraharjaDto getFndKantorJasaraharjaDto() {
		return fndKantorJasaraharjaDto;
	}

	public void setFndKantorJasaraharjaDto(
			FndKantorJasaraharjaDto fndKantorJasaraharjaDto) {
		this.fndKantorJasaraharjaDto = fndKantorJasaraharjaDto;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDto() {
		return listDataLakaDto;
	}

	public void setListDataLakaDto(List<PlDataKecelakaanDto> listDataLakaDto) {
		this.listDataLakaDto = listDataLakaDto;
	}

	public PlDataKecelakaanDto getPlDataKecelakaanDto() {
		return plDataKecelakaanDto;
	}

	public void setPlDataKecelakaanDto(PlDataKecelakaanDto plDataKecelakaanDto) {
		this.plDataKecelakaanDto = plDataKecelakaanDto;
	}

	public PlDataKecelakaanDto getSelectedKecelakaan() {
		return selectedKecelakaan;
	}

	public void setSelectedKecelakaan(PlDataKecelakaanDto selectedKecelakaan) {
		this.selectedKecelakaan = selectedKecelakaan;
	}

	public DasiJrRefCodeDto getDasiJrRefCodeDto() {
		return dasiJrRefCodeDto;
	}

	public void setDasiJrRefCodeDto(DasiJrRefCodeDto dasiJrRefCodeDto) {
		this.dasiJrRefCodeDto = dasiJrRefCodeDto;
	}

	public List<DasiJrRefCodeDto> getListStatusHubungan() {
		return listStatusHubungan;
	}

	public void setListStatusHubungan(List<DasiJrRefCodeDto> listStatusHubungan) {
		this.listStatusHubungan = listStatusHubungan;
	}

	public DasiJrRefCodeDto getStatusHubunganDto() {
		return statusHubunganDto;
	}

	public void setStatusHubunganDto(DasiJrRefCodeDto statusHubunganDto) {
		this.statusHubunganDto = statusHubunganDto;
	}

	public List<DasiJrRefCodeDto> getListJenisIdentitasDto() {
		return listJenisIdentitasDto;
	}

	public void setListJenisIdentitasDto(
			List<DasiJrRefCodeDto> listJenisIdentitasDto) {
		this.listJenisIdentitasDto = listJenisIdentitasDto;
	}

	public DasiJrRefCodeDto getIdentitasDto() {
		return identitasDto;
	}

	public void setIdentitasDto(DasiJrRefCodeDto identitasDto) {
		this.identitasDto = identitasDto;
	}

	public List<DasiJrRefCodeDto> getListSifatCidera() {
		return listSifatCidera;
	}

	public void setListSifatCidera(List<DasiJrRefCodeDto> listSifatCidera) {
		this.listSifatCidera = listSifatCidera;
	}

	public DasiJrRefCodeDto getSifatCideraDto() {
		return sifatCideraDto;
	}

	public void setSifatCideraDto(DasiJrRefCodeDto sifatCideraDto) {
		this.sifatCideraDto = sifatCideraDto;
	}

	public List<DasiJrRefCodeDto> getListStatusProses() {
		return listStatusProses;
	}

	public void setListStatusProses(List<DasiJrRefCodeDto> listStatusProses) {
		this.listStatusProses = listStatusProses;
	}

	public DasiJrRefCodeDto getStatusProsesDto() {
		return statusProsesDto;
	}

	public void setStatusProsesDto(DasiJrRefCodeDto statusProsesDto) {
		this.statusProsesDto = statusProsesDto;
	}

	public List<DasiJrRefCodeDto> getListPenyelesaian() {
		return listPenyelesaian;
	}

	public void setListPenyelesaian(List<DasiJrRefCodeDto> listPenyelesaian) {
		this.listPenyelesaian = listPenyelesaian;
	}

	public DasiJrRefCodeDto getStatusPenyelesainDto() {
		return statusPenyelesainDto;
	}

	public void setStatusPenyelesainDto(DasiJrRefCodeDto statusPenyelesainDto) {
		this.statusPenyelesainDto = statusPenyelesainDto;
	}

	public List<DasiJrRefCodeDto> getListJenisPembayaran() {
		return listJenisPembayaran;
	}

	public void setListJenisPembayaran(
			List<DasiJrRefCodeDto> listJenisPembayaran) {
		this.listJenisPembayaran = listJenisPembayaran;
	}

	public DasiJrRefCodeDto getJenisPembayaranDto() {
		return jenisPembayaranDto;
	}

	public void setJenisPembayaranDto(DasiJrRefCodeDto jenisPembayaranDto) {
		this.jenisPembayaranDto = jenisPembayaranDto;
	}

	public List<DasiJrRefCodeDto> getListJenisDokumenDto() {
		return listJenisDokumenDto;
	}

	public void setListJenisDokumenDto(
			List<DasiJrRefCodeDto> listJenisDokumenDto) {
		this.listJenisDokumenDto = listJenisDokumenDto;
	}

	public DasiJrRefCodeDto getJenisDokumenDto() {
		return jenisDokumenDto;
	}

	public void setJenisDokumenDto(DasiJrRefCodeDto jenisDokumenDto) {
		this.jenisDokumenDto = jenisDokumenDto;
	}

	public List<PlJaminanDto> getListPertanggungan() {
		return listPertanggungan;
	}

	public void setListPertanggungan(List<PlJaminanDto> listPertanggungan) {
		this.listPertanggungan = listPertanggungan;
	}

	public PlJaminanDto getPlJaminanDto() {
		return plJaminanDto;
	}

	public void setPlJaminanDto(PlJaminanDto plJaminanDto) {
		this.plJaminanDto = plJaminanDto;
	}

	public List<PlKorbanKecelakaanDto> getListKorban() {
		return listKorban;
	}

	public void setListKorban(List<PlKorbanKecelakaanDto> listKorban) {
		this.listKorban = listKorban;
	}

	public PlKorbanKecelakaanDto getPlKorbanKecelakaanDto() {
		return plKorbanKecelakaanDto;
	}

	public void setPlKorbanKecelakaanDto(
			PlKorbanKecelakaanDto plKorbanKecelakaanDto) {
		this.plKorbanKecelakaanDto = plKorbanKecelakaanDto;
	}

	public PlKorbanKecelakaanDto getSelectedKorban() {
		return selectedKorban;
	}

	public void setSelectedKorban(PlKorbanKecelakaanDto selectedKorban) {
		this.selectedKorban = selectedKorban;
	}

	public List<PlDataKecelakaanDto> getListPlDataKecelakaanDtos() {
		return listPlDataKecelakaanDtos;
	}

	public void setListPlDataKecelakaanDtos(
			List<PlDataKecelakaanDto> listPlDataKecelakaanDtos) {
		this.listPlDataKecelakaanDtos = listPlDataKecelakaanDtos;
	}

	public Date getTglKejadian() {
		return tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public Date getJamKejadian() {
		return jamKejadian;
	}

	public void setJamKejadian(Date jamKejadian) {
		this.jamKejadian = jamKejadian;
	}

	public String getStatusLaporan() {
		return statusLaporan;
	}

	public void setStatusLaporan(String statusLaporan) {
		this.statusLaporan = statusLaporan;
	}

	public String getLaporan1() {
		return laporan1;
	}

	public void setLaporan1(String laporan1) {
		this.laporan1 = laporan1;
	}

	public String getLaporan2() {
		return laporan2;
	}

	public void setLaporan2(String laporan2) {
		this.laporan2 = laporan2;
	}

	public String getLaporan3() {
		return laporan3;
	}

	public void setLaporan3(String laporan3) {
		this.laporan3 = laporan3;
	}

	public boolean isFlagRingkasanSantunan() {
		return flagRingkasanSantunan;
	}

	public void setFlagRingkasanSantunan(boolean flagRingkasanSantunan) {
		this.flagRingkasanSantunan = flagRingkasanSantunan;
	}

	public boolean isFlagPenyelesaian() {
		return flagPenyelesaian;
	}

	public void setFlagPenyelesaian(boolean flagPenyelesaian) {
		this.flagPenyelesaian = flagPenyelesaian;
	}


	public BigDecimal getBiaya1() {
		return biaya1;
	}


	public void setBiaya1(BigDecimal biaya1) {
		this.biaya1 = biaya1;
	}


	public BigDecimal getBiaya2() {
		return biaya2;
	}


	public void setBiaya2(BigDecimal biaya2) {
		this.biaya2 = biaya2;
	}


	public String getCideraKode() {
		return cideraKode;
	}


	public void setCideraKode(String cideraKode) {
		this.cideraKode = cideraKode;
	}


	public String getCideraKode2() {
		return cideraKode2;
	}


	public void setCideraKode2(String cideraKode2) {
		this.cideraKode2 = cideraKode2;
	}


	public boolean isVisibleJumlahPengajuan() {
		return visibleJumlahPengajuan;
	}


	public void setVisibleJumlahPengajuan(boolean visibleJumlahPengajuan) {
		this.visibleJumlahPengajuan = visibleJumlahPengajuan;
	}


	public boolean isVisibleJumlahPembayaran() {
		return visibleJumlahPembayaran;
	}


	public void setVisibleJumlahPembayaran(boolean visibleJumlahPembayaran) {
		this.visibleJumlahPembayaran = visibleJumlahPembayaran;
	}


	public boolean isVisibleCidera2() {
		return visibleCidera2;
	}


	public void setVisibleCidera2(boolean visibleCidera2) {
		this.visibleCidera2 = visibleCidera2;
	}


	public PlPenyelesaianSantunanDto getPlPenyelesaianSantunanDto() {
		return plPenyelesaianSantunanDto;
	}


	public void setPlPenyelesaianSantunanDto(
			PlPenyelesaianSantunanDto plPenyelesaianSantunanDto) {
		this.plPenyelesaianSantunanDto = plPenyelesaianSantunanDto;
	}


	public List<PlPenyelesaianSantunanDto> getListPenyelesaianDto() {
		return listPenyelesaianDto;
	}


	public void setListPenyelesaianDto(
			List<PlPenyelesaianSantunanDto> listPenyelesaianDto) {
		this.listPenyelesaianDto = listPenyelesaianDto;
	}

}
