package ui.operasionalDetail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import share.PlDataKecelakaanDto;
import share.PlInstansiDto;
import share.PlJaminanDto;
import share.PlKorbanKecelakaanDto;
import share.PlPengajuanSantunanDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseLovVmd;
import common.ui.BaseVmd;
import common.util.JsonUtil;

//@Init(superclass=true)
public class CariDataLakaVmd extends BaseVmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	@Wire("popupCariDataLaka")
//	private Window popupCariDataLaka;
	
	private String popUpHandler;
	private final String WS_URI_LOV = "/Lov";
	
	private List<PlInstansiDto> listInstansiDto = new ArrayList<>();
	private PlInstansiDto instansiDto = new PlInstansiDto();
	
	private List<PlJaminanDto> listJenisPertanggunganDto = new ArrayList<>();
	private PlJaminanDto jenisPertanggunganDto = new PlJaminanDto();
	
	private List<PlDataKecelakaanDto> listDataLakaDto =new ArrayList<>();
	private List<PlDataKecelakaanDto> listDataLakaDtoCopy =new ArrayList<>();
	private PlDataKecelakaanDto dataLakaDto = new PlDataKecelakaanDto();
	
	private List<PlKorbanKecelakaanDto> listKorbanLakaDto =new ArrayList<>();
	private List<PlKorbanKecelakaanDto> listKorbanLakaDtoCopy =new ArrayList<>();
	private PlKorbanKecelakaanDto dataKorbanDto = new PlKorbanKecelakaanDto();
	
	//filter
	private Date detailKejadianStartDate;
	private Date detailKejadianEndDate;
	private String detailInstansi;
	private Date detailLaporanStartDate;
	private Date detailLaporanEndDate;
	private String detailNoLaporanPolisi;
	private String detailNamaKorban;
	private List<String> listStatusLp = new ArrayList<>();
	private String statusLP;
	
	private int pageSize = 5;
	private String coba;
	
	@NotifyChange({"ujr","id"})
	@Init
	public void baseInit(
//			@ExecutionArgParam("popUpHandler") String popUpHandler,
			@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);		
//		this.popUpHandler = popUpHandler;
		listJenisPertanggungan();
		listStatusLp();
		System.out.println("Base popup init called");
	}
	protected void loadList() {
		listJenisPertanggungan();
		listStatusLp();
	}

	@NotifyChange({"statusLP","listStatusLp"})
	public void listStatusLp(){
		listStatusLp.add("Y");
		listStatusLp.add("N");
	}
	
	@Command
	public void listJenisPertanggungan() {
		RestResponse listJenisPertanggunganRest = callWs(WS_URI_LOV
				+ "/getListJenisPertanggungan", new HashMap<String, Object>(),
				HttpMethod.POST);
		try {
			listJenisPertanggunganDto = JsonUtil.mapJsonToListObject(
					listJenisPertanggunganRest.getContents(),
					PlJaminanDto.class);
			BindUtils.postNotifyChange(null, null, this,
					"listJenisPertanggunganDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void searchInstansi(@BindingParam("item")String searchInstansi) {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchInstansi);
		RestResponse rest = callWs(WS_URI_LOV + "/getListInstansi", map,
				HttpMethod.POST);
		try {
			listInstansiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlInstansiDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listInstansiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange("listDataLakaDtoCopy")
	@Command("cariDataLaka")
	public void cariDataLaka(){
		Map<String, Object> map =new HashMap<>();
		map.put("kejadianStartDate", dateToString(detailKejadianStartDate));
		map.put("kejadianEndDate", dateToString(detailKejadianEndDate));
		map.put("laporanStartDate", dateToString(detailLaporanStartDate));
		map.put("laporanEndDate", dateToString(detailLaporanEndDate));
		map.put("instansi", instansiDto.getKodeInstansi());
		map.put("noLaporan", detailNoLaporanPolisi);
		map.put("namaKorban", detailNamaKorban);
		map.put("jenisPertanggungan", jenisPertanggunganDto.getKodeJaminan());
		map.put("statusLp", statusLP);
		RestResponse rest = callWs(WS_URI_LOV+"/getListCariDataLaka", map, HttpMethod.POST);
		try {
			listKorbanLakaDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlKorbanKecelakaanDto.class);
			listKorbanLakaDtoCopy = new ArrayList<>();
			setListKorbanLakaDtoCopy(listKorbanLakaDto);
			BindUtils.postNotifyChange(null, null, this, "listKorbanLakaDtoCopy");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void pilihData(@BindingParam("popup") String popup,
			@BindingParam("item") PlKorbanKecelakaanDto selected, @BindingParam("window") Window win){
		if (selected == null|| selected.getIdKecelakaan() == null) {
			showSmartMsgBox("W001");
			return;
		}
		Map<String, Object> map = new HashMap<>();
//		map.put("idKecelakaan", selected.getIdKecelakaan());
//		map.put("plDataKecelakaanDto", selected);
		Executions.getCurrent().setAttribute("obj2", selected);		
		BindUtils.postGlobalCommand(null, null, "addPengajuanSantunan", map);
		getPageInfo().setAddDetailMode(true);
		try {
			((Window) Executions.createComponents(popup, null, map)).doModal();
			closeDetail(win);
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	@Command
	public void closeDetail(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}
	
	
	public List<PlInstansiDto> getListInstansiDto() {
		return listInstansiDto;
	}

	public void setListInstansiDto(List<PlInstansiDto> listInstansiDto) {
		this.listInstansiDto = listInstansiDto;
	}

	public PlInstansiDto getInstansiDto() {
		return instansiDto;
	}

	public void setInstansiDto(PlInstansiDto instansiDto) {
		this.instansiDto = instansiDto;
	}

	public List<PlJaminanDto> getListJenisPertanggunganDto() {
		return listJenisPertanggunganDto;
	}

	public void setListJenisPertanggunganDto(
			List<PlJaminanDto> listJenisPertanggunganDto) {
		this.listJenisPertanggunganDto = listJenisPertanggunganDto;
	}

	public PlJaminanDto getJenisPertanggunganDto() {
		return jenisPertanggunganDto;
	}

	public void setJenisPertanggunganDto(PlJaminanDto jenisPertanggunganDto) {
		this.jenisPertanggunganDto = jenisPertanggunganDto;
	}

	public Date getDetailKejadianStartDate() {
		return detailKejadianStartDate;
	}

	public void setDetailKejadianStartDate(Date detailKejadianStartDate) {
		this.detailKejadianStartDate = detailKejadianStartDate;
	}

	public Date getDetailKejadianEndDate() {
		return detailKejadianEndDate;
	}

	public void setDetailKejadianEndDate(Date detailKejadianEndDate) {
		this.detailKejadianEndDate = detailKejadianEndDate;
	}

	public String getDetailInstansi() {
		return detailInstansi;
	}

	public void setDetailInstansi(String detailInstansi) {
		this.detailInstansi = detailInstansi;
	}

	public Date getDetailLaporanStartDate() {
		return detailLaporanStartDate;
	}

	public void setDetailLaporanStartDate(Date detailLaporanStartDate) {
		this.detailLaporanStartDate = detailLaporanStartDate;
	}

	public Date getDetailLaporanEndDate() {
		return detailLaporanEndDate;
	}

	public void setDetailLaporanEndDate(Date detailLaporanEndDate) {
		this.detailLaporanEndDate = detailLaporanEndDate;
	}

	public String getDetailNoLaporanPolisi() {
		return detailNoLaporanPolisi;
	}

	public void setDetailNoLaporanPolisi(String detailNoLaporanPolisi) {
		this.detailNoLaporanPolisi = detailNoLaporanPolisi;
	}

	public String getDetailNamaKorban() {
		return detailNamaKorban;
	}

	public void setDetailNamaKorban(String detailNamaKorban) {
		this.detailNamaKorban = detailNamaKorban;
	}
	public String getStatusLP() {
		return statusLP;
	}

	public void setStatusLP(String statusLP) {
		this.statusLP = statusLP;
	}


	public List<String> getListStatusLp() {
		return listStatusLp;
	}


	public void setListStatusLp(List<String> listStatusLp) {
		this.listStatusLp = listStatusLp;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDto() {
		return listDataLakaDto;
	}

	public void setListDataLakaDto(List<PlDataKecelakaanDto> listDataLakaDto) {
		this.listDataLakaDto = listDataLakaDto;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDtoCopy() {
		return listDataLakaDtoCopy;
	}

	public void setListDataLakaDtoCopy(List<PlDataKecelakaanDto> listDataLakaDtoCopy) {
		this.listDataLakaDtoCopy = listDataLakaDtoCopy;
	}

	public PlDataKecelakaanDto getDataLakaDto() {
		return dataLakaDto;
	}

	public void setDataLakaDto(PlDataKecelakaanDto dataLakaDto) {
		this.dataLakaDto = dataLakaDto;
	}
	
	public String getCoba() {
		return coba;
	}

	public void setCoba(String coba) {
		this.coba = coba;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public List<PlKorbanKecelakaanDto> getListKorbanLakaDto() {
		return listKorbanLakaDto;
	}
	public void setListKorbanLakaDto(List<PlKorbanKecelakaanDto> listKorbanLakaDto) {
		this.listKorbanLakaDto = listKorbanLakaDto;
	}
	public List<PlKorbanKecelakaanDto> getListKorbanLakaDtoCopy() {
		return listKorbanLakaDtoCopy;
	}
	public void setListKorbanLakaDtoCopy(
			List<PlKorbanKecelakaanDto> listKorbanLakaDtoCopy) {
		this.listKorbanLakaDtoCopy = listKorbanLakaDtoCopy;
	}
	public PlKorbanKecelakaanDto getDataKorbanDto() {
		return dataKorbanDto;
	}
	public void setDataKorbanDto(PlKorbanKecelakaanDto dataKorbanDto) {
		this.dataKorbanDto = dataKorbanDto;
	}
	
}
