package ui.operasionalDetail;

import java.rmi.RemoteException;

import Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_psProxy;
import Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType;
import Bpm.UpdateDocInfo.PropertyType;
import Bpm.UpdateDocInfo.UpdateDocInfoResultType;
import Bpm.UpdateDocInfo.UpdateDocInfoType;

public class TestUpdateDocumentInfo {
	public static void main(String[] args) {
		UpdateDocInfoType param = new UpdateDocInfoType();
		param.setDDocName("2019-01-18-29E65BE41D");
		param.setDID(8948);
		param.setDRevLabel("1");
		
		//costume meta data
		PropertyType property[] = new PropertyType[1];
		PropertyType p = new PropertyType();
		p.setName("xComments");//hardcode
		p.setValue("FORMULIR PENGAJUAN SANTUNAN - N");
		property[0] = p;
		
		
		
		param.setCustomDocMetadata(property);
		
		UpdateDocInfoResultType result = new UpdateDocInfoResultType();
		
		TestUpdateDocumentInfo  thisClass= new TestUpdateDocumentInfo();
		try {
			result = thisClass.hitservice(param);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("status Info: " + result.getStatusInfo().getStatusCode());
		System.out.println("status Message: " + result.getStatusInfo().getStatusMessage());
	}
	
	public UpdateDocInfoResultType hitservice(UpdateDocInfoType parameters) throws RemoteException{
		Bpm_ecms_update_doc_info_psProxy svc = new Bpm_ecms_update_doc_info_psProxy();
		Bpm_ecms_update_doc_info_ps_PortType port = svc.getBpm_ecms_update_doc_info_ps_PortType();
		return port.update_doc_info(parameters);
	}
}
