package ui.operasionalDetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import Bpm.Ecms.queryByNoBerkas.ContentResult;
import Bpm.Ecms.queryByNoBerkas.DirectoryName;
import Bpm.Ecms.queryByNoBerkas.Response;
import Bpm.ListTaskByUser.UserTask;
import Bpm.ListTaskByUser.UserTaskService;
import Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_psProxy;
import Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType;
import Bpm.UpdateDocInfo.PropertyType;
import Bpm.UpdateDocInfo.UpdateDocInfoResultType;
import Bpm.UpdateDocInfo.UpdateDocInfoType;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.util.CommonConstants;
import common.util.JsonUtil;
import share.DasiJrRefCodeDto;
import share.DataBpjsDto;
import share.LampiranDto;
import share.PlDataKecelakaanDto;
import share.PlPengajuanSantunanDto;

public class DetailAbsahVmd extends BaseVmd implements Serializable{

	
	/**
	 * 
	 */
	private final String WS_URI_LOV = "/Lov";
	private final String WS_URI = "/AbsahBerkas";
	private static final long serialVersionUID = 1L;
	private List<LampiranDto> listDocs = new ArrayList<>();
	private List<LampiranDto> listDocsSelected = new ArrayList<>();
	private List<DasiJrRefCodeDto> listDocsType = new ArrayList<>();
	private List<PlPengajuanSantunanDto> listPengajuan = new ArrayList<>();
	private LampiranDto selectedDocs;
	private DasiJrRefCodeDto selectedDocsType;
	private PlPengajuanSantunanDto pengajuan;
	private PlPengajuanSantunanDto pengajuan2;
	private DataBpjsDto dataBpjs;
	private List<DataBpjsDto> listDataBpjs;
	private String detailHandler;
	private Form formMaster = new SimpleForm();
	private BigDecimal totalBayar = new BigDecimal(0);
	private BigDecimal akumMeninggal;
	private BigDecimal akumLukaLuka;
	private BigDecimal akumCacatTetap;
	private BigDecimal akumPenguburan;
	private BigDecimal akumAmbulance;
	private BigDecimal akumP3k;
	private BigDecimal totMeninggal;
	private BigDecimal totLukaLuka;
	private BigDecimal totCacatTetap;
	private BigDecimal totPenguburan;
	private BigDecimal totAmbulance;
	private BigDecimal totP3k;
	private List<DasiJrRefCodeDto> listDisposisi;
	private UserTask userTask = (UserTask) Executions.getCurrent().getAttribute("obj2");
	
	
	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	@Wire("#opAbsahDet")
	private Window winLov;
	@Command
	public void close(){
		if (winLov == null)
			throw new RuntimeException(
					"id popUp tidak sama dengan viewModel");
		winLov.detach();
	}
	
	@Command
	public void nextStep(){
		if(pengajuan2==null){
			Messagebox.show("data kosong");
			return;
		}
		
		pengajuan2.setLastUpdatedBy(getCurrentUserSessionJR().getUserName());
//		pengajuan2.setStatusProses(statusProses);
//		pengajuan2.set
		
		Map<String, Object> input = new HashMap<>();
		RestResponse rest = new RestResponse();
		input.put("option","nextStep");
		input.put("plPengajuanSantunanDto", pengajuan2);
		input.put("userSess", getCurrentUserSessionJR());
		try{
			/* for logging puspose do not delete this line */System.out.println(new Date()+" : Trying to process next step");
			rest = callWs(WS_URI+"/general", input, HttpMethod.POST);
			Map<String, Object> out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			if(rest.getStatus()==CommonConstants.OK_REST_STATUS){
				Messagebox.show("Data berhasil diproses ke otorisasi");
			}else{
				Messagebox.show("Data tidak berhasil di proses");
			}
		}catch(Exception s){
			s.printStackTrace();
		}
	}
	
	@Command("disposisiBerkas")
	public void showPopup(
			@BindingParam("popup") String popup,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();
		
		args.put("popUpHandler", globalHandleMethodName);
		
		if (!beforePopup(args, popup))
			return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	private void getDisposisi(){
//		disposisi
		Map<String, Object> input = new HashMap<>();
		input.put("option", "disposisi");
		input.put("noBerkas", pengajuan.getNoBerkas());
		input.put("userSess", getCurrentUserSessionJR());
		List<DasiJrRefCodeDto> listDisposisi = new ArrayList<>();
		RestResponse rest = new RestResponse();
		try{
			/* for logging puspose do not delete this line */System.out.println(new Date()+" : Trying to get disposisi");
			rest = callWs(WS_URI+"/general", input, HttpMethod.POST);
			Map<String, Object> out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			listDisposisi = (List<DasiJrRefCodeDto>) out.get("disposisi");
			System.out.println("size list Disposisi : "+listDisposisi.size());
		}catch(Exception s){
			s.printStackTrace();
		}
		if(this.listDisposisi!=null && !this.listDisposisi.isEmpty()){
			this.listDisposisi.clear();
		}
		this.listDisposisi = new ArrayList<>();
		this.listDisposisi.addAll(listDisposisi);
		BindUtils.postNotifyChange(null, null, this, "listDisposisi");
	}
	
	protected boolean beforePopup(Map<String, Object> args, String popup) {
		args.put("plPengajuanSantunanDto", pengajuan2);
		args.put("caller", "absahBerkas");
		if(listDisposisi.size()>0 && listDisposisi.size()>1){
			try {
				for(DasiJrRefCodeDto j : listDisposisi){
					if(j.getRvAbbreviation().equalsIgnoreCase("BV")){
						try {
							args.put("disposisi", j);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			args.put("disposisi", listDisposisi.get(0));
		}
		return true;
	}
	
	@GlobalCommand("disposisiHandler")
	public void titleHandler(@BindingParam("noBerkas") PlPengajuanSantunanDto selected) {
//		if(pengajuan2 != null){
//			this.pengajuan2 = selected;
//		}
	}
	
	@Command("save")
	public void save(){
		//TODO: Save (update Docs)
		updateDocs();
		//update BPM ke proses selanjutnya
				 try {
					 String pattern = "dd-MM-yyyy HH:mm:ss";
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

						String dateFormat = simpleDateFormat.format(new Date());
						//userTask = (UserTask) Executions.getCurrent().getAttribute("obj2");
						 if(userTask != null){
							 //if(ut.getNomorPermohonan().equalsIgnoreCase(plPengajuanSantunanDto.getNoBerkas())){
//								 UserTaskService.updateUserTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), plPengajuanSantunanDto.getNoBerkas(),
//										 "Entry Data Pengajuan","APPROVE", "", "", userSession.getUserName(), dateFormat,plPengajuanSantunanDto.getIdKecelakaan(), userSession.getUserLdap());
								 String instanceId = userTask.getOtherInfo2().substring(5);
								 UserTask uTask = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), "Identifikasi Kelengkapan dan Keabsahan Berkas", instanceId);
								 uTask.setOtherInfo1("Identifikasi Kelengkapan dan Keabsahan Berkas");
								 uTask.setNomorPermohonan(userTask.getNomorPermohonan());
								 uTask.setKodeCabang(userTask.getKodeCabang());
								 uTask.setCreationDate(userTask.getLastUpdateDate());
								 uTask.setCreatedBy(getCurrentUserSessionJR().getUserName());
								 uTask.setLastUpdatedBy(getCurrentUserSessionJR().getUserName());
								 uTask.setLastUpdateDate(dateFormat);
								 uTask.setIdGUID(userTask.getIdGUID());
								 uTask.setPengajuanType(userTask.getPengajuanType());
								 uTask.setKodeKantor(userTask.getKodeKantor());
								 uTask.setOtherInfo2(userTask.getOtherInfo2());
								 uTask.setOtherInfo3(userTask.getOtherInfo3());
								 uTask.setOtherInfo4(userTask.getOtherInfo4());
								 uTask.setOtherInfo5(userTask.getOtherInfo5());
								 uTask.setTaskNumber(userTask.getTaskNumber());
								 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),getCurrentUserSessionJR().getUserLdap(), "APPROVE", uTask, "");
							 //}
						 }
						 else{
							 	 UserTaskService.updateUserTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
							 	 pengajuan2.getNoBerkas(), "Identifikasi Kelengkapan dan Keabsahan Berkas","APPROVE",
							 	 "", "", getCurrentUserSessionJR().getUserName(),dateFormat , pengajuan2.getIdKecelakaan(), getCurrentUserSessionJR().getUserLdap());
						 }
				 } catch (Exception e) {
					 e.printStackTrace();
				 }
		
	}
	
	private void updateDocs(){
		try{
			
			String namaDok = "";
			for(LampiranDto l:listDocsSelected){
				UpdateDocInfoType param = new UpdateDocInfoType();
				param.setDDocName(l.getIdContent());
				param.setDID(l.getDid());
				param.setDRevLabel("1");
				
				//costume meta data
				PropertyType property[] = new PropertyType[1];
				PropertyType p = new PropertyType();
				p.setName("xComments");//hardcode
				String[] x = l.getCompleteXcomments().split("-");
				String xx;
				if(x.length==2){
					if(x[x.length-1].equalsIgnoreCase("Y")||x[x.length-1].equalsIgnoreCase("N")){
						x[x.length-1]=x[x.length-1].replace("N", "Y");
					}
					xx = x[0]+"-"+x[1];
				}else{
					xx= x[0];
				}
				p.setValue(xx);
				property[0] = p;
				
				
				param.setCustomDocMetadata(property);
				System.out.println("====================");
				System.out.println(JsonUtil.getJson(param));
				UpdateDocInfoResultType result = new UpdateDocInfoResultType();
				
				TestUpdateDocumentInfo  thisClass= new TestUpdateDocumentInfo();
				try {
					result = thisClass.hitservice(param);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				
				namaDok = l.getNamaFile()+" ";
				System.out.println("status Info: " + result.getStatusInfo().getStatusCode());
				System.out.println("status Message: " + result.getStatusInfo().getStatusMessage());
			}
			Messagebox.show("Dokumen "+namaDok+" berhasil disimpan");
		}catch(Exception s){
			Messagebox.show("Tidak dapat terhubung dengan server CMS,\n informasi dokumen tidak akan tersimpan");
			s.printStackTrace();
		}
		
		
	}
	
	@Command("checkAll")
	public void checkAllDocs(@ContextParam(ContextType.TRIGGER_EVENT) CheckEvent e){
		if(e.isChecked()){
			listDocsSelected.addAll(listDocs);
		}else{
			listDocsSelected.clear();
		}
		
		BindUtils.postNotifyChange(null, null, this, "listDocsSelected");
	}
	
	@NotifyChange({"pengajuan"})
	@Init
	public void baseInit(
			@ExecutionArgParam("detailHandler") String detailHandler,
			@ExecutionArgParam("dto") PlPengajuanSantunanDto dto,
			@ExecutionArgParam("obj2") UserTask ut,
			@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		this.detailHandler = detailHandler;
		this.pengajuan = dto;
		BindUtils.postNotifyChange(null, null, this, "pengajuan");
		hitBpm();
		this.userTask = ut;
		//System.out.println("USERRRRRRRR TASKKKKKKKKKKKK : " + userTask.getTaskTitle());
		System.out.println("Base init called");
//		System.out.println("==============");
//		System.out.println(JsonUtil.getJson(pengajuan));
		getSantunan();
		getListPengajuanForBaseCalculation();
		getDisposisi();
	}
	
	private void showMsg(String msg, int...i){
		if(i.length>0){
			if(i[0] == 1){
				Messagebox.show("Input " + msg + " Salah");
			}
		}else{
			Messagebox.show("Besar " + msg + " Tidak Boleh Lebih Dari Pengajuan Awal!!");
		}
		
	}
	
	@NotifyChange("formMaster")
	@Command
	public void calculate(@BindingParam("item")Long value){
		System.out.println(value);
		BigDecimal md = (BigDecimal) formMaster.getField("jumlahPengajuanMeninggal");
		if(md.compareTo(pengajuan2.getJumlahPengajuanMeninggal())>0){
			showMsg("MD");
			formMaster.setField("jumlahPengajuanMeninggal", pengajuan2.getJumlahPengajuanMeninggal());
			return;
		}
		
		BigDecimal ll = (BigDecimal) formMaster.getField("jumlahPengajuanLukaluka");
		if(ll.compareTo(pengajuan2.getJumlahPengajuanLukaluka())>0){
			showMsg("LL");
			formMaster.setField("jumlahPengajuanLukaluka", pengajuan2.getJumlahPengajuanLukaluka());
			return;
		}
		
		BigDecimal ambl = (BigDecimal) formMaster.getField("jmlPengajuanAmbl");
		if(ambl.compareTo(pengajuan2.getJmlByrAmblPenyelesaian())>0){
			showMsg("Ambulan");
			formMaster.setField("jmlPengajuanAmbl", pengajuan2.getJmlPengajuanAmbl());
			return;
		}
		
		BigDecimal p3k = (BigDecimal) formMaster.getField("jmlPengajuanP3k");
		if(p3k.compareTo(pengajuan2.getJmlPengajuanP3k())>0){
			showMsg("P3K");
			formMaster.setField("jmlPengajuanAmbl", pengajuan2.getJmlPengajuanAmbl());
			return;
		}
		
		formMaster.setField("totalPembayaran", md.add(ll.add(ambl.add(p3k))));
		totalBayar = md.add(ll.add(ambl.add(p3k)));
		BindUtils.postNotifyChange(null, null, this, "totalBayar");
		BindUtils.postNotifyChange(null, null, formMaster, "*");
	}
	
	protected void returnValue(Map<String, Object> args) {
		//invoke global method in parent view model and also send return value
		BindUtils.postGlobalCommand(null, null, getDetailHandler(), args);
		close();
	}
	
	private void getDocumentTypes(){
		RestResponse rest = new RestResponse();
		try{
			rest = callWs(WS_URI_LOV+"/getListJenisDokumen", new HashMap<String, Object>(), HttpMethod.POST);
			listDocsType = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
//			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listDocs");
		}catch(Exception e){
			listDocs = new ArrayList<>();
			e.printStackTrace();
			BindUtils.postNotifyChange(null, null, this, "listDocs");
		}
	}
	
	private void getSantunan(){
		Map<String, Object> input = new HashMap<>();
		input.put("option", "getPegajuanByNoBerkas");
		input.put("noBerkas", pengajuan.getNoBerkas());
		List<PlPengajuanSantunanDto> listPeng = new ArrayList<>();
		RestResponse rest = new RestResponse();
		try{
			/* for logging puspose do not delete this line */System.out.println(new Date()+" : Trying to get pengajuan from absah by no berkas");
			rest = callWs(WS_URI+"/general", input, HttpMethod.POST);
			Map<String, Object> out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			listPeng = (List<PlPengajuanSantunanDto>) out.get("PlPengajuanSantunanDtos");
			System.out.println("size list Pengajuan Santunan Dto : "+listPeng.size());
			if(listPeng.size()>0){
				pengajuan2 = JsonUtil.mapJsonToSingleObject(listPeng.get(0), PlPengajuanSantunanDto.class);
			}
		}catch(Exception s){
			pengajuan2 = new PlPengajuanSantunanDto();
			s.printStackTrace();
		}
		
		BindUtils.postNotifyChange(null, null, this, "pengajuan2");
	}
	
	@NotifyChange({"akumAmbulance","akumCacatTetap","akumLukaLuka","akumMeninggal","akumP3k","akumPenguburan"})
	private void getListPengajuanForBaseCalculation(){
		if(pengajuan2 == null){
			System.out.println(new Date()+"Pengajuan2 did not have any content or it was null");
			return;
		}
		
		RestResponse rest = new RestResponse();
		Map<String, Object> input = new HashMap<>();
		input.put("option", "getPengajuanByIdKec");
		input.put("idKec", pengajuan2.getIdKecelakaan());
		try{
			System.out.println("Trying to get pengajuan by Id Kecelakaan");
			rest = callWs(WS_URI+"/general", input, HttpMethod.POST);
			Map<String, Object> out = new HashMap<>();
			out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			List<PlPengajuanSantunanDto> listDto = JsonUtil.mapJsonToListObject(out.get("PlPengajuanSantunanDtos"), PlPengajuanSantunanDto.class);
			akumAmbulance = new BigDecimal(0);
			akumCacatTetap  = new BigDecimal(0);
			akumLukaLuka = new BigDecimal(0);
			akumMeninggal = new BigDecimal(0);
			akumP3k = new BigDecimal(0);
			akumPenguburan = new BigDecimal(0);
			for(PlPengajuanSantunanDto p : listDto){
				akumLukaLuka = akumLukaLuka.add(p.getJumlahPengajuanMeninggal());
				akumMeninggal = akumMeninggal.add(p.getJumlahPengajuanLukaluka());
				akumAmbulance = akumAmbulance.add(p.getJmlPengajuanAmbl());
				akumCacatTetap = akumCacatTetap.add(p.getJumlahPengajuanLukaluka());
				akumP3k = akumP3k.add(p.getJmlPengajuanP3k());
				akumPenguburan = akumPenguburan.add(p.getJumlahPengajuanPenguburan());
			}
			
			System.out.println("ambulance \t cacattetap \t lukaluka \t meninggal \t p3k \t penguburan");
			System.out.println(akumAmbulance +"\t"+ akumCacatTetap +"\t"+akumLukaLuka+"\t"+akumMeninggal+"\t"+akumPenguburan);
			
			
		}catch(Exception s){
			s.printStackTrace();
		}
	}
	
	
	
	private UpdateDocInfoResultType updateDoc(UpdateDocInfoType parameters) throws RemoteException{
		Bpm_ecms_update_doc_info_psProxy svc = new Bpm_ecms_update_doc_info_psProxy();
		Bpm_ecms_update_doc_info_ps_PortType port = svc.getBpm_ecms_update_doc_info_ps_PortType();
		return port.update_doc_info(parameters);
	}
	
	@Command
	private void hitBpm(){
		System.out.println(JsonUtil.getJson(pengajuan));
//		cmsCall();
		if(pengajuan!=null){
			cmsCall();
		}else{
			System.out.println(new Date()+" object pengajuan santunan dto is null");
		}
	}
	
	private void cmsCall(){
		DirectoryName dirName = new DirectoryName();
		DirectoryName dirName2 = new DirectoryName();
		dirName.setNoBerkas(pengajuan.getIdKorbanKecelakaan());
		dirName2.setNoBerkas(pengajuan.getNoBerkas());
		
		Response res = new Response();
		Response res2 = new Response();
		try {
			res = hitserviesBpmEcmsByNoBerkas(dirName);
			res2 = hitserviesBpmEcmsByNoBerkas(dirName2);
			
		} catch (RemoteException e) {
			System.out.println(new Date()+": Connection to BPM Server can not established ");
			BindUtils.postNotifyChange(null, null, this, "lampirans");
			e.printStackTrace();
			return;
		} catch (Exception s){
			System.out.println("General Failure in calling BPM Server, ");
			s.printStackTrace();
			BindUtils.postNotifyChange(null, null, this, "lampirans");
			return;
		}
		
		//TODO: getLampiran
		if(res.getResponse().getContent() != null){ 
			System.out.println("Response Content : ");
			System.out.println(JsonUtil.getJson(res.getResponse().getContent()));
			List<LampiranDto> lampirans = new ArrayList<>();
			
			if(res2.getResponse().getContent() != null){
				System.out.println("Response 2 Content : ");
				System.out.println(JsonUtil.getJson(res2.getResponse().getContent()));
				for(ContentResult contentResult : res2.getResponse().getContent()){
					LampiranDto lampiran = new LampiranDto();
					lampiran.setNamaFile(contentResult.getDOriginalName());
					lampiran.setUrl(contentResult.getDWebURL());
					lampiran.setCompleteXcomments(contentResult.getXComments());
					lampiran.setDid(contentResult.getDID());
					String[] coms = contentResult.getXComments().split("-");
					
					if(coms.length==2){
						lampiran.setDeskripsi(coms[0]);
						lampiran.setAbsah(coms[1]);
					}else if(coms.length==1){
						lampiran.setDeskripsi(coms[0]);
						lampiran.setAbsah("N");
					}else{
						lampiran.setDeskripsi(coms[0]+" - "+coms[1]);
						lampiran.setAbsah(coms[2]);
					}
					lampiran.setIdContent(contentResult.getDDocName()); 
					Date date;
					try {
						date = new SimpleDateFormat("yyyy-MM-dd").parse(contentResult.getDDocName().substring(0, 10));
						lampiran.setTglUpload(date);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					lampirans.add(lampiran);
				}
			}
			
			for(ContentResult contentResult : res.getResponse().getContent()){
				LampiranDto lampiran = new LampiranDto();
				lampiran.setNamaFile(contentResult.getDOriginalName());
				lampiran.setUrl(contentResult.getDWebURL());
				lampiran.setCompleteXcomments(contentResult.getXComments());
				lampiran.setDid(contentResult.getDID());
				String[] coms = contentResult.getXComments().split("-");
				
				if(coms.length==2){
					lampiran.setDeskripsi(coms[0]);
					lampiran.setAbsah(coms[1]);
				}else if(coms.length==1){
					lampiran.setDeskripsi(coms[0]);
					lampiran.setAbsah("N");
				}else{
					lampiran.setDeskripsi(coms[0]+" - "+coms[1]);
					lampiran.setAbsah(coms[2]);
				}
				lampiran.setIdContent(contentResult.getDDocName()); 
				Date date;
				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(contentResult.getDDocName().substring(0, 10));
					lampiran.setTglUpload(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				lampirans.add(lampiran);
			}
			listDocs = new ArrayList<>();
			listDocs.addAll(lampirans);
//			System.out.println("+++++++++++++++++++++++++++++++++");
//			System.out.println("ISI LAMPIRAN DTO : ");
//			System.out.println(JsonUtil.getJson(lampirans));
			BindUtils.postNotifyChange(null, null, this, "listDocs");
		}
	}
	
	@GlobalCommand("getDocsAbsah")
	private void getDocsAbsah(@BindingParam("dto") PlPengajuanSantunanDto pengajuanDto){
		DirectoryName dirName = new DirectoryName();
		dirName.setNoBerkas(pengajuanDto.getIdKorbanKecelakaan());
		
		Response res = new Response();
		try {
			res = hitserviesBpmEcmsByNoBerkas(dirName);
		} catch (RemoteException e) {
			BindUtils.postNotifyChange(null, null, this, "lampirans");
			e.printStackTrace();
			return;
		}
		
		
		
		if(res.getResponse().getContent() != null){
			List<LampiranDto> lampirans = new ArrayList<>();
			for(ContentResult contentResult : res.getResponse().getContent()){
				LampiranDto lampiran = new LampiranDto();
				lampiran.setNamaFile(contentResult.getDOriginalName());
				lampiran.setUrl(contentResult.getDWebURL());
				String[] coms = contentResult.getXComments().split("-");
				lampiran.setDeskripsi(coms[0]+" - "+coms[1]);
				lampiran.setAbsah(coms[2]);
				lampiran.setIdContent(contentResult.getDDocName()); 
				Date date;
				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(contentResult.getDDocName().substring(0, 10));
					lampiran.setTglUpload(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				lampirans.add(lampiran);
				System.out.println("===========ooo0000000000ooo=============");
				System.out.println(JsonUtil.getJson(contentResult));
			}
			listDocs = new ArrayList<>();
			listDocs.addAll(lampirans);
			BindUtils.postNotifyChange(null, null, this, "listDocs");
		}
	}
	

	public List<LampiranDto> getListDocs() {
		return listDocs;
	}

	public void setListDocs(List<LampiranDto> listDocs) {
		this.listDocs = listDocs;
	}

	public List<DasiJrRefCodeDto> getListDocsType() {
		return listDocsType;
	}

	public void setListDocsType(List<DasiJrRefCodeDto> listDocsType) {
		this.listDocsType = listDocsType;
	}

	public List<PlPengajuanSantunanDto> getListPengajuan() {
		return listPengajuan;
	}

	public void setListPengajuan(List<PlPengajuanSantunanDto> listPengajuan) {
		this.listPengajuan = listPengajuan;
	}

	public LampiranDto getSelectedDocs() {
		return selectedDocs;
	}

	public void setSelectedDocs(LampiranDto selectedDocs) {
		this.selectedDocs = selectedDocs;
	}

	public DasiJrRefCodeDto getSelectedDocsType() {
		return selectedDocsType;
	}

	public void setSelectedDocsType(DasiJrRefCodeDto selectedDocsType) {
		this.selectedDocsType = selectedDocsType;
	}

	public PlPengajuanSantunanDto getPengajuan() {
		return pengajuan;
	}

	public void setPengajuan(PlPengajuanSantunanDto pengajuan) {
		this.pengajuan = pengajuan;
	}

	public DataBpjsDto getDataBpjs() {
		return dataBpjs;
	}

	public void setDataBpjs(DataBpjsDto dataBpjs) {
		this.dataBpjs = dataBpjs;
	}

	public List<DataBpjsDto> getListDataBpjs() {
		return listDataBpjs;
	}

	public void setListDataBpjs(List<DataBpjsDto> listDataBpjs) {
		this.listDataBpjs = listDataBpjs;
	}

	public String getDetailHandler() {
		return detailHandler;
	}

	public void setDetailHandler(String detailHandler) {
		this.detailHandler = detailHandler;
	}

	public PlPengajuanSantunanDto getPengajuan2() {
		return pengajuan2;
	}

	public void setPengajuan2(PlPengajuanSantunanDto pengajuan2) {
		this.pengajuan2 = pengajuan2;
	}

	public Form getFormMaster() {
		return formMaster;
	}

	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}

	public BigDecimal getTotalBayar() {
		return totalBayar;
	}

	public void setTotalBayar(BigDecimal totalBayar) {
		this.totalBayar = totalBayar;
	}

	public BigDecimal getAkumMeninggal() {
		return akumMeninggal;
	}

	public void setAkumMeninggal(BigDecimal akumMeninggal) {
		this.akumMeninggal = akumMeninggal;
	}

	public BigDecimal getAkumLukaLuka() {
		return akumLukaLuka;
	}

	public void setAkumLukaLuka(BigDecimal akumLukaLuka) {
		this.akumLukaLuka = akumLukaLuka;
	}

	public BigDecimal getAkumCacatTetap() {
		return akumCacatTetap;
	}

	public void setAkumCacatTetap(BigDecimal akumCacatTetap) {
		this.akumCacatTetap = akumCacatTetap;
	}

	public BigDecimal getAkumPenguburan() {
		return akumPenguburan;
	}

	public void setAkumPenguburan(BigDecimal akumPenguburan) {
		this.akumPenguburan = akumPenguburan;
	}

	public BigDecimal getAkumAmbulance() {
		return akumAmbulance;
	}

	public void setAkumAmbulance(BigDecimal akumAmbulance) {
		this.akumAmbulance = akumAmbulance;
	}

	public BigDecimal getAkumP3k() {
		return akumP3k;
	}

	public void setAkumP3k(BigDecimal akumP3k) {
		this.akumP3k = akumP3k;
	}

	public BigDecimal getTotMeninggal() {
		return totMeninggal;
	}

	public void setTotMeninggal(BigDecimal totMeninggal) {
		this.totMeninggal = totMeninggal;
	}

	public BigDecimal getTotLukaLuka() {
		return totLukaLuka;
	}

	public void setTotLukaLuka(BigDecimal totLukaLuka) {
		this.totLukaLuka = totLukaLuka;
	}

	public BigDecimal getTotCacatTetap() {
		return totCacatTetap;
	}

	public void setTotCacatTetap(BigDecimal totCacatTetap) {
		this.totCacatTetap = totCacatTetap;
	}

	public BigDecimal getTotPenguburan() {
		return totPenguburan;
	}

	public void setTotPenguburan(BigDecimal totPenguburan) {
		this.totPenguburan = totPenguburan;
	}

	public BigDecimal getTotAmbulance() {
		return totAmbulance;
	}

	public void setTotAmbulance(BigDecimal totAmbulance) {
		this.totAmbulance = totAmbulance;
	}

	public BigDecimal getTotP3k() {
		return totP3k;
	}

	public void setTotP3k(BigDecimal totP3k) {
		this.totP3k = totP3k;
	}

	public List<LampiranDto> getListDocsSelected() {
		return listDocsSelected;
	}

	public void setListDocsSelected(List<LampiranDto> listDocsSelected) {
		this.listDocsSelected = listDocsSelected;
	}
	
	
}
