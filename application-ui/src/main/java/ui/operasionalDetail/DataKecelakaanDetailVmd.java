package ui.operasionalDetail;

import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;

import Bpm.StartProcess.Bpm_start_processProxy;
import Bpm.StartProcess.Bpm_start_processRequest;
import Bpm.StartProcess.Bpm_start_processResponse;
import Bpm.StartProcess.Bpm_start_process_PortType;
import share.DasiJrRefCodeDto;
import share.FndCamatDto;
import share.FndKantorJasaraharjaDto;
import share.FndLokasiDto;
import share.PlAngkutanKecelakaanDto;
import share.PlDataKecelakaanDto;
import share.PlInstansiDto;
import share.PlJaminanDto;
import share.PlKorbanKecelakaanDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;


@Init(superclass = true)
public class DataKecelakaanDetailVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpDataKecelakaan/_index.zul";


	private final String WS_URI = "/OpDataKecelakaan";
	private final String WS_URI_LOV = "/Lov";
		
	private FndKantorJasaraharjaDto asalBerkasDto = new FndKantorJasaraharjaDto();
	private FndKantorJasaraharjaDto samsatDto = new FndKantorJasaraharjaDto();
	private PlInstansiDto instansiDto = new PlInstansiDto();
	private DasiJrRefCodeDto instansiPembuatDto = new DasiJrRefCodeDto();
	private PlJaminanDto lingkupJaminanDto = new PlJaminanDto();
	private PlJaminanDto jenisPertanggunganDto = new PlJaminanDto();
	private DasiJrRefCodeDto sifatCideraDto = new DasiJrRefCodeDto();
	private DasiJrRefCodeDto sifatKecelakaanDto = new DasiJrRefCodeDto();
	private DasiJrRefCodeDto kasusKecelakaanDto = new DasiJrRefCodeDto();
	private FndLokasiDto lokasiDto = new FndLokasiDto();
	private FndCamatDto provinsiDto = new FndCamatDto();
	private FndCamatDto kabKotaDto = new FndCamatDto();
	private FndCamatDto camatDto = new FndCamatDto();

	private DasiJrRefCodeDto statusKendaraanDto = new DasiJrRefCodeDto();

	
	private List<FndKantorJasaraharjaDto> listAsalBerkasDto = new ArrayList<>();
	private List<FndKantorJasaraharjaDto> listSamsatDto = new ArrayList<>();
	private List<PlInstansiDto> listInstansiDto = new ArrayList<>(); 
	private List<DasiJrRefCodeDto> listInstansiPembuatDto = new ArrayList<>(); 
	private List<PlJaminanDto> listLingkupJaminanDto = new ArrayList<>();
	private List<PlJaminanDto> listJenisPertanggunganDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listSifatCideraDto = new ArrayList<>(); 	
	private List<DasiJrRefCodeDto> listSifatKecelakaanDto = new ArrayList<>(); 	
	private List<FndLokasiDto> listLokasiDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listKasusKecelakaanDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listStatusKendaraanDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listJenisSimDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listMerkKendaraanDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listIdentitasDto = new ArrayList<>(); 	
	private List<DasiJrRefCodeDto> listJenisPekerjaanDto = new ArrayList<>(); 	
	private List<DasiJrRefCodeDto> listStatusKorbanDto = new ArrayList<>(); 	
	private List<DasiJrRefCodeDto> listStatusNikahDto = new ArrayList<>(); 	
	private List<FndCamatDto> listProvinsiDto = new ArrayList<>();
	private List<FndCamatDto> listKabkotaDto = new ArrayList<>();
	private List<FndCamatDto> listCamatDto = new ArrayList<>();
	
	private PlDataKecelakaanDto plDataKecelakaanDto = new PlDataKecelakaanDto();
	private PlAngkutanKecelakaanDto plAngkutanKecelakaanDto = new PlAngkutanKecelakaanDto();
	private PlKorbanKecelakaanDto plKorbanKecelakaanDto = new PlKorbanKecelakaanDto();
	
	private Date tglKejadian;
	private Date jamKejadian;
	
	private boolean formTambahKendaraan;
	private boolean listKendaraan;
	
	private boolean formTambahKorban;
	private boolean listKorban;
	
	private boolean statusLaporanGroupBox = false;
	
	private String statusLaporan;
	
	private String searchCamat;
	private String searchProvinsi;
	
	//tab data kecelakaan
	private String searchAsalBerkas;


	@GlobalCommand("edit")
	public void edit(@BindingParam("name")PlDataKecelakaanDto dto){
		
		Messagebox.show("test get item "+dto.getIdKecelakaan());
		
		RestResponse rest2 = callWs(WS_URI+"/dataLakaById/"+dto.getIdKecelakaan(), null, HttpMethod.POST);
		System.out.println("test "+dto.getIdKecelakaan());
		Messagebox.show("test 1"+dto.getIdKecelakaan());
		try{
			Messagebox.show("test 2");
			PlDataKecelakaanDto dto2 = new PlDataKecelakaanDto();
			dto2 = JsonUtil.mapJsonToSingleObject(rest2.getContents(), PlDataKecelakaanDto.class);
			
			setSearchAsalBerkas(dto2.getAsalBerkas());
			Messagebox.show("test "+plDataKecelakaanDto.getAsalBerkas());
			BindUtils.postNotifyChange(null, null, this, "searchAsalBerkas");
			BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");

			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	protected void loadList(){
		showDataLaka();
	}
	
	@Command
	public void showDataLaka(){
		tabDataLaka();
	}
	
	@Command
	public void showKendaraanTerlibat(){
		setListKendaraan(true);
		setFormTambahKendaraan(false);

		BindUtils.postNotifyChange(null, null, this, "formTambahKendaraan");
		BindUtils.postNotifyChange(null, null, this, "listKendaraan");
	}
	
	@Command
	public void addKendaraanTerlibat(){
		setListKendaraan(false);
		setFormTambahKendaraan(true);
		tabKendaraanTerlibat();		
		BindUtils.postNotifyChange(null, null, this, "listKendaraan");
		BindUtils.postNotifyChange(null, null, this, "formTambahKendaraan");
	}
	
	@Command
	public void showKorban(){
		setListKorban(true);
		setFormTambahKorban(false);
		
		BindUtils.postNotifyChange(null, null, this, "listKorban");
		BindUtils.postNotifyChange(null, null, this, "formTambahKorban");
	}
	
	@Command	
	public void addKorban(){
		setListKorban(false);
		setFormTambahKorban(true);
		tabKorban();
		BindUtils.postNotifyChange(null, null, this, "listKorban");
		BindUtils.postNotifyChange(null, null, this, "formTambahKorban");
	}
	
	@Command
	public void back(){
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);

//		setListIndex(true);
//		BindUtils.postNotifyChange(null, null, this, "listIndex");

	}
	

	
	
	public void tabDataLaka(){
		RestResponse asalBerkasRest = callWs(WS_URI_LOV+"/getAsalBerkas", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listSamsatRest = callWs(WS_URI_LOV+"/getListSamsat", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listLokasiRest = callWs(WS_URI_LOV+"/getListLokasi", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listInstansiRest = callWs(WS_URI_LOV+"/getListInstansi", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listInstansiPembuatRest = callWs(WS_URI_LOV+"/getListInstansiPembuat", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listLingkupJaminanRest = callWs(WS_URI_LOV+"/getListLingkupJaminan", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listJenisPertanggunganRest = callWs(WS_URI_LOV+"/getListJenisPertanggungan", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listSifatCideraRest = callWs(WS_URI_LOV+"/getListSifatCidera", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listSifatKecelakaanRest = callWs(WS_URI_LOV+"/getListSifatKecelakaan", new HashMap<String, Object>(), HttpMethod.POST);

		try{
			listAsalBerkasDto = JsonUtil.mapJsonToListObject(asalBerkasRest.getContents(),FndKantorJasaraharjaDto.class);
			listSamsatDto = JsonUtil.mapJsonToListObject(listSamsatRest.getContents(),FndKantorJasaraharjaDto.class);
			listInstansiDto = JsonUtil.mapJsonToListObject(listInstansiRest.getContents(),PlInstansiDto.class);
			listLokasiDto = JsonUtil.mapJsonToListObject(listLokasiRest.getContents(),FndLokasiDto.class);
			listInstansiPembuatDto = JsonUtil.mapJsonToListObject(listInstansiPembuatRest.getContents(),DasiJrRefCodeDto.class);
			listLingkupJaminanDto = JsonUtil.mapJsonToListObject(listLingkupJaminanRest.getContents(),PlJaminanDto.class);
			listJenisPertanggunganDto = JsonUtil.mapJsonToListObject(listJenisPertanggunganRest.getContents(), PlJaminanDto.class);
			listSifatCideraDto = JsonUtil.mapJsonToListObject(listSifatCideraRest.getContents(),DasiJrRefCodeDto.class);
			listSifatKecelakaanDto = JsonUtil.mapJsonToListObject(listSifatKecelakaanRest.getContents(),DasiJrRefCodeDto.class);

			setTotalSize(asalBerkasRest.getTotalRecords());
			setTotalSize(listSamsatRest.getTotalRecords());
			setTotalSize(listInstansiRest.getTotalRecords());
			setTotalSize(listLokasiRest.getTotalRecords());
			setTotalSize(listInstansiPembuatRest.getTotalRecords());
			setTotalSize(listLingkupJaminanRest.getTotalRecords());
			setTotalSize(listJenisPertanggunganRest.getTotalRecords());
			setTotalSize(listSifatCideraRest.getTotalRecords());
			setTotalSize(listSifatKecelakaanRest.getTotalRecords());

			BindUtils.postNotifyChange(null, null, this, "listLokasiDto");
			BindUtils.postNotifyChange(null, null, this, "listAsalBerkasDto");
			BindUtils.postNotifyChange(null, null, this, "listSamsatDto");
			BindUtils.postNotifyChange(null, null, this, "listInstansiDto");
			BindUtils.postNotifyChange(null, null, this, "listInstansiPembuatDto");
			BindUtils.postNotifyChange(null, null, this, "listLingkupJaminanDto");
			BindUtils.postNotifyChange(null, null, this, "listJenisPertanggunganDto");
			BindUtils.postNotifyChange(null, null, this, "listSifatCideraDto");
			BindUtils.postNotifyChange(null, null, this, "listSifatKecelakaanDto");

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void tabKendaraanTerlibat(){
		RestResponse listKasusKecelakaanRest = callWs(WS_URI_LOV+"/getListKasusKecelakaan", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listStatusKendaraanRest = callWs(WS_URI_LOV+"/getListStatusKendaraan", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listJenisSimRest = callWs(WS_URI_LOV+"/getListJenisSim", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listMerkKendaraanRest = callWs(WS_URI_LOV+"/getListMerkKendaraan", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listProvinsiRest = callWs(WS_URI_LOV+"/getListProvinsi", new HashMap<String, Object>(), HttpMethod.POST);

		try{
			listKasusKecelakaanDto = JsonUtil.mapJsonToListObject(listKasusKecelakaanRest.getContents(),DasiJrRefCodeDto.class);
			listStatusKendaraanDto = JsonUtil.mapJsonToListObject(listStatusKendaraanRest.getContents(),DasiJrRefCodeDto.class);
			listJenisSimDto = JsonUtil.mapJsonToListObject(listJenisSimRest.getContents(),DasiJrRefCodeDto.class);
			listMerkKendaraanDto = JsonUtil.mapJsonToListObject(listMerkKendaraanRest.getContents(),DasiJrRefCodeDto.class);
			listProvinsiDto = JsonUtil.mapJsonToListObject(listProvinsiRest.getContents(),FndCamatDto.class);

			setTotalSize(listKasusKecelakaanRest.getTotalRecords());
			setTotalSize(listStatusKendaraanRest.getTotalRecords());
			setTotalSize(listJenisSimRest.getTotalRecords());
			setTotalSize(listMerkKendaraanRest.getTotalRecords());
			setTotalSize(listProvinsiRest.getTotalRecords());

			BindUtils.postNotifyChange(null, null, this, "listKasusKecelakaanDto");
			BindUtils.postNotifyChange(null, null, this, "listStatusKendaraanDto");
			BindUtils.postNotifyChange(null, null, this, "listJenisSimDto");
			BindUtils.postNotifyChange(null, null, this, "listMerkKendaraanDto");
			BindUtils.postNotifyChange(null, null, this, "listProvinsiDto");

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void tabKorban(){
		RestResponse listIdentitasRest = callWs(WS_URI_LOV+"/getListIdentitas", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listJenisPekerjaanRest = callWs(WS_URI_LOV+"/getListJenisPekerjaan", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listStatusKorbanRest = callWs(WS_URI_LOV+"/getListStatusKorban", new HashMap<String, Object>(), HttpMethod.POST);
		RestResponse listStatusNikahRest = callWs(WS_URI_LOV+"/getListStatusNikah", new HashMap<String, Object>(), HttpMethod.POST);
		searchProvinsi();
		try {			
			listIdentitasDto = JsonUtil.mapJsonToListObject(listIdentitasRest.getContents(),DasiJrRefCodeDto.class);
			listJenisPekerjaanDto = JsonUtil.mapJsonToListObject(listJenisPekerjaanRest.getContents(),DasiJrRefCodeDto.class);
			listStatusKorbanDto = JsonUtil.mapJsonToListObject(listStatusKorbanRest.getContents(),DasiJrRefCodeDto.class);
			listStatusNikahDto = JsonUtil.mapJsonToListObject(listStatusNikahRest.getContents(),DasiJrRefCodeDto.class);

			setTotalSize(listIdentitasRest.getTotalRecords());
			setTotalSize(listJenisPekerjaanRest.getTotalRecords());
			setTotalSize(listStatusKorbanRest.getTotalRecords());
			setTotalSize(listStatusNikahRest.getTotalRecords());
			
			BindUtils.postNotifyChange(null, null, this, "listIdentitasDto");
			BindUtils.postNotifyChange(null, null, this, "listJenisPekerjaanDto");
			BindUtils.postNotifyChange(null, null, this, "listStatusKorbanDto");
			BindUtils.postNotifyChange(null, null, this, "listStatusNikahDto");


		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	
	
	@Command("test")
	public void test(){
		
//		String test = plDataKecelakaanDto.getKodeLokasi()
		
	}
	
	
	@Command("saveDataLaka")
	public void saveDataLaka(){
		RestResponse restResponse = null;
		BigDecimal.valueOf(1);
		Date a = getTglKejadian();
		Date b = getJamKejadian();
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat time = new SimpleDateFormat("HH:mm");
		String tgl = date.format(a);
		String jam = time.format(b);
		String tglKejadian = tgl+" "+jam;
//		Messagebox.show(tglKejadian);
		Date c=null;
		try {
			c = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(tglKejadian);
//			Messagebox.show("test2 "+c);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		plDataKecelakaanDto.setIdKecelakaan("1");
		plDataKecelakaanDto.setKodeInstansi("1");
		plDataKecelakaanDto.setKodeKantorJr("1");
		plDataKecelakaanDto.setKodeKasusKecelakaan("1");
		plDataKecelakaanDto.setKodeLokasi("1");
		plDataKecelakaanDto.setTglKejadian(new Date());
		plDataKecelakaanDto.setNoUrut(BigDecimal.valueOf(1));
		plDataKecelakaanDto.setStatusTransisi("1");
		plDataKecelakaanDto.setAsalBerkas(plDataKecelakaanDto.getAsalBerkas());
		plDataKecelakaanDto.setCreatedBy("luthfi");
		plDataKecelakaanDto.setCreationDate(new Date());
		plDataKecelakaanDto.setDeskripsiKecelakaan(plDataKecelakaanDto.getDeskripsiKecelakaan());
		plDataKecelakaanDto.setDeskripsiLokasi(plDataKecelakaanDto.getDeskripsiLokasi());
		plDataKecelakaanDto.setIdGuid(plDataKecelakaanDto.getIdGuid());
		plDataKecelakaanDto.setKodeWilayah(plDataKecelakaanDto.getKodeWilayah());
		plDataKecelakaanDto.setNamaPetugas(plDataKecelakaanDto.getNamaPetugas());
		plDataKecelakaanDto.setNoLaporanPolisi(plDataKecelakaanDto.getNoLaporanPolisi());
		plDataKecelakaanDto.setTglLaporanPolisi(plDataKecelakaanDto.getTglLaporanPolisi());
		
		restResponse = callWs(WS_URI+"/saveDataLaka", plDataKecelakaanDto, HttpMethod.POST);
		
		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}
	
	@Command("saveDataKendaraan")
	public void saveDataKendaraan(){
		RestResponse restResponse = null;
		plAngkutanKecelakaanDto.setIdAngkutanKecelakaan(plAngkutanKecelakaanDto.getIdAngkutanKecelakaan());
		plAngkutanKecelakaanDto.setIdKecelakaan(plDataKecelakaanDto.getIdKecelakaan());
		plAngkutanKecelakaanDto.setStatusKendaraan(plAngkutanKecelakaanDto.getStatusKendaraan());
		plAngkutanKecelakaanDto.setAlamatPengemudi(plAngkutanKecelakaanDto.getAlamatPengemudi());
		plAngkutanKecelakaanDto.setCreatedBy("luthfi");
		plAngkutanKecelakaanDto.setCreationDate(new Date());
		plAngkutanKecelakaanDto.setIdGuid(plAngkutanKecelakaanDto.getIdGuid());
		plAngkutanKecelakaanDto.setKode(plAngkutanKecelakaanDto.getKode());
		plAngkutanKecelakaanDto.setKodeGolongan(plAngkutanKecelakaanDto.getKodeGolongan());
		plAngkutanKecelakaanDto.setKodeJenis(plAngkutanKecelakaanDto.getKodeJenis());
		plAngkutanKecelakaanDto.setKodeJenisSim(plAngkutanKecelakaanDto.getKodeJenisSim());
		plAngkutanKecelakaanDto.setKodeMerk(plAngkutanKecelakaanDto.getKodeMerk());
		plAngkutanKecelakaanDto.setKodePo(plAngkutanKecelakaanDto.getKodePo());
		plAngkutanKecelakaanDto.setMasaBerlakuSim(plAngkutanKecelakaanDto.getMasaBerlakuSim());
		plAngkutanKecelakaanDto.setNamaPemilik(plAngkutanKecelakaanDto.getNamaPemilik());
		plAngkutanKecelakaanDto.setNamaPengemudi(plAngkutanKecelakaanDto.getNamaPengemudi());
		plAngkutanKecelakaanDto.setNoPolisi(plAngkutanKecelakaanDto.getNoPolisi());
		plAngkutanKecelakaanDto.setNoSimPengemudi(plAngkutanKecelakaanDto.getNoSimPengemudi());
		plAngkutanKecelakaanDto.setTahunPembuatan(plAngkutanKecelakaanDto.getTahunPembuatan());
		
		restResponse = callWs(WS_URI+"/saveAngkutanKecelakaan", plAngkutanKecelakaanDto, HttpMethod.POST);
		
		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}
	
	@Command("saveDataKorban")
	public void saveDataKorban(){
		RestResponse restResponse = null;
		plKorbanKecelakaanDto.setAlamat(plKorbanKecelakaanDto.getAlamat());
		plKorbanKecelakaanDto.setIdKecelakaan(plDataKecelakaanDto.getIdKecelakaan());
		plKorbanKecelakaanDto.setIdKorbanKecelakaan(plKorbanKecelakaanDto.getIdKorbanKecelakaan());
		plKorbanKecelakaanDto.setKodePekerjaan(plKorbanKecelakaanDto.getKodePekerjaan());
		plKorbanKecelakaanDto.setKodeSifatCidera(plKorbanKecelakaanDto.getKodeSifatCidera());
		plKorbanKecelakaanDto.setKodeStatusKorban(plKorbanKecelakaanDto.getKodeStatusKorban());
		plKorbanKecelakaanDto.setNama(plKorbanKecelakaanDto.getNama());
		plKorbanKecelakaanDto.setCreatedBy("luthfi");
		plKorbanKecelakaanDto.setCreationDate(new Date());
		plKorbanKecelakaanDto.setIdAngkutanKecelakaan(plKorbanKecelakaanDto.getIdAngkutanKecelakaan());
		plKorbanKecelakaanDto.setIdAngkutanPenanggung(plKorbanKecelakaanDto.getIdAngkutanPenanggung());
		plKorbanKecelakaanDto.setIdGuid(plKorbanKecelakaanDto.getIdGuid());
		plKorbanKecelakaanDto.setJenisIdentitas(plKorbanKecelakaanDto.getJenisIdentitas());
		plKorbanKecelakaanDto.setJenisKelamin("");
		plKorbanKecelakaanDto.setKodeJaminan(plKorbanKecelakaanDto.getKodeJaminan());
		plKorbanKecelakaanDto.setNoIdentitas(plKorbanKecelakaanDto.getNoIdentitas());
		plKorbanKecelakaanDto.setNoTelp(plKorbanKecelakaanDto.getNoTelp());
		plKorbanKecelakaanDto.setStatusNikah(plKorbanKecelakaanDto.getStatusNikah());
		plKorbanKecelakaanDto.setUmur(plKorbanKecelakaanDto.getUmur());
				
		restResponse = callWs(WS_URI+"/saveKorbanKecelakaan", plKorbanKecelakaanDto, HttpMethod.POST);
		
		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}
	
	@NotifyChange("searchProvinsi")
	@Command
	public void searchProvinsi(){
		if(getSearchProvinsi()==null||getSearchProvinsi().equalsIgnoreCase("")){
			setSearchProvinsi("%%");
		}else if(getSearchProvinsi().contains("-")){
			setSearchProvinsi(getSearchProvinsi().substring(0,2));
		}
		RestResponse rest = callWs(WS_URI_LOV + "/getListProvinsi/"+getSearchProvinsi(),
				new HashMap<String, Object>(), HttpMethod.POST);
		try {			
			listProvinsiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndCamatDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listProvinsiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@NotifyChange("searchProvinsi")
	@Command
	public void getKabkota(){
		RestResponse listKabkotaRest = callWs(WS_URI_LOV+"/getListKabkota/"+provinsiDto.getKodeProvinsi(), new HashMap<String, Object>(), HttpMethod.POST);	
		try {
			listKabkotaDto = JsonUtil.mapJsonToListObject(listKabkotaRest.getContents(),FndCamatDto.class);
			setTotalSize(listKabkotaRest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listKabkotaDto");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	@NotifyChange({"kabKotaDto","searchCamat"})
	@Command
	public void getCamat(){
		if(getSearchCamat()==null||getSearchCamat().equalsIgnoreCase("")){
			setSearchCamat("%%");
		}
		RestResponse listCamatRest = callWs(WS_URI_LOV+"/getListCamat/"+kabKotaDto.getKodeKabkota()+"%/"+getSearchCamat(), new HashMap<String, Object>(), HttpMethod.POST);	
		try {
			listCamatDto = JsonUtil.mapJsonToListObject(listCamatRest.getContents(),FndCamatDto.class);
			setTotalSize(listCamatRest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listCamatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}


	public List<DasiJrRefCodeDto> getListInstansiPembuatDto() {
		return listInstansiPembuatDto;
	}
	public void setListInstansiPembuatDto(
			List<DasiJrRefCodeDto> listInstansiPembuatDto) {
		this.listInstansiPembuatDto = listInstansiPembuatDto;
	}
	public List<PlJaminanDto> getListLingkupJaminanDto() {
		return listLingkupJaminanDto;
	}
	public void setListLingkupJaminanDto(List<PlJaminanDto> listLingkupJaminanDto) {
		this.listLingkupJaminanDto = listLingkupJaminanDto;
	}
	public List<PlJaminanDto> getListJenisPertanggunganDto() {
		return listJenisPertanggunganDto;
	}
	public void setListJenisPertanggunganDto(
			List<PlJaminanDto> listJenisPertanggunganDto) {
		this.listJenisPertanggunganDto = listJenisPertanggunganDto;
	}
	public List<DasiJrRefCodeDto> getListSifatCideraDto() {
		return listSifatCideraDto;
	}
	public void setListSifatCideraDto(List<DasiJrRefCodeDto> listSifatCideraDto) {
		this.listSifatCideraDto = listSifatCideraDto;
	}
	public List<DasiJrRefCodeDto> getListSifatKecelakaanDto() {
		return listSifatKecelakaanDto;
	}
	public void setListSifatKecelakaanDto(
			List<DasiJrRefCodeDto> listSifatKecelakaanDto) {
		this.listSifatKecelakaanDto = listSifatKecelakaanDto;
	}
	public List<FndLokasiDto> getListLokasiDto() {
		return listLokasiDto;
	}
	public void setListLokasiDto(List<FndLokasiDto> listLokasiDto) {
		this.listLokasiDto = listLokasiDto;
	}

	public List<DasiJrRefCodeDto> getListKasusKecelakaanDto() {
		return listKasusKecelakaanDto;
	}

	public void setListKasusKecelakaanDto(
			List<DasiJrRefCodeDto> listKasusKecelakaanDto) {
		this.listKasusKecelakaanDto = listKasusKecelakaanDto;
	}

	public List<DasiJrRefCodeDto> getListStatusKendaraanDto() {
		return listStatusKendaraanDto;
	}

	public void setListStatusKendaraanDto(
			List<DasiJrRefCodeDto> listStatusKendaraanDto) {
		this.listStatusKendaraanDto = listStatusKendaraanDto;
	}

	public List<DasiJrRefCodeDto> getListJenisSimDto() {
		return listJenisSimDto;
	}

	public void setListJenisSimDto(List<DasiJrRefCodeDto> listJenisSimDto) {
		this.listJenisSimDto = listJenisSimDto;
	}

	public List<DasiJrRefCodeDto> getListMerkKendaraanDto() {
		return listMerkKendaraanDto;
	}

	public void setListMerkKendaraanDto(List<DasiJrRefCodeDto> listMerkKendaraanDto) {
		this.listMerkKendaraanDto = listMerkKendaraanDto;
	}

	public List<DasiJrRefCodeDto> getListIdentitasDto() {
		return listIdentitasDto;
	}

	public void setListIdentitasDto(List<DasiJrRefCodeDto> listIdentitasDto) {
		this.listIdentitasDto = listIdentitasDto;
	}

	public List<DasiJrRefCodeDto> getListJenisPekerjaanDto() {
		return listJenisPekerjaanDto;
	}

	public void setListJenisPekerjaanDto(
			List<DasiJrRefCodeDto> listJenisPekerjaanDto) {
		this.listJenisPekerjaanDto = listJenisPekerjaanDto;
	}

	public List<DasiJrRefCodeDto> getListStatusKorbanDto() {
		return listStatusKorbanDto;
	}

	public void setListStatusKorbanDto(List<DasiJrRefCodeDto> listStatusKorbanDto) {
		this.listStatusKorbanDto = listStatusKorbanDto;
	}

	public List<DasiJrRefCodeDto> getListStatusNikahDto() {
		return listStatusNikahDto;
	}

	public void setListStatusNikahDto(List<DasiJrRefCodeDto> listStatusNikahDto) {
		this.listStatusNikahDto = listStatusNikahDto;
	}


	public PlDataKecelakaanDto getPlDataKecelakaanDto() {
		return plDataKecelakaanDto;
	}


	public void setPlDataKecelakaanDto(PlDataKecelakaanDto plDataKecelakaanDto) {
		this.plDataKecelakaanDto = plDataKecelakaanDto;
	}


	public Date getTglKejadian() {
		return tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public PlAngkutanKecelakaanDto getPlAngkutanKecelakaanDto() {
		return plAngkutanKecelakaanDto;
	}

	public void setPlAngkutanKecelakaanDto(
			PlAngkutanKecelakaanDto plAngkutanKecelakaanDto) {
		this.plAngkutanKecelakaanDto = plAngkutanKecelakaanDto;
	}

	public PlKorbanKecelakaanDto getPlKorbanKecelakaanDto() {
		return plKorbanKecelakaanDto;
	}

	public void setPlKorbanKecelakaanDto(PlKorbanKecelakaanDto plKorbanKecelakaanDto) {
		this.plKorbanKecelakaanDto = plKorbanKecelakaanDto;
	}

	public Date getJamKejadian() {
		return jamKejadian;
	}

	public void setJamKejadian(Date jamKejadian) {
		this.jamKejadian = jamKejadian;
	}



	public boolean isFormTambahKendaraan() {
		return formTambahKendaraan;
	}



	public void setFormTambahKendaraan(boolean formTambahKendaraan) {
		this.formTambahKendaraan = formTambahKendaraan;
	}



	public boolean isListKendaraan() {
		return listKendaraan;
	}



	public void setListKendaraan(boolean listKendaraan) {
		this.listKendaraan = listKendaraan;
	}



	public boolean isFormTambahKorban() {
		return formTambahKorban;
	}



	public void setFormTambahKorban(boolean formTambahKorban) {
		this.formTambahKorban = formTambahKorban;
	}



	public boolean isListKorban() {
		return listKorban;
	}



	public void setListKorban(boolean listKorban) {
		this.listKorban = listKorban;
	}

	public String getStatusLaporan() {
		return statusLaporan;
	}

	public void setStatusLaporan(String statusLaporan) {
		this.statusLaporan = statusLaporan;
	}

	public boolean isStatusLaporanGroupBox() {
		return statusLaporanGroupBox;
	}

	public void setStatusLaporanGroupBox(boolean statusLaporanGroupBox) {
		this.statusLaporanGroupBox = statusLaporanGroupBox;
	}

	public FndKantorJasaraharjaDto getAsalBerkasDto() {
		return asalBerkasDto;
	}

	public void setAsalBerkasDto(FndKantorJasaraharjaDto asalBerkasDto) {
		this.asalBerkasDto = asalBerkasDto;
	}

	public FndKantorJasaraharjaDto getSamsatDto() {
		return samsatDto;
	}

	public void setSamsatDto(FndKantorJasaraharjaDto samsatDto) {
		this.samsatDto = samsatDto;
	}

	public PlInstansiDto getInstansiDto() {
		return instansiDto;
	}

	public void setInstansiDto(PlInstansiDto instansiDto) {
		this.instansiDto = instansiDto;
	}

	public DasiJrRefCodeDto getInstansiPembuatDto() {
		return instansiPembuatDto;
	}

	public void setInstansiPembuatDto(DasiJrRefCodeDto instansiPembuatDto) {
		this.instansiPembuatDto = instansiPembuatDto;
	}

	public PlJaminanDto getLingkupJaminanDto() {
		return lingkupJaminanDto;
	}

	public void setLingkupJaminanDto(PlJaminanDto lingkupJaminanDto) {
		this.lingkupJaminanDto = lingkupJaminanDto;
	}

	public PlJaminanDto getJenisPertanggunganDto() {
		return jenisPertanggunganDto;
	}

	public void setJenisPertanggunganDto(PlJaminanDto jenisPertanggunganDto) {
		this.jenisPertanggunganDto = jenisPertanggunganDto;
	}

	public DasiJrRefCodeDto getSifatCideraDto() {
		return sifatCideraDto;
	}

	public void setSifatCideraDto(DasiJrRefCodeDto sifatCideraDto) {
		this.sifatCideraDto = sifatCideraDto;
	}

	public DasiJrRefCodeDto getSifatKecelakaanDto() {
		return sifatKecelakaanDto;
	}

	public void setSifatKecelakaanDto(DasiJrRefCodeDto sifatKecelakaanDto) {
		this.sifatKecelakaanDto = sifatKecelakaanDto;
	}

	public FndLokasiDto getLokasiDto() {
		return lokasiDto;
	}

	public void setLokasiDto(FndLokasiDto lokasiDto) {
		this.lokasiDto = lokasiDto;
	}

	public List<FndKantorJasaraharjaDto> getListAsalBerkasDto() {
		return listAsalBerkasDto;
	}

	public void setListAsalBerkasDto(List<FndKantorJasaraharjaDto> listAsalBerkasDto) {
		this.listAsalBerkasDto = listAsalBerkasDto;
	}

	public List<FndKantorJasaraharjaDto> getListSamsatDto() {
		return listSamsatDto;
	}

	public void setListSamsatDto(List<FndKantorJasaraharjaDto> listSamsatDto) {
		this.listSamsatDto = listSamsatDto;
	}

	public List<PlInstansiDto> getListInstansiDto() {
		return listInstansiDto;
	}

	public void setListInstansiDto(List<PlInstansiDto> listInstansiDto) {
		this.listInstansiDto = listInstansiDto;
	}

	public FndCamatDto getProvinsiDto() {
		return provinsiDto;
	}

	public void setProvinsiDto(FndCamatDto provinsiDto) {
		this.provinsiDto = provinsiDto;
	}

	public FndCamatDto getKabKotaDto() {
		return kabKotaDto;
	}

	public void setKabKotaDto(FndCamatDto kabKotaDto) {
		this.kabKotaDto = kabKotaDto;
	}

	public FndCamatDto getCamatDto() {
		return camatDto;
	}

	public void setCamatDto(FndCamatDto camatDto) {
		this.camatDto = camatDto;
	}

	public List<FndCamatDto> getListProvinsiDto() {
		return listProvinsiDto;
	}

	public void setListProvinsiDto(List<FndCamatDto> listProvinsiDto) {
		this.listProvinsiDto = listProvinsiDto;
	}

	public List<FndCamatDto> getListKabkotaDto() {
		return listKabkotaDto;
	}

	public void setListKabkotaDto(List<FndCamatDto> listKabkotaDto) {
		this.listKabkotaDto = listKabkotaDto;
	}

	public List<FndCamatDto> getListCamatDto() {
		return listCamatDto;
	}

	public void setListCamatDto(List<FndCamatDto> listCamatDto) {
		this.listCamatDto = listCamatDto;
	}

	public String getSearchCamat() {
		return searchCamat;
	}

	public void setSearchCamat(String searchCamat) {
		this.searchCamat = searchCamat;
	}

	public DasiJrRefCodeDto getKasusKecelakaanDto() {
		return kasusKecelakaanDto;
	}

	public void setKasusKecelakaanDto(DasiJrRefCodeDto kasusKecelakaanDto) {
		this.kasusKecelakaanDto = kasusKecelakaanDto;
	}

	public DasiJrRefCodeDto getStatusKendaraanDto() {
		return statusKendaraanDto;
	}

	public void setStatusKendaraanDto(DasiJrRefCodeDto statusKendaraanDto) {
		this.statusKendaraanDto = statusKendaraanDto;
	}

	public String getSearchAsalBerkas() {
		return searchAsalBerkas;
	}

	public void setSearchAsalBerkas(String searchAsalBerkas) {
		this.searchAsalBerkas = searchAsalBerkas;
	}

	public String getSearchProvinsi() {
		return searchProvinsi;
	}

	public void setSearchProvinsi(String searchProvinsi) {
		this.searchProvinsi = searchProvinsi;
	}
	
	
}
