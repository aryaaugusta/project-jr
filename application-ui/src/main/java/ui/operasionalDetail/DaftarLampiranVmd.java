package ui.operasionalDetail;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.wsdl.Message;

import org.apache.axis.providers.java.MsgProvider;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteProxy;
import Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteRequest;
import Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteResponse;
import Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_PortType;
import Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdlProxy;
import Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdl_PortType;
import Bpm.Ecms.queryByNoBerkas.ContentResult;
import Bpm.Ecms.queryByNoBerkas.DirectoryName;
import Bpm.Ecms.queryByNoBerkas.Response;
import Bpm.Ecms.selectEcms.BpmEcmsSelectSvcImpl;
import Bpm.Ecms.selectEcmsSchema.BpmEcmsMapping;
import Bpm.Ecms.selectEcmsSchema.Bpm_ecms_mapping_querySelect_DIRECTORY_ID;
import Bpm.Ecms.upload.AttachmentResultType;
import Bpm.Ecms.upload.AttachmentsType;
import Bpm.Ecms.upload.Bpm_ecms_upload_wsdlProxy;
import Bpm.Ecms.upload.Bpm_ecms_upload_wsdl_PortType;
import share.DasiJrRefCodeDto;
import share.LampiranDto;
import share.PlBerkasEcmDto;
import share.PlBerkasEcmHDto;
import share.PlBerkasEcmsContainDto;
import share.PlBerkasPengajuanDto;
import share.PlDataKecelakaanDto;
import share.PlPengajuanSantunanDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.PageInfo;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

public class DaftarLampiranVmd extends BaseVmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH + "/operasionalDetail/DaftarLampiranDetail.zul";
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH + "/operasionalDetail/DaftarLampiran.zul";
	private final String DATA_LAKA_PAGE_PATH = UIConstants.BASE_PAGE_PATH + "/operasional/OpDataKecelakaan/_index.zul";
	private final String DATA_SANTUNAN_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpPengajuanSantunan/_index.zul";

	private final String WS_URI_LOV = "/Lov";
	private final String WS_URI = "/OpPengajuanSantunan";
	private final String WS_URI_BERKASECM = "/OpPlBerkasEcm";

	private List<DasiJrRefCodeDto> listJenisDokumenDto = new ArrayList<>();
	private DasiJrRefCodeDto jenisDokumenDto = new DasiJrRefCodeDto();
	private List<DasiJrRefCodeDto> listDokumen = new ArrayList<>();
	private String searchIndex = "";

	private PlDataKecelakaanDto plDataKecelakaanDto = new PlDataKecelakaanDto();
	private List<PlDataKecelakaanDto> listDataLakaDto = new ArrayList<>();
	private PlPengajuanSantunanDto plPengajuanSantunanDto = new PlPengajuanSantunanDto();

	private boolean flagDataLaka = false;
	private boolean flagDataSantunan = false;
	private String origin;
	private boolean multi = false;

	private Form formMaster = new SimpleForm();

	private boolean listLampiranGroupbox = true;
	private boolean tambahLampiranGroupbox = false;

	private String fileName;
	@Wire("#popUpLampiran")
	private Window winLov;

	private String popUpHandler;
	private UserSessionJR ujr = new UserSessionJR();
	private String id;

	private String idKecelakaan;

	private Media media;

	private List<LampiranDto> lampirans;
//	private List<LampiranDto> lampiran2;

	private boolean flagLampiranSantunan = false;
	private boolean ketVisible = false;

	private String deskripsiTambahan;

	@Command
	public void search() {
		for (LampiranDto s : lampirans) {
			if (s.getDeskripsi().contains(searchIndex)) {

			}
		}
	}

	public List<LampiranDto> getLampirans() {
		return lampirans;
	}

	public void setLampirans(List<LampiranDto> lampirans) {
		this.lampirans = lampirans;
	}

	@NotifyChange({ "plDataKecelakaanDto", "plPengajuanSantunanDto", "idKecelakaan" })
	@Init
	public void baseInit(@ExecutionArgParam("popUpHandler") String popUpHandler,
			@ExecutionArgParam("plPengajuanSantunanDto") PlPengajuanSantunanDto selected,
			@ExecutionArgParam("plDataKecelakaanDto") PlDataKecelakaanDto dataLakaDto,
			@ExecutionArgParam("origin") String origin, @ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		this.popUpHandler = popUpHandler;
		this.origin = origin;
		if (selected != null) {
			System.out.println("pengajuan not null");
			plPengajuanSantunanDto = selected;
			getDataLaka();

			PlDataKecelakaanDto selected2 = plDataKecelakaanDto;
			listLampirans(selected2, plPengajuanSantunanDto);
			setKetVisible(true);
			BindUtils.postNotifyChange(null, null, this, "ketVisible");
		}
		if (dataLakaDto != null) {
			System.out.println("data laka not null");
			plDataKecelakaanDto = dataLakaDto;
			// PlPengajuanSantunanDto selected1 = plPengajuanSantunanDto;
			listLampirans(dataLakaDto, selected);
		}
		System.out.println("Base popup init called");
	}

	public void listLampirans(PlDataKecelakaanDto selected, PlPengajuanSantunanDto selected2) {
		lampirans = new ArrayList<>();

		// Messagebox.show("etst" +isFlagDataLaka());
		plDataKecelakaanDto.setNamaKorban(selected.getNamaKorban());

		setPlDataKecelakaanDto(selected);
		if (flagLampiranSantunan) {
			setFlagDataLaka(true);
			setFlagDataSantunan(true);
			BindUtils.postNotifyChange(null, null, this, "flagDataLaka");
			BindUtils.postNotifyChange(null, null, this, "flagDataSantunan");
		}

		BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
		formMaster.setField("namaKorban", selected.getNamaKorban());
		BindUtils.postNotifyChange(null, null, formMaster, "namaKorban");
		// Messagebox.show("yang kedua..." + plDataKecelakaanDto.getIdKecelakaan());

		// getPageInfo().setAddMode(true);
		// getPageInfo().setAddDetailMode(true);
		// Messagebox.show("tes id : " + plDataKecelakaanDto.getIdKecelakaan());

		DirectoryName dirName = new DirectoryName();
		DirectoryName dirName2 = new DirectoryName();
		if (selected2 == null) {
			dirName.setNoBerkas(plDataKecelakaanDto.getIdKorbanKecelakaan());
			System.out.println("NOOOOOOOOOOOOOOOOOOOOOOOO BERKKKKKKAAAAAAASSSSSSSSSSSSSSSSSSSSSSSSSS DATA LAKA : "
					+ plDataKecelakaanDto.getIdKorbanKecelakaan());
		} else {
			dirName.setNoBerkas(selected2.getNoBerkas());
			dirName2.setNoBerkas(plDataKecelakaanDto.getIdKorbanKecelakaan());
			System.out.println(
					"NOOOOOOOOOOOOOOOOOOOOOOOO BERKKKKKKAAAAAAASSSSSSSSSSSSSSSSSSSSSSSSSS PENGAJUAN SANTUNAN : "
							+ selected2.getNoBerkas());
		}
		Response res = new Response();
		Response res2 = new Response();
		try {
			res = hitserviesBpmEcmsByNoBerkas(dirName);
			if (selected2 != null) {
				res2 = hitserviesBpmEcmsByNoBerkas(dirName2);
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
//			Messagebox.show("ecms server disconect");
			BindUtils.postNotifyChange(null, null, this, "lampirans");
			e.printStackTrace();
			return;
		}

		if (res.getResponse().getContent() != null) {
			if (selected2 != null) {
				if (res2.getResponse().getContent() != null) {
					for (ContentResult contentResult : res2.getResponse().getContent()) {
						LampiranDto lampiran = new LampiranDto();
						lampiran.setNamaFile(contentResult.getDOriginalName());
						lampiran.setUrl(contentResult.getDWebURL().replaceAll("~1", ""));
						// String[] coms = contentResult.getXComments().split("-");
						// lampiran.setDeskripsi(coms[0]+" - "+coms[1]);
						lampiran.setDeskripsi(contentResult.getXComments());
						lampiran.setIdContent(contentResult.getDDocName());

						Date date;
						try {
							date = new SimpleDateFormat("yyyy-MM-dd")
									.parse(contentResult.getDDocName().substring(0, 10));
							lampiran.setTglUpload(date);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						lampirans.add(lampiran);
					}
				}
			}

			for (ContentResult contentResult : res.getResponse().getContent()) {
				LampiranDto lampiran = new LampiranDto();
				lampiran.setNamaFile(contentResult.getDOriginalName());
				lampiran.setUrl(contentResult.getDWebURL().replaceAll("~1", ""));
				// String[] coms = contentResult.getXComments().split("-");
				// lampiran.setDeskripsi(coms[0]+" - "+coms[1]);
				lampiran.setDeskripsi(contentResult.getXComments());
				lampiran.setIdContent(contentResult.getDDocName());

				Date date;
				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(contentResult.getDDocName().substring(0, 10));
					lampiran.setTglUpload(date);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				lampirans.add(lampiran);
			}

			for (LampiranDto x : lampirans) {
				System.out.print("NAMA FILE : " + x.getNamaFile() + "/n");
				System.out.println("tgl upload " + x.getTglUpload());
				System.out.println("url: http://prodcontent1.jasaraharja.co.id:16200" + x.getUrl());

			}

			BindUtils.postNotifyChange(null, null, this, "lampirans");

		}
	}

	@Override
	protected void loadList() {
		if (Executions.getCurrent().getAttribute("obj") != null) {
			this.plDataKecelakaanDto = (PlDataKecelakaanDto) Executions.getCurrent().getAttribute("obj");
			addLampiran(plDataKecelakaanDto);
		}
	}

	public void getDataLaka() {
		// ============================GET DATA LAKA====================================
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", plPengajuanSantunanDto.getIdKecelakaan());
		RestResponse rest = callWs(WS_URI + "/findbyId", map, HttpMethod.POST);
		try {
			listDataLakaDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlDataKecelakaanDto.class);
			for (PlDataKecelakaanDto a : listDataLakaDto) {
				plDataKecelakaanDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
				plDataKecelakaanDto.setTglKejadian(a.getTglKejadian());
				plDataKecelakaanDto.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
				plDataKecelakaanDto.setStatus(a.getStatus());
				plDataKecelakaanDto.setKasus(a.getKasus());
				plDataKecelakaanDto.setDeskripsiLokasi(a.getDeskripsiLokasi());
			}
			plDataKecelakaanDto.setNamaKorban(plPengajuanSantunanDto.getNama());
			plDataKecelakaanDto.setIdKecelakaan(plPengajuanSantunanDto.getIdKecelakaan());
			plDataKecelakaanDto.setIdKorbanKecelakaan(plPengajuanSantunanDto.getIdKorbanKecelakaan());
			BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GlobalCommand
	public void addLampiran(@BindingParam("item") PlDataKecelakaanDto selected) {
		this.plDataKecelakaanDto = new PlDataKecelakaanDto();
		this.plDataKecelakaanDto = selected;

		lampirans = new ArrayList<>();

		// Messagebox.show("etst" +isFlagDataLaka());
		plDataKecelakaanDto.setNamaKorban(selected.getNamaKorban());

		setPlDataKecelakaanDto(selected);
		if (flagLampiranSantunan) {
			setFlagDataLaka(true);
			setFlagDataSantunan(true);
			BindUtils.postNotifyChange(null, null, this, "flagDataLaka");
			BindUtils.postNotifyChange(null, null, this, "flagDataSantunan");
		}

		BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
		formMaster.setField("namaKorban", selected.getNamaKorban());
		BindUtils.postNotifyChange(null, null, formMaster, "namaKorban");
		// Messagebox.show("yang kedua..." + plDataKecelakaanDto.getIdKecelakaan());

		// getPageInfo().setAddMode(true);
		// getPageInfo().setAddDetailMode(true);
		// Messagebox.show("tes id : " + plDataKecelakaanDto.getIdKecelakaan());

		DirectoryName dirName = new DirectoryName();
		dirName.setNoBerkas(plDataKecelakaanDto.getIdKorbanKecelakaan());

		Response res = new Response();
		try {
			res = hitserviesBpmEcmsByNoBerkas(dirName);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
//			Messagebox.show("ecms server disconect");
			BindUtils.postNotifyChange(null, null, this, "lampirans");
			e.printStackTrace();
			return;
		}

		if (res.getResponse().getContent() != null) {

			for (ContentResult contentResult : res.getResponse().getContent()) {
				LampiranDto lampiran = new LampiranDto();
				lampiran.setNamaFile(contentResult.getDOriginalName());
				lampiran.setUrl(contentResult.getDWebURL().replaceAll("~1", ""));
				String[] coms = contentResult.getXComments().split("-");
				lampiran.setDeskripsi(coms[0] + " - " + coms[1]);
				lampiran.setIdContent(contentResult.getDDocName());

				Date date;
				try {
					date = new SimpleDateFormat("yyyy-MM-dd").parse(contentResult.getDDocName().substring(0, 10));
					lampiran.setTglUpload(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				lampirans.add(lampiran);
			}

			for (LampiranDto x : lampirans) {
				System.out.print("NAMA FILE : " + x.getNamaFile() + "/n");
				System.out.println("tgl upload " + x.getTglUpload());
				System.out.println("url: http://prodcontent1.jasaraharja.co.id:16200" + x.getUrl());

			}

			BindUtils.postNotifyChange(null, null, this, "lampirans");

		}
	}

	public void onAddDetail() {
		plDataKecelakaanDto = (PlDataKecelakaanDto) Executions.getCurrent().getAttribute("obj");
		setPlDataKecelakaanDto(plDataKecelakaanDto);

		BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
//		Messagebox.show(plDataKecelakaanDto.getIdKecelakaan());
	}

	@Command
	@NotifyChange("deskripsiTambahan")
	public void add() {
		Executions.getCurrent().setAttribute("obj", plDataKecelakaanDto);
		getPageInfo().setAddMode(true);
//		navigate(DETAIL_PAGE_PATH);
		onAdd();
//		setListDokumen(null);
//		setDeskripsiTambahan(null);
		setListLampiranGroupbox(false);
		setTambahLampiranGroupbox(true);
		BindUtils.postNotifyChange(null, null, this, "listLampiranGroupbox");
		BindUtils.postNotifyChange(null, null, this, "tambahLampiranGroupbox");
	}

	public void onAdd() {
		// plDataKecelakaanDto = new PlDataKecelakaanDto();
		plDataKecelakaanDto = (PlDataKecelakaanDto) Executions.getCurrent().getAttribute("obj");
		deskripsiTambahan = new String();
		setFileName(null);

//		for(DasiJrRefCodeDto d : listDokumen){
//			System.out.println("Jenis Dokumen : " + d.getRvMeaning());
//		}

//		plDataKecelakaanDto.setNamaKorban(getPlDataKecelakaanDto().getNamaKorban());
		// Messagebox.show("Muncul Nama Korban :" +plDataKecelakaanDto.getNamaKorban());
//		BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
//		System.out.println("masuk sini ");
		RestResponse rest = callWs(WS_URI_LOV + "/getListJenisDokumen", new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listJenisDokumenDto = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			System.out.println("baca " + listJenisDokumenDto.size());
			setTotalSize(rest.getTotalRecords());

//			for(DasiJrRefCodeDto o : listJenisDokumenDto){
//				System.out.println("Jenis Dokumen : " + o.getRvLowValue());
//			}
			LampiranDto lampiran = new LampiranDto();
			for (LampiranDto l : lampirans) {
				lampiran.setDeskripsi(l.getDeskripsi());
				lampiran.setIdContent(l.getIdContent());

				for (DasiJrRefCodeDto d : listJenisDokumenDto) {
//					System.out.println("luthfi89 "+lampiran.getDeskripsi()+" "+d.getRvMeaning());
					if (lampiran.getDeskripsi().contains(",")) {
						String jnsBerkas[] = lampiran.getDeskripsi().split(",");
						String berkas;
						for (int i = 0; i <= jnsBerkas.length - 1; i++) {
							berkas = jnsBerkas[i];
							if (berkas.trim().equalsIgnoreCase(d.getRvMeaning())) {
								d.setDisabledItem(true);
								listDokumen.add(d);
							}
						}
					} else {
						if (lampiran.getDeskripsi().equalsIgnoreCase(d.getRvMeaning())) {
							d.setDisabledItem(true);
							listDokumen.add(d);
						}
					}
				}

			}

			BindUtils.postNotifyChange(null, null, this, "listDokumen");
			BindUtils.postNotifyChange(null, null, this, "listJenisDokumenDto");
			BindUtils.postNotifyChange(null, null, this, "fileName");

//			for (DasiJrRefCodeDto dasi : listJenisDokumenDto) {
//				// System.out.println("DASI "+JsonUtil.getJson(dasi));
//				if (dasi == null) {
//					return;
//				}
//
//				for (LampiranDto lampiran : lampirans) {
//					// checkDeskripsi = false;
//					if (lampiran.getDeskripsi().contains(dasi.getRvMeaning())) {
//						// System.out.println("test "+lampiran.getDeskripsi()+
//						// "\t" +dasi.getRvMeaning());
//						// setCheckDeskripsi(true);
//						int i = listJenisDokumenDto.indexOf(dasi);
//						dasi.setCheckedJenisDomumen(true);
//						listJenisDokumenDto.set(i, dasi);
//					}
//				}
//			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void back() {
		Executions.getCurrent().setAttribute("obj", plDataKecelakaanDto);
		getPageInfo().setListMode(true);
//		navigate(INDEX_PAGE_PATH);
		setListLampiranGroupbox(true);
		setTambahLampiranGroupbox(false);
		listDokumen.clear();
		BindUtils.postNotifyChange(null, null, this, "listDokumen");
		BindUtils.postNotifyChange(null, null, this, "listLampiranGroupbox");
		BindUtils.postNotifyChange(null, null, this, "tambahLampiranGroupbox");

	}

//	@Command
//	public void close(@BindingParam("window") Window win){
//		//if(isFlagDataLaka()==true){
//			
//			if(isFlagDataSantunan()==true){
//				//Messagebox.show("Tutup Lampiran Santunan ");
//				setFlagDataSantunan(false);
//				BindUtils.postNotifyChange(null, null, this, "flagDataSantunan");
//				navigate(DATA_SANTUNAN_PAGE_PATH);
//			//}
//		}else{
//		//	Messagebox.show("test ");
//			setFlagDataLaka(false);
//			BindUtils.postNotifyChange(null, null, this, "flagDataLaka");
//			navigate(DATA_LAKA_PAGE_PATH);
//			//navigate(DATA_PENGAJUAN_PAGE_PATH);
//			//setFlagLampiranSantunan(false);
//			//BindUtils.postNotifyChange(null, null, this, "flagLampiranSantunan");
//		}
////		if(win!=null)
////		win.detach();		
//	}

	@Command
	public void uploadFile(@ContextParam(ContextType.BIND_CONTEXT) BindContext ctx, @BindingParam("item") String id) {
		try {
			UploadEvent upEvent = (UploadEvent) ctx.getTriggerEvent();
			Media media = upEvent.getMedia();

			System.out.println("uploading " + upEvent.getMedia().getName());
			setFileName(upEvent.getMedia().getName());
			setMedia(media);

			// String noBerkas = new String();
			// noBerkas = plDataKecelakaanDto.getIdKecelakaan();
			// BindUtils.postNotifyChange(null, null, this, "");
			// uploading(noBerkas,media);
			// System.out.println(new String(media.getByteData()));

//			byte[] input_file = Files.readAllBytes(Paths.get(filePath+originalFileName));
//
//	        byte[] encodedBytes = Base64.getEncoder().encode(input_file);
//	        String encodedString =  new String(encodedBytes);
//	        byte[] decodedBytes = Base64.getDecoder().decode(encodedString.getBytes());
//	        System.out.println(encodedString);
//	        FileOutputStream fos = new FileOutputStream(filePath+newFileName);
//	        fos.write(decodedBytes);
//	        fos.flush();
//	        fos.close();

			// convert base64 string to image
//            String imageString = removeBase64Header(base64);
//            String imageType = getImageType(base64);
//            BufferedImage image = decodeToImage(imageString);
//            File imageFile = File.createTempFile("image", "." + imageType);
//            ImageIO.write(image, imageType, imageFile);

			// convert image to base64 string
//            String newImageString = encodeToString(imageFile, imageType);
//            System.out.println("Image Type -> " + imageType);
//            System.out.println(newImageString);
//            System.out.println();
//            System.out.println();

			BindUtils.postNotifyChange(null, null, this, "fileName");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	+============================================================================================

//	==============================================================================================
	@Command
	@NotifyChange({ "jenisDokumenDto", "deskripsiTambahan" })
	public void test(@BindingParam("item") DasiJrRefCodeDto selected) {
//		Messagebox.show(selected.getRvMeaning());
		try {
			deskripsiTambahan = new String();
			this.setJenisDokumenDto(selected);
			for (int i = 0; i < listDokumen.size(); i++) {
				if (listDokumen.size() == (i + 1)) {
					if (listDokumen.get(i).isDisabledItem() == false) {
						deskripsiTambahan += listDokumen.get(i).getRvMeaning();
					}
				} else {
					if (listDokumen.get(i).isDisabledItem() == false) {
						deskripsiTambahan += listDokumen.get(i).getRvMeaning() + ", ";
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	@Command
//	@NotifyChange({"plDataKecelakaanDto","jenisDokumenDto"})
//	public void uploading(){
//		RestResponse restResponse = null;
//		RestResponse restResponse2 = null;
//		RestResponse restResponse3 = null;
//		RestResponse restResponse4 = null;
//		boolean stat = true;
//		System.out.println("AKANNN UPLOADDDD");
//		System.out.println("ORIGINNN NIHHH : " + origin);
//		
//		AttachmentsType attachments[] = new AttachmentsType[1];
//		AttachmentsType attach = new AttachmentsType();
//		String namaFile = plDataKecelakaanDto.getNoLaporanPolisi().replaceAll("/", "-")+"_"+plDataKecelakaanDto.getIdKecelakaan()+ "_"+ deskripsiTambahan + "."+ media.getFormat();
//		attach.setNamaFile(namaFile);
//		attach.setDeskripsi(deskripsiTambahan);
////		if(deskripsiTambahan != null){
////			attach.setDeskripsi(getJenisDokumenDto().getRvMeaning() + "-" + "" + "N");
////		}else{
////			attach.setDeskripsi(getJenisDokumenDto().getRvMeaning() + "-" + deskripsiTambahan + "N");
////		}
//		
//		attach.setAttachments(media.getByteData());
//		attachments[0] = attach;
//		
//		
////		Messagebox.show("result upload : " + attachments[0].getDeskripsi());
//		AttachmentResultType[] result = new AttachmentResultType[1];
//		try {
//			result = hitserviesBpmEcmsUpload(getPlDataKecelakaanDto().getIdKorbanKecelakaan(), attachments);
//			
//			DirectoryName dirName = new DirectoryName();
//			dirName.setNoBerkas(plDataKecelakaanDto.getIdKorbanKecelakaan());
//			
//			Response res = new Response();
//			try {
//				res = hitserviesBpmEcmsByNoBerkas(dirName);
//			} catch (RemoteException e) {
//				// TODO Auto-generated catch block
////				Messagebox.show("ecms server disconect");
//				BindUtils.postNotifyChange(null, null, this, "lampirans");
//				e.printStackTrace();
//				return;
//			}
//			
//			String docName = new String();
//			String filePath = null;
//			
//			if (res.getResponse().getContent() != null) {
//				
//				for (ContentResult contentResult : res.getResponse().getContent()) {
//					if (contentResult.getDOriginalName().equalsIgnoreCase(namaFile)) {
//						docName = contentResult.getDDocName();
//						filePath = contentResult.getDWebURL();
//					}
//				}
//			}
//			 System.out.println("DOKUMEN NAME : " + docName);
//			 System.out.println("FILE PATH : " + filePath);
//			 
//			
//			PlBerkasEcmDto plBerkasEcmDto = new PlBerkasEcmDto();
//			plBerkasEcmDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
//			plBerkasEcmDto.setNoRegEcms(plDataKecelakaanDto.getIdKorbanKecelakaan());
//			
//			Date a = new Date();
//			Date b = new Date();
//			SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
//			SimpleDateFormat time = new SimpleDateFormat("HH:mm");
//
//			String tgl = date.format(a);
//			String jam = time.format(b);
//			String bufferTgl = tgl + " " + jam;
//			Date date1 = null;
//			try {
//				date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm")
//						.parse(bufferTgl);
//			} catch (ParseException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			plBerkasEcmDto.setCreatedDate(date1);
//			plBerkasEcmDto.setDdocname(docName);
//			plBerkasEcmDto.setFilepath(filePath);
//			plBerkasEcmDto.setNamafile(fileName);
//			
//			System.out.println("Ini DDOCNAME: " + docName);
//			
//			BindUtils.postNotifyChange(null, null, this,
//					"plBerkasEcmDto");
//			restResponse = callWs(WS_URI_BERKASECM + "/saveDataPlBerkasEcm", plBerkasEcmDto, HttpMethod.POST);
//			
//			if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
//				//showInfoMsgBox(restResponse.getMessage());
//			} else {
//				stat = false;
//				//showErrorMsgBox(restResponse.getMessage());
//			}
//			
//			for(int i = 0; i < listDokumen.size(); i++){
//				PlBerkasEcmsContainDto plBerkasEcmsContainDto = new PlBerkasEcmsContainDto();
//				plBerkasEcmsContainDto.setNoRegEcms(plDataKecelakaanDto.getIdKorbanKecelakaan());
//				plBerkasEcmsContainDto.setKodeBerkas(listDokumen.get(i).getRvLowValue());
//				plBerkasEcmsContainDto.setdDocname(docName);
//				BindUtils.postNotifyChange(null, null, this,
//						"plBerkasEcmsContainDto");
//				restResponse2 = callWs(WS_URI_BERKASECM + "/saveBerkasEcmsContain", plBerkasEcmsContainDto, HttpMethod.POST);
//				
//				if(origin == null){
//					PlBerkasPengajuanDto plBerkasPengajuanDto = new PlBerkasPengajuanDto();
//					plBerkasPengajuanDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
//					plBerkasPengajuanDto.setKodeBerkas(listDokumen.get(i).getRvLowValue());
//					plBerkasPengajuanDto.setDiterimaFlag("Y");
//					plBerkasPengajuanDto.setAbsahFlag("Y");
//					plBerkasPengajuanDto.setTglTerimaBerkas(date1);
//					plBerkasPengajuanDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
//					plBerkasPengajuanDto.setCreationDate(date1);
//					plBerkasPengajuanDto.setLastUpdatedDate(date1);
//					plBerkasPengajuanDto.setLastUpdatedBy(getCurrentUserSessionJR().getLoginID());
//					plBerkasPengajuanDto.setIdGuid("");
//					BindUtils.postNotifyChange(null, null, this,
//							"plBerkasPengajuanDto");
//					restResponse4 = callWs(WS_URI_BERKASECM + "/saveBerkasPengajuan", plBerkasPengajuanDto, HttpMethod.POST);
//					if (restResponse4.getStatus() == CommonConstants.OK_REST_STATUS) {
//						//showInfoMsgBox(restResponse2.getMessage());
//					} else {
//						stat = false;
//						//showErrorMsgBox(restResponse2.getMessage());
//					}
//				}
//				
//			}
//			
//			if (restResponse2.getStatus() == CommonConstants.OK_REST_STATUS) {
//				//showInfoMsgBox(restResponse2.getMessage());
//			} else {
//				stat = false;
//				//showErrorMsgBox(restResponse2.getMessage());
//			}
//			
//
//			
//			PlBerkasEcmHDto plBerkasEcmHDto = new PlBerkasEcmHDto();
//			plBerkasEcmHDto.setIdKorbanKecelakaan(plDataKecelakaanDto.getIdKorbanKecelakaan());
//			plBerkasEcmHDto.setNoRegEcms(plDataKecelakaanDto.getIdKorbanKecelakaan());
//			BindUtils.postNotifyChange(null, null, this,
//					"plBerkasEcmHDto");
//			restResponse3 = callWs(WS_URI_BERKASECM + "/saveBerkasEcmH", plBerkasEcmHDto, HttpMethod.POST);
//			
//			if (restResponse3.getStatus() == CommonConstants.OK_REST_STATUS) {
//				//showInfoMsgBox(restResponse3.getMessage());
//			} else {
//				stat = false;
//				//showErrorMsgBox(restResponse3.getMessage());
//			}
//			
//			
//			
//			//for message box
//			if (stat == true) {
//				showInfoMsgBox("Data berhasil disimpan");
//			} else {
//				showErrorMsgBox("Data gagal disimpan");
//			}
//			
//		} catch (RemoteException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		//Messagebox.show("ini id kecelakaan / no berkas : " + getPlDataKecelakaanDto().getIdKecelakaan());
//		//Messagebox.show("nama berkas " + attachments[0].getNamaFile());
////		Messagebox.show("deskripsi : " + attachments[0].getDeskripsi());
////		Messagebox.show("result upload : " + result[0].getStatusMessage());
//		listLampirans(plDataKecelakaanDto);
//		setListLampiranGroupbox(true);
//		setTambahLampiranGroupbox(false);
//		BindUtils.postNotifyChange(null, null, this, "listLampiranGroupbox");
//		BindUtils.postNotifyChange(null, null, this, "tambahLampiranGroupbox");
//
////		getPageInfo().setListMode(true);
////		Executions.getCurrent().setAttribute("obj", plDataKecelakaanDto);
////		navigate(INDEX_PAGE_PATH);
//	}

	@Command
	@NotifyChange({ "plDataKecelakaanDto", "jenisDokumenDto" })
	public void uploading() {
		try {
			if (media == null || media.getByteData() == null) {
				showInfoMsgBox("Pilih File!", "");
				return;
			} else if (listDokumen == null || listDokumen.size() == 0) {
				showInfoMsgBox("Pilih Jenis Dokumen!", "");
				return;
			}
			RestResponse restResponse = null;
			RestResponse restResponse2 = null;
			RestResponse restResponse3 = null;
			RestResponse restResponse4 = null;
			boolean stat = true;
			System.out.println("AKANNN UPLOADDDD");
			System.out.println("ORIGINNN NIHHH : " + origin);

			Bpm.Ecms.uploadByDirectory.AttachmentsType attachments[] = new Bpm.Ecms.uploadByDirectory.AttachmentsType[1];
			Bpm.Ecms.uploadByDirectory.AttachmentsType attach = new Bpm.Ecms.uploadByDirectory.AttachmentsType();
			String namaFile = plDataKecelakaanDto.getNoLaporanPolisi().replaceAll("/", "-") + "_"
					+ plDataKecelakaanDto.getIdKecelakaan() + "_" + deskripsiTambahan + "." + media.getFormat();
			attach.setNamaFile(namaFile);
			attach.setDeskripsi(deskripsiTambahan);
//			if(deskripsiTambahan != null){
//				attach.setDeskripsi(getJenisDokumenDto().getRvMeaning() + "-" + "" + "N");
//			}else{
//				attach.setDeskripsi(getJenisDokumenDto().getRvMeaning() + "-" + deskripsiTambahan + "N");
//			}

			attach.setAttachments(media.getByteData());
			attachments[0] = attach;

//			Messagebox.show("result upload : " + attachments[0].getDeskripsi());
			BpmEcmsMapping[] ecmsSelect = new BpmEcmsMapping[1];
			Bpm_ecms_mapping_querySelect_DIRECTORY_ID dirId = new Bpm_ecms_mapping_querySelect_DIRECTORY_ID();
			dirId.setDIRECTORY_ID(plDataKecelakaanDto.getIdKorbanKecelakaan());
			Bpm.Ecms.uploadByDirectory.AttachmentResultType[] result = new Bpm.Ecms.uploadByDirectory.AttachmentResultType[1];
			Bpm.Ecms.uploadByDirectory.AttachmentResultType[] result2 = new Bpm.Ecms.uploadByDirectory.AttachmentResultType[1];
			try {
				if (plPengajuanSantunanDto.getNoBerkas() == null) {
					System.out.println(
							"DARIIIIIIIIIIIIIIIII DATAAAAAAAAAAAAAAAAAAAAAA KECELAKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANNNNNN");
					result = uploadByDirectory(getPlDataKecelakaanDto().getIdKorbanKecelakaan(),
							"/Contribution Folders/DEV_BPM", attachments);
				} else {
					System.out.println(
							"DARIIIIIIIIIIIIIIIIIIIIIII PENGAJUANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN SANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNTUUUUUNANNNNNNN");
					Bpm.Ecms.uploadByDirectory.AttachmentsType attachments2[] = new Bpm.Ecms.uploadByDirectory.AttachmentsType[1];
					Bpm.Ecms.uploadByDirectory.AttachmentsType attach2 = new Bpm.Ecms.uploadByDirectory.AttachmentsType();
					attach2.setNamaFile("");
					attach2.setDeskripsi("");
					byte[] attachFile = new byte[1];
					attach2.setAttachments(attachFile);
					attachments2[0] = attach2;
					ecmsSelect = BpmEcmsSelectSvcImpl.bpm_ecms_mapping_querySelect(dirId);
					// if(ecmsSelect == null){
					System.out.println("FOLDERRRRRRR IDDD KORBAN KECELAKAAANNNNNNNNNNNN BELUMMM ADAAAAAA");
					result = uploadByDirectory(getPlDataKecelakaanDto().getIdKorbanKecelakaan(),
							"/Contribution Folders/DEV_BPM", attachments2);
					result2 = uploadByDirectory(plPengajuanSantunanDto.getNoBerkas(),
							"/Contribution Folders/DEV_BPM/" + getPlDataKecelakaanDto().getIdKorbanKecelakaan(),
							attachments);
					if (result[0].getStatusCode().equalsIgnoreCase("0")
							|| result2[0].getStatusCode().equalsIgnoreCase("0")) {
						Messagebox.show("Dokumen berhasil di upload");
					}
					// }
					/*
					 * else{ System.out.
					 * println("FOLDERRRRRRR IDDD KORBAN KECELAKAAANNNNNNNNNNNN SUDAHHHHHHH ADAAAAAA"
					 * ); result2 = uploadByDirectory(plPengajuanSantunanDto.getNoBerkas()
					 * ,"/Contribution Folders/DEV_BPM/"+getPlDataKecelakaanDto().
					 * getIdKorbanKecelakaan(), attachments);
					 * if(result2[0].getStatusCode().equalsIgnoreCase("0")){
					 * Messagebox.show("Dokumen berhasil di upload"); } }
					 */
				}

				DirectoryName dirName = new DirectoryName();
				if (plPengajuanSantunanDto.getNoBerkas() == null) {
					dirName.setNoBerkas(plDataKecelakaanDto.getIdKorbanKecelakaan());
				} else {
					dirName.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
				}
				Response res = new Response();
				try {
					res = hitserviesBpmEcmsByNoBerkas(dirName);

				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					Messagebox.show("ecms server disconect");
					BindUtils.postNotifyChange(null, null, this, "lampirans");
					e.printStackTrace();
					return;
				}

				String docName = new String();
				String filePath = null;

				if (res.getResponse().getContent() != null) {

					for (ContentResult contentResult : res.getResponse().getContent()) {
						if (contentResult.getDOriginalName().equalsIgnoreCase(namaFile)) {
							docName = contentResult.getDDocName();
							filePath = contentResult.getDWebURL();
						}
					}
				}
				System.out.println("DOKUMEN NAME : " + docName);
				System.out.println("FILE PATH : " + filePath.replaceAll("~1", ""));

				PlBerkasEcmDto plBerkasEcmDto = new PlBerkasEcmDto();
				plBerkasEcmDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
				plBerkasEcmDto.setNoRegEcms(plDataKecelakaanDto.getIdKorbanKecelakaan());

				Date a = new Date();
				Date b = new Date();
				SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");

				String tgl = date.format(a);
				String jam = time.format(b);
				String bufferTgl = tgl + " " + jam;
				Date date1 = null;
				try {
					date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(bufferTgl);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				plBerkasEcmDto.setCreatedDate(date1);
				plBerkasEcmDto.setDdocname(docName);
				plBerkasEcmDto.setFilepath(filePath.replaceAll("~1", ""));
				plBerkasEcmDto.setNamafile(fileName);

				System.out.println("Ini DDOCNAME: " + docName);

				BindUtils.postNotifyChange(null, null, this, "plBerkasEcmDto");
				restResponse = callWs(WS_URI_BERKASECM + "/saveDataPlBerkasEcm", plBerkasEcmDto, HttpMethod.POST);

				if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
					// showInfoMsgBox(restResponse.getMessage()+" 1");
				} else {
					stat = false;
					// showErrorMsgBox(restResponse.getMessage()+" 1");
				}

				for (int i = 0; i < listDokumen.size(); i++) {
					PlBerkasEcmsContainDto plBerkasEcmsContainDto = new PlBerkasEcmsContainDto();
					plBerkasEcmsContainDto.setNoRegEcms(plDataKecelakaanDto.getIdKorbanKecelakaan());
					plBerkasEcmsContainDto.setKodeBerkas(listDokumen.get(i).getRvLowValue());
					plBerkasEcmsContainDto.setdDocname(docName);
					BindUtils.postNotifyChange(null, null, this, "plBerkasEcmsContainDto");
					restResponse2 = callWs(WS_URI_BERKASECM + "/saveBerkasEcmsContain", plBerkasEcmsContainDto,
							HttpMethod.POST);
					System.out.println("ORIGINNYAAAA : " + origin);
					if (origin == null) {
						PlBerkasPengajuanDto plBerkasPengajuanDto = new PlBerkasPengajuanDto();
						if (plPengajuanSantunanDto.getNoBerkas() == null) {
							plBerkasPengajuanDto.setNoBerkas("-");
						} else {
							plBerkasPengajuanDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
						}
						plBerkasPengajuanDto.setKodeBerkas(listDokumen.get(i).getRvLowValue());
						plBerkasPengajuanDto.setDiterimaFlag("Y");
						plBerkasPengajuanDto.setAbsahFlag("Y");
						plBerkasPengajuanDto.setTglTerimaBerkas(date1);
						plBerkasPengajuanDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
						plBerkasPengajuanDto.setCreationDate(date1);
						plBerkasPengajuanDto.setLastUpdatedDate(date1);
						plBerkasPengajuanDto.setLastUpdatedBy(getCurrentUserSessionJR().getLoginID());
						plBerkasPengajuanDto.setIdGuid("");
						BindUtils.postNotifyChange(null, null, this, "plBerkasPengajuanDto");
						restResponse4 = callWs(WS_URI_BERKASECM + "/saveBerkasPengajuan", plBerkasPengajuanDto,
								HttpMethod.POST);
						if (restResponse4.getStatus() == CommonConstants.OK_REST_STATUS) {
							// showInfoMsgBox(restResponse4.getMessage()+" 4");
						} else {
							stat = false;
							// showErrorMsgBox(restResponse4.getMessage()+" 4");
						}
					}
//					else {
//						PlBerkasPengajuanDto plBerkasPengajuanDto = new PlBerkasPengajuanDto();
//						plBerkasPengajuanDto.setNoBerkas(plDataKecelakaanDto.getIdKorbanKecelakaan());
//						plBerkasPengajuanDto.setKodeBerkas(listDokumen.get(i).getRvLowValue());
//						plBerkasPengajuanDto.setDiterimaFlag("Y");
//						plBerkasPengajuanDto.setAbsahFlag("Y");
//						plBerkasPengajuanDto.setTglTerimaBerkas(date1);
//						plBerkasPengajuanDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
//						plBerkasPengajuanDto.setCreationDate(date1);
//						plBerkasPengajuanDto.setLastUpdatedDate(date1);
//						plBerkasPengajuanDto.setLastUpdatedBy(getCurrentUserSessionJR().getLoginID());
//						plBerkasPengajuanDto.setIdGuid("");
//						BindUtils.postNotifyChange(null, null, this,
//								"plBerkasPengajuanDto");
//						restResponse4 = callWs(WS_URI_BERKASECM + "/saveBerkasPengajuan", plBerkasPengajuanDto, HttpMethod.POST);
//						if (restResponse4.getStatus() == CommonConstants.OK_REST_STATUS) {
//							showInfoMsgBox(restResponse4.getMessage()+" 4");
//						} else {
//							//stat = false;
//							showErrorMsgBox(restResponse4.getMessage()+" 4");
//						}
//					}

				}

				if (restResponse2.getStatus() == CommonConstants.OK_REST_STATUS) {
					// showInfoMsgBox(restResponse2.getMessage()+" 2");
				} else {
					stat = false;
					// showErrorMsgBox(restResponse2.getMessage()+" 2");
				}

				PlBerkasEcmHDto plBerkasEcmHDto = new PlBerkasEcmHDto();
				plBerkasEcmHDto.setIdKorbanKecelakaan(plDataKecelakaanDto.getIdKorbanKecelakaan());
				plBerkasEcmHDto.setNoRegEcms(plDataKecelakaanDto.getIdKorbanKecelakaan());
				BindUtils.postNotifyChange(null, null, this, "plBerkasEcmHDto");
				restResponse3 = callWs(WS_URI_BERKASECM + "/saveBerkasEcmH", plBerkasEcmHDto, HttpMethod.POST);

				if (restResponse3.getStatus() == CommonConstants.OK_REST_STATUS) {
					// showInfoMsgBox(restResponse3.getMessage()+" 3");
				} else {
					stat = false;
					// showErrorMsgBox(restResponse3.getMessage()+" 3");
				}

				// for message box
				if (stat == true) {
					// showInfoMsgBox("Data berhasil disimpan");
				} else {
					// showErrorMsgBox("Data gagal disimpan");
				}

			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// Messagebox.show("ini id kecelakaan / no berkas : " +
			// getPlDataKecelakaanDto().getIdKecelakaan());
			// Messagebox.show("nama berkas " + attachments[0].getNamaFile());
//			Messagebox.show("deskripsi : " + attachments[0].getDeskripsi());
//			Messagebox.show("result upload : " + result[0].getStatusMessage());
			if (plPengajuanSantunanDto.getNoBerkas() == null) {
				listLampirans(plDataKecelakaanDto, null);
			} else {
				listLampirans(plDataKecelakaanDto, plPengajuanSantunanDto);
			}
			setListLampiranGroupbox(true);
			setTambahLampiranGroupbox(false);
			BindUtils.postNotifyChange(null, null, this, "listLampiranGroupbox");
			BindUtils.postNotifyChange(null, null, this, "tambahLampiranGroupbox");

//			getPageInfo().setListMode(true);
//			Executions.getCurrent().setAttribute("obj", plDataKecelakaanDto);
//			navigate(INDEX_PAGE_PATH);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@GlobalCommand("lampiranForSantunan")
	public void addForSantunan(@BindingParam("santunanDto") PlPengajuanSantunanDto selected,
			@BindingParam("noLaporanPolisi") String noLaporanPolisi) {
		setFlagLampiranSantunan(true);
		plDataKecelakaanDto = new PlDataKecelakaanDto();
		plDataKecelakaanDto.setIdKorbanKecelakaan(selected.getIdKorbanKecelakaan());
		plDataKecelakaanDto.setIdKecelakaan(selected.getIdKecelakaan());
		plDataKecelakaanDto.setNamaKorban(selected.getNama());
		plDataKecelakaanDto.setNoLaporanPolisi(noLaporanPolisi);
		BindUtils.postNotifyChange(null, null, this, "flagLampiranSantunan");
		BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
		// getPageInfo().setAddMode(true);
		// onAdd();
		addLampiran(plDataKecelakaanDto);
	}

	@Command
	public void close(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}

	private void close() {
		if (winLov == null)
			throw new RuntimeException("id popUp tidak sama dengan viewModel");
		winLov.detach();
	}

	@Command
	public void tutup() {
		close();
	}

	public List<DasiJrRefCodeDto> getListJenisDokumenDto() {
		return listJenisDokumenDto;
	}

	public void setListJenisDokumenDto(List<DasiJrRefCodeDto> listJenisDokumenDto) {
		this.listJenisDokumenDto = listJenisDokumenDto;
	}

	public DasiJrRefCodeDto getJenisDokumenDto() {
		return jenisDokumenDto;
	}

	public void setJenisDokumenDto(DasiJrRefCodeDto jenisDokumenDto) {
		this.jenisDokumenDto = jenisDokumenDto;
	}

	public boolean isFlagDataLaka() {
		return flagDataLaka;
	}

	public void setFlagDataLaka(boolean flagDataLaka) {
		this.flagDataLaka = flagDataLaka;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public PlDataKecelakaanDto getPlDataKecelakaanDto() {
		return plDataKecelakaanDto;
	}

	public void setPlDataKecelakaanDto(PlDataKecelakaanDto plDataKecelakaanDto) {
		this.plDataKecelakaanDto = plDataKecelakaanDto;
	}

	public Form getFormMaster() {
		return formMaster;
	}

	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}

	public Media getMedia() {
		return media;
	}

	public void setMedia(Media media) {
		this.media = media;
	}

	@Command
	public void viewLampiran(@BindingParam("item") String webUrl) {
		Executions.getCurrent().sendRedirect("http://prodcontent1.jasaraharja.co.id:16200" + webUrl, "_blank");
	}

	public String getIdKecelakaan() {
		return idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	@Command
	public void deleteLampiran(@BindingParam("item") final LampiranDto lampiran) {
		Messagebox.show(Labels.getLabel("C001"), Labels.getLabel("confirmation"),
				new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						System.out.println("EVENT NAME=" + evt.getName());
						System.out.println("EVENT NAME=" + Messagebox.ON_YES);
						if (Messagebox.ON_YES.equals(evt.getName())) {
							System.out.println("Deleting lampiran");
							Bpm_ecms_document_deleteRequest param = new Bpm_ecms_document_deleteRequest();
							param.setDocName(lampiran.getIdContent());
							Bpm_ecms_document_deleteResponse res = new Bpm_ecms_document_deleteResponse();
							try {
								res = deleteDoc(param);
								lampirans.remove(lampiran);
								BindUtils.postNotifyChange(null, null, DaftarLampiranVmd.this, "lampirans");
								System.out.println("BindUtils.postNotifyChange = lampirans");
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				});

	}

	private Bpm_ecms_document_deleteResponse deleteDoc(Bpm_ecms_document_deleteRequest params) throws RemoteException {
		Bpm_ecms_document_deleteProxy servc = new Bpm_ecms_document_deleteProxy();
		Bpm_ecms_document_delete_PortType port = servc.getBpm_ecms_document_delete_PortType();
		return port.bpm_ecms_document_delete(params);
	}

	public boolean isFlagLampiranSantunan() {
		return flagLampiranSantunan;
	}

	public void setFlagLampiranSantunan(boolean flagLampiranSantunan) {
		this.flagLampiranSantunan = flagLampiranSantunan;
	}

	public String getDeskripsiTambahan() {
		return deskripsiTambahan;
	}

	public void setDeskripsiTambahan(String deskripsiTambahan) {
		this.deskripsiTambahan = deskripsiTambahan;
	}

	public PlPengajuanSantunanDto getPlPengajuanSantunanDto() {
		return plPengajuanSantunanDto;
	}

	public void setPlPengajuanSantunanDto(PlPengajuanSantunanDto plPengajuanSantunanDto) {
		this.plPengajuanSantunanDto = plPengajuanSantunanDto;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDto() {
		return listDataLakaDto;
	}

	public void setListDataLakaDto(List<PlDataKecelakaanDto> listDataLakaDto) {
		this.listDataLakaDto = listDataLakaDto;
	}

	public boolean isListLampiranGroupbox() {
		return listLampiranGroupbox;
	}

	public void setListLampiranGroupbox(boolean listLampiranGroupbox) {
		this.listLampiranGroupbox = listLampiranGroupbox;
	}

	public boolean isTambahLampiranGroupbox() {
		return tambahLampiranGroupbox;
	}

	public void setTambahLampiranGroupbox(boolean tambahLampiranGroupbox) {
		this.tambahLampiranGroupbox = tambahLampiranGroupbox;
	}

	public boolean isKetVisible() {
		return ketVisible;
	}

	public void setKetVisible(boolean ketVisible) {
		this.ketVisible = ketVisible;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public List<DasiJrRefCodeDto> getListDokumen() {
		return listDokumen;
	}

	public void setListDokumen(List<DasiJrRefCodeDto> listDokumen) {
		this.listDokumen = listDokumen;
	}

	public boolean isMulti() {
		return multi;
	}

	public void setMulti(boolean multi) {
		this.multi = multi;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public boolean isFlagDataSantunan() {
		return flagDataSantunan;
	}

	public void setFlagDataSantunan(boolean flagDataSantunan) {
		this.flagDataSantunan = flagDataSantunan;
	}

}
