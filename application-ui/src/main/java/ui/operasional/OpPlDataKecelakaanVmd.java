package ui.operasional;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STPageOrientation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import Bpm.InstanceInfo.InstanceInfo;
import Bpm.ListTaskByUser.UserTask;
import Bpm.ListTaskByUser.UserTaskService;
import Bpm.StartProcess.Bpm_start_processProxy;
import Bpm.StartProcess.Bpm_start_processRequest;
import Bpm.StartProcess.Bpm_start_processResponse;
import Bpm.StartProcess.Bpm_start_process_PortType;
import Bpm.StartProcess.StartBPMSvcImpl;
import Bpm.UpdateTaskPayload.Bpm_update_task_payloadRequest;
import Bpm.UpdateTaskPayload.CredentialType;
import Bpm.UpdateTaskPayload.PayloadType;
import Bpm.UpdateTaskPayload.UpdateTaskPayloadServiceImpl;
import share.AuthRsPicDto;
import share.AuthUserDto;
import share.AuthUserGadgetDto;
import share.DasiJrRefCodeDto;
import share.DukcapilWinDto;
import share.FndBankDto;
import share.FndCamatDto;
import share.FndJenisKendaraanDto;
import share.FndKantorJasaraharjaDto;
import share.FndLokasiDto;
import share.PlAdditionalDescDto;
import share.PlAngkutanKecelakaanDto;
import share.PlBerkasEcmDto;
import share.PlBerkasEcmsContainDto;
import share.PlBerkasPengajuanDto;
import share.PlDataKecelakaanDto;
import share.PlDisposisiDto;
import share.PlInstansiDto;
import share.PlJaminanDto;
import share.PlKorbanKecelakaanDto;
import share.PlMappingCamatDto;
import share.PlMappingPoldaDto;
import share.PlNihilKecelakaanDto;
import share.PlPengajuanSantunanDto;
import share.PlPeriodeLockDto;
import share.PlPksRDto;
import share.PlRegisterSementaraDto;
import share.PlRekeningRDto;
import share.PlRequestPerubahanDto;
import share.PlRsBpjDto;
import share.PlRsMapBpjDto;
import share.PlRumahSakitDto;
import share.PlTindakLanjutDto;
import share.PlTlRDto;
import ui.TestVmd;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;
import core.model.PlAdditionalDesc;
import core.model.PlPksR;
import core.model.PlRsBpj;

@Init(superclass = true)
public class OpPlDataKecelakaanVmd extends BaseVmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static Logger logger = LoggerFactory.getLogger(OpPlDataKecelakaanVmd.class);

	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH + "/operasionalDetail/DataKecelakaanDetail.zul";
	private final String LAMPIRAN_PAGE_PATH = UIConstants.BASE_PAGE_PATH + "/operasionalDetail/DaftarLampiran.zul";
	private final String DATA_NIHIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/KonfirmasiDataLakaNihil.zul";
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH + "/operasional/OpDataKecelakaan/_index.zul";
	private final String INDEX_REGISTER_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpRegisterSementara/_index.zul";
	private final String INDEX_PENGAJUAN_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpPengajuanSantunan/_index.zul";

	private final String WS_URI = "/OpDataKecelakaan";
	private final String WS_URI_LOV = "/Lov";

	private PlDataKecelakaanDto selected = new PlDataKecelakaanDto();

	private List<AuthUserDto> authUserDtos = new ArrayList<>();
	private AuthUserDto authUserDto = new AuthUserDto();

	private List<PlDataKecelakaanDto> listPlDataKecelakaanDtos = new ArrayList<>();
	private List<PlDataKecelakaanDto> listPlDataKecelakaanDtosCopy = new ArrayList<>();
	private List<PlAngkutanKecelakaanDto> listPlAngkutanKecelakaanDtos = new ArrayList<>();
	private List<PlKorbanKecelakaanDto> listPlKorbanKecelakaanDtos = new ArrayList<>();

	// for search listbox
	private List<FndKantorJasaraharjaDto> listAsalBerkasDto = new ArrayList<>();
	private List<FndKantorJasaraharjaDto> listAsalBerkasDtoCopy = new ArrayList<>();

	private List<FndKantorJasaraharjaDto> listSamsatDto = new ArrayList<>();
	private List<FndKantorJasaraharjaDto> listSamsatDtoCopy = new ArrayList<>();

	private List<PlInstansiDto> listInstansiDto = new ArrayList<>();
	private List<PlInstansiDto> listInstansiDtoCopy = new ArrayList<>();

	private List<DasiJrRefCodeDto> listInstansiPembuatDto = new ArrayList<>();
	private List<PlJaminanDto> listLingkupJaminanDto = new ArrayList<>();
	private List<PlJaminanDto> listJenisPertanggunganDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listSifatCideraDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listSifatKecelakaanDto = new ArrayList<>();

	private List<FndLokasiDto> listLokasiDto = new ArrayList<>();
	private List<FndLokasiDto> listLokasiDtoCopy = new ArrayList<>();

	private List<DasiJrRefCodeDto> listKasusKecelakaanDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listStatusKendaraanDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listJenisSimDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listMerkKendaraanDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listIdentitasDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listJenisPekerjaanDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listStatusKorbanDto = new ArrayList<>();
	private List<DasiJrRefCodeDto> listStatusNikahDto = new ArrayList<>();
	private List<FndCamatDto> listProvinsiDto = new ArrayList<>();
	private List<FndCamatDto> listKabkotaDto = new ArrayList<>();
	private List<FndCamatDto> listCamatDto = new ArrayList<>();
	private List<FndJenisKendaraanDto> listJenisKendaraanDto = new ArrayList<>();

	private String katastrop;
	private List<String> listKatastrop = new ArrayList<>();

	private PlDataKecelakaanDto plDataKecelakaanDto = new PlDataKecelakaanDto();
	private PlAngkutanKecelakaanDto plAngkutanKecelakaanDto = new PlAngkutanKecelakaanDto();
	private PlKorbanKecelakaanDto plKorbanKecelakaanDto = new PlKorbanKecelakaanDto();
	private PlNihilKecelakaanDto plNihilKecelakaanDto = new PlNihilKecelakaanDto();

	private FndLokasiDto fndLokasiDto = new FndLokasiDto();

	private Date kejadianStartDate;
	private Date kejadianEndDate;
	private String asalBerkas;
	private String samsat;
	private Date laporanStartDate;
	private Date laporanEndDate;
	private String instansi;
	private String instansiPembuat;
	private String noLaporan;
	private String lokasi;
	private String namaKorban;
	private String noIdentitas;
	private String lingkupJaminan;
	private String jenisPertanggungan;
	private String sifatCidera;
	private String sifatKecelakaan;
	private String kecelakaanKatostrop;
	private String perusahaanPenerbangan;
	private String perusahaanOtobus;

	private String searchIndex;
	private int pageSize = 5;

	private String searchLokasi;

	private boolean formTambahKendaraan;
	private boolean listKendaraan;

	private boolean formTambahKorban;
	private boolean listKorban;

	// tab data laka
	private FndKantorJasaraharjaDto samsatDto = new FndKantorJasaraharjaDto();
	private FndCamatDto provinsiDto = new FndCamatDto();
	private FndCamatDto kabKotaDto = new FndCamatDto();
	private FndCamatDto camatDto = new FndCamatDto();
	private DasiJrRefCodeDto instansiPembuatDto = new DasiJrRefCodeDto();
	private Date tglKejadian;
	private Date jamKejadian;
	private boolean statusLaporanGroupBox = false;
	private String statusLaporan;
	private PlMappingPoldaDto poldaDto = new PlMappingPoldaDto();
	private PlMappingCamatDto plMappingCamatDto = new PlMappingCamatDto();

	// tab kendaraan
	private FndJenisKendaraanDto jenisKendaraanDto = new FndJenisKendaraanDto();
	private DasiJrRefCodeDto jenisSimDto = new DasiJrRefCodeDto();
	private DasiJrRefCodeDto merkKendaraanDto = new DasiJrRefCodeDto();
	private FndCamatDto provinsiPengemudiDto = new FndCamatDto();
	private FndCamatDto kabkotaPengemudiDto = new FndCamatDto();
	private FndCamatDto camatDtoPengemudi = new FndCamatDto();
	private FndCamatDto provinsiPemilikDto = new FndCamatDto();
	private FndCamatDto kabKotaPemilikDto = new FndCamatDto();
	private FndCamatDto camatDtoPemilik = new FndCamatDto();
	private List<FndCamatDto> listKabKotaPemilikDto = new ArrayList<>();
	private List<FndCamatDto> listCamatDtoPemilik = new ArrayList<>();

	private String searchKendaraan;

	// tab korban
	private List<FndCamatDto> listProvinsiKorbanDto = new ArrayList<>();
	private List<FndCamatDto> listKabKotaKorbanDto = new ArrayList<>();
	private List<FndCamatDto> listCamatKorbanDto = new ArrayList<>();
	private DasiJrRefCodeDto jenisIdentitasDto = new DasiJrRefCodeDto();
	private DasiJrRefCodeDto jenisPekerjaanDto = new DasiJrRefCodeDto();
	private DasiJrRefCodeDto statusKorbanDto = new DasiJrRefCodeDto();
	private DasiJrRefCodeDto statusNikahDto = new DasiJrRefCodeDto();
	private PlAngkutanKecelakaanDto kendaraanPenjaminDto = new PlAngkutanKecelakaanDto();
	private PlAngkutanKecelakaanDto kendaraanPosisiDto = new PlAngkutanKecelakaanDto();
	private FndCamatDto provinsiKorbanDto = new FndCamatDto();
	private FndCamatDto kabKotaKorbanDto = new FndCamatDto();
	private FndCamatDto camatDtoKorban = new FndCamatDto();
	private String searchKorban;

	private FndKantorJasaraharjaDto asalBerkasDto = new FndKantorJasaraharjaDto();
	private PlInstansiDto instansiDto = new PlInstansiDto();
	private PlJaminanDto lingkupJaminanDto = new PlJaminanDto();
	private PlJaminanDto jenisPertanggunganDto = new PlJaminanDto();
	private DasiJrRefCodeDto sifatCideraDto = new DasiJrRefCodeDto();
	private DasiJrRefCodeDto sifatKecelakaanDto = new DasiJrRefCodeDto();
	private DasiJrRefCodeDto kasusKecelakaanDto = new DasiJrRefCodeDto();
	private FndLokasiDto lokasiDto = new FndLokasiDto();
	private DasiJrRefCodeDto statusKendaraanDto = new DasiJrRefCodeDto();

	// for listbox search
	private String searchAsalBerkas;
	private String searchInstansi;
	private String searchSamsat;
	private String searchProvinsi;
	private String searchCamat;
	private String searchKabkota;

	// for groupbox status checkbox
	private boolean status;
	private boolean suratKeterangan;
	private boolean dataIrsms;
	private boolean dataMutasi;

	// no laporan
	private String laporan1;
	private String laporan2;
	private String laporan3;

	private boolean flagNihilRegister;
	private boolean flagDataLakaRegister;

	// flag
	private boolean flagSaveDataLaka = false;
	private boolean flagDataLakaSantunan;
	private boolean flagAddDataKendaraan;
	private boolean flagAddDataKorban;

	// constraint
	private String constraintAsalBerkas;
	private String constraintInstansi;
	private String constraintSamsat;

	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent().getAttribute(UIConstants.SESS_LOGIN_ID);

	private AMedia fileContent;
	private DukcapilWinDto dukcapilWinDto = new DukcapilWinDto();
	private boolean alertMsgVisible = false;
	private boolean alertMsgVisibleTglLaporan = false;
	private String statusData;

	@SuppressWarnings("unchecked")
	protected void loadList() {
		Map<String, Object> map = new HashMap<>();
		if (Executions.getCurrent().getAttribute("obj") != null) {
			map = (Map<String, Object>) Executions.getCurrent().getAttribute("obj");
			kejadianStartDate = (Date) map.get("kejadianStartDate");
			kejadianEndDate = (Date) map.get("kejadianEndDate");
			laporanStartDate = (Date) map.get("laporanStartDate");
			laporanEndDate = (Date) map.get("laporanEndDate");
			instansiDto.setKodeInstansi((String) map.get("kodeInstansi"));
			BindUtils.postNotifyChange(null, null, this, "kejadianStartDate");
			BindUtils.postNotifyChange(null, null, this, "kejadianEndDate");
			BindUtils.postNotifyChange(null, null, this, "laporanStartDate");
			BindUtils.postNotifyChange(null, null, this, "laporanEndDate");
			BindUtils.postNotifyChange(null, null, this, "instansiDto");
			searchIndex();
		}

//		searchAsalBerkas();
		loadAsalBerkas("%");
//		searchInstansi();
		loadInstansi("%");
//		searchSamsat();`
		loadSamsat("%");
		searchLokasi();
		for (FndKantorJasaraharjaDto asalBerkas : listAsalBerkasDto) {
			if (userSession.getKantor().equalsIgnoreCase(asalBerkas.getKodeKantorJr())) {
				asalBerkasDto = new FndKantorJasaraharjaDto();
				asalBerkasDto = asalBerkas;
			}
		}
		listKatastrop();
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		setKejadianStartDate(c.getTime());
		BindUtils.postNotifyChange(null, null, this, "kejadianStartDate");
		BindUtils.postNotifyChange(null, null, this, "asalBerkasDto");
	}

	public void listKatastrop() {
		listKatastrop.add("-");
		listKatastrop.add("Biasa");
		listKatastrop.add("Katastrop");
		// setKatastrop("Biasa");
		BindUtils.postNotifyChange(null, null, this, "listKatastrop");
	}

	@Command
	public void listLokasi() {
		RestResponse listLokasiRest = callWs(WS_URI_LOV + "/getListLokasi/" + getSearchLokasi(),
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listLokasiDto = JsonUtil.mapJsonToListObject(listLokasiRest.getContents(), FndLokasiDto.class);
			BindUtils.postNotifyChange(null, null, this, "listLokasiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listInstansiPembuat() {
		RestResponse listInstansiPembuatRest = callWs(WS_URI_LOV + "/getListInstansiPembuat",
				new HashMap<String, Object>(), HttpMethod.POST);
		listInstansiPembuatDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listInstansiPembuatRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listInstansiPembuatDto.add(kosong);
			listInstansiPembuatDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listInstansiPembuatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Command
	public void listLingkupJaminan() {
		RestResponse listLingkupJaminanRest = callWs(WS_URI_LOV + "/getListLingkupJaminan",
				new HashMap<String, Object>(), HttpMethod.POST);
		listLingkupJaminanDto = new ArrayList<>();
		List<PlJaminanDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listLingkupJaminanRest.getContents(), PlJaminanDto.class);
			PlJaminanDto kosong = new PlJaminanDto();
			kosong.setKodeJaminan(null);
			kosong.setLingkupJaminan("-");
			listLingkupJaminanDto.add(kosong);
			listLingkupJaminanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listLingkupJaminanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listSifatCidera() {
		RestResponse listSifatCideraRest = callWs(WS_URI_LOV + "/getListSifatCidera", new HashMap<String, Object>(),
				HttpMethod.POST);
		listSifatCideraDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listSifatCideraRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listSifatCideraDto.add(kosong);
			listSifatCideraDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listSifatCideraDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listSifatKecelakaan() {

		RestResponse listSifatKecelakaanRest = callWs(WS_URI_LOV + "/getListSifatKecelakaan",
				new HashMap<String, Object>(), HttpMethod.POST);
		listSifatKecelakaanDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listSifatKecelakaanRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listSifatKecelakaanDto.add(kosong);
			listSifatKecelakaanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listSifatKecelakaanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listJenisPertanggungan() {
		RestResponse listJenisPertanggunganRest = callWs(WS_URI_LOV + "/getListJenisPertanggungan",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listJenisPertanggunganDto = JsonUtil.mapJsonToListObject(listJenisPertanggunganRest.getContents(),
					PlJaminanDto.class);
			BindUtils.postNotifyChange(null, null, this, "listJenisPertanggunganDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void cekConstraint() {
		if (asalBerkasDto == null || asalBerkasDto.getKodeKantorJr() == null) {
			setConstraintAsalBerkas("no empty");
			BindUtils.postNotifyChange(null, null, this, "asalBerkasDto");
		}
	}

	/*
	 * public void updateTask() { System.out.println("Level Kantor : " +
	 * userSession.getLevelKantor()); String pattern = "dd-MM-yyyy";
	 * SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	 * 
	 * String dateFormat = simpleDateFormat.format(new Date());
	 * System.out.println(dateFormat); System.out.println("Kantor : " +
	 * userSession.getKantor()); System.out.println("Login ID : " +
	 * userSession.getLoginID()); String kantor = userSession.getKantor(); String
	 * login = userSession.getLoginID();
	 * 
	 * UserTask userTask = new UserTask(); try { userTask =
	 * UserTaskService.getUserTaskByIdKecelakaan(login, "welcome1",
	 * plDataKecelakaanDto.getIdKecelakaan());
	 * 
	 * } catch (Exception e) { e.printStackTrace(); Messagebox
	 * .show("Data belum proses BPM atau data kecelakaan tidak ditemukan"); }
	 * 
	 * Bpm_update_task_payloadRequest req = new Bpm_update_task_payloadRequest();
	 * CredentialType credential = new CredentialType(); credential.setLogin(login);
	 * credential.setPassword("welcome1"); credential.setIdentityContext("");
	 * credential.setOnBehalfOfUser("");
	 * 
	 * PayloadType payload = new PayloadType(); InstanceInfo instance = new
	 * InstanceInfo();
	 * 
	 * instance.setNomorPermohonan(plDataKecelakaanDto.getIdKecelakaan());
	 * instance.setKodeCabang(kantor);
	 * instance.setCreatedBy(userTask.getCreatedBy());
	 * instance.setCreationDate(userTask.getCreationDate());
	 * instance.setLastUpdatedBy(login); instance.setLastUpdatedDate(dateFormat);
	 * instance.setIdGUID("N"); instance.setPengajuanType("");
	 * instance.setKodeKantor(kantor); instance.setOtherInfo1("");
	 * instance.setOtherInfo2(""); instance.setOtherInfo3("");
	 * instance.setOtherInfo4(""); instance.setOtherInfo5("");
	 * 
	 * payload.setInstanceInfo(instance); try { req.setCredential(credential);
	 * req.setPayload(payload); req.setOutcome("");
	 * req.setTaskNumber(userTask.getTaskNumber());
	 * 
	 * UpdateTaskPayloadServiceImpl.bpmUpdateTaskPayload(req);
	 * 
	 * } catch (Exception e) { e.printStackTrace();
	 * 
	 * } }
	 */

	@Command
	public void searchIndex() {

		if (getSearchIndex() == null || getSearchIndex().equalsIgnoreCase("")) {
			setSearchIndex("%%");
		}

		if (laporanStartDate == null || dateToString(laporanStartDate).equalsIgnoreCase("")) {
			setLaporanStartDate(stringToDate("01/01/1990"));
		}
		if (laporanEndDate == null || dateToString(laporanEndDate).equalsIgnoreCase("")) {
			setLaporanEndDate(new Date());
		}
		if (kejadianStartDate == null || dateToString(kejadianStartDate).equalsIgnoreCase("")) {
			setKejadianStartDate(new Date());
		}
		if (kejadianEndDate == null || dateToString(kejadianEndDate).equalsIgnoreCase("")) {
			setKejadianEndDate(new Date());
		}
		if (instansiPembuatDto.getRvHighValue() == null || instansiPembuatDto.getRvHighValue().equalsIgnoreCase("")) {
			instansiPembuat = "";
		} else if (instansiPembuatDto.getRvHighValue() == "2"
				|| instansiPembuatDto.getRvHighValue().equalsIgnoreCase("2")) {
			instansiPembuat = "Y";
		} else if (instansiPembuatDto.getRvHighValue() == "1"
				|| instansiPembuatDto.getRvHighValue().equalsIgnoreCase("1")) {
			instansiPembuat = "Y";
		} else {
			instansiPembuat = "N";
		}
		if (asalBerkasDto == null) {
			asalBerkasDto = new FndKantorJasaraharjaDto();
		}
		if (asalBerkasDto.getKodeKantorJr() == null) {
			asalBerkasDto.setKodeKantorJr("");
		}

		if (samsatDto == null) {
			samsatDto = new FndKantorJasaraharjaDto();
		}
		if (samsatDto.getKodeKantorJr() == null) {
			samsatDto.setKodeKantorJr("");
		}

		if (fndLokasiDto == null) {
			fndLokasiDto = new FndLokasiDto();
		}
		if (fndLokasiDto.getKodeLokasi() == null) {
			fndLokasiDto.setKodeLokasi("");
		}
		if (instansiDto == null) {
			instansiDto = new PlInstansiDto();
		}
		if (instansiDto.getKodeInstansi() == null) {
			instansiDto.setKodeInstansi("");
		}

		Map<String, Object> filter = new HashMap<>();
		filter.put("kejadianStartDate", dateToString(kejadianStartDate));
		filter.put("kejadianEndDate", dateToString(kejadianEndDate));
		filter.put("asalBerkas", asalBerkasDto.getKodeKantorJr());
		filter.put("samsat", samsatDto.getKodeKantorJr());
		filter.put("laporanStartDate", dateToString(laporanStartDate));
		filter.put("laporanEndDate", dateToString(laporanEndDate));
		filter.put("instansi", instansiDto.getKodeInstansi());
		filter.put("instansiPembuat", instansiPembuat);
		filter.put("noLaporan", noLaporan);
		filter.put("lokasi", fndLokasiDto.getKodeLokasi());
		filter.put("namaKorban", namaKorban);
		filter.put("noIdentitas", noIdentitas);
		filter.put("lingkupJaminan", lingkupJaminanDto.getLingkupJaminan());
		filter.put("jenisPertanggungan", jenisPertanggunganDto.getKodeJaminan());
		filter.put("sifatCidera", sifatCideraDto.getRvLowValue());
		filter.put("sifatKecelakaan", sifatKecelakaanDto.getRvLowValue());
		filter.put("kecelakaanKatostrop", katastrop);
		filter.put("perusahaanPenerbangan", perusahaanPenerbangan);
		filter.put("perusahaanOtobus", perusahaanOtobus);
		filter.put("search", searchIndex);
		RestResponse rest = callWs(WS_URI + "/all", filter, HttpMethod.POST);
		try {
			listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(), PlDataKecelakaanDto.class);
			BindUtils.postNotifyChange(null, null, this, "listPlDataKecelakaanDtos");
			listPlDataKecelakaanDtosCopy = new ArrayList<>();
			listPlDataKecelakaanDtosCopy.addAll(listPlDataKecelakaanDtos);
			BindUtils.postNotifyChange(null, null, this, "listPlDataKecelakaanDtosCopy");

		} catch (Exception e) {
			showSmartMsgBox("Unable to Convert JSON!");
			e.printStackTrace();
		}

	}

	@NotifyChange("listPlDataKecelakaanDtosCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listPlDataKecelakaanDtosCopy));
		if (listPlDataKecelakaanDtosCopy != null || listPlDataKecelakaanDtosCopy.size() > 0) {
			listPlDataKecelakaanDtosCopy.clear();
		}
		if (listPlDataKecelakaanDtos != null && listPlDataKecelakaanDtos.size() > 0) {
			for (PlDataKecelakaanDto dto : listPlDataKecelakaanDtos) {
				System.out.println("+++");
				if (sdf.format(dto.getTglKejadian()).equals(cari) || date.format(dto.getTglKejadian()).equals(cari)
						|| month.format(dto.getTglKejadian()).equals(cari)
						|| year.format(dto.getTglKejadian()).equals(cari)
						|| dto.getLokasiDesc().toUpperCase().contains(cari)
						|| dto.getNamaInstansi().toUpperCase().contains(cari)
						|| dto.getNoLaporanPolisi().toUpperCase().contains(cari)
						|| dto.getNamaKorban().toUpperCase().contains(cari)
						|| dto.getCidera().toUpperCase().contains(cari)
						|| dto.getSifatKecelakaanDesc().toUpperCase().contains(cari)
						|| dto.getStatusLaporanPolisi().toUpperCase().contains(cari)) {
					listPlDataKecelakaanDtosCopy.add(dto);
				}
			}
		}
	}

	@Command
	public void edit(@BindingParam("popup") String popup, @BindingParam("item") PlDataKecelakaanDto selected) {
		if (selected == null || selected.getIdKecelakaan() == null) {
			showSmartMsgBox("W001");
			return;
		}
		Map<String, Object> args = new HashMap<>();
		Executions.getCurrent().setAttribute("obj", selected);
		getPageInfo().setEditMode(true);
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}

	}

	public void onEdit() {
		try {

			Map<String, Object> map = new HashMap<>();
			if (flagDataLakaRegister && plDataKecelakaanDto != null) {
				map.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
			} else if (flagDataLakaSantunan && plDataKecelakaanDto != null) {
				map.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
			} else {
				plDataKecelakaanDto = (PlDataKecelakaanDto) Executions.getCurrent().getAttribute("obj");
				map.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
			}
			RestResponse rest = callWs(WS_URI + "/dataLakaById", map, HttpMethod.POST);
			tabDataLaka();
			try {
				listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(), PlDataKecelakaanDto.class);

				for (PlDataKecelakaanDto a : listPlDataKecelakaanDtos) {
					SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
					SimpleDateFormat time = new SimpleDateFormat("HH:mm");
					String tgl = date.format(a.getTglKejadian());
					String jam = time.format(a.getTglKejadian());
					Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(tgl);
					Date date2 = new SimpleDateFormat("HH:mm").parse(jam);
					setTglKejadian(date1);
					setJamKejadian(date2);

					if (a.getStatusTransisi().equalsIgnoreCase("1")) {
						String string = a.getNoLaporanPolisi();
						String[] parts = string.split("/");
						String part1 = "";
						String part2 = "";
						String part3 = "";
						String part4 = "";
						String part5 = "";
						String part6 = "";

						try {
							part1 = parts[0];
							try {
								part2 = parts[1];
								try {
									part3 = parts[2];
									try {
										part4 = parts[3];
										try {
											part5 = parts[4];
											try {
												part6 = parts[5];
											} catch (Exception e) {
												e.printStackTrace();
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									} catch (Exception e) {
										e.printStackTrace();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						setLaporan1(part1 + "/" + part2 + "/");
						setLaporan2(part3);
						setLaporan3("/" + part4 + "/" + part5 + "/" + part6);
					} else if (a.getStatusTransisi().equalsIgnoreCase("6")) {
						String string = a.getNoLaporanPolisi();
						String[] parts = string.split("/");
						String part1 = "";
						String part2 = "";
						String part3 = "";

						try {
							part1 = parts[0];
							try {
								part2 = parts[1];
								try {
									part3 = parts[2];

								} catch (Exception e) {
									e.printStackTrace();
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						setLaporan1(part1 + "/" + part2 + "/");
						setLaporan2(part3);
					} else if (a.getStatusTransisi().equalsIgnoreCase("2")
							|| a.getStatusTransisi().equalsIgnoreCase("3")
							|| a.getStatusTransisi().equalsIgnoreCase("4")
							|| a.getStatusTransisi().equalsIgnoreCase("5")) {
						setLaporan2(a.getNoLaporanPolisi());
					} else {
						setLaporan2(a.getNoLaporanPolisi());
					}

					setSearchAsalBerkas(a.getAsalBerkasDesc());
					setSearchInstansi(a.getInstansiDesc());
					setSearchSamsat(a.getSamsatDesc());
					plDataKecelakaanDto.setNoUrut(a.getNoUrut());
					plDataKecelakaanDto.setIdKecelakaan(a.getIdKecelakaan());
					plDataKecelakaanDto.setNamaPetugas(a.getNamaPetugas());
					plDataKecelakaanDto.setTglLaporanPolisi(a.getTglLaporanPolisi());
					plDataKecelakaanDto.setDeskripsiLokasi(a.getDeskripsiLokasi());
					kasusKecelakaanDto.setRvMeaning(a.getKasusKecelakaanDesc());
					kasusKecelakaanDto.setRvLowValue(a.getKodeKasusKecelakaan());
					provinsiDto.setNamaProvinsi(a.getProvinsiDesc());
					provinsiDto.setKodeProvinsi(a.getKodeProvinsiLokasi());
					kabKotaDto.setNamaKabkota(a.getKabKotaDesc());
					kabKotaDto.setKodeKabkota(a.getKodeKabkotaLokasi());
					camatDto.setNamaCamat(a.getCamatDesc());
					camatDto.setKodeCamat(a.getKodeCamatLokasi());
					sifatKecelakaanDto.setRvMeaning(a.getSifatKecelakaanDesc());
					sifatKecelakaanDto.setRvLowValue(a.getSifatKecelakaan());
					asalBerkasDto.setKodeKantorJr(a.getAsalBerkas());
					instansiDto.setKodeInstansi(a.getKodeInstansi());
					samsatDto.setKodeKantorJr(a.getKodeWilayah());
					instansiPembuatDto.setRvLowValue(a.getStatusTransisi());
					plDataKecelakaanDto.setKasusKecelakaanDesc(a.getKasusKecelakaanDesc());
					plDataKecelakaanDto.setSifatKecelakaanDesc(a.getSifatKecelakaanDesc());
					plDataKecelakaanDto.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
					plDataKecelakaanDto.setGpsLu(a.getGpsLu());
					plDataKecelakaanDto.setGpsLs(a.getGpsLs());
					System.out.println("=================================================");
					for (DasiJrRefCodeDto dasi : listKasusKecelakaanDto) {
						if (kasusKecelakaanDto.getRvMeaning().equalsIgnoreCase(dasi.getRvMeaning())) {
							kasusKecelakaanDto = new DasiJrRefCodeDto();
							kasusKecelakaanDto = dasi;
							System.out.println("SET kasus kecelakaan dto dari listkecelakaan");
							System.out.println(JsonUtil.getJson(dasi));
						}
					}

					for (DasiJrRefCodeDto dasi : listInstansiPembuatDto) {
						if (instansiPembuatDto.getRvLowValue().equalsIgnoreCase(dasi.getRvLowValue())) {
							instansiPembuatDto = new DasiJrRefCodeDto();
							instansiPembuatDto = dasi;
						}
					}

					for (FndCamatDto provinsi : listProvinsiDto) {
						if (provinsiDto.getNamaProvinsi().equalsIgnoreCase(provinsi.getNamaProvinsi())) {
							provinsiDto = new FndCamatDto();
							provinsiDto = provinsi;
						}
					}
					getKabkota();

					for (FndCamatDto kabKota : listKabkotaDto) {
						if (kabKotaDto.getNamaKabkota().equalsIgnoreCase(kabKota.getNamaKabkota())) {
							kabKotaDto = new FndCamatDto();
							kabKotaDto = kabKota;
						}
					}
					getCamat();
					for (FndCamatDto camat : listCamatDto) {
						if (camatDto.getNamaCamat().equalsIgnoreCase(camat.getNamaCamat())) {
							camatDto = new FndCamatDto();
							camatDto = camat;
						}
					}
					for (DasiJrRefCodeDto sifatLaka : listSifatKecelakaanDto) {
						if (sifatKecelakaanDto.getRvMeaning().equalsIgnoreCase(sifatLaka.getRvMeaning())) {
							sifatKecelakaanDto = new DasiJrRefCodeDto();
							sifatKecelakaanDto = sifatLaka;
						}
					}
					for (DasiJrRefCodeDto instansiPembuat : listInstansiPembuatDto) {
						System.err.println("luthfi99 " + instansiPembuatDto.getRvLowValue() + " "
								+ instansiPembuat.getRvLowValue());
						if (instansiPembuatDto.getRvLowValue().equalsIgnoreCase(instansiPembuat.getRvLowValue())) {
							instansiPembuatDto = new DasiJrRefCodeDto();
							instansiPembuatDto = instansiPembuat;
						}
					}

					for (FndKantorJasaraharjaDto asalBerkas : listAsalBerkasDto) {
						if (a.getAsalBerkasDesc().equalsIgnoreCase(asalBerkas.getNama())) {
							asalBerkasDto = new FndKantorJasaraharjaDto();
							asalBerkasDto = asalBerkas;
						}
					}

					for (PlInstansiDto instansi : listInstansiDto) {
						if (a.getInstansiDesc().equalsIgnoreCase(instansi.getDeskripsi())) {
							instansiDto = new PlInstansiDto();
							instansiDto = instansi;
						}
					}
					loadSamsat("%");
					for (FndKantorJasaraharjaDto samsat : listSamsatDto) {
						if (samsatDto.getKodeKantorJr().equalsIgnoreCase(samsat.getKodeKantorJr())) {
							samsatDto = new FndKantorJasaraharjaDto();
							samsatDto = samsat;
						}
					}

					instansiPembuat();

					if (a.getStatusLaporanPolisi().equalsIgnoreCase("Y")) {
						setStatusData("ra1");
					} else if (a.getStatusLaporanPolisi().equalsIgnoreCase("S")) {
						setStatusData("ra2");
					} else if (a.getStatusLaporanPolisi().equalsIgnoreCase("")) {
						setStatusData("ra3");
					} else if (a.getStatusLaporanPolisi().equalsIgnoreCase("N")) {
						setStatusData("ra4");
						;
					}

					/*
					 * for(int i = 0; i < listKasusKecelakaanDto.size() ; i++){ Messagebox
					 * .show("test "+listKasusKecelakaanDto.get(i).getRvMeaning
					 * ()+" \n"+kasusKecelakaanDto.getRvMeaning()+"\n");
					 * 
					 * if(kasusKecelakaanDto.getRvLowValue().equalsIgnoreCase(
					 * listKasusKecelakaanDto.get(i).getRvLowValue())){ kasusKecelakaanDto =
					 * listKasusKecelakaanDto.get(i); Messagebox
					 * .show("OK "+listKasusKecelakaanDto.get(i).getRvMeaning ()); } }
					 */
					BindUtils.postNotifyChange(null, null, this, "asalBerkasDto");
					BindUtils.postNotifyChange(null, null, this, "instansiDto");
					BindUtils.postNotifyChange(null, null, this, "samsatDto");
					BindUtils.postNotifyChange(null, null, this, "searchAsalBerkas");
					BindUtils.postNotifyChange(null, null, this, "searchInstansi");
					BindUtils.postNotifyChange(null, null, this, "searchSamsat");
					BindUtils.postNotifyChange(null, null, this, "laporan1");
					BindUtils.postNotifyChange(null, null, this, "laporan2");
					BindUtils.postNotifyChange(null, null, this, "laporan3");
					BindUtils.postNotifyChange(null, null, this, "kasusKecelakaanDto");
					BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
					BindUtils.postNotifyChange(null, null, this, "kabKotaDto");
					BindUtils.postNotifyChange(null, null, this, "provinsiDto");
					BindUtils.postNotifyChange(null, null, this, "camatDto");
					BindUtils.postNotifyChange(null, null, this, "instansiPembuatDto");
					BindUtils.postNotifyChange(null, null, this, "sifatKecelakaanDto");
					BindUtils.postNotifyChange(null, null, this, "tglKejadian");
					BindUtils.postNotifyChange(null, null, this, "jamKejadian");
					BindUtils.postNotifyChange(null, null, this, "statusData");
					BindUtils.postNotifyChange(null, null, this, "instansiPembuatDto");
					BindUtils.postNotifyChange(null, null, this, "statusLaporanGroupBox");
					// update data BPM
					// String pattern = "dd-MM-yyyy";
					// SimpleDateFormat simpleDateFormat = new
					// SimpleDateFormat(pattern);
					//
					// String dateFormat = simpleDateFormat.format(new Date());
					// UserTaskService.updateUserTask(userSession.getLoginID(),
					// "welcome1", plDataKecelakaanDto.getIdKecelakaan(),
					// "Entry Data Pengajuan", "", "edit",
					// userSession.getKantor(), userSession.getLoginID(),
					// dateFormat);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Command("filter")
	public void filter() {
		sortAndSearch();
	}

	@Command
	public void add(@BindingParam("popup") String popup) {
		Map<String, Object> args = new HashMap<>();
		getPageInfo().setAddMode(true);
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
		// navigate(DETAIL_PAGE_PATH);
	}

	@GlobalCommand("addForRegister")
	public void addForRegister(@BindingParam("idKecelakaan") String idKecelakaan) {
		setFlagDataLakaRegister(true);
		plDataKecelakaanDto = new PlDataKecelakaanDto();
		plDataKecelakaanDto.setIdKecelakaan(idKecelakaan);
		BindUtils.postNotifyChange(null, null, this, "flagDataLakaRegister");
		getPageInfo().setEditMode(true);
		onEdit();
	}

	@GlobalCommand("addForSantunan")
	public void addForSantunan(@BindingParam("idKecelakaan") String idKecelakaan) {
		setFlagDataLakaSantunan(true);
		plDataKecelakaanDto = new PlDataKecelakaanDto();
		plDataKecelakaanDto.setIdKecelakaan(idKecelakaan);
		System.err.println("luthfi2191 " + plDataKecelakaanDto.getIdKecelakaan());
		BindUtils.postNotifyChange(null, null, this, "flagDataLakaSantunan");
		getPageInfo().setEditMode(true);
		onEdit();
	}

	public void onAdd() {
		tabDataLaka();
		instansiPembuatDto.setRvLowValue("1");
		for (DasiJrRefCodeDto dasi : listInstansiPembuatDto) {
			if (instansiPembuatDto.getRvLowValue().equalsIgnoreCase(dasi.getRvLowValue())) {
				instansiPembuatDto = new DasiJrRefCodeDto();
				instansiPembuatDto = dasi;
			}
		}
		setStatusData("ra1");
		instansiPembuat();
		BindUtils.postNotifyChange(null, null, this, "instansiPembuatDto");
		BindUtils.postNotifyChange(null, null, this, "statusData");
	}

	@Command
	public void konfirmasiNihil(@BindingParam("popup") String popup) {
		Map<String, Object> args = new HashMap<>();
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	@GlobalCommand
	public void nihilLaka2() {
		setFlagNihilRegister(true);
		BindUtils.postNotifyChange(null, null, this, "flagNihilRegister");
	}

	@Command
	public void addLampiran() {
		navigate(LAMPIRAN_PAGE_PATH);
	}

	@Command
	public void editAngkutan(@BindingParam("item") final PlAngkutanKecelakaanDto selected) {
		if (selected == null || selected.getIdAngkutanKecelakaan() == null) {
			showSmartMsgBox("W001");
			return;
		} else {
			getPageInfo().setCustomAttribute("editKendaraan");
			Map<String, Object> map = new HashMap<>();
			map.put("idAngkutan", selected.getIdAngkutanKecelakaan());
			RestResponse rest = callWs(WS_URI + "/angkutanLakaByIdAngkutan", map, HttpMethod.POST);
			tabKendaraanTerlibat();
			try {
				listPlAngkutanKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(),
						PlAngkutanKecelakaanDto.class);
				for (PlAngkutanKecelakaanDto a : listPlAngkutanKecelakaanDtos) {
					plAngkutanKecelakaanDto.setNoPolisi(a.getNoPolisi() == null ? "" : a.getNoPolisi());
					plAngkutanKecelakaanDto.setIdAngkutanKecelakaan(selected.getIdAngkutanKecelakaan());
					plAngkutanKecelakaanDto.setIdKecelakaan(plDataKecelakaanDto.getIdKecelakaan());
					jenisKendaraanDto.setKodeNama(a.getJenisDesc());
					statusKendaraanDto.setRvMeaning(a.getStatusDesc() == null ? "" : a.getStatusDesc());
					plAngkutanKecelakaanDto.setNamaPengemudi(a.getNamaPengemudi() == null ? "" : a.getNamaPengemudi());
					plAngkutanKecelakaanDto
							.setAlamatPengemudi(a.getAlamatPengemudi() == null ? "" : a.getAlamatPengemudi());
					jenisSimDto.setRvMeaning(a.getSimDesc() == null ? "" : a.getSimDesc());
					System.out.println("test " + a.getKodeJenisSim());
					System.out.println("test " + a.getNoPolisi());
					System.out.println("test " + a.getIdAngkutanKecelakaan());
					System.out.println("test " + a.getIdKecelakaan());
					System.out.println("test " + a.getJenisDesc());

					System.out.println("test " + a.getStatusDesc());
					System.out.println("test " + a.getNamaPemilik());
					System.out.println("test " + a.getAlamatPengemudi());
					System.out.println("test " + a.getNoSimPengemudi());
					System.out.println("test " + a.getAlamatPemilik());
					System.out.println("test " + a.getNamaPemilik());

					plAngkutanKecelakaanDto.setNoSimPengemudi(a.getNoSimPengemudi());
					plAngkutanKecelakaanDto.setMasaBerlakuSim(a.getMasaBerlakuSim());
					plAngkutanKecelakaanDto.setNamaPemilik(a.getNamaPemilik());
					plAngkutanKecelakaanDto.setAlamatPemilik(a.getAlamatPemilik());
					merkKendaraanDto.setRvMeaning(a.getMerkDesc());
					plAngkutanKecelakaanDto.setTahunPembuatan(a.getTahunPembuatan());
					plAngkutanKecelakaanDto.setKodePo(a.getKodePo());
				}
				for (DasiJrRefCodeDto jenisSim : listJenisSimDto) {
					if (jenisSimDto.getRvMeaning().equalsIgnoreCase(jenisSim.getRvMeaning())) {
						jenisSimDto = new DasiJrRefCodeDto();
						jenisSimDto = jenisSim;
					}
				}

				for (DasiJrRefCodeDto merk : listMerkKendaraanDto) {
					if (merkKendaraanDto.getRvMeaning().equalsIgnoreCase(merk.getRvMeaning())) {
						merkKendaraanDto = new DasiJrRefCodeDto();
						merkKendaraanDto = merk;
					}
				}
				for (DasiJrRefCodeDto status : listStatusKendaraanDto) {
					if (statusKendaraanDto.getRvMeaning().equalsIgnoreCase(status.getRvMeaning())) {
						statusKendaraanDto = new DasiJrRefCodeDto();
						statusKendaraanDto = status;
					}
				}
				for (FndJenisKendaraanDto jenis : listJenisKendaraanDto) {
					if (jenisKendaraanDto.getKodeNama().equalsIgnoreCase(jenis.getKodeNama())) {
						jenisKendaraanDto = new FndJenisKendaraanDto();
						jenisKendaraanDto = jenis;
					}
				}
				BindUtils.postNotifyChange(null, null, this, "plAngkutanKecelakaanDto");
				BindUtils.postNotifyChange(null, null, this, "merkKendaraanDto");
				BindUtils.postNotifyChange(null, null, this, "jenisSimDto");
				BindUtils.postNotifyChange(null, null, this, "statusKendaraanDto");
				BindUtils.postNotifyChange(null, null, this, "jenisKendaraanDto");

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Command
	public void test() throws ParseException {
		System.out.println("a " + asalBerkasDto.getKodeKantorJr());
		System.out.println("a " + instansiDto.getKodeInstansi());
		System.out.println("a " + samsatDto.getKodeKantorJr());
		System.out.println("a " + camatDto.getKodeCamat());
		Date a = getTglKejadian();
		Date b = getJamKejadian();
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat time = new SimpleDateFormat("HH:mm");
		String tgl = date.format(a);
		String jam = time.format(b);
		String tglKejadian = tgl + " " + jam;
		Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(tglKejadian);
		getLokasiByCamat();
		System.out.println("a " + plMappingCamatDto.getKodeLokasi());
		System.out.println("a " + plDataKecelakaanDto.getTglLaporanPolisi());
		System.out.println("a " + plDataKecelakaanDto.getNamaPetugas());
		System.out.println("a " + date1);
		System.out.println("a " + plDataKecelakaanDto.getDeskripsiLokasi());
		System.out.println("a " + kasusKecelakaanDto.getRvLowValue());
		System.out.println("a " + sifatKecelakaanDto.getRvLowValue());
		System.out.println("a " + plDataKecelakaanDto.getDeskripsiKecelakaan());
		System.out.println("a " + plDataKecelakaanDto.getGpsLu());
		System.out.println("a " + plDataKecelakaanDto.getGpsLs());
		System.out.println("a " + getLaporan1() + getLaporan2() + getLaporan3());
	}

	@Command
	public void saveAngkutanKecelakaan() throws ParseException {
		RestResponse restResponse = null;

		if (getPageInfo().getCustomAttribute() != null
				&& getPageInfo().getCustomAttribute().equalsIgnoreCase("addKendaraan")) {
			try {

				String idKecelakaan = plDataKecelakaanDto.getIdKecelakaan();

				Map<String, Object> map = new HashMap<>();
				map.put("idKecelakaan", idKecelakaan);

				RestResponse rest = callWs(WS_URI + "/angkutanLakaById", map, HttpMethod.POST);
				String idAngkutan = null;
				listPlAngkutanKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(),
						PlAngkutanKecelakaanDto.class);
				if (listPlAngkutanKecelakaanDtos.size() != 0) {
					for (PlAngkutanKecelakaanDto a : listPlAngkutanKecelakaanDtos) {
						idAngkutan = a.getIdAngkutanKecelakaan();
					}
					if (idAngkutan != null) {
						String incVal = idAngkutan.substring(30, 33);
						int countVal = Integer.valueOf(incVal) + 1;
						idAngkutan = idKecelakaan + "." + String.format("%03d", countVal);
						plAngkutanKecelakaanDto.setIdAngkutanKecelakaan(idAngkutan);
					}
				} else {
					idAngkutan = idKecelakaan + "." + "001";
					plAngkutanKecelakaanDto.setIdAngkutanKecelakaan(idAngkutan);
				}
				plAngkutanKecelakaanDto.setIdKecelakaan(plDataKecelakaanDto.getIdKecelakaan());
				plAngkutanKecelakaanDto.setAlamatPemilik(plAngkutanKecelakaanDto.getAlamatPemilik());
				plAngkutanKecelakaanDto.setAlamatPengemudi(plAngkutanKecelakaanDto.getAlamatPengemudi());
				plAngkutanKecelakaanDto.setCreatedBy(userSession.getLoginID());
				plAngkutanKecelakaanDto.setCreationDate(new Date());
				plAngkutanKecelakaanDto.setNoPolisi(plAngkutanKecelakaanDto.getNoPolisi());
				plAngkutanKecelakaanDto.setKodeJenis(jenisKendaraanDto.getKodeJenis());
				plAngkutanKecelakaanDto.setKodeMerk(merkKendaraanDto.getRvLowValue());
				plAngkutanKecelakaanDto.setMasaBerlakuSim(plAngkutanKecelakaanDto.getMasaBerlakuSim());
				plAngkutanKecelakaanDto.setKodePo(plAngkutanKecelakaanDto.getKodePo());
				plAngkutanKecelakaanDto.setTahunPembuatan(plAngkutanKecelakaanDto.getTahunPembuatan());
				plAngkutanKecelakaanDto.setNoSimPengemudi(plAngkutanKecelakaanDto.getNoSimPengemudi());
				plAngkutanKecelakaanDto.setNamaPemilik(plAngkutanKecelakaanDto.getNamaPemilik());
				plAngkutanKecelakaanDto.setNamaPengemudi(plAngkutanKecelakaanDto.getNamaPengemudi());
				plAngkutanKecelakaanDto.setStatusKendaraan(statusKendaraanDto.getRvLowValue());
				plAngkutanKecelakaanDto.setKodeJenisSim(jenisSimDto.getRvLowValue());

				restResponse = callWs(WS_URI + "/saveAngkutanKecelakaan", plAngkutanKecelakaanDto, HttpMethod.POST);
				if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
					showInfoMsgBox(restResponse.getMessage());
					setListKendaraan(true);
					setFormTambahKendaraan(false);
					getListKendaraanById();
					BindUtils.postNotifyChange(null, null, this, "formTambahKendaraan");
					BindUtils.postNotifyChange(null, null, this, "listKendaraan");
					setFlagAddDataKendaraan(false);
					BindUtils.postNotifyChange(null, null, this, "flagAddDataKendaraan");

				} else {
					showErrorMsgBox(restResponse.getMessage());
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			test2();
			plAngkutanKecelakaanDto.setAlamatPengemudi(plAngkutanKecelakaanDto.getAlamatPengemudi());
			plAngkutanKecelakaanDto.setLastUpdatedBy(userSession.getLoginID());
			plAngkutanKecelakaanDto.setLastUpdatedDate(new Date());
			plAngkutanKecelakaanDto.setNoPolisi(plAngkutanKecelakaanDto.getNoPolisi());
			plAngkutanKecelakaanDto.setKodeJenis(jenisKendaraanDto.getKodeJenis());
			plAngkutanKecelakaanDto.setKodeMerk(merkKendaraanDto.getRvLowValue());
			plAngkutanKecelakaanDto.setMasaBerlakuSim(plAngkutanKecelakaanDto.getMasaBerlakuSim());
			plAngkutanKecelakaanDto.setKodePo(plAngkutanKecelakaanDto.getKodePo());
			plAngkutanKecelakaanDto.setTahunPembuatan(plAngkutanKecelakaanDto.getTahunPembuatan());
			plAngkutanKecelakaanDto.setNoSimPengemudi(plAngkutanKecelakaanDto.getNoSimPengemudi());
			plAngkutanKecelakaanDto.setNamaPemilik(plAngkutanKecelakaanDto.getNamaPemilik());
			plAngkutanKecelakaanDto.setNamaPengemudi(plAngkutanKecelakaanDto.getNamaPengemudi());
			plAngkutanKecelakaanDto.setStatusKendaraan(statusKendaraanDto.getRvLowValue());
			plAngkutanKecelakaanDto.setKodeJenisSim(jenisSimDto.getRvLowValue());

			restResponse = callWs(WS_URI + "/saveAngkutanKecelakaan", plAngkutanKecelakaanDto, HttpMethod.POST);

			if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(restResponse.getMessage());
				setListKendaraan(true);
				setFormTambahKendaraan(false);
				getListKendaraanById();
				BindUtils.postNotifyChange(null, null, this, "formTambahKendaraan");
				BindUtils.postNotifyChange(null, null, this, "listKendaraan");
				setFlagAddDataKendaraan(false);
				BindUtils.postNotifyChange(null, null, this, "flagAddDataKendaraan");

			} else {
				showErrorMsgBox(restResponse.getMessage());
			}
		}
	}

	@Command
	public void test2() {
		System.out.println("a " + plAngkutanKecelakaanDto.getIdKecelakaan());
		System.out.println("a " + plAngkutanKecelakaanDto.getIdAngkutanKecelakaan());
		System.out.println("a " + plAngkutanKecelakaanDto.getNoPolisi());
		System.out.println("a " + jenisKendaraanDto.getKodeJenis());
		System.out.println("a " + statusKendaraanDto.getRvLowValue());
		System.out.println("a " + plAngkutanKecelakaanDto.getNamaPengemudi());
		System.out.println("a " + plAngkutanKecelakaanDto.getAlamatPengemudi());
		System.out.println("a " + jenisSimDto.getRvLowValue());
		System.out.println("a " + plAngkutanKecelakaanDto.getNoSimPengemudi());
		System.out.println("a " + plAngkutanKecelakaanDto.getMasaBerlakuSim());
		System.out.println("a " + plAngkutanKecelakaanDto.getNamaPemilik());
		System.out.println("a " + plAngkutanKecelakaanDto.getAlamatPemilik());
		System.out.println("a " + merkKendaraanDto.getRvLowValue());
		System.out.println("a " + plAngkutanKecelakaanDto.getTahunPembuatan());
		System.out.println("a " + plAngkutanKecelakaanDto.getKodePo());
	}

	@Command
	public void saveKorbanKecelakaan() {
		RestResponse restResponse = null;

		if (getPageInfo().getCustomAttribute() != null
				&& getPageInfo().getCustomAttribute().equalsIgnoreCase("addKorban")) {
			try {

				String idKecelakaan = plDataKecelakaanDto.getIdKecelakaan();

				Map<String, Object> map = new HashMap<>();
				map.put("idKecelakaan", idKecelakaan);

				RestResponse rest = callWs(WS_URI + "/korbanLakaById", map, HttpMethod.POST);

				String idKorban = null;
				listPlKorbanKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(),
						PlKorbanKecelakaanDto.class);
				if (listPlKorbanKecelakaanDtos.size() != 0) {
					for (PlKorbanKecelakaanDto a : listPlKorbanKecelakaanDtos) {
						idKorban = a.getIdKorbanKecelakaan();
					}
					if (idKorban != null) {
						String incVal = idKorban.substring(30, 33);
						int countVal = Integer.valueOf(incVal) + 1;
						idKorban = idKecelakaan + "." + String.format("%03d", countVal);
						plKorbanKecelakaanDto.setIdKorbanKecelakaan(idKorban);
					}
				} else {
					idKorban = idKecelakaan + "." + "001";
					plKorbanKecelakaanDto.setIdKorbanKecelakaan(idKorban);
				}
				getLokasiByCamatKorban();
				plKorbanKecelakaanDto.setIdKecelakaan(idKecelakaan);
				plKorbanKecelakaanDto.setAlamat(plKorbanKecelakaanDto.getAlamat());
				plKorbanKecelakaanDto.setCreatedBy(userSession.getLoginID());
				plKorbanKecelakaanDto.setCreationDate(new Date());
				plKorbanKecelakaanDto.setIdAngkutanKecelakaan(kendaraanPosisiDto.getIdAngkutanKecelakaan());
				plKorbanKecelakaanDto.setIdAngkutanPenanggung(kendaraanPenjaminDto.getIdAngkutanKecelakaan());
				plKorbanKecelakaanDto.setIdGuid(plMappingCamatDto.getKodeLokasi());
				plKorbanKecelakaanDto.setJenisIdentitas(jenisIdentitasDto.getRvLowValue());
				if (plKorbanKecelakaanDto.getJenisKelamin().equalsIgnoreCase("Pria")) {
					plKorbanKecelakaanDto.setJenisKelamin("P");
				} else if (plKorbanKecelakaanDto.getJenisKelamin().equalsIgnoreCase("Wanita")) {
					plKorbanKecelakaanDto.setJenisKelamin("W");
				}
				plKorbanKecelakaanDto.setUmur(plKorbanKecelakaanDto.getUmur());
				plKorbanKecelakaanDto.setKodeJaminan(jenisPertanggunganDto.getKodeJaminan());
				plKorbanKecelakaanDto.setKodePekerjaan(jenisPekerjaanDto.getRvLowValue());
				plKorbanKecelakaanDto.setKodeStatusKorban(statusKorbanDto.getRvLowValue());
				plKorbanKecelakaanDto.setStatusNikah(statusNikahDto.getRvLowValue());
				plKorbanKecelakaanDto.setKodeSifatCidera(sifatCideraDto.getRvLowValue());
				plKorbanKecelakaanDto.setNoTelp(plKorbanKecelakaanDto.getNoTelp());

				restResponse = callWs(WS_URI + "/saveKorbanKecelakaan", plKorbanKecelakaanDto, HttpMethod.POST);

				if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
					showInfoMsgBox(restResponse.getMessage());
					String pattern = "dd-MM-yyyy";
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

					String dateFormat = simpleDateFormat.format(new Date());
					System.out.println(dateFormat);
					System.out.println("Kantor : " + userSession.getKantor());
					System.out.println("Login ID : " + userSession.getLoginID());
					String kantor = userSession.getKantor();
					String login = userSession.getLoginID();
					String levelKantor = new String();

					if (userSession.getLevelKantor().equalsIgnoreCase("CA")) {
						levelKantor = "AB";
					} else if (userSession.getLevelKantor().equalsIgnoreCase("CB")) {
						levelKantor = "AB";
					} else if (userSession.getLevelKantor().equalsIgnoreCase("CC")) {
						levelKantor = "C";
					} else {
						levelKantor = "perwakilan";
					}
					// startBPM(levelKantor,
					// plKorbanKecelakaanDto.getIdKorbanKecelakaan(),"01",login,
					// dateFormat, login, dateFormat, "N", "", kantor, "","","",
					// plDataKecelakaanDto.getIdKecelakaan(),"" );
					showKorban();
				} else {
					showErrorMsgBox(restResponse.getMessage());
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.err.println("luthfi masuk edit korban");
			test3();
			getLokasiByCamatKorban();
			plKorbanKecelakaanDto.setNoTelp(plKorbanKecelakaanDto.getNoTelp());
			plKorbanKecelakaanDto.setAlamat(plKorbanKecelakaanDto.getAlamat());
			plKorbanKecelakaanDto.setLastUpdatedBy(userSession.getLoginID());
			plKorbanKecelakaanDto.setLastUpdatedDate(new Date());
			plKorbanKecelakaanDto.setIdAngkutanKecelakaan(kendaraanPenjaminDto.getIdAngkutanKecelakaan());
			plKorbanKecelakaanDto.setIdAngkutanPenanggung(kendaraanPosisiDto.getIdAngkutanKecelakaan());
			plKorbanKecelakaanDto.setIdGuid(plMappingCamatDto.getKodeLokasi());
			plKorbanKecelakaanDto.setJenisIdentitas(jenisIdentitasDto.getRvLowValue());
			if (plKorbanKecelakaanDto.getJenisKelamin().equalsIgnoreCase("Pria")) {
				plKorbanKecelakaanDto.setJenisKelamin("P");
			} else if (plKorbanKecelakaanDto.getJenisKelamin().equalsIgnoreCase("Wanita")) {
				plKorbanKecelakaanDto.setJenisKelamin("W");
			}
			plKorbanKecelakaanDto.setUmur(plKorbanKecelakaanDto.getUmur());
			plKorbanKecelakaanDto.setKodeJaminan(jenisPertanggunganDto.getKodeJaminan());
			plKorbanKecelakaanDto.setKodePekerjaan(jenisPekerjaanDto.getRvLowValue());
			plKorbanKecelakaanDto.setKodeStatusKorban(statusKorbanDto.getRvLowValue());
			plKorbanKecelakaanDto.setStatusNikah(statusNikahDto.getRvLowValue());
			plKorbanKecelakaanDto.setKodeSifatCidera(sifatCideraDto.getRvLowValue());

			restResponse = callWs(WS_URI + "/saveKorbanKecelakaan", plKorbanKecelakaanDto, HttpMethod.POST);

			if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(restResponse.getMessage());
				showKorban();
			} else {
				showErrorMsgBox(restResponse.getMessage());
			}
		}
	}

	@Command
	public void editKorban(@BindingParam("item") final PlKorbanKecelakaanDto selected) {
		if (selected == null || selected.getIdKorbanKecelakaan() == null) {
			showSmartMsgBox("W001");
			return;
		} else {
			getPageInfo().setCustomAttribute("editKorban");
			Map<String, Object> map = new HashMap<>();
			map.put("idKorban", selected.getIdKorbanKecelakaan());
			RestResponse rest = callWs(WS_URI + "/korbanLakaByIdKorban", map, HttpMethod.POST);
			tabKorban();
			try {
				listPlKorbanKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(),
						PlKorbanKecelakaanDto.class);

				for (PlKorbanKecelakaanDto a : listPlKorbanKecelakaanDtos) {
					plKorbanKecelakaanDto.setIdKecelakaan(plDataKecelakaanDto.getIdKecelakaan());
					plKorbanKecelakaanDto.setIdKorbanKecelakaan(selected.getIdKorbanKecelakaan());
					jenisIdentitasDto.setRvMeaning(a.getJenisIdentitasDesc());
					plKorbanKecelakaanDto.setNoIdentitas(a.getNoIdentitas());
					plKorbanKecelakaanDto.setNama(a.getNama());
					if (a.getJenisKelamin().equalsIgnoreCase("P")) {
						plKorbanKecelakaanDto.setJenisKelamin("Pria");
					} else if (a.getJenisKelamin().equalsIgnoreCase("W")) {
						plKorbanKecelakaanDto.setJenisKelamin("Wanita");
					}
					provinsiKorbanDto.setKodeProvinsi(a.getKodeProvinsi() == null ? "" : a.getKodeProvinsi());
					provinsiKorbanDto.setNamaProvinsi(a.getNamaProvinsi() == null ? "" : a.getNamaProvinsi());
					kabKotaKorbanDto.setKodeKabkota(a.getKodeKabkota() == null ? "" : a.getKodeKabkota());
					kabKotaKorbanDto.setNamaKabkota(a.getNamaKabkota() == null ? "" : a.getNamaKabkota());
					camatDtoKorban.setKodeCamat(a.getKodeCamat() == null ? "" : a.getKodeCamat());
					camatDtoKorban.setNamaCamat(a.getNamaCamat() == null ? "" : a.getNamaCamat());
					plKorbanKecelakaanDto.setUmur(a.getUmur() == null ? null : a.getUmur());
					plKorbanKecelakaanDto.setAlamat(a.getAlamat() == null ? "" : a.getAlamat());
					jenisPekerjaanDto.setRvMeaning(a.getPekerjaanDesc() == null ? "" : a.getPekerjaanDesc());
					sifatCideraDto.setRvMeaning(a.getCideraDesc() == null ? "" : a.getCideraDesc());
//					kendaraanPenjaminDto.setNoPlatNamaPengemudiDesc(a.getPenjaminDesc() == null ? "" : a.getPenjaminDesc());
//					kendaraanPosisiDto.setNoPlatNamaPengemudiDesc(a.getPosisiDesc() == null ? "" : a.getPosisiDesc());
					kendaraanPenjaminDto.setIdAngkutanKecelakaan(
							a.getIdAngkutanPenanggung() == null ? "" : a.getIdAngkutanPenanggung());
					kendaraanPosisiDto.setIdAngkutanKecelakaan(
							a.getIdAngkutanKecelakaan() == null ? "" : a.getIdAngkutanKecelakaan());
					statusKorbanDto.setRvMeaning(a.getStatusKorbanDesc() == null ? "" : a.getStatusKorbanDesc());
					jenisPertanggunganDto.setPertanggungan(a.getKodeJaminan() == null ? "" : a.getKodeJaminan());
					statusNikahDto.setRvMeaning(a.getStatusNikahDesc() == null ? "" : a.getStatusNikahDesc());
					plKorbanKecelakaanDto.setNoTelp(a.getNoTelp() == null ? "" : a.getNoTelp());
				}
				for (FndCamatDto provinsi : listProvinsiKorbanDto) {
					if (provinsiKorbanDto.getNamaProvinsi().equalsIgnoreCase(provinsi.getNamaProvinsi())) {
						provinsiKorbanDto = new FndCamatDto();
						provinsiKorbanDto = provinsi;
					}
				}
				getKabkotaKorban();
				for (FndCamatDto kabKota : listKabKotaKorbanDto) {
					if (kabKotaKorbanDto.getNamaKabkota().equalsIgnoreCase(kabKota.getNamaKabkota())) {
						kabKotaKorbanDto = new FndCamatDto();
						kabKotaKorbanDto = kabKota;
					}
				}
				getCamatKorban();
				for (FndCamatDto camat : listCamatKorbanDto) {
					if (camatDtoKorban.getNamaCamat().equalsIgnoreCase(camat.getNamaCamat())) {
						camatDtoKorban = new FndCamatDto();
						camatDtoKorban = camat;
					}
				}

				for (DasiJrRefCodeDto jenisPekerjaan : listJenisPekerjaanDto) {
					if (jenisPekerjaanDto.getRvMeaning().equalsIgnoreCase(jenisPekerjaan.getRvMeaning())) {
						jenisPekerjaanDto = new DasiJrRefCodeDto();
						jenisPekerjaanDto = jenisPekerjaan;
					}
				}
				for (DasiJrRefCodeDto sifatCidera : listSifatCideraDto) {
					if (sifatCideraDto.getRvMeaning().equalsIgnoreCase(sifatCidera.getRvMeaning())) {
						sifatCideraDto = new DasiJrRefCodeDto();
						sifatCideraDto = sifatCidera;
					}
				}
				for (PlAngkutanKecelakaanDto penjamin : listPlAngkutanKecelakaanDtos) {
					if (kendaraanPenjaminDto.getIdAngkutanKecelakaan()
							.equalsIgnoreCase(penjamin.getIdAngkutanKecelakaan())) {
						kendaraanPenjaminDto = new PlAngkutanKecelakaanDto();
						kendaraanPenjaminDto = penjamin;
					}
				}
				for (PlAngkutanKecelakaanDto posisi : listPlAngkutanKecelakaanDtos) {
					if (kendaraanPosisiDto.getIdAngkutanKecelakaan()
							.equalsIgnoreCase(posisi.getIdAngkutanKecelakaan())) {
						kendaraanPosisiDto = new PlAngkutanKecelakaanDto();
						kendaraanPosisiDto = posisi;
					}
				}
				for (DasiJrRefCodeDto jenisIdentitas : listIdentitasDto) {
					if (jenisIdentitasDto.getRvMeaning().equalsIgnoreCase(jenisIdentitas.getRvMeaning())) {
						jenisIdentitasDto = new DasiJrRefCodeDto();
						jenisIdentitasDto = jenisIdentitas;
					}
				}
				for (DasiJrRefCodeDto statusNikah : listStatusNikahDto) {
					if (statusNikahDto.getRvMeaning().equalsIgnoreCase(statusNikah.getRvMeaning())) {
						statusNikahDto = new DasiJrRefCodeDto();
						statusNikahDto = statusNikah;
					}
				}
				for (DasiJrRefCodeDto statusKorban : listStatusKorbanDto) {
					if (statusKorbanDto.getRvMeaning().equalsIgnoreCase(statusKorban.getRvMeaning())) {
						statusKorbanDto = new DasiJrRefCodeDto();
						statusKorbanDto = statusKorban;
					}
				}
				for (PlJaminanDto jenisPertanggungan : listJenisPertanggunganDto) {
					if (jenisPertanggunganDto.getPertanggungan()
							.equalsIgnoreCase(jenisPertanggungan.getKodeJaminan())) {
						jenisPertanggunganDto = new PlJaminanDto();
						jenisPertanggunganDto = jenisPertanggungan;
					}
				}

				BindUtils.postNotifyChange(null, null, this, "jenisIdentitasDto");
				BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");
				BindUtils.postNotifyChange(null, null, this, "jenisPekerjaanDto");
				BindUtils.postNotifyChange(null, null, this, "plKorbanKecelakaanDto");
				BindUtils.postNotifyChange(null, null, this, "kendaraanPosisiDto");
				BindUtils.postNotifyChange(null, null, this, "kendaraanPenjaminDto");
				BindUtils.postNotifyChange(null, null, this, "statusNikahDto");
				BindUtils.postNotifyChange(null, null, this, "jenisPertanggunganDto");
				BindUtils.postNotifyChange(null, null, this, "statusKorbanDto");
				BindUtils.postNotifyChange(null, null, this, "provinsiKorbanDto");
				BindUtils.postNotifyChange(null, null, this, "kabKotaKorbanDto");
				BindUtils.postNotifyChange(null, null, this, "camatDtoKorban");

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Command
	public void test3() {
		System.out.println("a " + jenisIdentitasDto.getRvLowValue());
		System.out.println("a " + plKorbanKecelakaanDto.getNoIdentitas());
		System.out.println("a " + plKorbanKecelakaanDto.getIdKorbanKecelakaan());
		System.out.println("a " + plKorbanKecelakaanDto.getIdKecelakaan());
		System.out.println("a " + plKorbanKecelakaanDto.getNama());
		System.out.println("a " + plKorbanKecelakaanDto.getJenisKelamin());
		System.out.println("a " + plKorbanKecelakaanDto.getUmur());
		System.out.println("a " + plKorbanKecelakaanDto.getAlamat());
		System.out.println("a " + jenisPekerjaanDto.getRvLowValue());
		System.out.println("a " + sifatCideraDto.getRvLowValue());
		// System.out.println("a " + plAngkutanKecelakaanDto.getNoPolisi());
		// System.out.println("a " + plAngkutanKecelakaanDto.getNoPolisi());
		System.out.println("a " + statusKorbanDto.getRvLowValue());
		System.out.println("a " + jenisPertanggunganDto.getKodeJaminan());
		System.out.println("a " + statusNikahDto.getRvLowValue());
		System.out.println("a " + plKorbanKecelakaanDto.getNoTelp());
	}

	@Command("saveDataLaka")
	public void saveDataLaka() throws ParseException {
		RestResponse restResponse = null;
		cekConstraint();
		if (plDataKecelakaanDto.getTglLaporanPolisi() == null) {
			showInfoMsgBox("Tgl laporan wajib diisi !", "");
			return;
		}

		if (tglKejadian == null) {
			showInfoMsgBox("Tgl kejadian wajib diisi !", "");
			return;
		}

		if (plDataKecelakaanDto.getTglLaporanPolisi() != null && tglKejadian != null) {
			if (plDataKecelakaanDto.getTglLaporanPolisi().before(tglKejadian) == true) {
				showErrorMsgBox("Tgl Kejadian tidak boleh lebih dari Tgl Laporan", "");
				return;
			}
		}

		getLokasiByCamat();
		if (plMappingCamatDto == null || plMappingCamatDto.getKodeLokasi().equalsIgnoreCase("")) {
			showInfoMsgBox("Lokasi kejadian wajib diisi !", "");
			return;

		} else {

			if (getPageInfo().isAddMode()) {
				try {
					DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					String strDate = format.format(new Date());
					String creationDate = strDate.substring(8, 10) + strDate.substring(3, 5) + strDate.substring(0, 2)
							+ strDate.substring(11, 13) + strDate.substring(14, 16);
					String idKecelakaan = plMappingCamatDto.getKodeLokasi() + "." + instansiDto.getKodeInstansi() + "."
							+ creationDate;
					Map<String, Object> map = new HashMap<>();
					map.put("idKecelakaan", idKecelakaan);
					RestResponse rest = callWs(WS_URI + "/dataLakaById", map, HttpMethod.POST);

					if (plDataKecelakaanDto.getIdKecelakaan() == null) {
						String idLaka = null;
						BigDecimal noUrut = null;
						listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(),
								PlDataKecelakaanDto.class);
						if (listPlDataKecelakaanDtos.size() != 0) {
							for (PlDataKecelakaanDto a : listPlDataKecelakaanDtos) {
								idLaka = a.getIdKecelakaan();
							}
							if (idLaka != null) {
								String incVal = idLaka.substring(27, 29);
								int countVal = Integer.valueOf(incVal) + 1;
								noUrut = BigDecimal.valueOf(countVal);
								idKecelakaan = plDataKecelakaanDto.getKodeLokasi() + "."
										+ plDataKecelakaanDto.getKodeInstansi() + "." + creationDate + "."
										+ String.format("%02d", countVal);
								plDataKecelakaanDto.setIdKecelakaan(idKecelakaan);
								plDataKecelakaanDto.setNoUrut(noUrut);
							}
						} else {
							idKecelakaan = plMappingCamatDto.getKodeLokasi() + "." + instansiDto.getKodeInstansi() + "."
									+ creationDate + "." + "01";
							noUrut = new BigDecimal("01");
							plDataKecelakaanDto.setIdKecelakaan(idKecelakaan);
							plDataKecelakaanDto.setNoUrut(noUrut);
						}
					} else {
						plDataKecelakaanDto.setIdKecelakaan(plDataKecelakaanDto.getIdKecelakaan());
						plDataKecelakaanDto.setNoUrut(plDataKecelakaanDto.getNoUrut());
					}

					plDataKecelakaanDto.setKodeKantorJr(userSession.getKantor());
					plDataKecelakaanDto.setAsalBerkas(asalBerkasDto.getKodeKantorJr());
					plDataKecelakaanDto.setKodeInstansi(instansiDto.getKodeInstansi());
					plDataKecelakaanDto.setKodeLokasi(plMappingCamatDto.getKodeLokasi());
					plDataKecelakaanDto.setKodeWilayah(samsatDto.getKodeKantorJr());
					plDataKecelakaanDto.setTglLaporanPolisi(plDataKecelakaanDto.getTglLaporanPolisi());
					plDataKecelakaanDto.setDeskripsiLokasi(plDataKecelakaanDto.getDeskripsiLokasi());
					plDataKecelakaanDto.setDeskripsiKecelakaan(plDataKecelakaanDto.getDeskripsiKecelakaan());
					plDataKecelakaanDto.setGpsLu(plDataKecelakaanDto.getGpsLu());
					plDataKecelakaanDto.setGpsLs(plDataKecelakaanDto.getGpsLs());
					plDataKecelakaanDto.setCreatedBy(userSession.getLoginID());
					plDataKecelakaanDto.setCreationDate(new Date());
					plDataKecelakaanDto.setKodeKasusKecelakaan(kasusKecelakaanDto.getRvLowValue());
					plDataKecelakaanDto.setSifatKecelakaan(sifatKecelakaanDto.getRvLowValue());
					plDataKecelakaanDto.setStatusTransisi(instansiPembuatDto.getRvLowValue());
					if (plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("1")) {
						plDataKecelakaanDto.setNoLaporanPolisi(getLaporan1() + getLaporan2() + getLaporan3());
					} else if (plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("2")
							|| plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("3")
							|| plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("4")
							|| plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("5")) {
						plDataKecelakaanDto.setNoLaporanPolisi(getLaporan2());
					} else if (plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("6")) {
						plDataKecelakaanDto.setNoLaporanPolisi(getLaporan1() + getLaporan2());
					}
					plDataKecelakaanDto.setIdGuid("N");
					Date b = null;
					if (getJamKejadian() == null) {
						b = new Date();
					} else {
						b = getJamKejadian();
					}
					Date a = getTglKejadian();
					SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
					SimpleDateFormat time = new SimpleDateFormat("HH:mm");

					String tgl = date.format(a);
					String jam = time.format(b);
					String tglKejadian = tgl + " " + jam;
					Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(tglKejadian);
					plDataKecelakaanDto.setTglKejadian(date1);
					if (statusData.equalsIgnoreCase("ra1")) {
						plDataKecelakaanDto.setStatusLaporanPolisi("Y");
					} else if (statusData.equalsIgnoreCase("ra2")) {
						plDataKecelakaanDto.setStatusLaporanPolisi("S");
					} else if (statusData.equalsIgnoreCase("ra3")) {
						plDataKecelakaanDto.setStatusLaporanPolisi("");
					} else if (statusData.equalsIgnoreCase("ra4")) {
						plDataKecelakaanDto.setStatusLaporanPolisi("N");
					}

					BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
					restResponse = callWs(WS_URI + "/saveDataLaka", plDataKecelakaanDto, HttpMethod.POST);

					if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
						showInfoMsgBox(restResponse.getMessage());
						setFlagSaveDataLaka(true);
						BindUtils.postNotifyChange(null, null, this, "flagSaveDataLaka");
						String pattern = "dd-MM-yyyy";
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

						String dateFormat = simpleDateFormat.format(new Date());
						System.out.println(dateFormat);
						System.out.println("Kantor : " + userSession.getKantor());
						System.out.println("Login ID : " + userSession.getLoginID());
						String kantor = userSession.getKantor();
						String login = userSession.getLoginID();
						String levelKantor = new String();

						if (userSession.getLevelKantor().equalsIgnoreCase("CA")) {
							levelKantor = "AB";
						} else if (userSession.getLevelKantor().equalsIgnoreCase("CB")) {
							levelKantor = "AB";
						} else if (userSession.getLevelKantor().equalsIgnoreCase("CC")) {
							levelKantor = "C";
						} else {
							levelKantor = "perwakilan";
						}
						// startBPM(levelKantor,
						// plDataKecelakaanDto.getIdKecelakaan(),"01",login,
						// dateFormat, login, dateFormat, "N", "", kantor,
						// "","","","","" );
					} else {
						showErrorMsgBox(restResponse.getMessage());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				try {
					plDataKecelakaanDto.setNoUrut(plDataKecelakaanDto.getNoUrut());
					plDataKecelakaanDto.setKodeKantorJr(userSession.getKantor());
					plDataKecelakaanDto.setAsalBerkas(asalBerkasDto.getKodeKantorJr());
					plDataKecelakaanDto.setKodeInstansi(instansiDto.getKodeInstansi());
					plDataKecelakaanDto.setKodeLokasi(plMappingCamatDto.getKodeLokasi());
					plDataKecelakaanDto.setKodeWilayah(samsatDto.getKodeKantorJr());
					plDataKecelakaanDto.setTglLaporanPolisi(plDataKecelakaanDto.getTglLaporanPolisi());
					plDataKecelakaanDto.setStatusTransisi(instansiPembuatDto.getRvLowValue());
					if (plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("1")) {
						plDataKecelakaanDto.setNoLaporanPolisi(getLaporan1() + getLaporan2() + getLaporan3());
					} else if (plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("2")
							|| plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("3")
							|| plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("4")
							|| plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("5")) {
						plDataKecelakaanDto.setNoLaporanPolisi(getLaporan2());
					} else if (plDataKecelakaanDto.getStatusTransisi().equalsIgnoreCase("6")) {
						plDataKecelakaanDto.setNoLaporanPolisi(getLaporan1() + getLaporan2());
					}
					plDataKecelakaanDto.setDeskripsiLokasi(plDataKecelakaanDto.getDeskripsiLokasi());
					plDataKecelakaanDto.setDeskripsiKecelakaan(plDataKecelakaanDto.getDeskripsiKecelakaan());
					plDataKecelakaanDto.setGpsLu(plDataKecelakaanDto.getGpsLu());
					plDataKecelakaanDto.setGpsLs(plDataKecelakaanDto.getGpsLs());
					plDataKecelakaanDto.setKodeKasusKecelakaan(kasusKecelakaanDto.getRvLowValue());
					plDataKecelakaanDto.setLastUpdatedBy(userSession.getLoginID());
					plDataKecelakaanDto.setLastUpdatedDate(new Date());
					plDataKecelakaanDto.setSifatKecelakaan(sifatKecelakaanDto.getRvLowValue());
					plDataKecelakaanDto.setStatusTransisi(instansiPembuatDto.getRvLowValue());
					plDataKecelakaanDto.setIdGuid("N");

					Date a = getTglKejadian();
					Date b = getJamKejadian();
					SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
					SimpleDateFormat time = new SimpleDateFormat("HH:mm");
					String tgl = date.format(a);
					String jam = time.format(b);
					String tglKejadian = tgl + " " + jam;
					Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(tglKejadian);
					plDataKecelakaanDto.setTglKejadian(date1);
					if (statusData.equalsIgnoreCase("ra1")) {
						plDataKecelakaanDto.setStatusLaporanPolisi("Y");
					} else if (statusData.equalsIgnoreCase("ra2")) {
						plDataKecelakaanDto.setStatusLaporanPolisi("S");
					} else if (statusData.equalsIgnoreCase("ra3")) {
						plDataKecelakaanDto.setStatusLaporanPolisi("");
					} else if (statusData.equalsIgnoreCase("ra4")) {
						plDataKecelakaanDto.setStatusLaporanPolisi("N");
					}

					BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
					restResponse = callWs(WS_URI + "/saveDataLaka", plDataKecelakaanDto, HttpMethod.POST);

					if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
						showInfoMsgBox(restResponse.getMessage());
						setFlagSaveDataLaka(true);
						BindUtils.postNotifyChange(null, null, this, "flagSaveDataLaka");
					} else {
						showErrorMsgBox(restResponse.getMessage());
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void startBPM(String tipeCabang, String nomorPermohonan, String kodeCabang, String createdBy,
			String creationDate, String lastUpdatedBy, String lastUpdatedDate, String idGUID, String pengajuanType,
			String kodeKantor, String otherInfo1, String otherInfo2, String otherInfo3, String otherInfo4,
			String otherInfo5) {
		System.out.println("Level Kantor : " + userSession.getLevelKantor());

		Bpm_start_processRequest req = new Bpm_start_processRequest();
		req.setTipeCabang(tipeCabang);
		req.setNomorPermohonan(nomorPermohonan);
		req.setKodeCabang(kodeCabang);
		req.setCreatedBy(createdBy);
		req.setCreationDate(creationDate);
		req.setLastUpdatedBy(lastUpdatedBy);
		req.setLastUpdatedDate(lastUpdatedDate);
		req.setIdGUID(idGUID);
		req.setPengajuanType(pengajuanType);
		req.setKodeKantor(kodeKantor);
		req.setOtherInfo1(otherInfo1);
		req.setOtherInfo2(otherInfo2);
		req.setOtherInfo3(otherInfo3);
		req.setOtherInfo4(otherInfo4);
		req.setOtherInfo5(otherInfo5);

		// StartProcessSvcImpl startProcess = new StartProcessSvcImpl();
		try {

			StartBPMSvcImpl.bpmStartProcess(req);
			// System.out.println("Response Service BPM : " +
			// bpmStartProcess(req).getResponse().getResponseCode());
			// showInfoMsgBox("Response " +
			// bpmStartProcess(req).getResponse().getResponseCode());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	@Command
	public void saveNihilKecelakaan() {
		RestResponse restResponse = null;

		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String tgl = dateFormat.format(date).substring(0, 2);
		String bulan = dateFormat.format(date).substring(3, 5);
		String tahun = dateFormat.format(date).substring(6, 10);

		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 10) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();

		String idLakaNihil = userSession.getLoginID() + "." + tgl + bulan + tahun + saltStr;

		plNihilKecelakaanDto.setCreatedBy(userSession.getLoginID());
		plNihilKecelakaanDto.setCreationDate(date);
		plNihilKecelakaanDto.setIdLakaNhl(idLakaNihil);
		plNihilKecelakaanDto.setKodeInstansi(instansiDto.getKodeInstansi());
		plNihilKecelakaanDto.setKodeKantorJr(userSession.getKantor());
		plNihilKecelakaanDto.setLastUpdatedBy(userSession.getLoginID());
		plNihilKecelakaanDto.setLastUpdatedDate(date);
		plNihilKecelakaanDto.setStatusNhl("Y");
		plNihilKecelakaanDto.setTglKejadian(plDataKecelakaanDto.getTglKejadian());

		restResponse = callWs(WS_URI + "/saveNihilKecelakaan", plNihilKecelakaanDto, HttpMethod.POST);

		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
			if (isFlagNihilRegister() == true) {
				navigate(INDEX_REGISTER_PAGE_PATH);
			} else {
				navigate(INDEX_PAGE_PATH);
			}
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}

	@Command
	public void instansiPembuat() {
		setStatusLaporanGroupBox(true);
		setStatusLaporan("Status " + instansiPembuatDto.getRvMeaning());
//		setStatus(true);
//		setStatusData("ra1");
		BindUtils.postNotifyChange(null, null, this, "statusLaporanGroupBox");
		BindUtils.postNotifyChange(null, null, this, "statusLaporan");
//		BindUtils.postNotifyChange(null, null, this, "status");
//		BindUtils.postNotifyChange(null, null, this, "statusData");
	}

	public void tabDataLaka() {
//		searchAsalBerkas();
		loadAsalBerkas("%");
//		searchInstansi();
		loadInstansi("%");
//		searchSamsat();
		loadSamsat("%");
		searchProvinsi();
		getKabkota();
		getCamat();
		listInstansiPembuat();
		listKasusKecelakaan();
		listSifatKecelakaan();

	}

	public void tabKendaraanTerlibat() {
		if (getPageInfo().getCustomAttribute().equalsIgnoreCase("addKendaraan")) {
			// plAngkutanKecelakaanDto = new PlAngkutanKecelakaanDto();
			// listStatusKendaraanDto = new ArrayList<>();
			// listJenisSimDto = new ArrayList<>();
			// listMerkKendaraanDto = new ArrayList<>();
			// setFlagAddDataKendaraan(true);
			// BindUtils.postNotifyChange(null, null,
			// this,"plAngkutanKecelakaanDto");
			// BindUtils.postNotifyChange(null, null,
			// this,"flagAddDataKendaraan");
			// BindUtils.postNotifyChange(null, null,
			// this,"listMerkKendaraanDto");
			// BindUtils.postNotifyChange(null, null, this,"listJenisSimDto");
			// BindUtils.postNotifyChange(null, null,
			// this,"listStatusKendaraanDto");
		}
		listJenisKendaraan();
		listStatusKendaraan();
		listJenisSim();
		listMerkKendaraan();
		setListKendaraan(false);
		setFormTambahKendaraan(true);
		BindUtils.postNotifyChange(null, null, this, "listKendaraan");
		BindUtils.postNotifyChange(null, null, this, "formTambahKendaraan");

	}

	public void tabKorban() {
		listProvinsiKorban();
		listPekerjaan();
		listStatusKorban();
		listStatusNikah();
		listIdentitas();
		listSifatCidera();
		getListKendaraanByIdForKorban();
		listJenisPertanggungan();
		getKabkotaKorban();
		getCamatKorban();
		getCamatDtoKorban();

		setListKorban(false);
		setFormTambahKorban(true);

		BindUtils.postNotifyChange(null, null, this, "listKorban");
		BindUtils.postNotifyChange(null, null, this, "formTambahKorban");
	}

	@Command
	public void listIdentitas() {
		RestResponse listIdentitasRest = callWs(WS_URI_LOV + "/getListId", new HashMap<String, Object>(),
				HttpMethod.POST);
		try {
			listIdentitasDto = JsonUtil.mapJsonToListObject(listIdentitasRest.getContents(), DasiJrRefCodeDto.class);
			BindUtils.postNotifyChange(null, null, this, "listIdentitasDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listKasusKecelakaan() {
		RestResponse listKasusKecelakaanRest = callWs(WS_URI_LOV + "/getListKasusKecelakaan",
				new HashMap<String, Object>(), HttpMethod.POST);
		listKasusKecelakaanDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listKasusKecelakaanRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue(null);
			kosong.setRvMeaning("-");
			kosong.setLowMeaning("-");
			listKasusKecelakaanDto.add(kosong);
			listKasusKecelakaanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listKasusKecelakaanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listPekerjaan() {
		RestResponse listJenisPekerjaanRest = callWs(WS_URI_LOV + "/getListJenisPekerjaan",
				new HashMap<String, Object>(), HttpMethod.POST);
		listJenisPekerjaanDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listJenisPekerjaanRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue(null);
			kosong.setRvMeaning("-");
			listJenisPekerjaanDto.add(kosong);
			listJenisPekerjaanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listJenisPekerjaanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listStatusKorban() {
		RestResponse listStatusKorbanRest = callWs(WS_URI_LOV + "/getListStatusKorban", new HashMap<String, Object>(),
				HttpMethod.POST);
		listStatusKorbanDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listStatusKorbanRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue(null);
			kosong.setRvMeaning("-");
			listStatusKorbanDto.add(kosong);
			listStatusKorbanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listStatusKorbanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listStatusNikah() {
		RestResponse listStatusNikahRest = callWs(WS_URI_LOV + "/getListStatusNikah", new HashMap<String, Object>(),
				HttpMethod.POST);
		listStatusNikahDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listStatusNikahRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue(null);
			kosong.setRvMeaning("-");
			listStatusNikahDto.add(kosong);
			listStatusNikahDto.addAll(listBaru);
			setTotalSize(listStatusNikahRest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listStatusNikahDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listJenisKendaraan() {
		RestResponse listJenisKendaraanRest = callWs(WS_URI_LOV + "/getListJenisKendaraan",
				new HashMap<String, Object>(), HttpMethod.POST);
		listJenisKendaraanDto = new ArrayList<>();
		List<FndJenisKendaraanDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listJenisKendaraanRest.getContents(), FndJenisKendaraanDto.class);
			FndJenisKendaraanDto kosong = new FndJenisKendaraanDto();
			kosong.setDeskripsi("-");
			kosong.setKodeJenis(null);
			kosong.setKodeNama("-");
			listJenisKendaraanDto.add(kosong);
			listJenisKendaraanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listJenisKendaraanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listStatusKendaraan() {
		RestResponse listStatusKendaraanRest = callWs(WS_URI_LOV + "/getListStatusKendaraan",
				new HashMap<String, Object>(), HttpMethod.POST);
		listStatusKendaraanDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listStatusKendaraanRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue(null);
			kosong.setRvMeaning("-");
			listStatusKendaraanDto.add(kosong);
			listStatusKendaraanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listStatusKendaraanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listJenisSim() {
		RestResponse listJenisSimRest = callWs(WS_URI_LOV + "/getListJenisSim", new HashMap<String, Object>(),
				HttpMethod.POST);
		listJenisSimDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listJenisSimRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue(null);
			kosong.setRvMeaning("-");
			listJenisSimDto.add(kosong);
			listJenisSimDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listJenisSimDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listMerkKendaraan() {
		RestResponse listMerkKendaraanRest = callWs(WS_URI_LOV + "/getListMerkKendaraan", new HashMap<String, Object>(),
				HttpMethod.POST);
		listMerkKendaraanDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listMerkKendaraanRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue(null);
			kosong.setRvMeaning("-");
			listMerkKendaraanDto.add(kosong);
			listMerkKendaraanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listMerkKendaraanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getLokasiByCamat() {
		Map<String, Object> map = new HashMap<>();
		RestResponse rest = callWs(WS_URI_LOV + "/getLokasiByCamat/" + getCamatDto().getKodeCamat(), map,
				HttpMethod.POST);
		try {
			plMappingCamatDto = JsonUtil.mapJsonToSingleObject(rest.getContents(), PlMappingCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "plMappingCamatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void getLokasiByCamatKorban() {
		Map<String, Object> map = new HashMap<>();
		RestResponse rest = callWs(WS_URI_LOV + "/getLokasiByCamat/" + getCamatDtoKorban().getKodeCamat(), map,
				HttpMethod.POST);
		try {
			plMappingCamatDto = JsonUtil.mapJsonToSingleObject(rest.getContents(), PlMappingCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "plMappingCamatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void getTxtLaporan1() {
		if (instansiDto == null) {
			instansiDto = new PlInstansiDto();
		}
		if (getInstansiDto().getKodeInstansi() == null || getInstansiDto().getKodeInstansi().equalsIgnoreCase("")) {
			setLaporan1("");
			BindUtils.postNotifyChange(null, null, this, "laporan1");
		}
		HashMap<String, Object> map = new HashMap<>();
		map.put("kodeInstansi", instansiDto.getKodeInstansi());
		RestResponse rest = callWs(WS_URI + "/kodeNoLaporan", map, HttpMethod.POST);
		List<PlMappingPoldaDto> listPoldaDto = new ArrayList<>();
		String kodeNoLaporan = null;
		try {
			listPoldaDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlMappingPoldaDto.class);
			for (PlMappingPoldaDto a : listPoldaDto) {
				kodeNoLaporan = a.getKodeNoLaporan();
			}
			if (kodeNoLaporan == null || kodeNoLaporan.equalsIgnoreCase("null") || kodeNoLaporan.equalsIgnoreCase("")) {
				kodeNoLaporan = "-";
			}
			setLaporan1("LP/" + kodeNoLaporan + "/");
			// setLaporan1("LP/" + getInstansiDto().getKodeInstansi() + "/");
			BindUtils.postNotifyChange(null, null, this, "laporan1");
			cekData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void getTxtLaporan3() {
		String bulanToRomawi = null;
		String tahun;
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = dateFormat.format(plDataKecelakaanDto.getTglLaporanPolisi());
		if (strDate.substring(3, 5).equalsIgnoreCase("01")) {
			bulanToRomawi = "I";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("02")) {
			bulanToRomawi = "II";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("03")) {
			bulanToRomawi = "III";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("04")) {
			bulanToRomawi = "IV";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("05")) {
			bulanToRomawi = "V";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("06")) {
			bulanToRomawi = "VI";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("07")) {
			bulanToRomawi = "VII";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("08")) {
			bulanToRomawi = "VIII";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("09")) {
			bulanToRomawi = "IX";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("10")) {
			bulanToRomawi = "X";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("11")) {
			bulanToRomawi = "XI";
		} else if (strDate.substring(3, 5).equalsIgnoreCase("12")) {
			bulanToRomawi = "XII";
		}
		tahun = strDate.substring(6, 10);
		setLaporan3("/" + bulanToRomawi + "/" + tahun + "/LL");
		BindUtils.postNotifyChange(null, null, this, "laporan3");
		cekData();
	}

//	@Command
//	public void searchAsalBerkas() {
//		Map<String, Object> map = new HashMap<>();
//		map.put("search", searchAsalBerkas);
//		RestResponse rest = callWs(WS_URI_LOV + "/getAsalBerkas", map,
//				HttpMethod.POST);
//		try {
//			listAsalBerkasDto = JsonUtil.mapJsonToListObject(rest.getContents(), FndKantorJasaraharjaDto.class);
//			BindUtils.postNotifyChange(null, null, this, "listAsalBerkasDto");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	@NotifyChange("listAsalBerkasDtoCopy")
	@Command
	public void cariAsalBerkas(@BindingParam("item") String cari) {
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listAsalBerkasDtoCopy));
		if (listAsalBerkasDtoCopy != null || listAsalBerkasDtoCopy.size() > 0) {
			listAsalBerkasDtoCopy.clear();
		}
		if (listAsalBerkasDto != null && listAsalBerkasDto.size() > 0) {
			for (FndKantorJasaraharjaDto dto : listAsalBerkasDto) {
				System.out.println("+++");
				if (dto.getKodeNama().toUpperCase().contains(cari.toUpperCase())) {
					listAsalBerkasDtoCopy.add(dto);
				}
			}
		}
	}

	@Command
	public void resetKriteria() {
		navigate("");
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}

	@Command
	public void showDataLaka() {
		if (getPageInfo().isEditMode()) {
			edit("/WEB-INF/application-pages/operasionalDetail/DataKecelakaanDetail.zul", plDataKecelakaanDto);
			onEdit();
		}
	}

	@NotifyChange("flagAddDataKendaraan")
	@Command
	public void showKendaraanTerlibat() {
		if (getPageInfo().isAddMode() && flagSaveDataLaka == false) {
			setListKendaraan(false);
			setFormTambahKendaraan(false);

			BindUtils.postNotifyChange(null, null, this, "formTambahKendaraan");
			BindUtils.postNotifyChange(null, null, this, "listKendaraan");
		} else if (flagAddDataKendaraan == true) {
			setListKendaraan(false);
			setFormTambahKendaraan(true);
			BindUtils.postNotifyChange(null, null, this, "formTambahKendaraan");
			BindUtils.postNotifyChange(null, null, this, "listKendaraan");
		} else {
			setListKendaraan(true);
			setFormTambahKendaraan(false);
			getListKendaraanById();
			BindUtils.postNotifyChange(null, null, this, "formTambahKendaraan");
			BindUtils.postNotifyChange(null, null, this, "listKendaraan");
		}
	}

	@Command
	public void backKendaraanTerlibat() {
		setFlagAddDataKendaraan(false);
		BindUtils.postNotifyChange(null, null, this, "flagAddDataKendaraan");
		showKendaraanTerlibat();
	}

	@Command
	public void getListKendaraanById() {
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
		map.put("search", searchKendaraan);
		RestResponse rest = callWs(WS_URI + "/angkutanLakaById", map, HttpMethod.POST);
		try {
			listPlAngkutanKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlAngkutanKecelakaanDto.class);
			BindUtils.postNotifyChange(null, null, this, "listPlAngkutanKecelakaanDtos");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void getListKendaraanByIdForKorban() {
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
		map.put("search", searchKendaraan);
		RestResponse rest = callWs(WS_URI + "/angkutanLakaById", map, HttpMethod.POST);
		listPlAngkutanKecelakaanDtos = new ArrayList<>();
		List<PlAngkutanKecelakaanDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(), PlAngkutanKecelakaanDto.class);
			PlAngkutanKecelakaanDto kosong = new PlAngkutanKecelakaanDto();
			kosong.setNoPlatNamaPengemudiDesc("-");
			kosong.setIdAngkutanKecelakaan(null);
			listPlAngkutanKecelakaanDtos.add(kosong);
			listPlAngkutanKecelakaanDtos.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listPlAngkutanKecelakaanDtos");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void addKendaraanTerlibat() {
		getPageInfo().setCustomAttribute("addKendaraan");
		plAngkutanKecelakaanDto = new PlAngkutanKecelakaanDto();
		jenisSimDto = new DasiJrRefCodeDto();
		merkKendaraanDto = new DasiJrRefCodeDto();
		statusKendaraanDto = new DasiJrRefCodeDto();
		setFlagAddDataKendaraan(true);
		BindUtils.postNotifyChange(null, null, this, "plAngkutanKecelakaanDto");
		BindUtils.postNotifyChange(null, null, this, "flagAddDataKendaraan");
		BindUtils.postNotifyChange(null, null, this, "jenisSimDto");
		BindUtils.postNotifyChange(null, null, this, "merkKendaraanDto");
		BindUtils.postNotifyChange(null, null, this, "statusKendaraanDto");
		tabKendaraanTerlibat();
	}

	@Command
	public void deleteAngkutan(@BindingParam("item") final PlAngkutanKecelakaanDto selected) {
		if (selected == null || selected.getIdAngkutanKecelakaan() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Messagebox.show(Labels.getLabel("C001"), Labels.getLabel("confirmation"),
				new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							restRespone = callWs(WS_URI + "/deleteAngkutan", selected, HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data successfully deleted");
								showKendaraanTerlibat();
							} else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});
	}

	@Command
	public void showKorban() {
		if (getPageInfo().isAddMode() && flagSaveDataLaka == false) {
			setListKorban(false);
			setFormTambahKorban(false);

			BindUtils.postNotifyChange(null, null, this, "listKorban");
			BindUtils.postNotifyChange(null, null, this, "formTambahKorban");
		} else {
			setListKorban(true);
			setFormTambahKorban(false);
			getListKorbanById();
			BindUtils.postNotifyChange(null, null, this, "listKorban");
			BindUtils.postNotifyChange(null, null, this, "formTambahKorban");
		}
	}

	@Command
	public void getListKorbanById() {
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
		map.put("search", searchKorban);
		RestResponse rest = callWs(WS_URI + "/korbanLakaById", map, HttpMethod.POST);
		try {
			listPlKorbanKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(), PlKorbanKecelakaanDto.class);
			BindUtils.postNotifyChange(null, null, this, "listPlKorbanKecelakaanDtos");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/* TAB KORBAN */
	@Command
	public void addKorban() {
		getPageInfo().setCustomAttribute("addKorban");
		plKorbanKecelakaanDto = null;
		plKorbanKecelakaanDto = new PlKorbanKecelakaanDto();
		camatDtoKorban = new FndCamatDto();
		jenisIdentitasDto = new DasiJrRefCodeDto();
		jenisPekerjaanDto = new DasiJrRefCodeDto();
		sifatCideraDto = new DasiJrRefCodeDto();
		kendaraanPenjaminDto = new PlAngkutanKecelakaanDto();
		kendaraanPosisiDto = new PlAngkutanKecelakaanDto();
		statusKorbanDto = new DasiJrRefCodeDto();
		jenisPertanggunganDto = new PlJaminanDto();
		statusNikahDto = new DasiJrRefCodeDto();
		tabKorban();
		BindUtils.postNotifyChange(null, null, this, "plKorbanKecelakaanDto");
		BindUtils.postNotifyChange(null, null, this, "camatDtoKorban");
		BindUtils.postNotifyChange(null, null, this, "jenisIdentitasDto");
		BindUtils.postNotifyChange(null, null, this, "jenisPekerjaanDto");
		BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");
		BindUtils.postNotifyChange(null, null, this, "kendaraanPenjaminDto");
		BindUtils.postNotifyChange(null, null, this, "kendaraanPosisiDto");
		BindUtils.postNotifyChange(null, null, this, "statusKorbanDto");
		BindUtils.postNotifyChange(null, null, this, "jenisPertanggunganDto");
		BindUtils.postNotifyChange(null, null, this, "statusNikahDto");

	}

	@Command
	public void deleteKorban(@BindingParam("item") final PlKorbanKecelakaanDto selected) {
		try {
			if (selected == null || selected.getIdKorbanKecelakaan() == null) {
				showSmartMsgBox("W001");
				return;
			}

			Messagebox.show(Labels.getLabel("C001"), Labels.getLabel("confirmation"),
					new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
					new EventListener<ClickEvent>() {
						@Override
						public void onEvent(ClickEvent evt) throws Exception {
							if (Messagebox.ON_YES.equals(evt.getName())) {
								RestResponse restRespone;
								restRespone = callWs(WS_URI + "/deleteKorban", selected, HttpMethod.POST);
								if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
									showInfoMsgBox("Data successfully deleted");
									if (getPageInfo().getCustomAttribute().equalsIgnoreCase("deleteIndex")) {
										searchIndex();
									} else {
										showKorban();
									}
								} else {
									showErrorMsgBox(restRespone.getMessage());
								}
							}
						}
					});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void deleteKorbanIndex(@BindingParam("item") final PlDataKecelakaanDto dto) {
		getPageInfo().setCustomAttribute("deleteIndex");
		PlKorbanKecelakaanDto selected = new PlKorbanKecelakaanDto();
		selected.setIdKorbanKecelakaan(dto.getIdKorbanKecelakaan());
		if (selected.getIdKorbanKecelakaan() == null || selected.getIdKorbanKecelakaan().equalsIgnoreCase("-")) {
			Messagebox.show(Labels.getLabel("C001"), Labels.getLabel("confirmation"),
					new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
					new EventListener<ClickEvent>() {
						@Override
						public void onEvent(ClickEvent evt) throws Exception {
							if (Messagebox.ON_YES.equals(evt.getName())) {
								RestResponse restRespone;
								restRespone = callWs(WS_URI + "/deleteLaka", dto, HttpMethod.POST);
								if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
									showInfoMsgBox("Data successfully deleted");
									if (getPageInfo().getCustomAttribute().equalsIgnoreCase("deleteIndex")) {
										searchIndex();
									} else {
										showKorban();
									}
								} else {
									showErrorMsgBox(restRespone.getMessage());
								}
							}
						}
					});
		} else {
			deleteKorban(selected);
		}
	}

	@Command
	public void back() {
		if (isFlagDataLakaRegister() == true) {
			getPageInfo().setListMode(true);
			navigate(INDEX_REGISTER_PAGE_PATH);
		} else if (flagDataLakaSantunan) {
			getPageInfo().setListMode(true);
			navigate(INDEX_PENGAJUAN_PAGE_PATH);
		} else {
			getPageInfo().setListMode(true);
			navigate(INDEX_PAGE_PATH);
		}

	}

//	@Command
//	public void searchInstansi() {
//		Map<String, Object> map = new HashMap<>();
//		if (instansiDto == null || instansiDto.getKodeInstansi() == null) {
//			map.put("search", searchInstansi);
//		}
//		map.put("search", searchInstansi);
//		RestResponse rest = callWs(WS_URI_LOV + "/getListInstansi", map,
//				HttpMethod.POST);
//		try {
//			listInstansiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
//					PlInstansiDto.class);
//			listInstansiDtoCopy = new ArrayList<>();
//			listInstansiDtoCopy.addAll(listInstansiDto);
//			BindUtils.postNotifyChange(null, null, this, "listInstansiDtoCopy");
//			BindUtils.postNotifyChange(null, null, this, "listInstansiDto");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	@NotifyChange("listInstansiDtoCopy")
	@Command
	public void cariInstansi(@BindingParam("item") String cari) {
		if (listInstansiDtoCopy != null || listInstansiDtoCopy.size() > 0) {
			listInstansiDtoCopy.clear();
		}
		if (listInstansiDto != null && listInstansiDto.size() > 0) {
			for (PlInstansiDto dto : listInstansiDto) {
				if (dto.getKodeNama().toUpperCase().contains(cari.toUpperCase())) {
					listInstansiDtoCopy.add(dto);
				}
			}
		}
	}

//	@NotifyChange("samsatDto")
//	@Command
//	public void searchSamsat() {
//		Map<String, Object> map = new HashMap<>();
//		if (samsatDto == null || samsatDto.getKodeKantorJr() == null) {
//			map.put("search", searchSamsat);
//		}
//		map.put("search", searchSamsat);
//		RestResponse rest = callWs(WS_URI_LOV + "/getListSamsat", map,
//				HttpMethod.POST);
//		try {
//			listSamsatDto = JsonUtil.mapJsonToListObject(rest.getContents(),
//					FndKantorJasaraharjaDto.class);
//			listSamsatDtoCopy = new ArrayList<>();
//			listSamsatDtoCopy.addAll(listSamsatDto);
//			BindUtils.postNotifyChange(null, null, this, "listSamsatDtoCopy");
//			BindUtils.postNotifyChange(null, null, this, "listSamsatDto");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	@NotifyChange("listSamsatDtoCopy")
	@Command
	public void cariSamsat(@BindingParam("item") String cari) {
		if (listSamsatDtoCopy != null || listSamsatDtoCopy.size() > 0) {
			listSamsatDtoCopy.clear();
		}
		if (listSamsatDto != null && listSamsatDto.size() > 0) {
			for (FndKantorJasaraharjaDto dto : listSamsatDto) {
				if (dto.getKodeNama().toUpperCase().contains(cari.toUpperCase())) {
					listSamsatDtoCopy.add(dto);
				}
			}
		}
	}

	@Command
	public void searchLokasi() {
		Map<String, Object> map = new HashMap<>();

		if (fndLokasiDto == null || fndLokasiDto.getKodeLokasi() == null) {
			map.put("search", searchLokasi);
		}
		map.put("search", searchLokasi);

		RestResponse listLokasiRest = callWs(WS_URI_LOV + "/getListLokasi", map, HttpMethod.POST);
		try {
			listLokasiDto = JsonUtil.mapJsonToListObject(listLokasiRest.getContents(), FndLokasiDto.class);
			listLokasiDtoCopy = new ArrayList<>();
			listLokasiDtoCopy.addAll(listLokasiDto);
			BindUtils.postNotifyChange(null, null, this, "listLokasiDto");
			BindUtils.postNotifyChange(null, null, this, "listLokasiDtoCopy");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange("listLokasiDtoCopy")
	@Command
	public void cariLokasi(@BindingParam("item") String cari) {
		if (listLokasiDtoCopy != null || listLokasiDtoCopy.size() > 0) {
			listLokasiDtoCopy.clear();
		}
		if (listLokasiDto != null && listLokasiDto.size() > 0) {
			for (FndLokasiDto dto : listLokasiDto) {
				if (dto.getKodeDeskripsi().toUpperCase().contains(cari.toUpperCase())) {
					listLokasiDtoCopy.add(dto);
				}
			}
		}
	}

	@Command
	public void searchProvinsi() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchProvinsi);
		RestResponse rest = callWs(WS_URI_LOV + "/getListProvinsi", map, HttpMethod.POST);
		listProvinsiDto = new ArrayList<>();
		List<FndCamatDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(), FndCamatDto.class);
			FndCamatDto kosong = new FndCamatDto();
			kosong.setKodeProvinsi(null);
			kosong.setNamaProvinsi("-");
			;
			kosong.setKodeNamaProv("-");
			listProvinsiDto.add(kosong);
			listProvinsiDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listProvinsiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listProvinsiKorban() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchProvinsi);
		RestResponse rest = callWs(WS_URI_LOV + "/getListProvinsi", map, HttpMethod.POST);
		listProvinsiKorbanDto = new ArrayList<>();
		List<FndCamatDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(), FndCamatDto.class);
			FndCamatDto kosong = new FndCamatDto();
			kosong.setKodeProvinsi(null);
			kosong.setNamaProvinsi("-");
			;
			kosong.setKodeNamaProv("-");
			listProvinsiKorbanDto.add(kosong);
			listProvinsiKorbanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listProvinsiKorbanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange({ "searchProvinsi" })
	@Command
	public void getKabkota() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchKabkota);
		RestResponse listKabkotaRest = callWs(WS_URI_LOV + "/getListKabkota/" + provinsiDto.getKodeProvinsi(), map,
				HttpMethod.POST);
		listKabkotaDto = new ArrayList<>();
		List<FndCamatDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listKabkotaRest.getContents(), FndCamatDto.class);
			FndCamatDto kosong = new FndCamatDto();
			kosong.setKodeKabkota(null);
			kosong.setNamaKabkota("-");
			kosong.setKodeNamaKabKota("-");
			listKabkotaDto.add(kosong);
			listKabkotaDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listKabkotaDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void getKabkotaPengemudi() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchKabkota);
		RestResponse listKabkotaRest = callWs(WS_URI_LOV + "/getListKabkota/" + provinsiPengemudiDto.getKodeProvinsi(),
				map, HttpMethod.POST);
		try {
			listKabkotaDto = JsonUtil.mapJsonToListObject(listKabkotaRest.getContents(), FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKabkotaDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void getKabkotaPemilik() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchKabkota);
		RestResponse listKabkotaRest = callWs(WS_URI_LOV + "/getListKabkota/" + provinsiPemilikDto.getKodeProvinsi(),
				map, HttpMethod.POST);
		try {
			listKabKotaPemilikDto = JsonUtil.mapJsonToListObject(listKabkotaRest.getContents(), FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKabKotaPemilikDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void getKabkotaKorban() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchKabkota);
		RestResponse listKabkotaRest = callWs(WS_URI_LOV + "/getListKabkota/" + provinsiKorbanDto.getKodeProvinsi(),
				map, HttpMethod.POST);
		listKabKotaKorbanDto = new ArrayList<>();
		List<FndCamatDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listKabkotaRest.getContents(), FndCamatDto.class);
			FndCamatDto kosong = new FndCamatDto();
			kosong.setKodeKabkota(null);
			kosong.setNamaKabkota("-");
			kosong.setKodeNamaKabKota("-");
			listKabKotaKorbanDto.add(kosong);
			listKabKotaKorbanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listKabKotaKorbanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange({ "kabKotaDto", "searchCamat", "searchKabkota" })
	@Command
	public void getCamat() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchCamat);
		RestResponse listCamatRest = callWs(WS_URI_LOV + "/getListCamat/" + kabKotaDto.getKodeKabkota(), map,
				HttpMethod.POST);
		listCamatDto = new ArrayList<>();
		List<FndCamatDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listCamatRest.getContents(), FndCamatDto.class);
			FndCamatDto kosong = new FndCamatDto();
			kosong.setKodeCamat(null);
			kosong.setNamaCamat("-");
			kosong.setKodeNamaCamat("-");
			listCamatDto.add(kosong);
			listCamatDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listCamatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void getCamatPengemudi() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchCamat);
		RestResponse listCamatRest = callWs(WS_URI_LOV + "/getListCamat/" + kabkotaPengemudiDto.getKodeKabkota(), map,
				HttpMethod.POST);
		try {
			listCamatDto = JsonUtil.mapJsonToListObject(listCamatRest.getContents(), FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listCamatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void getCamatPemilik() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchCamat);
		RestResponse listCamatRest = callWs(WS_URI_LOV + "/getListCamat/" + kabKotaPemilikDto.getKodeKabkota(), map,
				HttpMethod.POST);
		try {
			listCamatDtoPemilik = JsonUtil.mapJsonToListObject(listCamatRest.getContents(), FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listCamatDtoPemilik");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void getCamatKorban() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchCamat);
		RestResponse listCamatRest = callWs(WS_URI_LOV + "/getListCamat/" + kabKotaKorbanDto.getKodeKabkota(), map,
				HttpMethod.POST);
		listCamatKorbanDto = new ArrayList<>();
		List<FndCamatDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(listCamatRest.getContents(), FndCamatDto.class);
			FndCamatDto kosong = new FndCamatDto();
			kosong.setKodeCamat(null);
			kosong.setNamaCamat("-");
			kosong.setKodeNamaCamat("-");
			listCamatKorbanDto.add(kosong);
			listCamatKorbanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listCamatKorbanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void closeDetail(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}

	// ======================================== CETAK
	// ==================================================

	// TODO: cetak word dokument
	/*
	 * Untuk buat file docx
	 */
	private String desktop = "C:\\Users\\Superman\\Desktop\\";
	private String companyName = "PT. JASA RAHARJA (PERSERO)";
	private String noBerkas = "2-001-00-02-00-11-2018";
	private String namaCabang = "CABANG DKI JAKARTA";
	private String namaPengaju = "Jajat Sudrajat";
	private String kecPengaju = "Kec. Cikarang Utara";
	private String kotaPengaju = "Bekasi";
	private String telpPengaju = "0853516689577";
	private String alamatPengaju = "Kp. Cabang Lio, RT.003/004, Kel. Karang Asih" + ", " + kecPengaju + ", "
			+ kotaPengaju + " / " + telpPengaju;
	private String hubunganPengajuDgKorban = "Korban Sendiri";
	private String identitasKorban = "3216091601790011";
	private String namaKorban2 = "Jajat Sudrajat";
	private String umurKorban = "37";
	private String kecKorban = "Kec. Cikarang Utara";
	private String kotaKorban = "Bekasi";
	private String telpKorban = "0853516689577";
	private String alamatKorban = "Kp. Cabang Lio, RT.003/004, Kel. Karang Asih" + ", " + kecKorban + ", " + kotaKorban
			+ " / " + telpKorban;
	private String kecKecelakaan = "Kec. Pulogadung";
	private String kotaKecelakaan = "Jakarta Timur";
	private String tempatKecelakaan = kecKecelakaan + ", " + kotaKecelakaan;
	private Date tglKecelakaan = new Date();
	private String sifatCidera2 = "Luka - Luka";
	private List<String> listBerkasDiterima = new ArrayList<>();
	private List<String> listBerkasKurang = new ArrayList<>();
	private String namaLogin = "Petugas Testing 03";
	private String lokasi2 = "Jakarta";

	// CETAK DATA LAKA
	// data kendaraan
	private List<String> listNoPolisi = new ArrayList<>();
	private List<String> listGol = new ArrayList<>();
	private List<String> listPengemudi = new ArrayList<>();
	private List<String> listPemilik = new ArrayList<>();
	private List<String> listStatus = new ArrayList<>();

	// data korban
	private List<String> listNamaJkUmur = new ArrayList<>();
	private List<String> listAlamat = new ArrayList<>();
	private List<String> listCidera = new ArrayList<>();
	private List<String> listStatusKorban = new ArrayList<>();
	private List<String> listKendaraanKorban = new ArrayList<>();
	private List<String> listKendaraanPenjamin = new ArrayList<>();
	private List<String> listJenisJaminan = new ArrayList<>();

	public static void main(String[] args) throws Exception {
		System.err.println(JsonUtil.getJson(new PlPksRDto()));
		System.err.println(JsonUtil.getJson(new PlRsMapBpjDto()));
		System.err.println(JsonUtil.getJson(new PlRekeningRDto()));
		System.err.println(JsonUtil.getJson(new PlRsBpjDto()));
		System.err.println(JsonUtil.getJson(new AuthRsPicDto()));
		System.err.println(JsonUtil.getJson(new AuthUserGadgetDto()));
		System.err.println(JsonUtil.getJson(new AuthRsPicDto()));
		System.err.println(JsonUtil.getJson(new AuthUserDto()));
		System.err.println(JsonUtil.getJson(new FndBankDto()));
		System.err.println(JsonUtil.getJson(new PlAdditionalDescDto()));
//		TestVmd a = new TestVmd();
		// System.out.println("");
		// a.createDoc();
//		a.loaddong();
	}

	private void createBlank() throws Exception {
		XWPFDocument doc = new XWPFDocument();

		FileOutputStream out = new FileOutputStream(new File(desktop + "blank.docx"));
		doc.write(out);
		out.close();
		System.out.println("blank.docx written successully");
	}

	private void createParagraph() throws Exception {
		// Blank Document
		XWPFDocument document = new XWPFDocument();

		// Write the Document in file system
		FileOutputStream out = new FileOutputStream(new File(desktop + "createparagraph.docx"));

		// create Paragraph
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setText("At tutorialspoint.com, we strive hard to " + "provide quality tutorials for self-learning "
				+ "purpose in the domains of Academics, Information "
				+ "Technology, Management and Computer Programming Languages.");

		document.write(out);
		out.close();
		System.out.println("createparagraph.docx written successfully");
	}

	/*
	 * nambah berkas yg diterima
	 */

	private void addListKendaraan() {
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
		RestResponse rest = callWs(WS_URI + "/angkutanLakaById", map, HttpMethod.POST);
		try {
			listPlAngkutanKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlAngkutanKecelakaanDto.class);
			for (PlAngkutanKecelakaanDto a : listPlAngkutanKecelakaanDtos) {
				listNoPolisi.add(a.getNoPolisi());
				listGol.add(a.getJenisDesc());
				listPemilik.add(a.getNamaPemilik());
				listPengemudi.add(a.getNamaPengemudi());
				listStatus.add(a.getStatusDesc());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addListKorban() {
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
		RestResponse rest = callWs(WS_URI + "/korbanLakaById", map, HttpMethod.POST);
		try {
			listPlKorbanKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(), PlKorbanKecelakaanDto.class);

			for (PlKorbanKecelakaanDto a : listPlKorbanKecelakaanDtos) {
				listNamaJkUmur.add(a.getNama() + " / " + a.getJenisKelamin() + " / " + a.getUmur() + " Tahun");
				listAlamat.add(a.getAlamat());
				listCidera.add(a.getCideraHighDesc());
				listStatusKorban.add(a.getStatusKorbanDesc());
				listJenisJaminan.add(a.getPertanggunganDesc());
				listKendaraanPenjamin.add(a.getPenjaminDesc());
				listKendaraanKorban.add(a.getPosisiDesc());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String getBulan(Date tgl) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String month = sdf.format(new Date());
		return month.equalsIgnoreCase("01") ? "Januari"
				: month.equalsIgnoreCase("02") ? "Februari"
						: month.equalsIgnoreCase("03") ? "Maret"
								: month.equalsIgnoreCase("04") ? "April"
										: month.equalsIgnoreCase("05") ? "Mei"
												: month.equalsIgnoreCase("06") ? "Juni"
														: month.equalsIgnoreCase("07") ? "Juli"
																: month.equalsIgnoreCase("08") ? "Agustus"
																		: month.equalsIgnoreCase("09") ? "September"
																				: month.equalsIgnoreCase("10")
																						? "Oktober"
																						: month.equalsIgnoreCase("11")
																								? "Nopember"
																								: month.equalsIgnoreCase(
																										"12") ? "Desember"
																												: "";

	}

	private String checkJam(Date tgl) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(tgl);
	}

	private String getHari(Date tgl) {
		SimpleDateFormat sdf = new SimpleDateFormat("DD");
		return sdf.format(tgl);
	}

	private String getTahun(Date tgl) {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
		return sdf.format(tgl);
	}

	private String formatTanggal(Date tgl) {
		return getHari(tgl) + " " + getBulan(tgl) + " " + getTahun(tgl);
	}

	private void setTableBorderNone(XWPFTable table) {
		CTTblPr tblpro = table.getCTTbl().getTblPr();

		CTTblWidth tblW = tblpro.addNewTblW();
		// tblW.setW(arg0);

		CTTblBorders border = tblpro.addNewTblBorders();

		border.addNewBottom().setVal(STBorder.NONE);
		border.addNewLeft().setVal(STBorder.NONE);
		border.addNewRight().setVal(STBorder.NONE);
		border.addNewTop().setVal(STBorder.NONE);
		border.addNewInsideH().setVal(STBorder.NONE);
		border.addNewInsideV().setVal(STBorder.NONE);
	}

	private void addRow(XWPFTable table, String... a) {
		if (a.length > 0) {
			XWPFTableRow row = table.createRow();
			for (int x = 0; x < a.length; x++) {
				if (x > 0 && (row.getCell(x) == null)) {
					row.addNewTableCell().setText(a[x]);
				} else {
					row.getCell(x).setText(a[x]);
				}
			}
		}
	}

	private void addFirstRow(XWPFTable table, String... a) {
		if (a.length > 0) {
			XWPFTableRow row = table.createRow();
			row.getCell(0).setText(a[0]);
			for (int x = 1; x < a.length; x++) {
				row.addNewTableCell().setText(a[x]);
			}
		}
	}

	private void setCellWidth(XWPFTable tabel, int column, long value) {
		tabel.getRow(0).getCell(column).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(value));
	}

	private void setCellWidth(XWPFTableRow baris, int column, long value) {
		baris.getCell(column).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(value));
	}

	public void setTableAlign(XWPFTable table, ParagraphAlignment align) {
		CTTblPr tblPr = table.getCTTbl().getTblPr();
		CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
		STJc.Enum en = STJc.Enum.forInt(align.getValue());
		jc.setVal(en);
	}

	@Command
	public void printDocument(@BindingParam("item") PlDataKecelakaanDto plDataKecelakaanDto) {
		String idKec = plDataKecelakaanDto.getIdKecelakaan();
		// String userLogin = getCurrentUserSessionJR().getLoginID();
		// String kantor = getCurrentUserSessionJR().getKantor();
		Map<String, Object> map = new HashMap<>();
		map.put("idKec", idKec);
		map.put("login", getCurrentUserSessionJR().getLoginID());
		map.put("kantor", getCurrentUserSessionJR().getKantor());
		AuthUserDto petugas = new AuthUserDto();
		petugas.setDescription(getCurrentUserSessionJR().getLoginDesc());
		petugas.setLoginUsername(getCurrentUserSessionJR().getUserName());
		map.put("petugas", petugas);
		Map<String, Object> out = new HashMap<>();
		RestResponse rest = new RestResponse();
		try {
			rest = callWs(WS_URI + "/getPrintData", map, HttpMethod.POST);
			out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		Map<String, Object> args = new HashMap<>();
		args.put("map", out);
		BindUtils.postGlobalCommand(null, null, "cetakDataLaka", args);
	}

	// =================================Lampiran====================================================
	@Command("lampiranSantunan")
	public void showPopupLampiran(@BindingParam("popup") String popup,
			@BindingParam("item") PlDataKecelakaanDto plDataKecelakaanDto,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();

		args.put("popUpHandler", globalHandleMethodName);
		args.put("plDataKecelakaanDto", plDataKecelakaanDto);

		// if (!beforePopup(args, popup))
		// return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	@GlobalCommand("lampiranHandler")
	public void titleHandlerLampiran(@BindingParam("idKecelakaan") PlDataKecelakaanDto selected) {
		if (plDataKecelakaanDto != null) {
			this.plDataKecelakaanDto = selected;
		}
	}

	@Command
	public void loaddong(@BindingParam("item") PlDataKecelakaanDto plDataKecelakaanDto) throws Exception {
		int cc = 0;

		if (cc == 0) {
			Messagebox.show("Under Construction");
			return;
		}

		if (plDataKecelakaanDto == null || plDataKecelakaanDto.getIdKecelakaan() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Map<String, Object> map1 = new HashMap<>();
		map1.put("kantorUser", userSession.getKantor().substring(0, 4));

		RestResponse rest2 = callWs(WS_URI + "/getKepalaCabangByUser", map1, HttpMethod.POST);
		try {
			authUserDtos = JsonUtil.mapJsonToListObject(rest2.getContents(), AuthUserDto.class);
			for (AuthUserDto a : authUserDtos) {
				authUserDto.setDescription(a.getDescription());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
		RestResponse rest = callWs(WS_URI + "/dataLakaById", map, HttpMethod.POST);
		tabDataLaka();
		try {
			listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(), PlDataKecelakaanDto.class);

			for (PlDataKecelakaanDto a : listPlDataKecelakaanDtos) {
				SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String tgl = date.format(a.getTglKejadian());
				String jam = time.format(a.getTglKejadian());
				Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(tgl);
				Date date2 = new SimpleDateFormat("HH:mm").parse(jam);
				setTglKejadian(date1);
				setJamKejadian(date2);
				String string = a.getNoLaporanPolisi();
				String[] parts = string.split("/");
				String part1 = parts[0];
				String part2 = parts[1];
				String part3 = parts[2];
				String part4 = parts[3];
				String part5 = parts[4];
				String part6 = parts[5];

				setLaporan1(part1 + "/" + part2 + "/");
				setLaporan2(part3);
				setLaporan3("/" + part4 + "/" + part5 + "/" + part6);

				setSearchAsalBerkas(a.getAsalBerkasDesc());
				setSearchInstansi(a.getInstansiDesc());
				setSearchSamsat(a.getSamsatDesc());
				plDataKecelakaanDto.setNoUrut(a.getNoUrut());
				plDataKecelakaanDto.setIdKecelakaan(a.getIdKecelakaan());
				plDataKecelakaanDto.setNamaPetugas(a.getNamaPetugas());
				plDataKecelakaanDto.setTglLaporanPolisi(a.getTglLaporanPolisi());
				plDataKecelakaanDto.setDeskripsiLokasi(a.getDeskripsiLokasi());
				kasusKecelakaanDto.setRvMeaning(a.getKasusKecelakaanDesc());
				kasusKecelakaanDto.setRvLowValue(a.getKodeKasusKecelakaan());
				provinsiDto.setNamaProvinsi(a.getProvinsiDesc());
				provinsiDto.setKodeProvinsi(a.getKodeProvinsiLokasi());

				kabKotaDto.setNamaKabkota(a.getKabKotaDesc());
				kabKotaDto.setKodeKabkota(a.getKodeKabkotaLokasi());

				camatDto.setNamaCamat(a.getCamatDesc());
				camatDto.setKodeCamat(a.getKodeCamatLokasi());
				sifatKecelakaanDto.setRvMeaning(a.getSifatKecelakaanDesc());
				sifatKecelakaanDto.setRvLowValue(a.getSifatKecelakaan());
				asalBerkasDto.setKodeKantorJr(a.getAsalBerkas());
				instansiDto.setKodeInstansi(a.getKodeInstansi());
				samsatDto.setKodeKantorJr(a.getKodeWilayah());
				plDataKecelakaanDto.setKasusKecelakaanDesc(a.getKasusKecelakaanDesc());
				plDataKecelakaanDto.setSifatKecelakaanDesc(a.getSifatKecelakaanDesc());
				plDataKecelakaanDto.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
				plDataKecelakaanDto.setGpsLu(a.getGpsLu());
				plDataKecelakaanDto.setGpsLs(a.getGpsLs());

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat time = new SimpleDateFormat("HH:mm");
		String tgl = date.format(plDataKecelakaanDto.getTglLaporanPolisi());
		String tgl2 = date.format(plDataKecelakaanDto.getTglKejadian());
		String jam = time.format(plDataKecelakaanDto.getTglKejadian());

		SimpleDateFormat tglKec = new SimpleDateFormat();
		String tagglKec = tglKec.format(tglKecelakaan);
		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();

		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}

		pageSize.setOrient(STPageOrientation.LANDSCAPE);
		pageSize.setW(BigInteger.valueOf(842 * 20));
		pageSize.setH(BigInteger.valueOf(595 * 20));
		// document.getDocument().getBody().getSectPr().addNewPgSz().setOrient(STPageOrientation.LANDSCAPE);

		// nama file
		// String name = "document.docx";

		// tempat dokument di simpan
		// FileOutputStream out = new FileOutputStream(new File(desktop+name));

		// bagian 2
		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.CENTER);
		// set align CENTER untuk tulisan

		// run kedua
		XWPFRun run2 = par2.createRun();
		run2.setText("LEMBAR HASIL CETAK DATA LAKA DASI-JR");

		// bagian 3
		XWPFParagraph par3 = document.createParagraph();
		XWPFRun run3 = par3.createRun();
		run3.setText("1. DATA KECELAKAAN ");

		// bagian 4
		XWPFTable tabelDataDiri = document.createTable();

		// set tabel border = NONE
		setTableBorderNone(tabelDataDiri);

		XWPFTableRow row1Data = tabelDataDiri.getRow(0);
		row1Data.getCell(0).setText("A. KECAMATAN TKP");
		row1Data.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000));
		row1Data.addNewTableCell().setText(":");
		row1Data.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(100));
		row1Data.addNewTableCell().setText(plDataKecelakaanDto.getLokasiDesc());
		row1Data.addNewTableCell().setText("");
		row1Data.addNewTableCell().setText("");
		row1Data.addNewTableCell().setText("");
		XWPFTableRow row2Data = tabelDataDiri.createRow();
		row2Data.getCell(0).setText("B. LOKET KANTOR");
		row2Data.getCell(1).setText(":");
		row2Data.getCell(2).setText(getSearchInstansi());
		XWPFTableRow row3Data = tabelDataDiri.createRow();
		row3Data.getCell(0).setText("C. INSTANSI YANG MENANGANI");
		row3Data.getCell(1).setText(":");
		row3Data.getCell(2).setText(getSearchInstansi());
		XWPFTableRow row4Data = tabelDataDiri.createRow();
		row4Data.getCell(0).setText("D. PETUGAS YANG MENANGANI");
		row4Data.getCell(1).setText(":");
		row4Data.getCell(2).setText(plDataKecelakaanDto.getNamaPetugas());
		XWPFTableRow row5Data = tabelDataDiri.createRow();
		row5Data.getCell(0).setText("E. NOMOR & TANGGAL LP");
		row5Data.getCell(1).setText(":");
		row5Data.getCell(2).setText(plDataKecelakaanDto.getNoLaporanPolisi());
		row5Data.getCell(3).setText("TANGGAL : " + tgl);
		XWPFTableRow row6Data = tabelDataDiri.createRow();
		row6Data.getCell(0).setText("F. WAKTU KEJADIAN");
		row6Data.getCell(1).setText(":");
		row6Data.getCell(2).setText("HARI : ");
		row6Data.getCell(3).setText("TANGGAL : " + tgl2);
		row6Data.getCell(4).setText("");
		row6Data.getCell(5).setText("JAM : " + jam);
		XWPFTableRow row7Data = tabelDataDiri.createRow();
		row7Data.getCell(0).setText("I. URAIAN LOKASI KEJADIAN");
		row7Data.getCell(1).setText(":");
		row7Data.getCell(2).setText(plDataKecelakaanDto.getDeskripsiLokasi());
		XWPFTableRow row8Data = tabelDataDiri.createRow();
		row8Data.getCell(0).setText("H. KASUS & SIFAT KECELAKAAN");
		row8Data.getCell(1).setText(":");
		row8Data.getCell(2).setText(plDataKecelakaanDto.getKasusKecelakaanDesc());
		row8Data.getCell(3).setText("SIFAT : " + sifatKecelakaanDto.getRvMeaning());
		XWPFTableRow row9Data = tabelDataDiri.createRow();
		row9Data.getCell(0).setText("I. URAIAN KEJADIAN");
		row9Data.getCell(1).setText(":");
		row9Data.getCell(2).setText(plDataKecelakaanDto.getDeskripsiKecelakaan());

		// Bagian 5
		XWPFParagraph par5 = document.createParagraph();
		XWPFRun run5 = par5.createRun();
		run5.setText("2. DATA KENDARAAN ");
		// isi list dokumen
		addListKendaraan();
		addListKorban();

		XWPFTable tabelBerkas = document.createTable();
		setTableBorderNone(tabelBerkas);

		XWPFTableRow rowData = tabelBerkas.getRow(0);
		rowData.getCell(0).setText("No Polisi : ");
		rowData.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000));
		rowData.addNewTableCell().setText("Pengemudi");
		rowData.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(1000));

		for (int i = 0; i < listPlAngkutanKecelakaanDtos.size(); i++) {
			String noPolisi = "";
			String pengemudi = "";
			noPolisi = listNoPolisi.get(i);
			pengemudi = listPengemudi.get(i);
			addRow(tabelBerkas, noPolisi, pengemudi);
		}

		// Bagian 6
		XWPFParagraph par6 = document.createParagraph();
		par6.setAlignment(ParagraphAlignment.RIGHT);
		XWPFRun run6 = par6.createRun();
		run6.setText(lokasi + ", " + formatTanggal(new Date()));
		run6.addCarriageReturn();
		run6.setText("Jam Proses : " + checkJam(new Date()));

		// bagian 7
		XWPFTable tabelTTD = document.createTable();
		setTableBorderNone(tabelTTD);
		XWPFTableRow row = tabelTTD.getRow(0);
		row.getCell(0).setText("Yang Menyerahkan");
		setCellWidth(row, 0, 10000);
		row.addNewTableCell().setText("Yang Menerima");
		row.getCell(1).getParagraphs().get(0).setAlignment(ParagraphAlignment.RIGHT);
		setCellWidth(row, 1, 10000);
		row.setHeight(100);

		XWPFTableRow rowx = tabelTTD.createRow();
		addRow(tabelTTD, namaPengaju, namaLogin);
		rowx.getCell(1).getParagraphs().get(0).setAlignment(ParagraphAlignment.RIGHT);

		// download dokumen
		File temp = File.createTempFile("Document JR", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Document JR", "docx", "application/file", fis));
		temp.delete();

		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");

	}

	@SuppressWarnings("deprecation")
	@Command("cetak")
	public void cete(@BindingParam("popup") String popup, @BindingParam("item") PlDataKecelakaanDto selectedDto)
			throws ClassNotFoundException, SQLException, IOException, JRException, Exception {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			PlDataKecelakaanDto dataLakaReport = new PlDataKecelakaanDto();
			List<PlDataKecelakaanDto> dataKecelakaanDtos = new ArrayList<>();
			List<PlAngkutanKecelakaanDto> angkutanKecelakaanDtos = new ArrayList<>();
			List<PlKorbanKecelakaanDto> korbanKecelakaanDtos = new ArrayList<>();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
			Date date = new Date();

//			=================================GET DATA LAKA======================================
			Map<String, Object> mapLaka = new HashMap<>();
			mapLaka.put("idKec", selectedDto.getIdKecelakaan());
			RestResponse restLaka = callWs(WS_URI + "/reportDataLaka", mapLaka, HttpMethod.POST);
			try {
				dataKecelakaanDtos = JsonUtil.mapJsonToListObject(restLaka.getContents(), PlDataKecelakaanDto.class);
				for (PlDataKecelakaanDto a : dataKecelakaanDtos) {
					dataLakaReport.setAsalBerkasDesc(a.getAsalBerkasDesc());
					dataLakaReport.setNamaPetugas(a.getNamaPetugas());
					dataLakaReport.setTglLaporanPolisi(a.getTglLaporanPolisi());
					dataLakaReport.setTglKejadian(a.getTglKejadian());
					dataLakaReport.setDeskripsiLokasi(a.getDeskripsiLokasi());
					dataLakaReport.setKasusKecelakaanDesc(a.getKasusKecelakaanDesc());
					dataLakaReport.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
					dataLakaReport
							.setLastUpdatedBy(a.getLastUpdatedBy() == null ? a.getCreatedBy() : a.getLastUpdatedBy());
					dataLakaReport.setLastUpdatedDate(
							a.getLastUpdatedDate() == null ? a.getCreationDate() : a.getLastUpdatedDate());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

//			=================================GET DATA KENDARAAN======================================
			Map<String, Object> mapKend = new HashMap<>();
			mapKend.put("idKec", selectedDto.getIdKecelakaan());
			RestResponse restKend = callWs(WS_URI + "/reportDataKendaraan", mapKend, HttpMethod.POST);
			try {
				angkutanKecelakaanDtos = JsonUtil.mapJsonToListObject(restKend.getContents(),
						PlAngkutanKecelakaanDto.class);
			} catch (Exception e) {
				e.printStackTrace();
			}

//			=================================GET DATA KORBAN======================================
			Map<String, Object> mapKorban = new HashMap<>();
			mapKorban.put("idKec", selectedDto.getIdKecelakaan());
			RestResponse restKorban = callWs(WS_URI + "/reportDataKorban", mapKorban, HttpMethod.POST);
			try {
				korbanKecelakaanDtos = JsonUtil.mapJsonToListObject(restKorban.getContents(),
						PlKorbanKecelakaanDto.class);
			} catch (Exception e) {
				e.printStackTrace();
			}

//			=================================GET USER LOGIN==========================================
			Map<String, Object> mapUser = new HashMap<>();
			mapUser.put("login", userSession.getLoginID());
			RestResponse restUser = callWs(WS_URI_LOV + "/getUserByLogin", mapUser, HttpMethod.POST);
			List<AuthUserDto> listUserDtos = new ArrayList<>();
			AuthUserDto userCetak = new AuthUserDto();
			try {
				listUserDtos = JsonUtil.mapJsonToListObject(restUser.getContents(), AuthUserDto.class);
				for (AuthUserDto a : listUserDtos) {
					userCetak.setUserName(a.getUserName() == null ? "" : a.getUserName());
					userCetak.setDescription(a.getDescription() == null ? "" : a.getDescription());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

//			=================================GET USER Update==========================================
			Map<String, Object> mapUserUpdate = new HashMap<>();
			mapUserUpdate.put("login", dataLakaReport.getLastUpdatedBy());
			RestResponse restUserUpdate = callWs(WS_URI_LOV + "/getUserByLogin", mapUserUpdate, HttpMethod.POST);
			List<AuthUserDto> listUserUpdateDtos = new ArrayList<>();
			AuthUserDto userUpdate = new AuthUserDto();
			try {
				listUserUpdateDtos = JsonUtil.mapJsonToListObject(restUserUpdate.getContents(), AuthUserDto.class);
				for (AuthUserDto a : listUserUpdateDtos) {
					userUpdate.setUserName(a.getUserName() == null ? "" : a.getUserName());
					userUpdate.setDescription(a.getDescription() == null ? "" : a.getDescription());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			JRBeanCollectionDataSource listKendaraan = new JRBeanCollectionDataSource(angkutanKecelakaanDtos);
			JRBeanCollectionDataSource listKorban = new JRBeanCollectionDataSource(korbanKecelakaanDtos);

			parameters.put("DATA_LAKA_KEC", selectedDto.getLokasiDesc());
			parameters.put("DATA_LAKA_INSTANSI", selectedDto.getNamaInstansi());
			parameters.put("DATA_LAKA_NO_LP", selectedDto.getNoLaporanPolisi());
			parameters.put("DATA_LAKA_SIFAT", selectedDto.getSifatKecelakaanDesc());
			parameters.put("DATA_LAKA_LOKET", dataLakaReport.getAsalBerkasDesc());
			parameters.put("DATA_LAKA_PETUGAS", dataLakaReport.getNamaPetugas());
			parameters.put("DATA_LAKA_TGL_LP", sdf.format(dataLakaReport.getTglLaporanPolisi()));
			parameters.put("DATA_LAKA_URAIAN_LOKASI", dataLakaReport.getDeskripsiLokasi());
			parameters.put("DATA_LAKA_KASUS", dataLakaReport.getKasusKecelakaanDesc());
			parameters.put("DATA_LAKA_URAIAN_KEJADIAN", dataLakaReport.getDeskripsiKecelakaan());
			parameters.put("DATA_LAKA_URAIAN_KEJADIAN", dataLakaReport.getDeskripsiKecelakaan());
			parameters.put("DATA_LAKA_WAKTU_HARI", hari(dataLakaReport.getTglKejadian()));
			parameters.put("DATA_LAKA_WAKTU_TGL", sdf.format(dataLakaReport.getTglKejadian()));
			parameters.put("DATA_LAKA_WAKTU_JAM", sdf2.format(dataLakaReport.getTglKejadian()));
			parameters.put("LIST_KENDARAAN", listKendaraan);
			parameters.put("LIST_KORBAN", listKorban);
			parameters.put("DIUPDATE_TANGGAL", dataLakaReport.getLastUpdatedDate() == null ? sdf.format(date)
					: sdf.format(dataLakaReport.getLastUpdatedDate()));
			parameters.put("DICETAK_TANGGAL", sdf.format(new Date()));
			parameters.put("NAMA_UPDATE", userUpdate.getUserName());
			parameters.put("NAMA_CETAK", userCetak.getUserName());
			parameters.put("JABATAN_UPDATE", userUpdate.getDescription());
			parameters.put("JABATAN_CETAK", userCetak.getDescription());

			File temp = File.createTempFile("DATA KECELAKAAN DOCUMENT", ".pdf");
			FileOutputStream outs = new FileOutputStream(temp);
			File tempJsp = File.createTempFile("Jasper", ".jrxml");
			JasperReport jasper = null;
			try {
				ByteArrayOutputStream baos = getByteArrayOutputStream("DataLakaReport.jrxml");
				try (OutputStream outputStream = new FileOutputStream(tempJsp)) {
					baos.writeTo(outputStream);
					outputStream.close();
				} catch (Exception x) {
					x.printStackTrace();
				}
				jasper = JasperCompileManager.compileReport(tempJsp.getAbsolutePath());
			} catch (Exception s) {
				jasper = JasperCompileManager.compileReport("/home/glassfish/report/DataLakaReport.jrxml");
			}
			JasperPrint jasperReport = JasperFillManager.fillReport(jasper, parameters, new JREmptyDataSource(1));
			jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql",
					"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");

			// PDF-Export
			JRPdfExporter exportPdf = new JRPdfExporter();
			System.out.println(temp.getAbsolutePath());
			exportPdf.setParameter(JRExporterParameter.JASPER_PRINT, jasperReport);
			exportPdf.setParameter(JRExporterParameter.OUTPUT_STREAM, outs);
			exportPdf.exportReport();

			// show exported pdf
			File f = new File(temp.getPath());
			byte[] buffer = new byte[(int) f.length()];
			FileInputStream fs = new FileInputStream(f);
			fs.read(buffer);
			fs.close();
			ByteArrayInputStream is = new ByteArrayInputStream(buffer);
			this.fileContent = new AMedia("report", "pdf", "application/pdf", is);

		} catch (Exception p) {
			p.printStackTrace();
		}
		Map<String, Object> args = new HashMap<>();
		args.put("media", fileContent);
		((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH + popup, null, args)).doModal();
//		System.out.println(JsonUtil.getJson(map));

	}

	private ByteArrayOutputStream getByteArrayOutputStream(String... param) throws IOException, FileNotFoundException {
		URL url;
		String reportName = "";
		if (param.length > 0) {
			url = new URL(getLoc() + "/" + param[0]);
		} else {
			url = new URL(getLoc() + "/" + param[0]);

		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			is = url.openStream();
			byte[] byteChunk = new byte[4096];
			int n;
			while ((n = is.read(byteChunk)) > 0) {
				baos.write(byteChunk, 0, n);
			}
		} catch (IOException e) {
			System.err.printf("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
			e.printStackTrace();
		} finally {
			if (is != null) {
				is.close();
			}
		}

		return baos;
	}

	private String getLoc() {
		String port = (Executions.getCurrent().getServerPort() == 80) ? ""
				: (":" + Executions.getCurrent().getServerPort());
		/*
		 * String url = Executions.getCurrent().getScheme() + "://" +
		 * Executions.getCurrent().getServerName() + port +
		 * Executions.getCurrent().getContextPath() +
		 * Executions.getCurrent().getDesktop().getRequestPath();
		 */
		String x = "";
		if (System.getProperty("location") != null) {
			if (System.getProperty("location").equalsIgnoreCase("linux")) {
				x = "/home/glassfish/report";
			} else {
				x = Executions.getCurrent().getScheme() + "://" + Executions.getCurrent().getServerName() + port
						+ Executions.getCurrent().getContextPath() + "/report";
			}
		} else {
			x = Executions.getCurrent().getScheme() + "://" + Executions.getCurrent().getServerName() + port
					+ Executions.getCurrent().getContextPath() + "/report";
		}
		return x;
	}

	protected String hari(Date tgl) {
		if (tgl == null) {
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("E", Locale.ENGLISH);
		String ha = sdf.format(tgl);
		return ha.equalsIgnoreCase("Mon") ? "Senin"
				: ha.equalsIgnoreCase("Tue") ? "Selasa"
						: ha.equalsIgnoreCase("Wed") ? "Rabu"
								: ha.equalsIgnoreCase("Thu") ? "Kamis"
										: ha.equalsIgnoreCase("Fri") ? "Jum'at"
												: ha.equalsIgnoreCase("Sat") ? "Sabtu"
														: ha.equalsIgnoreCase("Sun") ? "Minggu" : "";
	}

	@Command
	public void cekKtp() {
		try {
			dukcapilWinDto = getDataFromDukcapil(plKorbanKecelakaanDto.getNoIdentitas());
			plKorbanKecelakaanDto.setNama(dukcapilWinDto.getNamaLengkap());
			plKorbanKecelakaanDto.setAlamat(dukcapilWinDto.getAlamat());

//			=============UMUR=============
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String tgl[] = sdf.format(dukcapilWinDto.getTglLahir()).split("/");
			int day = Integer.parseInt(tgl[0]);
			int month = Integer.parseInt(tgl[1]);
			int year = Integer.parseInt(tgl[2]);

			LocalDate l1 = LocalDate.of(year, month, day);
			LocalDate now1 = LocalDate.now();
			Period diff1 = Period.between(l1, now1);
			plKorbanKecelakaanDto.setUmur(new BigDecimal(diff1.getYears()));

//			=============GENDER==================
			String first = dukcapilWinDto.getJnsKelamin().substring(0, 1);
			if (first.equalsIgnoreCase("L")) {
				plKorbanKecelakaanDto.setJenisKelamin("Pria");
			} else if (first.equalsIgnoreCase("P")) {
				plKorbanKecelakaanDto.setJenisKelamin("Wanita");
			}
			System.err.println("luthfi44 " + dukcapilWinDto.getJnsKelamin() + " " + dukcapilWinDto.getTglLahir() + " "
					+ dukcapilWinDto.getStatusNikah());
			System.err.println("luthfi99 " + dukcapilWinDto.getKodeKecamatan() + " " + dukcapilWinDto.getKodeKabupaten()
					+ " " + dukcapilWinDto.getKodePropinsi());

			String kodeProv = "";
			String kodeKab = "";
			String kodeCamat = "";
			if (dukcapilWinDto.getKodePropinsi().length() == 1) {
				kodeProv = "0" + dukcapilWinDto.getKodePropinsi();
			} else {
				kodeProv = dukcapilWinDto.getKodePropinsi();
			}

			if (dukcapilWinDto.getKodeKabupaten().length() == 1) {
				kodeKab = "0" + dukcapilWinDto.getKodeKabupaten();
			} else {
				kodeKab = dukcapilWinDto.getKodeKabupaten();
			}

			if (dukcapilWinDto.getKodeKecamatan().length() == 1) {
				kodeCamat = "0" + dukcapilWinDto.getKodeKecamatan();
			} else {
				kodeCamat = dukcapilWinDto.getKodeKecamatan();
			}

			System.err.println("luthfi99 " + kodeCamat + " " + kodeKab + " " + kodeProv);

			listProvinsiKorban();
			provinsiKorbanDto.setKodeProvinsi(kodeProv);
			kabKotaKorbanDto.setKodeKabkota(kodeProv + kodeKab);
			camatDtoKorban.setKodeCamat(kodeProv + kodeKab + kodeCamat);

			for (FndCamatDto prov : listProvinsiKorbanDto) {
				if (provinsiKorbanDto.getKodeProvinsi().equalsIgnoreCase(prov.getKodeProvinsi())) {
					provinsiKorbanDto = new FndCamatDto();
					provinsiKorbanDto = prov;
				}
			}
			getKabkotaKorban();

			for (FndCamatDto kab : listKabKotaKorbanDto) {
				if (kabKotaKorbanDto.getKodeKabkota().equalsIgnoreCase(kab.getKodeKabkota())) {
					kabKotaKorbanDto = new FndCamatDto();
					kabKotaKorbanDto = kab;
				}
			}
			getCamatKorban();

			for (FndCamatDto camat : listCamatKorbanDto) {
				if (camatDtoKorban.getKodeCamat().equalsIgnoreCase(camat.getKodeCamat())) {
					camatDtoKorban = new FndCamatDto();
					camatDtoKorban = camat;
				}
			}

			BindUtils.postNotifyChange(null, null, this, "dukcapilWinDto");
			BindUtils.postNotifyChange(null, null, this, "plKorbanKecelakaanDto");
			BindUtils.postNotifyChange(null, null, this, "provinsiKorbanDto");
			BindUtils.postNotifyChange(null, null, this, "kabKotaKorbanDto");
			BindUtils.postNotifyChange(null, null, this, "camatDtoKorban");

		} catch (Exception s) {
			s.printStackTrace();
		}
	}

//	====================================BANDBOX ASAL BERKAS================================================
	@Command
	public void searchAsalBerkas(@BindingParam("val") String search) {
		loadAsalBerkas(search);
	}

	private void loadAsalBerkas(String search) {
		Map<String, Object> map = new HashMap<>();
		map.put("search", search);
		RestResponse rest = callWs(WS_URI_LOV + "/getAsalBerkas", map, HttpMethod.POST);
		try {
			listAsalBerkasDto = JsonUtil.mapJsonToListObject(rest.getContents(), FndKantorJasaraharjaDto.class);
			BindUtils.postNotifyChange(null, null, this, "listAsalBerkasDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void pilihAsalBerkas(@BindingParam("item") FndKantorJasaraharjaDto asalBerkas) {
		this.asalBerkasDto = asalBerkas;
		
		String kodeKantor = this.asalBerkasDto.getKodeKantorJr();
		logger.info("DEBUG loadInstansiByKodeKantorAsalBerkas=" + kodeKantor);
		loadInstansiByKodeKantorAsalBerkas(kodeKantor);
		
		BindUtils.postNotifyChange(null, null, this, "asalBerkasDto");
	}

	private void loadInstansiByKodeKantorAsalBerkas(String kodeKantor) {
		Map<String, Object> map = new HashMap<>();
		map.put("search", "");
		RestResponse rest = callWs(WS_URI_LOV + "/getListInstansi", map, HttpMethod.POST);
		List<PlInstansiDto> temp1 = listInstansiDto;
		try {
			listInstansiDto = new ArrayList<>();
			List<PlInstansiDto> temp = JsonUtil.mapJsonToListObject(rest.getContents(), PlInstansiDto.class);
			for (PlInstansiDto instansi : temp) {
				String pre = kodeKantor.substring(0, 2);
				if (instansi.getKodeInstansi().substring(0, 2).equals(pre)) {
					listInstansiDto.add(instansi);
				}
			}
			BindUtils.postNotifyChange(null, null, this, "listInstansiDto");
		} catch (Exception e) {
			listInstansiDto = temp1;
			e.printStackTrace();
		}
	}

//	====================================BANDBOX INSTANSI================================================
	@Command
	public void searchInstansi(@BindingParam("val") String search) {
		loadInstansi(search);
	}

	private void loadInstansi(String search) {
		Map<String, Object> map = new HashMap<>();
		map.put("search", search);
		RestResponse rest = callWs(WS_URI_LOV + "/getListInstansi", map, HttpMethod.POST);
		try {
			listInstansiDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlInstansiDto.class);
			BindUtils.postNotifyChange(null, null, this, "listInstansiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void pilihInstansi(@BindingParam("item") PlInstansiDto instansiDto) {
		this.instansiDto = instansiDto;
		BindUtils.postNotifyChange(null, null, this, "instansiDto");
	}

//	====================================BANDBOX SAMSAT================================================
	@Command
	public void searchSamsat(@BindingParam("val") String search) {
		loadSamsat(search);
	}

	private void loadSamsat(String search) {
		Map<String, Object> map = new HashMap<>();
		map.put("search", search);
		RestResponse rest = callWs(WS_URI_LOV + "/getListSamsat", map, HttpMethod.POST);
		try {
			listSamsatDto = JsonUtil.mapJsonToListObject(rest.getContents(), FndKantorJasaraharjaDto.class);
			BindUtils.postNotifyChange(null, null, this, "listSamsatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void pilihSamsat(@BindingParam("item") FndKantorJasaraharjaDto samsatDto) {
		this.samsatDto = samsatDto;
		BindUtils.postNotifyChange(null, null, this, "samsatDto");
	}

	@Command
	public void cekData() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String tglKejadianInput = tglKejadian == null ? "" : sdf.format(tglKejadian);
		String tglLaporanInput = plDataKecelakaanDto.getTglLaporanPolisi() == null ? ""
				: sdf.format(plDataKecelakaanDto.getTglLaporanPolisi());
		String kodeInstansiInput = instansiDto.getKodeInstansi() == null ? "" : instansiDto.getKodeInstansi();

		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("tglKejadian", tglKejadianInput);
		mapInput.put("tglLaporan", tglLaporanInput);
		mapInput.put("kodeInstansi", kodeInstansiInput);
		RestResponse rest = callWs(WS_URI + "/cekData", mapInput, HttpMethod.POST);
		try {
			listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(rest.getContents(), PlDataKecelakaanDto.class);
			if (listPlDataKecelakaanDtos.size() > 0) {
				setAlertMsgVisibleTglLaporan(true);
				if (tglKejadianInput.equalsIgnoreCase("")) {
					setAlertMsgVisible(false);
				} else {
					setAlertMsgVisible(true);
				}
			} else {
				setAlertMsgVisible(false);
				setAlertMsgVisibleTglLaporan(false);
			}
			BindUtils.postNotifyChange(null, null, this, "alertMsgVisible");
			BindUtils.postNotifyChange(null, null, this, "alertMsgVisibleTglLaporan");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void cekDataClick(@BindingParam("window") Window win) {
		closeDetail(win);

		Map<String, Object> map = new HashMap<>();
		map.put("kejadianStartDate", tglKejadian == null ? new Date() : tglKejadian);
		map.put("kejadianEndDate", tglKejadian == null ? new Date() : tglKejadian);
		map.put("laporanStartDate", plDataKecelakaanDto.getTglLaporanPolisi() == null ? new Date()
				: plDataKecelakaanDto.getTglLaporanPolisi());
		map.put("laporanEndDate", plDataKecelakaanDto.getTglLaporanPolisi() == null ? new Date()
				: plDataKecelakaanDto.getTglLaporanPolisi());
		map.put("instansiDto", instansiDto.getKodeInstansi() == null ? "" : instansiDto.getKodeInstansi());
//		kejadianStartDate = tglKejadian==null?new Date():tglKejadian;
//		kejadianEndDate = tglKejadian==null?new Date():tglKejadian;
//		laporanStartDate = plDataKecelakaanDto.getTglLaporanPolisi()==null?new Date():plDataKecelakaanDto.getTglLaporanPolisi();
//		laporanEndDate = plDataKecelakaanDto.getTglLaporanPolisi()==null?new Date():plDataKecelakaanDto.getTglLaporanPolisi();
//		instansiDto.setKodeInstansi(instansiDto.getKodeInstansi()==null?"":instansiDto.getKodeInstansi());
//		BindUtils.postNotifyChange(null, null, this, "kejadianStartDate");
//		BindUtils.postNotifyChange(null, null, this, "kejadianEndDate");
//		BindUtils.postNotifyChange(null, null, this, "laporanStartDate");
//		BindUtils.postNotifyChange(null, null, this, "laporanEndDate");
//		BindUtils.postNotifyChange(null, null, this, "instansiDto");
		Executions.getCurrent().setAttribute("obj", map);
		searchIndex();
		loadList();
	}

	public List<DasiJrRefCodeDto> getListInstansiPembuatDto() {
		return listInstansiPembuatDto;
	}

	public void setListInstansiPembuatDto(List<DasiJrRefCodeDto> listInstansiPembuatDto) {
		this.listInstansiPembuatDto = listInstansiPembuatDto;
	}

	public List<PlJaminanDto> getListLingkupJaminanDto() {
		return listLingkupJaminanDto;
	}

	public void setListLingkupJaminanDto(List<PlJaminanDto> listLingkupJaminanDto) {
		this.listLingkupJaminanDto = listLingkupJaminanDto;
	}

	public List<PlJaminanDto> getListJenisPertanggunganDto() {
		return listJenisPertanggunganDto;
	}

	public void setListJenisPertanggunganDto(List<PlJaminanDto> listJenisPertanggunganDto) {
		this.listJenisPertanggunganDto = listJenisPertanggunganDto;
	}

	public List<DasiJrRefCodeDto> getListSifatCideraDto() {
		return listSifatCideraDto;
	}

	public void setListSifatCideraDto(List<DasiJrRefCodeDto> listSifatCideraDto) {
		this.listSifatCideraDto = listSifatCideraDto;
	}

	public List<DasiJrRefCodeDto> getListSifatKecelakaanDto() {
		return listSifatKecelakaanDto;
	}

	public void setListSifatKecelakaanDto(List<DasiJrRefCodeDto> listSifatKecelakaanDto) {
		this.listSifatKecelakaanDto = listSifatKecelakaanDto;
	}

	public List<FndLokasiDto> getListLokasiDto() {
		return listLokasiDto;
	}

	public void setListLokasiDto(List<FndLokasiDto> listLokasiDto) {
		this.listLokasiDto = listLokasiDto;
	}

	public List<FndKantorJasaraharjaDto> getListAsalBerkasDto() {
		return listAsalBerkasDto;
	}

	public void setListAsalBerkasDto(List<FndKantorJasaraharjaDto> listAsalBerkasDto) {
		this.listAsalBerkasDto = listAsalBerkasDto;
	}

	public List<FndKantorJasaraharjaDto> getListSamsatDto() {
		return listSamsatDto;
	}

	public void setListSamsatDto(List<FndKantorJasaraharjaDto> listSamsatDto) {
		this.listSamsatDto = listSamsatDto;
	}

	public List<PlInstansiDto> getListInstansiDto() {
		return listInstansiDto;
	}

	public void setListInstansiDto(List<PlInstansiDto> listInstansiDto) {
		this.listInstansiDto = listInstansiDto;
	}

	public String getAsalBerkas() {
		return asalBerkas;
	}

	public void setAsalBerkas(String asalBerkas) {
		this.asalBerkas = asalBerkas;
	}

	public String getSamsat() {
		return samsat;
	}

	public void setSamsat(String samsat) {
		this.samsat = samsat;
	}

	public Date getLaporanStartDate() {
		return laporanStartDate;
	}

	public void setLaporanStartDate(Date laporanStartDate) {
		this.laporanStartDate = laporanStartDate;
	}

	public Date getLaporanEndDate() {
		return laporanEndDate;
	}

	public void setLaporanEndDate(Date laporanEndDate) {
		this.laporanEndDate = laporanEndDate;
	}

	public String getInstansi() {
		return instansi;
	}

	public void setInstansi(String instansi) {
		this.instansi = instansi;
	}

	public String getInstansiPembuat() {
		return instansiPembuat;
	}

	public void setInstansiPembuat(String instansiPembuat) {
		this.instansiPembuat = instansiPembuat;
	}

	public String getNoLaporan() {
		return noLaporan;
	}

	public void setNoLaporan(String noLaporan) {
		this.noLaporan = noLaporan;
	}

	public String getLokasi() {
		return lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}

	public String getNamaKorban() {
		return namaKorban;
	}

	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}

	public String getNoIdentitas() {
		return noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getLingkupJaminan() {
		return lingkupJaminan;
	}

	public void setLingkupJaminan(String lingkupJaminan) {
		this.lingkupJaminan = lingkupJaminan;
	}

	public String getJenisPertanggungan() {
		return jenisPertanggungan;
	}

	public void setJenisPertanggungan(String jenisPertanggungan) {
		this.jenisPertanggungan = jenisPertanggungan;
	}

	public String getSifatCidera() {
		return sifatCidera;
	}

	public void setSifatCidera(String sifatCidera) {
		this.sifatCidera = sifatCidera;
	}

	public String getSifatKecelakaan() {
		return sifatKecelakaan;
	}

	public void setSifatKecelakaan(String sifatKecelakaan) {
		this.sifatKecelakaan = sifatKecelakaan;
	}

	public String getKecelakaanKatostrop() {
		return kecelakaanKatostrop;
	}

	public void setKecelakaanKatostrop(String kecelakaanKatostrop) {
		this.kecelakaanKatostrop = kecelakaanKatostrop;
	}

	public String getPerusahaanPenerbangan() {
		return perusahaanPenerbangan;
	}

	public void setPerusahaanPenerbangan(String perusahaanPenerbangan) {
		this.perusahaanPenerbangan = perusahaanPenerbangan;
	}

	public String getPerusahaanOtobus() {
		return perusahaanOtobus;
	}

	public void setPerusahaanOtobus(String perusahaanOtobus) {
		this.perusahaanOtobus = perusahaanOtobus;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public List<PlDataKecelakaanDto> getListPlDataKecelakaanDtos() {
		return listPlDataKecelakaanDtos;
	}

	public void setListPlDataKecelakaanDtos(List<PlDataKecelakaanDto> listPlDataKecelakaanDtos) {
		this.listPlDataKecelakaanDtos = listPlDataKecelakaanDtos;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSearchLokasi() {
		return searchLokasi;
	}

	public void setSearchLokasi(String searchLokasi) {
		this.searchLokasi = searchLokasi;
	}

	public FndLokasiDto getFndLokasiDto() {
		return fndLokasiDto;
	}

	public void setFndLokasiDto(FndLokasiDto fndLokasiDto) {
		this.fndLokasiDto = fndLokasiDto;
	}

	public PlDataKecelakaanDto getSelected() {
		return selected;
	}

	public void setSelected(PlDataKecelakaanDto selected) {
		this.selected = selected;
	}

	public PlDataKecelakaanDto getPlDataKecelakaanDto() {
		return plDataKecelakaanDto;
	}

	public void setPlDataKecelakaanDto(PlDataKecelakaanDto plDataKecelakaanDto) {
		this.plDataKecelakaanDto = plDataKecelakaanDto;
	}

	public String getSearchAsalBerkas() {
		return searchAsalBerkas;
	}

	public void setSearchAsalBerkas(String searchAsalBerkas) {
		this.searchAsalBerkas = searchAsalBerkas;
	}

	public String getSearchInstansi() {
		return searchInstansi;
	}

	public void setSearchInstansi(String searchInstansi) {
		this.searchInstansi = searchInstansi;
	}

	public String getSearchSamsat() {
		return searchSamsat;
	}

	public void setSearchSamsat(String searchSamsat) {
		this.searchSamsat = searchSamsat;
	}

	public String getSearchProvinsi() {
		return searchProvinsi;
	}

	public void setSearchProvinsi(String searchProvinsi) {
		this.searchProvinsi = searchProvinsi;
	}

	public String getSearchCamat() {
		return searchCamat;
	}

	public void setSearchCamat(String searchCamat) {
		this.searchCamat = searchCamat;
	}

	public List<FndCamatDto> getListProvinsiDto() {
		return listProvinsiDto;
	}

	public void setListProvinsiDto(List<FndCamatDto> listProvinsiDto) {
		this.listProvinsiDto = listProvinsiDto;
	}

	public List<FndCamatDto> getListKabkotaDto() {
		return listKabkotaDto;
	}

	public void setListKabkotaDto(List<FndCamatDto> listKabkotaDto) {
		this.listKabkotaDto = listKabkotaDto;
	}

	public List<FndCamatDto> getListCamatDto() {
		return listCamatDto;
	}

	public void setListCamatDto(List<FndCamatDto> listCamatDto) {
		this.listCamatDto = listCamatDto;
	}

	public FndCamatDto getCamatDto() {
		return camatDto;
	}

	public void setCamatDto(FndCamatDto camatDto) {
		this.camatDto = camatDto;
	}

	public FndCamatDto getProvinsiDto() {
		return provinsiDto;
	}

	public void setProvinsiDto(FndCamatDto provinsiDto) {
		this.provinsiDto = provinsiDto;
	}

	public FndCamatDto getKabKotaDto() {
		return kabKotaDto;
	}

	public void setKabKotaDto(FndCamatDto kabKotaDto) {
		this.kabKotaDto = kabKotaDto;
	}

	public FndKantorJasaraharjaDto getSamsatDto() {
		return samsatDto;
	}

	public void setSamsatDto(FndKantorJasaraharjaDto samsatDto) {
		this.samsatDto = samsatDto;
	}

	public DasiJrRefCodeDto getInstansiPembuatDto() {
		return instansiPembuatDto;
	}

	public void setInstansiPembuatDto(DasiJrRefCodeDto instansiPembuatDto) {
		this.instansiPembuatDto = instansiPembuatDto;
	}

	public boolean isFormTambahKendaraan() {
		return formTambahKendaraan;
	}

	public void setFormTambahKendaraan(boolean formTambahKendaraan) {
		this.formTambahKendaraan = formTambahKendaraan;
	}

	public boolean isListKendaraan() {
		return listKendaraan;
	}

	public void setListKendaraan(boolean listKendaraan) {
		this.listKendaraan = listKendaraan;
	}

	public boolean isFormTambahKorban() {
		return formTambahKorban;
	}

	public void setFormTambahKorban(boolean formTambahKorban) {
		this.formTambahKorban = formTambahKorban;
	}

	public boolean isListKorban() {
		return listKorban;
	}

	public void setListKorban(boolean listKorban) {
		this.listKorban = listKorban;
	}

	public Date getTglKejadian() {
		return tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public Date getJamKejadian() {
		return jamKejadian;
	}

	public void setJamKejadian(Date jamKejadian) {
		this.jamKejadian = jamKejadian;
	}

	public boolean isStatusLaporanGroupBox() {
		return statusLaporanGroupBox;
	}

	public void setStatusLaporanGroupBox(boolean statusLaporanGroupBox) {
		this.statusLaporanGroupBox = statusLaporanGroupBox;
	}

	public String getStatusLaporan() {
		return statusLaporan;
	}

	public void setStatusLaporan(String statusLaporan) {
		this.statusLaporan = statusLaporan;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<DasiJrRefCodeDto> getListKasusKecelakaanDto() {
		return listKasusKecelakaanDto;
	}

	public void setListKasusKecelakaanDto(List<DasiJrRefCodeDto> listKasusKecelakaanDto) {
		this.listKasusKecelakaanDto = listKasusKecelakaanDto;
	}

	public List<DasiJrRefCodeDto> getListStatusKendaraanDto() {
		return listStatusKendaraanDto;
	}

	public void setListStatusKendaraanDto(List<DasiJrRefCodeDto> listStatusKendaraanDto) {
		this.listStatusKendaraanDto = listStatusKendaraanDto;
	}

	public List<DasiJrRefCodeDto> getListJenisSimDto() {
		return listJenisSimDto;
	}

	public void setListJenisSimDto(List<DasiJrRefCodeDto> listJenisSimDto) {
		this.listJenisSimDto = listJenisSimDto;
	}

	public List<DasiJrRefCodeDto> getListMerkKendaraanDto() {
		return listMerkKendaraanDto;
	}

	public void setListMerkKendaraanDto(List<DasiJrRefCodeDto> listMerkKendaraanDto) {
		this.listMerkKendaraanDto = listMerkKendaraanDto;
	}

	public List<DasiJrRefCodeDto> getListIdentitasDto() {
		return listIdentitasDto;
	}

	public void setListIdentitasDto(List<DasiJrRefCodeDto> listIdentitasDto) {
		this.listIdentitasDto = listIdentitasDto;
	}

	public List<DasiJrRefCodeDto> getListJenisPekerjaanDto() {
		return listJenisPekerjaanDto;
	}

	public void setListJenisPekerjaanDto(List<DasiJrRefCodeDto> listJenisPekerjaanDto) {
		this.listJenisPekerjaanDto = listJenisPekerjaanDto;
	}

	public List<DasiJrRefCodeDto> getListStatusKorbanDto() {
		return listStatusKorbanDto;
	}

	public void setListStatusKorbanDto(List<DasiJrRefCodeDto> listStatusKorbanDto) {
		this.listStatusKorbanDto = listStatusKorbanDto;
	}

	public List<DasiJrRefCodeDto> getListStatusNikahDto() {
		return listStatusNikahDto;
	}

	public void setListStatusNikahDto(List<DasiJrRefCodeDto> listStatusNikahDto) {
		this.listStatusNikahDto = listStatusNikahDto;
	}

	public PlAngkutanKecelakaanDto getPlAngkutanKecelakaanDto() {
		return plAngkutanKecelakaanDto;
	}

	public void setPlAngkutanKecelakaanDto(PlAngkutanKecelakaanDto plAngkutanKecelakaanDto) {
		this.plAngkutanKecelakaanDto = plAngkutanKecelakaanDto;
	}

	public PlKorbanKecelakaanDto getPlKorbanKecelakaanDto() {
		return plKorbanKecelakaanDto;
	}

	public void setPlKorbanKecelakaanDto(PlKorbanKecelakaanDto plKorbanKecelakaanDto) {
		this.plKorbanKecelakaanDto = plKorbanKecelakaanDto;
	}

	public String getDETAIL_PAGE_PATH() {
		return DETAIL_PAGE_PATH;
	}

	public FndKantorJasaraharjaDto getAsalBerkasDto() {
		return asalBerkasDto;
	}

	public void setAsalBerkasDto(FndKantorJasaraharjaDto asalBerkasDto) {
		this.asalBerkasDto = asalBerkasDto;
	}

	public PlInstansiDto getInstansiDto() {
		return instansiDto;
	}

	public void setInstansiDto(PlInstansiDto instansiDto) {
		this.instansiDto = instansiDto;
	}

	public PlJaminanDto getLingkupJaminanDto() {
		return lingkupJaminanDto;
	}

	public void setLingkupJaminanDto(PlJaminanDto lingkupJaminanDto) {
		this.lingkupJaminanDto = lingkupJaminanDto;
	}

	public PlJaminanDto getJenisPertanggunganDto() {
		return jenisPertanggunganDto;
	}

	public void setJenisPertanggunganDto(PlJaminanDto jenisPertanggunganDto) {
		this.jenisPertanggunganDto = jenisPertanggunganDto;
	}

	public DasiJrRefCodeDto getSifatCideraDto() {
		return sifatCideraDto;
	}

	public void setSifatCideraDto(DasiJrRefCodeDto sifatCideraDto) {
		this.sifatCideraDto = sifatCideraDto;
	}

	public DasiJrRefCodeDto getSifatKecelakaanDto() {
		return sifatKecelakaanDto;
	}

	public void setSifatKecelakaanDto(DasiJrRefCodeDto sifatKecelakaanDto) {
		this.sifatKecelakaanDto = sifatKecelakaanDto;
	}

	public DasiJrRefCodeDto getKasusKecelakaanDto() {
		return kasusKecelakaanDto;
	}

	public void setKasusKecelakaanDto(DasiJrRefCodeDto kasusKecelakaanDto) {
		this.kasusKecelakaanDto = kasusKecelakaanDto;
	}

	public FndLokasiDto getLokasiDto() {
		return lokasiDto;
	}

	public void setLokasiDto(FndLokasiDto lokasiDto) {
		this.lokasiDto = lokasiDto;
	}

	public DasiJrRefCodeDto getStatusKendaraanDto() {
		return statusKendaraanDto;
	}

	public void setStatusKendaraanDto(DasiJrRefCodeDto statusKendaraanDto) {
		this.statusKendaraanDto = statusKendaraanDto;
	}

	public String getSearchKabkota() {
		return searchKabkota;
	}

	public void setSearchKabkota(String searchKabkota) {
		this.searchKabkota = searchKabkota;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isSuratKeterangan() {
		return suratKeterangan;
	}

	public void setSuratKeterangan(boolean suratKeterangan) {
		this.suratKeterangan = suratKeterangan;
	}

	public boolean isDataIrsms() {
		return dataIrsms;
	}

	public void setDataIrsms(boolean dataIrsms) {
		this.dataIrsms = dataIrsms;
	}

	public boolean isDataMutasi() {
		return dataMutasi;
	}

	public void setDataMutasi(boolean dataMutasi) {
		this.dataMutasi = dataMutasi;
	}

	public String getLaporan1() {
		return laporan1;
	}

	public void setLaporan1(String laporan1) {
		this.laporan1 = laporan1;
	}

	public String getLaporan2() {
		return laporan2;
	}

	public void setLaporan2(String laporan2) {
		this.laporan2 = laporan2;
	}

	public String getLaporan3() {
		return laporan3;
	}

	public void setLaporan3(String laporan3) {
		this.laporan3 = laporan3;
	}

	public FndCamatDto getCamatDtoPemilik() {
		return camatDtoPemilik;
	}

	public void setCamatDtoPemilik(FndCamatDto camatDtoPemilik) {
		this.camatDtoPemilik = camatDtoPemilik;
	}

	public List<PlAngkutanKecelakaanDto> getListPlAngkutanKecelakaanDtos() {
		return listPlAngkutanKecelakaanDtos;
	}

	public void setListPlAngkutanKecelakaanDtos(List<PlAngkutanKecelakaanDto> listPlAngkutanKecelakaanDtos) {
		this.listPlAngkutanKecelakaanDtos = listPlAngkutanKecelakaanDtos;
	}

	public Date getKejadianStartDate() {
		return kejadianStartDate;
	}

	public void setKejadianStartDate(Date kejadianStartDate) {
		this.kejadianStartDate = kejadianStartDate;
	}

	public Date getKejadianEndDate() {
		return kejadianEndDate;
	}

	public void setKejadianEndDate(Date kejadianEndDate) {
		this.kejadianEndDate = kejadianEndDate;
	}

	public List<FndJenisKendaraanDto> getListJenisKendaraanDto() {
		return listJenisKendaraanDto;
	}

	public void setListJenisKendaraanDto(List<FndJenisKendaraanDto> listJenisKendaraanDto) {
		this.listJenisKendaraanDto = listJenisKendaraanDto;
	}

	public FndJenisKendaraanDto getJenisKendaraanDto() {
		return jenisKendaraanDto;
	}

	public void setJenisKendaraanDto(FndJenisKendaraanDto jenisKendaraanDto) {
		this.jenisKendaraanDto = jenisKendaraanDto;
	}

	public PlMappingCamatDto getPlMappingCamatDto() {
		return plMappingCamatDto;
	}

	public void setPlMappingCamatDto(PlMappingCamatDto plMappingCamatDto) {
		this.plMappingCamatDto = plMappingCamatDto;
	}

	public boolean isFlagSaveDataLaka() {
		return flagSaveDataLaka;
	}

	public void setFlagSaveDataLaka(boolean flagSaveDataLaka) {
		this.flagSaveDataLaka = flagSaveDataLaka;
	}

	public DasiJrRefCodeDto getJenisSimDto() {
		return jenisSimDto;
	}

	public void setJenisSimDto(DasiJrRefCodeDto jenisSimDto) {
		this.jenisSimDto = jenisSimDto;
	}

	public DasiJrRefCodeDto getMerkKendaraanDto() {
		return merkKendaraanDto;
	}

	public void setMerkKendaraanDto(DasiJrRefCodeDto merkKendaraanDto) {
		this.merkKendaraanDto = merkKendaraanDto;
	}

	public DasiJrRefCodeDto getJenisIdentitasDto() {
		return jenisIdentitasDto;
	}

	public void setJenisIdentitasDto(DasiJrRefCodeDto jenisIdentitasDto) {
		this.jenisIdentitasDto = jenisIdentitasDto;
	}

	public DasiJrRefCodeDto getJenisPekerjaanDto() {
		return jenisPekerjaanDto;
	}

	public void setJenisPekerjaanDto(DasiJrRefCodeDto jenisPekerjaanDto) {
		this.jenisPekerjaanDto = jenisPekerjaanDto;
	}

	public DasiJrRefCodeDto getStatusKorbanDto() {
		return statusKorbanDto;
	}

	public void setStatusKorbanDto(DasiJrRefCodeDto statusKorbanDto) {
		this.statusKorbanDto = statusKorbanDto;
	}

	public DasiJrRefCodeDto getStatusNikahDto() {
		return statusNikahDto;
	}

	public void setStatusNikahDto(DasiJrRefCodeDto statusNikahDto) {
		this.statusNikahDto = statusNikahDto;
	}

	public List<PlKorbanKecelakaanDto> getListPlKorbanKecelakaanDtos() {
		return listPlKorbanKecelakaanDtos;
	}

	public void setListPlKorbanKecelakaanDtos(List<PlKorbanKecelakaanDto> listPlKorbanKecelakaanDtos) {
		this.listPlKorbanKecelakaanDtos = listPlKorbanKecelakaanDtos;
	}

	public PlAngkutanKecelakaanDto getKendaraanPenjaminDto() {
		return kendaraanPenjaminDto;
	}

	public void setKendaraanPenjaminDto(PlAngkutanKecelakaanDto kendaraanPenjaminDto) {
		this.kendaraanPenjaminDto = kendaraanPenjaminDto;
	}

	public PlAngkutanKecelakaanDto getKendaraanPosisiDto() {
		return kendaraanPosisiDto;
	}

	public void setKendaraanPosisiDto(PlAngkutanKecelakaanDto kendaraanPosisiDto) {
		this.kendaraanPosisiDto = kendaraanPosisiDto;
	}

	public FndCamatDto getProvinsiPengemudiDto() {
		return provinsiPengemudiDto;
	}

	public void setProvinsiPengemudiDto(FndCamatDto provinsiPengemudiDto) {
		this.provinsiPengemudiDto = provinsiPengemudiDto;
	}

	public FndCamatDto getKabkotaPengemudiDto() {
		return kabkotaPengemudiDto;
	}

	public void setKabkotaPengemudiDto(FndCamatDto kabkotaPengemudiDto) {
		this.kabkotaPengemudiDto = kabkotaPengemudiDto;
	}

	public FndCamatDto getCamatDtoPengemudi() {
		return camatDtoPengemudi;
	}

	public void setCamatDtoPengemudi(FndCamatDto camatDtoPengemudi) {
		this.camatDtoPengemudi = camatDtoPengemudi;
	}

	public FndCamatDto getProvinsiPemilikDto() {
		return provinsiPemilikDto;
	}

	public void setProvinsiPemilikDto(FndCamatDto provinsiPemilikDto) {
		this.provinsiPemilikDto = provinsiPemilikDto;
	}

	public FndCamatDto getKabKotaPemilikDto() {
		return kabKotaPemilikDto;
	}

	public void setKabKotaPemilikDto(FndCamatDto kabKotaPemilikDto) {
		this.kabKotaPemilikDto = kabKotaPemilikDto;
	}

	public PlNihilKecelakaanDto getPlNihilKecelakaanDto() {
		return plNihilKecelakaanDto;
	}

	public void setPlNihilKecelakaanDto(PlNihilKecelakaanDto plNihilKecelakaanDto) {
		this.plNihilKecelakaanDto = plNihilKecelakaanDto;
	}

	public String getSearchKendaraan() {
		return searchKendaraan;
	}

	public void setSearchKendaraan(String searchKendaraan) {
		this.searchKendaraan = searchKendaraan;
	}

	public String getSearchKorban() {
		return searchKorban;
	}

	public void setSearchKorban(String searchKorban) {
		this.searchKorban = searchKorban;
	}

	public FndCamatDto getProvinsiKorbanDto() {
		return provinsiKorbanDto;
	}

	public void setProvinsiKorbanDto(FndCamatDto provinsiKorbanDto) {
		this.provinsiKorbanDto = provinsiKorbanDto;
	}

	public FndCamatDto getKabKotaKorbanDto() {
		return kabKotaKorbanDto;
	}

	public void setKabKotaKorbanDto(FndCamatDto kabKotaKorbanDto) {
		this.kabKotaKorbanDto = kabKotaKorbanDto;
	}

	public FndCamatDto getCamatDtoKorban() {
		return camatDtoKorban;
	}

	public void setCamatDtoKorban(FndCamatDto camatDtoKorban) {
		this.camatDtoKorban = camatDtoKorban;
	}

	public List<FndCamatDto> getListKabKotaPemilikDto() {
		return listKabKotaPemilikDto;
	}

	public void setListKabKotaPemilikDto(List<FndCamatDto> listKabKotaPemilikDto) {
		this.listKabKotaPemilikDto = listKabKotaPemilikDto;
	}

	public List<FndCamatDto> getListCamatDtoPemilik() {
		return listCamatDtoPemilik;
	}

	public void setListCamatDtoPemilik(List<FndCamatDto> listCamatDtoPemilik) {
		this.listCamatDtoPemilik = listCamatDtoPemilik;
	}

	public UserSessionJR getUserSession() {
		return userSession;
	}

	public void setUserSession(UserSessionJR userSession) {
		this.userSession = userSession;
	}

	public boolean isFlagNihilRegister() {
		return flagNihilRegister;
	}

	public void setFlagNihilRegister(boolean flagNihilRegister) {
		this.flagNihilRegister = flagNihilRegister;
	}

	public boolean isFlagDataLakaRegister() {
		return flagDataLakaRegister;
	}

	public void setFlagDataLakaRegister(boolean flagDataLakaRegister) {
		this.flagDataLakaRegister = flagDataLakaRegister;
	}

	public boolean isFlagDataLakaSantunan() {
		return flagDataLakaSantunan;
	}

	public void setFlagDataLakaSantunan(boolean flagDataLakaSantunan) {
		this.flagDataLakaSantunan = flagDataLakaSantunan;
	}

	public PlMappingPoldaDto getPoldaDto() {
		return poldaDto;
	}

	public void setPoldaDto(PlMappingPoldaDto poldaDto) {
		this.poldaDto = poldaDto;
	}

	public List<AuthUserDto> getAuthUserDtos() {
		return authUserDtos;
	}

	public void setAuthUserDtos(List<AuthUserDto> authUserDtos) {
		this.authUserDtos = authUserDtos;
	}

	public AuthUserDto getAuthUserDto() {
		return authUserDto;
	}

	public void setAuthUserDto(AuthUserDto authUserDto) {
		this.authUserDto = authUserDto;
	}

	public List<FndCamatDto> getListProvinsiKorbanDto() {
		return listProvinsiKorbanDto;
	}

	public void setListProvinsiKorbanDto(List<FndCamatDto> listProvinsiKorbanDto) {
		this.listProvinsiKorbanDto = listProvinsiKorbanDto;
	}

	public List<FndCamatDto> getListKabKotaKorbanDto() {
		return listKabKotaKorbanDto;
	}

	public void setListKabKotaKorbanDto(List<FndCamatDto> listKabKotaKorbanDto) {
		this.listKabKotaKorbanDto = listKabKotaKorbanDto;
	}

	public List<FndCamatDto> getListCamatKorbanDto() {
		return listCamatKorbanDto;
	}

	public void setListCamatKorbanDto(List<FndCamatDto> listCamatKorbanDto) {
		this.listCamatKorbanDto = listCamatKorbanDto;
	}

	public String getKatastrop() {
		return katastrop;
	}

	public void setKatastrop(String katastrop) {
		this.katastrop = katastrop;
	}

	public List<String> getListKatastrop() {
		return listKatastrop;
	}

	public void setListKatastrop(List<String> listKatastrop) {
		this.listKatastrop = listKatastrop;
	}

	public List<PlDataKecelakaanDto> getListPlDataKecelakaanDtosCopy() {
		return listPlDataKecelakaanDtosCopy;
	}

	public void setListPlDataKecelakaanDtosCopy(List<PlDataKecelakaanDto> listPlDataKecelakaanDtosCopy) {
		this.listPlDataKecelakaanDtosCopy = listPlDataKecelakaanDtosCopy;
	}

	public boolean isFlagAddDataKendaraan() {
		return flagAddDataKendaraan;
	}

	public void setFlagAddDataKendaraan(boolean flagAddDataKendaraan) {
		this.flagAddDataKendaraan = flagAddDataKendaraan;
	}

	public boolean isFlagAddDataKorban() {
		return flagAddDataKorban;
	}

	public void setFlagAddDataKorban(boolean flagAddDataKorban) {
		this.flagAddDataKorban = flagAddDataKorban;
	}

	public List<FndKantorJasaraharjaDto> getListAsalBerkasDtoCopy() {
		return listAsalBerkasDtoCopy;
	}

	public void setListAsalBerkasDtoCopy(List<FndKantorJasaraharjaDto> listAsalBerkasDtoCopy) {
		this.listAsalBerkasDtoCopy = listAsalBerkasDtoCopy;
	}

	public List<FndKantorJasaraharjaDto> getListSamsatDtoCopy() {
		return listSamsatDtoCopy;
	}

	public void setListSamsatDtoCopy(List<FndKantorJasaraharjaDto> listSamsatDtoCopy) {
		this.listSamsatDtoCopy = listSamsatDtoCopy;
	}

	public List<PlInstansiDto> getListInstansiDtoCopy() {
		return listInstansiDtoCopy;
	}

	public void setListInstansiDtoCopy(List<PlInstansiDto> listInstansiDtoCopy) {
		this.listInstansiDtoCopy = listInstansiDtoCopy;
	}

	public List<FndLokasiDto> getListLokasiDtoCopy() {
		return listLokasiDtoCopy;
	}

	public void setListLokasiDtoCopy(List<FndLokasiDto> listLokasiDtoCopy) {
		this.listLokasiDtoCopy = listLokasiDtoCopy;
	}

	public String getConstraintAsalBerkas() {
		return constraintAsalBerkas;
	}

	public void setConstraintAsalBerkas(String constraintAsalBerkas) {
		this.constraintAsalBerkas = constraintAsalBerkas;
	}

	public String getConstraintInstansi() {
		return constraintInstansi;
	}

	public void setConstraintInstansi(String constraintInstansi) {
		this.constraintInstansi = constraintInstansi;
	}

	public String getConstraintSamsat() {
		return constraintSamsat;
	}

	public void setConstraintSamsat(String constraintSamsat) {
		this.constraintSamsat = constraintSamsat;
	}

	public DukcapilWinDto getDukcapilWinDto() {
		return dukcapilWinDto;
	}

	public void setDukcapilWinDto(DukcapilWinDto dukcapilWinDto) {
		this.dukcapilWinDto = dukcapilWinDto;
	}

	public String getStatusData() {
		return statusData;
	}

	public void setStatusData(String statusData) {
		this.statusData = statusData;
	}

	public boolean isAlertMsgVisible() {
		return alertMsgVisible;
	}

	public void setAlertMsgVisible(boolean alertMsgVisible) {
		this.alertMsgVisible = alertMsgVisible;
	}

	public boolean isAlertMsgVisibleTglLaporan() {
		return alertMsgVisibleTglLaporan;
	}

	public void setAlertMsgVisibleTglLaporan(boolean alertMsgVisibleTglLaporan) {
		this.alertMsgVisibleTglLaporan = alertMsgVisibleTglLaporan;
	}

}
