package ui.operasional;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.time.Hour;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zul.Window;

import Bpm.ListTaskByUser.UserTaskService;

import com.fasterxml.jackson.annotation.JsonFormat.Value;

import Bpm.ListTaskByUser.UserTask;
import share.DasiJrRefCodeDto;
import share.FndBankDto;
import share.FndKantorJasaraharjaDto;
import share.PlAdditionalDescDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import share.PlRegisterSementaraDto;
import share.PlRekeningRDto;
import share.PlTindakLanjutDto;
import ui.operasional.dokumen.CetakLdpb;
import ui.operasional.dokumen.DokumenDP2S;
import ui.operasional.dokumen.DokumenPendampingPenyelesaian;
import ui.operasional.dokumen.KuitansiPembayaranSantunan;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass = true)
public class OpPenyelesaianPengajuanVmd extends BaseVmd implements Serializable {

	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent().getAttribute(UIConstants.SESS_LOGIN_ID);

	private static final long serialVersionUID = 1L;
	private final String WS_URI = "/OperasionalPenyelesaianPengajuan";
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpPenyelesaianPengajuan/_index.zul";
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/PenyelesaianPengajuanDetail.zul";
	private final String RINGKASAN_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DetailRingkasanPengajuan.zul";
	private final String WS_URI_LOV = "/Lov";
	private final String WS_URI_RS = "/MasterRumahSakit";

	private final String WS_URI_SANTUNAN = "/OpPengajuanSantunan";

	private List<PlPengajuanSantunanDto> listPengajuanDto = new ArrayList<>();
	private PlPengajuanSantunanDto pengajuanDto = new PlPengajuanSantunanDto();
	private FndKantorJasaraharjaDto fndKantorDto = new FndKantorJasaraharjaDto();
	private PlPengajuanSantunanDto saveDto = new PlPengajuanSantunanDto();
	private List<FndKantorJasaraharjaDto> listFndKantorDto = new ArrayList<>();
	private List<FndBankDto> listBankDto = new ArrayList<>();
	private FndBankDto bankDto = new FndBankDto();
	private List<DasiJrRefCodeDto> listDokumenDto = new ArrayList<>();
	private DasiJrRefCodeDto dokumenDto = new DasiJrRefCodeDto();
	private PlPengajuanSantunanDto cetakKuitansiDto = new PlPengajuanSantunanDto();
	private List<PlPenyelesaianSantunanDto> listPenyelesaianKuitansiDto = new ArrayList<>();
	private PlPenyelesaianSantunanDto penyelesaianKuitansiDto = new PlPenyelesaianSantunanDto();
	private PlPengajuanSantunanDto dokumenDP2SDto = new PlPengajuanSantunanDto();
	private List<PlPenyelesaianSantunanDto> akumulasiPembayaran = new ArrayList<>();
	private DokumenPendampingPenyelesaian dokumenPendamping = new DokumenPendampingPenyelesaian();
	private List<DasiJrRefCodeDto> listBerkasDto = new ArrayList<>();
	private FndKantorJasaraharjaDto kantorDto = new FndKantorJasaraharjaDto();

	private List<PlPenyelesaianSantunanDto> listPlPenyelesaianSantunanDto = new ArrayList<>();
	private PlPenyelesaianSantunanDto plPenyelesaianSantunanDto = new PlPenyelesaianSantunanDto();

	private List<String> listPilihanPengajuan = new ArrayList<>();
	private String pengajuan;

	private List<PlAdditionalDescDto> listPlAdditionalDescDto = new ArrayList<>();
	private PlAdditionalDescDto plAdditionalDescDto = new PlAdditionalDescDto();

	private List<PlPengajuanSantunanDto> listIndex = new ArrayList<>();
	private List<PlPengajuanSantunanDto> listIndexCopy = new ArrayList<>();
	private boolean listIndexWindow = false;

	private List<DasiJrRefCodeDto> listKesimpulanSementara = new ArrayList<>();
	private DasiJrRefCodeDto kesimpulanSementaraDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listOtorisasiFlag = new ArrayList<>();
	private DasiJrRefCodeDto otorisasiFlagDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listKodeJenisPembayaran = new ArrayList<>();
	private DasiJrRefCodeDto kodeJenisPembayaranDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listStatusProses = new ArrayList<>();
	private DasiJrRefCodeDto statusProsesDto = new DasiJrRefCodeDto();

	private String pilihanPengajuan;
	private String noBerkasIndex = "";
	private Date tglPengajuan;

	private String label1;
	private String label2;
	private String uang1;
	private String uang2;
	private int uangTotal;
	private Date tglPenyelesaian;
	private Date jamPenyelesaian;
	private SimpleDateFormat tgl = new SimpleDateFormat("dd/MM/YYYY");
	private SimpleDateFormat jam = new SimpleDateFormat("hh:mm");
	private boolean penyelesaianFlag = true;
	private String dilimpahkanKe;
	private String noSurat = "";
	private Date tglProses = new Date();
	Calendar jamProses = Calendar.getInstance();
	private Date jamP;
	private boolean bayarFlag = false;
	private String metodeBayar = null;
	private List<String> listMetodeBayar = new ArrayList<>();
	private String searchBank;
	private String noRekening;
	private String jenisRekening;
	private String namaRekening;
	private UserTask userTask;

	// add by luthfi
	// for search and filter
	private Date tglPenerimaan;
	private String noBerkas;
	private String searchIndex;
	private String pilihPengajuan;
	private int pageSize = 5;

	private BigDecimal biaya1;
	private BigDecimal biaya2;
	private double totalAmbulance;
	private double totalP3k;

	private List<DasiJrRefCodeDto> listSifatCidera = new ArrayList<>();
	private DasiJrRefCodeDto sifatCideraDto = new DasiJrRefCodeDto();
	private String cideraKode;
	private String cideraKode2;

	private List<PlRekeningRDto> rekeningDtos = new ArrayList<>();
	private PlRekeningRDto rekeningDto = new PlRekeningRDto();

	@Command
	public void changePageSize() {
		setPageSize(getPageSize());
		setListIndexWindow(true);
		BindUtils.postNotifyChange(null, null, this, "pageSize");
	}

	// @Command("detailRingkasanPengajuan")
	// public void detailRingkasanPengajuan(
	// @BindingParam("item") PlPengajuanSantunanDto selected) {
	// // Messagebox.show("MUNCUL Di Santunan id Kecelakaan "
	// +selected.getIdKecelakaan());
	// // Messagebox.show("MUNCUL di santunan id korban "
	// +selected.getIdKorbanKecelakaan());
	// navigate(RINGKASAN_PAGE_PATH);
	//
	// Map<String, Object> map = new HashMap<>();
	// map.put("noBerkas", selected.getNoBerkas());
	// map.put("flagPenyelesaian", penyelesaianFlag);
	// BindUtils.postGlobalCommand(null, null, "viewRingkasan", map);
	// }

	@Command("detailRingkasanPengajuan")
	public void showPopupRingkasan(@BindingParam("popup") String popup,
			@BindingParam("item") PlPengajuanSantunanDto plPengajuanSantunanDto,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();

		args.put("popUpHandler", globalHandleMethodName);
		args.put("plPengajuanSantunanDto", plPengajuanSantunanDto);

		// if (!beforePopup(args, popup))
		// return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	protected boolean beforePopupRingkasan(Map<String, Object> args, String popup) {
		args.put("plPengajuanSantunanDto", pengajuanDto);
		return true;
	}

	@GlobalCommand("ringkasanHandler")
	public void titleHandlerRingkasan(@BindingParam("noBerkas") PlPengajuanSantunanDto selected) {
		if (pengajuanDto != null) {
			this.pengajuanDto = selected;
		}
	}

//	@Command("caribank")
//	public void loadBank() {
//		Map<String, Object> mapInput = new HashMap<>();
//
//		RestResponse rest = callWs("/MasterRumahSakit/findAllBank", mapInput,
//				HttpMethod.POST);
//		try {
//			listBankDto = JsonUtil.mapJsonToListObject(rest.getContents(),
//					FndBankDto.class);
//			BindUtils.postNotifyChange(null, null, this, "listBankDto");
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

//	@Command("caribank")
//	public void loadBank(@BindingParam("item")String cariBank) {
//		Map<String, Object> mapInput = new HashMap<>();
//		mapInput.put("search", cariBank);
//		RestResponse rest = callWs(WS_URI_LOV+"/allBank", mapInput,
//				HttpMethod.POST);
//		try {
//			listBankDto = JsonUtil.mapJsonToListObject(rest.getContents(),
//					FndBankDto.class);
//			BindUtils.postNotifyChange(null, null, this, "listBankDto");
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	protected void loadList() {
		setPengajuan("Pengajuan Untuk Diproses");
		listPilihanPengajuan.add("Pengajuan Untuk Diproses");
		listPilihanPengajuan.add("Pengajuan Seluruhnya");
		BindUtils.postNotifyChange(null, null, this, "listPilihanPengajuan");
		UserTask ut = (UserTask) Executions.getCurrent().getAttribute("obj2");
		if (ut != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			setNoBerkasIndex(ut.getNomorPermohonan());
			userTask = ut;
			Date tgl = null;
			try {
				tgl = sdf.parse(ut.getCreationDate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			setTglPengajuan(tgl);

			if (getSearchIndex() == null || getSearchIndex().equalsIgnoreCase("")) {
				setSearchIndex("%%");
			}
			if (getNoBerkas() == null || getNoBerkas().equalsIgnoreCase("")) {
				setNoBerkas("%%");
			}
			if (getPilihPengajuan() == null || getPilihPengajuan().equalsIgnoreCase("")) {
				setPilihPengajuan("%%");
			}
			if (getPilihPengajuan().contains("Diproses")) {
				setPilihPengajuan("O");
			}

			HashMap<String, Object> filter = new HashMap<>();
			filter.put("pilihPengajuan", pengajuan);
			filter.put("tglPenerimaan", dateToString(tglPengajuan));
			filter.put("noBerkas", noBerkasIndex);
			filter.put("search", searchIndex);
			RestResponse rest = callWs(WS_URI + "/index", filter, HttpMethod.POST);
			try {
				listIndex = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
				listIndexCopy = new ArrayList<>();
				listIndexCopy.addAll(listIndex);
				setListIndexWindow(true);
				for (PlPengajuanSantunanDto a : listIndex) {
					pengajuanDto.setNoBerkas(a.getNoBerkas());
					pengajuanDto.setIdKecelakaan(a.getIdKecelakaan());
				}
				BindUtils.postNotifyChange(null, null, this, "listIndex");
				BindUtils.postNotifyChange(null, null, this, "listIndexCopy");
				BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
				BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			setNoBerkasIndex(null);
			setTglPengajuan(null);
		}

	}

	@Command("cetakDisposisi")
	public void cetakDisposisi(@BindingParam("item") PlPengajuanSantunanDto selected) throws IOException {
		if (selected == null || selected.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}
		CetakLdpb cetak = new CetakLdpb();
		cetak.cetakDisposisi(selected);
	}

	@Command("search")
	public void searchIndex() {
		if (getSearchIndex() == null || getSearchIndex().equalsIgnoreCase("")) {
			setSearchIndex("%%");
		}
		if (getNoBerkas() == null || getNoBerkas().equalsIgnoreCase("")) {
			setNoBerkas("%%");
		}
		if (getPengajuan() == null || getPengajuan().equalsIgnoreCase("")) {
			setPilihPengajuan("%%");
		}
		if (getPengajuan().contains("Diproses")) {
			setPilihPengajuan("O");
		}
		HashMap<String, Object> filter = new HashMap<>();
		filter.put("pilihPengajuan", pilihPengajuan);
		filter.put("tglPenerimaan", dateToString(tglPengajuan));
		filter.put("noBerkas", noBerkasIndex);
		filter.put("search", searchIndex);
		RestResponse rest = callWs(WS_URI + "/index", filter, HttpMethod.POST);
		try {
			listIndex = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
			listIndexCopy = new ArrayList<>();
			listIndexCopy.addAll(listIndex);
			setListIndexWindow(true);
			for (PlPengajuanSantunanDto a : listIndex) {
				pengajuanDto.setNoBerkas(a.getNoBerkas());
				pengajuanDto.setIdKecelakaan(a.getIdKecelakaan());
			}
			BindUtils.postNotifyChange(null, null, this, "listIndex");
			BindUtils.postNotifyChange(null, null, this, "listIndexCopy");
			BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
			BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listMetodePembayaran(String type) {
		if ("KAS".equals(type) || "BANK".equals(type)) {
			setMetodeBayar(type);
			listMetodeBayar.add(type);
			BindUtils.postNotifyChange(null, null, this, "metodeBayar");
			BindUtils.postNotifyChange(null, null, this, "listMetodeBayar");
		} else {
			listMetodePembayaran();
		}
	}

	public void listMetodePembayaran() {
		setMetodeBayar("KAS");
		listMetodeBayar.add("KAS");
		listMetodeBayar.add("BANK");
		BindUtils.postNotifyChange(null, null, this, "metodeBayar");
		BindUtils.postNotifyChange(null, null, this, "listMetodeBayar");
	}

	@Command(value = "cari")
	public void loadIndex() {
		Map<String, Object> mapinput = new HashMap<>();
		SimpleDateFormat tgl = new SimpleDateFormat("dd/MM/YYYY");

		if (listPilihanPengajuan != null) {
			if (pengajuan.equalsIgnoreCase("Pengajuan Untuk Diproses")) {
				setPilihanPengajuan("O");
				mapinput.put("jenisPengajuan", getPilihanPengajuan());
				mapinput.put("noBerkas", getNoBerkasIndex());
				if (getTglPengajuan() == null) {
					mapinput.put("tglPengajuan", "");
					mapinput.put("dateFlag", "N");
				} else {
					mapinput.put("tglPengajuan", tgl.format(getTglPengajuan()));
					mapinput.put("dateFlag", "Y");

				}
				mapinput.put("search", getSearchIndex());

				RestResponse rest = callWs(WS_URI + "/index", mapinput, HttpMethod.POST);
				try {
					listPengajuanDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
					setTotalSize(rest.getTotalRecords());
					setListIndexWindow(true);
					BindUtils.postNotifyChange(null, null, this, "listPengajuanDto");
					BindUtils.postNotifyChange(null, null, this, "listIndex");

				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			} else if (pengajuan.equalsIgnoreCase("Pengajuan Seluruhnya")) {
				if ((tglPengajuan == null && noBerkasIndex == null)) {
					Messagebox.show("Isi filter pencarian Tanggal atau Nomor Berkas !");
					setListIndexWindow(false);
					BindUtils.postNotifyChange(null, null, this, "listIndex");
				} else if (tglPengajuan == null && noBerkasIndex != null) {
					setPilihanPengajuan("");
					mapinput.put("dateFlag", "N");
					mapinput.put("jenisPengajuan", getPilihanPengajuan());
					mapinput.put("noBerkas", getNoBerkasIndex());
					mapinput.put("tglPengajuan", "");
					mapinput.put("search", getSearchIndex());

					RestResponse rest = callWs(WS_URI + "/index", mapinput, HttpMethod.POST);
					try {
						listPengajuanDto = JsonUtil.mapJsonToListObject(rest.getContents(),
								PlPengajuanSantunanDto.class);
						setTotalSize(rest.getTotalRecords());
						setListIndexWindow(true);
						BindUtils.postNotifyChange(null, null, this, "listPengajuanDto");
						BindUtils.postNotifyChange(null, null, this, "listIndex");

					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				} else {
					setPilihanPengajuan("");
					mapinput.put("dateFlag", "Y");
					mapinput.put("jenisPengajuan", getPilihanPengajuan());
					mapinput.put("noBerkas", getNoBerkasIndex());
					mapinput.put("tglPengajuan", tgl.format(getTglPengajuan()));
					mapinput.put("search", getSearchIndex());

					RestResponse rest = callWs(WS_URI + "/index", mapinput, HttpMethod.POST);
					try {
						listPengajuanDto = JsonUtil.mapJsonToListObject(rest.getContents(),
								PlPengajuanSantunanDto.class);
						setTotalSize(rest.getTotalRecords());
						setListIndexWindow(true);
						BindUtils.postNotifyChange(null, null, this, "listPengajuanDto");
						BindUtils.postNotifyChange(null, null, this, "listIndex");

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}

	}

	@Command("viewData")
	public void viewData(@BindingParam("popup") String popup, @BindingParam("item") PlPengajuanSantunanDto selected,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Executions.getCurrent().setAttribute("obj", selected);
		getPageInfo().setEditMode(true);
		Map<String, Object> args = new HashMap<>();

		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}

	}

	@Command("openDetail")
	public void openDetail(@BindingParam("popup") String popup, @BindingParam("item") PlPengajuanSantunanDto selected,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Executions.getCurrent().setAttribute("obj", selected);
		Executions.getCurrent().setAttribute("obj2", userTask);
		getPageInfo().setEditMode(true);
		Map<String, Object> args = new HashMap<>();

		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}

	}

	public void onEdit() {

		pengajuanDto = (PlPengajuanSantunanDto) Executions.getCurrent().getAttribute("obj");
		userTask = (UserTask) Executions.getCurrent().getAttribute("obj2");
		BindUtils.postNotifyChange(null, null, this, "pengajuanDto");

		loadJenisDokumen();
		listKesimpulanSementara();
		listKodeJenisPembayaran();
		listOtorisasiFlag();
		listStatusProses();
		// listMetodePembayaran();
		pengajuanDto.setKodeRumahsakitPengajuanRs(
				pengajuanDto.getKodeRumahsakitPengajuanRs() == null ? "" : pengajuanDto.getKodeRumahsakitPengajuanRs());
		loadBank("%", pengajuanDto.getKodeRumahsakitPengajuanRs());

		Map<String, Object> mapinput = new HashMap<>();
		mapinput.put("kodeKantor", pengajuanDto.getDilimpahkanKe());

		if (pengajuanDto.getStatusProses().equalsIgnoreCase("OJ")) {
			setBayarFlag(true);
		} else {
			setBayarFlag(false);
		}
		BindUtils.postNotifyChange(null, null, this, "isPenyelesaianBayar");

		RestResponse rest = callWs(WS_URI_LOV + "/getKantorByKodeKantor", mapinput, HttpMethod.POST);
		try {
			listFndKantorDto = JsonUtil.mapJsonToListObject(rest.getContents(), FndKantorJasaraharjaDto.class);
			setFndKantorDto(listFndKantorDto.get(0));
			setDilimpahkanKe(fndKantorDto.getNama());
			BindUtils.postNotifyChange(null, null, this, "dilimpahkanKe");

		} catch (Exception e) {
			e.printStackTrace();
		}

		// ========================================GET DATA
		// SANTUNAN====================================================
		HashMap<String, Object> map = new HashMap<>();
		map.put("noBerkas", pengajuanDto.getNoBerkas());
		RestResponse rest2 = callWs(WS_URI_SANTUNAN + "/findByNoBerkas", map, HttpMethod.POST);
		try {
			listPengajuanDto = JsonUtil.mapJsonToListObject(rest2.getContents(), PlPengajuanSantunanDto.class);
			for (PlPengajuanSantunanDto a : listPengajuanDto) {
				pengajuanDto.setTglPengajuan(a.getTglPengajuan() == null ? new Date() : a.getTglPengajuan());
				statusProsesDto.setRvLowValue(a.getStatusProses() == null ? "" : a.getStatusProses());
				kesimpulanSementaraDto
						.setRvLowValue(a.getKesimpulanSementara() == null ? "" : a.getKesimpulanSementara());
				otorisasiFlagDto.setRvLowValue(a.getOtorisasiFlag() == null ? "" : a.getOtorisasiFlag());
				sifatCideraDto.setRvLowValue(a.getCideraKorban() == null ? "" : a.getCideraKorban());
				kodeJenisPembayaranDto.setRvLowValue(a.getJaminanPembayaran() == null ? "" : a.getJaminanPembayaran());
				pengajuanDto.setTglPenyelesaian(a.getTglPenyelesaian() == null ? new Date() : a.getTglPenyelesaian());
				// plPengajuanSantunanDto.setJumlahPengajuanMeninggal(a.getJumlahPengajuanMeninggal());
				// plPengajuanSantunanDto.setJumlahPengajuanLukaluka(a.getJumlahPengajuanLukaluka());
				// plPengajuanSantunanDto.setJmlPengajuanAmbl(a.getJmlPengajuanAmbl());
				// plPengajuanSantunanDto.setJmlPengajuanP3k(a.getJmlPengajuanP3k());
				// plPengajuanSantunanDto.setDilimpahkanKe(a.getDilimpahkanKe());
				pengajuanDto.setCideraKorban(a.getCideraKorban() == null ? "" : a.getCideraKorban());
				// plPengajuanSantunanDto.setStatusProses(a.getStatusProses());
				// plPengajuanSantunanDto.setIdKorbanKecelakaan(a.getIdKorbanKecelakaan());
			}
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			if (pengajuanDto.getTglPenyelesaian() == null) {
				Date date = new Date();
				setTglPenyelesaian(date);
				setJamPenyelesaian(date);
			} else {
				setTglPenyelesaian(pengajuanDto.getTglPenyelesaian());
				setJamPenyelesaian(pengajuanDto.getTglPenyelesaian());
			}

			BindUtils.postNotifyChange(null, null, this, "tglPenyelesaian");
			BindUtils.postNotifyChange(null, null, this, "jamPenyelesaian");

			for (DasiJrRefCodeDto kesimpulan : listKesimpulanSementara) {
				if (kesimpulanSementaraDto.getRvLowValue().equalsIgnoreCase(kesimpulan.getRvLowValue())) {
					kesimpulanSementaraDto = new DasiJrRefCodeDto();
					kesimpulanSementaraDto = kesimpulan;
				}
			}
			for (DasiJrRefCodeDto otorisasi : listOtorisasiFlag) {
				if (otorisasiFlagDto.getRvLowValue().equalsIgnoreCase(otorisasi.getRvLowValue())) {
					otorisasiFlagDto = new DasiJrRefCodeDto();
					otorisasiFlagDto = otorisasi;
				}
			}
			for (DasiJrRefCodeDto statusProses : listStatusProses) {
				if (statusProsesDto.getRvLowValue().equalsIgnoreCase(statusProses.getRvLowValue())) {
					statusProsesDto = new DasiJrRefCodeDto();
					statusProsesDto = statusProses;
				}
			}
			for (DasiJrRefCodeDto jenisPembayaran : listKodeJenisPembayaran) {
				if (kodeJenisPembayaranDto.getRvLowValue().equalsIgnoreCase(jenisPembayaran.getRvLowValue())) {
					kodeJenisPembayaranDto = new DasiJrRefCodeDto();
					kodeJenisPembayaranDto = jenisPembayaran;
				}
			}
			BindUtils.postNotifyChange(null, null, this, "otorisasiFlagDto");
			BindUtils.postNotifyChange(null, null, this, "kodeJenisPembayaranDto");
			BindUtils.postNotifyChange(null, null, this, "kesimpulanSementaraDto");
			BindUtils.postNotifyChange(null, null, this, "pengajuanDto");
			BindUtils.postNotifyChange(null, null, this, "statusProsesDto");
			rupiahPengajuan();

		} catch (Exception e) {
			e.printStackTrace();
		}

		// ========================================GET DATA
		// PENYELESAIAN====================================================
		HashMap<String, Object> map2 = new HashMap<>();
		map2.put("noBerkas", pengajuanDto.getNoBerkas());
		RestResponse rest3 = callWs(WS_URI_LOV + "/getPenyelesaianSantunanByNoBerkas", map2, HttpMethod.POST);

		System.out.println(JsonUtil.getJson(rest3));

		try {
			listPlPenyelesaianSantunanDto = JsonUtil.mapJsonToListObject(rest3.getContents(),
					PlPenyelesaianSantunanDto.class);

			System.out.println(JsonUtil.getJson(listPlPenyelesaianSantunanDto));

			for (PlPenyelesaianSantunanDto a : listPlPenyelesaianSantunanDto) {
				plPenyelesaianSantunanDto = a;
				setBiaya1(a.getJumlahDibayarMeninggal() == null ? new BigDecimal(0) : a.getJumlahDibayarMeninggal());
				setBiaya2(a.getJumlahDibayarLukaluka() == null ? new BigDecimal(0) : a.getJumlahDibayarLukaluka());
				plPenyelesaianSantunanDto
						.setJmlByrAmbl(a.getJmlByrAmbl() == null ? new BigDecimal(0) : a.getJmlByrAmbl());
				plPenyelesaianSantunanDto.setJmlByrP3k(a.getJmlByrP3k() == null ? new BigDecimal(0) : a.getJmlByrP3k());
				setTotalAmbulance(a.getJmlByrAmbl() == null ? 0 : a.getJmlByrAmbl().doubleValue());
				setTotalP3k(a.getJmlByrP3k() == null ? 0 : a.getJmlByrP3k().doubleValue());
				plPenyelesaianSantunanDto.setJenisPembayaran(a.getJenisPembayaran());
				plPenyelesaianSantunanDto.setIdGuid(a.getIdGuid());
				plPenyelesaianSantunanDto.setNoRekening(a.getNoRekening());
				setNoRekening(plPenyelesaianSantunanDto.getNoRekening());
				plPenyelesaianSantunanDto.setTglProses(a.getTglProses());
			}
			int jmlByrAmbl = plPenyelesaianSantunanDto.getJmlByrAmbl() == null ? 0
					: plPenyelesaianSantunanDto.getJmlByrAmbl().intValue();
			int jmlByrP3k = plPenyelesaianSantunanDto.getJmlByrP3k() == null ? 0
					: plPenyelesaianSantunanDto.getJmlByrP3k().intValue();
			int biaya2Int = biaya2 == null ? 0 : biaya2.intValue();
			int i = biaya1.intValue() + biaya2Int + jmlByrAmbl + jmlByrP3k;
			setUangTotal(i);

			jamProses.setTime(new Date());
			jamProses.set(Calendar.HOUR_OF_DAY, 0);
			jamProses.set(Calendar.MINUTE, 1);
			jamP = jamProses.getTime();

			System.out.println(JsonUtil.getJson(plPenyelesaianSantunanDto));

			if (plPenyelesaianSantunanDto.getJenisPembayaran() == null) {
				// bisa pilih metode pembayaran (?)
				this.listMetodePembayaran();
			} else if (plPenyelesaianSantunanDto.getJenisPembayaran().equalsIgnoreCase("T")) {
				this.listMetodePembayaran("BANK");
			} else if (plPenyelesaianSantunanDto.getJenisPembayaran().equalsIgnoreCase("K")) {
				this.listMetodePembayaran("KAS");
			}
			BindUtils.postNotifyChange(null, null, this, "tglProses");
			BindUtils.postNotifyChange(null, null, this, "jamP");
			BindUtils.postNotifyChange(null, null, this, "metodeBayar");
			BindUtils.postNotifyChange(null, null, this, "noRekening");
			BindUtils.postNotifyChange(null, null, this, "plPenyelesaianSantunanDto");
			BindUtils.postNotifyChange(null, null, this, "biaya1");
			BindUtils.postNotifyChange(null, null, this, "biaya2");
			BindUtils.postNotifyChange(null, null, this, "totalAmbulance");
			BindUtils.postNotifyChange(null, null, this, "totalP3k");

		} catch (Exception e) {
			e.printStackTrace();
		}

//		==================================FIND ONE BANK=============================
		plPenyelesaianSantunanDto.setIdGuid(
				plPenyelesaianSantunanDto.getIdGuid() == null ? null : plPenyelesaianSantunanDto.getIdGuid());
		if (plPenyelesaianSantunanDto.getIdGuid() != null) {
//			System.err.println("luthfi88 " + plPenyelesaianSantunanDto.getIdGuid());
//			HashMap<String, Object> map3 = new HashMap<>();
//			map3.put("noBerkas", plPenyelesaianSantunanDto.getIdGuid());
//			RestResponse rest4 = callWs(WS_URI_LOV + "/findOneBank", map3, HttpMethod.POST);
//			try {
//				listBankDto = JsonUtil.mapJsonToListObject(rest4.getContents(), FndBankDto.class);
//				for (FndBankDto a : listBankDto) {
//					bankDto = new FndBankDto();
//					bankDto.setKodeBank(a.getKodeBank());
//					bankDto.setNamaBank(a.getNamaBank());
//					bankDto.setKodeBank2(a.getKodeBank2());
//				}
//
//				for (FndBankDto bank : listBankDto) {
//					System.err.println("luthfi66 " + bankDto.getKodeBank() + " " + bank.getKodeBank());
//					if (bankDto.getKodeBank().equalsIgnoreCase(bank.getKodeBank())) {
//						bankDto = new FndBankDto();
//						bankDto = bank;
//					}
//				}
//				BindUtils.postNotifyChange(null, null, this, "bankDto");
//				System.err.println("luthfi77 " + bankDto.getNamaBank());
//
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
		}

		// ============================GET ADDITIONAL
		// DESC====================================
		Map<String, Object> mapAdditional = new HashMap<>();
		mapAdditional.put("noBerkas", pengajuanDto.getNoBerkas());
		RestResponse restAdditional = callWs(WS_URI_SANTUNAN + "/getAdditionalByNoBerkas", mapAdditional,
				HttpMethod.POST);
		try {
			listPlAdditionalDescDto = JsonUtil.mapJsonToListObject(restAdditional.getContents(),
					PlAdditionalDescDto.class);

			for (PlAdditionalDescDto a : listPlAdditionalDescDto) {
				plAdditionalDescDto.setTglMd(a.getTglMd());
				plAdditionalDescDto.setKodeLokasiPemohon(a.getKodeLokasiPemohon());
				plAdditionalDescDto.setTglRawatAwal(a.getTglRawatAwal());
				plAdditionalDescDto.setTglRawatAkhir(a.getTglRawatAkhir());
				plAdditionalDescDto.setJnsRekening(a.getJnsRekening());
				plAdditionalDescDto.setNamaRekening(a.getNamaRekening());
				setNamaRekening(plAdditionalDescDto.getNamaRekening());
			}
			System.err.println("luthfi10 " + JsonUtil.getJson(restAdditional.getContents()));
			BindUtils.postNotifyChange(null, null, this, "plAdditionalDescDto");
			BindUtils.postNotifyChange(null, null, this, "namaRekening");

		} catch (Exception e) {
			e.printStackTrace();
		}

		// if (pengajuanDto.getCideraKorban().equalsIgnoreCase("01")) {
		// setLabel1("MD");
		// setLabel2("");
		// setUang1(pengajuanDto.getJumlahDibayarMeninggalPenyelesaian().toString());
		// setUang2(BigDecimal.valueOf(0).toString());
		// } else if (pengajuanDto.getCideraKorban().equalsIgnoreCase("02")) {
		// setLabel1("LL");
		// setLabel2("");
		// setUang1(pengajuanDto.getJumlahDibayarMeninggalPenyelesaian().toString());
		// setUang2(BigDecimal.valueOf(0).toString());
		// } else if (pengajuanDto.getCideraKorban().equalsIgnoreCase("04")) {
		// setLabel1("CT");
		// setLabel2("");
		// setUang1(pengajuanDto.getJumlahDibayarMeninggalPenyelesaian().toString());
		// setUang2(BigDecimal.valueOf(0).toString());
		// } else if (pengajuanDto.getCideraKorban().equalsIgnoreCase("05")) {
		// setLabel1("MD");
		// setLabel2("LL");
		// setUang1(pengajuanDto.getJumlahDibayarMeninggalPenyelesaian().toString());
		// setUang2(pengajuanDto.getJumlahDibayarLukalukaPenyelesaian().toString());
		// } else if (pengajuanDto.getCideraKorban().equalsIgnoreCase("06")) {
		// setLabel1("LL");
		// setLabel2("CT");
		// setUang1(pengajuanDto.getJumlahDibayarMeninggalPenyelesaian().toString());
		// setUang2(pengajuanDto.getJumlahDibayarLukalukaPenyelesaian().toString());
		// } else if (pengajuanDto.getCideraKorban().equalsIgnoreCase("07")) {
		// setLabel1("PG");
		// setLabel2("");
		// setUang1(pengajuanDto.getJumlahDibayarMeninggalPenyelesaian().toString());
		// setUang2(BigDecimal.valueOf(0).toString());
		// } else if (pengajuanDto.getCideraKorban().equalsIgnoreCase("08")) {
		// setLabel1("LL");
		// setLabel2("PG");
		// setUang1(pengajuanDto.getJumlahDibayarMeninggalPenyelesaian().toString());
		// setUang2(pengajuanDto.getJumlahDibayarLukalukaPenyelesaian().toString());
		// }
		// if (pengajuanDto.getJumlahDibayarMeninggalPenyelesaian()==null) {
		// pengajuanDto.setJumlahDibayarMeninggalPenyelesaian(BigDecimal.ZERO);
		// }
		// if (pengajuanDto.getJumlahDibayarLukalukaPenyelesaian()==null) {
		// pengajuanDto.setJumlahDibayarLukalukaPenyelesaian(BigDecimal.ZERO);
		// }
		// if (pengajuanDto.getJmlByrAmblPenyelesaian()==null) {
		// pengajuanDto.setJmlByrAmblPenyelesaian(BigDecimal.ZERO);
		// }
		// if (pengajuanDto.getJmlByrP3kPenyelesaian()==null) {
		// pengajuanDto.setJmlByrP3kPenyelesaian(BigDecimal.ZERO);
		// }
		//
		// int total =
		// pengajuanDto.getJumlahDibayarMeninggalPenyelesaian().intValue()+pengajuanDto.getJumlahDibayarLukalukaPenyelesaian().intValue()+
		// pengajuanDto.getJmlByrAmblPenyelesaian().intValue()+pengajuanDto.getJmlByrP3kPenyelesaian().intValue();
		// setUangTotal(String.valueOf(total));
		// setTglPenyelesaian(pengajuanDto.getTglPenyelesaian());
		//
		// setJamPenyelesaian(pengajuanDto.getTglPenyelesaian());
		// BindUtils.postNotifyChange(null, null, this, "label1");
		// BindUtils.postNotifyChange(null, null, this, "label2");
		// BindUtils.postNotifyChange(null, null, this, "uang1");
		// BindUtils.postNotifyChange(null, null, this, "uang2");
		// BindUtils.postNotifyChange(null, null, this, "uangTotal");
		// BindUtils.postNotifyChange(null, null, this, "tglPenyelesaian");
		// BindUtils.postNotifyChange(null, null, this, "jamPenyelesaian");

	}

	public void listKesimpulanSementara() {
		RestResponse rest = callWs(WS_URI_LOV + "/getListKesimpulanSementara", new HashMap<>(), HttpMethod.POST);
		listKesimpulanSementara = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listKesimpulanSementara.add(kosong);
			listKesimpulanSementara.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listKesimpulanSementara");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listOtorisasiFlag() {
		RestResponse rest = callWs(WS_URI_LOV + "/getListOtorisasiFlag", new HashMap<>(), HttpMethod.POST);
		listOtorisasiFlag = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listOtorisasiFlag.add(kosong);
			listOtorisasiFlag.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listOtorisasiFlag");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listKodeJenisPembayaran() {
		RestResponse rest = callWs(WS_URI_LOV + "/getListKodeJenisPembayaran", new HashMap<>(), HttpMethod.POST);
		try {
			listKodeJenisPembayaran = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKodeJenisPembayaran");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listStatusProses() {

		RestResponse restStatusProses = callWs(WS_URI_LOV + "/getListStatusProses", new HashMap<String, Object>(),
				HttpMethod.POST);
		listStatusProses = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(restStatusProses.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listStatusProses.add(kosong);
			listStatusProses.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listStatusProses");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void savePenyelesaian() throws ParseException {
		try {

			SimpleDateFormat tglPros = new SimpleDateFormat("dd/MM/YYYY");
			SimpleDateFormat timePros = new SimpleDateFormat("dd/MM/YYYY HH:mm");
			String tgl = tglPros.format(tglProses);
			jamProses.setTime(new Date());
			jamP = jamProses.getTime();
			String jam = new SimpleDateFormat("HH:mm").format(jamP);
			Date waktu = timePros.parse(tgl + " " + jam);
			String tglpeny = tglPros.format(tglPenyelesaian);
			String jampeny = new SimpleDateFormat("HH:mm").format(jamPenyelesaian);
			Date waktupeny = timePros.parse(tglpeny + " " + jampeny);

			if (pengajuanDto.getStatusProses().equalsIgnoreCase("OJ")) {
				if (getNoRekening() == null)
					this.setNoRekening("");
				if (metodeBayar == null)
					metodeBayar = "";

				if (getNoRekening().equalsIgnoreCase("") && metodeBayar.equalsIgnoreCase("BANK")
						|| getNoRekening() == null && metodeBayar.equalsIgnoreCase("BANK")) {
					Messagebox.show("Nomer Rekening Harus Diisi");
				} else if (getNamaRekening().equalsIgnoreCase("") && metodeBayar.equalsIgnoreCase("BANK")
						|| getNamaRekening() == null && metodeBayar.equalsIgnoreCase("BANK")) {
					Messagebox.show("Nama Pemilik Rekening Harus Diisi");
				} else {
					// update pengajuan santunan
					saveDto.setNoBerkas(pengajuanDto.getNoBerkas());
					saveDto.setStatusProses(pengajuanDto.getStatusProses());
					saveDto.setTglPenyelesaian(waktupeny);
					saveDto.setLastUpdatedBy(userSession.getLoginID());
					saveDto.setLastUpdatedDate(new Date());

					saveDto.setOtorisasiFlag(pengajuanDto.getOtorisasiFlag());
					saveDto.setKesimpulanSementara(pengajuanDto.getKesimpulanSementara());
					saveDto.setJaminanPembayaran(pengajuanDto.getJaminanPembayaran());
					saveDto.setTglPengajuan(pengajuanDto.getTglPengajuan());

					// penyelesaian santunan
					saveDto.setNoBerkasPenyelesaian(pengajuanDto.getNoBerkas());
					saveDto.setTglProsesPenyelesaian(waktu);
					saveDto.setJumlahDibayarMeninggalPenyelesaian(pengajuanDto.getJumlahPengajuanMeninggal());
					saveDto.setJumlahDibayarLukalukaPenyelesaian(pengajuanDto.getJumlahPengajuanLukaluka());
					saveDto.setJumlahDibayarPenguburanPenyelesaian(pengajuanDto.getJumlahPengajuanPenguburan());
					saveDto.setCreatedByPenyelesaian(userSession.getLoginID());
					saveDto.setCreationDatePenyelesaian(new Date());
					saveDto.setJmlByrAmblPenyelesaian(pengajuanDto.getJmlPengajuanAmbl());
					saveDto.setJmlByrP3kPenyelesaian(pengajuanDto.getJmlPengajuanP3k());

					if (metodeBayar.equalsIgnoreCase("BANK")) {
						saveDto.setJenisPembayaranPenyelesaian("T");
						saveDto.setNoRekeningPenyelesaian(getNoRekening());
						saveDto.setIdGuidPenyelesaian(getBankDto().getKodeBank());

						// pl additional
						saveDto.setNamaRekeningAdd(getNamaRekening());
//						saveDto.setJnsRekeningAdd(jnsRekeningAdd);
					} else if (metodeBayar.equalsIgnoreCase("KAS")) {
						saveDto.setJenisPembayaranPenyelesaian("K");
						saveDto.setNoRekeningPenyelesaian("-");
						saveDto.setIdGuidPenyelesaian("");
					}
					saveDto.setNoSuratPenyelesaianPenyelesaian(getNoSurat());

					// penyelesaianRS
					saveDto.setKodeRs(pengajuanDto.getKodeRs());
					saveDto.setJumlahPengajuanLuka2RS(pengajuanDto.getJumlahPengajuanLuka2RS());

					RestResponse rest = callWs(WS_URI + "/save", saveDto, HttpMethod.POST);
					if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
						showInfoMsgBox(rest.getMessage());
						// update BPM ke proses selanjutnya
						try {

							String instanceId = "";
							if (userTask != null) {
								instanceId = userTask.getOtherInfo2().substring(5);
								UserTask uTask = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"),
										Labels.getLabel("passBPM"), "Entry Penyelesaian Berkas", instanceId);
								String pattern = "dd-MM-yyyy HH:mm:ss";
								SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

								String dateFormat = simpleDateFormat.format(new Date());
								if (uTask.getPengajuanType().equalsIgnoreCase("EXGRATIA")) {
									uTask.setOtherInfo1("Entry Data Pengajuan");
									// uTask.setOtherInfo5("PELIMPAHAN");
									uTask.setNomorPermohonan(userTask.getNomorPermohonan());
									uTask.setPengajuanType(pengajuan);
									uTask.setKodeCabang(userTask.getKodeCabang());
									uTask.setCreationDate(userTask.getLastUpdateDate());
									uTask.setCreatedBy(userSession.getUserName());
									uTask.setLastUpdatedBy(userSession.getUserName());
									uTask.setLastUpdateDate(dateFormat);
									uTask.setIdGUID(userTask.getIdGUID());
									uTask.setKodeKantor(userTask.getKodeKantor());
									uTask.setOtherInfo2(userTask.getOtherInfo2());
									uTask.setOtherInfo3(userTask.getOtherInfo3());
									uTask.setOtherInfo4(userTask.getOtherInfo4());
									uTask.setOtherInfo5(userTask.getOtherInfo5());
									uTask.setTaskNumber(userTask.getTaskNumber());
									UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
											userSession.getUserLdap(), "APPROVE", uTask, "");
								} else {
									// UserTaskService.updateUserTask(Labels.getLabel("userBPM"),
									// Labels.getLabel("passBPM"),
									// pengajuanDto.getNoBerkas(),
									// "Entry Penyelesaian Berkas","APPROVE",
									// "", "", userSession.getUserName(),
									// dateFormat,pengajuanDto.getIdKecelakaan(),userSession.getUserLdap());
									uTask.setOtherInfo1("Entry Penyelesaian Berkas");
									uTask.setNomorPermohonan(userTask.getNomorPermohonan());
									uTask.setPengajuanType(pengajuan);
									uTask.setKodeCabang(userTask.getKodeCabang());
									uTask.setCreationDate(userTask.getLastUpdateDate());
									uTask.setCreatedBy(userSession.getUserName());
									uTask.setLastUpdatedBy(userSession.getUserName());
									uTask.setLastUpdateDate(dateFormat);
									uTask.setIdGUID(userTask.getIdGUID());
									uTask.setKodeKantor(userTask.getKodeKantor());
									uTask.setOtherInfo2(userTask.getOtherInfo2());
									uTask.setOtherInfo3(userTask.getOtherInfo3());
									uTask.setOtherInfo4(userTask.getOtherInfo4());
									uTask.setOtherInfo5(userTask.getOtherInfo5());
									uTask.setTaskNumber(userTask.getTaskNumber());
									UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
											userSession.getUserLdap(), "APPROVE", uTask, "");

								}
							} else {
								instanceId = UserTaskService.getInstanceId("Entry Otorisasi Pengajuan Berkas",
										pengajuanDto.getNoBerkas());
								UserTask uTask = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"),
										Labels.getLabel("passBPM"), "Entry Penyelesaian Berkas", instanceId);
								String pattern = "dd-MM-yyyy HH:mm:ss";
								SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

								String dateFormat = simpleDateFormat.format(new Date());
								if (uTask.getPengajuanType().equalsIgnoreCase("EXGRATIA")) {
									uTask.setOtherInfo1("Entry Data Pengajuan");
									// uTask.setOtherInfo5("PELIMPAHAN");
									uTask.setCreationDate(uTask.getLastUpdateDate());
									uTask.setCreatedBy(userSession.getUserName());
									uTask.setLastUpdatedBy(userSession.getUserName());
									uTask.setLastUpdateDate(dateFormat);
									UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
											userSession.getUserLdap(), "APPROVE", uTask, "");
								} else {
									// UserTaskService.updateUserTask(Labels.getLabel("userBPM"),
									// Labels.getLabel("passBPM"),
									// pengajuanDto.getNoBerkas(),
									// "Entry Penyelesaian Berkas","APPROVE",
									// "", "", userSession.getUserName(),
									// dateFormat,pengajuanDto.getIdKecelakaan(),userSession.getUserLdap());
									uTask.setOtherInfo1("Entry Penyelesaian Berkas");
									uTask.setCreationDate(uTask.getLastUpdateDate());
									uTask.setCreatedBy(userSession.getUserName());
									uTask.setLastUpdatedBy(userSession.getUserName());
									uTask.setLastUpdateDate(dateFormat);
									UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
											userSession.getUserLdap(), "APPROVE", uTask, "");

								}
							}

						} catch (RemoteException e) {
							e.printStackTrace();
						}
					} else {
						showErrorMsgBox(rest.getMessage());
					}
				}
			} else {
				if (getNoSurat().equalsIgnoreCase("") || getNoSurat() == null) {
					Messagebox.show("Nomer Surat Harus Diisi");
				} else {
					// update pengajuan santunan
					saveDto.setNoBerkas(pengajuanDto.getNoBerkas());
					saveDto.setStatusProses(pengajuanDto.getStatusProses());
					saveDto.setTglPenyelesaian(waktupeny);
					saveDto.setLastUpdatedBy(userSession.getLoginID());
					saveDto.setLastUpdatedDate(new Date());

					saveDto.setOtorisasiFlag(pengajuanDto.getOtorisasiFlag());
					saveDto.setKesimpulanSementara(pengajuanDto.getKesimpulanSementara());
					saveDto.setJaminanPembayaran(pengajuanDto.getJaminanPembayaran());
					saveDto.setTglPengajuan(pengajuanDto.getTglPengajuan());

					// penyelesaian santunan
					saveDto.setNoBerkasPenyelesaian(pengajuanDto.getNoBerkas());
					saveDto.setTglProsesPenyelesaian(waktu);
					saveDto.setJumlahDibayarMeninggalPenyelesaian(pengajuanDto.getJumlahPengajuanMeninggal());
					saveDto.setJumlahDibayarLukalukaPenyelesaian(pengajuanDto.getJumlahPengajuanLukaluka());
					saveDto.setJumlahDibayarPenguburanPenyelesaian(pengajuanDto.getJumlahPengajuanPenguburan());
					saveDto.setCreatedByPenyelesaian(userSession.getLoginID());
					saveDto.setCreationDatePenyelesaian(new Date());
					saveDto.setJmlByrAmblPenyelesaian(pengajuanDto.getJmlPengajuanAmbl());
					saveDto.setJmlByrP3kPenyelesaian(pengajuanDto.getJmlPengajuanP3k());

					saveDto.setNoSuratPenyelesaianPenyelesaian(getNoSurat());

					// penyelesaianRS
					saveDto.setKodeRs(pengajuanDto.getKodeRs());
					saveDto.setJumlahPengajuanLuka2RS(pengajuanDto.getJumlahPengajuanLuka2RS());

					RestResponse rest = callWs(WS_URI + "/save", saveDto, HttpMethod.POST);
					if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
						showInfoMsgBox(rest.getMessage());
						try {
							// UserTask ut = (UserTask)
							// Executions.getCurrent().getAttribute("obj2");
							String instanceId = "";
							if (userTask != null) {
								instanceId = userTask.getOtherInfo2().substring(5);
							} else {
								instanceId = UserTaskService.getInstanceId("Entry Otorisasi Pengajuan Berkas",
										pengajuanDto.getNoBerkas());
							}
							UserTask uTask = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"),
									Labels.getLabel("passBPM"), "Entry Penyelesaian Berkas", instanceId);
							String pattern = "dd-MM-yyyy HH:mm:ss";
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

							String dateFormat = simpleDateFormat.format(new Date());
							if (uTask.getPengajuanType().equalsIgnoreCase("EXGRATIA")) {
								uTask.setOtherInfo1("Entry Data Pengajuan");
								// uTask.setOtherInfo5("PELIMPAHAN");
								uTask.setCreationDate(uTask.getLastUpdateDate());
								uTask.setCreatedBy(userSession.getUserName());
								uTask.setLastUpdatedBy(userSession.getUserName());
								uTask.setLastUpdateDate(dateFormat);
								UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
										userSession.getUserLdap(), "APPROVE", uTask, "");
							} else {
								// UserTaskService.updateUserTask(Labels.getLabel("userBPM"),
								// Labels.getLabel("passBPM"),
								// pengajuanDto.getNoBerkas(),
								// "Entry Penyelesaian Berkas","APPROVE",
								// "", "", userSession.getUserName(),
								// dateFormat,pengajuanDto.getIdKecelakaan(),userSession.getUserLdap());
								uTask.setOtherInfo1("Entry Penyelesaian Berkas");
								uTask.setCreationDate(uTask.getLastUpdateDate());
								uTask.setCreatedBy(userSession.getUserName());
								uTask.setLastUpdatedBy(userSession.getUserName());
								uTask.setLastUpdateDate(dateFormat);
								UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
										userSession.getUserLdap(), "APPROVE", uTask, "");

							}
						} catch (RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						showErrorMsgBox(rest.getMessage());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// update BPM ke proses selanjutnya
		// try {
		// UserTaskService.updateUserTask(userSession.getLoginID(), "welcome1",
		// plPengajuanSantunanDto.getIdKecelakaan(),
		// "Entry Penyelesaian Berkas","APPROVE",
		// "", "", "", "");
		// } catch (RemoteException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

	}

	@Command
	public void prosesSelanjutnya() throws ParseException {
		try {
			// ========================================GET DATA
			// SANTUNAN====================================================
			HashMap<String, Object> map = new HashMap<>();
			map.put("noBerkas", pengajuanDto.getNoBerkas());
			RestResponse rest2 = callWs(WS_URI_SANTUNAN + "/findByNoBerkas", map, HttpMethod.POST);
			try {
				listPengajuanDto = JsonUtil.mapJsonToListObject(rest2.getContents(), PlPengajuanSantunanDto.class);
				for (PlPengajuanSantunanDto a : listPengajuanDto) {
					pengajuanDto.setTglPenyelesaian(a.getTglPenyelesaian());
				}
				if (pengajuanDto.getTglPenyelesaian() == null) {
					showInfoMsgBox("Data belum disimpan!", "");
				} else {
					RestResponse restResponse = null;
					pengajuanDto.setStatusProses("BK");
					pengajuanDto.setNoBerkas(pengajuanDto.getNoBerkas());
					restResponse = callWs(WS_URI_SANTUNAN + "/saveSantunanProsesSelanjutnya", pengajuanDto,
							HttpMethod.POST);
					if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
						showInfoMsgBox("Data berhasil dikirim ke Proses Selanjutnya", null);
					} else {
						showErrorMsgBox(restResponse.getMessage());
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

//
//			SimpleDateFormat tglPros = new SimpleDateFormat("dd/MM/YYYY");
//			SimpleDateFormat timePros = new SimpleDateFormat("dd/MM/YYYY HH:mm");
//			String tgl = tglPros.format(tglProses);
//			jamProses.setTime(new Date());
//			jamP = jamProses.getTime();
//			String jam = new SimpleDateFormat("HH:mm").format(jamP);
//			Date waktu = timePros.parse(tgl + " " + jam);
//			String tglpeny = tglPros.format(tglPenyelesaian);
//			String jampeny = new SimpleDateFormat("HH:mm")
//					.format(jamPenyelesaian);
//			Date waktupeny = timePros.parse(tglpeny + " " + jampeny);
//
//			if (pengajuanDto.getStatusProses().equalsIgnoreCase("OJ")) {
//				if (getNoRekening().equalsIgnoreCase("")
//						|| getNoRekening() == null) {
//					Messagebox.show("Nomer Rekening Harus Diisi");
//				} else if (getNamaRekening().equalsIgnoreCase("")
//						|| getNamaRekening() == null) {
//					Messagebox.show("Nama Pemilik Rekening Harus Diisi");
//				} else {
//					// update pengajuan santunan
//					saveDto.setNoBerkas(pengajuanDto.getNoBerkas());
//
//					if (pengajuanDto.getStatusProses().equalsIgnoreCase("OJ")
//							|| pengajuanDto.getStatusProses().equalsIgnoreCase(
//									"OS")
//							|| pengajuanDto.getStatusProses().equalsIgnoreCase(
//									"OR")) {
//						saveDto.setStatusProses("BK");
//					} else if (pengajuanDto.getStatusProses().equalsIgnoreCase(
//							"OT")
//							|| pengajuanDto.getStatusProses().equalsIgnoreCase(
//									"OD")
//							|| pengajuanDto.getStatusProses().equalsIgnoreCase(
//									"OL")) {
//						saveDto.setStatusProses("BS");
//					}
//					saveDto.setTglPenyelesaian(waktupeny);
//					saveDto.setLastUpdatedBy(userSession.getLoginID());
//					saveDto.setLastUpdatedDate(new Date());
//
//					saveDto.setOtorisasiFlag(pengajuanDto.getOtorisasiFlag());
//					saveDto.setKesimpulanSementara(pengajuanDto
//							.getKesimpulanSementara());
//					saveDto.setJaminanPembayaran(pengajuanDto
//							.getJaminanPembayaran());
//					saveDto.setTglPengajuan(pengajuanDto.getTglPengajuan());
//
//					// penyelesaian santunan
//					saveDto.setNoBerkasPenyelesaian(pengajuanDto.getNoBerkas());
//					saveDto.setTglProsesPenyelesaian(waktu);
//					saveDto.setJumlahDibayarMeninggalPenyelesaian(pengajuanDto
//							.getJumlahPengajuanMeninggal());
//					saveDto.setJumlahDibayarLukalukaPenyelesaian(pengajuanDto
//							.getJumlahPengajuanLukaluka());
//					saveDto.setJumlahDibayarPenguburanPenyelesaian(pengajuanDto
//							.getJumlahPengajuanPenguburan());
//					saveDto.setCreatedByPenyelesaian(userSession.getLoginID());
//					saveDto.setCreationDatePenyelesaian(new Date());
//					saveDto.setJmlByrAmblPenyelesaian(pengajuanDto
//							.getJmlPengajuanAmbl());
//					saveDto.setJmlByrP3kPenyelesaian(pengajuanDto
//							.getJmlPengajuanP3k());
//
//					if (metodeBayar.equalsIgnoreCase("BANK")) {
//						saveDto.setJenisPembayaranPenyelesaian("T");
//						saveDto.setNoRekeningPenyelesaian(getNoRekening());
//						saveDto.setIdGuidPenyelesaian(getBankDto()
//								.getKodeBank());
//					} else if (metodeBayar.equalsIgnoreCase("KAS")) {
//						saveDto.setJenisPembayaranPenyelesaian("K");
//						saveDto.setNoRekeningPenyelesaian("-");
//						saveDto.setIdGuidPenyelesaian("");
//					}
//					saveDto.setNoSuratPenyelesaianPenyelesaian(getNoSurat());
//
//					RestResponse rest = callWs(WS_URI + "/save", saveDto,
//							HttpMethod.POST);
//					if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
//						showInfoMsgBox(rest.getMessage());
//					} else {
//						showErrorMsgBox(rest.getMessage());
//					}
//				}
//			} else {
//				if (getNoSurat().equalsIgnoreCase("") || getNoSurat() == null) {
//					Messagebox.show("Nomer Surat Harus Diisi");
//				} else {
//					// update pengajuan santunan
//					saveDto.setNoBerkas(pengajuanDto.getNoBerkas());
//
//					if (pengajuanDto.getStatusProses().equalsIgnoreCase("OJ")
//							|| pengajuanDto.getStatusProses().equalsIgnoreCase(
//									"OS")
//							|| pengajuanDto.getStatusProses().equalsIgnoreCase(
//									"OR")) {
//						saveDto.setStatusProses("BK");
//					} else if (pengajuanDto.getStatusProses().equalsIgnoreCase(
//							"OT")
//							|| pengajuanDto.getStatusProses().equalsIgnoreCase(
//									"OD")
//							|| pengajuanDto.getStatusProses().equalsIgnoreCase(
//									"OL")) {
//						saveDto.setStatusProses("BS");
//					}
//					saveDto.setTglPenyelesaian(waktupeny);
//					saveDto.setLastUpdatedBy(userSession.getLoginID());
//					saveDto.setLastUpdatedDate(new Date());
//
//					saveDto.setOtorisasiFlag(pengajuanDto.getOtorisasiFlag());
//					saveDto.setKesimpulanSementara(pengajuanDto
//							.getKesimpulanSementara());
//					saveDto.setJaminanPembayaran(pengajuanDto
//							.getJaminanPembayaran());
//					saveDto.setTglPengajuan(pengajuanDto.getTglPengajuan());
//
//					// penyelesaian santunan
//					saveDto.setNoBerkasPenyelesaian(pengajuanDto.getNoBerkas());
//					saveDto.setTglProsesPenyelesaian(waktu);
//					saveDto.setJumlahDibayarMeninggalPenyelesaian(pengajuanDto
//							.getJumlahPengajuanMeninggal());
//					saveDto.setJumlahDibayarLukalukaPenyelesaian(pengajuanDto
//							.getJumlahPengajuanLukaluka());
//					saveDto.setJumlahDibayarPenguburanPenyelesaian(pengajuanDto
//							.getJumlahPengajuanPenguburan());
//					saveDto.setCreatedByPenyelesaian(userSession.getLoginID());
//					saveDto.setCreationDatePenyelesaian(new Date());
//					saveDto.setJmlByrAmblPenyelesaian(pengajuanDto
//							.getJmlPengajuanAmbl());
//					saveDto.setJmlByrP3kPenyelesaian(pengajuanDto.getJmlPengajuanP3k());
//
//					saveDto.setNoSuratPenyelesaianPenyelesaian(getNoSurat());
//
//					RestResponse rest = callWs(WS_URI + "/save", saveDto,
//							HttpMethod.POST);
//					if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
//						showInfoMsgBox(rest.getMessage());
//					} else {
//						showErrorMsgBox(rest.getMessage());
//					}
//				}
//			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void loadJenisDokumen() {
		String revDomain = "PL JENIS SURAT";
		String flag = "Y";
		Map<String, Object> mapinput = new HashMap<>();

		mapinput.put("search", "");
		mapinput.put("rvDomain", revDomain);
		mapinput.put("flag", flag);
		RestResponse rest = callWs(WS_URI_LOV + "/refCode", mapinput, HttpMethod.POST);
		try {
			listDokumenDto = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listDokumenDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void cetakKuitansi() {
		Map<String, Object> mapinput = new HashMap<>();
		mapinput.put("noBerkas", pengajuanDto.getNoBerkas());
		// RestResponse rest = callWs(WS_URI+"/cetakKuitansi", mapinput,
		// HttpMethod.POST);
		RestResponse rest = new RestResponse();
		PlAdditionalDescDto additionalDto = new PlAdditionalDescDto();

		int x = 0;

		try {
			rest = callWs(WS_URI + "/onePenyelesaian", mapinput, HttpMethod.POST);
			;
			listPenyelesaianKuitansiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlPenyelesaianSantunanDto.class);
			listPenyelesaianKuitansiDto.get(0);
			x = 1;
		} catch (Exception s) {
			x = 0;
		}

		try {
			if (x != 0 && rest.getContents() != null) {
				listPenyelesaianKuitansiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
						PlPenyelesaianSantunanDto.class);
				penyelesaianKuitansiDto = listPenyelesaianKuitansiDto.get(0);
				System.out.println("luthfi " + JsonUtil.getJson(rest));
				List<PlPengajuanSantunanDto> listDto = JsonUtil.mapJsonToListObject(rest.getContents(),
						PlPengajuanSantunanDto.class);

				cetakKuitansiDto = listDto.get(0);

				// setTotalSize(rest.getTotalRecords());

				// BigDecimal totalBiaya =
				// pengajuanDto.getJumlahPengajuanMeninggal().add(pengajuanDto.getJumlahPengajuanLukaluka())
				// .add(pengajuanDto.getJumlahPengajuanPenguburan()).add(pengajuanDto.getJmlPengajuanAmbl()).add(pengajuanDto.getJmlPengajuanP3k());

				BigDecimal totalBiaya = pengajuanDto.getJumlahPengajuanMeninggal() == null ? new BigDecimal(0)
						: pengajuanDto.getJumlahPengajuanMeninggal()
								.add(pengajuanDto.getJumlahPengajuanLukaluka() == null ? new BigDecimal(0)
										: pengajuanDto.getJumlahPengajuanLukaluka())
								.add(pengajuanDto.getJumlahPengajuanPenguburan() == null ? new BigDecimal(0)
										: pengajuanDto.getJumlahPengajuanPenguburan())
								.add(pengajuanDto.getJmlPengajuanAmbl() == null ? new BigDecimal(0)
										: pengajuanDto.getJmlPengajuanAmbl())
								.add(pengajuanDto.getJmlPengajuanP3k() == null ? new BigDecimal(0)
										: pengajuanDto.getJmlPengajuanP3k());

				cetakKuitansiDto.setNoBerkas(pengajuanDto.getNoBerkas());
				cetakKuitansiDto.setPembayaranDes(pengajuanDto.getPembayaranDes());
				cetakKuitansiDto.setNamaKantorDilimpahkan(pengajuanDto.getNamaKantorDilimpahkan());
				cetakKuitansiDto.setTotalPembayaran(totalBiaya);
				cetakKuitansiDto.setNamaKorban(pengajuanDto.getNamaKorban());
				cetakKuitansiDto.setAlamatKorban(pengajuanDto.getAlamatKorban());

				cetakKuitansiDto.setJaminanDesc(pengajuanDto.getJaminanDesc());
				cetakKuitansiDto.setTglKejadian(pengajuanDto.getTglKejadian());
				cetakKuitansiDto.setDeskripsiLokasi(pengajuanDto.getDeskripsiLokasi());
				cetakKuitansiDto.setNoPolisi(pengajuanDto.getNoPolisi());
				cetakKuitansiDto.setJenisKendaraanDesc(pengajuanDto.getJenisKendaraanDesc());
				cetakKuitansiDto.setUmur(pengajuanDto.getUmur());
				cetakKuitansiDto.setNamaPemohon(pengajuanDto.getNamaPemohon());
				cetakKuitansiDto.setAlamatPemohon(pengajuanDto.getAlamatPemohon());
				cetakKuitansiDto.setHubKorbanDesc(pengajuanDto.getHubKorbanDesc());
				cetakKuitansiDto.setCideraDes(pengajuanDto.getCideraDes());

				cetakKuitansiDto.setTglProsesPenyelesaian(penyelesaianKuitansiDto.getTglProses());

				additionalDto.setNoBerkas(pengajuanDto.getNoBerkas());
				additionalDto.setCreatedBy(userSession.getLoginID());
				additionalDto.setCreatedDate(new Date());
				additionalDto.setTglMd(null);
				additionalDto.setTglTerimaLimpah(pengajuanDto.getTglPenerimaan());
				additionalDto.setNamaRekening(namaRekening);
				additionalDto.setJnsRekening(jenisRekening);
				additionalDto.setTglRawatAwal(null);
				additionalDto.setTglRawatAkhir(null);
				additionalDto.setIdRekRs(null);
				additionalDto.setLastUpdatedBy(userSession.getLoginID());
				additionalDto.setLastUpdatedDate(new Date());
				if (metodeBayar.equalsIgnoreCase("BANK")) {
					additionalDto.setJenisPembayaranPenyelesaian("T");
					additionalDto.setNoRekPenyelesaian(getNoRekening());
					additionalDto.setIdGUIDPenyelesaian(getBankDto().getKodeBank());
				} else if (metodeBayar.equalsIgnoreCase("KAS")) {
					additionalDto.setJenisPembayaranPenyelesaian("K");
					additionalDto.setNoRekPenyelesaian("-");
					additionalDto.setIdGUIDPenyelesaian("");
				}
				additionalDto.setLastUpdatedBy(userSession.getLoginID());

				BindUtils.postNotifyChange(null, null, this, "cetakKuitansiDto");
				KuitansiPembayaranSantunan kuitansi = new KuitansiPembayaranSantunan();
				kuitansi.cetakKuitansi(cetakKuitansiDto);

				RestResponse restSaveKuitansi = callWs(WS_URI + "/saveKuitansi", additionalDto, HttpMethod.POST);
				if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
					showInfoMsgBox(restSaveKuitansi.getMessage());
				} else {
					showErrorMsgBox(restSaveKuitansi.getMessage());
				}
			} else {
				BigDecimal totalBiaya = pengajuanDto.getJumlahPengajuanMeninggal()
						.add(pengajuanDto.getJumlahPengajuanLukaluka()).add(pengajuanDto.getJumlahPengajuanPenguburan())
						.add(pengajuanDto.getJmlPengajuanAmbl()).add(pengajuanDto.getJmlPengajuanP3k());

				cetakKuitansiDto.setNoBerkas(pengajuanDto.getNoBerkas());
				cetakKuitansiDto.setPembayaranDes(pengajuanDto.getPembayaranDes());
				cetakKuitansiDto.setNamaKantorDilimpahkan(pengajuanDto.getNamaKantorDilimpahkan());
				cetakKuitansiDto.setTotalPembayaran(totalBiaya);
				cetakKuitansiDto.setNamaKorban(pengajuanDto.getNamaKorban());
				cetakKuitansiDto.setAlamatKorban(pengajuanDto.getAlamatKorban());

				cetakKuitansiDto.setJaminanDesc(pengajuanDto.getJaminanDesc());
				cetakKuitansiDto.setTglKejadian(pengajuanDto.getTglKejadian());
				cetakKuitansiDto.setDeskripsiLokasi(pengajuanDto.getDeskripsiLokasi());
				cetakKuitansiDto.setNoPolisi(pengajuanDto.getNoPolisi());
				cetakKuitansiDto.setJenisKendaraanDesc(pengajuanDto.getJenisKendaraanDesc());
				cetakKuitansiDto.setUmur(pengajuanDto.getUmur());
				cetakKuitansiDto.setNamaPemohon(pengajuanDto.getNamaPemohon());
				cetakKuitansiDto.setAlamatPemohon(pengajuanDto.getAlamatPemohon());
				cetakKuitansiDto.setHubKorbanDesc(pengajuanDto.getHubKorbanDesc());
				cetakKuitansiDto.setCideraDes(pengajuanDto.getCideraDes());

				cetakKuitansiDto.setTglProsesPenyelesaian(getJamP());
				cetakKuitansiDto.setNoBpkPenyelesaian("");

				additionalDto.setNoBerkas(pengajuanDto.getNoBerkas());
				additionalDto.setCreatedBy(userSession.getLoginID());
				additionalDto.setCreatedDate(new Date());
				additionalDto.setTglMd(null);
				additionalDto.setTglTerimaLimpah(pengajuanDto.getTglPenerimaan());
				additionalDto.setNamaRekening(namaRekening);
				additionalDto.setJnsRekening(jenisRekening);
				additionalDto.setTglRawatAwal(null);
				additionalDto.setTglRawatAkhir(null);
				additionalDto.setIdRekRs(null);
				additionalDto.setLastUpdatedBy(userSession.getLoginID());
				additionalDto.setLastUpdatedDate(new Date());
				if (metodeBayar.equalsIgnoreCase("BANK")) {
					additionalDto.setJenisPembayaranPenyelesaian("T");
					additionalDto.setNoRekPenyelesaian(getNoRekening());
					additionalDto.setIdGUIDPenyelesaian(getBankDto().getKodeBank());
				} else if (metodeBayar.equalsIgnoreCase("KAS")) {
					additionalDto.setJenisPembayaranPenyelesaian("K");
					additionalDto.setNoRekPenyelesaian("-");
					additionalDto.setIdGUIDPenyelesaian("");
				}
				additionalDto.setLastUpdateByPenyelesaian(userSession.getLoginID());

				BindUtils.postNotifyChange(null, null, this, "cetakKuitansiDto");
				KuitansiPembayaranSantunan kuitansi = new KuitansiPembayaranSantunan();
				kuitansi.cetakKuitansi(cetakKuitansiDto);

				RestResponse restSaveKuitansi = callWs(WS_URI + "/saveKuitansi", additionalDto, HttpMethod.POST);
				if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
					showInfoMsgBox(restSaveKuitansi.getMessage());
				} else {
					showErrorMsgBox(restSaveKuitansi.getMessage());
				}
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
	}

	@Command("cetakDP2S")
	public void cetakDP2S(@BindingParam("item") PlPengajuanSantunanDto selected) throws IOException {
		if (selected == null || selected.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("noBerkas", selected.getNoBerkas());
		List<PlPenyelesaianSantunanDto> listDto = new ArrayList<>();
		PlPenyelesaianSantunanDto dto = new PlPenyelesaianSantunanDto();
		RestResponse rest = callWs(WS_URI + "/onePenyelesaian", mapInput, HttpMethod.POST);
		try {
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlPenyelesaianSantunanDto.class);
			BindUtils.postNotifyChange(null, null, this, "listDto");
			dto = listDto.get(0);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		akumulasiPembayaran(selected.getIdKorbanKecelakaan());
		selected.setLoketMenangani(userSession.getNamaKantor());
		selected.setTglCetak(new Date());
		DokumenDP2S dokumenDP2S = new DokumenDP2S();
		dokumenDP2S.cetakDP2S(selected, akumulasiPembayaran, dto);
	}

	public void akumulasiPembayaran(String idKorban) {
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("idKorban", idKorban);
		RestResponse rest = callWs("/OperasionalPenyelesaianPengajuan/akumulasiPembayaran", mapInput, HttpMethod.POST);
		try {
			akumulasiPembayaran = JsonUtil.mapJsonToListObject(rest.getContents(), PlPenyelesaianSantunanDto.class);
			BindUtils.postNotifyChange(null, null, this, "akumulasiPembayaran");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Command("back")
	public void back(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}

	@Command("printDokumenPendamping")
	public void cetakDokumenPendamping() throws IOException {
		System.out.println("Masuk print");
		System.out.println(dokumenDto.getRvLowValue());
		if (dokumenDto.getRvLowValue().equalsIgnoreCase("7")) {
			dokumenPendamping.cetakSuratPemberitahuanOverbookKpdKorban(pengajuanDto);

		} else if (dokumenDto.getRvLowValue().equalsIgnoreCase("5")) {
			if (pengajuanDto.getOtorisasiDesc().equalsIgnoreCase("DILIMPAHKAN")) {
				loadBerkas();
				getOneKantor(userSession.getKantor().substring(0, 2) + "00000");
				pengajuanDto.setNamaKantorAsal(kantorDto.getNama());
				getOneKantor(pengajuanDto.getKodeKantorDilimpahkan().substring(0, 2) + "00000");
				pengajuanDto.setNamaKantorDilimpahkan(kantorDto.getNama());
				dokumenPendamping.cetakSuratPengantarPelimpahan(pengajuanDto, listBerkasDto);
			} else {
				Messagebox.show("Surat Pengantar Pelimpahan Hanya Bisa Dibuat Untuk Otorisasi Dilimpahkan !");
			}

		} else if (dokumenDto.getRvLowValue().equalsIgnoreCase("6")) {
			if (pengajuanDto.getOtorisasiDesc().equalsIgnoreCase("DILIMPAHKAN")) {
				loadBerkas();
				getOneKantor(pengajuanDto.getDiajukanDi().substring(0, 2) + "00000");
				pengajuanDto.setNamaKantorDiajukanDi(kantorDto.getNama());
				getOneKantor(userSession.getKantor().substring(0, 2) + "00000");
				pengajuanDto.setNamaKantorAsal(kantorDto.getNama());
				getOneKantor(pengajuanDto.getKodeKantorDilimpahkan().substring(0, 2) + "00000");
				pengajuanDto.setNamaKantorDilimpahkan(kantorDto.getNama());
				dokumenPendamping.cetakSuratPanggilanPelimpahan(pengajuanDto, listBerkasDto);
			} else {
				Messagebox.show("Surat Panggilan Pelimpahan Hanya Bisa Dibuat Untuk Otorisasi Dilimpahkan !");
			}
		}

	}

	public void getOneKantor(String kodeKantorJr) {
		Map<String, Object> mapinput = new HashMap<>();
		List<FndKantorJasaraharjaDto> listDto = new ArrayList<>();
		mapinput.put("kodeKantorJr", kodeKantorJr);
		RestResponse rest = callWs(WS_URI_LOV + "/kantorByKode", mapinput, HttpMethod.POST);
		try {
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(), FndKantorJasaraharjaDto.class);
			setTotalSize(rest.getTotalRecords());
			setKantorDto(listDto.get(0));
			BindUtils.postNotifyChange(null, null, this, "kantorDto");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
	}

	public void loadBerkas() {
		String revDomain = "KODE BERKAS";
		String flag = "Y";
		Map<String, Object> mapinput = new HashMap<>();

		mapinput.put("search", "");
		mapinput.put("rvDomain", revDomain);
		mapinput.put("flag", flag);
		RestResponse rest = callWs(WS_URI_LOV + "/refCode", mapinput, HttpMethod.POST);
		try {
			listBerkasDto = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listBerkasDto");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();

		}
	}

	@NotifyChange({ "cideraKode", "cideraKode2" })
	@Command
	public void rupiahPengajuan() {
		System.err.println("luthfi99 " + sifatCideraDto.getRvLowValue());
		sifatCidera();
		if (sifatCideraDto.getRvLowValue() == "01" || sifatCideraDto.getRvLowValue().equalsIgnoreCase("01")) {
			setCideraKode("MD");
			setCideraKode2(null);

		} else if (sifatCideraDto.getRvLowValue() == "02" || sifatCideraDto.getRvLowValue().equalsIgnoreCase("02")) {
			setCideraKode("LL");
			setCideraKode2(null);

		} else if (sifatCideraDto.getRvLowValue() == "04" || sifatCideraDto.getRvLowValue().equalsIgnoreCase("04")) {
			setCideraKode("CT");

		} else if (sifatCideraDto.getRvLowValue() == "05" || sifatCideraDto.getRvLowValue().equalsIgnoreCase("05")) {
			String string = "MD-LL";
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);

		} else if (sifatCideraDto.getRvLowValue() == "06" || sifatCideraDto.getRvLowValue().equalsIgnoreCase("06")) {
			String string = "LL-CT";
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);

		} else if (sifatCideraDto.getRvLowValue() == "07" || sifatCideraDto.getRvLowValue().equalsIgnoreCase("07")) {
			setCideraKode("PG");
			setCideraKode2(null);
		} else if (sifatCideraDto.getRvLowValue() == "08" || sifatCideraDto.getRvLowValue().equalsIgnoreCase("08")) {
			String string = "LL-PG";
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
		}
		BindUtils.postNotifyChange(null, null, this, "cideraKode");
		BindUtils.postNotifyChange(null, null, this, "cideraKode2");
	}

	public void sifatCidera() {
		RestResponse restCidera = callWs(WS_URI_LOV + "/getListSifatCidera", new HashMap<String, Object>(),
				HttpMethod.POST);
		try {
			listSifatCidera = JsonUtil.mapJsonToListObject(restCidera.getContents(), DasiJrRefCodeDto.class);
			for (DasiJrRefCodeDto o : listSifatCidera) {
				System.out.println("Test" + listSifatCidera);
			}
			setTotalSize(restCidera.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listSifatCidera");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	====================================BANDBOX BANK================================================
	@Command
	public void searchBank(@BindingParam("val") String search, String kodeRs) {
		kodeRs = pengajuanDto.getKodeRumahsakitPengajuanRs() == null ? "" : pengajuanDto.getKodeRumahsakitPengajuanRs();
		loadBank(search, kodeRs);
	}

	private void loadBank(String search, String kodeRs) {
		Map<String, Object> map1 = new HashMap<>();
		map1.put("search", search);

		Map<String, Object> map = new HashMap<>();
		map.put("search", search);
		map.put("kodeRumahsakit", kodeRs);
		RestResponse rest = callWs(WS_URI_LOV + "/allBank", map1, HttpMethod.POST);
		RestResponse restBank = callWs(WS_URI + "/findBankByKodeRs", map, HttpMethod.POST);
		try {
			List<FndBankDto> banks = JsonUtil.mapJsonToListObject(restBank.getContents(), FndBankDto.class);
			if (banks.size() == 0) {
				banks = JsonUtil.mapJsonToListObject(rest.getContents(), FndBankDto.class);
			}
			listBankDto.clear();
			for (FndBankDto bank : banks) {
				if (this.getRekening(bank.getKodeBank(), kodeRs).size() > 0) {
					listBankDto.add(bank);
				}
			}
			BindUtils.postNotifyChange(null, null, this, "listBankDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void pilihBank(@BindingParam("item") FndBankDto bank) {
		this.bankDto = bank;
		BindUtils.postNotifyChange(null, null, this, "bankDto");
		rekeningDto = null;
		BindUtils.postNotifyChange(null, null, this, "rekeningDto");
		bankDto.setKodeBank(bankDto.getKodeBank() == null ? "" : bankDto.getKodeBank());
		pengajuanDto.setKodeRumahsakitPengajuanRs(
				pengajuanDto.getKodeRumahsakitPengajuanRs() == null ? "" : pengajuanDto.getKodeRumahsakitPengajuanRs());
		showRekening(bankDto.getKodeBank(), pengajuanDto.getKodeRumahsakitPengajuanRs());
	}

	private List<PlRekeningRDto> getRekening(String kodeBank, String kodeRumahsakit) {
		Map<String, Object> map = new HashMap<>();
		map.put("kodeBank", kodeBank);
		map.put("kodeRumahsakit", kodeRumahsakit);
		RestResponse restRekening = callWs(WS_URI + "/findRekByKodeRsKodeBank", map, HttpMethod.POST);
		try {
			List<PlRekeningRDto> reks = JsonUtil.mapJsonToListObject(restRekening.getContents(), PlRekeningRDto.class);
			if (reks == null)
				return new ArrayList<>();
			else
				return reks;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<>();
		}
	}

	private void showRekening(String kodeBank, String kodeRumahsakit) {
		rekeningDtos.clear();
		namaRekening = "";
		rekeningDtos = this.getRekening(kodeBank, kodeRumahsakit);
		BindUtils.postNotifyChange(null, null, this, "rekeningDtos");
		BindUtils.postNotifyChange(null, null, this, "namaRekening");
	}

	@Command
	public void pilihRekening() {
		String namaPemilik = rekeningDto.getNamaPemilik() == null ? "" : rekeningDto.getNamaPemilik();
		setNoRekening(rekeningDto.getNoRekening());
		setNamaRekening(namaPemilik);
		BindUtils.postNotifyChange(null, null, this, "namaRekening");
		BindUtils.postNotifyChange(null, null, this, "rekeningDto");
	}

	public String getPilihanPengajuan() {
		return pilihanPengajuan;
	}

	public void setPilihanPengajuan(String pilihanPengajuan) {
		this.pilihanPengajuan = pilihanPengajuan;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public void setTglPengajuan(Date tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}

	public String getNoBerkasIndex() {
		return noBerkasIndex;
	}

	public void setNoBerkasIndex(String noBerkasIndex) {
		this.noBerkasIndex = noBerkasIndex;
	}

	public Date getTglPengajuan() {
		return tglPengajuan;
	}

	public List<PlPengajuanSantunanDto> getListPengajuanDto() {
		return listPengajuanDto;
	}

	public void setListPengajuanDto(List<PlPengajuanSantunanDto> listPengajuanDto) {
		this.listPengajuanDto = listPengajuanDto;
	}

	public PlPengajuanSantunanDto getPengajuanDto() {
		return pengajuanDto;
	}

	public void setPengajuanDto(PlPengajuanSantunanDto pengajuanDto) {
		this.pengajuanDto = pengajuanDto;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getLabel1() {
		return label1;
	}

	public void setLabel1(String label1) {
		this.label1 = label1;
	}

	public String getLabel2() {
		return label2;
	}

	public void setLabel2(String label2) {
		this.label2 = label2;
	}

	public String getUang1() {
		return uang1;
	}

	public void setUang1(String uang1) {
		this.uang1 = uang1;
	}

	public String getUang2() {
		return uang2;
	}

	public void setUang2(String uang2) {
		this.uang2 = uang2;
	}

	public List<String> getListPilihanPengajuan() {
		return listPilihanPengajuan;
	}

	public void setListPilihanPengajuan(List<String> listPilihanPengajuan) {
		this.listPilihanPengajuan = listPilihanPengajuan;
	}

	public void setTglPenyelesaian(Date tglPenyelesaian) {
		this.tglPenyelesaian = tglPenyelesaian;
	}

	public boolean isPenyelesaianFlag() {
		return penyelesaianFlag;
	}

	public void setPenyelesaianFlag(boolean penyelesaianFlag) {
		this.penyelesaianFlag = penyelesaianFlag;
	}

	public Date getTglPenyelesaian() {
		return tglPenyelesaian;
	}

	public FndKantorJasaraharjaDto getFndKantorDto() {
		return fndKantorDto;
	}

	public void setFndKantorDto(FndKantorJasaraharjaDto fndKantorDto) {
		this.fndKantorDto = fndKantorDto;
	}

	public String getDilimpahkanKe() {
		return dilimpahkanKe;
	}

	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}

	public String getPengajuan() {
		return pengajuan;
	}

	public void setPengajuan(String pengajuan) {
		this.pengajuan = pengajuan;
	}

	public PlPengajuanSantunanDto getSaveDto() {
		return saveDto;
	}

	public void setSaveDto(PlPengajuanSantunanDto saveDto) {
		this.saveDto = saveDto;
	}

	public List<FndKantorJasaraharjaDto> getListFndKantorDto() {
		return listFndKantorDto;
	}

	public void setListFndKantorDto(List<FndKantorJasaraharjaDto> listFndKantorDto) {
		this.listFndKantorDto = listFndKantorDto;
	}

	public String getNoSurat() {
		return noSurat;
	}

	public void setNoSurat(String noSurat) {
		this.noSurat = noSurat;
	}

	public Date getTglProses() {
		return tglProses;
	}

	public void setTglProses(Date tglProses) {
		this.tglProses = tglProses;
	}

	public Date getJamPenyelesaian() {
		return jamPenyelesaian;
	}

	public void setJamPenyelesaian(Date jamPenyelesaian) {
		this.jamPenyelesaian = jamPenyelesaian;
	}

	public boolean isBayarFlag() {
		return bayarFlag;
	}

	public void setBayarFlag(boolean bayarFlag) {
		this.bayarFlag = bayarFlag;
	}

	public String getMetodeBayar() {
		return metodeBayar;
	}

	public void setMetodeBayar(String metodeBayar) {
		this.metodeBayar = metodeBayar;
	}

	public String getSearchBank() {
		return searchBank;
	}

	public void setSearchBank(String searchBank) {
		this.searchBank = searchBank;
	}

	public List<FndBankDto> getListBankDto() {
		return listBankDto;
	}

	public void setListBankDto(List<FndBankDto> listBankDto) {
		this.listBankDto = listBankDto;
	}

	public FndBankDto getBankDto() {
		return bankDto;
	}

	public void setBankDto(FndBankDto bankDto) {
		this.bankDto = bankDto;
	}

	public String getNoRekening() {
		return noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

	public String getJenisRekening() {
		return jenisRekening;
	}

	public void setJenisRekening(String jenisRekening) {
		this.jenisRekening = jenisRekening;
	}

	public String getNamaRekening() {
		return namaRekening;
	}

	public void setNamaRekening(String namaRekening) {
		this.namaRekening = namaRekening;
	}

	public List<DasiJrRefCodeDto> getListDokumenDto() {
		return listDokumenDto;
	}

	public void setListDokumenDto(List<DasiJrRefCodeDto> listDokumenDto) {
		this.listDokumenDto = listDokumenDto;
	}

	public DasiJrRefCodeDto getDokumenDto() {
		return dokumenDto;
	}

	public void setDokumenDto(DasiJrRefCodeDto dokumenDto) {
		this.dokumenDto = dokumenDto;
	}

	public PlPengajuanSantunanDto getCetakKuitansiDto() {
		return cetakKuitansiDto;
	}

	public void setCetakKuitansiDto(PlPengajuanSantunanDto cetakKuitansiDto) {
		this.cetakKuitansiDto = cetakKuitansiDto;
	}

	public List<PlPenyelesaianSantunanDto> getListPenyelesaianKuitansiDto() {
		return listPenyelesaianKuitansiDto;
	}

	public void setListPenyelesaianKuitansiDto(List<PlPenyelesaianSantunanDto> listPenyelesaianKuitansiDto) {
		this.listPenyelesaianKuitansiDto = listPenyelesaianKuitansiDto;
	}

	public PlPenyelesaianSantunanDto getPenyelesaianKuitansiDto() {
		return penyelesaianKuitansiDto;
	}

	public void setPenyelesaianKuitansiDto(PlPenyelesaianSantunanDto penyelesaianKuitansiDto) {
		this.penyelesaianKuitansiDto = penyelesaianKuitansiDto;
	}

	public Calendar getJamProses() {
		return jamProses;
	}

	public void setJamProses(Calendar jamProses) {
		this.jamProses = jamProses;
	}

	public Date getJamP() {
		return jamP;
	}

	public void setJamP(Date jamP) {
		this.jamP = jamP;
	}

	public PlPengajuanSantunanDto getDokumenDP2SDto() {
		return dokumenDP2SDto;
	}

	public void setDokumenDP2SDto(PlPengajuanSantunanDto dokumenDP2SDto) {
		this.dokumenDP2SDto = dokumenDP2SDto;
	}

	public List<PlPenyelesaianSantunanDto> getAkumulasiPembayaran() {
		return akumulasiPembayaran;
	}

	public void setAkumulasiPembayaran(List<PlPenyelesaianSantunanDto> akumulasiPembayaran) {
		this.akumulasiPembayaran = akumulasiPembayaran;
	}

	public DokumenPendampingPenyelesaian getDokumenPendamping() {
		return dokumenPendamping;
	}

	public void setDokumenPendamping(DokumenPendampingPenyelesaian dokumenPendamping) {
		this.dokumenPendamping = dokumenPendamping;
	}

	public List<DasiJrRefCodeDto> getListBerkasDto() {
		return listBerkasDto;
	}

	public void setListBerkasDto(List<DasiJrRefCodeDto> listBerkasDto) {
		this.listBerkasDto = listBerkasDto;
	}

	public FndKantorJasaraharjaDto getKantorDto() {
		return kantorDto;
	}

	public void setKantorDto(FndKantorJasaraharjaDto kantorDto) {
		this.kantorDto = kantorDto;
	}

	public Date getTglPenerimaan() {
		return tglPenerimaan;
	}

	public void setTglPenerimaan(Date tglPenerimaan) {
		this.tglPenerimaan = tglPenerimaan;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getPilihPengajuan() {
		return pilihPengajuan;
	}

	public void setPilihPengajuan(String pilihPengajuan) {
		this.pilihPengajuan = pilihPengajuan;
	}

	public List<PlPengajuanSantunanDto> getListIndex() {
		return listIndex;
	}

	public void setListIndex(List<PlPengajuanSantunanDto> listIndex) {
		this.listIndex = listIndex;
	}

	public List<PlPengajuanSantunanDto> getListIndexCopy() {
		return listIndexCopy;
	}

	public void setListIndexCopy(List<PlPengajuanSantunanDto> listIndexCopy) {
		this.listIndexCopy = listIndexCopy;
	}

	public boolean isListIndexWindow() {
		return listIndexWindow;
	}

	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}

	public List<DasiJrRefCodeDto> getListKesimpulanSementara() {
		return listKesimpulanSementara;
	}

	public void setListKesimpulanSementara(List<DasiJrRefCodeDto> listKesimpulanSementara) {
		this.listKesimpulanSementara = listKesimpulanSementara;
	}

	public DasiJrRefCodeDto getKesimpulanSementaraDto() {
		return kesimpulanSementaraDto;
	}

	public void setKesimpulanSementaraDto(DasiJrRefCodeDto kesimpulanSementaraDto) {
		this.kesimpulanSementaraDto = kesimpulanSementaraDto;
	}

	public List<DasiJrRefCodeDto> getListOtorisasiFlag() {
		return listOtorisasiFlag;
	}

	public void setListOtorisasiFlag(List<DasiJrRefCodeDto> listOtorisasiFlag) {
		this.listOtorisasiFlag = listOtorisasiFlag;
	}

	public DasiJrRefCodeDto getOtorisasiFlagDto() {
		return otorisasiFlagDto;
	}

	public void setOtorisasiFlagDto(DasiJrRefCodeDto otorisasiFlagDto) {
		this.otorisasiFlagDto = otorisasiFlagDto;
	}

	public List<PlPenyelesaianSantunanDto> getListPlPenyelesaianSantunanDto() {
		return listPlPenyelesaianSantunanDto;
	}

	public void setListPlPenyelesaianSantunanDto(List<PlPenyelesaianSantunanDto> listPlPenyelesaianSantunanDto) {
		this.listPlPenyelesaianSantunanDto = listPlPenyelesaianSantunanDto;
	}

	public PlPenyelesaianSantunanDto getPlPenyelesaianSantunanDto() {
		return plPenyelesaianSantunanDto;
	}

	public void setPlPenyelesaianSantunanDto(PlPenyelesaianSantunanDto plPenyelesaianSantunanDto) {
		this.plPenyelesaianSantunanDto = plPenyelesaianSantunanDto;
	}

	public BigDecimal getBiaya1() {
		return biaya1;
	}

	public void setBiaya1(BigDecimal biaya1) {
		this.biaya1 = biaya1;
	}

	public BigDecimal getBiaya2() {
		return biaya2;
	}

	public void setBiaya2(BigDecimal biaya2) {
		this.biaya2 = biaya2;
	}

	public double getTotalAmbulance() {
		return totalAmbulance;
	}

	public void setTotalAmbulance(double totalAmbulance) {
		this.totalAmbulance = totalAmbulance;
	}

	public double getTotalP3k() {
		return totalP3k;
	}

	public void setTotalP3k(double totalP3k) {
		this.totalP3k = totalP3k;
	}

	public List<DasiJrRefCodeDto> getListStatusProses() {
		return listStatusProses;
	}

	public void setListStatusProses(List<DasiJrRefCodeDto> listStatusProses) {
		this.listStatusProses = listStatusProses;
	}

	public DasiJrRefCodeDto getStatusProsesDto() {
		return statusProsesDto;
	}

	public void setStatusProsesDto(DasiJrRefCodeDto statusProsesDto) {
		this.statusProsesDto = statusProsesDto;
	}

	public List<String> getListMetodeBayar() {
		return listMetodeBayar;
	}

	public void setListMetodeBayar(List<String> listMetodeBayar) {
		this.listMetodeBayar = listMetodeBayar;
	}

	public int getUangTotal() {
		return uangTotal;
	}

	public void setUangTotal(int uangTotal) {
		this.uangTotal = uangTotal;
	}

	public List<DasiJrRefCodeDto> getListKodeJenisPembayaran() {
		return listKodeJenisPembayaran;
	}

	public void setListKodeJenisPembayaran(List<DasiJrRefCodeDto> listKodeJenisPembayaran) {
		this.listKodeJenisPembayaran = listKodeJenisPembayaran;
	}

	public DasiJrRefCodeDto getKodeJenisPembayaranDto() {
		return kodeJenisPembayaranDto;
	}

	public void setKodeJenisPembayaranDto(DasiJrRefCodeDto kodeJenisPembayaranDto) {
		this.kodeJenisPembayaranDto = kodeJenisPembayaranDto;
	}

	public List<DasiJrRefCodeDto> getListSifatCidera() {
		return listSifatCidera;
	}

	public void setListSifatCidera(List<DasiJrRefCodeDto> listSifatCidera) {
		this.listSifatCidera = listSifatCidera;
	}

	public DasiJrRefCodeDto getSifatCideraDto() {
		return sifatCideraDto;
	}

	public void setSifatCideraDto(DasiJrRefCodeDto sifatCideraDto) {
		this.sifatCideraDto = sifatCideraDto;
	}

	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	public String getCideraKode() {
		return cideraKode;
	}

	public void setCideraKode(String cideraKode) {
		this.cideraKode = cideraKode;
	}

	public String getCideraKode2() {
		return cideraKode2;
	}

	public void setCideraKode2(String cideraKode2) {
		this.cideraKode2 = cideraKode2;
	}

	public List<PlAdditionalDescDto> getListPlAdditionalDescDto() {
		return listPlAdditionalDescDto;
	}

	public void setListPlAdditionalDescDto(List<PlAdditionalDescDto> listPlAdditionalDescDto) {
		this.listPlAdditionalDescDto = listPlAdditionalDescDto;
	}

	public PlAdditionalDescDto getPlAdditionalDescDto() {
		return plAdditionalDescDto;
	}

	public void setPlAdditionalDescDto(PlAdditionalDescDto plAdditionalDescDto) {
		this.plAdditionalDescDto = plAdditionalDescDto;
	}

	public List<PlRekeningRDto> getRekeningDtos() {
		return rekeningDtos;
	}

	public void setRekeningDtos(List<PlRekeningRDto> rekeningDtos) {
		this.rekeningDtos = rekeningDtos;
	}

	public PlRekeningRDto getRekeningDto() {
		return rekeningDto;
	}

	public void setRekeningDto(PlRekeningRDto rekeningDto) {
		this.rekeningDto = rekeningDto;
	}
}
