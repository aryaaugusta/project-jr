package ui.operasional;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import share.DukcapilSentObject;
import share.KKDukcapilDto;
import common.model.RestResponse;
import common.model.RestResponseDukcapil;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.JasperConnectionUtil;
import common.util.JsonUtil;

@Init(superclass = true)
public class OpNikDukcapilVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String WS_BPM = "/JR-BPM/BPM/ProxyServices/bpm_get_dukcapil_ps";
	private final String WS_URI = "/dukcapil";
	
	private KKDukcapilDto kkDucapilDto;
	private List<KKDukcapilDto> listKKDukcapilDto = new ArrayList<>();
	private String alamat;
	private String nikRequest;
	private AMedia fileContent;
	@Command
	public void search(@BindingParam("item")String search){
		RestResponseDukcapil rest = new RestResponseDukcapil();
		RestResponse restDuk = new RestResponse();
		if(search == null){
			System.out.println("search == null");
			clear();
			return;
		}else if(search.equalsIgnoreCase("")){
			System.out.println("search equals ignore case \"\" ");
			clear();
			return;
		}else if(search.isEmpty()){
			System.out.println("search is empty");
			clear();
			return;
		}
		try{
			Long nik = Long.parseLong(search);
			nikRequest = search;
			DukcapilSentObject obj = new DukcapilSentObject();
			obj.setNik(search);
			obj.setIpUser(Labels.getLabel("wsSOAIP"));
			obj.setPassword(Labels.getLabel("wsSOAPwd"));
			obj.setUserId(Labels.getLabel("wsSOAUser"));
			
			rest = callWsDukcapil(WS_BPM, obj, HttpMethod.POST);
			System.out.println("==================================");
			System.out.println(JsonUtil.getJson(rest));
			
			
			listKKDukcapilDto = JsonUtil.mapJsonToListObject(rest.getContent(), KKDukcapilDto.class);
			
			boolean found = false;
			try{
				kkDucapilDto = listKKDukcapilDto.get(0);
				found = true;
				
			}catch(Exception s){
				found = false;
				Messagebox.show("Data Tidak Ditemukan");
			}
			
			if(found){
				try {
					for(KKDukcapilDto kk : listKKDukcapilDto){
						if(nikRequest.equalsIgnoreCase(kk.getnIK())){
							kk.setIsCall(true);
							System.out.println("Dapat NIK pemanggil : "+nikRequest);
						}
					}
					Map<String, Object> input = new HashMap<>();
					input.put("kk", listKKDukcapilDto);
					input.put("option", "save");
					System.out.println("SAVING DUKCAPIL TO DB");
					restDuk = callWs(WS_URI+"/save", input, HttpMethod.POST);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (kkDucapilDto.getRespon()!= null){
				listKKDukcapilDto = new ArrayList<>();
				Messagebox.show(kkDucapilDto.getRespon());
				return;
			}
			if(kkDucapilDto.getaLAMAT()!= null || !kkDucapilDto.getaLAMAT().isEmpty()){
				alamat = kkDucapilDto.getaLAMAT()+"\n"+
						(kkDucapilDto.getnORT()==null?"":"RT. "+kkDucapilDto.getnORT()+" RW. "+kkDucapilDto.getnORW()+"\n")+
						kkDucapilDto.getkELNAME()+", "+kkDucapilDto.getkECNAME()+", "+kkDucapilDto.getkABNAME()+"\n"+
						kkDucapilDto.getpROPNAME();
			}else{
				listKKDukcapilDto = new ArrayList<>();
//				kkDucapilDto = null;
			}
//			for(KKDukcapilDto objs: listKKDukcapilDto){
//				
//			}
			
		}catch(Exception s){
			s.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "listKKDukcapilDto");
		BindUtils.postNotifyChange(null, null, this, "alamat");
	}
	
	public List<KKDukcapilDto> getKKDukcapil(String nik){
		search(nik);
		List<KKDukcapilDto> listKK = new ArrayList<>();
		if(listKKDukcapilDto!= null && !listKKDukcapilDto.isEmpty() && listKKDukcapilDto.size()>0){
			listKK.addAll(this.listKKDukcapilDto);
		}
		
		return listKK;
	}
	
	public static void main(String[] args) {
		OpNikDukcapilVmd op = new OpNikDukcapilVmd();
		List<KKDukcapilDto> listDto = op.getKKDukcapil("");
	}
	
	private void clear(){
		listKKDukcapilDto.clear();
		alamat = "";
		BindUtils.postNotifyChange(null, null, this, "listKKDukcapilDto");
		BindUtils.postNotifyChange(null, null, this, "alamat");
	}
	
	@Command
	public void close(@BindingParam("item") Window win){
		/*if (winLov == null)
			throw new RuntimeException(
					"id popUp tidak sama dengan viewModel");
		winLov.detach();*/
		win.detach();	
	}
	
	@Command("cetak")
	public void cete(@BindingParam("popup")String popup) throws ClassNotFoundException, SQLException, IOException, JRException, Exception{
		Map<String, Object> map = getParam();
//		System.out.println(JsonUtil.getJson(map));
//		System.out.println("0000000000000");
		try{
			cetak(getParam());
		}catch(Exception p){
			p.printStackTrace();
		}
		Map<String, Object> args = new HashMap<>();
		args.put("media", fileContent);
		((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
		System.out.println(JsonUtil.getJson(map));
		
	}
	
	//KONEKSI BPM 
//	private static Connection getSOAINFRAOracleConnection() throws ClassNotFoundException, SQLException{
//		Class.forName("oracle.jdbc.driver.OracleDriver");
//		
//		String host = Labels.getLabel("soaHost");
//		String port = Labels.getLabel("soaPort");
//		String sid = Labels.getLabel("soaService");
//		String password = Labels.getLabel("soaUser");
//		String user = Labels.getLabel("soaPwd");
//		
//		//java.sql.DriverManager.getConnection("jdbc:oracle:thin:@198.168.1.30:1521:jr", "jr_user", "jr_user")
//
//		String connectionUrl = "jdbc:oracle:thin:@"+host+":"+port+":"+sid;
////		String connectionUrl = "jdbc:oracle:thin:@"+host+":"+Labels.getLabel("port")+":"+sid;
//		Connection conn = DriverManager.getConnection(connectionUrl, user, password);
//		return conn;
//	}
	
	@SuppressWarnings("deprecation")
	private void cetak(Map<String, Object> parameter) throws ClassNotFoundException, SQLException, 
		IOException, JRException, Exception{
	
		File temp = File.createTempFile("NIK", ".pdf");
		FileOutputStream outs = new FileOutputStream(temp);
		File tempJsp = File.createTempFile("Jasper", ".jrxml");
		JasperReport jasper = null;
		try{
			ByteArrayOutputStream baos = getByteArrayOutputStream("KartuKeluarga.jrxml");
			try (OutputStream outputStream = new FileOutputStream(tempJsp)) {
				baos.writeTo(outputStream);
				outputStream.close();
			} catch (Exception x) {
				x.printStackTrace();
			}
			jasper = JasperCompileManager.compileReport(tempJsp
					.getAbsolutePath());
//		jasper = JasperCompileManager.compileReport("/home/glassfish/report/KartuKeluarga.jrxm");
		}catch(Exception s){
			jasper = JasperCompileManager.compileReport("/home/glassfish/report/KartuKeluarga.jrxml");
		}
		JasperPrint jasperReport = JasperFillManager.fillReport(jasper, parameter,
				new JREmptyDataSource(1));
		jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql"
                ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
//		new com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory();
//		JRProperties.setProperty( JRQueryExecuterFactory.QUERY_EXECUTER_FACTORY_PREFIX+"plsql"
//                ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
	
		// PDF-Export
		JRPdfExporter exportPdf = new JRPdfExporter();
		System.out.println(temp.getAbsolutePath());
		exportPdf.setParameter(JRExporterParameter.JASPER_PRINT, jasperReport);
		exportPdf.setParameter(JRExporterParameter.OUTPUT_STREAM, outs);
		exportPdf.exportReport();
	
		// show exported pdf 
		File f = new File(temp.getPath());
		byte[] buffer = new byte[(int) f.length()];
		FileInputStream fs = new FileInputStream(f);
		fs.read(buffer);
		fs.close();
		ByteArrayInputStream is = new ByteArrayInputStream(buffer);
		this.fileContent = new AMedia("report", "pdf", "application/pdf", is);
		
		
		
//		File temp2 = File.createTempFile("ReportJR", ".pdf");
		
//		Executions.getCurrent().sendRedirect(temp2.getPath(), "_blank");
		
		
		
//		Filedownload.save(new AMedia(laporan.getNamaLaporan(),"pdf","application/file", is));
		/*
		 * InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Data Kecelakaan", "docx", "application/file", fis));
		temp.delete();
		 */
		
		
		
	}
	
	
	private ByteArrayOutputStream getByteArrayOutputStream(String... param)
			throws IOException, FileNotFoundException {
		URL url;
		String reportName = "";
		if(param.length>0){
			url = new URL(getLoc() + "/" + param[0]);
		}else{
			url = new URL(getLoc() + "/" + param[0]);
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			is = url.openStream();
			byte[] byteChunk = new byte[4096];
			int n;
			while ((n = is.read(byteChunk)) > 0) {
				baos.write(byteChunk, 0, n);
			}
		} catch (IOException e) {
			System.err.printf("Failed while reading bytes from %s: %s",
					url.toExternalForm(), e.getMessage());
			e.printStackTrace();
		} finally {
			if (is != null) {
				is.close();
			}
		}
	
		return baos;
	}
	
	private String getLoc() {
		String port = (Executions.getCurrent().getServerPort() == 80) ? ""
				: (":" + Executions.getCurrent().getServerPort());
		/*
		 * String url = Executions.getCurrent().getScheme() + "://" +
		 * Executions.getCurrent().getServerName() + port +
		 * Executions.getCurrent().getContextPath() +
		 * Executions.getCurrent().getDesktop().getRequestPath();
		 */
		String x = "";
		if(System.getProperty("location")!=null){
			if(System.getProperty("location").equalsIgnoreCase("linux")){
				x = "/home/glassfish/report";
			} else{
				x = Executions.getCurrent().getScheme() + "://"
						+ Executions.getCurrent().getServerName() + port
						+ Executions.getCurrent().getContextPath()+"/report";
			}
		}else{
			x = Executions.getCurrent().getScheme() + "://"
					+ Executions.getCurrent().getServerName() + port
					+ Executions.getCurrent().getContextPath()+"/report";
		}
		return x;
	}
	
	//==========================================================
	
	private Date stringKeTanggal(String in){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			return sdf.parse(in);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
			// TODO Auto-generated catch block
		}
	}
	
	private Map<String, Object> getParam(){
		Map<String, Object> map = new HashMap<>();
		if(listKKDukcapilDto==null || listKKDukcapilDto.size()==0){
			return null;
		}
		KKDukcapilDto kak = listKKDukcapilDto.get(0);
		KKDukcapilDto kakKepala = new KKDukcapilDto();
		KKDukcapilDto kkRequest = new KKDukcapilDto();
		List<KKDukcapilDto> listKkBuffer = new ArrayList<>();
		listKkBuffer.addAll(listKKDukcapilDto);
		List<KKDukcapilDto> buffer2 = new ArrayList<>();
		buffer2.addAll(listKKDukcapilDto);
		List<KKDukcapilDto> listKkNew = new ArrayList<>();
		List<Date> listTgl = new ArrayList<>();
		map.put("NOKK", kak.getnOKK() );
		map.put("ALAMAT_KK", kak.getaLAMAT());
		map.put("USERID", getCurrentUserSessionJR().getLoginID());
		map.put("USERNAME", getCurrentUserSessionJR().getUserName());
		for(KKDukcapilDto kk : listKkBuffer){
			if(kk.getnIK().equalsIgnoreCase(nikRequest)){
				kkRequest = new KKDukcapilDto(kk);
				map.put("NAMA_REQUEST", kk.getnAMALGKP());
				map.put("NIK_REQUEST", kk.getnIK());
			}
			if(kk.getsTATHBKEL().equalsIgnoreCase("KEPALA KELUARGA")){
				listKkNew.add(kk);
				kakKepala = new KKDukcapilDto(kk);
				map.put("KEPALA_KELUARGA", kk.getnIK());
				buffer2.remove(kk);
			}
			if(kk.getsTATHBKEL().equalsIgnoreCase("ISTRI")){
				listKkNew.add(kk);
				buffer2.remove(kk);
			}
		}
		for(KKDukcapilDto kk : buffer2){
			try{
				listTgl.add(stringKeTanggal(kk.gettGLLHR()));
			}catch(Exception s){
				s.printStackTrace();
			}
		}
		Collections.sort(listTgl);
		for(Date a : listTgl){
			try{
				for(KKDukcapilDto kk : listKkBuffer){
					if(stringKeTanggal(kk.gettGLLHR()).compareTo(a)==0){
						listKkNew.add(kk);
						buffer2.remove(kk);
						break;
					}
				}
			}catch(Exception s){
				s.printStackTrace();
			}
		}
		int i = 1;
		for(KKDukcapilDto kk : listKkNew){
			map.put("NAMA_LGKP_"+i, kk.getnAMALGKP());
			map.put("STAT_HBKEL_"+i, kk.getsTATHBKEL());
			map.put("AGAMA_"+i, kk.getaGAMA());
			map.put("JENIS_PKRJN_"+i, kk.getjENISPKRJN());
			map.put("PDDK_AKH_"+i, kk.getpDDKAKH());
			map.put("TMPT_LHR_"+i, kk.gettMPTLHR());
			map.put("STATUS_KAWIN_"+i,kk.getsTATUSKAWIN());
			map.put("GOL_DARAH_"+i, kk.getgOLDARAH());
			map.put("JENIS_KLMIN_"+i, kk.getjENISKLMIN());
			map.put("NIK_"+i, kk.getnIK());
			map.put("NIK_NAMA_LGKP_IBU_"+i,kk.getnAMALGKPIBU());
			map.put("TGL_LHR_"+i, kk.gettGLLHR());
			map.put("NAMA_LGKP_AYAH_"+i, kk.getnAMALGKPAYAH());
			map.put("NAMA_LGKP_IBU_"+i, kk.getnAMALGKPIBU());
			map.put("KEWARGANEGARAAN_"+i, "WNI");
			i++;
		}
		for(int k=i;k<=10;k++){
			map.put("NAMA_LGKP_"+k,"");
			map.put("STAT_HBKEL_"+k, "");
			map.put("AGAMA_"+k, "");
			map.put("JENIS_PKRJN_"+k, "");
			map.put("PDDK_AKH_"+k, "");
			map.put("TMPT_LHR_"+k, "");
			map.put("STATUS_KAWIN_"+k,"");
			map.put("GOL_DARAH_"+k, "");
			map.put("JENIS_KLMIN_"+k, "");
			map.put("NIK_"+k, "");
			map.put("NIK_NAMA_LGKP_IBU_"+k,"");
			map.put("TGL_LHR_"+k, "");
			map.put("NAMA_LGKP_AYAH_"+k, "");
			map.put("NAMA_LGKP_IBU_"+k, "");
			map.put("KEWARGANEGARAAN_"+k, "");
		}
		
		return map;
	}

	public KKDukcapilDto getKkDucapilDto() {
		return kkDucapilDto;
	}

	public void setKkDucapilDto(KKDukcapilDto kkDucapilDto) {
		this.kkDucapilDto = kkDucapilDto;
	}

	public List<KKDukcapilDto> getListKKDukcapilDto() {
		return listKKDukcapilDto;
	}

	public void setListKKDukcapilDto(List<KKDukcapilDto> listKKDukcapilDto) {
		this.listKKDukcapilDto = listKKDukcapilDto;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	
}
