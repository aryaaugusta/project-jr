package ui.operasional.dokumen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;

import share.DasiJrRefCodeDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import common.ui.DocumentUtil;

public class DokumenPendampingPenyelesaian extends DocumentUtil{

	private static Logger logger = LoggerFactory.getLogger(DokumenPendampingPenyelesaian.class);
	
	@Init
	public void init(){
		logger.debug("Comencing Print VM");
	}
	
	@GlobalCommand("printSuratPemberitahuanOverbookKpdKorban")
	public void cetakSuratPemberitahuanOverbookKpdKorban(PlPengajuanSantunanDto dto)throws IOException{
		logger.debug("Entering document creating process");
		SimpleDateFormat waktu = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		SimpleDateFormat tgl = new SimpleDateFormat("dd/MM/YYYY");
		DecimalFormat formatUang = new DecimalFormat("###,###,###");
		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();
		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}
		String name = "SuratPemberitahuanOverbookKpdKorban.docx";
		
		XWPFParagraph par1 = document.createParagraph();
		par1.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run1 = par1.createRun();
		run1.setText("Nomor	:	PP/R/       /2016");
		run1.addBreak();
		run1.setText("Sifat	:	Penting");
		run1.addBreak();
		run1.setText("Hal	:	Pembayaran Santunan Asuransi Kecelakaan JASA RAHARJA");
		run1.addBreak();
		
		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.LEFT);
		
		XWPFRun run2 = par2.createRun();
		run2.setText("Yth. Bpk./lbu/Sdr   "+dto.getNamaKorban());
		run2.addBreak();
		run2.setText(dto.getAlamatKorban());
		run2.addBreak();
		
		XWPFParagraph par3 = document.createParagraph();
		par3.setAlignment(ParagraphAlignment.LEFT);
		
		XWPFRun run3 = par3.createRun();
		run3.setText("Melalui surat ini, kami beritahukan bahwa Bapakl/bu/Saudara sebagai korban kecelakaan lalu lintas berhak mendapatkan santunan biaya "
				+ "perawatan dari PT Jasa Raharja (Persero) maksimal sebesar Rp.20.000.000,00 (dua puluh juta rupiah) dan dalam hal ini telah dibayarkan "
				+ "secara langsung kepada "+dto.getAlamatRS()+" yang merupakan mitra kami.");
		run3.addBreak();
		
		XWPFParagraph par4 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.LEFT);
		
		XWPFRun run4 = par4.createRun();
		BigDecimal max = new BigDecimal(0);
		BigDecimal used = new BigDecimal(0);
		if (dto.getJumlahPengajuanMeninggal()==null) {
			max = new BigDecimal(0);
		} else {
			if (dto.getJumlahPengajuanMeninggal().intValue()>20000000) {
				max = dto.getJumlahPengajuanMeninggal();
			} else {
				max = new BigDecimal(20000000);
			}
			
		}
		if (dto.getJumlahDibayarMeninggalPenyelesaian()==null) {
			used = new BigDecimal(0);
		} else {
			used = dto.getJumlahDibayarMeninggalPenyelesaian();
		}
		BigDecimal sisa = max.subtract(used);
		run2.setText("Perincian Biaya Santunan sebagai berikut :");
		run2.addBreak();
		run2.setText("-	Besaran santunan biaya rawatan (maksimal)	Rp. "+max);
		run2.addBreak();
		run2.setText("-	Besaran santunan biaya rawatan (maksimal)	Rp. "+used);
		run2.addBreak();
		run2.setText("-	Besaran santunan biaya rawatan (maksimal)	Rp. "+sisa);
		run2.addBreak();
		
		XWPFParagraph par5 = document.createParagraph();
		par5.setAlignment(ParagraphAlignment.LEFT);
		
		XWPFRun run5 = par5.createRun();
		run5.setText("Dalam hal Bapak/lbu/Saudara/i masih membutuhkan biaya rawat lanjutan, dapat "
				+ "mengajukan penggantian biaya rawat sebesar nilai santunan yang masih tersedia.");
		run5.addBreak();
		
		XWPFParagraph par6 = document.createParagraph();
		par6.setAlignment(ParagraphAlignment.LEFT);
		
		XWPFRun run6 = par6.createRun();
		run6.setText("Apabila Bapak/lbu/Saudara/i memerlukan informasi Iebih lanjut, silahkan "
				+ "menghubungi petugas kami di Kantor Cabang / Perwakilan / KPJR di nomor telepon <<nmr_telp>>.");
		run6.addBreak();
		
		XWPFParagraph par7 = document.createParagraph();
		par7.setAlignment(ParagraphAlignment.LEFT);
		
		XWPFRun run7 = par7.createRun();
		run7.setText("Kami terus berupaya memberikan pelayanan yang terbaik untuk Bapakllbu/Saudara/i. "
				+ "Semoga   Bapak/lbu/Saudara/i selalu diberikan kesehatan.");
		run7.addBreak();
		run7.addBreak();

		XWPFParagraph par8 = document.createParagraph();
		par8.setAlignment(ParagraphAlignment.LEFT);
		
		XWPFRun run8 = par8.createRun();
		run8.setText("Hormat Kami,");
		run8.addBreak();
		run8.setText("PT Jasa Raharja (Persero)");
		run8.addBreak();
		run8.addBreak();
		run8.addBreak();
		run8.addBreak();
		run8.setText("..........................");
		run8.addBreak();
		run8.setText("Kepala Cabang");

		// download dokumen
		File temp = File.createTempFile("SuratPemberitahuanOverbookKpdKorban", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("SuratPemberitahuanOverbookKpdKorban", ".docx", "application/file",fis));
		temp.delete();
		
		
		
		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");
		
	}
	
	
	@GlobalCommand("printSuratPengantarPelimpahan")
	public void cetakSuratPengantarPelimpahan(PlPengajuanSantunanDto dto,List<DasiJrRefCodeDto> listBerkas)throws IOException{
		logger.debug("Entering document creating process");
		SimpleDateFormat waktu = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		SimpleDateFormat tgl = new SimpleDateFormat("dd/MM/YYYY");
		SimpleDateFormat tanggal = new SimpleDateFormat("dd MMMM YYYY");
		DecimalFormat formatUang = new DecimalFormat("###,###,###");
		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();
		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}
		String name = "SuratPngantarPelimpahan.docx";
		
		XWPFParagraph par1 = document.createParagraph();
		par1.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run1 = par1.createRun();
		run1.setFontSize(22);
		run1.setBold(true);
		run1.setText("JASA RAHARJA");
		
		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run2 = par2.createRun();
		run2.setFontSize(8);
		run2.setText("Utama dalam Perlindungan, Prima dalam Pelayanan");
		run2.addBreak();
		
		XWPFParagraph par3 = document.createParagraph();
		par3.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run3 = par3.createRun();
		run3.setText("Jakarta,"+tanggal.format(new Date()));
		run3.addBreak();
		
		XWPFParagraph par4 = document.createParagraph();
		par4.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run4 = par4.createRun();
		run2.setText("Nomor");
		run2.addTab();
		run2.setText(": 12325");
		run2.addBreak();
		run2.setText("Sifat");
		run2.addTab();
		run2.setText(": Penting");
		run2.addBreak();
		run2.setText("Hal");
		run2.addTab();
		run2.setText(": Pelimpahan Berkas Santunan");
		run2.addBreak();
		run2.addTab();
		run2.setText("  a.n.  "+dto.getNamaKorban());
		run2.addBreak();
		run2.addTab();
		run2.setText("  Berkas No. "+dto.getNoBerkas());
		run2.addBreak();
		
		XWPFParagraph par5 = document.createParagraph();
		par5.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run5 = par5.createRun();
		run5.setText("Yth : KEPALA "+dto.getNamaKantorDilimpahkan());
		run5.addBreak();
		
		XWPFParagraph par6 = document.createParagraph();
		par6.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run6 = par6.createRun();
		run6.setText("Sehubungan dengan pengajuan santunan "
				+ "atas nama korban tersebut diatas, yang mengalami kecelakaan di "+dto.getDeskripsiLokasi()+" "
				+ "dan karena korban / ahli waris korban berdomisili "
				+ "(sesuai identitas diri) di dalam wilayah unit kerja Saudara dengan alamat "+dto.getAlamatKorban()+" "+dto.getNamaKorban()+" , "
				+ "maka untuk penyelesaian selanjutnya, terlampir kami sampaikan berkas pengajuan santunan bersangkutan terdiri dari :");
		run6.addBreak();
		
		XWPFParagraph par7 = document.createParagraph();
		par7.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run7 = par7.createRun();
		int i = 0;
		for (DasiJrRefCodeDto x : listBerkas) {
			i++;
			run7.setText(i+" " + x.getRvMeaning());
			run7.addBreak();
		}
		
		XWPFParagraph par8 = document.createParagraph();
		par8.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run8 = par8.createRun(); 
		run8.setText("Berdasarkan penelitian kami terhadap kasus kecelakaan yang dialami, disimpulkan bahwa korban terjamin sesuai dengan UU 33 th 1962.");
		run8.addBreak();
		
		XWPFParagraph par9 = document.createParagraph();
		par9.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run9 = par9.createRun(); 
		run9.setText("Atas kerjasama Saudara, kami ucapkan terima kasih.");
		run9.addBreak();
		run9.addBreak();
		
		XWPFParagraph par10 = document.createParagraph();
		par10.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run10 = par10.createRun(); 
		run10.setText(dto.getNamaKantorAsal());
		run10.addBreak();
		run10.addBreak();
		run10.addBreak();
		run10.addBreak();
		run10.addBreak();
		
		XWPFParagraph par11 = document.createParagraph();
		par11.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run11 = par11.createRun(); 
		run11.setText("nama kepala cabang");
		run11.addBreak();
		run11.setText("Kepala Cabang");
		
		// download dokumen
		File temp = File.createTempFile("SuratPngantarPelimpahan", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("SuratPngantarPelimpahan", ".docx", "application/file",fis));
		temp.delete();
		
		
		
		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");
	}
	
	@GlobalCommand("printSuratPanggilanPelimpahan")
	public void cetakSuratPanggilanPelimpahan(PlPengajuanSantunanDto dto,List<DasiJrRefCodeDto> listBerkas)throws IOException{
		logger.debug("Entering document creating process");
		SimpleDateFormat waktu = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		SimpleDateFormat tgl = new SimpleDateFormat("dd/MM/YYYY");
		SimpleDateFormat tanggal = new SimpleDateFormat("dd MMMM YYYY");
		DecimalFormat formatUang = new DecimalFormat("###,###,###");
		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();
		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}
		String name = "SuratPanggilanPelimpahan.docx";
		

		
		XWPFParagraph par1 = document.createParagraph();
		par1.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run1 = par1.createRun();
		run1.setText("Jakarta,"+tanggal.format(new Date()));
		run1.addBreak();
		
		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run2 = par2.createRun();
		run2.setText("Nomor");
		run2.addTab();
		run2.addTab();
		run2.setText(": 12325");
		run2.addBreak();
		run2.setText("Sifat");
		run2.addTab();
		run2.addTab();
		run2.setText(": Penting");
		run2.addBreak();
		run2.setText("Hal");
		run2.addTab();
		run2.addTab();
		run2.setText(": Persyaratan Memperoleh Santunan a.n. "+dto.getNamaKorban());
		run2.addBreak();
		
		
		XWPFParagraph par3 = document.createParagraph();
		par3.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run3 = par3.createRun();
		run3.setText("Yth : Kel. "+dto.getNamaKorban());
		run3.addBreak();
		run3.setText(dto.getAlamatKorban());
		
		XWPFParagraph par4 = document.createParagraph();
		par4.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run4 = par4.createRun();
		run4.setText("Berdasarkan informasi yang kami peroleh dari "
				+ dto.getNamaKantorDiajukanDi()+". nomor: "+dto.getNoLaporanPolisi()+" bahwa keluarga Saudara atas nama : "+dto.getNamaKorban()+" "
				+ " telah  mengalami kecelakaan lalu lintas, sehubungan dengan hal tersebut kami menyampaikan rasa prihatin dan duka cita "
				+ "atas musibah yang terjadi.");
		run4.addBreak();
		
		XWPFParagraph par5 = document.createParagraph();
		par5.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run5 = par5.createRun();
		run5.setText("Sesuai ketentuan yang ada  dengan memperhatikan kasus kecelakaannya, maka ahli waris korban berhak memperoleh santunan kematian dan "
				+ "perawatan dari Pemerintah  melalui  PT. Jasa Raharja (Persero), sebesar Rp. 1.000.000,00 (terbilang  Satu Juta rupiah).");
		
		XWPFParagraph par6 = document.createParagraph();
		par6.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run6 = par6.createRun();
		run6.setText("Sebelum pemberian santunan  dimaksud, kami  minta  agar Saudara  dapat melengkapi data-data seperti dibawah ini :" );
		run6.addBreak();
		
		XWPFParagraph par7 = document.createParagraph();
		par7.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run7 = par7.createRun();
		int i = 0;
		for (DasiJrRefCodeDto x : listBerkas) {
			i++;
			run7.setText(i+" " + x.getRvMeaning());
			run7.addBreak();
		}
		
		XWPFParagraph par8 = document.createParagraph();
		par8.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run8 = par8.createRun(); 
		run8.setText("Apabila persyaratan tersebut telah dilengkapi, supaya diserahkan kepada PT.Jasa Raharja (Persero) : "+dto.getNamaKantorDilimpahkan()+" ... ... ");
		run8.addBreak();
		
		XWPFParagraph par9 = document.createParagraph();
		par9.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run9 = par9.createRun(); 
		run9.setText("Atas perhatian Saudara, kami ucapkan terima kasih.");
		run9.addBreak();
		run9.addBreak();
		
		XWPFParagraph par10 = document.createParagraph();
		par10.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run10 = par10.createRun(); 
		run10.setText(dto.getNamaKantorAsal());
		run10.addBreak();
		run10.addBreak();
		run10.addBreak();
		run10.addBreak();
		run10.addBreak();
		
		XWPFParagraph par11 = document.createParagraph();
		par11.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run11 = par11.createRun(); 
		run11.setText("nama kepala cabang");
		run11.addBreak();
		run11.setText("Kepala Cabang");
		run11.addBreak();
		
		XWPFParagraph par12 = document.createParagraph();
		par12.setAlignment(ParagraphAlignment.LEFT);
		

		XWPFRun run12 = par12.createRun(); 
		run12.setText("Tembusan :   ");
		run12.addBreak();
		run12.setText("PT. Jasa Raharja (Persero) "+dto.getNamaKantorDilimpahkan());
		
		// download dokumen
		File temp = File.createTempFile("SuratPanggilanPelimpahan", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("SuratPanggilanPelimpahan", ".docx", "application/file",fis));
		temp.delete();
		
		
		
		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");
	}
}
