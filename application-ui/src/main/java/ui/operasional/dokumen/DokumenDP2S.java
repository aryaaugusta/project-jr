package ui.operasional.dokumen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;

import common.ui.DocumentUtil;
import common.util.JsonUtil;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;

public class DokumenDP2S extends DocumentUtil {

	private static Logger logger = LoggerFactory.getLogger(DokumenDP2S.class);
	private List<PlPenyelesaianSantunanDto> akumulasiPembayaran = new ArrayList<>();

	@Init
	public void init() {
		logger.debug("Comencing Print VM");
	}

	private String formatDate(Date date, String format, String defaultReturn) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			return sdf.format(date);
		} catch (Exception e) {
			return defaultReturn;
		}
	}

	private String formatDate(Date date, String format) {
		return this.formatDate(date, format, "");
	}

	private String formatDecimal(long number) {
		return this.formatDecimal(new BigDecimal(number), "###,###,###");
	}

	private String formatDecimal(BigDecimal number) {
		return this.formatDecimal(number, "###,###,###");
	}

	private String formatDecimal(BigDecimal number, String format) {
		return this.formatDecimal(number, format, "N/A");
	}

	private String formatDecimal(BigDecimal number, String format, String defaultReturn) {
		DecimalFormat formatDec = new DecimalFormat(format);
		try {
			return formatDec.format(number);
		} catch (Exception e) {
			return defaultReturn;
		}
	}

	private BigDecimal sumBigDecimal(List<BigDecimal> numbers) {
		BigDecimal result = new BigDecimal(0);
		for (BigDecimal num : numbers) {
			if (num != null) {
				result = result.add(num);
			}
		}
		return result;
	}

	@GlobalCommand("printDP2S")
	public void cetakDP2S(PlPengajuanSantunanDto dto, List<PlPenyelesaianSantunanDto> listDtos,
			PlPenyelesaianSantunanDto penyelesaian) throws IOException {
		logger.debug("Entering document creating process");
		logger.info(JsonUtil.getJson(dto));

		String formatWaktu = "dd/MM/yyyy hh:mm:ss";
		String formatTgl = "dd/MM/YYYY";

		// SimpleDateFormat waktu = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		// SimpleDateFormat tgl = new SimpleDateFormat("dd/MM/YYYY");
		// DecimalFormat formatUang = new DecimalFormat("###,###,###");
		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();
		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}
		String name = "Dokumen DP2S.docx";

		XWPFParagraph par1 = document.createParagraph();
		par1.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run1 = par1.createRun();
		// run1.setText("Tanggal Cetak : "+waktu.format(dto.getTglCetak()));
		run1.setText("Tanggal Cetak : " + this.formatDate(dto.getTglCetak(), formatWaktu));

		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.CENTER);

		XWPFRun run2 = par2.createRun();
		setRunFontText(run2, 1);
		run2.setText("DATA PENDUKUNG PENYELESAIAN SANTUNAN (DP2S)");

		XWPFTable tabela = document.createTable();
		setTableBorder(tabela, 0);

		XWPFTableRow row1 = tabela.getRow(0);
		XWPFRun runTabelARow1Cell0 = row1.getCell(0).getParagraphArray(0).createRun();
		runTabelARow1Cell0.setText("Nomor Berkas");
		setRunFontText(runTabelARow1Cell0, 1);
		setCellWidth(tabela, 0, 2000);

		XWPFRun runTabelARow1Cell1 = row1.addNewTableCell().getParagraphArray(0).createRun();
		runTabelARow1Cell1.setText(":");

		XWPFRun runTabelARow1Cell2 = row1.addNewTableCell().getParagraphArray(0).createRun();
		runTabelARow1Cell2.setText(dto.getNoBerkas());
		setRunFontText(runTabelARow1Cell2, 1);
		setCellWidth(tabela, 2, 3000);

		XWPFRun runTabelARow1Cell3 = row1.addNewTableCell().getParagraphArray(0).createRun();
		runTabelARow1Cell3.setText("Tanggal Pengajuan");
		setCellWidth(tabela, 3, 2000);

		XWPFRun runTabelARow1Cell4 = row1.addNewTableCell().getParagraphArray(0).createRun();
		runTabelARow1Cell4.setText(":");

		XWPFRun runTabelARow1Cell5 = row1.addNewTableCell().getParagraphArray(0).createRun();
		// runTabelARow1Cell5.setText(waktu.format(dto.getTglPengajuan()));
		runTabelARow1Cell5.setText(this.formatDate(dto.getTglPengajuan(), formatWaktu));

		XWPFTableRow row2 = tabela.createRow();
		row2.getCell(0).setText("Loket Penyelesaian");
		row2.getCell(1).setText(":");
		row2.getCell(2).setText(dto.getNamaKantorDilimpahkan());

		XWPFTableRow row3 = tabela.createRow();
		row3.getCell(0).setText("Santunan");

		XWPFTableRow row4 = tabela.createRow();
		row4.getCell(0).setText("");

		XWPFParagraph par3 = document.createParagraph();
		par3.setAlignment(ParagraphAlignment.LEFT);
		par3.setBorderBottom(Borders.SINGLE);
		XWPFRun run3 = par3.createRun();
		run3.setText("Data Kecelakaan");

		XWPFTable tabelb = document.createTable();
		setTableBorder(tabelb, 0);

		XWPFTableRow row1b = tabelb.getRow(0);
		row1b.getCell(0).setText("Loket yang menangani");
		row1b.addNewTableCell().setText(":");
		row1b.addNewTableCell().setText(dto.getLoketMenangani());
		row1b.addNewTableCell().setText("");
		row1b.addNewTableCell().setText("");
		row1b.addNewTableCell().setText("");

		XWPFTableRow row2b = tabelb.createRow();
		row2b.getCell(0).setText("Tgl & Jam Kejadian");
		row2b.getCell(1).setText(":");
		// row2b.getCell(2).setText(waktu.format(dto.getTglKejadian()));
		row2b.getCell(2).setText(this.formatDate(dto.getTglKejadian(), formatWaktu));

		XWPFTableRow row3b = tabelb.createRow();
		row3b.getCell(0).setText("Lokasi Kejadian");
		row3b.getCell(1).setText(":");
		row3b.getCell(2).setText(dto.getDeskripsiLokasi());

		XWPFTableRow row4b = tabelb.createRow();
		row4b.getCell(0).setText("Instansi yg Menangani");
		row4b.getCell(1).setText(":");
		row4b.getCell(2).setText(dto.getNamaInstansi());

		XWPFTableRow row5b = tabelb.createRow();
		row5b.getCell(0).setText("Lap Polisi/Bukti Kejadian");
		row5b.getCell(1).setText(":");
		row5b.getCell(2).setText(dto.getNoLaporanPolisi());
		row5b.getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));
		row5b.getCell(3).setText("Tanggal");
		row5b.getCell(4).setText(":");
		// row5b.getCell(5).setText(tgl.format(dto.getTglLaporanPolisi()));
		row5b.getCell(5).setText(this.formatDate(dto.getTglLaporanPolisi(), formatTgl));

		XWPFTableRow row6b = tabelb.createRow();
		row6b.getCell(0).setText("Kasus Kecelakaan");
		row6b.getCell(1).setText(":");
		row6b.getCell(2).setText(dto.getKasusKecelakaan());
		row6b.getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));
		row6b.getCell(3).setText("Sifat");
		row6b.getCell(4).setText(":");
		row6b.getCell(5).setText(dto.getSifatLakaDesc());

		XWPFTableRow row7b = tabelb.createRow();
		row7b.getCell(0).setText("Status Korban dalam Laka");
		row7b.getCell(1).setText(":");
		row7b.getCell(2).setText(dto.getStatusKroban());
		row7b.getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));

		XWPFTableRow row8b = tabelb.createRow();
		row8b.getCell(0).setText("No/Identitas Kend.");
		row8b.getCell(1).setText(":");
		row8b.getCell(2).setText(dto.getNoPolisi());
		row8b.getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));

		XWPFTableRow row9b = tabelb.createRow();
		row9b.getCell(0).setText("Penjamin");

		XWPFTableRow row10b = tabelb.createRow();
		row10b.getCell(0).setText("Gol/Jenis Kend. Penjamin");
		row10b.getCell(1).setText(":");
		row10b.getCell(2).setText(dto.getGolonganKendaraan() + "/" + dto.getJenisKendaraanDesc());
		row10b.getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));

		XWPFTableRow row11b = tabelb.createRow();
		row11b.getCell(0).setText("Jenis Jaminan");
		row11b.getCell(1).setText(":");
		row11b.getCell(2).setText(dto.getJaminanDesc());
		row11b.getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(4000));

		XWPFTableRow row12b = tabelb.createRow();
		row12b.getCell(0).setText("");

		XWPFParagraph parData = document.createParagraph();
		parData.setAlignment(ParagraphAlignment.LEFT);
		parData.setBorderBottom(Borders.SINGLE);
		XWPFRun runData = parData.createRun();
		runData.setText("Data Korban");
		runData.addTab();
		runData.addTab();
		runData.addTab();
		runData.addTab();
		runData.addTab();
		runData.setText("Data Pemohon");

		XWPFTable tabelc = document.createTable();
		setTableBorder(tabelc, 0);

		XWPFTableRow row1c = tabelc.getRow(0);
		row1c.getCell(0).setText("Nama");
		row1c.addNewTableCell().setText(":");
		row1c.addNewTableCell().setText(dto.getNamaKorban());
		row1c.addNewTableCell().setText("Nama");
		row1c.addNewTableCell().setText(":");
		row1c.addNewTableCell().setText(dto.getNamaPemohon());

		XWPFTableRow row3c = tabelc.createRow();
		row3c.getCell(0).setText("Umur/Jenis");
		row3c.getCell(1).setText(":");
		row3c.getCell(2).setText("" + dto.getUmur());
		row3c.getCell(3).setText("No. KTP/NIK");
		row3c.getCell(4).setText(":");
		row3c.getCell(5).setText(dto.getNoIdentitasPemohon());

		XWPFTableRow row4c = tabelc.createRow();
		row4c.getCell(0).setText("Kelamin");
		row4c.getCell(1).setText("");
		row4c.getCell(2).setText("");
		row4c.getCell(3).setText("No. HP/Telepon");
		row4c.getCell(4).setText(":");
		row4c.getCell(5).setText(dto.getNoTelpPemohon());

		XWPFTableRow row5c = tabelc.createRow();
		row5c.getCell(0).setText("No. KTP/NIK");
		row5c.getCell(1).setText(":");
		row5c.getCell(2).setText(dto.getNoIdentitasKorban());
		row5c.getCell(3).setText("Hub dg Korban");
		row5c.getCell(4).setText(":");
		row5c.getCell(5).setText(dto.getHubKorbanDesc());

		XWPFTableRow row6c = tabelc.createRow();
		row6c.getCell(0).setText("No. HP/Telepon");
		row6c.getCell(1).setText(":");
		row6c.getCell(2).setText(dto.getNoTelpKorban());
		row6c.getCell(3).setText("Alamat");
		row6c.getCell(4).setText(":");
		row6c.getCell(5).setText(dto.getAlamatPemohon());

		XWPFTableRow row7c = tabelc.createRow();
		row7c.getCell(0).setText("Cidera dalam");
		row7c.getCell(1).setText(":");
		row7c.getCell(2).setText(dto.getCideraDes());

		XWPFTableRow row8c = tabelc.createRow();
		row8c.getCell(0).setText("Laka");

		XWPFTableRow row9c = tabelc.createRow();
		row9c.getCell(0).setText("Alamat");
		row9c.getCell(1).setText(":");
		row9c.getCell(2).setText(dto.getAlamatKorban());

		XWPFTableRow row10c = tabelc.createRow();
		row10c.getCell(0).setText("");

		XWPFParagraph parData2 = document.createParagraph();
		parData2.setAlignment(ParagraphAlignment.LEFT);
		parData2.setBorderBottom(Borders.SINGLE);
		XWPFRun runData2 = parData2.createRun();
		runData2.setText("Data Pengajuan dan Penyelesaian");

		XWPFTable tabeld = document.createTable();
		setTableBorder(tabeld, 0);

		XWPFTableRow row1d = tabeld.getRow(0);
		row1d.getCell(0).setText("Rumah Sakit");
		row1d.addNewTableCell().setText(":");
		row1d.addNewTableCell().setText(dto.getNamaRS());
		spanCellsAcrossRow(tabeld, 0, 2, 2);
		row1d.addNewTableCell().setText("");
		row1d.addNewTableCell().setText("");

		XWPFTableRow row2d = tabeld.createRow();
		row2d.getCell(0).setText("Jenis & Jumlah Santunan yang Diajukan");
		row2d.getCell(1).setText(":");
//		row2d.getCell(2).setText("MD Rp" + formatUang.format(dto.getJumlahPengajuanMeninggal()));
//		row2d.getCell(3).setText("LL Rp" + formatUang.format(dto.getJumlahPengajuanLukaluka()));
		row2d.getCell(2).setText("MD Rp" + this.formatDecimal(dto.getJumlahPengajuanMeninggal()));
		row2d.getCell(3).setText("LL Rp" + this.formatDecimal(dto.getJumlahPengajuanLukaluka()));

		XWPFTableRow row3d = tabeld.createRow();
//		BigDecimal jumlah = dto.getJumlahPengajuanLukaluka().add(dto.getJumlahPengajuanMeninggal())
//				.add(dto.getJmlPengajuanAmbl()).add(dto.getJmlPengajuanP3k());

		BigDecimal jumlah = sumBigDecimal(Arrays.asList(dto.getJumlahPengajuanLukaluka(),
				dto.getJumlahPengajuanMeninggal(), dto.getJmlPengajuanAmbl(), dto.getJmlPengajuanP3k()));
		row3d.getCell(1).setText("");
//		row3d.getCell(2).setText("AMBL Rp" + formatUang.format(dto.getJmlPengajuanAmbl()));
//		row3d.getCell(3).setText("P3K Rp" + formatUang.format(dto.getJmlPengajuanP3k()));
//		row3d.getCell(4).setText("Jumlah Rp" + formatUang.format(jumlah));
		row3d.getCell(2).setText("AMBL Rp" + this.formatDecimal(dto.getJmlPengajuanAmbl()));
		row3d.getCell(3).setText("P3K Rp" + this.formatDecimal(dto.getJmlPengajuanP3k()));
		row3d.getCell(4).setText("Jumlah Rp" + this.formatDecimal(jumlah));

		XWPFTableRow row4d = tabeld.createRow();
		row4d.getCell(0).setText("Kesimpulan");
		row4d.getCell(1).setText(":");
		row4d.getCell(2).setText(dto.getKesimpulan());

		XWPFTableRow row5d = tabeld.createRow();
		row5d.getCell(0).setText("Otorisasi Pembayaran");
		row5d.getCell(1).setText(":");
		row5d.getCell(2).setText(dto.getPembayaranDes());

		XWPFTableRow row6d = tabeld.createRow();
		row6d.getCell(0).setText("Jaminan Pembayaran");
		row6d.getCell(1).setText(":");
		row6d.getCell(2).setText(dto.getJaminanDesc());

		XWPFTableRow row7d = tabeld.createRow();
		row7d.getCell(0).setText("Jumlah Santunan Dibayar");
		row7d.getCell(1).setText(":");
//		row7d.getCell(2).setText("MD Rp" + formatUang.format(dto.getJumlahPengajuanMeninggal()));
//		row7d.getCell(3).setText("LL Rp" + formatUang.format(dto.getJumlahPengajuanLukaluka()));
		row7d.getCell(2).setText("MD Rp" + this.formatDecimal(dto.getJumlahPengajuanMeninggal()));
		row7d.getCell(3).setText("LL Rp" + this.formatDecimal(dto.getJumlahPengajuanLukaluka()));

		XWPFTableRow row8d = tabeld.createRow();
		row8d.getCell(1).setText("");
//		row8d.getCell(2).setText("AMBL Rp" + formatUang.format(dto.getJmlPengajuanAmbl()));
//		row8d.getCell(3).setText("P3K Rp" + formatUang.format(dto.getJmlPengajuanP3k()));
//		row8d.getCell(4).setText("Jumlah Rp" + formatUang.format(jumlah));
		row8d.getCell(2).setText("AMBL Rp" + this.formatDecimal(dto.getJmlPengajuanAmbl()));
		row8d.getCell(3).setText("P3K Rp" + this.formatDecimal(dto.getJmlPengajuanP3k()));
		row8d.getCell(4).setText("Jumlah Rp" + this.formatDecimal(jumlah));

		XWPFTableRow row9d = tabeld.createRow();
		row9d.getCell(0).setText("Nomor Rekening");
		row9d.getCell(1).setText(":");
		row9d.getCell(2).setText(penyelesaian.getNoRekening());

		XWPFTableRow row10d = tabeld.createRow();
		row10d.getCell(0).setText("No & Tgl Surat/BKK");
		row10d.getCell(1).setText(":");
		row10d.getCell(2).setText("TANGGAL");

		XWPFTableRow row11d = tabeld.createRow();
		row11d.getCell(0).setText("Tujuan Pelimpahan");
		row11d.getCell(1).setText(":");
		row11d.getCell(2).setText(dto.getNamaKantorDilimpahkan());

		XWPFParagraph parData3 = document.createParagraph();
		parData3.setAlignment(ParagraphAlignment.LEFT);
		XWPFRun runData3 = parData3.createRun();
		runData3.setText("Akumulasi Santunan yang Sudah Dibayarkan s.d. Pengajuan ini");

		BigDecimal totMD = BigDecimal.ZERO;
		BigDecimal totLL = BigDecimal.ZERO;
		BigDecimal totCT = BigDecimal.ZERO;
		BigDecimal totPG = BigDecimal.ZERO;
		BigDecimal totAMBL = BigDecimal.ZERO;
		BigDecimal totP3K = BigDecimal.ZERO;
		for (PlPenyelesaianSantunanDto zz : listDtos) {
			totMD = totMD.add(zz.getJumlahDibayarMeninggal());
			totLL = totLL.add(zz.getJumlahDibayarLukaluka());
			if (zz.getJumlahDibayarPenguburan() == null) {
				totPG = totPG.add(BigDecimal.ZERO);
			} else {
				totPG = totPG.add(zz.getJumlahDibayarPenguburan());
			}

			if (zz.getJmlByrAmbl() == null) {
				totAMBL = totAMBL.add(BigDecimal.ZERO);
			} else {
				totAMBL = totAMBL.add(zz.getJmlByrAmbl());
			}
			if (zz.getJmlByrP3k() == null) {
				totP3K = totP3K.add(BigDecimal.ZERO);
			} else {
				totP3K = totP3K.add(zz.getJmlByrP3k());
			}

		}

		XWPFTable tabele = document.createTable();

		XWPFTableRow row1e = tabele.getRow(0);
//		row1e.getCell(0).setText("MD Rp" + formatUang.format(totMD));
		row1e.getCell(0).setText("MD Rp" + this.formatDecimal(totMD));
		row1e.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2500));
//		row1e.addNewTableCell().setText("LL Rp" + formatUang.format(totLL));
		row1e.addNewTableCell().setText("LL Rp" + this.formatDecimal(totLL));
		row1e.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2500));
//		row1e.addNewTableCell().setText("CT Rp" + formatUang.format(totCT));
		row1e.addNewTableCell().setText("CT Rp" + this.formatDecimal(totCT));
		row1e.getCell(2).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2500));
//		row1e.addNewTableCell().setText("PG Rp" + formatUang.format(totPG));
		row1e.addNewTableCell().setText("PG Rp" + this.formatDecimal(totPG));
		row1e.getCell(3).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2500));

		XWPFTableRow row2e = tabele.createRow();
//		row2e.getCell(0).setText("AMBL Rp" + formatUang.format(totAMBL));
		row2e.getCell(0).setText("AMBL Rp" + this.formatDecimal(totAMBL));
		row2e.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2500));
//		row2e.getCell(1).setText("P3K Rp" + formatUang.format(totP3K));
		row2e.getCell(1).setText("P3K Rp" + this.formatDecimal(totP3K));
		row2e.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2500));

		XWPFParagraph parData4 = document.createParagraph();
		parData4.setAlignment(ParagraphAlignment.LEFT);
		XWPFRun runData4 = parData4.createRun();
		runData4.setText("Data Penyelesaian Sebelumnya");

		XWPFTable tabelf = document.createTable();

		XWPFTableRow row1f = tabelf.getRow(0);
		XWPFRun runTabelFRow1Cell0 = row1f.getCell(0).getParagraphArray(0).createRun();
		runTabelFRow1Cell0.setText("Uraian Penyelesaian");
		setRunFontText(runTabelFRow1Cell0, 1);
		row1f.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(6500));

		XWPFRun runTabelFRow1Cell1 = row1f.addNewTableCell().getParagraphArray(0).createRun();
		runTabelFRow1Cell1.setText("Santunan Dibayarkan");
		setRunFontText(runTabelFRow1Cell1, 1);
		row1f.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(3000));

		XWPFTableRow row2f = tabelf.createRow();
		;
		XWPFRun runTabelFRow2Cell0 = row2f.getCell(0).getParagraphArray(0).createRun();
		runTabelFRow2Cell0.setText("No. Berkas/tanggal :" + dto.getNoBerkas());
		runTabelFRow2Cell0.addBreak();
		runTabelFRow2Cell0.setText("Loket Penyelesaian :" + dto.getNamaKantorDilimpahkan());
		runTabelFRow2Cell0.addBreak();
		runTabelFRow2Cell0.setText("Pemohon/hubungan:" + dto.getNamaPemohon() + "/" + dto.getHubKorbanDesc());
		runTabelFRow2Cell0.addBreak();
		runTabelFRow2Cell0.setText("Jenis Pembayaran :" + dto.getPembayaranDes());

		XWPFRun runTabelFRow2Cell1 = row2f.getCell(1).getParagraphArray(0).createRun();
//		runTabelFRow2Cell1.setText("MD	: Rp" + formatUang.format(dto.getJumlahPengajuanMeninggal()));
		runTabelFRow2Cell1.setText("MD	: Rp" + this.formatDecimal(dto.getJumlahPengajuanMeninggal()));
		runTabelFRow2Cell1.addBreak();
//		runTabelFRow2Cell1.setText("LL	: Rp" + formatUang.format(dto.getJumlahPengajuanLukaluka()));
		runTabelFRow2Cell1.setText("LL	: Rp" + this.formatDecimal(dto.getJumlahPengajuanLukaluka()));
		runTabelFRow2Cell1.addBreak();
//		runTabelFRow2Cell1.setText("CT	: Rp" + formatUang.format(0));
		runTabelFRow2Cell1.setText("CT	: Rp" + this.formatDecimal(0));
		runTabelFRow2Cell1.addBreak();
//		runTabelFRow2Cell1.setText("PG	: Rp" + formatUang.format(dto.getJumlahPengajuanPenguburan()));
		runTabelFRow2Cell1.setText("PG	: Rp" + this.formatDecimal(dto.getJumlahPengajuanPenguburan()));
		runTabelFRow2Cell1.addBreak();
//		runTabelFRow2Cell1.setText("AMBL: Rp" + formatUang.format(dto.getJmlPengajuanAmbl()));
		runTabelFRow2Cell1.setText("AMBL: Rp" + this.formatDecimal(dto.getJmlPengajuanAmbl()));
		runTabelFRow2Cell1.addBreak();
//		runTabelFRow2Cell1.setText("P3K	: Rp" + formatUang.format(dto.getJmlPengajuanP3k()));
		runTabelFRow2Cell1.setText("P3K	: Rp" + this.formatDecimal(dto.getJmlPengajuanP3k()));

		// download dokumen
		File temp = File.createTempFile("Dokumen DP2S", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Dokumen DP2S", ".docx", "application/file", fis));
		temp.delete();

		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");
	}

	public List<PlPenyelesaianSantunanDto> getAkumulasiPembayaran() {
		return akumulasiPembayaran;
	}

	public void setAkumulasiPembayaran(List<PlPenyelesaianSantunanDto> akumulasiPembayaran) {
		this.akumulasiPembayaran = akumulasiPembayaran;
	}

}
