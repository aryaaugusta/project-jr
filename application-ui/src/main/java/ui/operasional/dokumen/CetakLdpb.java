package ui.operasional.dokumen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zul.Messagebox;

import share.DasiJrRefCodeDto;
import share.PlAngkutanKecelakaanDto;
import share.PlDataKecelakaanDto;
import share.PlDisposisiDto;
import share.PlKorbanKecelakaanDto;
import share.PlPengajuanSantunanDto;
import share.PlTindakLanjutDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.ui.DocumentUtil;
import common.util.JsonUtil;

public class CetakLdpb extends BaseVmd{
	
	// =================CETAK LEMBAR DISPOSISI=================================

	private List<String> listDari = new ArrayList<>();
	private List<String> listPendapatSaranPetunjuk = new ArrayList<>();
	private List<String> listTgl = new ArrayList<>();
	private List<String> listNamaUserJr = new ArrayList<>();

	private List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
	
	private List<PlDisposisiDto> listDisposisi = new ArrayList<>();
	private PlPengajuanSantunanDto plPengajuanSantunanDto = new PlPengajuanSantunanDto();
	
	private final String WS_URI = "/OpPengajuanSantunan";
	private final String WS_URI_LAKA = "/OpDataKecelakaan";
	private final String WS_URI_LOV = "/Lov";
	
	private static Logger logger = LoggerFactory.getLogger(CetakLdpb.class);
	
	private String tglTerima;
	private String tglPenyelesaian;

	
	@Init
	public void init(){
		logger.debug("Comenching Print VM");
	}

	@SuppressWarnings("unchecked")
	@GlobalCommand("printLDBP")
	public void cetakDisposisi(PlPengajuanSantunanDto plPengajuanSantunanDto) throws IOException {
		logger.debug("Entering document creating process");
		// ==========================================GET LIST DISPOSISI=================================================
		Map<String, Object> map = new HashMap<>();
		map.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());

		RestResponse restDisposisi = callWs(WS_URI_LOV + "/getListDisposisi",
				map, HttpMethod.POST);
		try {
			listDisposisi = JsonUtil.mapJsonToListObject(
					restDisposisi.getContents(), PlDisposisiDto.class);
			for (PlDisposisiDto d : listDisposisi) {
				SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String tgl = date.format(d.getTglDisposisi());
				String jam = time.format(d.getTglDisposisi());
				listPendapatSaranPetunjuk.add(d.getDisposisi());
				listDari.add(d.getDari());
				listTgl.add(tgl);
				listTgl.add(jam);
				listNamaUserJr.add(d.getCreatedBy());
			}
			BindUtils.postNotifyChange(null, null, this, "listDisposisi");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ===============================GET PENGAJUAN SANTUNAN==========================
		Map<String, Object> mapInput2 = new HashMap<>();
		mapInput2.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse rest3 = callWs(WS_URI_LOV + "/getValueLdbp", mapInput2,
				HttpMethod.POST);
		try {
			listDto = JsonUtil.mapJsonToListObject(rest3.getContents(),
					PlPengajuanSantunanDto.class);
			for (PlPengajuanSantunanDto a : listDto) {
				SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				if(a.getTglPenerimaan()!=null){
					setTglTerima(date.format(a.getTglPenerimaan()));	
				}
				if(a.getTglPenyelesaian()!=null){
					setTglPenyelesaian(date.format(a.getTglPenyelesaian()));					
				}
				plPengajuanSantunanDto.setTglPenerimaan(a.getTglPenerimaan());
				plPengajuanSantunanDto.setTglPenyelesaian(a.getTglPenyelesaian());
				plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
				plPengajuanSantunanDto.setNamaKorban(a.getNamaKorban());
				plPengajuanSantunanDto.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
				plPengajuanSantunanDto.setInstansi(a.getInstansi());
				plPengajuanSantunanDto.setKesimpulanSementara(a.getKesimpulanSementara());
			}
			if(plPengajuanSantunanDto.getKesimpulanSementara()==null){
				plPengajuanSantunanDto.setKesimpulanSementara("");
			}
			if(getTglTerima()==null){
				setTglTerima("");
			}
			if(getTglPenyelesaian()==null){
				setTglPenyelesaian("");
			}
			BindUtils.postNotifyChange(null, null, this,"plPengajuanSantunanDto");
			BindUtils.postNotifyChange(null, null, this,"tglPenyelesaian");
			BindUtils.postNotifyChange(null, null, this,"tglTerima");

		} catch (Exception e) {
			e.printStackTrace();
		}

		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();
		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}
		String name = "Lembar Disposisi.docx";

		// bagian 1
		XWPFParagraph par1 = document.createParagraph();
		par1.setAlignment(ParagraphAlignment.CENTER);
		// set align CENTER untuk tulisan

		// run satu
		XWPFRun run1 = par1.createRun();
		run1.setText("LEMBAR DISPOSISI PENGAWAL BERKAS");

		// bag 2
		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.RIGHT);

		XWPFRun run2 = par2.createRun();
		run2.setText(" 1 ");

		// bag 3 (tabel)
		XWPFTable tabelSantunanLakaKorban = document.createTable();

		// rows
		XWPFTableRow row1Data = tabelSantunanLakaKorban.getRow(0);
		row1Data.getCell(0).setText(
				"Kesimpulan : "+plPengajuanSantunanDto.getKesimpulanSementara());
		row1Data.getCell(0).getCTTc().addNewTcPr();
		row1Data.getCell(0).getCTTc().getTcPr().addNewGridSpan();
		row1Data.getCell(0).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(2));
		row1Data.addNewTableCell().setText("Kode: ");
		row1Data.getCell(1).getCTTc().addNewTcPr();
		row1Data.getCell(1).getCTTc().getTcPr().addNewGridSpan();
		row1Data.getCell(1).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(4));
		row1Data.addNewTableCell().setText(
				"Tgl Penyelesaian : " + getTglPenyelesaian());
		row1Data.getCell(2).getCTTc().addNewTcPr();
		row1Data.getCell(2).getCTTc().getTcPr().addNewGridSpan();
		row1Data.getCell(2).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(3));
		XWPFTable tabel1 = document.createTable();

		// rows
		XWPFTableRow row1 = tabel1.getRow(0);
		row1.getCell(0)
				.setText(
						"Isi Ringkas : "+plPengajuanSantunanDto.getDeskripsiKecelakaan());
		row1.getCell(0).getCTTc().addNewTcPr();
		row1.getCell(0).getCTTc().getTcPr().addNewGridSpan();
		row1.getCell(0).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(9));

		XWPFTable tabel2 = document.createTable();

		// rows
		XWPFTableRow row2 = tabel2.getRow(0);
		row2.getCell(0).setText(
				"Asal / No Tgl : "+plPengajuanSantunanDto.getInstansi());
		row2.getCell(0).getCTTc().addNewTcPr();
		row2.getCell(0).getCTTc().getTcPr().addNewGridSpan();
		row2.getCell(0).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(5));
		row2.addNewTableCell().setText("Tgl Terima : " + getTglTerima());
		row2.getCell(1).getCTTc().addNewTcPr();
		row2.getCell(1).getCTTc().getTcPr().addNewGridSpan();
		row2.getCell(1).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(4));

		XWPFTable tabel3 = document.createTable();

		// rows
		XWPFTableRow row3 = tabel3.getRow(0);
		row3.getCell(0).setText(
				"No Berkas : " + plPengajuanSantunanDto.getNoBerkas());
		row3.getCell(0).getCTTc().addNewTcPr();
		row3.getCell(0).getCTTc().getTcPr().addNewGridSpan();
		row3.getCell(0).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(5));
		row3.addNewTableCell().setText(
				"Nama Korban : "+plPengajuanSantunanDto.getNamaKorban());
		row3.getCell(1).getCTTc().addNewTcPr();
		row3.getCell(1).getCTTc().getTcPr().addNewGridSpan();
		row3.getCell(1).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(4));

		// bag 4 (tabel)
		XWPFTable tabelDisposisi = document.createTable();

		XWPFTableRow rowHeadDisposisi = tabelDisposisi.getRow(0);
		rowHeadDisposisi.getCell(0).setText("No.");
		rowHeadDisposisi.addNewTableCell().setText("Dari");
		rowHeadDisposisi.addNewTableCell().setText("Pendapat/Saran/Petunjuk");
		rowHeadDisposisi.getCell(2).getCTTc().addNewTcPr();
		rowHeadDisposisi.getCell(2).getCTTc().getTcPr().addNewGridSpan();
		rowHeadDisposisi.getCell(2).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(5));
		rowHeadDisposisi.addNewTableCell().setText("Tgl");
		rowHeadDisposisi.addNewTableCell().setText("Nama User Dari JR");
		int a = 0;
		SimpleDateFormat tgldisposisi = new SimpleDateFormat("dd/MM/YYYY HH:mm");
		for (PlDisposisiDto x : listDisposisi) {
			a++;
			addRow(tabelDisposisi, 5, 2, Integer.toString(a), x.getDari(),
					x.getDisposisi(), tgldisposisi.format(x.getTglDisposisi()),
					x.getCreatedBy());
		}

		// XWPFTableRow row7Data = tabel.createRow();
		// row7Data.getCell(0).setText("1");
		// row7Data.getCell(1).setText("Front Office");
		// row7Data.getCell(2).setText("Isi Disposisi dari FO");
		// row7Data.getCell(3).setText("28/01/2019 10:00");
		// row7Data.getCell(4).setText("USER ONLINE");

		// download dokumen
		File temp = File.createTempFile("Document JR", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Document JR", "docx", "application/file",
				fis));
		temp.delete();

		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");
	}

	private void addRow(XWPFTable table, int b, int c, String... a) {
		if (a.length > 0) {
			XWPFTableRow row = table.createRow();
			for (int x = 0; x < a.length; x++) {
				if (x > 0 && (row.getCell(x) == null)) {
					row.addNewTableCell().setText(a[x]);
					if (x == c) {
						row.getCell(x).getCTTc().addNewTcPr();
						row.getCell(x).getCTTc().getTcPr().addNewGridSpan();
						row.getCell(x).getCTTc().getTcPr().getGridSpan()
								.setVal(BigInteger.valueOf(b));
					}

				} else {
					row.getCell(x).setText(a[x]);
					if (x == c) {
						row.getCell(x).getCTTc().addNewTcPr();
						row.getCell(x).getCTTc().getTcPr().addNewGridSpan();
						row.getCell(x).getCTTc().getTcPr().getGridSpan()
								.setVal(BigInteger.valueOf(b));
					}
				}
			}
		}

	}

	public String getTglTerima() {
		return tglTerima;
	}

	public void setTglTerima(String tglTerima) {
		this.tglTerima = tglTerima;
	}

	public String getTglPenyelesaian() {
		return tglPenyelesaian;
	}

	public void setTglPenyelesaian(String tglPenyelesaian) {
		this.tglPenyelesaian = tglPenyelesaian;
	}
	


	}



