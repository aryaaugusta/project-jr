package ui.operasional.dokumen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;

import common.ui.DocumentUtil;
import common.util.NumberUtil;
import share.PlPengajuanSantunanDto;

public class KuitansiPembayaranSantunan extends DocumentUtil{
	
	private static Logger logger = LoggerFactory.getLogger(KuitansiPembayaranSantunan.class);

	private SimpleDateFormat tgl = new SimpleDateFormat("dd MMMM YYYY");

	@Init
	public void init(){
		logger.debug("Comenching Print VM");
	}
	
	@SuppressWarnings("unchecked")
	@GlobalCommand("printKuitansi")
	public void cetakKuitansi(PlPengajuanSantunanDto dto)throws IOException{
		logger.debug("Entering document creating process");
		
		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();
		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}
		String name = "Kuitansi Penyelesaian.docx";
		
		XWPFParagraph par1 = document.createParagraph();
		par1.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run1 = par1.createRun();
		run1.setText(dto.getNoBerkas()+" "+dto.getPembayaranDes());
		
		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run2 = par2.createRun();
		run2.setText("");
		
		XWPFParagraph par3 = document.createParagraph();
		par3.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run3 = par3.createRun();
		String[] namaKantor = dto.getNamaKantorDilimpahkan().split(" ");
		String nmKantor = "";
		for (int i = 1; i < namaKantor.length; i++) {
			nmKantor = nmKantor + " " + namaKantor[i];
		}
		run3.setText("	"+nmKantor);
		
		XWPFParagraph par4 = document.createParagraph();
		par4.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run4 = par4.createRun();
		NumberUtil numberutil = new NumberUtil();
		run4.setText(numberutil.terbilang(dto.getTotalPembayaran().intValue()));

		XWPFParagraph par5 = document.createParagraph();
		par5.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run5 = par5.createRun();
		run5.setText("DANA PERTANGGUNGAN WAJIB KECELAKAAN PENUMPANG - UU "+dto.getJaminanDesc().substring(0,4));
		
		XWPFParagraph par6 = document.createParagraph();
		par6.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run6 = par6.createRun();
		run6.setText("	"+tgl.format(dto.getTglKejadian())+" di "+dto.getDeskripsiLokasi());
				
		XWPFParagraph par7 = document.createParagraph();
		par7.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run7 = par7.createRun();
		run7.setText(dto.getNoPolisi()+" "+dto.getJenisKendaraanDesc());
		
		XWPFTable tabel = document.createTable();
		setTableBorder(tabel, 0);
		XWPFTableRow row1 = tabel.getRow(0);
		row1.getCell(0).setText(dto.getNamaKorban());
		setCellWidth(tabel, 0, 5500);
		row1.addNewTableCell().setText(dto.getNamaPemohon());
		setCellWidth(tabel, 1, 5500);

		
		XWPFTableRow row2 = tabel.createRow();
		row2.getCell(0).setText(dto.getUmur().toString());
		row2.getCell(1).setText("");
		
		XWPFTableRow row3 = tabel.createRow();
		row3.getCell(0).setText(dto.getAlamatKorban());
		row3.getCell(1).setText(dto.getAlamatPemohon());
		
		XWPFTableRow row4 = tabel.createRow();
		row4.getCell(0).setText("");
		row4.getCell(1).setText("");
		
		XWPFTableRow row5 = tabel.createRow();
		row5.getCell(0).setText("");
		row5.getCell(1).setText(dto.getHubKorbanDesc());
		
		XWPFTableRow row6 = tabel.createRow();
		SimpleDateFormat jam = new SimpleDateFormat("hh:mm");
		SimpleDateFormat tgl = new SimpleDateFormat("dd MMMM YYYY");

		row6.getCell(0).setText(dto.getCideraDes()+" Ambulans P3K");
		row6.getCell(1).setText("JAM PROSES:"+jam.format(dto.getTglProsesPenyelesaian()));
		
		XWPFTableRow row7 = tabel.createRow();
		row7.getCell(0).setText("");
		row7.getCell(1).setText("JAKARTA, "+tgl.format(dto.getTglProsesPenyelesaian()));
		
		XWPFTableRow row8 = tabel.createRow();
		DecimalFormat moneyFormat = new DecimalFormat("###,###,###.##");
		row8.getCell(0).setText("******"+moneyFormat.format(dto.getTotalPembayaran()));
		row8.getCell(1).setText("");
		
		XWPFTableRow row9 = tabel.createRow();
		row9.getCell(0).setText("");
		row9.getCell(1).setText("");
		
		XWPFTableRow row10 = tabel.createRow();
		row10.getCell(0).setText(dto.getNoBpkPenyelesaian());
		row10.getCell(1).setText(dto.getNamaPemohon());
		
		// download dokumen
		File temp = File.createTempFile("Kuitansi Penyelesaian", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Kuitansi Penyelesaian", ".docx", "application/file",
				fis));
		temp.delete();

		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");		
	}

}
