
package ui.operasional.dokumen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.TabExpander;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.hibernate.id.IdentityGenerator.GetGeneratedKeysDelegate;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTHMerge;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zul.Messagebox;

import share.AuthUserDto;
import share.PlAngkutanKecelakaanDto;
import share.PlDataKecelakaanDto;
import share.PlKorbanKecelakaanDto;
import common.ui.DocumentUtil;
import common.util.JsonUtil;

public class DataKecelakaanPrint extends DocumentUtil {

	private PlDataKecelakaanDto dataKec = new PlDataKecelakaanDto();
	private List<PlAngkutanKecelakaanDto> listKend = new ArrayList<>();
	private List<PlKorbanKecelakaanDto> listKorban = new ArrayList<>();
	private AuthUserDto kepalaCabang = new AuthUserDto();
	private AuthUserDto petugas = new AuthUserDto();
	private static Logger logger = LoggerFactory.getLogger(DataKecelakaanPrint.class);
	private String lol = "TEST";
	@Init
	public void init(){
		logger.debug("Comenching Print VM");
	}
	
	@SuppressWarnings("unchecked")
	@GlobalCommand("cetakDataLaka")
	public void cetak(@BindingParam("map")Map<String, Object> map) throws Exception{
		System.out.println("=========================================================");
		System.out.println(JsonUtil.getJson(map));
//		Map<String, Object> map = new HashMap<>();
//		map = (Map<String, Object>) mapo.get("map");
		
		logger.debug("Entering document creating process");
		if(map!=null){
			if(map.get("dataKec")!=null){
//				dataKec = (PlDataKecelakaanDto) map.get("dataKec");
				dataKec = JsonUtil.mapJsonToSingleObject(map.get("dataKec"), PlDataKecelakaanDto.class);
				System.out.println(JsonUtil.getJson(dataKec));
			}
			
			if(map.get("listKend")!=null){
//				listKend = (List<PlAngkutanKecelakaanDto>) map.get("listKend");
				listKend = JsonUtil.mapJsonToListObject(map.get("listKend"), PlAngkutanKecelakaanDto.class);

			}
			
			if(map.get("listKorban")!=null){
				listKorban = JsonUtil.mapJsonToListObject(map.get("listKorban"), PlKorbanKecelakaanDto.class);
//				listKorban = (List<PlKorbanKecelakaanDto>) map.get("listKorban");
			}
			
			if(map.get("kepalaCabang")!=null){
				kepalaCabang = JsonUtil.mapJsonToSingleObject(map.get("kepalaCabang"), AuthUserDto.class);
			}
			
			if(map.get("petugas")!=null){
//				petugas = (AuthUserDto) map.get("petugas");
				petugas = JsonUtil.mapJsonToSingleObject(map.get("petugas"), AuthUserDto.class);				
			}
		}
		
		try {
			createDoc();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("======== Document Debuging ========");
			logger.debug("Creating document failed due to : ");
			e.printStackTrace();
		}
	}
	
	public void createDoc() throws Exception, Exception{
		XWPFDocument document = new XWPFDocument();
		
		//Header
		XWPFParagraph par = document.createParagraph();
		
		XWPFRun run = par.createRun();
		setAlignment(par, 1);
		run.setText("LEMBAR HASIL CETAK DATA LAKA DASI-JR");
		Date today = new Date();
		run = par.createRun();
		setAlignment(par, 2);
		par = document.createParagraph();
		run = par.createRun();
		run.setText("1.");
		run.addTab();
		run.setText("DATA KECELAKAAN");

		XWPFTable tabel = document.createTable();
		setTableBorder(tabel);
		
		XWPFTableRow row = tabel.getRow(0);
		row.getCell(0).setText("A. KECAMATAN TKP");
		row.addNewTableCell().setText(":");
		row.addNewTableCell().setText(dataKec.getCamatDesc()+", "+dataKec.getKabKotaDesc());
		row.addNewTableCell().setText(" ");;
		row.addNewTableCell().setText(" ");;
		row.addNewTableCell().setText(" ");;
		setCellWidth(tabel, 0, 3000L);
		setCellWidth(tabel, 1, 100L);
		setCellWidth(tabel, 2, 2000L);
		setCellWidth(tabel, 3, 3000L);
		setCellWidth(tabel, 4, 5000L);
		setCellWidth(tabel, 5, 3000L);
		setHMerge(tabel,0, 2, 5);
//		spanCellsAcrossRow(tabel, 0, 2, 2);
		
		addRow(tabel, "B. LOKET KANTOR", ":",dataKec.getInstansiDesc(),"","","");
		addRow(tabel, "C. INSTANSI YANG MENANGANI",":",dataKec.getInstansiDesc(),"","","");
		addRow(tabel, "D. PETUGAS YANG MENANGANI",":",dataKec.getNamaPetugas(),"","","");
		row = tabel.createRow();
		row.getCell(0).setText("E. NOMOR & TANGGAL LP");
		row.getCell(1).setText(":");
		row.getCell(2).setText(dataKec.getNoLaporanPolisi());
		row.getCell(3).setText("");
		row.getCell(4).setText("TANGGAL : "+getDateFormat(dataKec.getTglLaporanPolisi()==null?null:dataKec.getTglLaporanPolisi()));
		row.getCell(5).setText("");
		setHMerge(tabel,4, 4, 5);
//		addRow(tabel, "E. NOMOR & TANGGAL LP",":",dataKec.getNoLaporanPolisi(),"TANGGAL :",getDateFormat(dataKec.getTglLaporanPolisi()==null?null:dataKec.getTglLaporanPolisi()),"");
		
		row = tabel.createRow();
		row.getCell(0).setText("F. WAKTU KEJADIAN");
		row.getCell(1).setText(":");
		row.getCell(2).setText("HARI : "+getHari(dataKec.getTglKejadian()));
		row.getCell(3).setText("TANGGAL : "+getDateFormat(dataKec.getTglKejadian()!=null?dataKec.getTglKejadian():null));
		row.getCell(4).setText("");
//		setHMerge(tabel,5 , 3, 4);
		row.getCell(5).setText("JAM : "+ getJam(dataKec.getTglKejadian()!=null?dataKec.getTglKejadian():null));
//		addRow(tabel, "F. WAKTU KEJADIAN",":","HARI : "+getHari(dataKec.getTglKejadian()),"TANGGAL : "+getDateFormat(dataKec.getTglKejadian()!=null?dataKec.getTglKejadian():null),"JAM : "+ getJam(dataKec.getTglKejadian()!=null?dataKec.getTglKejadian():null));
		addRow(tabel, "G. URAIAN LOKASI KEJADIAN ",":",dataKec.getDeskripsiLokasi(),"","","");
		addRow(tabel, "H. KASUS & SIFAT KECELAKAAN",":",dataKec.getStatus(),"","SIFAT : "+dataKec.getSifatKecelakaan(),"");
//		setHMerge(tabel.getRow(7), 2, 5);
		addRow(tabel, "I. URAIAN KEJADIAN",":",dataKec.getDeskripsiKecelakaan(),"","","");
//		setHMerge(tabel,8 , 2, 5);
		
		par = document.createParagraph();
		run = par.createRun();
		run.addCarriageReturn();
		run.addCarriageReturn();
		run.addCarriageReturn();
		
		run.setText("2. DATA KENDARAAN");
		
		tabel = document.createTable();
		row = tabel.getRow(0);
		row.getCell(0).setText("NO");
		row.addNewTableCell().setText("NO.POL/ID");
		row.addNewTableCell().setText("GOL/JENIS");
		row.addNewTableCell().setText("PENGEMUDI");
		row.addNewTableCell().setText("PEMILIK");
		row.addNewTableCell().setText("STATUS");
		
		int nomer = 1;
		for(PlAngkutanKecelakaanDto kend : listKend){
			addRow(tabel, nomer+"",kend.getNoPolisi(),kend.getKodeJenis(),kend.getAlamatPengemudi(),kend.getNamaPemilik(),kend.getStatusDesc());
			nomer++;
		}
		
		par = document.createParagraph();
		run = par.createRun();
		run.addCarriageReturn();
		run.addCarriageReturn();
		
		run.setText("3. DATA KORBAN");
		
//		par = document.createParagraph();
		tabel = document.createTable();
		row = tabel.getRow(0);
		row.getCell(0).setText("NO");
		row.addNewTableCell().setText("NAMA/JK/UMUR");
		row.addNewTableCell().setText("ALAMAT");
		row.addNewTableCell().setText("CIDERA");
		row.addNewTableCell().setText("STATUS DLM LAKA");
		row.addNewTableCell().setText("KEND. KORBAN");
		row.addNewTableCell().setText("KEND. PENJAMIN");
		row.addNewTableCell().setText("JENIS JAMINAN");
		
		nomer = 1;
		for(PlKorbanKecelakaanDto korban : listKorban){
			addRow(tabel, ""+nomer,korban.getNamaJkUmur(),korban.getAlamat(),korban.getCideraDesc(),korban.getStatusKorbanDesc(),korban.getIdAngkutanKecelakaan(),korban.getIdAngkutanPenanggung(),korban.getKodeJaminan());
			nomer++;
		}
		
		par = document.createParagraph();
		tabel = document.createTable();
		row = tabel.getRow(0);
		row.getCell(0).setText("DI-UPDATE TANGGAL : "+getDateFormat(dataKec.getLastUpdatedDate()==null?dataKec.getCreationDate():dataKec.getLastUpdatedDate()));
		row.addNewTableCell().setText("DICETAK TANGGAL : "+getDateFormat(today));
		setCellWidth(tabel, 0, 10000L);
		setCellWidth(tabel, 1, 10000L);
		addRow(tabel, "NAMA : "+petugas.getDescription(),"NAMA :");
		addRow(tabel, "JABATAN : "+petugas.getDescription(),"JABATAN : ");
		row = tabel.createRow();
		row.getCell(0).setText("TANDA TANGAN");
		row.getCell(1).setText("TANDA TANGAN");
		addRow(tabel, " "," ");
		addRow(tabel, " "," ");
		addRow(tabel, " "," ");
		setVMerge(tabel, 0, 3, 6);
		setVMerge(tabel, 1, 3, 6);
		
		par = document.createParagraph();
		run = par.createRun();
		run.setText("MENGETAHUI");
		run.addCarriageReturn();
		run.addCarriageReturn();
		run.addCarriageReturn();
		run.addCarriageReturn();
		run.setText(kepalaCabang.getUserName());
		run.addCarriageReturn();
		run.setText(kepalaCabang.getDescription());
		
		/*FileOutputStream out = new FileOutputStream(File.createTempFile("Data Kecelakaan",".docx"));
		document.write(out);
		out.close();*/
		
		File temp = File.createTempFile("Data Kecelakaan", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Data Kecelakaan", "docx", "application/file", fis));
		temp.delete();
		
		logger.debug("Data Kecelakaan.docx written successully");
	}

	public String getLol() {
		return lol;
	}

	public void setLol(String lol) {
		this.lol = lol;
	}
}
