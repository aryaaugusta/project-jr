package ui.operasional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STPageOrientation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zul.Window;

import Bpm.Ecms.queryByNoBerkas.Response;
import Bpm.InstanceInfo.InstanceInfo;
import Bpm.ListTaskByUser.UserTask;
import Bpm.ListTaskByUser.UserTaskService;
import Bpm.StartProcess.Bpm_start_processRequest;
import Bpm.StartProcess.StartBPMSvcImpl;
import Bpm.UpdateAndInsertTask.Bpm_update_task_payloadRequest;
import Bpm.UpdateAndInsertTask.CredentialType;
import Bpm.UpdateAndInsertTask.PayloadType;
import Bpm.UpdateAndInsertTask.UpdateAndInsertTaskServiceImpl;
import share.AuthUserDto;
import share.DasiJrRefCodeDto;
import share.DukcapilWinDto;
import share.FndCamatDto;
import share.FndKantorJasaraharjaDto;
import share.KKDukcapilDto;
import share.LampiranDto;
import share.PlAdditionalDescDto;
import share.PlAngkutanKecelakaanDto;
import share.PlBerkasPengajuanDto;
import share.PlDataKecelakaanDto;
import share.PlDisposisiDto;
import share.PlInstansiDto;
import share.PlJaminanDto;
import share.PlKorbanKecelakaanDto;
import share.PlMappingCamatDto;
import share.PlPengajuanRsDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import share.PlRegisterSementaraDto;
import share.PlRumahSakitDto;
import ui.TestVmd;
import ui.operasional.dokumen.CetakLdpb;
import ui.operasional.dokumen.DataKecelakaanPrint;
import ui.operasionalDetail.DisposisiBerkasVmd;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass = true)
public class OpPlPengajuanSantunanVmd extends BaseVmd implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpPengajuanSantunan/_index.zul";
	private final String INDEX_PAGE_PATH2 = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/CariDataKecelakaan.zul";

	private final String DETAIL_PAGE_PATH2 = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DataPengajuanSantunan.zul";

	private final String RINGKASAN_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DetailRingkasanPengajuan.zul";

	private final String DATA_LAKA_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DataKecelakaanDetail.zul";

	private final String LAMPIRAN_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DaftarLampiran.zul";

	private final String DISPOSISI_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DisposisiDetail.zul";

	private final String ENTRI_JAMINAN_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/EntriJaminanLanjutan.zul";

	private final String WS_URI = "/OpPengajuanSantunan";
	private final String WS_URI_REGISTER = "/OperasionalRegisterSementara";
	private final String WS_URI_LAKA = "/OpDataKecelakaan";
	private final String WS_URI_LOV = "/Lov";

	private PlPengajuanSantunanDto plPengajuanSantunanDto = new PlPengajuanSantunanDto();
	private List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
	private List<PlPengajuanSantunanDto> listDtoCopy = new ArrayList<>();

	private PlPengajuanSantunanDto selectedPengajuan = new PlPengajuanSantunanDto();

	private PlRegisterSementaraDto plRegisterSementaraDto = new PlRegisterSementaraDto();
	private List<PlRegisterSementaraDto> listRegisterSementaraDtos = new ArrayList<>();

	private List<FndKantorJasaraharjaDto> listKantorDto = new ArrayList<>();
	private FndKantorJasaraharjaDto fndKantorJasaraharjaDto = new FndKantorJasaraharjaDto();

	private List<PlDataKecelakaanDto> listDataLakaDto = new ArrayList<>();
	private PlDataKecelakaanDto plDataKecelakaanDto = new PlDataKecelakaanDto();
	private PlDataKecelakaanDto selectedKecelakaan = new PlDataKecelakaanDto();

	private DasiJrRefCodeDto dasiJrRefCodeDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listStatusHubungan = new ArrayList<>();
	private DasiJrRefCodeDto statusHubunganDto = new DasiJrRefCodeDto();

	List<DasiJrRefCodeDto> listJenisIdentitasDto = new ArrayList<>();
	private DasiJrRefCodeDto identitasDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listSifatCidera = new ArrayList<>();
	private DasiJrRefCodeDto sifatCideraDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listStatusProses = new ArrayList<>();
	private DasiJrRefCodeDto statusProsesDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listPenyelesaian = new ArrayList<>();
	private DasiJrRefCodeDto statusPenyelesainDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listJenisPembayaran = new ArrayList<>();
	private DasiJrRefCodeDto jenisPembayaranDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listJenisDokumenDtoSelected = new ArrayList<>();
	private List<DasiJrRefCodeDto> listJenisDokumenDto = new ArrayList<>();
	private DasiJrRefCodeDto jenisDokumenDto = new DasiJrRefCodeDto();

	private List<PlJaminanDto> listPertanggungan = new ArrayList<>();
	private PlJaminanDto plJaminanDto = new PlJaminanDto();

	private List<PlKorbanKecelakaanDto> listKorban = new ArrayList<>();
	private PlKorbanKecelakaanDto plKorbanKecelakaanDto = new PlKorbanKecelakaanDto();
	private PlKorbanKecelakaanDto selectedKorban = new PlKorbanKecelakaanDto();

	private List<PlDataKecelakaanDto> listPlDataKecelakaanDtos = new ArrayList<>();

	private List<PlJaminanDto> listJaminanDto = new ArrayList<>();
	private PlJaminanDto jaminanDto = new PlJaminanDto();

	private List<PlInstansiDto> listInstansi = new ArrayList<>();
	private PlInstansiDto PlInstansiDto = new PlInstansiDto();

	private List<PlRumahSakitDto> listRs = new ArrayList<>();
	private PlRumahSakitDto plRumahSakitDto = new PlRumahSakitDto();

	private List<FndCamatDto> fndCamatDtos = new ArrayList<>();
	
	private List<FndCamatDto> listProvinsiDto = new ArrayList<>();
	private List<FndCamatDto> listKabkotaDto = new ArrayList<>();
	private List<FndCamatDto> listCamatDto = new ArrayList<>();

	private FndCamatDto provinsiDto = new FndCamatDto();
	private FndCamatDto kabKotaDto = new FndCamatDto();
	private FndCamatDto camatDto = new FndCamatDto();

	private List<PlAdditionalDescDto> listPlAdditionalDescDto = new ArrayList<>();
	private PlAdditionalDescDto plAdditionalDescDto = new PlAdditionalDescDto();

	private List<PlPengajuanRsDto> listPlPengajuanRsDto = new ArrayList<>();
	private PlPengajuanRsDto plPengajuanRsDto = new PlPengajuanRsDto();

	private List<PlDisposisiDto> listDisposisi = new ArrayList<>();
	private PlDisposisiDto plDisposisiDto = new PlDisposisiDto();

	private List<PlPenyelesaianSantunanDto> listPlPenyelesaianSantunanDto = new ArrayList<>();
	private PlPenyelesaianSantunanDto plPenyelesaianSantunanDto = new PlPenyelesaianSantunanDto();

	// private LampiranDto lampiran;

	// for search
	private List<String> listJenisBerkas = new ArrayList<>();
	private String searchDiajukanDi;
	private List<FndKantorJasaraharjaDto> listDiajukanDiDto = new ArrayList<>();
	private String pengajuanDay;
	private String pengajuanMonth;
	private String pengajuanYear;
	private String lakaDay;
	private String lakaMonth;
	private String lakaYear;
	private String namaPemohon;
	private String namaKorban;
	private String noBerkas;
	private String dilimpahkanKe;
	private String searchIndex;
	private String searchKantorDiajukan;
	private String searchStatusProses;
	private String searchStatusPenyelesaian;
	private String searchRs;
	List<String> listMonthPengajuan = new ArrayList<>();
	List<String> listMonthLaka = new ArrayList<>();
	private String pengajuanMonthStr;
	private String lakaMonthStr;
	private String pilihJenisBerkas;
	private String searchInstansi;
	private String noLaporan;
	private List<PlInstansiDto> listInstansiDto = new ArrayList<>();
	private PlInstansiDto instansiDto = new PlInstansiDto();

	private String searchProvinsi;
	private String searchCamat;
	private String searchKabkota;

	private int pageSize = 5;
	private boolean listIndex = true;

	private boolean editBPM = true;

	private UserTask userTask;

	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	public boolean isEditBPM() {
		return editBPM;
	}

	public void setEditBPM(boolean editBPM) {
		this.editBPM = editBPM;
	}

	private Date tglPenerimaan;
	private Date jamPenerimaan;

	private Form formMaster = new SimpleForm();
	private Form formDetail = new SimpleForm();

	private boolean formDataPemohon;
	private boolean formDataPengajuan;

	private boolean modeDisableBiaya2;
	private BigDecimal biaya1;
	private BigDecimal biaya2;
	private String cideraKode;
	private String cideraKode2;

	// variable onView
	private Date tglKejadian;
	private Date jamKejadian;
	private String statusLaporan;
	private String laporan1;
	private String laporan2;
	private String laporan3;

	private boolean showListDisposisi = false;
	private boolean showFormDisposisi = false;
	private Date tglDisposisi;
	private Date jamDisposisi;

	private boolean checkDeskripsi;
	private boolean checkedSave = true;

	private boolean disableDisposisi = true;
	private boolean disableProsesSelanjutnya = true;
	private boolean disableSave = true;

	// ====================== CEK VALIDASI IDENTITAS===================
	private String validasiIdentitas;
	private String styleValidasiIdentitas;

	// ===================FOR CARI DATA LAKA=======================

	private List<PlJaminanDto> listJenisPertanggunganDto = new ArrayList<>();
	private PlJaminanDto jenisPertanggunganDto = new PlJaminanDto();

	private List<PlDataKecelakaanDto> listDataLakaDtoCopy = new ArrayList<>();
	private PlDataKecelakaanDto dataLakaDto = new PlDataKecelakaanDto();

	// filter
	private Date detailKejadianStartDate;
	private Date detailKejadianEndDate;
	private String detailInstansi;
	private Date detailLaporanStartDate;
	private Date detailLaporanEndDate;
	private String detailNoLaporanPolisi;
	private String detailNamaKorban;
	private List<String> listStatusLp = new ArrayList<>();
	private String statusLP;
	// ============================================================

	// validasi
	private List<PlPengajuanSantunanDto> listPengajuan = new ArrayList<>();

	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent()
			.getAttribute(UIConstants.SESS_LOGIN_ID);

	// ADDED BY LUTHFI
	private PlMappingCamatDto plMappingCamatDto = new PlMappingCamatDto();
	private List<DasiJrRefCodeDto> listOtorisasiFlag = new ArrayList<>();
	
	private DukcapilWinDto dukcapilWinDto = null;
	private boolean jaminanGandaFlag;
	private List<LampiranDto> lampirans;
	
	private PlBerkasPengajuanDto plBerkasPengajuanDto = new PlBerkasPengajuanDto();
	private List<PlBerkasPengajuanDto> listBerkasPengajuanDtos = new ArrayList<>();
	private List<PlBerkasPengajuanDto> listKelengkapanBerkas = new ArrayList<>();
	
	private Date tglTerimaBerkas;
	private boolean saveSantunanFlag;
	private boolean cekKtpButton;	

	public void listJenisBerkas() {
		setPilihJenisBerkas("-");
		listJenisBerkas.add("-");
		listJenisBerkas.add("Berkas Sendiri");
		listJenisBerkas.add("Pelimpahan Masuk");
		BindUtils.postNotifyChange(null, null, this, "listJenisBerkas");
		BindUtils.postNotifyChange(null, null, this, "pilihJenisBerkas");
	}

	@NotifyChange("listDokumen")
	protected void loadList() {
		listJenisBerkas();
		HashMap<String, Object> map = new HashMap<>();
		map.put("search", userSession.getKantor());
		RestResponse listKantorDiajukan = callWs(WS_URI_LOV + "/getAsalBerkas",
				map, HttpMethod.POST);
		try {
			listKantorDto = JsonUtil.mapJsonToListObject(
					listKantorDiajukan.getContents(),
					FndKantorJasaraharjaDto.class);
			for (FndKantorJasaraharjaDto kantorDiajukan : listKantorDto) {
				// setSearchKantorDiajukan(kantorDiajukan.getKodeKantorJr()
				// + " - " + kantorDiajukan.getNama());
				fndKantorJasaraharjaDto.setKodeNama(kantorDiajukan
						.getKodeKantorJr() + " - " + kantorDiajukan.getNama());
			}
			BindUtils.postNotifyChange(null, null, this, "listKantorDto");
			BindUtils
					.postNotifyChange(null, null, this, "searchKantorDiajukan");
			for (FndKantorJasaraharjaDto diajukanDi : listKantorDto) {
				if (fndKantorJasaraharjaDto.getKodeNama().equalsIgnoreCase(
						diajukanDi.getKodeNama())) {
					fndKantorJasaraharjaDto = new FndKantorJasaraharjaDto();
					fndKantorJasaraharjaDto = diajukanDi;
				}
			}
			BindUtils.postNotifyChange(null, null, this,
					"fndKantorJasaraharjaDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * PlDataKecelakaanDto dataLaka = (PlDataKecelakaanDto)
		 * Sessions.getCurrent() .getAttribute("obj2");
		 * System.out.println("Id Kecelakaan : " + dataLaka.getIdKecelakaan());
		 * System.out.println("Korban Kecelakaan : " +
		 * dataLaka.getIdKorbanKecelakaan());
		 * System.out.println("Asal Berkas : " + dataLaka.getAsalBerkas());
		 */

		listStatusProses();
		listStatusPenyelesaian();
		// listRs();
		// sifatCidera();
		// searchProvinsi();
		// getKabkota();
		// getCamat();
		// jenisPembayaran();
		listDokumen();
		formDataPemohonOpen();
		listMonthPengajuan();
		listMonthLaka();
		// String[] months = new DateFormatSymbols().getMonths();
		// for (int i = 0; i < months.length; i++) {
		// listMonthPengajuan.add(months[i]);
		// listMonthLaka.add(months[i]);
		// }
		UserTask ut = (UserTask) Executions.getCurrent().getAttribute("obj2");
		if (ut != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			setNoBerkas(ut.getNomorPermohonan());
			setFndKantorJasaraharjaDto(null);
			userTask = ut;
			this.noBerkas = ut.getNomorPermohonan();
			search();
			setPengajuanDay("");
			setPengajuanYear("");
			// tambahan
			// getPageInfo().setAddDetailMode(true);
			setEditBPM(false);
			// tambahan
			// getPageInfo().setEditMode(false);
			// getPageInfo().setEditDetailMode(false);
			// Date tgl = null;
			// try {
			// tgl = sdf.parse(ut.getCreationDate());
			// } catch (ParseException e) {
			// e.printStackTrace();
			// }
			// setTglPenerimaan(tgl);
		} else {
			setNoBerkas(null);
			// setTglPenerimaan(null);
		}
	}

	public void listMonthPengajuan() {
		setPengajuanMonthStr("-");
		listMonthPengajuan.add("-");
		listMonthPengajuan.add("Januari");
		listMonthPengajuan.add("Februari");
		listMonthPengajuan.add("Maret");
		listMonthPengajuan.add("April");
		listMonthPengajuan.add("Mei");
		listMonthPengajuan.add("Juni");
		listMonthPengajuan.add("Juli");
		listMonthPengajuan.add("Agustus");
		listMonthPengajuan.add("September");
		listMonthPengajuan.add("Oktober");
		listMonthPengajuan.add("November");
		listMonthPengajuan.add("Desember");
		BindUtils.postNotifyChange(null, null, this, "pengajuanMonthStr");
		BindUtils.postNotifyChange(null, null, this, "listMonthPengajuan");
	}

	public void listMonthLaka() {
		setLakaMonthStr("-");
		listMonthLaka.add("-");
		listMonthLaka.add("Januari");
		listMonthLaka.add("Februari");
		listMonthLaka.add("Maret");
		listMonthLaka.add("April");
		listMonthLaka.add("Mei");
		listMonthLaka.add("Juni");
		listMonthLaka.add("Juli");
		listMonthLaka.add("Agustus");
		listMonthLaka.add("September");
		listMonthLaka.add("Oktober");
		listMonthLaka.add("November");
		listMonthLaka.add("Desember");
		BindUtils.postNotifyChange(null, null, this, "lakaMonthStr");
		BindUtils.postNotifyChange(null, null, this, "listMonthLaka");
	}

	@NotifyChange("listDtoCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listDtoCopy));
		if (listDtoCopy != null || listDtoCopy.size() > 0) {
			listDtoCopy.clear();
		}
		if (listDto != null && listDto.size() > 0) {
			for (PlPengajuanSantunanDto dto : listDto) {
				System.out.println("+++");
				if (dto.getTglPengajuan() != null) {
					if (dto.getDiajukanDi().toUpperCase().contains(cari)
							|| dto.getNoBerkas().toUpperCase().contains(cari)
							|| dto.getNama().toUpperCase().contains(cari)
							|| dto.getCideraKorban().toUpperCase()
									.contains(cari)
							|| dto.getNamaPemohon().toUpperCase()
									.contains(cari)
							|| dto.getStatusProsesDesc().toUpperCase()
									.contains(cari)
							|| sdf.format(dto.getTglPengajuan()).equals(cari)
							|| date.format(dto.getTglPengajuan()).equals(cari)
							|| month.format(dto.getTglPengajuan()).equals(cari)
							|| year.format(dto.getTglPengajuan()).equals(cari)
							|| dto.getPenyelesaian().toUpperCase()
									.contains(cari))

					{

						listDtoCopy.add(dto);

					}
				} else {
					if (dto.getDiajukanDi().toUpperCase().contains(cari)
							|| dto.getNoBerkas().toUpperCase().contains(cari)
							|| dto.getNama().toUpperCase().contains(cari)
							|| dto.getCideraKorban().toUpperCase()
									.contains(cari)
							|| dto.getNamaPemohon().toUpperCase()
									.contains(cari)
							|| dto.getStatusProsesDesc().toUpperCase()
									.contains(cari)
							|| dto.getPenyelesaian().toUpperCase()
									.contains(cari)) {
						listDtoCopy.add(dto);
					}
				}
			}
		}
	}
	
	public String generateNoBerkas() {
		Date date = new Date();
		SimpleDateFormat mm = new SimpleDateFormat("MM");
		SimpleDateFormat yyyy = new SimpleDateFormat("yyyy");
		
		// for validasi
		String a1 = null;
		String a2 = null;
		String a3 = null;
		String a4 = null;
		String a5 = null;
		String a6 = null;

		// Validasi 1 jenis pertanggungan
		if (plJaminanDto.getLingkupJaminan().equalsIgnoreCase("33")) {
			a1 = "1";
		} else if (plJaminanDto.getLingkupJaminan().equalsIgnoreCase("34")) {
			a1 = "2";
		}

		// Validasi 2 berapa kali mengajukan
		if (plDataKecelakaanDto.getIdKorbanKecelakaan() != null
				|| plPengajuanSantunanDto.getIdKorbanKecelakaan() != null) {
			Map<String, Object> map = new HashMap<>();
			map.put("idKorban", plDataKecelakaanDto.getIdKorbanKecelakaan());

			RestResponse rest = callWs(WS_URI + "/getPengajuanByIdKorban", map,
					HttpMethod.POST);
			String jumlahPengajuan = null;
			try {
				listPengajuan = JsonUtil.mapJsonToListObject(
						rest.getContents(), PlPengajuanSantunanDto.class);
				if (listPengajuan.size() != 0) {
					for (PlPengajuanSantunanDto a : listPengajuan) {
						jumlahPengajuan = a.getNoBerkas();
					}
					if (jumlahPengajuan != null) {
						String incVal = jumlahPengajuan.substring(6, 8);
						int countVal = Integer.valueOf(incVal) + 1;
						jumlahPengajuan = String.format("%02d", countVal);
						a2 = jumlahPengajuan;
					}
				} else {
					jumlahPengajuan = "00";
					a2 = jumlahPengajuan;
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
				// e.printStackTrace();
			}
		}
		// Validasi 3 2char session kantor login 2 digit awal
		a3 = userSession.getKantor().substring(0, 2);
		// validasi 4 2char kantor perwakilan jr
		a4 = userSession.getKantor().substring(3, 5);

		// validasi 5 2char bulan
		a5 = mm.format(date);

		// validasi 6 4char tahun
		a6 = yyyy.format(date);
		
		return a1 + "-" + "001" + "-" + a2 + "-" + a3 + "-" + a4 + "-" + a5 + "-" + a6;
	}

	@Command
	public boolean noBerkasValidasi() {
		if (plPengajuanSantunanDto.getNoBerkas() == null || plPengajuanSantunanDto.getNoBerkas().trim().length() != 22) {
			return false;
		}
		String b = plPengajuanSantunanDto.getNoBerkas();
		String b1 = b.substring(0, 1);
		String b2 = b.substring(6, 8);
		String b3 = b.substring(9, 11);
		String b4 = b.substring(12, 14);
		String b5 = b.substring(15, 17);
		String b6 = b.substring(18, 22);

		Date date = new Date();
		SimpleDateFormat mm = new SimpleDateFormat("MM");
		SimpleDateFormat yyyy = new SimpleDateFormat("yyyy");

		// for validasi
		String a1 = null;
		String a2 = null;
		String a3 = null;
		String a4 = null;
		String a5 = null;
		String a6 = null;

		// Validasi 1 jenis pertanggungan
		if (plJaminanDto.getLingkupJaminan().equalsIgnoreCase("33")) {
			a1 = "1";
		} else if (plJaminanDto.getLingkupJaminan().equalsIgnoreCase("34")) {
			a1 = "2";
		}

		// Validasi 2 berapa kali mengajukan
		if (plDataKecelakaanDto.getIdKorbanKecelakaan() != null
				|| plPengajuanSantunanDto.getIdKorbanKecelakaan() != null) {
			Map<String, Object> map = new HashMap<>();
			map.put("idKorban", plDataKecelakaanDto.getIdKorbanKecelakaan());

			RestResponse rest = callWs(WS_URI + "/getPengajuanByIdKorban", map,
					HttpMethod.POST);
			String jumlahPengajuan = null;
			try {
				listPengajuan = JsonUtil.mapJsonToListObject(
						rest.getContents(), PlPengajuanSantunanDto.class);
				if (listPengajuan.size() != 0) {
					for (PlPengajuanSantunanDto a : listPengajuan) {
						jumlahPengajuan = a.getNoBerkas();
					}
					if (jumlahPengajuan != null) {
						String incVal = jumlahPengajuan.substring(6, 8);
						int countVal = Integer.valueOf(incVal) + 1;
						jumlahPengajuan = String.format("%02d", countVal);
						a2 = jumlahPengajuan;
					}
				} else {
					jumlahPengajuan = "00";
					a2 = jumlahPengajuan;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// Validasi 3 2char session kantor login 2 digit awal
		a3 = userSession.getKantor().substring(0, 2);
		// validasi 4 2char kantor perwakilan jr
		a4 = userSession.getKantor().substring(3, 5);

		// validasi 5 2char bulan
		a5 = mm.format(date);

		// validasi 6 4char tahun
		a6 = yyyy.format(date);

		System.out.println("luthfi " + a1);
		System.out.println("luthfi " + a2);
		System.out.println("luthfi " + a3);
		System.out.println("luthfi " + a4);
		System.out.println("luthfi " + a5);
		System.out.println("luthfi " + a6);

		System.out.println("luthfi " + b1);
		System.out.println("luthfi " + b2);
		System.out.println("luthfi " + b3);
		System.out.println("luthfi " + b4);
		System.out.println("luthfi " + b5);
		System.out.println("luthfi " + b6);

		if (a1.equalsIgnoreCase(b1) && a2.equalsIgnoreCase(b2)
				&& a3.equalsIgnoreCase(b3) && a4.equalsIgnoreCase(b4)
				&& a5.equalsIgnoreCase(b5) && a6.equalsIgnoreCase(b6)) {

			return true;
		} else {
			return false;
		}
	}

	@NotifyChange({ "formDataPemohon", "formDataPengajuan" })
	@Command
	public void formDataPemohonOpen() {
		setFormDataPemohon(true);
		setFormDataPengajuan(false);
	}

	@NotifyChange({ "formDataPemohon", "formDataPengajuan" })
	@Command
	public void formDataPengajuanOpen() {
		setFormDataPemohon(false);
		setFormDataPengajuan(true);
	}

	@Command("search")
	public void search() {
		if (instansiDto == null) {
			instansiDto = new PlInstansiDto();
		}
		if (instansiDto.getKodeInstansi() == null) {
			instansiDto.setKodeInstansi("");
		}
		if (fndKantorJasaraharjaDto == null) {
			fndKantorJasaraharjaDto = new FndKantorJasaraharjaDto();
		}
		if (fndKantorJasaraharjaDto.getKodeKantorJr() == null) {
			fndKantorJasaraharjaDto.setKodeKantorJr("");
		}

		// pengajuan
		if (getPengajuanDay() == null || getPengajuanDay().equalsIgnoreCase("")) {
			setPengajuanDay("%%");
		}

		if (getPengajuanYear() == null
				|| getPengajuanYear().equalsIgnoreCase("")) {
			setPengajuanYear("%%");
		}

		if (getLakaMonthStr() == null || getLakaMonthStr().equalsIgnoreCase("")) {
			setLakaMonthStr("");
		} else {
			setLakaMonthStr(getLakaMonthStr());
		}
		if (pengajuanMonthStr == null
				|| pengajuanMonthStr.equalsIgnoreCase("-")) {
			pengajuanMonth = "%%";
		} else {
			if (pengajuanMonthStr.equalsIgnoreCase("Januari")) {
				pengajuanMonth = "01";
			} else if (pengajuanMonthStr.equalsIgnoreCase("Februari")) {
				pengajuanMonth = "02";
			} else if (pengajuanMonthStr.equalsIgnoreCase("Maret")) {
				pengajuanMonth = "03";
			} else if (pengajuanMonthStr.equalsIgnoreCase("April")) {
				pengajuanMonth = "04";
			} else if (pengajuanMonthStr.equalsIgnoreCase("Mei")) {
				pengajuanMonth = "05";
			} else if (pengajuanMonthStr.equalsIgnoreCase("Juni")) {
				pengajuanMonth = "06";
			} else if (pengajuanMonthStr.equalsIgnoreCase("Juli")) {
				pengajuanMonth = "07";
			} else if (pengajuanMonthStr.equalsIgnoreCase("Agustus")) {
				pengajuanMonth = "08";
			} else if (pengajuanMonthStr.equalsIgnoreCase("September")) {
				pengajuanMonth = "09";
			} else if (pengajuanMonthStr.equalsIgnoreCase("Oktober")) {
				pengajuanMonth = "10";
			} else if (pengajuanMonthStr.equalsIgnoreCase("November")) {
				pengajuanMonth = "11";
			} else if (pengajuanMonthStr.equalsIgnoreCase("Desember")) {
				pengajuanMonth = "12";
			}
		}

		// tgl kecelakaan
		if (getLakaDay() == null || getLakaDay().equalsIgnoreCase("")) {
			setLakaDay("");
		}
		if (getLakaYear() == null || getLakaYear().equalsIgnoreCase("")) {
			setLakaYear("");
		}
		if (getLakaMonth() == null || getLakaMonth().equalsIgnoreCase("")) {
			setLakaMonth("");
		} else {
			if (lakaMonthStr.equalsIgnoreCase("Januari")) {
				lakaMonth = "01";
			} else if (lakaMonthStr.equalsIgnoreCase("Februari")) {
				lakaMonth = "02";
			} else if (lakaMonthStr.equalsIgnoreCase("Maret")) {
				lakaMonth = "03";
			} else if (lakaMonthStr.equalsIgnoreCase("April")) {
				lakaMonth = "04";
			} else if (lakaMonthStr.equalsIgnoreCase("Mei")) {
				lakaMonth = "05";
			} else if (lakaMonthStr.equalsIgnoreCase("Juni")) {
				lakaMonth = "06";
			} else if (lakaMonthStr.equalsIgnoreCase("Juli")) {
				lakaMonth = "07";
			} else if (lakaMonthStr.equalsIgnoreCase("Agustus")) {
				lakaMonth = "08";
			} else if (lakaMonthStr.equalsIgnoreCase("September")) {
				lakaMonth = "09";
			} else if (lakaMonthStr.equalsIgnoreCase("Oktober")) {
				lakaMonth = "10";
			} else if (lakaMonthStr.equalsIgnoreCase("November")) {
				lakaMonth = "11";
			} else if (lakaMonthStr.equalsIgnoreCase("Desember")) {
				lakaMonth = "12";
			}
		}

		Map<String, Object> filter = new HashMap<>();
		filter.put("diajukanDi", fndKantorJasaraharjaDto.getKodeKantorJr());
		filter.put("dilimpahkanKe", pilihJenisBerkas);
		filter.put("namaKorban", namaKorban);
		filter.put("pengajuanDay", getPengajuanDay());
		filter.put("pengajuanMonth", pengajuanMonth);
		filter.put("pengajuanYear", getPengajuanYear());
		filter.put("lakaDay", getLakaDay());
		filter.put("lakaMonth", lakaMonth);
		filter.put("lakaYear", getLakaYear());
		filter.put("kodeInstansi", instansiDto.getKodeInstansi());
		filter.put("noLaporan", noLaporan);
		filter.put("noBerkas", noBerkas);
		filter.put("statusProses", statusProsesDto.getRvLowValue());
		filter.put("penyelesaian", statusPenyelesainDto.getRvLowValue());
		filter.put("namaPemohon", namaPemohon);
		filter.put("kodeRs", plRumahSakitDto.getKodeRumahsakit());
		// filter.put("search", fndKantorJasaraharjaDto.getKodeKantorJr());

		if (searchIndex == null)
			searchIndex = "%%";

		filter.put("search", searchIndex);

		setListIndex(true);
		RestResponse rest = callWs(WS_URI + "/all", filter, HttpMethod.POST);

		try {
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlPengajuanSantunanDto.class);
			listDtoCopy = new ArrayList<>();
			listDtoCopy.addAll(listDto);
			BindUtils.postNotifyChange(null, null, this, "listDto");
			BindUtils.postNotifyChange(null, null, this, "listDtoCopy");
		} catch (Exception e) {
			e.printStackTrace();
		}

		BindUtils.postNotifyChange(null, null, this, "sortAndSearch");
		BindUtils.postNotifyChange(null, null, this, "listIndex");
		BindUtils.postNotifyChange(null, null, this, "pengajuanMonth");
		BindUtils.postNotifyChange(null, null, this, "pengajuanMonthStr");
		BindUtils.postNotifyChange(null, null, this, "lakaMonthStr");
		BindUtils.postNotifyChange(null, null, this, "lakaMonth");

	}

	public int convertMonth(String month) {
		Date date = null;
		try {
			date = (Date) new SimpleDateFormat("MMM", Locale.ENGLISH)
					.parse(month.substring(0, 2));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int bulan = cal.get(Calendar.MONTH);
		return bulan;
	}

	@NotifyChange({ "cideraKode", "cideraKode2", "modeDisableBiaya2" })
	@Command
	public void rupiahPengajuan() {
		if (sifatCideraDto.getRvLowValue() == "01"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("01")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setModeDisableBiaya2(true);

		} else if (sifatCideraDto.getRvLowValue() == "02"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("02")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setModeDisableBiaya2(true);

		} else if (sifatCideraDto.getRvLowValue() == "04"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("04")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setModeDisableBiaya2(true);

		} else if (sifatCideraDto.getRvLowValue() == "05"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("05")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setModeDisableBiaya2(false);

		} else if (sifatCideraDto.getRvLowValue() == "06"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("06")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setModeDisableBiaya2(false);

		} else if (sifatCideraDto.getRvLowValue() == "07"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("07")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setModeDisableBiaya2(true);

		} else if (sifatCideraDto.getRvLowValue() == "08"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("08")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setModeDisableBiaya2(false);

		}
	}

	@Command
	public void cekKtp() {
		try{
			dukcapilWinDto = getDataFromDukcapil(plPengajuanSantunanDto.getNoIdentitas());
			plPengajuanSantunanDto.setNamaPemohon(dukcapilWinDto.getNamaLengkap());
			plPengajuanSantunanDto.setAlamatPemohon(dukcapilWinDto.getAlamat());
			String kodeProv = "";
			String kodeKab = "";
			String kodeCamat = "";
			if(dukcapilWinDto.getKodePropinsi().length()==1){
				kodeProv = "0"+dukcapilWinDto.getKodePropinsi();
			}else{
				kodeProv = dukcapilWinDto.getKodePropinsi();
			}

			if(dukcapilWinDto.getKodeKabupaten().length()==1){
				kodeKab = "0"+dukcapilWinDto.getKodeKabupaten();
			}else{
				kodeKab = dukcapilWinDto.getKodeKabupaten();
			}
			
			if(dukcapilWinDto.getKodeKecamatan().length()==1){
				kodeCamat = "0"+dukcapilWinDto.getKodeKecamatan();
			}else{
				kodeCamat = dukcapilWinDto.getKodeKecamatan();
			}

			
			provinsiDto.setKodeProvinsi(kodeProv);
			kabKotaDto.setKodeKabkota(kodeProv+kodeKab);
			camatDto.setKodeCamat(kodeProv+kodeKab+kodeCamat);
			
			
			for(FndCamatDto prov : listProvinsiDto){
				if(provinsiDto.getKodeProvinsi().equalsIgnoreCase(prov.getKodeProvinsi())){
					provinsiDto = new FndCamatDto();
					provinsiDto = prov;
				}
			}
			getKabkota();
			
			for(FndCamatDto kab : listKabkotaDto){
				if(kabKotaDto.getKodeKabkota().equalsIgnoreCase(kab.getKodeKabkota())){
					kabKotaDto = new FndCamatDto();
					kabKotaDto = kab;
				}
			}
			getCamat();

			for(FndCamatDto camat : listCamatDto){
				if(camatDto.getKodeCamat().equalsIgnoreCase(camat.getKodeCamat())){
					camatDto = new FndCamatDto();
					camatDto = camat;
				}
			}
			
			System.err.println("luthfi99 "+dukcapilWinDto.getKodeKecamatan()+" "+dukcapilWinDto.getKodeKabupaten()+" "+dukcapilWinDto.getKodePropinsi());
			BindUtils.postNotifyChange(null, null, this, "dukcapilWinDto");
			BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
			BindUtils.postNotifyChange(null, null, this, "provinsiDto");
			BindUtils.postNotifyChange(null, null, this, "kabKotaDto");
			BindUtils.postNotifyChange(null, null, this, "camatDto");

		}catch(Exception s){
			s.printStackTrace();
		}
	}
	
	@Command
	@NotifyChange("cekKtpButton")
	public void cekJenisIdentitas(){
		try{
			identitasDto.setRvLowValue(identitasDto.getRvLowValue()==null?"":identitasDto.getRvLowValue());
		    if(identitasDto.getRvLowValue().equalsIgnoreCase("KTP")){
				setCekKtpButton(true);
			}else{
				setCekKtpButton(false);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Command
	public void openDetail(@BindingParam("popup") String popup) {
		Map<String, Object> args = new HashMap<>();
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

//	==========================================ON EDIT START===================================================

	@Command("edit")
	public void edit(@BindingParam("popup") String popup,
			@BindingParam("item") PlPengajuanSantunanDto selectedPengajuan) {
		if (selectedPengajuan == null
				|| selectedPengajuan.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Executions.getCurrent().setAttribute("obj", selectedPengajuan);
		// UserTask ut = (UserTask)
		// Executions.getCurrent().getAttribute("obj2");
		// UserTask ut = (UserTask)
		// Executions.getCurrent().getAttribute("obj2");
		// Executions.getCurrent().setAttribute("obj2", ut);
		if (userTask != null) {
			setEditBPM(false);
		}
		Executions.getCurrent().setAttribute("editBPM", editBPM);
		Executions.getCurrent().setAttribute("obj2", userTask);
		getPageInfo().setEditMode(true);
		setDisableSave(false);
		Map<String, Object> args = new HashMap<>();
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
		// navigate(DETAIL_PAGE_PATH2);
	}

	
	public void onEdit() {
		setSaveSantunanFlag(true);
		BindUtils.postNotifyChange(null, null, this, "saveSantunanFlag");
		plPengajuanSantunanDto = (PlPengajuanSantunanDto) Executions.getCurrent().getAttribute("obj");
		editBPM = (Boolean) Executions.getCurrent().getAttribute("editBPM");
		userTask = (UserTask) Executions.getCurrent().getAttribute("obj2");
		plPengajuanSantunanDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
		plPengajuanSantunanDto.setIdKorbanKecelakaan(plPengajuanSantunanDto.getIdKorbanKecelakaan());
		PlDataKecelakaanDto dataKecelakaanDto = new PlDataKecelakaanDto();
		dataKecelakaanDto.setIdKorbanKecelakaan(plPengajuanSantunanDto.getIdKorbanKecelakaan());
		listBerkas(plPengajuanSantunanDto);
		listDokumen();
		// listKantor();
		listInstansi();
		listStatusProses();
		// listPenyelesaian();
		searchProvinsi();
		listStatusHubungan();
		listJenisJaminan();
		jenisIdentitas();
		sifatCidera();
		jenisPembayaran();
		try {
			tanggalJam();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// ==========================GET DATA KORBAN=====================================
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("idKorban", plPengajuanSantunanDto.getIdKorbanKecelakaan());
		RestResponse rest2 = callWs(WS_URI_LAKA + "/korbanLakaByIdKorban",
				mapInput, HttpMethod.POST);

		try {
			listKorban = JsonUtil.mapJsonToListObject(rest2.getContents(),
					PlKorbanKecelakaanDto.class);

			for (PlKorbanKecelakaanDto a : listKorban) {
				plDataKecelakaanDto.setNamaKorban(a.getNama());
				plDataKecelakaanDto.setCidera(a.getCideraDesc());
				plDataKecelakaanDto.setJenisIdentitas(a.getJenisIdentitasDesc());
				plDataKecelakaanDto.setNoIdentitas(a.getNoIdentitas());
				plDataKecelakaanDto.setStatus(a.getStatusKorbanDesc());
				plDataKecelakaanDto.setProvinsiDesc(a.getNamaProvinsi());
				plDataKecelakaanDto.setKabKotaDesc(a.getNamaKabkota());
				plDataKecelakaanDto.setCamatDesc(a.getNamaCamat());
				plDataKecelakaanDto.setAlamat(a.getAlamat());
				plDataKecelakaanDto.setNoTelp(a.getNoTelp());
				plPengajuanSantunanDto.setNama(a.getNama());
			}
			BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
			BindUtils.postNotifyChange(null, null, this,
					"plPengajuanSantunanDto");

		} catch (Exception e) {
			e.printStackTrace();
		}

		// ============================GET DATA LAKA====================================		
		Map<String, Object> mapLaka = new HashMap<>();
		mapLaka.put("idKecelakaan", plPengajuanSantunanDto.getIdKecelakaan());
		RestResponse restLaka = callWs(WS_URI_LAKA + "/dataLakaById", mapLaka,
				HttpMethod.POST);
		try {
			listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(
					restLaka.getContents(), PlDataKecelakaanDto.class);
			for (PlDataKecelakaanDto a : listPlDataKecelakaanDtos) {
				plDataKecelakaanDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
				plDataKecelakaanDto.setTglKejadian(a.getTglKejadian());
				plDataKecelakaanDto.setKasusKecelakaanDesc(a
						.getKasusKecelakaanDesc());
				plDataKecelakaanDto.setDeskripsiKecelakaan(a
						.getDeskripsiKecelakaan());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");

		
		// ============================GET ADDITIONAL DESC====================================
		Map<String, Object> mapAdditional = new HashMap<>();
		mapAdditional.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse restAdditional = callWs(WS_URI + "/getAdditionalByNoBerkas", mapAdditional, HttpMethod.POST);
		try {
			listPlAdditionalDescDto = JsonUtil.mapJsonToListObject(restAdditional.getContents(),
					PlAdditionalDescDto.class);

			for (PlAdditionalDescDto a : listPlAdditionalDescDto) {
				plAdditionalDescDto.setTglMd(a.getTglMd());
				plAdditionalDescDto.setKodeLokasiPemohon(a.getKodeLokasiPemohon());
				plAdditionalDescDto.setTglRawatAwal(a.getTglRawatAwal());
				plAdditionalDescDto.setTglRawatAkhir(a.getTglRawatAkhir());
				System.err.println("tes11 "+plAdditionalDescDto.getTglMd());

			}
			System.err.println("luthfi10 "+JsonUtil.getJson(restAdditional.getContents()));
			BindUtils.postNotifyChange(null, null, this, "plAdditionalDescDto");
			System.err.println("tes10 "+plAdditionalDescDto.getTglMd());

		} catch (Exception e) {
			e.printStackTrace();
		}
				
		Map<String, Object> mapLokasi = new HashMap<>();
		RestResponse restLokasi = callWs(WS_URI_LOV + "/getCamatByLokasi/"+ plAdditionalDescDto.getKodeLokasiPemohon(), mapLokasi, HttpMethod.POST);
		try {
			plMappingCamatDto = JsonUtil.mapJsonToSingleObject(
					restLokasi.getContents(), PlMappingCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "plMappingCamatDto");
			
			Map<String, Object> mapAllLokasi = new HashMap<>();
			mapAllLokasi.put("kodeCamat", plMappingCamatDto.getKodeCamat());
			RestResponse restAllLokasi = callWs(WS_URI_LOV + "/getAllByCamat", mapAllLokasi, HttpMethod.POST);
			try {
				fndCamatDtos = JsonUtil.mapJsonToListObject(restAllLokasi.getContents(), FndCamatDto.class);
				for(FndCamatDto c : fndCamatDtos){
					provinsiDto.setKodeProvinsi(c.getKodeProvinsi());
					kabKotaDto.setKodeKabkota(c.getKodeKabkota());
					camatDto.setKodeCamat(c.getKodeCamat());
				}
				BindUtils.postNotifyChange(null, null, this, "fndCamatDtos");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			for (FndCamatDto provinsi : listProvinsiDto) {
				if (provinsiDto.getKodeProvinsi().equalsIgnoreCase(provinsi.getKodeProvinsi())) {
					provinsiDto = new FndCamatDto();
					provinsiDto = provinsi;
				}
			}
			getKabkota();

			for (FndCamatDto kabKota : listKabkotaDto) {
				if (kabKotaDto.getKodeKabkota().equalsIgnoreCase(kabKota.getKodeKabkota())) {
					kabKotaDto = new FndCamatDto();
					kabKotaDto = kabKota;
				}
			}
			getCamat();
			for (FndCamatDto camat : listCamatDto) {
				if (camatDto.getKodeCamat().equalsIgnoreCase(camat.getKodeCamat())) {
					camatDto = new FndCamatDto();
					camatDto = camat;
				}
			}
			
			BindUtils.postNotifyChange(null, null, this, "provinsiDto");
			BindUtils.postNotifyChange(null, null, this, "kabKotaDto");
			BindUtils.postNotifyChange(null, null, this, "camatDto");

		} catch (Exception e) {
			e.printStackTrace();
		}		

		// =======================================GET PENGAJUAN RS==============================
		Map<String, Object> mapPengajuanRs = new HashMap<>();
		mapPengajuanRs.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse rest4 = callWs(WS_URI + "/findPengajuanRsByNoBerkas", mapPengajuanRs,
				HttpMethod.POST);
		try{
			listPlPengajuanRsDto = JsonUtil.mapJsonToListObject(rest4.getContents(), PlPengajuanRsDto.class);
			for(PlPengajuanRsDto prs : listPlPengajuanRsDto){
				plPengajuanRsDto.setKodeRumahsakit(prs.getKodeRumahsakit());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "plPengajuanRsDto");
		
		//	======================================GET ONE RS====================================
		Map<String, Object> mapRs = new HashMap<>();
		mapRs.put("kodeRumahSakit", plPengajuanRsDto.getKodeRumahsakit());
		RestResponse rest5 = callWs(WS_URI_LOV + "/findOneRs", mapRs,
				HttpMethod.POST);
		try{
			listRs = JsonUtil.mapJsonToListObject(rest5.getContents(), PlRumahSakitDto.class);
			for(PlRumahSakitDto rs : listRs){
				plRumahSakitDto.setKodeRumahsakit(rs.getKodeRumahsakit());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		for(PlRumahSakitDto a : listRs){
			System.err.println("luthfi99 "+plRumahSakitDto.getKodeRumahsakit()+" "+a.getKodeRumahsakit());
			if(plRumahSakitDto.getKodeRumahsakit().equalsIgnoreCase(a.getKodeRumahsakit())){
				plRumahSakitDto = new PlRumahSakitDto();
				plRumahSakitDto = a;
			}
		}
		
		BindUtils.postNotifyChange(null, null, this, "plRumahSakitDto");

		// =======================================GET DATA SANTUNAN==============================
		Map<String, Object> mapSantunan = new HashMap<>();
		mapSantunan.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse rest3 = callWs(WS_URI + "/findByNoBerkas", mapSantunan,
				HttpMethod.POST);
		try {
			listDto = JsonUtil.mapJsonToListObject(rest3.getContents(),
					PlPengajuanSantunanDto.class);

			for (PlPengajuanSantunanDto a : listDto) {
				SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String tgl = date.format(a.getTglPenerimaan());
				String jam = time.format(a.getTglPenerimaan());
				Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(tgl);
				Date date2 = new SimpleDateFormat("HH:mm").parse(jam);
				setTglPenerimaan(date1);
				setJamPenerimaan(date2);
				plPengajuanSantunanDto.setNamaPemohon(a.getNamaPemohon());
				plPengajuanSantunanDto.setAlamatPemohon(a.getAlamatPemohon());
				plPengajuanSantunanDto.setNoIdentitas(a.getNoIdentitas());
				plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
				plPengajuanSantunanDto.setDiajukanDi(a.getDiajukanDi());
				plPengajuanSantunanDto.setJumlahPengajuanLukaluka(a.getJumlahPengajuanLukaluka());
				plPengajuanSantunanDto.setJumlahPengajuanMeninggal(a.getJumlahPengajuanMeninggal());
				plPengajuanSantunanDto.setJumlahPengajuanPenguburan(a.getJumlahPengajuanPenguburan());
				plPengajuanSantunanDto.setStatusProses(a.getStatusProses());
				plPengajuanSantunanDto.setIdGuid(a.getIdGuid());
				plPengajuanSantunanDto.setKodeJaminan(a.getKodeJaminan());
				identitasDto.setRvLowValue(a.getJenisIdentitas());
				sifatCideraDto.setRvLowValue(a.getCideraKorban());
				statusHubunganDto.setRvLowValue(a.getKodeHubunganKorban());
				jenisPembayaranDto.setRvLowValue(a.getKodePengajuan());
				plJaminanDto.setKodeJaminan(a.getKodeJaminan());
				statusProsesDto.setRvLowValue(a.getStatusProses());
				plPengajuanSantunanDto.setJmlPengajuanAmbl(a.getJmlPengajuanAmbl());
				plPengajuanSantunanDto.setJmlPengajuanP3k(a.getJmlPengajuanP3k());
				plPengajuanSantunanDto.setStatusProses(a.getStatusProses());
			}
			plPengajuanSantunanDto.setMenuPengajuan(true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		//	====================================GET NAMA KANTOR==============================
		HashMap<String, Object> mapKantor = new HashMap<>();
		mapKantor.put("kodeKantorJr", plPengajuanSantunanDto.getDiajukanDi());
		RestResponse restGetKantor = callWs(WS_URI_LOV+ "/kantorByKode", mapKantor, HttpMethod.POST);
		try{
			listKantorDto = JsonUtil.mapJsonToListObject(restGetKantor.getContents(), FndKantorJasaraharjaDto.class);
			for(FndKantorJasaraharjaDto k : listKantorDto){
				plPengajuanSantunanDto.setKodeNamaKantor(k.getNama());
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
		
		// ========================================GET DATA PENYELESAIAN====================================================
		HashMap<String, Object> map2 = new HashMap<>();
		map2.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse restPenyelesaian = callWs(WS_URI_LOV
				+ "/getPenyelesaianSantunanByNoBerkas", map2, HttpMethod.POST);
		try {
			listPlPenyelesaianSantunanDto = JsonUtil.mapJsonToListObject(
					restPenyelesaian.getContents(),
					PlPenyelesaianSantunanDto.class);
			for (PlPenyelesaianSantunanDto a : listPlPenyelesaianSantunanDto) {
				setBiaya1(a.getJumlahDibayarMeninggal());
				setBiaya2(a.getJumlahDibayarLukaluka());
				plPenyelesaianSantunanDto.setJmlByrAmbl(a.getJmlByrAmbl());
				plPenyelesaianSantunanDto.setJmlByrP3k(a.getJmlByrP3k());
			}
			BindUtils.postNotifyChange(null, null, this,
					"plPenyelesaianSantunanDto");
			BindUtils.postNotifyChange(null, null, this, "biaya1");
			BindUtils.postNotifyChange(null, null, this, "biaya2");

		} catch (Exception e) {
			e.printStackTrace();
		}
		for (PlJaminanDto jenisJaminan : listJaminanDto) {
			if (plJaminanDto.getKodeJaminan().equalsIgnoreCase(
					jenisJaminan.getKodeJaminan())) {
				plJaminanDto = new PlJaminanDto();
				plJaminanDto = jenisJaminan;
			}
		}
		for (DasiJrRefCodeDto statusProses : listStatusProses) {
			if (statusProsesDto.getRvLowValue().equalsIgnoreCase(
					statusProses.getRvLowValue())) {
				statusProsesDto = new DasiJrRefCodeDto();
				statusProsesDto = statusProses;
			}
		}
		for (DasiJrRefCodeDto sifatCidera : listSifatCidera) {
			if (sifatCideraDto.getRvLowValue().equalsIgnoreCase(
					sifatCidera.getRvLowValue())) {
				sifatCideraDto = new DasiJrRefCodeDto();
				sifatCideraDto = sifatCidera;
			}
		}
		for (DasiJrRefCodeDto statusHubungan : listStatusHubungan) {
			if (statusHubunganDto.getRvLowValue().equalsIgnoreCase(
					statusHubungan.getRvLowValue())) {
				statusHubunganDto = new DasiJrRefCodeDto();
				statusHubunganDto = statusHubungan;
			}
		}
		for (DasiJrRefCodeDto jenisPembayaran : listJenisPembayaran) {
			if (jenisPembayaranDto.getRvLowValue().equalsIgnoreCase(
					jenisPembayaran.getRvLowValue())) {
				jenisPembayaranDto = new DasiJrRefCodeDto();
				jenisPembayaranDto = jenisPembayaran;
			}
		}
		plPengajuanSantunanDto.setKodePengajuan(jenisPembayaranDto
				.getRvMeaning());
		for (DasiJrRefCodeDto jenisIdentitas : listJenisIdentitasDto) {
			if (identitasDto.getRvLowValue().equalsIgnoreCase(
					jenisIdentitas.getRvLowValue())) {
				identitasDto = new DasiJrRefCodeDto();
				identitasDto = jenisIdentitas;
			}
		}
		rupiahPengajuan();
		checkDisposisiBerkas();
		BindUtils.postNotifyChange(null, null, this, "statusHubunganDto");
		BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");
		BindUtils.postNotifyChange(null, null, this, "jenisDokumenDto");
		BindUtils.postNotifyChange(null, null, this, "identitasDto");
		BindUtils.postNotifyChange(null, null, this, "statusProsesDto");
		BindUtils.postNotifyChange(null, null, this, "plJaminanDto");
		BindUtils.postNotifyChange(null, null, this, "tglPenerimaan");
		BindUtils.postNotifyChange(null, null, this, "jamPenerimaan");

		// plPengajuanSantunanDto.setTglPenerimaan(date1);

		// Messagebox.show("MUNCUL ID"
		// +plPengajuanSantunanDto.getIdKecelakaan());

		// update BPM ke proses selanjutnya
		// try {
		// UserTaskService.updateUserTask(userSession.getLoginID(), "welcome1",
		// plPengajuanSantunanDto.getIdKecelakaan(),
		// "Entry Data Pengajuan","APPROVE",
		// "", "", "", "");
		// } catch (RemoteException e) {
		// e.printStackTrace();
		// }

	}

//	==========================================ON EDIT END===================================================

	public void tanggalJam() throws ParseException {
		Date date2 = new Date();
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat time = new SimpleDateFormat("HH:mm");
		String tgl = date.format(date2);
		String jam = time.format(date2);

		Date date3 = new SimpleDateFormat("HH:mm").parse(jam);
		Date date4 = new SimpleDateFormat("dd/MM/yyyy").parse(tgl);

		setJamPenerimaan(date3);
		setTglPenerimaan(date4);
	}

	@Command("add")
	public void add(@BindingParam("popup") String popup) {
		Map<String, Object> args = new HashMap<>();
		getPageInfo().setAddMode(true);
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	public void onAdd() {
		listJenisPertanggungan();
		listStatusLp();
	}

	// =======================================CARI DATA
	// LAKA=====================================
	@NotifyChange({ "statusLP", "listStatusLp" })
	public void listStatusLp() {
		listStatusLp.add("Y");
		listStatusLp.add("N");
	}

	@Command
	public void listJenisPertanggungan() {
		RestResponse listJenisPertanggunganRest = callWs(WS_URI_LOV
				+ "/getListJenisPertanggungan", new HashMap<String, Object>(),
				HttpMethod.POST);
		try {
			listJenisPertanggunganDto = JsonUtil.mapJsonToListObject(
					listJenisPertanggunganRest.getContents(),
					PlJaminanDto.class);
			BindUtils.postNotifyChange(null, null, this,
					"listJenisPertanggunganDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void searchInstansiCariDataLaka(
			@BindingParam("item") String searchInstansi) {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchInstansi);
		RestResponse rest = callWs(WS_URI_LOV + "/getListInstansi", map,
				HttpMethod.POST);
		try {
			listInstansiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlInstansiDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listInstansiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @NotifyChange("listDataLakaDtoCopy")
	@Command("cariDataLaka")
	public void cariDataLaka() throws ParseException {
		Map<String, Object> map = new HashMap<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		detailKejadianStartDate = detailKejadianStartDate==null?sdf.parse("01/01/2000"):detailKejadianStartDate;
		detailKejadianEndDate = detailKejadianEndDate==null?sdf.parse("01/01/9999"):detailKejadianEndDate;
		detailLaporanStartDate = detailLaporanStartDate==null?sdf.parse("01/01/2000"):detailLaporanStartDate;
		detailLaporanEndDate = detailLaporanEndDate ==null?sdf.parse("01/01/9999"):detailLaporanEndDate;

		map.put("kejadianStartDate", dateToString(detailKejadianStartDate));
		map.put("kejadianEndDate", dateToString(detailKejadianEndDate));
		map.put("laporanStartDate", dateToString(detailLaporanStartDate));
		map.put("laporanEndDate", dateToString(detailLaporanEndDate));
		map.put("instansi", instansiDto.getKodeInstansi());
		map.put("noLaporan", detailNoLaporanPolisi);
		map.put("namaKorban", detailNamaKorban);
		map.put("jenisPertanggungan", jenisPertanggunganDto.getKodeJaminan());
		map.put("statusLp", statusLP);
		map.put("menu", "pengajuan santunan");
		RestResponse rest = callWs(WS_URI_LOV + "/getListCariDataLaka", map,
				HttpMethod.POST);
		try {
			listDataLakaDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlDataKecelakaanDto.class);
			listDataLakaDtoCopy = new ArrayList<>();
			listDataLakaDtoCopy.addAll(listDataLakaDto);
			BindUtils.postNotifyChange(null, null, this, "listDataLakaDtoCopy");
			BindUtils.postNotifyChange(null, null, this, "listDataLakaDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange("listDataLakaDtoCopy")
	@Command("cariFilterDataLaka")
	public void cariFilterDataLaka(@BindingParam("item") String cari) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		if (listDataLakaDtoCopy != null || listDataLakaDtoCopy.size() > 0) {
			listDataLakaDtoCopy.clear();
		}
		if (listDataLakaDto != null && listDataLakaDto.size() > 0) {
			for (PlDataKecelakaanDto dto : listDataLakaDto) {
				System.out.println("+++");
				if (sdf.format(dto.getTglKejadian()).equals(cari)
						|| date.format(dto.getTglKejadian()).equals(cari)
						|| month.format(dto.getTglKejadian()).equals(cari)
						|| year.format(dto.getTglKejadian()).equals(cari)
						|| dto.getNoLaporanPolisi().toUpperCase()
								.contains(cari)
						|| dto.getNama().toUpperCase().contains(cari)
						|| dto.getDeskripsiJaminan().toUpperCase()
								.contains(cari)
						|| dto.getStatusLaporanPolisi().toUpperCase()
								.contains(cari)) {
					listDataLakaDtoCopy.add(dto);
				}
			}
		}
	}

	// @GlobalCommand("addDetail")
	// public void addDetail(@BindingParam("item") PlDataKecelakaanDto selected)
	// {
	// Executions.getCurrent().setAttribute("obj2", selected);
	// if (selected == null || selected.getIdKecelakaan() == null) {
	// showSmartMsgBox("W001");
	// return;
	// }
	// getPageInfo().setAddDetailMode(true);
	// navigate(DETAIL_PAGE_PATH2);
	// }

	@Command
	public void addDetail(@BindingParam("popup") String popup,
			@BindingParam("item") PlDataKecelakaanDto selected,
			@BindingParam("window") Window win) {

		if (selected == null || selected.getIdKecelakaan() == null) {
			showSmartMsgBox("W001");
			return;
		}
		Map<String, Object> map = new HashMap<>();
		Executions.getCurrent().setAttribute("obj2", selected);
		getPageInfo().setAddDetailMode(true);
		try {
			((Window) Executions.createComponents(popup, null, map)).doModal();
			closeDetail(win);
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	@Command
	public void closeDetail(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}

	@NotifyChange({ "listStatusHubungan", "jenisIdentitas", "listSifatCidera",
			"jenisPembayaran", "searchProvinsi", "getKabKota", "getCamat",
			"plDataKecelakaanDto","saveSantunanFlag" })
	public void onAddDetail() {
		plDataKecelakaanDto = (PlDataKecelakaanDto) Executions.getCurrent().getAttribute("obj2");
		setSaveSantunanFlag(false);
		cekJenisIdentitas();
		// ==============================CHECK DATA LAKA IN REGISTER=================================
		Map<String, Object> mapRegister = new HashMap<>();
		mapRegister.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
		RestResponse restRegister = callWs(WS_URI_REGISTER
				+ "/getRegisterByIdKorban", mapRegister, HttpMethod.POST);
		try {
			listRegisterSementaraDtos = JsonUtil.mapJsonToListObject(
					restRegister.getContents(), PlRegisterSementaraDto.class);

			if (listRegisterSementaraDtos != null) {
				for (PlRegisterSementaraDto a : listRegisterSementaraDtos) {
					plPengajuanSantunanDto.setDiajukanDi(a.getKodeKantorJr());
					plPengajuanSantunanDto.setNamaPemohon(a.getNamaPemohon());
					plPengajuanSantunanDto.setKodeHubunganKorban(a.getKodeHubunganKorban());
					plPengajuanSantunanDto.setJenisIdentitas(a.getJenisIdentitas());
					plPengajuanSantunanDto.setNoIdentitas(a.getNoIdentitas());
					plPengajuanSantunanDto.setIdGuid(a.getTelpPemohon());
					plPengajuanSantunanDto.setAlamatPemohon(a.getAlamatPemohon());
					plPengajuanSantunanDto.setCideraKorban(a.getCideraKorban());
					plPengajuanRsDto.setKodeRumahsakit(a.getKodeRumahSakit());
					plPengajuanSantunanDto.setJumlahPengajuanMeninggal(a.getJumlahPengajuan1());
					plPengajuanSantunanDto.setJumlahPengajuanLukaluka(a.getJumlahPengajuan2());
					plAdditionalDescDto.setTglMd(a.getTglMdKorban());
					plPengajuanSantunanDto.setJmlPengajuanAmbl(a.getJmlAmbl());
					plPengajuanSantunanDto.setJmlPengajuanP3k(a.getJmlP3k());
				}
				plPengajuanSantunanDto.setMenuPengajuan(true);
				
				BindUtils.postNotifyChange(null, null, this,"plPengajuanSantunanDto");
				BindUtils.postNotifyChange(null, null, this,"plPengajuanRsDto");
				BindUtils.postNotifyChange(null, null, this,"plAdditionalDescDto");
				
//				======================================GET ONE RS====================================
				Map<String, Object> mapRs = new HashMap<>();
				mapRs.put("kodeRumahSakit", plPengajuanRsDto.getKodeRumahsakit());
				RestResponse rest5 = callWs(WS_URI_LOV + "/findOneRs", mapRs,
						HttpMethod.POST);
				try{
					listRs = JsonUtil.mapJsonToListObject(rest5.getContents(), PlRumahSakitDto.class);
					for(PlRumahSakitDto rs : listRs){
						plRumahSakitDto.setKodeRumahsakit(rs.getKodeRumahsakit());
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				
				for(PlRumahSakitDto a : listRs){
					System.err.println("luthfi99 "+plRumahSakitDto.getKodeRumahsakit()+" "+a.getKodeRumahsakit());
					if(plRumahSakitDto.getKodeRumahsakit().equalsIgnoreCase(a.getKodeRumahsakit())){
						plRumahSakitDto = new PlRumahSakitDto();
						plRumahSakitDto = a;
					}
				}
				
				BindUtils.postNotifyChange(null, null, this, "plRumahSakitDto");
			} else {

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("ID Kecelakaan Pengajuan Santunan VMD: "+ plDataKecelakaanDto.getIdKecelakaan());
		// plPengajuanSantunanDto.setKodeNamaKantor(plDataKecelakaanDto.getNamaInstansi());
		getKantor();
		checkDisposisiBerkas();
		listJenisJaminan();
		listStatusHubungan();
		jenisIdentitas();
		sifatCidera();
		jenisPembayaran();
		listBerkas(plPengajuanSantunanDto);
		listDokumen();
		searchProvinsi();
		getKabkota();
		getCamat();
		cekJenisIdentitas();
		try {
			tanggalJam();
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// =============================GET DATA LAKA=======================================
		Map<String, Object> mapLaka = new HashMap<>();
		mapLaka.put("idKecelakaan", plDataKecelakaanDto.getIdKecelakaan());
		RestResponse restLaka = callWs(WS_URI_LAKA + "/dataLakaById", mapLaka,
				HttpMethod.POST);
		try {
			listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(restLaka.getContents(), PlDataKecelakaanDto.class);
			for (PlDataKecelakaanDto a : listPlDataKecelakaanDtos) {
				plDataKecelakaanDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
				plDataKecelakaanDto.setTglKejadian(a.getTglKejadian());
				plDataKecelakaanDto.setKasusKecelakaanDesc(a.getKasusKecelakaanDesc());
				plDataKecelakaanDto.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ==========================GET DATA KORBAN=====================================

		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("idKorban", plDataKecelakaanDto.getIdKorbanKecelakaan());
		RestResponse rest = callWs(WS_URI_LAKA + "/korbanLakaByIdKorban",mapInput, HttpMethod.POST);

		try {
			listKorban = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlKorbanKecelakaanDto.class);

			for (PlKorbanKecelakaanDto a : listKorban) {
				plDataKecelakaanDto.setNamaKorban(a.getNama());
				plDataKecelakaanDto.setCidera(a.getCideraDesc());
				plDataKecelakaanDto.setJenisIdentitas(a.getJenisIdentitasDesc());
				plDataKecelakaanDto.setNoIdentitas(a.getNoIdentitas());
				plDataKecelakaanDto.setStatus(a.getStatusKorbanDesc());
				plDataKecelakaanDto.setProvinsiDesc(a.getNamaProvinsi());
				plDataKecelakaanDto.setKabKotaDesc(a.getNamaKabkota());
				plDataKecelakaanDto.setCamatDesc(a.getNamaCamat());
				plDataKecelakaanDto.setAlamat(a.getAlamat());
				plDataKecelakaanDto.setNoTelp(a.getNoTelp());
				plDataKecelakaanDto.setKodeJaminan(a.getKodeJaminan());
				plDataKecelakaanDto.setKodeSifatCidera(a.getKodeSifatCidera());
				plPengajuanSantunanDto.setNama(a.getNama());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		plJaminanDto.setKodeJaminan(plDataKecelakaanDto.getKodeJaminan());
		for (PlJaminanDto jenisJaminan : listJaminanDto) {
			if (plJaminanDto.getKodeJaminan().equalsIgnoreCase(jenisJaminan.getKodeJaminan())) {
				plJaminanDto = new PlJaminanDto();
				plJaminanDto = jenisJaminan;
			}
		}
		if (plPengajuanSantunanDto.getCideraKorban() != null) {
			sifatCideraDto.setRvLowValue(plPengajuanSantunanDto.getCideraKorban());
			for (DasiJrRefCodeDto sifatCidera : listSifatCidera) {
				if (sifatCideraDto.getRvLowValue().equalsIgnoreCase(sifatCidera.getRvLowValue())) {
					sifatCideraDto = new DasiJrRefCodeDto();
					sifatCideraDto = sifatCidera;
				}
			}
			BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");
		} else {
			sifatCideraDto.setRvLowValue(plDataKecelakaanDto.getKodeSifatCidera());
			for (DasiJrRefCodeDto sifatCidera : listSifatCidera) {
				if (sifatCideraDto.getRvLowValue().equalsIgnoreCase(sifatCidera.getRvLowValue())) {
					sifatCideraDto = new DasiJrRefCodeDto();
					sifatCideraDto = sifatCidera;
				}
			}
		}
		if (plPengajuanSantunanDto.getKodeHubunganKorban() != null) {
			statusHubunganDto.setRvLowValue(plPengajuanSantunanDto.getKodeHubunganKorban());
			for (DasiJrRefCodeDto statusHubungan : listStatusHubungan) {
				if (statusHubunganDto.getRvLowValue().equalsIgnoreCase(statusHubungan.getRvLowValue())) {
					statusHubunganDto = new DasiJrRefCodeDto();
					statusHubunganDto = statusHubungan;
				}
			}
			BindUtils.postNotifyChange(null, null, this, "statusHubunganDto");
		}
		if (plPengajuanSantunanDto.getJenisIdentitas() != null) {
			identitasDto.setRvLowValue(plPengajuanSantunanDto
					.getJenisIdentitas());
			for (DasiJrRefCodeDto jenisIdentitas : listJenisIdentitasDto) {
				if (identitasDto.getRvLowValue().equalsIgnoreCase(
						jenisIdentitas.getRvLowValue())) {
					identitasDto = new DasiJrRefCodeDto();
					identitasDto = jenisIdentitas;
				}
			}
			BindUtils.postNotifyChange(null, null, this, "identitasDto");
		}
		rupiahPengajuan();
		
		jenisPembayaranDto.setRvLowValue("0");
		for(DasiJrRefCodeDto jenisPembayaran : listJenisPembayaran){
			if(jenisPembayaranDto.getRvLowValue().equalsIgnoreCase(jenisPembayaran.getRvLowValue())){
				jenisPembayaranDto = new DasiJrRefCodeDto();
				jenisPembayaranDto = jenisPembayaran;
			}
		}
		BindUtils.postNotifyChange(null, null, this, "jenisPembayaranDto");

		// =====================================GET JENIS
		// PENGAJUAN=============================
		// Map<String, Object> map = new HashMap<>();
		// map.put("idKorban", plDataKecelakaanDto.getIdKorbanKecelakaan());
		//
		// RestResponse rest = callWs(WS_URI+"/getPengajuanByIdKorban", map,
		// HttpMethod.POST);
		// String jumlahPengajuan = null;
		// try {
		// listPengajuan = JsonUtil.mapJsonToListObject(rest.getContents(),
		// PlPengajuanSantunanDto.class);
		// if(listPengajuan.size()!=0){
		// for (PlPengajuanSantunanDto a : listPengajuan) {
		// jumlahPengajuan = a.getNoBerkas();
		// }
		// if (jumlahPengajuan != null) {
		// String incVal = jumlahPengajuan.substring(6, 8);
		// int countVal = Integer.valueOf(incVal) + 1;
		// jumlahPengajuan = String.format("%02d", countVal);
		// a2 = jumlahPengajuan;
		// }
		// }else{
		// jumlahPengajuan = "00";
		// a2 = jumlahPengajuan;
		// }
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		
		try {
			String noBerkasGenerated = this.generateNoBerkas();
			plPengajuanSantunanDto.setNoBerkas(noBerkasGenerated);
			System.out.println("GENERATED NO BERKAS " + noBerkasGenerated);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		try{
			identitasDto.setRvLowValue(identitasDto.getRvLowValue()==null?"":identitasDto.getRvLowValue());
		    if(identitasDto.getRvLowValue().equalsIgnoreCase("KTP")){
				setCekKtpButton(true);
			}else{
				setCekKtpButton(false);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
		BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
		BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");
		BindUtils.postNotifyChange(null, null, this, "plJaminanDto");
	}

	@NotifyChange("lampirans")
	private void listBerkas(PlPengajuanSantunanDto pengajuanSantunan) {
		ui.operasionalDetail.DaftarLampiranVmd lam = new ui.operasionalDetail.DaftarLampiranVmd();
		Bpm.Ecms.queryByNoBerkas.DirectoryName dir = new Bpm.Ecms.queryByNoBerkas.DirectoryName();
		dir.setNoBerkas(pengajuanSantunan.getNoBerkas());
		this.lampirans = new ArrayList<>();
		Response res;
		try {
			res = lam.hitserviesBpmEcmsByNoBerkas(dir);
			if (res.getResponse().getStatus().getStatusCode() != -16) {
				Bpm.Ecms.queryByNoBerkas.ContentResult cr[] = res.getResponse()
						.getContent();
				for (Bpm.Ecms.queryByNoBerkas.ContentResult contenR : cr) {
					LampiranDto lampiran = new LampiranDto();
					lampiran.setDeskripsi(contenR.getXComments());
					lampiran.setNamaFile(contenR.getDOriginalName());
					Date date;
					try {
						date = new SimpleDateFormat("yyyy-MM-dd").parse(contenR
								.getDDocName().substring(0, 10));
						lampiran.setTglUpload(date);
					} catch (ParseException e) {
						e.printStackTrace();
					}

					lampirans.add(lampiran);
				}
			}

		} catch (RemoteException e) {
			e.printStackTrace();
			// Messagebox.show("server ecms disconect");
			System.err.println("Server ecms disconnect");
		}
	}

	@Command
	public void test() {
		try{
			// pl pengajuan santunan
			getLokasiByCamat();
			System.out.println("a " + plPengajuanSantunanDto.getNoBerkas());
			System.out.println("a " + statusHubunganDto.getRvLowValue());
			System.out.println("a " + plAdditionalDescDto.getTglRawatAwal());
			System.out.println("a " + plAdditionalDescDto.getTglRawatAkhir());
			System.out.println("a " + identitasDto.getRvLowValue());
			System.out.println("a " + plPengajuanSantunanDto.getNoIdentitas());
			System.out.println("a " + plPengajuanSantunanDto.getNamaPemohon());
			System.out.println("a " + plMappingCamatDto.getKodeLokasi());
			System.out.println("a " + plPengajuanSantunanDto.getAlamatPemohon());
			System.out.println("a " + plPengajuanSantunanDto.getIdGuid());
			System.out.println("a " + plDataKecelakaanDto.getJaminan());
			System.out.println("a " + plPengajuanSantunanDto.getJaminanGandaFlag());
			System.out.println("a " + sifatCideraDto.getRvLowValue());
			System.out.println("a " + plAdditionalDescDto.getTglMd());
			System.out.println("a " + plRumahSakitDto.getKodeRumahsakit());
			System.out.println("a " + jenisPembayaranDto.getRvLowValue());
			System.out.println("a "	+ plPengajuanSantunanDto.getJumlahPengajuanLukaluka());
			System.out.println("a "
					+ plPengajuanSantunanDto.getJumlahPengajuanMeninggal());
			System.out.println("a "
					+ plPengajuanSantunanDto.getJumlahPengajuanPenguburan());
			System.out.println("a " + plPengajuanSantunanDto.getJmlPengajuanP3k());
			System.out.println("a " + plPengajuanSantunanDto.getJmlPengajuanAmbl());
			System.out.println("a " + plDataKecelakaanDto.getIdKecelakaan());
			System.out.println("a " + plDataKecelakaanDto.getIdKorbanKecelakaan());
			System.out.println("a " + plJaminanDto.getKodeJaminan());
			System.out.println("a " + plMappingCamatDto.getKodeLokasi());
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void getLokasiByCamat() {
		Map<String, Object> map = new HashMap<>();
		RestResponse rest = callWs(WS_URI_LOV + "/getLokasiByCamat/"
				+ getCamatDto().getKodeCamat(), map, HttpMethod.POST);
		try {
			plMappingCamatDto = JsonUtil.mapJsonToSingleObject(
					rest.getContents(), PlMappingCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "plMappingCamatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command("saveSantunan")
	@NotifyChange("saveSantunanFlag")
	public void save() throws ParseException, RemoteException {
		RestResponse restResponse = null;
		getLokasiByCamat();
		if (plPengajuanSantunanDto.getNoBerkas() == null) {
			showInfoMsgBox("No berkas wajib diisi", "");					
		} else {
			if (getPageInfo().isAddDetailMode()) {
				test();
				Date a = getTglPenerimaan();
				Date b = getJamPenerimaan();
				SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String tgl = date.format(a);
				String jam = time.format(b);
				String tglPenerimaan = tgl + " " + jam;
				Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(tglPenerimaan);
				plPengajuanSantunanDto.setTglPenerimaan(date1);
				plPengajuanSantunanDto.setDiajukanDi(userSession.getKantor());
				plPengajuanSantunanDto.setKodeHubunganKorban(statusHubunganDto.getRvLowValue());
				plPengajuanSantunanDto.setIdKecelakaan(plDataKecelakaanDto.getIdKecelakaan());
				plPengajuanSantunanDto.setIdKorbanKecelakaan(plDataKecelakaanDto.getIdKorbanKecelakaan());
				plPengajuanSantunanDto.setNoIdentitas(plPengajuanSantunanDto.getNoIdentitas());
				plPengajuanSantunanDto.setJenisIdentitas(identitasDto.getRvLowValue());
				plPengajuanSantunanDto.setCideraKorban(getSifatCideraDto().getRvLowValue());
				plPengajuanSantunanDto.setKodeJaminan(plJaminanDto.getKodeJaminan());
				plPengajuanSantunanDto.setNamaPemohon(plPengajuanSantunanDto.getNamaPemohon());
				plPengajuanSantunanDto.setKodePengajuan(jenisPembayaranDto.getRvLowValue());
				plPengajuanSantunanDto.setFlagHasilSurvey("N");
				plPengajuanSantunanDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
				plPengajuanSantunanDto.setStatusProses("DT");
				plPengajuanSantunanDto.setTransisiFlag("N");
				plPengajuanSantunanDto.setCreatedBy(userSession.getLoginID());
				plPengajuanSantunanDto.setCreationDate(new Date());
				plPengajuanSantunanDto.setNamaPemohon(plPengajuanSantunanDto.getNamaPemohon());
				plPengajuanSantunanDto.setAlamatPemohon(plPengajuanSantunanDto.getAlamatPemohon());
				plPengajuanSantunanDto.setJumlahPengajuanLukaluka(plPengajuanSantunanDto.getJumlahPengajuanLukaluka());
				plPengajuanSantunanDto.setJumlahPengajuanMeninggal(plPengajuanSantunanDto.getJumlahPengajuanMeninggal());
				plPengajuanSantunanDto.setJumlahPengajuanPenguburan(plPengajuanSantunanDto.getJumlahPengajuanPenguburan());
				plPengajuanSantunanDto.setJmlPengajuanAmbl(plPengajuanSantunanDto.getJmlPengajuanAmbl());
				plPengajuanSantunanDto.setJmlPengajuanP3k(plPengajuanSantunanDto.getJmlPengajuanP3k());
				plPengajuanSantunanDto.setTipePengajuan(jenisPembayaranDto.getRvLowValue());
				plPengajuanSantunanDto.setCideraKorban(sifatCideraDto.getRvLowValue());
				plPengajuanSantunanDto.setIdGuid(plPengajuanSantunanDto.getIdGuid());
				plPengajuanSantunanDto.setTglPengajuan(plPengajuanSantunanDto.getTglPengajuan());
				plPengajuanSantunanDto.setSeqNo(new BigDecimal(plPengajuanSantunanDto.getNoBerkas().substring(7,8)));
				plPengajuanSantunanDto.setNoPengajuan(plPengajuanSantunanDto.getNoBerkas().substring(2,5)+"-"+plPengajuanSantunanDto.getNoBerkas().substring(9,22));
				plPengajuanSantunanDto.setLengkapFlag("N");
				plPengajuanSantunanDto.setAbsahFlag("N");
				if (isJaminanGandaFlag()) {
					plPengajuanSantunanDto.setJaminanGandaFlag("Y");
				} else if (isJaminanGandaFlag()==false) {
					plPengajuanSantunanDto.setJaminanGandaFlag("N");					
				}
				System.err.println("luthf88 "+plMappingCamatDto.getKodeLokasi());
				plPengajuanSantunanDto.setKodeLokasiPemohonAdd(plMappingCamatDto.getKodeLokasi());

				// save additional
				if (plAdditionalDescDto.getTglMd() != null) {
					plPengajuanSantunanDto.setTglMdAdd(plAdditionalDescDto.getTglMd());
					plPengajuanSantunanDto.setTglRawatAwalAdd(plAdditionalDescDto.getTglRawatAwal());
					plPengajuanSantunanDto.setTglRawatAkhirAdd(plAdditionalDescDto.getTglRawatAkhir());
				}

				// save pengajuanRs
				if (plRumahSakitDto.getKodeRumahsakit() != null) {
					String SALTCHARS = "1234567890";
					StringBuilder salt = new StringBuilder();
					Random rnd = new Random();
					while (salt.length() < 18) { // length of the random string.
						int index = (int) (rnd.nextFloat() * SALTCHARS.length());
						salt.append(SALTCHARS.charAt(index));
					}
					String saltStr = salt.toString();
					String idPengajuanRs = userSession.getLoginID() + "-"+ saltStr;
					plPengajuanSantunanDto.setIdPengajuanRs(idPengajuanRs);
					plPengajuanSantunanDto.setKodeRumahsakitPengajuanRs(plRumahSakitDto.getKodeRumahsakit());
				}

				if (noBerkasValidasi()) {
					restResponse = callWs(WS_URI + "/saveSantunan",	plPengajuanSantunanDto, HttpMethod.POST);
					
					if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
						setSaveSantunanFlag(true);
						showInfoMsgBox(restResponse.getMessage());
						checkDisposisiBerkas();
						// update BPM ke proses selanjutnya
						try {
							String pattern = "dd-MM-yyyy HH:mm:ss";
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
									pattern);

							String dateFormat = simpleDateFormat.format(new Date());
							System.out.println(dateFormat);
							System.out.println("Kantor : "
									+ userSession.getKantor());
							System.out.println("Login ID : "
									+ userSession.getLoginID());
							String kantor = userSession.getKantor();
							String login = userSession.getUserName();
							String levelKantor = new String();

							if (userSession.getLevelKantor().equalsIgnoreCase("CA")) {
								levelKantor = "AB";
							} else if (userSession.getLevelKantor()
									.equalsIgnoreCase("CB")) {
								levelKantor = "AB";
							} else if (userSession.getLevelKantor()
									.equalsIgnoreCase("CC")) {
								levelKantor = "C";
							} else {
								levelKantor = "perwakilan";
							}
							UserTask ut = (UserTask) Executions.getCurrent()
									.getAttribute("obj2");
							if (ut == null) {
								startBPM(levelKantor,
										plPengajuanSantunanDto.getNoBerkas(),
										kantor, login, dateFormat, login,
										dateFormat, "N", "", kantor, "", "",
										plDataKecelakaanDto.getIdKecelakaan(),
										levelKantor, "");
								TimeUnit.SECONDS.sleep(3);
								UserTaskService.updateUserTask(
										Labels.getLabel("userBPM"),
										Labels.getLabel("passBPM"),
										plPengajuanSantunanDto.getNoBerkas(),
										"Entry Data Pengajuan", "APPROVE", "", "",
										userSession.getUserName(), dateFormat,
										plPengajuanSantunanDto.getIdKecelakaan(),
										userSession.getUserLdap());
							} else {
								// ut.setKodeCabang(kantor);
								String instanceId = ut.getOtherInfo2().substring(5);
								UserTask uTask = UserTaskService
										.getTaskByInstanceId(
												Labels.getLabel("userBPM"),
												Labels.getLabel("passBPM"),
												"Entry Data Pengajuan", instanceId);
								uTask.setNomorPermohonan(plPengajuanSantunanDto
										.getNoBerkas());
								UserTaskService.updateTask(
										Labels.getLabel("userBPM"),
										Labels.getLabel("passBPM"),
										userSession.getUserLdap(), "APPROVE",
										uTask, "");
								// UserTaskService.updateUserTask("adminwls",
								// "welcome1", plPengajuanSantunanDto.getNoBerkas(),
								// "Entry Data Pengajuan","APPROVE", "", "", "",
								// "",plPengajuanSantunanDto.getIdKecelakaan(),
								// userSession.getUserLdap());
							}
						} catch (RemoteException e) {
							e.printStackTrace();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}

					} else {
						showErrorMsgBox(restResponse.getMessage());
					}
				} else {
					showInfoMsgBox("Validasi no berkas gagal", "");
				}

			} else {
				System.out.println("EDIIIIIIIIITTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT SANTUNANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN");

				Date a = getTglPenerimaan();
				Date b = getJamPenerimaan();
				SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String tgl = date.format(a);
				String jam = time.format(b);
				String tglPenerimaan = tgl + " " + jam;
				Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm")
						.parse(tglPenerimaan);
				if (userTask != null) {
					plPengajuanSantunanDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
				}
				plPengajuanSantunanDto.setTglPenerimaan(date1);
				plPengajuanSantunanDto.setDiajukanDi(userSession.getKantor());
				plPengajuanSantunanDto.setKodeHubunganKorban(statusHubunganDto.getRvLowValue());
				plPengajuanSantunanDto.setNoIdentitas(plPengajuanSantunanDto.getNoIdentitas());
				plPengajuanSantunanDto.setJenisIdentitas(identitasDto.getRvLowValue());
				plPengajuanSantunanDto.setCideraKorban(getSifatCideraDto().getRvLowValue());
				plPengajuanSantunanDto.setKodeJaminan(plJaminanDto.getKodeJaminan());
				System.out.println("KODEEEEEEEEEEE JAMINNNNNNNAAAAAAAAAAAAAANNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN : "+ plJaminanDto.getKodeJaminan());
				plPengajuanSantunanDto.setNamaPemohon(plPengajuanSantunanDto.getNamaPemohon());
				plPengajuanSantunanDto.setKodePengajuan(jenisPembayaranDto.getRvLowValue());
				plPengajuanSantunanDto.setFlagHasilSurvey("N");
				plPengajuanSantunanDto.setStatusProses("BS");
				plPengajuanSantunanDto.setTransisiFlag("N");
				plPengajuanSantunanDto.setCreatedBy(userSession.getLoginID());
				plPengajuanSantunanDto.setCreationDate(new Date());
				// plPengajuanSantunanDto.setKodeJaminan(plDataKecelakaanDto
				// .getJaminan());
				plPengajuanSantunanDto.setNamaPemohon(plPengajuanSantunanDto.getNamaPemohon());
				plPengajuanSantunanDto.setAlamatPemohon(plPengajuanSantunanDto.getAlamatPemohon());
				plPengajuanSantunanDto.setJumlahPengajuanLukaluka(plPengajuanSantunanDto.getJumlahPengajuanLukaluka());
				plPengajuanSantunanDto.setJumlahPengajuanMeninggal(plPengajuanSantunanDto.getJumlahPengajuanMeninggal());
				plPengajuanSantunanDto.setJumlahPengajuanPenguburan(plPengajuanSantunanDto.getJumlahPengajuanPenguburan());

				plPengajuanSantunanDto.setSeqNo(new BigDecimal(plPengajuanSantunanDto.getNoBerkas().substring(7,8)));
				plPengajuanSantunanDto.setNoPengajuan(plPengajuanSantunanDto.getNoBerkas().substring(2,5)+"-"+plPengajuanSantunanDto.getNoBerkas().substring(9,22));
				if (isJaminanGandaFlag()) {
					plPengajuanSantunanDto.setJaminanGandaFlag("Y");
				} else if (isJaminanGandaFlag()==false) {
					plPengajuanSantunanDto.setJaminanGandaFlag("N");					
				}
				plPengajuanSantunanDto.setTipePengajuan(jenisPembayaranDto.getRvLowValue());
				plPengajuanSantunanDto.setCideraKorban(sifatCideraDto.getRvLowValue());
				plPengajuanSantunanDto.setIdGuid(plPengajuanSantunanDto.getIdGuid());

				saveAdditional();
				savePengajuanRs();
				restResponse = callWs(WS_URI + "/saveSantunan",	plPengajuanSantunanDto, HttpMethod.POST);

				if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
					showInfoMsgBox(restResponse.getMessage());
					back();
					// try {
					String pattern = "dd-MM-yyyy HH:mm:ss";
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
							pattern);
					String dateFormat = simpleDateFormat.format(new Date());
					UserTask ut = (UserTask) Executions.getCurrent()
							.getAttribute("obj2");
					if (userTask != null) {
						System.out
								.println("TASKKKKKkkkkkkkkkkkkkkkkkkkkkkk NUMBERNNYAAAAAAAAAAA : "
										+ userTask.getTaskNumber());
						// if(ut.getNomorPermohonan().equalsIgnoreCase(plPengajuanSantunanDto.getNoBerkas())){
						// UserTaskService.updateUserTask(Labels.getLabel("userBPM"),
						// Labels.getLabel("passBPM"),
						// plPengajuanSantunanDto.getNoBerkas(),
						// "Entry Data Pengajuan","APPROVE", "", "",
						// userSession.getUserName(),
						// dateFormat,plPengajuanSantunanDto.getIdKecelakaan(),
						// userSession.getUserLdap());
						if (userTask.getOtherInfo5().equalsIgnoreCase(
								"PELIMPAHAN")) {
							System.out
									.println("PELIIIIIIIIIIIIMMMMMMMMMMMMMMMMMMMMPPPPPPPPPPPPPPPPPPPPPPPAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHHHHHHHHAAAAAAAAAAAAAAAAAAAAAANNNNNN");
							String instanceId = UserTaskService
									.getInstanceIdCreatePelimpahan(
											userTask.getNomorPermohonan(),
											userTask.getCreationDate(),
											userTask.getOtherInfo2())
									.substring(5);
							String otherInfo2 = UserTaskService
									.getInstanceIdCreatePelimpahan(
											userTask.getNomorPermohonan(),
											userTask.getCreationDate(),
											userTask.getOtherInfo2());
							UserTask uTask = UserTaskService
									.getTaskByInstanceId(
											Labels.getLabel("userBPM"),
											Labels.getLabel("passBPM"),
											"Entry Data Pengajuan", instanceId);
							uTask.setNomorPermohonan(plPengajuanSantunanDto
									.getNoBerkas());
							uTask.setKodeCabang(getCurrentUserSessionJR()
									.getKantor());
							uTask.setOtherInfo1("Entry Data Pengajuan");
							uTask.setCreationDate(userTask.getLastUpdateDate());
							uTask.setCreatedBy(getCurrentUserSessionJR()
									.getUserName());
							uTask.setLastUpdatedBy(getCurrentUserSessionJR()
									.getUserName());
							uTask.setLastUpdateDate(dateFormat);
							uTask.setIdGUID(userTask.getIdGUID());
							uTask.setPengajuanType(userTask.getPengajuanType());
							uTask.setKodeKantor(userTask.getKodeKantor());
							uTask.setOtherInfo2(otherInfo2);
							uTask.setOtherInfo3(userTask.getOtherInfo3());
							uTask.setOtherInfo4(userTask.getOtherInfo4());
							uTask.setOtherInfo5("");
							uTask.setTaskNumber(uTask.getTaskNumber());
							UserTaskService.updateTask(
									Labels.getLabel("userBPM"),
									Labels.getLabel("passBPM"),
									userSession.getUserLdap(), "APPROVE",
									uTask, "");
						} else {
							String instanceId = userTask.getOtherInfo2()
									.substring(5);
							UserTask uTask = UserTaskService
									.getTaskByInstanceId(
											Labels.getLabel("userBPM"),
											Labels.getLabel("passBPM"),
											"Entry Data Pengajuan", instanceId);
							uTask.setNomorPermohonan(plPengajuanSantunanDto
									.getNoBerkas());
							uTask.setKodeCabang(userTask.getKodeCabang());
							uTask.setOtherInfo1("Entry Data Pengajuan");
							uTask.setCreationDate(userTask.getLastUpdateDate());
							uTask.setCreatedBy(getCurrentUserSessionJR()
									.getUserName());
							uTask.setLastUpdatedBy(getCurrentUserSessionJR()
									.getUserName());
							uTask.setLastUpdateDate(dateFormat);
							uTask.setIdGUID(userTask.getIdGUID());
							uTask.setPengajuanType(userTask.getPengajuanType());
							uTask.setKodeKantor(userTask.getKodeKantor());
							uTask.setOtherInfo2(userTask.getOtherInfo2());
							uTask.setOtherInfo3(userTask.getOtherInfo3());
							uTask.setOtherInfo4(userTask.getOtherInfo4());
							uTask.setOtherInfo5(userTask.getOtherInfo5());
							uTask.setTaskNumber(userTask.getTaskNumber());
							UserTaskService.updateTask(
									Labels.getLabel("userBPM"),
									Labels.getLabel("passBPM"),
									userSession.getUserLdap(), "APPROVE",
									uTask, "");
						}
						// }
					} else {
						String instanceId = "";
						try {
							// instanceId =
							// UserTaskService.getInstanceId("Entry Data Pengajuan",
							// plPengajuanSantunanDto.getNoBerkas());
							//
							// UserTask uTask =
							// UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"),
							// Labels.getLabel("passBPM"),
							// "Entry Data Pengajuan", instanceId);
							// uTask.setNomorPermohonan(plPengajuanSantunanDto.getNoBerkas());
							// UserTaskService.updateTask(Labels.getLabel("userBPM"),
							// Labels.getLabel("passBPM"),userSession.getUserLdap(),
							// "", uTask, "");

							UserTaskService.updateUserTask(
									Labels.getLabel("userBPM"),
									Labels.getLabel("passBPM"),
									plPengajuanSantunanDto.getNoBerkas(),
									"Entry Data Pengajuan", "", "edit", "",
									userSession.getUserName(), dateFormat,
									plPengajuanSantunanDto.getIdKecelakaan(),
									userSession.getUserLdap());
						} catch (RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						// UserTaskService.updateUserTask(Labels.getLabel("userBPM"),
						// Labels.getLabel("passBPM"),
						// plPengajuanSantunanDto.getNoBerkas(),
						// "Entry Data Pengajuan","", "edit", "",
						// userSession.getUserName(),
						// dateFormat,plPengajuanSantunanDto.getIdKecelakaan(),
						// userSession.getUserLdap());
					}

					// } catch (RemoteException e) {
					// e.printStackTrace();
					// }

				} else {
					showErrorMsgBox(restResponse.getMessage());
				}
			}
		}
	}

	public void startBPM(String tipeCabang, String nomorPermohonan,
			String kodeCabang, String createdBy, String creationDate,
			String lastUpdatedBy, String lastUpdatedDate, String idGUID,
			String pengajuanType, String kodeKantor, String otherInfo1,
			String otherInfo2, String otherInfo3, String otherInfo4,
			String otherInfo5) {
		System.out.println("Level Kantor : " + userSession.getLevelKantor());

		Bpm_start_processRequest req = new Bpm_start_processRequest();
		req.setTipeCabang(tipeCabang);
		req.setNomorPermohonan(nomorPermohonan);
		req.setKodeCabang(kodeCabang);
		req.setCreatedBy(createdBy);
		req.setCreationDate(creationDate);
		req.setLastUpdatedBy(lastUpdatedBy);
		req.setLastUpdatedDate(lastUpdatedDate);
		req.setIdGUID(idGUID);
		req.setPengajuanType(pengajuanType);
		req.setKodeKantor(kodeKantor);
		req.setOtherInfo1(otherInfo1);
		req.setOtherInfo2(otherInfo2);
		req.setOtherInfo3(otherInfo3);
		req.setOtherInfo4(otherInfo4);
		req.setOtherInfo5(otherInfo5);

		// StartProcessSvcImpl startProcess = new StartProcessSvcImpl();
		try {

			StartBPMSvcImpl.bpmStartProcess(req);
			// System.out.println("Response Service BPM : " +
			// bpmStartProcess(req).getResponse().getResponseCode());
			// showInfoMsgBox("Response " +
			// bpmStartProcess(req).getResponse().getResponseCode());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/*
	 * public void updateTask() { System.out.println("Level Kantor : " +
	 * userSession.getLevelKantor()); String pattern = "dd-MM-yyyy";
	 * SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
	 * 
	 * String dateFormat = simpleDateFormat.format(new Date());
	 * System.out.println(dateFormat); System.out.println("Kantor : " +
	 * userSession.getKantor()); System.out.println("Login ID : " +
	 * userSession.getLoginID()); String kantor = userSession.getKantor();
	 * String login = userSession.getLoginID();
	 * 
	 * UserTask userTask = new UserTask(); try { //userTask =
	 * UserTaskService.getUserTaskByIdKecelakaan(login, // "welcome1",
	 * plDataKecelakaanDto.getIdKecelakaan());
	 * 
	 * } catch (Exception e) { e.printStackTrace(); Messagebox
	 * .show("Data belum proses BPM atau data kecelakaan tidak ditemukan"); }
	 * 
	 * Bpm_update_task_payloadRequest req = new
	 * Bpm_update_task_payloadRequest(); CredentialType credential = new
	 * CredentialType(); credential.setLogin(login);
	 * credential.setPassword("welcome1"); credential.setIdentityContext("");
	 * credential.setOnBehalfOfUser("");
	 * 
	 * PayloadType payload = new PayloadType(); InstanceInfo instance = new
	 * InstanceInfo();
	 * 
	 * instance.setNomorPermohonan(plDataKecelakaanDto.getIdKecelakaan());
	 * instance.setKodeCabang(kantor);
	 * instance.setCreatedBy(userTask.getCreatedBy());
	 * instance.setCreationDate(userTask.getCreationDate());
	 * instance.setLastUpdatedBy(login);
	 * instance.setLastUpdatedDate(dateFormat); instance.setIdGUID("N");
	 * instance.setPengajuanType(""); instance.setKodeKantor(kantor);
	 * instance.setOtherInfo1(plPengajuanSantunanDto.getNoBerkas());
	 * instance.setOtherInfo2(""); instance.setOtherInfo3("");
	 * instance.setOtherInfo4(""); instance.setOtherInfo5("");
	 * 
	 * payload.setInstanceInfo(instance); try { req.setCredential(credential);
	 * req.setPayload(payload); req.setOutcome("APPROVE");
	 * req.setTaskNumber(userTask.getTaskNumber());
	 * 
	 * UpdateAndInsertTaskServiceImpl.bpmUpdateInsertTask(req);
	 * 
	 * } catch (Exception e) { e.printStackTrace();
	 * 
	 * } }
	 */

	public void saveAdditional() {
		RestResponse restAdditional;
		plAdditionalDescDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
		plAdditionalDescDto.setTglMd(plAdditionalDescDto.getTglMd());
		plAdditionalDescDto.setCreatedBy(userSession.getLoginID());
		plAdditionalDescDto.setCreatedDate(new Date());
		// plAdditionalDescDto.setIdRekRs(userSession.getLoginID());
		// plAdditionalDescDto.setJnsRekening(plAdditionalDescDto.getJnsRekening());
		plAdditionalDescDto.setTglRawatAwal(plAdditionalDescDto
				.getTglRawatAwal());
		plAdditionalDescDto.setTglRawatAkhir(plAdditionalDescDto
				.getTglRawatAkhir());
		// plAdditionalDescDto.setLastUpdatedBy("Meilona");
		// plAdditionalDescDto.setLastUpdatedDate(new Date());
		restAdditional = callWs(WS_URI + "/saveAdditional",
				plAdditionalDescDto, HttpMethod.POST);

		if (restAdditional.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restAdditional.getMessage());
			back();

		} else {
			showErrorMsgBox(restAdditional.getMessage());
		}

	}

	public void savePengajuanRs() {
		RestResponse restPengajuanRs;

		String SALTCHARS = "1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 18) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();

		String idPengajuanRs = userSession.getLoginID() + "-" + saltStr;
		plPengajuanRsDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
		plPengajuanRsDto.setIdPengajuanRs(idPengajuanRs);
		// Messagebox.show("BERAPA ?" + idPengajuanRs);
		plPengajuanRsDto.setKodeRumahsakit(plRumahSakitDto.getKodeRumahsakit());
		// plPengajuanRsDto.setIdGuid(plPengajuanRsDto.getIdGuid());
		// plPengajuanRsDto.setJumlahPengajuanLukaluka(plPengajuanRsDto.getJumlahPengajuanLukaluka());

		restPengajuanRs = callWs(WS_URI + "/savePengRs", plPengajuanRsDto,
				HttpMethod.POST);

		if (restPengajuanRs.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restPengajuanRs.getMessage());
			back();
		} else {
			showErrorMsgBox(restPengajuanRs.getMessage());
		}
	}

	@Command("back")
	public void keluar() {
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}

	@Command("tutup")
	public void tutup(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
		// getPageInfo().setListMode(true);
		// navigate(INDEX_PAGE_PATH);
	}

	// added by luthfi
	public void checkDisposisiBerkas() {

		if (plPengajuanSantunanDto.getStatusProses() == null) {
			setDisableDisposisi(true);
			setDisableProsesSelanjutnya(true);
			setDisableSave(false);
		} else if (plPengajuanSantunanDto.getStatusProses().equalsIgnoreCase(
				"BS")
				&& getPageInfo().isEditDetailMode()) {
			setDisableDisposisi(true);
			setDisableProsesSelanjutnya(true);
			setDisableSave(false);
		} else if (plPengajuanSantunanDto.getStatusProses().equalsIgnoreCase(
				"BS")) {
			setDisableDisposisi(true);
			setDisableProsesSelanjutnya(true);
			setDisableSave(true);
		} else {
			setDisableDisposisi(false);
			setDisableProsesSelanjutnya(false);
			setDisableSave(false);
		}
		BindUtils.postNotifyChange(null, null, this, "disableDisposisi");
		BindUtils
				.postNotifyChange(null, null, this, "disableProsesSelanjutnya");
		BindUtils.postNotifyChange(null, null, this, "disableSave");
	}

	@Command
	public void checkHubPemohon() {
		if (statusHubunganDto == null
				|| statusHubunganDto.getRvLowValue() == null) {
			plPengajuanSantunanDto = new PlPengajuanSantunanDto();
			identitasDto = new DasiJrRefCodeDto();
			camatDto = new FndCamatDto();
			kabKotaDto = new FndCamatDto();
			provinsiDto = new FndCamatDto();
			getKantor();
		} else {
			if (statusHubunganDto.getRvLowValue().equalsIgnoreCase("01")
					&& sifatCideraDto.getRvLowValue().equalsIgnoreCase("01")||statusHubunganDto.getRvLowValue().equalsIgnoreCase("01")
					&& sifatCideraDto.getRvLowValue().equalsIgnoreCase("05")) {
				showInfoMsgBox(
						"Korban Meninggal Tidak Dapat Mengajukan Diri Sendiri",
						"");
				listStatusHubungan();
			} else if (statusHubunganDto.getRvLowValue().equalsIgnoreCase("01")) {
				plPengajuanSantunanDto.setNoIdentitas(plDataKecelakaanDto.getNoIdentitas());
				plPengajuanSantunanDto.setNamaPemohon(plDataKecelakaanDto.getNamaKorban());
				plPengajuanSantunanDto.setAlamatPemohon(plDataKecelakaanDto.getAlamat());
				plPengajuanSantunanDto.setIdGuid(plDataKecelakaanDto.getNoTelp());
				provinsiDto.setNamaProvinsi(plDataKecelakaanDto.getProvinsiDesc());
				kabKotaDto.setNamaKabkota(plDataKecelakaanDto.getKabKotaDesc());
				camatDto.setNamaCamat(plDataKecelakaanDto.getCamatDesc());
				identitasDto.setRvLowValue(plDataKecelakaanDto.getJenisIdentitas());
				cekJenisIdentitas();
				if (identitasDto.getRvLowValue() != null) {
					for (DasiJrRefCodeDto jenisIdentitas : listJenisIdentitasDto) {
						if (identitasDto.getRvLowValue().equalsIgnoreCase(
								jenisIdentitas.getRvLowValue())) {
							identitasDto = new DasiJrRefCodeDto();
							identitasDto = jenisIdentitas;
						}
					}
				}
				if (provinsiDto.getNamaProvinsi() != null) {
					for (FndCamatDto provinsi : listProvinsiDto) {
						if (provinsiDto.getNamaProvinsi().equalsIgnoreCase(
								provinsi.getNamaProvinsi())) {
							provinsiDto = new FndCamatDto();
							provinsiDto = provinsi;
						}
					}
					getKabkota();
					for (FndCamatDto kabKota : listKabkotaDto) {
						if (kabKotaDto.getNamaKabkota().equalsIgnoreCase(
								kabKota.getNamaKabkota())) {
							kabKotaDto = new FndCamatDto();
							kabKotaDto = kabKota;
						}
					}
					getCamat();
					for (FndCamatDto camat : listCamatDto) {
						if (camatDto.getNamaCamat().equalsIgnoreCase(
								camat.getNamaCamat())) {
							camatDto = new FndCamatDto();
							camatDto = camat;
						}
					}
				}
			}
		}
		BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
		BindUtils.postNotifyChange(null, null, this, "identitasDto");
		BindUtils.postNotifyChange(null, null, this, "cekKtpButton");
		BindUtils.postNotifyChange(null, null, this, "provinsiDto");
		BindUtils.postNotifyChange(null, null, this, "kabKotaDto");
		BindUtils.postNotifyChange(null, null, this, "camatDto");
	}

	@Command("pengajuanBerikutnya")
	public void editDetail(@BindingParam("popup") String popup,
			@BindingParam("item") PlPengajuanSantunanDto selectedPengajuan) {
		if (selectedPengajuan == null
				|| selectedPengajuan.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}
		if (userTask != null) {
			setEditBPM(false);
		}
		Executions.getCurrent().setAttribute("editBPM", editBPM);
		Executions.getCurrent().setAttribute("obj2", userTask);
		Executions.getCurrent().setAttribute("obj", selectedPengajuan);
		getPageInfo().setEditDetailMode(true);
		onEditDetail2();
		Map<String, Object> args = new HashMap<>();
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	public void onEditDetail2() {
		plPengajuanSantunanDto = (PlPengajuanSantunanDto) Executions
				.getCurrent().getAttribute("obj");

		if (plPengajuanSantunanDto.getStatusProses().equalsIgnoreCase("BS")) {
			Map<String, Object> map = new HashMap<>();
			map.put("idKecelakaan", plPengajuanSantunanDto.getIdKecelakaan());
			RestResponse rest = callWs(WS_URI + "/findbyId", map,
					HttpMethod.POST);
			onEdit();
			BindUtils.postNotifyChange(null, null, this,
					"plPengajuanSantunanDto");
		} else {
			showInfoMsgBox(
					"Pengajuan Berikutnya hanya dapat diajukan apabila Berkas sebelumnya telah Selesai !",
					"");
			plPengajuanSantunanDto.setNoBerkas("");
			BindUtils.postNotifyChange(null, null, this,
					"plPengajuanSantunanDto");
			checkDisposisiBerkas();
		}
	}

	public void sifatCidera() {
		RestResponse restCidera = callWs(WS_URI_LOV + "/getListSifatCidera",
				new HashMap<String, Object>(), HttpMethod.POST);

		try {
			listSifatCidera = JsonUtil.mapJsonToListObject(
					restCidera.getContents(), DasiJrRefCodeDto.class);
			for (DasiJrRefCodeDto o : listSifatCidera) {
			}
			setTotalSize(restCidera.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listSifatCidera");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listInstansi() {
		RestResponse restInstansi = callWs(WS_URI_LOV + "/getListInstansi",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {

			listInstansi = JsonUtil.mapJsonToListObject(
					restInstansi.getContents(), PlInstansiDto.class);

			setTotalSize(restInstansi.getTotalRecords());

			BindUtils.postNotifyChange(null, null, this, "listInstansi");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void searchKantorDiajukan() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchKantorDiajukan);

		if (getSearchKantorDiajukan() == null
				|| getSearchKantorDiajukan().equalsIgnoreCase("")) {
			setSearchKantorDiajukan("");
		}
		RestResponse rest = callWs(WS_URI_LOV + "/getAsalBerkas", map,
				HttpMethod.POST);
		try {
			listKantorDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndKantorJasaraharjaDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listKantorDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void searchInstansi() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchInstansi);
		if (getSearchInstansi() == null
				|| getSearchInstansi().equalsIgnoreCase("")) {
			setSearchInstansi("");
		}
		RestResponse rest = callWs(WS_URI_LOV + "/getListInstansi", map,
				HttpMethod.POST);
		try {
			listInstansiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlInstansiDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listInstansiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listStatusProses() {

		RestResponse restStatusProses = callWs(WS_URI_LOV
				+ "/getListStatusProses", new HashMap<String, Object>(),
				HttpMethod.POST);
		listStatusProses = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(
					restStatusProses.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listStatusProses.add(kosong);
			listStatusProses.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listStatusProses");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listJenisJaminan() {
		RestResponse restStatusProses = callWs(WS_URI_LOV
				+ "/getListJenisPertanggungan", new HashMap<String, Object>(),
				HttpMethod.POST);
		listJaminanDto = new ArrayList<>();
		List<PlJaminanDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(
					restStatusProses.getContents(), PlJaminanDto.class);
			PlJaminanDto kosong = new PlJaminanDto();
			kosong.setDeskripsi("-");
			kosong.setKodeJaminan(null);
			listJaminanDto.add(kosong);
			listJaminanDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listJaminanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void listStatusPenyelesaian() {

		RestResponse restPenyelesaian = callWs(WS_URI_LOV
				+ "/getListStatusPenyelesaian", new HashMap<String, Object>(),
				HttpMethod.POST);
		listPenyelesaian = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(
					restPenyelesaian.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listPenyelesaian.add(kosong);
			listPenyelesaian.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listPenyelesaian");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void searchRs(@BindingParam("item")String searchRs) {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchRs);
		if (getSearchRs() == null || getSearchRs().equalsIgnoreCase("")) {
			setSearchRs("");
		}

		RestResponse restRs = callWs(WS_URI_LOV + "/findAllRS", map,
				HttpMethod.POST);
		try {

			listRs = JsonUtil.mapJsonToListObject(restRs.getContents(),
					PlRumahSakitDto.class);

			setTotalSize(restRs.getTotalRecords());

			BindUtils.postNotifyChange(null, null, this, "listRs");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listStatusHubungan() {

		RestResponse restStatusHubungan = callWs(WS_URI_LOV
				+ "/getListStatusHubungan", new HashMap<String, Object>(),
				HttpMethod.POST);
		listStatusHubungan = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(
					restStatusHubungan.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listStatusHubungan.add(kosong);
			listStatusHubungan.addAll(listBaru);
			setTotalSize(restStatusHubungan.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listStatusHubungan");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void jenisIdentitas() {
		RestResponse restIdentitas = callWs(WS_URI_LOV + "/getListId",
				new HashMap<String, Object>(), HttpMethod.POST);
		listJenisIdentitasDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(
					restIdentitas.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listJenisIdentitasDto.add(kosong);
			listJenisIdentitasDto.addAll(listBaru);
			setTotalSize(restIdentitas.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this,
					"listJenisIdentitasDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void jenisPembayaran() {
		RestResponse restPembayaran = callWs(WS_URI_LOV
				+ "/getListJenisPembayaran", new HashMap<String, Object>(),
				HttpMethod.POST);
		try {
			listJenisPembayaran = JsonUtil.mapJsonToListObject(
					restPembayaran.getContents(), DasiJrRefCodeDto.class);
			setTotalSize(restPembayaran.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listJenisPembayaran");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void searchProvinsi() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchProvinsi);
		RestResponse rest = callWs(WS_URI_LOV + "/getListProvinsi", map,
				HttpMethod.POST);
		try {
			listProvinsiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listProvinsiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange({ "searchProvinsi" })
	@Command
	public void getKabkota() {
		camatDto.setKodeNamaCamat("");
		BindUtils.postNotifyChange(null, null, this, "camatDto");
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchKabkota);
		RestResponse listKabkotaRest = callWs(WS_URI_LOV + "/getListKabkota/"
				+ provinsiDto.getKodeProvinsi(), map, HttpMethod.POST);

		try {
			listKabkotaDto = JsonUtil.mapJsonToListObject(
					listKabkotaRest.getContents(), FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKabkotaDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange({ "kabKotaDto", "searchCamat", "searchKabkota" })
	@Command
	public void getCamat() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchCamat);
		RestResponse listCamatRest = callWs(WS_URI_LOV + "/getListCamat/"
				+ kabKotaDto.getKodeKabkota(), map, HttpMethod.POST);
		try {
			listCamatDto = JsonUtil.mapJsonToListObject(
					listCamatRest.getContents(), FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listCamatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange("checkDeskripsi")
	public void listDokumen() {
		RestResponse rest = callWs(WS_URI_LOV + "/getListJenisDokumen",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listJenisDokumenDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), DasiJrRefCodeDto.class);
			setTotalSize(rest.getTotalRecords());
			for(DasiJrRefCodeDto o : listJenisDokumenDto){
				jenisDokumenDto.setRvLowValue(o.getRvLowValue());
			}
			BindUtils.postNotifyChange(null, null, this, "listJenisDokumenDto");
			BindUtils.postNotifyChange(null, null, this, "jenisDokumenDto");

			for (DasiJrRefCodeDto dasi : listJenisDokumenDto) {
				// System.out.println("DASI "+JsonUtil.getJson(dasi));
				if (dasi == null) {
					return;
				}

				for (LampiranDto lampiran : lampirans) {
					// checkDeskripsi = false;
					if (lampiran.getDeskripsi().contains(dasi.getRvMeaning())) {
						// System.out.println("test "+lampiran.getDeskripsi()+
						// "\t" +dasi.getRvMeaning());
						// setCheckDeskripsi(true);
						int i = listJenisDokumenDto.indexOf(dasi);
						dasi.setCheckedJenisDomumen(true);
						listJenisDokumenDto.set(i, dasi);
						BindUtils.postNotifyChange(null, null, this,
								"checkDeskripsi");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private boolean flagSelectAllDokumen;

	@Command("selectAllDokumen")
	public void selectAllDokumen(
			@BindingParam("item2") DasiJrRefCodeDto jenisDokumenDto) {
		if (checkedSave == true) {
			for (DasiJrRefCodeDto a : listJenisDokumenDto) {
				a.setCheckedJenisDomumen(true);

			}
		} else if (checkedSave == false) {
			for (DasiJrRefCodeDto a : listJenisDokumenDto) {
				a.setCheckedJenisDomumen(false);
			}
		}
		BindUtils.postNotifyChange(null, null, this, "jenisDokumenDto");
		BindUtils.postNotifyChange(null, null, this, "listJenisDokumenDto");
	}

	@Command("changeValue")
	@NotifyChange({ "listJenisDokumenDto", "jenisDokumenDto" })
	public void changeValue() {
		checkedSave = !checkedSave;
	}

	// @Command("detailRingkasanPengajuan")
	// public void detailRingkasanPengajuan(
	// @BindingParam("item") PlPengajuanSantunanDto selected) {
	// // Messagebox.show("MUNCUL Di Santunan id Kecelakaan "
	// // +selected.getIdKecelakaan());
	// // Messagebox.show("MUNCUL di santunan id korban "
	// // +selected.getIdKorbanKecelakaan());
	// navigate(RINGKASAN_PAGE_PATH);
	//
	// Map<String, Object> map = new HashMap<>();
	// map.put("noBerkas", selected.getNoBerkas());
	// BindUtils.postGlobalCommand(null, null, "viewRingkasan", map);
	// }

	@Command("detailRingkasanPengajuan")
	public void showPopupRingkasan(
			@BindingParam("popup") String popup,
			@BindingParam("item") PlPengajuanSantunanDto plPengajuanSantunanDto,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();

		args.put("popUpHandler", globalHandleMethodName);
		args.put("plPengajuanSantunanDto", plPengajuanSantunanDto);

		// if (!beforePopup(args, popup))
		// return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	protected boolean beforePopupRingkasan(Map<String, Object> args,
			String popup) {
		args.put("plPengajuanSantunanDto", plPengajuanSantunanDto);
		return true;
	}

	@GlobalCommand("ringkasanHandler")
	public void titleHandlerRingkasan(
			@BindingParam("noBerkas") PlPengajuanSantunanDto selected) {
		if (plPengajuanSantunanDto != null) {
			this.plPengajuanSantunanDto = selected;
		}
	}

	/*
	 * @Command("detailRingkasanPengajuan") public void
	 * detailRingkasanPengajuan(
	 * 
	 * @BindingParam("item") PlPengajuanSantunanDto selectedPengajuan) { if
	 * (selectedPengajuan == null || selectedPengajuan.getNoBerkas() == null) {
	 * 
	 * // Messagebox.show("TAMPIL NO BERKAS" //
	 * +selectedPengajuan.getNoBerkas()); showSmartMsgBox("W001"); return;
	 * 
	 * }
	 * 
	 * Executions.getCurrent().setAttribute("obj", selectedPengajuan);
	 * getPageInfo().setViewMode(true); navigate(RINGKASAN_PAGE_PATH); }
	 * 
	 * public void onView() { plPengajuanSantunanDto = (PlPengajuanSantunanDto)
	 * Executions .getCurrent().getAttribute("obj"); Map<String, Object> map =
	 * new HashMap<>(); map.put("idKecelakaan",
	 * plPengajuanSantunanDto.getIdKecelakaan()); RestResponse rest =
	 * callWs(WS_URI_LAKA + "/dataLakaById", map, HttpMethod.POST);
	 * plPengajuanSantunanDto.setTglPenyelesaian(plPengajuanSantunanDto
	 * .getTglPenyelesaian());
	 * 
	 * try { listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(
	 * rest.getContents(), PlDataKecelakaanDto.class); //
	 * Messagebox.show("DATA NYA BERAPA ? "+listPlDataKecelakaanDtos.size());
	 * for (PlDataKecelakaanDto a : listPlDataKecelakaanDtos) { SimpleDateFormat
	 * date = new SimpleDateFormat("dd/MM/yyyy"); SimpleDateFormat time = new
	 * SimpleDateFormat("HH:mm"); String tgl = date.format(a.getTglKejadian());
	 * String jam = time.format(a.getTglKejadian()); Date date1 = new
	 * SimpleDateFormat("dd/MM/yyyy").parse(tgl); Date date2 = new
	 * SimpleDateFormat("HH:mm").parse(jam); setTglKejadian(date1);
	 * setJamKejadian(date2); String string = a.getNoLaporanPolisi(); String[]
	 * parts = string.split("/"); String part1 = parts[0]; String part2 =
	 * parts[1]; String part3 = parts[2]; String part4 = parts[3]; String part5
	 * = parts[4]; String part6 = parts[5];
	 * 
	 * setLaporan1(part1 + "/" + part2 + "/"); setLaporan2(part3);
	 * setLaporan3("/" + part4 + "/" + part5 + "/" + part6);
	 * plDataKecelakaanDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
	 * plDataKecelakaanDto.setNamaPetugas(a.getNamaPetugas());
	 * plDataKecelakaanDto .setTglLaporanPolisi(a.getTglLaporanPolisi());
	 * plDataKecelakaanDto.setKasus(a.getKasusKecelakaanDesc());
	 * plDataKecelakaanDto.setSifatKecelakaanDesc(a .getSifatKecelakaanDesc());
	 * plDataKecelakaanDto.setDeskripsiKecelakaan(a .getDeskripsiKecelakaan());
	 * plDataKecelakaanDto.setTglKejadian(date1);
	 * plDataKecelakaanDto.setDeskripsiLokasi(a.getDeskripsiLokasi()); //
	 * onEdit();
	 * 
	 * BindUtils.postNotifyChange(null, null, this, "laporan1");
	 * BindUtils.postNotifyChange(null, null, this, "laporan2");
	 * BindUtils.postNotifyChange(null, null, this, "laporan3");
	 * BindUtils.postNotifyChange(null, null, this, "kasusKecelakaanDto");
	 * BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
	 * BindUtils.postNotifyChange(null, null, this, "sifatKecelakaanDto");
	 * BindUtils.postNotifyChange(null, null, this, "tglKejadian");
	 * BindUtils.postNotifyChange(null, null, this, "date1");
	 * 
	 * } } catch (Exception e) { e.printStackTrace(); } }
	 * 
	 * @Command public void showDataPengajuanSantunan() { // ================GET
	 * DATA KORBAN======================== Map<String, Object> mapInput = new
	 * HashMap<>(); // // Messagebox.show("test " // +
	 * plPengajuanSantunanDto.getIdKorbanKecelakaan()); mapInput.put("idKorban",
	 * plPengajuanSantunanDto.getIdKorbanKecelakaan()); RestResponse rest2 =
	 * callWs(WS_URI_LAKA + "/korbanLakaByIdKorban", mapInput, HttpMethod.POST);
	 * 
	 * try { listKorban = JsonUtil.mapJsonToListObject(rest2.getContents(),
	 * PlKorbanKecelakaanDto.class);
	 * 
	 * for (PlKorbanKecelakaanDto a : listKorban) {
	 * plDataKecelakaanDto.setNamaKorban(a.getNama());
	 * plDataKecelakaanDto.setCidera(a.getCideraDesc()); plDataKecelakaanDto
	 * .setJenisIdentitas(a.getJenisIdentitasDesc());
	 * plDataKecelakaanDto.setNoIdentitas(a.getNoIdentitas());
	 * plDataKecelakaanDto.setStatus(a.getStatusKorbanDesc());
	 * plDataKecelakaanDto.setProvinsiDesc(a.getNamaProvinsi());
	 * plDataKecelakaanDto.setKabKotaDesc(a.getNamaKabkota());
	 * plDataKecelakaanDto.setCamatDesc(a.getNamaCamat());
	 * plDataKecelakaanDto.setAlamat(a.getAlamat());
	 * plDataKecelakaanDto.setNoTelp(a.getNoTelp());
	 * plDataKecelakaanDto.setJaminan(a.getPertanggunganDesc());
	 * 
	 * } BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto"); }
	 * catch (Exception e) { e.printStackTrace(); }
	 * 
	 * }
	 * 
	 * @Command("showHistory") public void showHistory() { Map<String, Object>
	 * map = new HashMap<>(); map.put("idKecelakaan",
	 * plPengajuanSantunanDto.getIdKecelakaan()); RestResponse restHistory =
	 * callWs(WS_URI + "/findByNoPengajuan", map, HttpMethod.POST); try {
	 * listDto = JsonUtil.mapJsonToListObject(restHistory.getContents(),
	 * PlPengajuanSantunanDto.class); // Messagebox.show("DATA NYA ADA "
	 * +listDto.size()); for (PlPengajuanSantunanDto p : listDto) {
	 * plPengajuanSantunanDto.setNoBerkas(p.getNoBerkas());
	 * plPengajuanSantunanDto.setDiajukanDi(p.getDiajukanDi());
	 * plPengajuanSantunanDto.setTglPengajuan(p.getTglPengajuan());
	 * plPengajuanSantunanDto.setTglPenyelesaian(p .getTglPenyelesaian());
	 * plPengajuanSantunanDto.setStatusProses(p.getStatusProses());
	 * plPengajuanSantunanDto.setPenyelesaian(p.getPenyelesaian());
	 * 
	 * } BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
	 * 
	 * } catch (Exception e) { e.printStackTrace(); }
	 * 
	 * }
	 */
	@Command
	public void prosesSelanjutnya() {
		try{
			RestResponse restResponse = null;

			DisposisiBerkasVmd disposisi = new DisposisiBerkasVmd();
			disposisi.checkDisposisi(plPengajuanSantunanDto, userSession);
			if (disposisi.checkDisposisi(plPengajuanSantunanDto, userSession) == true) {
				
				Map<String, Object> map = new HashMap<>();
				map.put("noBerkas",plPengajuanSantunanDto.getNoBerkas());
				RestResponse restBerkas = callWs(WS_URI+"/getBerkasPengajuan", map, HttpMethod.POST);
				
				try{
					listBerkasPengajuanDtos = JsonUtil.mapJsonToListObject(restBerkas.getContents(), PlBerkasPengajuanDto.class);
					if(listBerkasPengajuanDtos.size()<1){
						showInfoMsgBox("Kelengkapan Berkas Belum Diisi!", "");
						return;
					}else{
						Map<String, Object> mapSantunan = new HashMap<>();
						mapSantunan.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
						RestResponse restSantunan = callWs(WS_URI+"/findByNoBerkas", mapSantunan, HttpMethod.POST);
						PlPengajuanSantunanDto santunanDto = new PlPengajuanSantunanDto();
						listDto = JsonUtil.mapJsonToListObject(restSantunan.getContents(), PlPengajuanSantunanDto.class);
						for(PlPengajuanSantunanDto o : listDto){
							o.setLengkapFlag("Y");
							o.setStatusProses("BL");
							santunanDto = o;
						}
						
						restResponse = callWs(WS_URI + "/saveSantunanOnly", santunanDto, HttpMethod.POST);
						if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
							Map<String, Object> mapBerkas = new HashMap<>();
							mapBerkas.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());							
							RestResponse restBerkasPengajuan = callWs(WS_URI+"/getBerkasPengajuan", mapBerkas, HttpMethod.POST);
							try{
								listBerkasPengajuanDtos = JsonUtil.mapJsonToListObject(restBerkasPengajuan.getContents(), PlBerkasPengajuanDto.class);
								List<PlBerkasPengajuanDto> berkasPengajuanDtos = new ArrayList<>();
								PlBerkasPengajuanDto berkasPengajuanDto = new PlBerkasPengajuanDto();
								for(PlBerkasPengajuanDto b: listBerkasPengajuanDtos){
									b.setDiterimaFlag("Y");
									berkasPengajuanDto = b;
									berkasPengajuanDtos.add(berkasPengajuanDto);
								}
								RestResponse restSaveBerkas = callWs(WS_URI + "/saveBerkasPengajuan", berkasPengajuanDtos, HttpMethod.POST);
								
								if(restSaveBerkas.getStatus() == CommonConstants.OK_REST_STATUS){
									showInfoMsgBox("Data berhasil dikirim ke Proses Selanjutnya","");								
								}								
							}catch(Exception e){
								e.printStackTrace();
							}
						} else {
//							showErrorMsgBox(restResponse.getMessage());
						}

//						plPengajuanSantunanDto.setStatusProses("BL");
//						plPengajuanSantunanDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
//
//						restResponse = callWs(WS_URI + "/saveSantunanProsesSelanjutnya", plPengajuanSantunanDto, HttpMethod.POST);
//						if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
//							showInfoMsgBox("Data berhasil dikirim ke Proses Selanjutnya","");
//						} else {
//							showErrorMsgBox(restResponse.getMessage());
//						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}				
			} else {
				showInfoMsgBox("Disposisi Wajib Untuk Diisi!", "");
			}			
		}catch(Exception e){
			e.printStackTrace();
		}

	}

	@Command
	public void editDataLakaSantunan(@BindingParam("popup") String popup,
			@BindingParam("item") PlPengajuanSantunanDto selected) {
		// navigate(DATA_LAKA_PAGE_PATH);
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", selected.getIdKecelakaan());
		BindUtils.postGlobalCommand(null, null, "addForSantunan", map);

		try {
			((Window) Executions.createComponents(popup, null, map)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	@GlobalCommand("addPengajuanSantunan")
	public void addPengajuanSantunan(
			@BindingParam("idKecelakaan") String idKecelakaan,
			@BindingParam("idKorban") String idKorban) {
		plDataKecelakaanDto = new PlDataKecelakaanDto();
		plDataKecelakaanDto.setIdKecelakaan(idKecelakaan);
		plDataKecelakaanDto.setIdKorbanKecelakaan(idKorban);
		getPageInfo().setAddDetailMode(true);
		onAddDetail();
	}

	// @Command("lampiranSantunan")
	// public void lampiran(@BindingParam("item") PlPengajuanSantunanDto
	// selected) {
	// navigate(LAMPIRAN_PAGE_PATH);
	// Map<String, Object> map = new HashMap<>();
	//
	// Map<String, Object> map2 = new HashMap<>();
	// map2.put("idKecelakaan", selected.getIdKecelakaan());
	// RestResponse rest = callWs(WS_URI + "/findbyId", map2, HttpMethod.POST);
	// try {
	// listDataLakaDto = JsonUtil.mapJsonToListObject(rest.getContents(),
	// PlDataKecelakaanDto.class);
	//
	// for (PlDataKecelakaanDto a : listDataLakaDto) {
	// plDataKecelakaanDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
	// plDataKecelakaanDto.setTglKejadian(a.getTglKejadian());
	// plDataKecelakaanDto.setDeskripsiKecelakaan(a
	// .getDeskripsiKecelakaan());
	// plDataKecelakaanDto.setStatus(a.getStatus());
	// plDataKecelakaanDto.setKasus(a.getKasus());
	// plDataKecelakaanDto.setDeskripsiLokasi(a.getDeskripsiLokasi());
	// }
	// BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// // -------------------------------------------------
	//
	// map.put("santunanDto", selected);
	// map.put("noLaporanPolisi", plDataKecelakaanDto.getNoLaporanPolisi());
	// BindUtils.postGlobalCommand(null, null, "lampiranForSantunan", map);
	// }

	// =================================Lampiran====================================================
	@Command("lampiranSantunan")
	public void showPopupLampiran(
			@BindingParam("popup") String popup,
			@BindingParam("item") PlPengajuanSantunanDto plPengajuanSantunanDto,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();

		args.put("popUpHandler", globalHandleMethodName);
		args.put("plPengajuanSantunanDto", plPengajuanSantunanDto);
		System.out.println(JsonUtil.getJson(plPengajuanSantunanDto));
		// if (!beforePopup(args, popup))
		// return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	protected boolean beforePopupLampiran(Map<String, Object> args, String popup) {
		args.put("plPengajuanSantunanDto", plPengajuanSantunanDto);
		return true;
	}

	@GlobalCommand("lampiranHandler")
	public void titleHandlerLampiran(
			@BindingParam("idKecelakaan") PlPengajuanSantunanDto selected) {
		if (plPengajuanSantunanDto != null) {
			this.plPengajuanSantunanDto = selected;
		}
	}

	@Command
	public void searchAsalBerkas() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchDiajukanDi);

		if (getSearchDiajukanDi() == null
				|| getSearchDiajukanDi().equalsIgnoreCase("")) {
			setSearchDiajukanDi("");
		}
		RestResponse rest = callWs(WS_URI_LOV + "/getAsalBerkas", map,
				HttpMethod.POST);
		try {
			listDiajukanDiDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), FndKantorJasaraharjaDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listDiajukanDiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command("addDisposisi")
	public void add() {
		if (plPengajuanSantunanDto == null
				|| plPengajuanSantunanDto.getNoBerkas() == null) {

			// Messagebox.show("TAMPIL NO BERKAS"
			// +selected.getNoBerkas());
			showSmartMsgBox("W001");
			return;
		}
		// Executions.getCurrent().setAttribute("obj", selectedPengajuan);
		// Messagebox.show("masuk disposisi");
		Executions.getCurrent().setAttribute("obj", plPengajuanSantunanDto);
		getPageInfo().setViewDetailMode(true);
		navigate(DISPOSISI_PAGE_PATH);
		// disposisi();
	}

	// @Command("addDisposisi")
	public void onViewDetail() {
		plPengajuanSantunanDto = (PlPengajuanSantunanDto) Executions
				.getCurrent().getAttribute("obj");
		// getPageInfo().getCustomAttribute().equalsIgnoreCase("disposisi");
		Map<String, Object> mapInput = new HashMap<>();

		mapInput.put("idKorban", plPengajuanSantunanDto.getIdKorbanKecelakaan());
		RestResponse rest2 = callWs(WS_URI_LAKA + "/korbanLakaByIdKorban",
				mapInput, HttpMethod.POST);
		plPengajuanSantunanDto
				.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
		// Messagebox.show("DAPAT NO NYA :"
		// +plPengajuanSantunanDto.getNoBerkas());

		try {
			listKorban = JsonUtil.mapJsonToListObject(rest2.getContents(),
					PlKorbanKecelakaanDto.class);

			for (PlKorbanKecelakaanDto a : listKorban) {
				plDataKecelakaanDto.setNamaKorban(a.getNama());

			}
			BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
			showListDisposisi();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// ========================================DISPOSISI BERKAS==================================
	@Command("disposisiBerkas")
	public void showPopup(
			@BindingParam("popup") String popup,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();

		args.put("popUpHandler", globalHandleMethodName);

		if (!beforePopup(args, popup))
			return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	protected boolean beforePopup(Map<String, Object> args, String popup) {
		args.put("plPengajuanSantunanDto", plPengajuanSantunanDto);
		return true;
	}

	@GlobalCommand("iniHandler")
	public void titleHandler(
			@BindingParam("noBerkas") PlPengajuanSantunanDto selected) {
		if (plPengajuanSantunanDto != null) {
			this.plPengajuanSantunanDto = selected;
		}
	}

	@Command("tambahDisposisi")
	public void tambahDisposisi() {
		setShowFormDisposisi(true);
		BindUtils.postNotifyChange(null, null, this, "showFormDisposisi");
		setShowListDisposisi(false);
		BindUtils.postNotifyChange(null, null, this, "showListDisposisi");
	}

	@Command("showDisposisi")
	public void showListDisposisi() {
		setShowFormDisposisi(false);
		BindUtils.postNotifyChange(null, null, this, "showFormDisposisi");
		setShowListDisposisi(true);
		BindUtils.postNotifyChange(null, null, this, "showListDisposisi");
		Map<String, Object> map = new HashMap<>();
		map.put("noBerkas", plDisposisiDto.getNoBerkas());
		// Messagebox.show("MUNCUL "+plDisposisiDto.getNoBerkas());
		RestResponse restDisposisi = callWs(WS_URI_LOV + "/getListDisposisi",
				map, HttpMethod.POST);
		// Messagebox.show("a "+restDisposisi.getMessage()+" "+restDisposisi.getContents());
		try {
			listDisposisi = JsonUtil.mapJsonToListObject(
					restDisposisi.getContents(), PlDisposisiDto.class);
			// Messagebox.show("MUNCUL LIST "+listDisposisi.size());
			BindUtils.postNotifyChange(null, null, this, "listDisposisi");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Command("saveDisposisi")
	public void saveDisposisi() {
		RestResponse restResponse = null;
		plDisposisiDto.setDari("FO");
		plDisposisiDto.setTglDisposisi(plDisposisiDto.getTglDisposisi());
		plDisposisiDto.setDisposisi(plDisposisiDto.getDisposisi());
		plDisposisiDto.setIdDisposisi("100");
		plDisposisiDto.setLevelCabangDisp("CC");
		plDisposisiDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
		restResponse = callWs(WS_URI + "/saveDisposisi", plDisposisiDto,
				HttpMethod.POST);

		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
			showListDisposisi();

		} else {
			showErrorMsgBox(restResponse.getMessage());
		}

	}

	public void cetakDokumen() {

	}

	// ======================================== CETAK
	// ==================================================

	/*
	 * Untuk buat file docx
	 */
	private String desktop = "C:\\Users\\Superman\\Desktop\\";
	private String companyName = "PT. JASA RAHARJA (PERSERO)";
	private String noBerkass = "2-001-00-02-00-11-2018";
	private String namaCabang = "CABANG DKI JAKARTA";
	private String namaPengaju = "Jajat Sudrajat";
	private String kecPengaju = "Kec. Cikarang Utara";
	private String kotaPengaju = "Bekasi";
	private String telpPengaju = "0853516689577";
	private String alamatPengaju = "Kp. Cabang Lio, RT.003/004, Kel. Karang Asih"
			+ ", " + kecPengaju + ", " + kotaPengaju + " / " + telpPengaju;
	private String hubunganPengajuDgKorban = "Korban Sendiri";
	private String identitasKorban = "3216091601790011";
	private String namaKorbann = "Jajat Sudrajat";
	private String umurKorban = "37";
	private String kecKorban = "Kec. Cikarang Utara";
	private String kotaKorban = "Bekasi";
	private String telpKorban = "0853516689577";
	private String alamatKorban = "Kp. Cabang Lio, RT.003/004, Kel. Karang Asih"
			+ ", " + kecKorban + ", " + kotaKorban + " / " + telpKorban;
	private String kecKecelakaan = "Kec. Pulogadung";
	private String kotaKecelakaan = "Jakarta Timur";
	private String tempatKecelakaan = kecKecelakaan + ", " + kotaKecelakaan;
	private Date tglKecelakaan = new Date();
	private String sifatCidera = "Luka - Luka";
	private List<String> listBerkasDiterima = new ArrayList<>();
	private List<String> listBerkasKurang = new ArrayList<>();
	private String namaLogin = "Petugas Testing 03";
	private String lokasi = "Jakarta";

	public static void main(String[] args) throws Exception {

		TestVmd a = new TestVmd();
		// System.out.println("");
		// a.createDoc();
		a.loaddong();
	}

	private void createBlank() throws Exception {
		XWPFDocument doc = new XWPFDocument();

		FileOutputStream out = new FileOutputStream(new File(desktop
				+ "blank.docx"));
		doc.write(out);
		out.close();
		System.out.println("blank.docx written successully");
	}

	private void createParagraph() throws Exception {
		// Blank Document
		XWPFDocument document = new XWPFDocument();

		// Write the Document in file system
		FileOutputStream out = new FileOutputStream(new File(desktop
				+ "createparagraph.docx"));

		// create Paragraph
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setText("At tutorialspoint.com, we strive hard to "
				+ "provide quality tutorials for self-learning "
				+ "purpose in the domains of Academics, Information "
				+ "Technology, Management and Computer Programming Languages.");

		document.write(out);
		out.close();
		System.out.println("createparagraph.docx written successfully");
	}

	/*
	 * nambah berkas yg diterima
	 */

	private void addListBerkas() {
		listBerkasDiterima = new ArrayList<>();
		if (lampirans != null) {
			for (LampiranDto l : lampirans) {
				listBerkasDiterima.add(l.getDeskripsi());
			}
		}

	}

	private void addDokumenKurang() {
		/*
		 * Semua dokumen yang belum masuk list diterima, dimasukkan disini
		 */
	}

	private String getBulan(Date tgl) {
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String month = sdf.format(new Date());
		return month.equalsIgnoreCase("01") ? "Januari" : month
				.equalsIgnoreCase("02") ? "Februari" : month
				.equalsIgnoreCase("03") ? "Maret" : month
				.equalsIgnoreCase("04") ? "April" : month
				.equalsIgnoreCase("05") ? "Mei"
				: month.equalsIgnoreCase("06") ? "Juni" : month
						.equalsIgnoreCase("07") ? "Juli" : month
						.equalsIgnoreCase("08") ? "Agustus" : month
						.equalsIgnoreCase("09") ? "September" : month
						.equalsIgnoreCase("10") ? "Oktober" : month
						.equalsIgnoreCase("11") ? "Nopember" : month
						.equalsIgnoreCase("12") ? "Desember" : "";

	}

	private String checkJam(Date tgl) {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(tgl);
	}

	private String getHari(Date tgl) {
		SimpleDateFormat sdf = new SimpleDateFormat("DD");
		return sdf.format(tgl);
	}

	private String getTahun(Date tgl) {
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
		return sdf.format(tgl);
	}

	private String formatTanggal(Date tgl) {
		return getHari(tgl) + " " + getBulan(tgl) + " " + getTahun(tgl);
	}

	private void setTableBorderNone(XWPFTable table) {
		CTTblPr tblpro = table.getCTTbl().getTblPr();

		CTTblWidth tblW = tblpro.addNewTblW();
		// tblW.setW(arg0);

		CTTblBorders border = tblpro.addNewTblBorders();

		border.addNewBottom().setVal(STBorder.NONE);
		border.addNewLeft().setVal(STBorder.NONE);
		border.addNewRight().setVal(STBorder.NONE);
		border.addNewTop().setVal(STBorder.NONE);
		border.addNewInsideH().setVal(STBorder.NONE);
		border.addNewInsideV().setVal(STBorder.NONE);
	}

	private void addRow(XWPFTable table, String... a) {
		if (a.length > 0) {
			XWPFTableRow row = table.createRow();
			for (int x = 0; x < a.length; x++) {
				if (x > 0 && (row.getCell(x) == null)) {
					row.addNewTableCell().setText(a[x]);
				} else {
					row.getCell(x).setText(a[x]);
				}
			}
		}
	}

	private void addFirstRow(XWPFTable table, String... a) {
		if (a.length > 0) {
			XWPFTableRow row = table.createRow();
			row.getCell(0).setText(a[0]);
			for (int x = 1; x < a.length; x++) {
				row.addNewTableCell().setText(a[x]);
			}
		}
	}

	private void setCellWidth(XWPFTable tabel, int column, long value) {
		tabel.getRow(0).getCell(column).getCTTc().addNewTcPr().addNewTcW()
				.setW(BigInteger.valueOf(value));
	}

	private void setCellWidth(XWPFTableRow baris, int column, long value) {
		baris.getCell(column).getCTTc().addNewTcPr().addNewTcW()
				.setW(BigInteger.valueOf(value));
	}

	public void setTableAlign(XWPFTable table, ParagraphAlignment align) {
		CTTblPr tblPr = table.getCTTbl().getTblPr();
		CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
		STJc.Enum en = STJc.Enum.forInt(align.getValue());
		jc.setVal(en);
	}

	@Command
	public void loaddong(
			@BindingParam("item") PlPengajuanSantunanDto selectedPengajuan)
			throws Exception {
		/*
		 * if (selectedPengajuan == null || selectedPengajuan.getNoBerkas() ==
		 * null) { showSmartMsgBox("W001"); return;
		 * 
		 * }
		 */

		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", plPengajuanSantunanDto.getIdKecelakaan());
		// Messagebox.show("MUNCUL :" +plPengajuanSantunanDto.get)
		RestResponse rest = callWs(WS_URI + "/findbyId", map, HttpMethod.POST);
		try {
			listDataLakaDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlDataKecelakaanDto.class);
			plPengajuanSantunanDto.setNoBerkas(plPengajuanSantunanDto
					.getNoBerkas());
			plPengajuanSantunanDto.setIdKecelakaan(plPengajuanSantunanDto
					.getIdKecelakaan());
			plPengajuanSantunanDto.setAlamatPemohon(plPengajuanSantunanDto
					.getAlamatPemohon());
			statusHubunganDto.setRvMeaning(plPengajuanSantunanDto
					.getStatusHubunganDes());
			plPengajuanSantunanDto.setNoIdentitas(plPengajuanSantunanDto
					.getNoIdentitas());
			sifatCideraDto.setRvMeaning(plPengajuanSantunanDto
					.getKodeCideraDes());
			plPengajuanSantunanDto.setNamaPemohon(plPengajuanSantunanDto
					.getNamaPemohon());
			sifatCideraDto.setRvLowValue(plPengajuanSantunanDto.getCideraDes());
			for (PlDataKecelakaanDto a : listDataLakaDto) {
				SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat time = new SimpleDateFormat("HH:mm");
				String tgl = date.format(a.getTglKejadian());
				String jam = time.format(a.getTglKejadian());
				Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(tgl);
				Date date2 = new SimpleDateFormat("HH:mm").parse(jam);
				setTglKejadian(date1);
				setJamKejadian(date2);
				String string = a.getNoLaporanPolisi();
				String[] parts = string.split("/");
				String part1 = parts[0];
				String part2 = parts[1];
				String part3 = parts[2];
				String part4 = parts[3];
				String part5 = parts[4];
				String part6 = parts[5];

				setLaporan1(part1 + "/" + part2 + "/");
				setLaporan2(part3);
				setLaporan3("/" + part4 + "/" + part5 + "/" + part6);
				plDataKecelakaanDto.setNamaKorban(a.getNamaKorban());
				plDataKecelakaanDto.setAlamat(a.getAlamat());
				plDataKecelakaanDto.setDeskripsiLokasi(a.getDeskripsiLokasi());
				plDataKecelakaanDto.setCidera(a.getCidera());
				plDataKecelakaanDto.setNoIdentitas(a.getNoIdentitas());
				plDataKecelakaanDto.setJenisIdentitas(a.getJenisIdentitas());
				plDataKecelakaanDto.setUmur(a.getUmur());
				plDataKecelakaanDto.setIdKecelakaan(a.getIdKecelakaan());
				plDataKecelakaanDto.setIdKorbanKecelakaan(a
						.getIdKorbanKecelakaan());

			}
			BindUtils.postNotifyChange(null, null, this,
					"plPengajuanSantunanDto");

		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

		SimpleDateFormat date = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat time = new SimpleDateFormat("HH:mm");
		String tgl2 = date.format(plDataKecelakaanDto.getTglKejadian());
		String jam = time.format(plDataKecelakaanDto.getTglKejadian());

		SimpleDateFormat tglKec = new SimpleDateFormat();
		String tagglKec = tglKec.format(tglKecelakaan);
		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();

		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}

		pageSize.setOrient(STPageOrientation.LANDSCAPE);
		pageSize.setW(BigInteger.valueOf(842 * 20));
		pageSize.setH(BigInteger.valueOf(595 * 20));
		// document.getDocument().getBody().getSectPr().addNewPgSz().setOrient(STPageOrientation.LANDSCAPE);

		// nama file
		// String name = "document.docx";

		// tempat dokument di simpan
		// FileOutputStream out = new FileOutputStream(new File(desktop+name));

		// bagian 1
		XWPFParagraph par = document.createParagraph();
		// par.getCTP().

		// run pertama
		XWPFRun run1 = par.createRun();
		run1.setFontFamily("calibri");
		run1.setText(companyName);
		int x = 0;
		while (x < 5) {
			run1.addTab();
			x++;
		}
		run1.setText("No Berkas: " + plPengajuanSantunanDto.getNoBerkas());
		run1.addCarriageReturn();
		run1.setText(namaCabang);
		run1.addCarriageReturn();

		// bagian 2
		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.CENTER);
		// set align CENTER untuk tulisan

		// run kedua
		XWPFRun run2 = par2.createRun();
		run2.setText("TANDA TERIMA");

		// bagian 3
		XWPFParagraph par3 = document.createParagraph();
		XWPFRun run3 = par3.createRun();
		run3.setText("Telah terima berkas pengajuan klaim dari : ");

		// bagian 4
		XWPFTable tabelDataDiri = document.createTable();

		// set tabel border = NONE
		setTableBorderNone(tabelDataDiri);

		XWPFTableRow row1Data = tabelDataDiri.getRow(0);
		row1Data.getCell(0).setText("Nama");
		row1Data.getCell(0).getCTTc().addNewTcPr().addNewTcW()
				.setW(BigInteger.valueOf(2000));
		row1Data.addNewTableCell().setText(":");
		row1Data.getCell(1).getCTTc().addNewTcPr().addNewTcW()
				.setW(BigInteger.valueOf(100));
		row1Data.addNewTableCell().setText(
				plPengajuanSantunanDto.getNamaPemohon());
		XWPFTableRow row2Data = tabelDataDiri.createRow();
		row2Data.getCell(0).setText("Alamat / Telp");
		row2Data.getCell(1).setText(":");
		row2Data.getCell(2).setText(plPengajuanSantunanDto.getAlamatPemohon());
		XWPFTableRow row3Data = tabelDataDiri.createRow();
		row3Data.getCell(0).setText("Hub dgn korban");
		row3Data.getCell(1).setText(":");
		row3Data.getCell(2).setText(
				plPengajuanSantunanDto.getStatusHubunganDes());
		XWPFTableRow row4Data = tabelDataDiri.createRow();
		row4Data.getCell(0).setText("Identitas Korban");
		row4Data.getCell(1).setText(":");
		row4Data.getCell(2).setText(plDataKecelakaanDto.getNoIdentitas());
		XWPFTableRow row5Data = tabelDataDiri.createRow();
		row5Data.getCell(0).setText("Nama / Umur");
		row5Data.getCell(1).setText(":");
		row5Data.getCell(2).setText(
				plDataKecelakaanDto.getNamaKorban() + " / "
						+ plDataKecelakaanDto.getUmur() + "tahun");
		XWPFTableRow row6Data = tabelDataDiri.createRow();
		row6Data.getCell(0).setText("Alamat / Telp");
		row6Data.getCell(1).setText(":");
		row6Data.getCell(2).setText(
				plDataKecelakaanDto.getAlamat() + "/"
						+ plDataKecelakaanDto.getNoTelp());
		XWPFTableRow row7Data = tabelDataDiri.createRow();
		row7Data.getCell(0).setText("Tempat Kecelakaan");
		row7Data.getCell(1).setText(":");
		row7Data.getCell(2).setText(plDataKecelakaanDto.getDeskripsiLokasi());
		XWPFTableRow row8Data = tabelDataDiri.createRow();
		row8Data.getCell(0).setText("Tanggal Kecelakaan");
		row8Data.getCell(1).setText(":");
		row8Data.getCell(2).setText(formatTanggal(tglKejadian));
		XWPFTableRow row9Data = tabelDataDiri.createRow();
		row9Data.getCell(0).setText("Sifat Cidera");
		row9Data.getCell(1).setText(":");
		row9Data.getCell(2).setText(plPengajuanSantunanDto.getCideraDes());

		// Bagian 5
		XWPFParagraph par5 = document.createParagraph();
		// isi list dokumen
		addListBerkas();

		XWPFTable tabelBerkas = document.createTable();
		setTableBorderNone(tabelBerkas);

		XWPFTableRow rowData = tabelBerkas.getRow(0);
		rowData.getCell(0).setText("Berkas terdiri dari : ");
		rowData.getCell(0).getCTTc().addNewTcPr().addNewTcW()
				.setW(BigInteger.valueOf(10000));
		rowData.addNewTableCell().setText("Dokumen yang harus dilengkapi");
		rowData.getCell(1).getCTTc().addNewTcPr().addNewTcW()
				.setW(BigInteger.valueOf(10000));

		if (listBerkasDiterima.size() < listBerkasKurang.size()) {
			for (int i = 0; i < listBerkasKurang.size(); i++) {
				String kurang = listBerkasKurang.get(i);
				String diterima = "";
				try {
					diterima = listBerkasDiterima.get(i);
				} catch (Exception p) {

				}
				addRow(tabelBerkas, diterima, kurang);
			}
		} else if (listBerkasDiterima.size() > listBerkasKurang.size()) {
			for (int i = 0; i < listBerkasDiterima.size(); i++) {
				String diterima = listBerkasDiterima.get(i);
				String kurang = "";
				try {
					kurang = listBerkasKurang.get(i);
				} catch (Exception p) {
				}
				addRow(tabelBerkas, diterima, kurang);
			}
		}

		// Bagian 6
		XWPFParagraph par6 = document.createParagraph();
		par6.setAlignment(ParagraphAlignment.RIGHT);
		XWPFRun run6 = par6.createRun();
		run6.setText(lokasi + ", " + formatTanggal(new Date()));
		run6.addCarriageReturn();
		run6.setText("Jam Proses : " + checkJam(new Date()));

		// bagian 7
		XWPFTable tabelTTD = document.createTable();
		setTableBorderNone(tabelTTD);
		XWPFTableRow row = tabelTTD.getRow(0);
		row.getCell(0).setText("Yang Menyerahkan");
		setCellWidth(row, 0, 10000);
		row.addNewTableCell().setText("Yang Menerima");
		row.getCell(1).getParagraphs().get(0)
				.setAlignment(ParagraphAlignment.RIGHT);
		setCellWidth(row, 1, 10000);
		row.setHeight(100);

		XWPFTableRow rowx = tabelTTD.createRow();
		addRow(tabelTTD, plPengajuanSantunanDto.getNamaPemohon(), namaLogin);
		rowx.getCell(1).getParagraphs().get(0)
				.setAlignment(ParagraphAlignment.RIGHT);

		// download dokumen
		File temp = File.createTempFile("Document JR", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Document JR", "docx", "application/file",
				fis));
		temp.delete();

		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");

	}

	// =================CETAK LEMBAR DISPOSISI=================================

	private List<String> listDari = new ArrayList<>();
	private List<String> listPendapatSaranPetunjuk = new ArrayList<>();
	private List<String> listTgl = new ArrayList<>();
	private List<String> listNamaUserJr = new ArrayList<>();

	@Command("cetakDisposisi")
	public void cetakDisposisi(
			@BindingParam("item") PlPengajuanSantunanDto selected)
			throws IOException {
		if (selected == null || selected.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}

		CetakLdpb cetak = new CetakLdpb();
		cetak.cetakDisposisi(selected);
	}

	@Command
	public void printDocument(
			@BindingParam("item") PlPengajuanSantunanDto plDataKecelakaanDto)
			throws Exception {
		String idKec = plDataKecelakaanDto.getIdKecelakaan();
		// String userLogin = getCurrentUserSessionJR().getLoginID();
		// String kantor = getCurrentUserSessionJR().getKantor();
		Map<String, Object> map = new HashMap<>();
		map.put("idKec", idKec);
		map.put("login", getCurrentUserSessionJR().getLoginID());
		map.put("kantor", getCurrentUserSessionJR().getKantor());
		AuthUserDto petugas = new AuthUserDto();
		petugas.setDescription(getCurrentUserSessionJR().getLoginDesc());
		petugas.setLoginUsername(getCurrentUserSessionJR().getUserName());
		map.put("petugas", petugas);
		Map<String, Object> out = new HashMap<>();
		RestResponse rest = new RestResponse();
		try {
			rest = callWs(WS_URI_LAKA + "/getPrintData", map, HttpMethod.POST);
			out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			System.out.println(JsonUtil.getJson(out));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		Map<String, Object> args = new HashMap<>();
		args.put("map", out);
		DataKecelakaanPrint print = new DataKecelakaanPrint();
		print.cetak(out);
		BindUtils.postGlobalCommand(null, null, "cetakDataLaka", args);
	}

	// private void addRow(XWPFTable tabelDisposisi, String string, String dari,
	// String disposisi, Date tglDisposisi2, String createdBy) {
	//
	// }

	@Command("entriJaminanLanjutan")
	public void entriJaminanLanjutan(
			@BindingParam("item") PlPengajuanSantunanDto selectedPengajuan) {
		if (selectedPengajuan == null
				|| selectedPengajuan.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;

		}
		Executions.getCurrent().setAttribute("obj", selectedPengajuan);
		getPageInfo().setTaskMode(true);
		navigate(ENTRI_JAMINAN_PAGE_PATH);

	}

	public void onTask() {
		plPengajuanSantunanDto = (PlPengajuanSantunanDto) Executions
				.getCurrent().getAttribute("obj");

		plPengajuanSantunanDto
				.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
		plPengajuanSantunanDto.setIdKorbanKecelakaan(plPengajuanSantunanDto
				.getIdKorbanKecelakaan());
		PlDataKecelakaanDto dataKecelakaanDto = new PlDataKecelakaanDto();
		dataKecelakaanDto.setIdKorbanKecelakaan(plPengajuanSantunanDto
				.getIdKorbanKecelakaan());

		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("idKorban", selectedPengajuan.getIdKorbanKecelakaan());
		RestResponse rest2 = callWs(WS_URI_LAKA + "/korbanLakaByIdKorban",
				mapInput, HttpMethod.POST);

		try {
			listKorban = JsonUtil.mapJsonToListObject(rest2.getContents(),
					PlKorbanKecelakaanDto.class);

			for (PlKorbanKecelakaanDto a : listKorban) {
				plDataKecelakaanDto.setNamaKorban(a.getNama());
				plDataKecelakaanDto.setCidera(a.getCideraDesc());
				plDataKecelakaanDto
						.setJenisIdentitas(a.getJenisIdentitasDesc());
				plDataKecelakaanDto.setNoIdentitas(a.getNoIdentitas());
				plDataKecelakaanDto.setStatus(a.getStatusKorbanDesc());
				plDataKecelakaanDto.setUmur(a.getUmur());
				plDataKecelakaanDto.setJenisKelamin(a.getJenisKelamin());
				plDataKecelakaanDto.setAlamat(a.getAlamat());

			}
			BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
		// ============================GET DATA
		// LAKA====================================
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", selectedPengajuan.getIdKecelakaan());
		RestResponse rest = callWs(WS_URI + "/findbyId", map, HttpMethod.POST);
		try {
			listDataLakaDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlDataKecelakaanDto.class);

			for (PlDataKecelakaanDto a : listDataLakaDto) {
				plDataKecelakaanDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
				plDataKecelakaanDto.setTglKejadian(a.getTglKejadian());
				plDataKecelakaanDto.setDeskripsiKecelakaan(a
						.getDeskripsiKecelakaan());
				plDataKecelakaanDto.setStatus(a.getStatus());
				plDataKecelakaanDto.setKasus(a.getKasus());
				plDataKecelakaanDto.setDeskripsiLokasi(a.getDeskripsiLokasi());
			}
			BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Command
	@NotifyChange({"lampirans","listKelengkapanBerkas","saveSantunanFlag"})
	public void addDeskripsiLampiran() {
		try{
			if(isSaveSantunanFlag()==false){
				showInfoMsgBox("Data Pengajuan Santunan Belum Disimpan!", "");
				return;
			}
			else{
				for(DasiJrRefCodeDto a : listJenisDokumenDtoSelected){
					PlBerkasPengajuanDto dto = new PlBerkasPengajuanDto();
					dto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
					dto.setKodeBerkas(a.getRvLowValue());
					dto.setDiterimaFlag("N");
					dto.setAbsahFlag("N");
					dto.setTglTerimaBerkas(getTglTerimaBerkas());
					dto.setCreatedBy(userSession.getLoginID());
					dto.setCreationDate(new Date());
					a.setCheckedJenisDomumen(true);
					listBerkasPengajuanDtos.add(dto);
				}
				
				RestResponse restResponseBerkas = callWs(WS_URI + "/saveBerkasPengajuan",listBerkasPengajuanDtos, HttpMethod.POST);
					
				if (restResponseBerkas.getStatus() == CommonConstants.OK_REST_STATUS) {
					List<PlBerkasPengajuanDto> lLampiran = new ArrayList<>();
					outerloop: for (DasiJrRefCodeDto dasi : listJenisDokumenDtoSelected) {
						// System.out.println("DASI "+JsonUtil.getJson(dasi));
						if (dasi == null) {
							break outerloop;
						}

						if (dasi.isCheckedJenisDomumen()) {
							PlBerkasPengajuanDto lamp = new PlBerkasPengajuanDto();
							lamp.setKodeBerkasDesc(dasi.getRvMeaning());
							lamp.setKodeBerkas(dasi.getRvLowValue());
							lamp.setTglTerimaBerkas(tglTerimaBerkas);
							lamp.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
							lLampiran.add(lamp);
						}

					}

					for (final PlBerkasPengajuanDto lampiranDto : lLampiran) {

						if (listKelengkapanBerkas.stream().anyMatch(new Predicate<PlBerkasPengajuanDto>() {

							@Override
							public boolean test(PlBerkasPengajuanDto arg0) {
								return arg0.getKodeBerkasDesc().contains(
										lampiranDto.getKodeBerkasDesc());
							}
						})) {

						} else {
							listKelengkapanBerkas.add(lampiranDto);
						}
					}
//					showInfoMsgBox(restResponseBerkas.getMessage());
				}else{
//					showErrorMsgBox(restResponseBerkas.getMessage());
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
//		List<LampiranDto> lLampiran = new ArrayList<>();
//		outerloop: for (DasiJrRefCodeDto dasi : listJenisDokumenDto) {
//			// System.out.println("DASI "+JsonUtil.getJson(dasi));
//			if (dasi == null) {
//				break outerloop;
//			}
//
//			if (dasi.isCheckedJenisDomumen()) {
//				LampiranDto lamp = new LampiranDto();
//				lamp.setDeskripsi(dasi.getRvMeaning());
//				lamp.setTglUpload(new Date());
//				lLampiran.add(lamp);
//			}
//
//		}
//
//		for (final LampiranDto lampiranDto : lLampiran) {
//
//			if (lampirans.stream().anyMatch(new Predicate<LampiranDto>() {
//
//				@Override
//				public boolean test(LampiranDto arg0) {
//					return arg0.getDeskripsi().contains(
//							lampiranDto.getDeskripsi());
//				}
//			})) {
//
//			} else {
//				lampirans.add(lampiranDto);
//			}
//		}

		// for(LampiranDto lampiranDto : lampirans){
		// System.out.println(lampiranDto.getDeskripsi() + "\t" +
		// lampiranDto.getTglUpload());
		//
		// }
	}

	@Command
	// @NotifyChange("listJenisDokumenDto")
	public void addToLampirans(
			@BindingParam("item") DasiJrRefCodeDto jenisDokumen) {
		// LampiranDto lampiranDto = new LampiranDto();
		// lampiranDto.setDeskripsi(rvMeanig);
		// lampiranDto.setTglUpload(new Date());
		// lampirans.add(lampiranDto);

		int index = listJenisDokumenDto.indexOf(jenisDokumen);
		jenisDokumen.setCheckedJenisDomumen(true);
		listJenisDokumenDto.set(index, jenisDokumen);
		// BindUtils.postNotifyChange(null, null, this, "listJenisDokumenDto");
	}

	@Command
	@NotifyChange({"lampirans","listKelengkapanBerkas","listJenisDokumenDtoSelected"})
	public void deleteListBerkas(@BindingParam("item") PlBerkasPengajuanDto selectedDto) {
		try{
			List<PlBerkasPengajuanDto> listDelete = new ArrayList<>();
			listDelete.add(selectedDto);
			System.err.println("luthfi77 "+selectedDto.getNoBerkas());
			RestResponse deleteBerkasRest = callWs(WS_URI + "/deleteBerkasPengajuan", listDelete, HttpMethod.POST);
			if(deleteBerkasRest.getStatus()== CommonConstants.OK_REST_STATUS){
				listKelengkapanBerkas.remove(selectedDto);
				// listJenisDokumenDto.stream().filter(new Predicate<DasiJrRefCodeDto>()
				// {
				//
				// @Override
				// public boolean test(DasiJrRefCodeDto arg0) {
				// return arg0.getRvMeaning().equals(lampiranDto.getDeskripsi());
				// }
				// });
				for (DasiJrRefCodeDto dto : listJenisDokumenDto) {
					if (dto.getRvMeaning().equals(selectedDto.getKodeBerkasDesc())) {
						listJenisDokumenDtoSelected.remove(dto);
					}
				}				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	@NotifyChange({"lampirans","listKelengkapanBerkas","listJenisDokumenDtoSelected"})
	public void deleteAllBerkas(@ContextParam(ContextType.TRIGGER_EVENT) CheckEvent d){
		try{
			List<PlBerkasPengajuanDto> listDelete = new ArrayList<>();
			listDelete = getListKelengkapanBerkas();
			RestResponse deleteBerkasRest = callWs(WS_URI + "/deleteBerkasPengajuan", listDelete, HttpMethod.POST);
			if(deleteBerkasRest.getStatus()== CommonConstants.OK_REST_STATUS){
				for(PlBerkasPengajuanDto a : listDelete){
					PlBerkasPengajuanDto selectedDto = new PlBerkasPengajuanDto();
					selectedDto.setKodeBerkasDesc(a.getKodeBerkasDesc());
					for (DasiJrRefCodeDto dto : listJenisDokumenDto) {
						if (dto.getRvMeaning().equals(selectedDto.getKodeBerkasDesc())) {
							listJenisDokumenDtoSelected.remove(dto);
						}
					}				
				}
				
				listKelengkapanBerkas.removeAll(listDelete);
			}else{
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	public void tabKelengkapanBerkas(){
		setTglTerimaBerkas(new Date());
		listDokumen();
		listKelengkapanBerkas.clear();
		listJenisDokumenDtoSelected.clear();
		Map<String, Object> map = new HashMap<>();
		map.put("noBerkas",plPengajuanSantunanDto.getNoBerkas());
		RestResponse restBerkas = callWs(WS_URI+"/getBerkasPengajuan", map, HttpMethod.POST);

		try{
			listBerkasPengajuanDtos = JsonUtil.mapJsonToListObject(restBerkas.getContents(), PlBerkasPengajuanDto.class);
			for(PlBerkasPengajuanDto b : listBerkasPengajuanDtos){
				plBerkasPengajuanDto.setKodeBerkas(b.getKodeBerkas());
				
				for(DasiJrRefCodeDto d : listJenisDokumenDto){
					if(plBerkasPengajuanDto.getKodeBerkas().equalsIgnoreCase(d.getRvLowValue())){
						listJenisDokumenDtoSelected.add(d);
					}
				}
				listKelengkapanBerkas.add(b);
			}
			BindUtils.postNotifyChange(null, null, this, "listJenisDokumenDtoSelected");
			BindUtils.postNotifyChange(null, null, this, "listKelengkapanBerkas");
			BindUtils.postNotifyChange(null, null, this, "tglTerimaBerkas");
		}catch(Exception e){
			e.printStackTrace();
		}				

	}
	
	@Command("checkAll")
	public void checkAllDocs(@ContextParam(ContextType.TRIGGER_EVENT) CheckEvent e){
		if(e.isChecked()){
			listJenisDokumenDtoSelected.addAll(listJenisDokumenDto);
		}else{
			listJenisDokumenDtoSelected.clear();
		}
		
		BindUtils.postNotifyChange(null, null, this, "listJenisDokumenDtoSelected");
	}
	
	public void getKantor(){
		HashMap<String, Object> map = new HashMap<>();
		map.put("search", userSession.getKantor());
		RestResponse listKantor = callWs(WS_URI_LOV + "/getAsalBerkas", map,HttpMethod.POST);
		try {
			listDiajukanDiDto = JsonUtil.mapJsonToListObject(listKantor.getContents(), FndKantorJasaraharjaDto.class);
			for (FndKantorJasaraharjaDto kantor : listDiajukanDiDto) {
				plPengajuanSantunanDto.setKodeNamaKantor(kantor.getNama());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public PlPengajuanSantunanDto getPlPengajuanSantunanDto() {
		return plPengajuanSantunanDto;
	}

	public void setPlPengajuanSantunanDto(
			PlPengajuanSantunanDto plPengajuanSantunanDto) {
		this.plPengajuanSantunanDto = plPengajuanSantunanDto;
	}

	public List<PlPengajuanSantunanDto> getListDto() {
		return listDto;
	}

	public void setListDto(List<PlPengajuanSantunanDto> listDto) {
		this.listDto = listDto;
	}

	public PlPengajuanSantunanDto getSelectedPengajuan() {
		return selectedPengajuan;
	}

	public void setSelectedPengajuan(PlPengajuanSantunanDto selectedPengajuan) {
		this.selectedPengajuan = selectedPengajuan;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDto() {
		return listDataLakaDto;
	}

	public void setListDataLakaDto(List<PlDataKecelakaanDto> listDataLakaDto) {
		this.listDataLakaDto = listDataLakaDto;
	}

	public PlDataKecelakaanDto getPlDataKecelakaanDto() {
		return plDataKecelakaanDto;
	}

	public void setPlDataKecelakaanDto(PlDataKecelakaanDto plDataKecelakaanDto) {
		this.plDataKecelakaanDto = plDataKecelakaanDto;
	}

	public PlDataKecelakaanDto getSelectedKecelakaan() {
		return selectedKecelakaan;
	}

	public void setSelectedKecelakaan(PlDataKecelakaanDto selectedKecelakaan) {
		this.selectedKecelakaan = selectedKecelakaan;
	}

	public DasiJrRefCodeDto getDasiJrRefCodeDto() {
		return dasiJrRefCodeDto;
	}

	public void setDasiJrRefCodeDto(DasiJrRefCodeDto dasiJrRefCodeDto) {
		this.dasiJrRefCodeDto = dasiJrRefCodeDto;
	}

	public List<DasiJrRefCodeDto> getListStatusHubungan() {
		return listStatusHubungan;
	}

	public void setListStatusHubungan(List<DasiJrRefCodeDto> listStatusHubungan) {
		this.listStatusHubungan = listStatusHubungan;
	}

	public List<DasiJrRefCodeDto> getListSifatCidera() {
		return listSifatCidera;
	}

	public void setListSifatCidera(List<DasiJrRefCodeDto> listSifatCidera) {
		this.listSifatCidera = listSifatCidera;
	}

	public List<DasiJrRefCodeDto> getListStatusProses() {
		return listStatusProses;
	}

	public void setListStatusProses(List<DasiJrRefCodeDto> listStatusProses) {
		this.listStatusProses = listStatusProses;
	}

	public List<DasiJrRefCodeDto> getListPenyelesaian() {
		return listPenyelesaian;
	}

	public void setListPenyelesaian(List<DasiJrRefCodeDto> listPenyelesaian) {
		this.listPenyelesaian = listPenyelesaian;
	}

	public List<PlJaminanDto> getListPertanggungan() {
		return listPertanggungan;
	}

	public void setListPertanggungan(List<PlJaminanDto> listPertanggungan) {
		this.listPertanggungan = listPertanggungan;
	}

	public PlJaminanDto getPlJaminanDto() {
		return plJaminanDto;
	}

	public void setPlJaminanDto(PlJaminanDto plJaminanDto) {
		this.plJaminanDto = plJaminanDto;
	}

	public List<PlKorbanKecelakaanDto> getListKorban() {
		return listKorban;
	}

	public void setListKorban(List<PlKorbanKecelakaanDto> listKorban) {
		this.listKorban = listKorban;
	}

	public PlKorbanKecelakaanDto getPlKorbanKecelakaanDto() {
		return plKorbanKecelakaanDto;
	}

	public void setPlKorbanKecelakaanDto(
			PlKorbanKecelakaanDto plKorbanKecelakaanDto) {
		this.plKorbanKecelakaanDto = plKorbanKecelakaanDto;
	}

	public PlKorbanKecelakaanDto getSelectedKorban() {
		return selectedKorban;
	}

	public void setSelectedKorban(PlKorbanKecelakaanDto selectedKorban) {
		this.selectedKorban = selectedKorban;
	}

	public List<PlJaminanDto> getListJaminanDto() {
		return listJaminanDto;
	}

	public void setListjaminanDto(List<PlJaminanDto> listJaminanDto) {
		this.listJaminanDto = listJaminanDto;
	}

	public PlJaminanDto getJaminanDto() {
		return jaminanDto;
	}

	public void setJaminanDto(PlJaminanDto jaminanDto) {
		this.jaminanDto = jaminanDto;
	}

	public List<PlInstansiDto> getListInstansi() {
		return listInstansi;
	}

	public void setListInstansi(List<PlInstansiDto> listInstansi) {
		this.listInstansi = listInstansi;
	}

	public PlInstansiDto getPlInstansiDto() {
		return PlInstansiDto;
	}

	public void setPlInstansiDto(PlInstansiDto plInstansiDto) {
		PlInstansiDto = plInstansiDto;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getWS_URI() {
		return WS_URI;
	}

	public String getWS_URI_LOV() {
		return WS_URI_LOV;
	}

	public boolean isListIndex() {
		return listIndex;
	}

	public void setListIndex(boolean listIndex) {
		this.listIndex = listIndex;
	}

	public FndKantorJasaraharjaDto getFndKantorJasaraharjaDto() {
		return fndKantorJasaraharjaDto;
	}

	public void setFndKantorJasaraharjaDto(
			FndKantorJasaraharjaDto fndKantorJasaraharjaDto) {
		this.fndKantorJasaraharjaDto = fndKantorJasaraharjaDto;
	}

	public List<PlRumahSakitDto> getListRs() {
		return listRs;
	}

	public void setListRs(List<PlRumahSakitDto> listRs) {
		this.listRs = listRs;
	}

	public PlRumahSakitDto getPlRumahSakitDto() {
		return plRumahSakitDto;
	}

	public void setPlRumahSakitDto(PlRumahSakitDto plRumahSakitDto) {
		this.plRumahSakitDto = plRumahSakitDto;
	}

	public List<DasiJrRefCodeDto> getListJenisIdentitasDto() {
		return listJenisIdentitasDto;
	}

	public void setListJenisIdentitasDto(
			List<DasiJrRefCodeDto> listJenisIdentitasDto) {
		this.listJenisIdentitasDto = listJenisIdentitasDto;
	}

	public Form getFormMaster() {
		return formMaster;
	}

	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}

	public Form getFormDetail() {
		return formDetail;
	}

	public void setFormDetail(Form formDetail) {
		this.formDetail = formDetail;
	}

	public List<FndCamatDto> getListProvinsiDto() {
		return listProvinsiDto;
	}

	public void setListProvinsiDto(List<FndCamatDto> listProvinsiDto) {
		this.listProvinsiDto = listProvinsiDto;
	}

	public List<FndCamatDto> getListKabkotaDto() {
		return listKabkotaDto;
	}

	public void setListKabkotaDto(List<FndCamatDto> listKabkotaDto) {
		this.listKabkotaDto = listKabkotaDto;
	}

	public List<FndCamatDto> getListCamatDto() {
		return listCamatDto;
	}

	public void setListCamatDto(List<FndCamatDto> listCamatDto) {
		this.listCamatDto = listCamatDto;
	}

	public DasiJrRefCodeDto getSifatCideraDto() {
		return sifatCideraDto;
	}

	public void setSifatCideraDto(DasiJrRefCodeDto sifatCideraDto) {
		this.sifatCideraDto = sifatCideraDto;
	}

	public FndCamatDto getProvinsiDto() {
		return provinsiDto;
	}

	public void setProvinsiDto(FndCamatDto provinsiDto) {
		this.provinsiDto = provinsiDto;
	}

	public FndCamatDto getKabKotaDto() {
		return kabKotaDto;
	}

	public void setKabKotaDto(FndCamatDto kabKotaDto) {
		this.kabKotaDto = kabKotaDto;
	}

	public String getSearchProvinsi() {
		return searchProvinsi;
	}

	public void setSearchProvinsi(String searchProvinsi) {
		this.searchProvinsi = searchProvinsi;
	}

	public String getSearchCamat() {
		return searchCamat;
	}

	public void setSearchCamat(String searchCamat) {
		this.searchCamat = searchCamat;
	}

	public String getSearchKabkota() {
		return searchKabkota;
	}

	public void setSearchKabkota(String searchKabkota) {
		this.searchKabkota = searchKabkota;
	}

	public FndCamatDto getCamatDto() {
		return camatDto;
	}

	public void setCamatDto(FndCamatDto camatDto) {
		this.camatDto = camatDto;
	}

	public DasiJrRefCodeDto getStatusHubunganDto() {
		return statusHubunganDto;
	}

	public void setStatusHubunganDto(DasiJrRefCodeDto statusHubunganDto) {
		this.statusHubunganDto = statusHubunganDto;
	}

	public DasiJrRefCodeDto getIdentitasDto() {
		return identitasDto;
	}

	public void setIdentitasDto(DasiJrRefCodeDto identitasDto) {
		this.identitasDto = identitasDto;
	}

	public DasiJrRefCodeDto getStatusProsesDto() {
		return statusProsesDto;
	}

	public void setStatusProsesDto(DasiJrRefCodeDto statusProsesDto) {
		this.statusProsesDto = statusProsesDto;
	}

	public DasiJrRefCodeDto getStatusPenyelesainDto() {
		return statusPenyelesainDto;
	}

	public void setStatusPenyelesainDto(DasiJrRefCodeDto statusPenyelesainDto) {
		this.statusPenyelesainDto = statusPenyelesainDto;
	}

	public List<DasiJrRefCodeDto> getListJenisPembayaran() {
		return listJenisPembayaran;
	}

	public void setListJenisPembayaran(
			List<DasiJrRefCodeDto> listJenisPembayaran) {
		this.listJenisPembayaran = listJenisPembayaran;
	}

	public DasiJrRefCodeDto getJenisPembayaranDto() {
		return jenisPembayaranDto;
	}

	public void setJenisPembayaranDto(DasiJrRefCodeDto jenisPembayaranDto) {
		this.jenisPembayaranDto = jenisPembayaranDto;
	}

	public UserSessionJR getUserSession() {
		return userSession;
	}

	public void setUserSession(UserSessionJR userSession) {
		this.userSession = userSession;
	}

	public List<PlDataKecelakaanDto> getListPlDataKecelakaanDtos() {
		return listPlDataKecelakaanDtos;
	}

	public void setListPlDataKecelakaanDtos(
			List<PlDataKecelakaanDto> listPlDataKecelakaanDtos) {
		this.listPlDataKecelakaanDtos = listPlDataKecelakaanDtos;
	}

	public List<DasiJrRefCodeDto> getListJenisDokumenDto() {
		return listJenisDokumenDto;
	}

	public void setListJenisDokumenDto(
			List<DasiJrRefCodeDto> listJenisDokumenDto) {
		this.listJenisDokumenDto = listJenisDokumenDto;
	}

	public DasiJrRefCodeDto getJenisDokumenDto() {
		return jenisDokumenDto;
	}

	public void setJenisDokumenDto(DasiJrRefCodeDto jenisDokumenDto) {
		this.jenisDokumenDto = jenisDokumenDto;
	}

	public Date getTglPenerimaan() {
		return tglPenerimaan;
	}

	public void setTglPenerimaan(Date tglPenerimaan) {
		this.tglPenerimaan = tglPenerimaan;
	}

	public Date getJamPenerimaan() {
		return jamPenerimaan;
	}

	public void setJamPenerimaan(Date jamPenerimaan) {
		this.jamPenerimaan = jamPenerimaan;
	}

	public List<PlInstansiDto> getListInstansiDto() {
		return listInstansiDto;
	}

	public void setListInstansiDto(List<PlInstansiDto> listInstansiDto) {
		this.listInstansiDto = listInstansiDto;
	}

	public String getSearchInstansi() {
		return searchInstansi;
	}

	public void setSearchInstansi(String searchInstansi) {
		this.searchInstansi = searchInstansi;
	}

	public PlInstansiDto getInstansiDto() {
		return instansiDto;
	}

	public void setInstansiDto(PlInstansiDto instansiDto) {
		this.instansiDto = instansiDto;
	}

	public String getNoLaporan() {
		return noLaporan;
	}

	public void setNoLaporan(String noLaporan) {
		this.noLaporan = noLaporan;
	}

	public boolean isFormDataPemohon() {
		return formDataPemohon;
	}

	public void setFormDataPemohon(boolean formDataPemohon) {
		this.formDataPemohon = formDataPemohon;
	}

	public boolean isFormDataPengajuan() {
		return formDataPengajuan;
	}

	public void setFormDataPengajuan(boolean formDataPengajuan) {
		this.formDataPengajuan = formDataPengajuan;
	}

	public String getPengajuanDay() {
		return pengajuanDay;
	}

	public void setPengajuanDay(String pengajuanDay) {
		this.pengajuanDay = pengajuanDay;
	}

	public String getPengajuanMonth() {
		return pengajuanMonth;
	}

	public void setPengajuanMonth(String pengajuanMonth) {
		this.pengajuanMonth = pengajuanMonth;
	}

	public String getPengajuanYear() {
		return pengajuanYear;
	}

	public void setPengajuanYear(String pengajuanYear) {
		this.pengajuanYear = pengajuanYear;
	}

	public String getLakaDay() {
		return lakaDay;
	}

	public void setLakaDay(String lakaDay) {
		this.lakaDay = lakaDay;
	}

	public String getLakaMonth() {
		return lakaMonth;
	}

	public void setLakaMonth(String lakaMonth) {
		this.lakaMonth = lakaMonth;
	}

	public String getLakaYear() {
		return lakaYear;
	}

	public void setLakaYear(String lakaYear) {
		this.lakaYear = lakaYear;
	}

	public String getNamaPemohon() {
		return namaPemohon;
	}

	public void setNamaPemohon(String namaPemohon) {
		this.namaPemohon = namaPemohon;
	}

	public String getNamaKorban() {
		return namaKorban;
	}

	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public List<String> getListMonthPengajuan() {
		return listMonthPengajuan;
	}

	public void setListMonthPengajuan(List<String> listMonthPengajuan) {
		this.listMonthPengajuan = listMonthPengajuan;
	}

	public List<String> getListMonthLaka() {
		return listMonthLaka;
	}

	public void setListMonthLaka(List<String> listMonthLaka) {
		this.listMonthLaka = listMonthLaka;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public PlAdditionalDescDto getPlAdditionalDescDto() {
		return plAdditionalDescDto;
	}

	public void setPlAdditionalDescDto(PlAdditionalDescDto plAdditionalDescDto) {
		this.plAdditionalDescDto = plAdditionalDescDto;
	}

	public String getCideraKode() {
		return cideraKode;
	}

	public void setCideraKode(String cideraKode) {
		this.cideraKode = cideraKode;
	}

	public String getCideraKode2() {
		return cideraKode2;
	}

	public void setCideraKode2(String cideraKode2) {
		this.cideraKode2 = cideraKode2;
	}

	public Date getTglKejadian() {
		return tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public Date getJamKejadian() {
		return jamKejadian;
	}

	public void setJamKejadian(Date jamKejadian) {
		this.jamKejadian = jamKejadian;
	}

	public String getStatusLaporan() {
		return statusLaporan;
	}

	public void setStatusLaporan(String statusLaporan) {
		this.statusLaporan = statusLaporan;
	}

	public String getLaporan1() {
		return laporan1;
	}

	public void setLaporan1(String laporan1) {
		this.laporan1 = laporan1;
	}

	public String getLaporan2() {
		return laporan2;
	}

	public void setLaporan2(String laporan2) {
		this.laporan2 = laporan2;
	}

	public String getLaporan3() {
		return laporan3;
	}

	public void setLaporan3(String laporan3) {
		this.laporan3 = laporan3;
	}

	public String getSearchDiajukanDi() {
		return searchDiajukanDi;
	}

	public void setSearchDiajukanDi(String searchDiajukanDi) {
		this.searchDiajukanDi = searchDiajukanDi;
	}

	public String getSearchKantorDiajukan() {
		return searchKantorDiajukan;
	}

	public void setSearchKantorDiajukan(String searchKantorDiajukan) {
		this.searchKantorDiajukan = searchKantorDiajukan;
	}

	public String getSearchStatusProses() {
		return searchStatusProses;
	}

	public void setSearchStatusProses(String searchStatusProses) {
		this.searchStatusProses = searchStatusProses;
	}

	public String getSearchStatusPenyelesaian() {
		return searchStatusPenyelesaian;
	}

	public void setSearchStatusPenyelesaian(String searchStatusPenyelesaian) {
		this.searchStatusPenyelesaian = searchStatusPenyelesaian;
	}

	public String getSearchRs() {
		return searchRs;
	}

	public void setSearchRs(String searchRs) {
		this.searchRs = searchRs;
	}

	public List<FndKantorJasaraharjaDto> getListKantorDto() {
		return listKantorDto;
	}

	public void setListKantorDto(List<FndKantorJasaraharjaDto> listKantorDto) {
		this.listKantorDto = listKantorDto;
	}

	public List<LampiranDto> getLampirans() {
		return lampirans;
	}

	public void setLampirans(List<LampiranDto> lampirans) {
		this.lampirans = lampirans;
	}

	public List<PlDisposisiDto> getListDisposisi() {
		return listDisposisi;
	}

	public void setListDisposisi(List<PlDisposisiDto> listDisposisi) {
		this.listDisposisi = listDisposisi;
	}

	public PlDisposisiDto getPlDisposisiDto() {
		return plDisposisiDto;
	}

	public void setPlDisposisiDto(PlDisposisiDto plDisposisiDto) {
		this.plDisposisiDto = plDisposisiDto;
	}

	public boolean isShowListDisposisi() {
		return showListDisposisi;
	}

	public void setShowListDisposisi(boolean showListDisposisi) {
		this.showListDisposisi = showListDisposisi;
	}

	public boolean isShowFormDisposisi() {
		return showFormDisposisi;
	}

	public void setShowFormDisposisi(boolean showFormDisposisi) {
		this.showFormDisposisi = showFormDisposisi;
	}

	public boolean isFlagSelectAllDokumen() {
		return flagSelectAllDokumen;
	}

	public void setFlagSelectAllDokumen(boolean flagSelectAllDokumen) {
		this.flagSelectAllDokumen = flagSelectAllDokumen;
	}

	public boolean isCheckedSave() {
		return checkedSave;
	}

	public void setCheckedSave(boolean checkedSave) {
		this.checkedSave = checkedSave;
	}

	public Date getTglDisposisi() {
		return tglDisposisi;
	}

	public void setTglDisposisi(Date tglDisposisi) {
		this.tglDisposisi = tglDisposisi;
	}

	public Date getJamDisposisi() {
		return jamDisposisi;
	}

	public void setJamDisposisi(Date jamDisposisi) {
		this.jamDisposisi = jamDisposisi;
	}

	public String getPengajuanMonthStr() {
		return pengajuanMonthStr;
	}

	public void setPengajuanMonthStr(String pengajuanMonthStr) {
		this.pengajuanMonthStr = pengajuanMonthStr;
	}

	public List<String> getListJenisBerkas() {
		return listJenisBerkas;
	}

	public void setListJenisBerkas(List<String> listJenisBerkas) {
		this.listJenisBerkas = listJenisBerkas;
	}

	public String getPilihJenisBerkas() {
		return pilihJenisBerkas;
	}

	public void setPilihJenisBerkas(String pilihJenisBerkas) {
		this.pilihJenisBerkas = pilihJenisBerkas;
	}

	public String getDilimpahkanKe() {
		return dilimpahkanKe;
	}

	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}

	public List<DasiJrRefCodeDto> getListOtorisasiFlag() {
		return listOtorisasiFlag;
	}

	public void setListOtorisasiFlag(List<DasiJrRefCodeDto> listOtorisasiFlag) {
		this.listOtorisasiFlag = listOtorisasiFlag;
	}

	public boolean isDisableDisposisi() {
		return disableDisposisi;
	}

	public void setDisableDisposisi(boolean disableDisposisi) {
		this.disableDisposisi = disableDisposisi;
	}

	public List<PlPenyelesaianSantunanDto> getListPlPenyelesaianSantunanDto() {
		return listPlPenyelesaianSantunanDto;
	}

	public void setListPlPenyelesaianSantunanDto(
			List<PlPenyelesaianSantunanDto> listPlPenyelesaianSantunanDto) {
		this.listPlPenyelesaianSantunanDto = listPlPenyelesaianSantunanDto;
	}

	public PlPenyelesaianSantunanDto getPlPenyelesaianSantunanDto() {
		return plPenyelesaianSantunanDto;
	}

	public void setPlPenyelesaianSantunanDto(
			PlPenyelesaianSantunanDto plPenyelesaianSantunanDto) {
		this.plPenyelesaianSantunanDto = plPenyelesaianSantunanDto;
	}

	public void setBiaya1(BigDecimal biaya1) {
		this.biaya1 = biaya1;
	}

	public void setBiaya2(BigDecimal biaya2) {
		this.biaya2 = biaya2;
	}

	public BigDecimal getBiaya1() {
		return biaya1;
	}

	public BigDecimal getBiaya2() {
		return biaya2;
	}

	public boolean isDisableProsesSelanjutnya() {
		return disableProsesSelanjutnya;
	}

	public void setDisableProsesSelanjutnya(boolean disableProsesSelanjutnya) {
		this.disableProsesSelanjutnya = disableProsesSelanjutnya;
	}

	public boolean isDisableSave() {
		return disableSave;
	}

	public void setDisableSave(boolean disableSave) {
		this.disableSave = disableSave;
	}

	public PlRegisterSementaraDto getPlRegisterSementaraDto() {
		return plRegisterSementaraDto;
	}

	public void setPlRegisterSementaraDto(
			PlRegisterSementaraDto plRegisterSementaraDto) {
		this.plRegisterSementaraDto = plRegisterSementaraDto;
	}

	public List<PlRegisterSementaraDto> getListRegisterSementaraDtos() {
		return listRegisterSementaraDtos;
	}

	public void setListRegisterSementaraDtos(
			List<PlRegisterSementaraDto> listRegisterSementaraDtos) {
		this.listRegisterSementaraDtos = listRegisterSementaraDtos;
	}

	public List<PlPengajuanSantunanDto> getListDtoCopy() {
		return listDtoCopy;
	}

	public void setListDtoCopy(List<PlPengajuanSantunanDto> listDtoCopy) {
		this.listDtoCopy = listDtoCopy;
	}

	public String getLakaMonthStr() {
		return lakaMonthStr;
	}

	public void setLakaMonthStr(String lakaMonthStr) {
		this.lakaMonthStr = lakaMonthStr;
	}

	public boolean isModeDisableBiaya2() {
		return modeDisableBiaya2;
	}

	public void setModeDisableBiaya2(boolean modeDisableBiaya2) {
		this.modeDisableBiaya2 = modeDisableBiaya2;
	}

	public List<PlJaminanDto> getListJenisPertanggunganDto() {
		return listJenisPertanggunganDto;
	}

	public void setListJenisPertanggunganDto(
			List<PlJaminanDto> listJenisPertanggunganDto) {
		this.listJenisPertanggunganDto = listJenisPertanggunganDto;
	}

	public PlJaminanDto getJenisPertanggunganDto() {
		return jenisPertanggunganDto;
	}

	public void setJenisPertanggunganDto(PlJaminanDto jenisPertanggunganDto) {
		this.jenisPertanggunganDto = jenisPertanggunganDto;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDtoCopy() {
		return listDataLakaDtoCopy;
	}

	public void setListDataLakaDtoCopy(
			List<PlDataKecelakaanDto> listDataLakaDtoCopy) {
		this.listDataLakaDtoCopy = listDataLakaDtoCopy;
	}

	public PlDataKecelakaanDto getDataLakaDto() {
		return dataLakaDto;
	}

	public void setDataLakaDto(PlDataKecelakaanDto dataLakaDto) {
		this.dataLakaDto = dataLakaDto;
	}

	public Date getDetailKejadianStartDate() {
		return detailKejadianStartDate;
	}

	public void setDetailKejadianStartDate(Date detailKejadianStartDate) {
		this.detailKejadianStartDate = detailKejadianStartDate;
	}

	public Date getDetailKejadianEndDate() {
		return detailKejadianEndDate;
	}

	public void setDetailKejadianEndDate(Date detailKejadianEndDate) {
		this.detailKejadianEndDate = detailKejadianEndDate;
	}

	public String getDetailInstansi() {
		return detailInstansi;
	}

	public void setDetailInstansi(String detailInstansi) {
		this.detailInstansi = detailInstansi;
	}

	public Date getDetailLaporanStartDate() {
		return detailLaporanStartDate;
	}

	public void setDetailLaporanStartDate(Date detailLaporanStartDate) {
		this.detailLaporanStartDate = detailLaporanStartDate;
	}

	public Date getDetailLaporanEndDate() {
		return detailLaporanEndDate;
	}

	public void setDetailLaporanEndDate(Date detailLaporanEndDate) {
		this.detailLaporanEndDate = detailLaporanEndDate;
	}

	public String getDetailNoLaporanPolisi() {
		return detailNoLaporanPolisi;
	}

	public void setDetailNoLaporanPolisi(String detailNoLaporanPolisi) {
		this.detailNoLaporanPolisi = detailNoLaporanPolisi;
	}

	public String getDetailNamaKorban() {
		return detailNamaKorban;
	}

	public void setDetailNamaKorban(String detailNamaKorban) {
		this.detailNamaKorban = detailNamaKorban;
	}

	public List<String> getListStatusLp() {
		return listStatusLp;
	}

	public void setListStatusLp(List<String> listStatusLp) {
		this.listStatusLp = listStatusLp;
	}

	public String getStatusLP() {
		return statusLP;
	}

	public void setStatusLP(String statusLP) {
		this.statusLP = statusLP;
	}

	public String getValidasiIdentitas() {
		return validasiIdentitas;
	}

	public void setValidasiIdentitas(String validasiIdentitas) {
		this.validasiIdentitas = validasiIdentitas;
	}

	public String getStyleValidasiIdentitas() {
		return styleValidasiIdentitas;
	}

	public void setStyleValidasiIdentitas(String styleValidasiIdentitas) {
		this.styleValidasiIdentitas = styleValidasiIdentitas;
	}

	public List<PlAdditionalDescDto> getListPlAdditionalDescDto() {
		return listPlAdditionalDescDto;
	}

	public void setListPlAdditionalDescDto(
			List<PlAdditionalDescDto> listPlAdditionalDescDto) {
		this.listPlAdditionalDescDto = listPlAdditionalDescDto;
	}

	public List<PlPengajuanRsDto> getListPlPengajuanRsDto() {
		return listPlPengajuanRsDto;
	}

	public void setListPlPengajuanRsDto(List<PlPengajuanRsDto> listPlPengajuanRsDto) {
		this.listPlPengajuanRsDto = listPlPengajuanRsDto;
	}

	public List<FndCamatDto> getFndCamatDtos() {
		return fndCamatDtos;
	}

	public void setFndCamatDtos(List<FndCamatDto> fndCamatDtos) {
		this.fndCamatDtos = fndCamatDtos;
	}

	public DukcapilWinDto getDukcapilWinDto() {
		return dukcapilWinDto;
	}

	public void setDukcapilWinDto(DukcapilWinDto dukcapilWinDto) {
		this.dukcapilWinDto = dukcapilWinDto;
	}

	public boolean isJaminanGandaFlag() {
		return jaminanGandaFlag;
	}

	public void setJaminanGandaFlag(boolean jaminanGandaFlag) {
		this.jaminanGandaFlag = jaminanGandaFlag;
	}

	public List<PlBerkasPengajuanDto> getListBerkasPengajuanDtos() {
		return listBerkasPengajuanDtos;
	}

	public void setListBerkasPengajuanDtos(
			List<PlBerkasPengajuanDto> listBerkasPengajuanDtos) {
		this.listBerkasPengajuanDtos = listBerkasPengajuanDtos;
	}
	
	public boolean isCheckDeskripsi() {
		return checkDeskripsi;
	}

	public void setCheckDeskripsi(boolean checkDeskripsi) {
		this.checkDeskripsi = checkDeskripsi;
	}

	public PlBerkasPengajuanDto getPlBerkasPengajuanDto() {
		return plBerkasPengajuanDto;
	}

	public void setPlBerkasPengajuanDto(PlBerkasPengajuanDto plBerkasPengajuanDto) {
		this.plBerkasPengajuanDto = plBerkasPengajuanDto;
	}

	public Date getTglTerimaBerkas() {
		return tglTerimaBerkas;
	}

	public void setTglTerimaBerkas(Date tglTerimaBerkas) {
		this.tglTerimaBerkas = tglTerimaBerkas;
	}

	public List<DasiJrRefCodeDto> getListJenisDokumenDtoSelected() {
		return listJenisDokumenDtoSelected;
	}

	public void setListJenisDokumenDtoSelected(
			List<DasiJrRefCodeDto> listJenisDokumenDtoSelected) {
		this.listJenisDokumenDtoSelected = listJenisDokumenDtoSelected;
	}

	public List<PlBerkasPengajuanDto> getListKelengkapanBerkas() {
		return listKelengkapanBerkas;
	}

	public void setListKelengkapanBerkas(
			List<PlBerkasPengajuanDto> listKelengkapanBerkas) {
		this.listKelengkapanBerkas = listKelengkapanBerkas;
	}

	public boolean isSaveSantunanFlag() {
		return saveSantunanFlag;
	}

	public void setSaveSantunanFlag(boolean saveSantunanFlag) {
		this.saveSantunanFlag = saveSantunanFlag;
	}

	public boolean isCekKtpButton() {
		return cekKtpButton;
	}

	public void setCekKtpButton(boolean cekKtpButton) {
		this.cekKtpButton = cekKtpButton;
	}
}
