package ui.operasional;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.CheckEvent;
import org.zkoss.zul.Window;

import Bpm.ListTaskByUser.UserTask;
import share.DasiJrRefCodeDto;
import share.GeneralDto;
import share.LampiranDto;
import share.PlBerkasPengajuanDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import ui.operasional.dokumen.CetakLdpb;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass = true)
public class OpIdentifikasiKeabsahanBerkasVmd extends BaseVmd implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpIndentifikasiKeabsahanBerkas/_index.zul";
	private PlPengajuanSantunanDto pengajuanSantunan;
	private List<PlPengajuanSantunanDto> listPengajuan;
	private final String WS_URI = "/AbsahBerkas";
	private final String WS_URI_LOV = "/Lov";
	private final String WS_URI_PENGAJUAN_SANTUNAN = "/OpPengajuanSantunan";

	private Date tglPenerimaan;
	private List<GeneralDto> optionPengajuan = new ArrayList<>();
	private GeneralDto pilihanPengajuan;
	private String noBerkas;
	private String filter = null;
	private UserTask userTask;
	
//	for search and filter
	private String searchIndex; 
	private String pilihPengajuan;
	private int pageSize = 5;

	
	private List<PlPengajuanSantunanDto> listIndex = new ArrayList<>();
	private List<PlPengajuanSantunanDto> listIndexCopy = new ArrayList<>();
	private List<String> listPilihanPengajuan = new ArrayList<>();
	private boolean listIndexWindow = false;
	private PlPengajuanSantunanDto pengajuanSantunanDto = new PlPengajuanSantunanDto();
	private List<PlPengajuanSantunanDto> pengajuanSantunanDtos = new ArrayList<>();
	private List<PlBerkasPengajuanDto> plBerkasPengajuanDtos = new ArrayList<>();
	private List<PlBerkasPengajuanDto> plBerkasPengajuanDtosSelected = new ArrayList<>();
	private PlBerkasPengajuanDto plBerkasPengajuanDto = new PlBerkasPengajuanDto();
	private PlPenyelesaianSantunanDto penyelesaianSantunanDto = new PlPenyelesaianSantunanDto();
	
	//pengajuan rupiah
	private String cideraKode;
	private String cideraKode2;
	private BigDecimal biaya1;
	private BigDecimal biaya2;
	private BigDecimal biayaAmbulance;
	private BigDecimal biayaP3k;
	private double biayaTotal;
	private DasiJrRefCodeDto sifatCideraDto = new DasiJrRefCodeDto();
	private List<DasiJrRefCodeDto> listSifatCidera = new ArrayList<>(); 
	
	//akumulasi 
	private double akmMeninggal;
	private double akmLuka;
	private double akmCacat;
	private double akmPenguburan;
	private double akmAmbulance;
	private double akmP3k;
	//total
	private double totalMeninggal;
	private double totalLuka;
	private double totalCacat;
	private double totalPenguburan;
	private double totalAmbulance;
	private double totalP3k;
	
	private List<LampiranDto> listDocs = new ArrayList<>();

	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent()
			.getAttribute(UIConstants.SESS_LOGIN_ID);

	public UserTask getUserTask() {
		return userTask;
	}
	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}
	public void loadList(){
		listPilihanPengajuan();
		createOptionPengajuan();
		UserTask ut = (UserTask) Executions.getCurrent().getAttribute("obj2");
		//System.out.println("USERRRR TASKKKKKK : " + ut.getTaskTitle());
		if (ut != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			setNoBerkas(ut.getNomorPermohonan());
			Date tgl = null;
			userTask = ut;
			try {
				tgl = sdf.parse(ut.getCreationDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			setTglPenerimaan(tgl);
			//search list no berkas
			RestResponse rest = new RestResponse();
			Map<String, Object> input = new HashMap<>();
			String tglTerima = "";
			try{
				tglTerima = dateToString(tglPenerimaan);
				System.out.println("----------------------------");
				System.out.println(tglTerima);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			try{
				input.put("option", pilihanPengajuan);
				input.put("tglDiterima", tglTerima);
				input.put("noBerkas", noBerkas);
				input.put("pilihan", pilihanPengajuan);
				if(filter==null || filter.isEmpty()){
					filter="ALL";
				}
				if(filter!=null || !filter.isEmpty()){
					input.put("filter", filter);
				}
				
				rest = callWs(WS_URI+"/search", input, HttpMethod.POST);
				Map<String, Object> out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
				
				listPengajuan = JsonUtil.mapJsonToListObject(out.get("listDto"), PlPengajuanSantunanDto.class);
				BindUtils.postNotifyChange(null, null, this, "listPengajuan");
				
			}catch(Exception e){
				e.printStackTrace();
			}
		} else {
			setNoBerkas(null);
			setTglPenerimaan(null);
		}
	}
	@Command
	public void search(){
		RestResponse rest = new RestResponse();
		Map<String, Object> input = new HashMap<>();
		String tglTerima = "";
		try{
			tglTerima = dateToString(tglPenerimaan);
			System.out.println("----------------------------");
			System.out.println(tglTerima);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			input.put("option", pilihanPengajuan.getKode());
			input.put("tglDiterima", tglTerima);
			input.put("noBerkas", noBerkas);
			if(filter==null || filter.isEmpty()){
				filter="ALL";
			}
			if(filter!=null || !filter.isEmpty()){
				input.put("filter", filter);
			}
			
			rest = callWs(WS_URI+"/search", input, HttpMethod.POST);
			Map<String, Object> out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			
			listPengajuan = JsonUtil.mapJsonToListObject(out.get("listDto"), PlPengajuanSantunanDto.class);
			BindUtils.postNotifyChange(null, null, this, "listPengajuan");
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	public void filteredSearch(@BindingParam("item")String filter){
		if(filter != null){
			this.filter = filter;
			search();
		}
	}
	
//	@Command
//	public void openDetail(@BindingParam("popup") String popup, @BindingParam("item") PlPengajuanSantunanDto dto,
//			@Default("popUpHandler") @BindingParam("detailHandler") String globalHandleMethodName){
//		Map<String, Object> args = new HashMap<>();
//		if(dto!=null){
//			this.pengajuanSantunan = dto;
//		}
//		//System.out.println("USERRRR TASKKKKK OPENNNN DETAILLLLL : "+ userTask.getTaskTitle());
//		args.put("dto", dto);
//		args.put("detailHandler", globalHandleMethodName);
//		args.put("obj2", userTask);
//		
//		if (!beforeDetail(args, popup))
//			return;
//		try {
//			//navigate(UIConstants.BASE_PAGE_PATH + "/operasionalDetail/DataPengajuanSantunan.zul");navigate(UIConstants.BASE_PAGE_PATH + "/operasionalDetail/DataPengajuanSantunan.zul");
//			///operasional/OpRegisterSementara/_index.zul
//			System.out.println(popup);
//			userTask = (UserTask) Executions.getCurrent().getAttribute("obj2");
//			Executions.getCurrent().setAttribute("obj2", userTask);
//			((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
//			BindUtils.postGlobalCommand(null, null, "getDocsAbsah", args);
//		} catch (UiException u) {
//			u.printStackTrace();
//		}
//		
//	}
	
//	protected boolean beforeDetail(Map<String, Object> args, String popup){
//		args.put("dto",pengajuanSantunan );
//		return true;
//	}
	
//	@GlobalCommand("absahHandler")
//	public void titleHandler(@BindingParam("dto") PlPengajuanSantunanDto dto) {
//		if(pengajuanSantunan != null){
//			this.pengajuanSantunan = dto;
//		}
//	}
	
	private void createOptionPengajuan(){
		GeneralDto dt = new GeneralDto();
		dt.setKode("1");
		dt.setName("Pengajuan Untuk Diproses");
		optionPengajuan.add(dt);
		pilihanPengajuan = dt;
		dt = new GeneralDto();
		dt.setKode("2");
		dt.setName("Pengajuan Seluruhnya");
		optionPengajuan.add(dt);
		BindUtils.postNotifyChange(null, null, this, "optionPengajuan");
		BindUtils.postNotifyChange(null, null, this, "pilihanPengajuan");
	}
	
	protected boolean beforePopupRingkasan(Map<String, Object> args, String popup) {
		args.put("plPengajuanSantunanDto", pengajuanSantunan);
		return true;
	}
	
	@Command("viewResume")
	public void showPopupRingkasan(
			@BindingParam("popup") String popup, @BindingParam("item") PlPengajuanSantunanDto plPengajuanSantunanDto,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();
		
		args.put("popUpHandler", globalHandleMethodName);
		args.put("pengajuanSantunan", plPengajuanSantunanDto);
		
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	@Command("cetakDisposisi")
	public void cetakDisposisi(@BindingParam("item") PlPengajuanSantunanDto selected)
			throws IOException {
		if (pengajuanSantunan == null
				|| pengajuanSantunan.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}		
		CetakLdpb cetak = new CetakLdpb();
		cetak.cetakDisposisi(selected);
	}
	
	@Command
	public void searchIndex(){
		
		if (getSearchIndex() == null || getSearchIndex().equalsIgnoreCase("")) {
			setSearchIndex("%%");
		}
		if (getPilihPengajuan().contains("Diproses")) {
			setPilihPengajuan("BL");
		}

		tglPenerimaan = tglPenerimaan==null?null:tglPenerimaan;
		noBerkas = getNoBerkas()==null?"":getNoBerkas();
		
		System.err.println("luthfi80 "+pilihPengajuan);
		HashMap<String, Object> filter = new HashMap<>();
		filter.put("pilihPengajuan", pilihPengajuan);
		if(tglPenerimaan==null){
			filter.put("tglPenerimaan", null);
		}else{
			filter.put("tglPenerimaan", dateToString(tglPenerimaan));			
		}
		filter.put("noBerkas", noBerkas);
		filter.put("search", searchIndex);
		RestResponse rest = callWs(WS_URI+"/all", filter, HttpMethod.POST);
		try{
			listIndexCopy.clear();
			listIndex = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
			listIndexCopy = new ArrayList<>();
			listIndexCopy.addAll(listIndex);
			setListIndexWindow(true);
			for(PlPengajuanSantunanDto a : listIndex){
				pengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
				pengajuanSantunanDto.setIdKecelakaan(a.getIdKecelakaan());
			}
			BindUtils.postNotifyChange(null, null, this, "listIndex");
			BindUtils.postNotifyChange(null, null, this, "listIndexCopy");
			BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
			BindUtils.postNotifyChange(null, null, this, "pengajuanSantunanDto");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void listPilihanPengajuan(){
		setPilihPengajuan("Pengajuan Untuk Diproses");
		listPilihanPengajuan.add("Pengajuan Untuk Diproses");
		listPilihanPengajuan.add("Pengajuan Seluruhnya");
		BindUtils.postNotifyChange(null, null, this, "listPilihanPengajuan");
		BindUtils.postNotifyChange(null, null, this, "pilihPengajuan");
	}
	
	
	@Command
	public void prosesKeabsahan(@BindingParam("popup") String popup, 
			@BindingParam("item")PlPengajuanSantunanDto selectedDto){

		Executions.getCurrent().setAttribute("obj", selectedDto);
		getPageInfo().setEditMode(true);
		Map<String, Object> args = new HashMap<>();
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	public void onEdit(){
		pengajuanSantunanDto = (PlPengajuanSantunanDto) Executions.getCurrent().getAttribute("obj");

//		===========================================GET LIST DOKUMEN==============================================
		Map<String,Object> mapInput = new HashMap<>();
		mapInput.put("noBerkas", pengajuanSantunanDto.getNoBerkas());
		RestResponse listDok = callWs(WS_URI_PENGAJUAN_SANTUNAN+"/getBerkasPengajuan", mapInput, HttpMethod.POST);
		System.err.println("luthfi88 "+pengajuanSantunanDto.getNoBerkas());
		try{
			plBerkasPengajuanDtos = JsonUtil.mapJsonToListObject(listDok.getContents(), PlBerkasPengajuanDto.class);
			for(PlBerkasPengajuanDto b : plBerkasPengajuanDtos){
				plBerkasPengajuanDto = b;
			}
			BindUtils.postNotifyChange(null, null, this, "plBerkasPengajuanDtos");
			BindUtils.postNotifyChange(null, null, this, "plBerkasPengajuanDto");
		}catch(Exception e){
			e.printStackTrace();
		}
		
//		==========================================GET PEMBAYARAN================================================
		Map<String,Object> mapInput2 = new HashMap<>();
		mapInput2.put("noBerkas", pengajuanSantunanDto.getNoBerkas());
		RestResponse restPengajuanSantunan = callWs(WS_URI_PENGAJUAN_SANTUNAN+"/findByNoBerkas", mapInput2, HttpMethod.POST);
		try{
			pengajuanSantunanDtos = JsonUtil.mapJsonToListObject(restPengajuanSantunan.getContents(), PlPengajuanSantunanDto.class);
			for(PlPengajuanSantunanDto dto : pengajuanSantunanDtos){
				setBiaya1(dto.getJumlahPengajuanMeninggal()==null?new BigDecimal(0):dto.getJumlahPengajuanMeninggal());
				setBiaya2(dto.getJumlahPengajuanLukaluka()==null?new BigDecimal(0):dto.getJumlahPengajuanLukaluka());
				setBiayaAmbulance(dto.getJmlPengajuanAmbl()==null?new BigDecimal(0):dto.getJmlPengajuanAmbl());
				setBiayaP3k(dto.getJmlPengajuanP3k()==null?new BigDecimal(0):dto.getJmlPengajuanP3k());
				sifatCideraDto.setRvLowValue(dto.getCideraKorban());
				int i = getBiaya1().intValue()+getBiaya2().intValue()+getBiayaAmbulance().intValue()+getBiayaP3k().intValue();
				setBiayaTotal(i);
				setTotalAmbulance(dto.getJmlPengajuanAmbl()==null?0:dto.getJmlPengajuanAmbl().doubleValue());
				setTotalP3k(dto.getJmlPengajuanP3k()==null?0:dto.getJmlPengajuanP3k().doubleValue());
				BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		sifatCidera();
		kodeCidera();

		BindUtils.postNotifyChange(null, null, this, "pengajuanSantunanDto");
		BindUtils.postNotifyChange(null, null, this, "biaya1");
		BindUtils.postNotifyChange(null, null, this, "biaya2");
		BindUtils.postNotifyChange(null, null, this, "biayaAmbulance");
		BindUtils.postNotifyChange(null, null, this, "biayaP3k");
		BindUtils.postNotifyChange(null, null, this, "biayaTotal");
		BindUtils.postNotifyChange(null, null, this, "totalAmbulance");
		BindUtils.postNotifyChange(null, null, this, "totalP3k");
	}
	
	@Command("checkAll")
	public void checkAllDocs(@ContextParam(ContextType.TRIGGER_EVENT) CheckEvent e){
		if(e.isChecked()){
			plBerkasPengajuanDtosSelected.addAll(plBerkasPengajuanDtos);
		}else{
			plBerkasPengajuanDtosSelected.clear();
		}
		
		BindUtils.postNotifyChange(null, null, this, "plBerkasPengajuanDtosSelected");
	}

	@Command
	public void close(@BindingParam("win")Window winLov){
		if (winLov == null)
			throw new RuntimeException(
					"id popUp tidak sama dengan viewModel");
		winLov.detach();
	}
	
	@Command
	public void nextStep() {
		Map<String, Object> input = new HashMap<>();
		RestResponse rest = new RestResponse();
		input.put("option","nextStep");
		input.put("plPengajuanSantunanDto", this.pengajuanSantunanDto);
		input.put("userSess", getCurrentUserSessionJR());
		try{
			rest = callWs(WS_URI+"/general", input, HttpMethod.POST);
			Map<String, Object> out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			if(rest.getStatus()==CommonConstants.OK_REST_STATUS){
				showInfoMsgBox("Berhasil melanjutkan ke proses otorisasi!", "");
			}else{
				showErrorMsgBox("Gagal melanjutkan ke proses otorisasi!", "");
			}
		}catch(Exception s){
			showErrorMsgBox("Gagal melanjutkan ke proses otorisasi!", "");
		}
	}
	
	@Command
	public void save(){
		if(plBerkasPengajuanDtosSelected.size()==0){
			showInfoMsgBox("Pilih minimal 1 data untuk menyimpan (Gunakan Checkbox pada sisi kiri baris data) !", "");
		}else{
			List<PlBerkasPengajuanDto> berkasPengajuanDtos = new ArrayList<>();
			for(PlBerkasPengajuanDto a : plBerkasPengajuanDtosSelected){
				PlBerkasPengajuanDto dto = new PlBerkasPengajuanDto();
				dto = a;
				for(PlBerkasPengajuanDto b: plBerkasPengajuanDtos){
					if(a.getKodeBerkas().equalsIgnoreCase(b.getKodeBerkas())){
						dto.setAbsahFlag("Y");
					}
				}
				berkasPengajuanDtos.add(dto);
			}
			
			System.err.println("luthfi "+pengajuanSantunanDto.getNoBerkas());
			penyelesaianSantunanDto.setNoBerkas(pengajuanSantunanDto.getNoBerkas());
			penyelesaianSantunanDto.setTglProses(new Date());
			penyelesaianSantunanDto.setJumlahDibayarMeninggal(getBiaya1());
			penyelesaianSantunanDto.setJumlahDibayarLukaluka(getBiaya2());
			penyelesaianSantunanDto.setCreatedBy(userSession.getLoginID());
			penyelesaianSantunanDto.setCreationDate(new Date());
			penyelesaianSantunanDto.setJmlByrAmbl(getBiayaAmbulance());
			penyelesaianSantunanDto.setJmlByrP3k(getBiayaP3k());
			
			Map<String, Object> input = new HashMap<>();
			RestResponse rest = new RestResponse();
			input.put("option","save");
			input.put("penyelesaianSantunanDto", penyelesaianSantunanDto);
			input.put("userSess", getCurrentUserSessionJR());
			input.put("berkasPengajuanDtos", berkasPengajuanDtos);
			try{
				/* for logging puspose do not delete this line */System.out.println(new Date()+" : Trying to process next step");
				rest = callWs(WS_URI+"/general", input, HttpMethod.POST);
				Map<String, Object> out = JsonUtil.mapJsonToHashMapObject(rest.getContents());
				if(rest.getStatus()==CommonConstants.OK_REST_STATUS){
					showInfoMsgBox("Data Berhasil Disimpan!", "");
				}else{
					showErrorMsgBox("Data Gagal Disimpan!", "");
				}
			}catch(Exception s){
				s.printStackTrace();
			}

		}
	}
	
	public void sifatCidera() {
		RestResponse restCidera = callWs(WS_URI_LOV + "/getListSifatCidera",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listSifatCidera = JsonUtil.mapJsonToListObject(
					restCidera.getContents(), DasiJrRefCodeDto.class);
//			for (DasiJrRefCodeDto o : listSifatCidera) {
//				sifatCideraDto.setRvHighValue(o.getRvHighValue());
//				sifatCideraDto.setRvLowValue(o.getRvLowValue());
//				sifatCideraDto.setRvMeaning(o.getRvMeaning());
//			}
			BindUtils.postNotifyChange(null, null, this, "listSifatCidera");
//			BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@NotifyChange({ "cideraKode", "cideraKode2" })
	@Command
	public void kodeCidera() {		
		for(DasiJrRefCodeDto sifatCidera : listSifatCidera){
			if(sifatCideraDto.getRvLowValue().equalsIgnoreCase(sifatCidera.getRvLowValue())){
				sifatCideraDto = new DasiJrRefCodeDto();
				sifatCideraDto = sifatCidera;
			}
		}
		System.err.println("luthfi70 "+sifatCideraDto.getRvLowValue());
		if (sifatCideraDto.getRvLowValue() == "01"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("01")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setTotalMeninggal(getBiaya1().doubleValue());

		} else if (sifatCideraDto.getRvLowValue() == "02"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("02")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setTotalLuka((getBiaya1().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "04"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("04")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setTotalCacat((getBiaya1().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "05"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("05")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setTotalMeninggal((getBiaya1().doubleValue()));
			setTotalLuka((getBiaya2().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "06"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("06")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setTotalLuka((getBiaya1().doubleValue()));
			setTotalCacat((getBiaya2().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "07"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("07")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setTotalPenguburan((getBiaya1().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "08"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("08")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setTotalLuka((getBiaya1().doubleValue()));
			setTotalPenguburan((getBiaya2().doubleValue()));

		}
		BindUtils.postNotifyChange(null, null, this, "totalMeninggal");
		BindUtils.postNotifyChange(null, null, this, "totalLuka");
		BindUtils.postNotifyChange(null, null, this, "totalCacat");
		BindUtils.postNotifyChange(null, null, this, "totalPenguburan");
	}
	
	// ========================================DISPOSISI BERKAS==================================
	@Command("disposisiBerkas")
	public void showPopup(
			@BindingParam("popup") String popup,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();

		args.put("popUpHandler", globalHandleMethodName);

		if (!beforePopup(args, popup))
			return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	protected boolean beforePopup(Map<String, Object> args, String popup) {
		args.put("plPengajuanSantunanDto", pengajuanSantunanDto);
		return true;
	}

	@GlobalCommand("iniHandler")
	public void titleHandler(
			@BindingParam("noBerkas") PlPengajuanSantunanDto selected) {
		if (pengajuanSantunanDto != null) {
			this.pengajuanSantunanDto = selected;
		}
	}


	
	public PlPengajuanSantunanDto getPengajuanSantunan() {
		return pengajuanSantunan;
	}

	public void setPengajuanSantunan(PlPengajuanSantunanDto pengajuanSantunan) {
		this.pengajuanSantunan = pengajuanSantunan;
	}

	public List<PlPengajuanSantunanDto> getListPengajuan() {
		return listPengajuan;
	}

	public void setListPengajuan(List<PlPengajuanSantunanDto> listPengajuan) {
		this.listPengajuan = listPengajuan;
	}

	public Date getTglPenerimaan() {
		return tglPenerimaan;
	}

	public void setTglPenerimaan(Date tglPenerimaan) {
		this.tglPenerimaan = tglPenerimaan;
	}


	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public List<GeneralDto> getOptionPengajuan() {
		return optionPengajuan;
	}

	public void setOptionPengajuan(List<GeneralDto> optionPengajuan) {
		this.optionPengajuan = optionPengajuan;
	}

	public GeneralDto getPilihanPengajuan() {
		return pilihanPengajuan;
	}

	public void setPilihanPengajuan(GeneralDto pilihanPengajuan) {
		this.pilihanPengajuan = pilihanPengajuan;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getSearchIndex() {
		return searchIndex;
	}
	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}
	public String getPilihPengajuan() {
		return pilihPengajuan;
	}
	public void setPilihPengajuan(String pilihPengajuan) {
		this.pilihPengajuan = pilihPengajuan;
	}
	public List<PlPengajuanSantunanDto> getListIndex() {
		return listIndex;
	}
	public void setListIndex(List<PlPengajuanSantunanDto> listIndex) {
		this.listIndex = listIndex;
	}
	public List<PlPengajuanSantunanDto> getListIndexCopy() {
		return listIndexCopy;
	}
	public void setListIndexCopy(List<PlPengajuanSantunanDto> listIndexCopy) {
		this.listIndexCopy = listIndexCopy;
	}
	public List<String> getListPilihanPengajuan() {
		return listPilihanPengajuan;
	}
	public void setListPilihanPengajuan(List<String> listPilihanPengajuan) {
		this.listPilihanPengajuan = listPilihanPengajuan;
	}
	public boolean isListIndexWindow() {
		return listIndexWindow;
	}
	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}
	
	public List<PlPengajuanSantunanDto> getPengajuanSantunanDtos() {
		return pengajuanSantunanDtos;
	}
	public void setPengajuanSantunanDtos(
			List<PlPengajuanSantunanDto> pengajuanSantunanDtos) {
		this.pengajuanSantunanDtos = pengajuanSantunanDtos;
	}
	public PlPengajuanSantunanDto getPengajuanSantunanDto() {
		return pengajuanSantunanDto;
	}
	public void setPengajuanSantunanDto(PlPengajuanSantunanDto pengajuanSantunanDto) {
		this.pengajuanSantunanDto = pengajuanSantunanDto;
	}
	public List<PlBerkasPengajuanDto> getPlBerkasPengajuanDtos() {
		return plBerkasPengajuanDtos;
	}
	public void setPlBerkasPengajuanDtos(
			List<PlBerkasPengajuanDto> plBerkasPengajuanDtos) {
		this.plBerkasPengajuanDtos = plBerkasPengajuanDtos;
	}
	public List<PlBerkasPengajuanDto> getPlBerkasPengajuanDtosSelected() {
		return plBerkasPengajuanDtosSelected;
	}
	public void setPlBerkasPengajuanDtosSelected(
			List<PlBerkasPengajuanDto> plBerkasPengajuanDtosSelected) {
		this.plBerkasPengajuanDtosSelected = plBerkasPengajuanDtosSelected;
	}
	public String getCideraKode() {
		return cideraKode;
	}
	public void setCideraKode(String cideraKode) {
		this.cideraKode = cideraKode;
	}
	public String getCideraKode2() {
		return cideraKode2;
	}
	public void setCideraKode2(String cideraKode2) {
		this.cideraKode2 = cideraKode2;
	}
	public BigDecimal getBiaya1() {
		return biaya1;
	}
	public void setBiaya1(BigDecimal biaya1) {
		this.biaya1 = biaya1;
	}
	public BigDecimal getBiaya2() {
		return biaya2;
	}
	public void setBiaya2(BigDecimal biaya2) {
		this.biaya2 = biaya2;
	}
	public DasiJrRefCodeDto getSifatCideraDto() {
		return sifatCideraDto;
	}
	public void setSifatCideraDto(DasiJrRefCodeDto sifatCideraDto) {
		this.sifatCideraDto = sifatCideraDto;
	}
	public List<DasiJrRefCodeDto> getListSifatCidera() {
		return listSifatCidera;
	}
	public void setListSifatCidera(List<DasiJrRefCodeDto> listSifatCidera) {
		this.listSifatCidera = listSifatCidera;
	}
	public double getAkmMeninggal() {
		return akmMeninggal;
	}
	public void setAkmMeninggal(double akmMeninggal) {
		this.akmMeninggal = akmMeninggal;
	}
	public double getAkmLuka() {
		return akmLuka;
	}
	public void setAkmLuka(double akmLuka) {
		this.akmLuka = akmLuka;
	}
	public double getAkmCacat() {
		return akmCacat;
	}
	public void setAkmCacat(double akmCacat) {
		this.akmCacat = akmCacat;
	}
	public double getAkmPenguburan() {
		return akmPenguburan;
	}
	public void setAkmPenguburan(double akmPenguburan) {
		this.akmPenguburan = akmPenguburan;
	}
	public double getAkmAmbulance() {
		return akmAmbulance;
	}
	public void setAkmAmbulance(double akmAmbulance) {
		this.akmAmbulance = akmAmbulance;
	}
	public double getAkmP3k() {
		return akmP3k;
	}
	public void setAkmP3k(double akmP3k) {
		this.akmP3k = akmP3k;
	}
	public double getTotalMeninggal() {
		return totalMeninggal;
	}
	public void setTotalMeninggal(double totalMeninggal) {
		this.totalMeninggal = totalMeninggal;
	}
	public double getTotalLuka() {
		return totalLuka;
	}
	public void setTotalLuka(double totalLuka) {
		this.totalLuka = totalLuka;
	}
	public double getTotalCacat() {
		return totalCacat;
	}
	public void setTotalCacat(double totalCacat) {
		this.totalCacat = totalCacat;
	}
	public double getTotalPenguburan() {
		return totalPenguburan;
	}
	public void setTotalPenguburan(double totalPenguburan) {
		this.totalPenguburan = totalPenguburan;
	}
	public double getTotalAmbulance() {
		return totalAmbulance;
	}
	public void setTotalAmbulance(double totalAmbulance) {
		this.totalAmbulance = totalAmbulance;
	}
	public double getTotalP3k() {
		return totalP3k;
	}
	public void setTotalP3k(double totalP3k) {
		this.totalP3k = totalP3k;
	}
	public BigDecimal getBiayaAmbulance() {
		return biayaAmbulance;
	}
	public void setBiayaAmbulance(BigDecimal biayaAmbulance) {
		this.biayaAmbulance = biayaAmbulance;
	}
	public BigDecimal getBiayaP3k() {
		return biayaP3k;
	}
	public void setBiayaP3k(BigDecimal biayaP3k) {
		this.biayaP3k = biayaP3k;
	}
	
	public void setBiayaTotal(double biayaTotal) {
		this.biayaTotal = biayaTotal;
	}
	public double getBiayaTotal() {
		return biayaTotal;
	}
	public PlBerkasPengajuanDto getPlBerkasPengajuanDto() {
		return plBerkasPengajuanDto;
	}
	public void setPlBerkasPengajuanDto(PlBerkasPengajuanDto plBerkasPengajuanDto) {
		this.plBerkasPengajuanDto = plBerkasPengajuanDto;
	}
	public PlPenyelesaianSantunanDto getPenyelesaianSantunanDto() {
		return penyelesaianSantunanDto;
	}
	public void setPenyelesaianSantunanDto(
			PlPenyelesaianSantunanDto penyelesaianSantunanDto) {
		this.penyelesaianSantunanDto = penyelesaianSantunanDto;
	}
	
	public List<LampiranDto> getListDocs() {
		return listDocs;
	}
	public void setListDocs(List<LampiranDto> listDocs) {
		this.listDocs = listDocs;
	}

	
}
