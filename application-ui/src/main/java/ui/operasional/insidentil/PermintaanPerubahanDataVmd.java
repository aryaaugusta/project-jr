package ui.operasional.insidentil;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import share.FndKantorJasaraharjaDto;
import share.PlPengajuanSantunanDto;
import share.PlRequestPerubahanDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass=true)
public class PermintaanPerubahanDataVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpInsidentil/InsPermintaanPerubahanData/_index.zul";
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpInsidentil/InsPermintaanPerubahanData/EntryPermintaanPerubahanDataPengajuan.zul";
	private List<String> listStatusPermintaan = new ArrayList<>();
	
	private final String WS_URI = "/PermintaanPerubahanDataPengajuan";
	private final String WS_URI_LOV = "/Lov";
	
	
	private PlRequestPerubahanDto plRequestPerubahanDto= new PlRequestPerubahanDto();
	private List<PlRequestPerubahanDto> listDto= new ArrayList<>();
	private List<PlRequestPerubahanDto> listDtoCopy=new ArrayList<>();
	
	private FndKantorJasaraharjaDto fndKantorJasaraharjaDto= new FndKantorJasaraharjaDto();
	private List<FndKantorJasaraharjaDto> listKantor = new ArrayList<>();
	private String pilihStatusPermintaan;
	private int pageSize = 5;
	
	//filter dan search
	private String noBerkas;
	private String searchIndex;
	
	private boolean visibleUpdate;
	
	
	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent()
			.getAttribute(UIConstants.SESS_LOGIN_ID);
	
	
	protected void loadList() {
		listStatusPermintaan();
		
		
		RestResponse rest = callWs(WS_URI + "/all", new HashMap<String,Object>(), HttpMethod.POST);
		try {
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRequestPerubahanDto.class);
			listDtoCopy=new ArrayList<>();
			listDtoCopy.addAll(listDto);
			BindUtils.postNotifyChange(null, null, this, "listDto");
			BindUtils.postNotifyChange(null, null, this, "listDtoCopy");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@NotifyChange("listDtoCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listDtoCopy));
		if(listDtoCopy != null || listDtoCopy.size() >0 ){
			listDtoCopy.clear();
		}
		if(listDto != null && listDto.size() > 0){
			for(PlRequestPerubahanDto dto : listDto){
				System.out.println("+++");
				
					if (
							sdf.format(dto.getCreatedDate()).equals(cari)
							|| date.format(dto.getCreatedDate()).equals(cari)
							|| month.format(dto.getCreatedDate())
									.equals(cari)
							|| year.format(dto.getCreatedDate()).equals(cari)
							|| dto.getNoBerkas().toUpperCase().contains(cari)
							|| dto.getNamaKantor().toUpperCase()
									.contains(cari)
							|| dto.getNamaKorban().toUpperCase().contains(cari)
							|| dto.getStatusRequest().toUpperCase()
									.contains(cari)) 
					{
						listDtoCopy.add(dto);
					}
				
			}
		}
	}
	
	
	@Command("filter")
	public void filter() {
		sortAndSearch();
	}
	
	@Command("search")
	public void searchIndex(){
		
		
		if (getNoBerkas() == null || getNoBerkas().equalsIgnoreCase("")) {
			setNoBerkas("%%");
		}
		if (searchIndex == null){
			searchIndex="%%";
		}
		if (pilihStatusPermintaan.equalsIgnoreCase("Selesai")){
			pilihStatusPermintaan="DONE";
		}else if(pilihStatusPermintaan.equalsIgnoreCase("Semua")){
			pilihStatusPermintaan="";
		}
		
		HashMap<String, Object> filter = new HashMap<>();
		filter.put("noBerkas", noBerkas);
		filter.put("status", pilihStatusPermintaan);
		filter.put("search", searchIndex);
		
		RestResponse rest = callWs(WS_URI + "/all", filter, HttpMethod.POST);
		try {
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRequestPerubahanDto.class);
			BindUtils.postNotifyChange(null, null, this, "listDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	public void listStatusPermintaan(){
		setPilihStatusPermintaan("Pending");
		listStatusPermintaan.add("Semua");
		listStatusPermintaan.add("Pending");
		listStatusPermintaan.add("Selesai");
		BindUtils.postNotifyChange(null, null, this, "pilihStatusPermintaan");
		BindUtils.postNotifyChange(null, null, this, "listStatusPermintaan");
	}
	
		
	@Command("addPermintaan")
	public void add(){
		getPageInfo().setAddMode(true);
		navigate(DETAIL_PAGE_PATH);
	}
	
	public void onAdd(){
		plRequestPerubahanDto = new PlRequestPerubahanDto();
		plRequestPerubahanDto.setKodeKantorJr(userSession.getKantor()+ "-" +userSession.getNamaKantor());
		plRequestPerubahanDto.setCreatedBy(userSession.getLoginID()+ "-" +userSession.getLoginDesc());
		plRequestPerubahanDto.setCreatedDate(new Date());
		
		
		BindUtils.postNotifyChange(null, null, this, "userSession");
		BindUtils.postNotifyChange(null, null, this, "plRequestPerubahanDto");
	}
	
	@Command("edit")
	public void edit(@BindingParam("item") PlRequestPerubahanDto selected){
		if (selected == null || selected.getNoBerkas() == null){
			showSmartMsgBox("W001");
			return;
		}
		 
		Executions.getCurrent().setAttribute("obj",selected);
		getPageInfo().setEditMode(true);
		navigate(DETAIL_PAGE_PATH);
	}
	
	public void onEdit(){
		plRequestPerubahanDto = (PlRequestPerubahanDto) Executions.getCurrent()
				.getAttribute("obj");
		
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("noBerkas", plRequestPerubahanDto.getNoBerkas());
		RestResponse restEdit = callWs(WS_URI + "/findByNoBerkas",
				mapInput, HttpMethod.POST);
		

		try {
			listDto = JsonUtil.mapJsonToListObject(restEdit.getContents(),
					PlRequestPerubahanDto.class);

			for (PlRequestPerubahanDto a : listDto) {
				plRequestPerubahanDto.setKodeKantorJr(a.getKodeNamaKantor());
				plRequestPerubahanDto.setNoBerkas(a.getNoBerkas());
				plRequestPerubahanDto.setCreatedBy(a.getCreatedByDesc());
				plRequestPerubahanDto.setCreatedDate(a.getCreatedDate());
				plRequestPerubahanDto.setKeterangan(a.getKeterangan());
				plRequestPerubahanDto.setIdRequest(a.getIdRequest());
				plRequestPerubahanDto.setUpdatedBy(a.getUpdatedBy());
				plRequestPerubahanDto.setTglUpdate(a.getTglUpdate());
				plRequestPerubahanDto.setKetStatus(a.getKetStatus());
			}
			BindUtils.postNotifyChange(null, null, this, "plRequestPerubahanDto");
			 if (plRequestPerubahanDto.getTglUpdate() !=null){
				 setVisibleUpdate(true);
			 }

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command("savePermintaan")
	public void save(){
		RestResponse rest;
	if (getPageInfo().isAddMode()) {
		try {
//			ambil menit dan bulan
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String a = sdf.format(date);
			String b = a.substring(3,5);//bulan
			String c =a.substring(14, 16);//menit
			String d= a.substring(6, 10);//tahun
			String e = a.substring(11, 19).replace(":", "");//jam
			String idRequest= userSession.getKantor() + "." + d + c + b + "." + e;
			Messagebox.show(idRequest);

			plRequestPerubahanDto.setKodeKantorJr(userSession.getKantor());
			plRequestPerubahanDto.setNoBerkas(plRequestPerubahanDto.getNoBerkas());
			plRequestPerubahanDto.setCreatedBy(userSession.getLoginID());
			plRequestPerubahanDto.setCreatedDate(new Date());
			plRequestPerubahanDto.setKeterangan(plRequestPerubahanDto.getKeterangan());
			plRequestPerubahanDto.setIdRequest(idRequest);
			//Messagebox.show("id Request :::" +idRequest);
			plRequestPerubahanDto.setStatusRequest("PENDING");
			
			rest = callWs(WS_URI + "/savePermintaan",
					plRequestPerubahanDto, HttpMethod.POST);

			if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(rest.getMessage());
				back();

			} else {
				showErrorMsgBox(rest.getMessage());
			}

			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}else {
		plRequestPerubahanDto.setKodeKantorJr(userSession.getKantor());
		plRequestPerubahanDto.setNoBerkas(plRequestPerubahanDto.getNoBerkas());
		plRequestPerubahanDto.setUpdatedBy(userSession.getLoginID()+ "-" +userSession.getLoginDesc());
		plRequestPerubahanDto.setTglUpdate(new Date());
		plRequestPerubahanDto.setKeterangan(plRequestPerubahanDto.getKeterangan());
		plRequestPerubahanDto.setStatusRequest("PENDING");
		
		rest = callWs(WS_URI + "/savePermintaan",
				plRequestPerubahanDto, HttpMethod.POST);

		if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(rest.getMessage());
			back();

		} else {
			showErrorMsgBox(rest.getMessage());
		}
	}
		
	}
	
	@Command
	public void deletePermintaan(
			@BindingParam("item") final PlRequestPerubahanDto selected) {
		if (selected == null || selected.getIdRequest() == null) {
			showSmartMsgBox("W001");
			return;
		}
		

		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							restRespone = callWs(WS_URI + "/deletePermintaan",
									selected, HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data Berhasil Dihapus");
								refresh();
							} else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});
	}
	
	@Command
	public void refresh() {
		navigate("");
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}

	@Command("back")
	public void back() {
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}

	
	public List<String> getListStatusPermintaan() {
		return listStatusPermintaan;
	}


	public void setListStatusPermintaan(List<String> listStatusPermintaan) {
		this.listStatusPermintaan = listStatusPermintaan;
	}


	public String getPilihStatusPermintaan() {
		return pilihStatusPermintaan;
	}


	public void setPilihStatusPermintaan(String pilihStatusPermintaan) {
		this.pilihStatusPermintaan = pilihStatusPermintaan;
	}


	public int getPageSize() {
		return pageSize;
	}


	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}


	public PlRequestPerubahanDto getPlRequestPerubahanDto() {
		return plRequestPerubahanDto;
	}


	public void setPlRequestPerubahanDto(PlRequestPerubahanDto plRequestPerubahanDto) {
		this.plRequestPerubahanDto = plRequestPerubahanDto;
	}


	public List<PlRequestPerubahanDto> getListDto() {
		return listDto;
	}


	public void setListDto(List<PlRequestPerubahanDto> listDto) {
		this.listDto = listDto;
	}


	public FndKantorJasaraharjaDto getFndKantorJasaraharjaDto() {
		return fndKantorJasaraharjaDto;
	}


	public void setFndKantorJasaraharjaDto(
			FndKantorJasaraharjaDto fndKantorJasaraharjaDto) {
		this.fndKantorJasaraharjaDto = fndKantorJasaraharjaDto;
	}


	public boolean isVisibleUpdate() {
		return visibleUpdate;
	}


	public void setVisibleUpdate(boolean visibleUpdate) {
		this.visibleUpdate = visibleUpdate;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public List<PlRequestPerubahanDto> getListDtoCopy() {
		return listDtoCopy;
	}

	public void setListDtoCopy(List<PlRequestPerubahanDto> listDtoCopy) {
		this.listDtoCopy = listDtoCopy;
	}

	public List<FndKantorJasaraharjaDto> getListKantor() {
		return listKantor;
	}

	public void setListKantor(List<FndKantorJasaraharjaDto> listKantor) {
		this.listKantor = listKantor;
	}

}
