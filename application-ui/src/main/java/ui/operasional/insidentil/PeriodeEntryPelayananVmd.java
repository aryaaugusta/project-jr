package ui.operasional.insidentil;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import share.FndKantorJasaraharjaDto;
import share.PlPeriodeLockDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;
import core.model.PlPeriodeLock;

@Init(superclass = true)
public class PeriodeEntryPelayananVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private final String WS_URI = "/OpPeriodeEntryLayanan";
	private final String WS_URI_LOV = "/Lov";
	
	private boolean listIndexWindow;

	private List<PlPeriodeLockDto> listPeriodeLockDto = new ArrayList<>();
	private List<PlPeriodeLockDto> listPeriodeLockDtoCopy = new ArrayList<>();
	private PlPeriodeLockDto periodeLockDto = new PlPeriodeLockDto();
	private List<String> listBulan = new ArrayList<>();
	private String bulanPeriode;
	private String tahunPeriode;
	private boolean admins = false;
	
	private int pageSize = 5;
	private String status;
	
	private String searchIndex;
	private String kantorFull;
	private boolean winEdit = false;
	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent()
			.getAttribute(UIConstants.SESS_LOGIN_ID);
	
	protected void loadList() {
		admins = isAdmin();
		kantorFull = getCurrentUserSessionJR().getKantor() + " - " + getCurrentUserSessionJR().getNamaKantor();
		listBulanPeriode();
		BindUtils.postNotifyChange(null, null, this, "admins");
		BindUtils.postNotifyChange(null, null, this, "kantorFull");
	}
	
	public void listBulanPeriode(){
		setBulanPeriode("-");
		listBulan.add("-");
		listBulan.add("Januari");
		listBulan.add("Februari");
		listBulan.add("Maret");
		listBulan.add("April");
		listBulan.add("Mei");
		listBulan.add("Juni");
		listBulan.add("Juli");
		listBulan.add("Agustus");
		listBulan.add("September");
		listBulan.add("Oktober");
		listBulan.add("November");
		listBulan.add("Desember");
		BindUtils.postNotifyChange(null, null, this, "bulanPeriode");
		BindUtils.postNotifyChange(null, null, this, "listBulan");
	}
	
	@Command
	public void edit(@BindingParam("item")PlPeriodeLockDto dto){
		if(dto == null){
			showInfoMsgBox("Harap pilih data terlebih dahulu");
			return;
		}
		try{
			status = dto.getStatus();
			winEdit = true;
		}catch(Exception s){
			s.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "status");
		BindUtils.postNotifyChange(null, null, this, "winEdit");
		
	}
	
	@Command
	public void simpan(){
		String statuss = "";
		if(status.equalsIgnoreCase("BUKA")){
			statuss = "OPEN";
		}else{
			statuss = "CLOSED";
		}
		periodeLockDto.setStatus(statuss);
		periodeLockDto.setUpdatedBy(getCurrentUserSessionJR().getLoginID());
		periodeLockDto.setUpdatedDate(new Date());
		Map<String, Object> input = new HashMap<>();
		input.put("option", "update");
		input.put("periode", periodeLockDto);
		RestResponse rest = new RestResponse();
		
		try {
			rest = callWs(WS_URI+"/general", input, HttpMethod.POST);
			Map<String, Object> maps = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			if(CommonConstants.OK_REST_STATUS == (int) maps.get("status")){
				showInfoMsgBox("Data Berhasil di Update");
			}else{
				showInfoMsgBox("Data tidak berhasil di update");
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}
	}
	
	@Command("cari")
	public void cari(){
		if(bulanPeriode=="-"){
			bulanPeriode = "";
		}
		if(tahunPeriode==null){
			tahunPeriode = "";
		}
		if(bulanPeriode.equalsIgnoreCase("Januari")){
			bulanPeriode = "01";
		}else if(bulanPeriode.equalsIgnoreCase("Februari")){
			bulanPeriode = "02";			
		}else if(bulanPeriode.equalsIgnoreCase("Maret")){
			bulanPeriode = "03";			
		}else if(bulanPeriode.equalsIgnoreCase("April")){
			bulanPeriode = "04";			
		}else if(bulanPeriode.equalsIgnoreCase("Mei")){
			bulanPeriode = "05";			
		}else if(bulanPeriode.equalsIgnoreCase("Juni")){
			bulanPeriode = "06";			
		}else if(bulanPeriode.equalsIgnoreCase("Juli")){
			bulanPeriode = "07";			
		}else if(bulanPeriode.equalsIgnoreCase("Agustus")){
			bulanPeriode = "08";			
		}else if(bulanPeriode.equalsIgnoreCase("September")){
			bulanPeriode = "09";			
		}else if(bulanPeriode.equalsIgnoreCase("Oktober")){
			bulanPeriode = "10";			
		}else if(bulanPeriode.equalsIgnoreCase("November")){
			bulanPeriode = "11";			
		}else if(bulanPeriode.equalsIgnoreCase("Desember")){
			bulanPeriode = "12";			
		}
		Map<String, Object> map = new HashMap<>();
		map.put("kodeKantorJr", userSession.getKantor());
		map.put("periodeBulan", bulanPeriode);
		map.put("periodeTahun", tahunPeriode);
		RestResponse rest = callWs(WS_URI+"/all", map, HttpMethod.POST);
		try {
			listPeriodeLockDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlPeriodeLockDto.class);
			
			BindUtils.postNotifyChange(null, null, this, "listPeriodeLockDto");
			listPeriodeLockDtoCopy = new ArrayList<>();
			listPeriodeLockDtoCopy.addAll(listPeriodeLockDto);
			BindUtils.postNotifyChange(null, null, this, "listPeriodeLockDtoCopy");
		} catch (Exception e) {
			e.printStackTrace();
		}
		setListIndexWindow(true);
		BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
	}
	
	@Command("tampilkanSemua")
	public void tampilkanSemua(){
		bulanPeriode = "";
		tahunPeriode = "";
		Map<String, Object> map = new HashMap<>();
		map.put("kodeKantorJr", userSession.getKantor());
		map.put("bulanPeriode", bulanPeriode);
		map.put("tahunPeriode", tahunPeriode);
		RestResponse rest = callWs(WS_URI+"/all", map, HttpMethod.POST);
		try {
			listPeriodeLockDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlPeriodeLockDto.class);
			listPeriodeLockDtoCopy.addAll(listPeriodeLockDto);
			BindUtils.postNotifyChange(null, null, this, "listPeriodeLockDto");
			BindUtils.postNotifyChange(null, null, this, "listPeriodeLockDtoCopy");
		} catch (Exception e) {
			e.printStackTrace();
		}
		setListIndexWindow(true);
		BindUtils.postNotifyChange(null, null, this, "listIndexWindow");		
	}
	
	@Command("searchIndex")
	public void searchIndex(){
		
	}
	
	@Wire("#winPeriod")
	private Window winLov;
	
	@Command
	public void close(){
		if (winLov == null)
			throw new RuntimeException(
					"id popUp tidak sama dengan viewModel");
		winLov.detach();
	}

	

	public boolean isListIndexWindow() {
		return listIndexWindow;
	}

	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}


	public List<PlPeriodeLockDto> getListPeriodeLockDto() {
		return listPeriodeLockDto;
	}


	public void setListPeriodeLockDto(List<PlPeriodeLockDto> listPeriodeLockDto) {
		this.listPeriodeLockDto = listPeriodeLockDto;
	}


	public PlPeriodeLockDto getPeriodeLockDto() {
		return periodeLockDto;
	}


	public void setPeriodeLockDto(PlPeriodeLockDto periodeLockDto) {
		this.periodeLockDto = periodeLockDto;
	}



	public List<String> getListBulan() {
		return listBulan;
	}



	public void setListBulan(List<String> listBulan) {
		this.listBulan = listBulan;
	}



	public String getBulanPeriode() {
		return bulanPeriode;
	}



	public void setBulanPeriode(String bulanPeriode) {
		this.bulanPeriode = bulanPeriode;
	}



	public String getTahunPeriode() {
		return tahunPeriode;
	}



	public void setTahunPeriode(String tahunPeriode) {
		this.tahunPeriode = tahunPeriode;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	@NotifyChange("listPeriodeLockDtoCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println("sssssssssssssssssssss");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listPeriodeLockDto));
		if(listPeriodeLockDtoCopy != null || listPeriodeLockDtoCopy.size() >0 ){
			listPeriodeLockDtoCopy.clear();
		}
		if(listPeriodeLockDto != null && listPeriodeLockDto.size() > 0){
			for(PlPeriodeLockDto dto : listPeriodeLockDto){
				System.out.println("+++");
				System.out.println(sdf.format(dto.getCreationDate()));
				if(dto.getKodeKantorJr().toUpperCase().contains(cari)||
						dto.getPeriode().toUpperCase().contains(cari)||
						dto.getStatus().toUpperCase().contains(cari)||
						sdf.format(dto.getCreationDate()).equals(cari)||
						date.format(dto.getCreationDate()).equals(cari)||
						month.format(dto.getCreationDate()).equals(cari)||
						year.format(dto.getCreationDate()).equals(cari)){
					listPeriodeLockDtoCopy.add(dto);
				}
			}
		}
	}

	public List<PlPeriodeLockDto> getListPeriodeLockDtoCopy() {
		return listPeriodeLockDtoCopy;
	}

	public void setListPeriodeLockDtoCopy(
			List<PlPeriodeLockDto> listPeriodeLockDtoCopy) {
		this.listPeriodeLockDtoCopy = listPeriodeLockDtoCopy;
	}

	public boolean isAdmins() {
		return admins;
	}

	public void setAdmins(boolean admins) {
		this.admins = admins;
	}

	public String getKantorFull() {
		return kantorFull;
	}

	public void setKantorFull(String kantorFull) {
		this.kantorFull = kantorFull;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public boolean isWinEdit() {
		return winEdit;
	}

	public void setWinEdit(boolean winEdit) {
		this.winEdit = winEdit;
	}
	
}
