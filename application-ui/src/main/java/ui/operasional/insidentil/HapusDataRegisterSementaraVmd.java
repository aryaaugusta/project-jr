package ui.operasional.insidentil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.impl.MessageboxDlg;

import share.PlAngkutanKecelakaanDto;
import share.PlRegisterSementaraDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.util.CommonConstants;
import common.util.JsonUtil;
import core.model.PlRegisterSementara;

@Init(superclass = true)
public class HapusDataRegisterSementaraVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private final String WS_URI = "/OperasionalRegisterSementara";
	private final String WS_URI_LOV = "/Lov";
	
	private List<PlRegisterSementaraDto> listRegisterSementaraDto = new ArrayList<>();
	
	private boolean listIndexWindow;
	
	private String searchNoRegister;
	
	
	protected void loadList(){
//		listPilihanPengajuan();
	}
	
	@Command("cari")
	public void cari(){
		Map<String, Object> map = new HashMap<>();		
		map.put("noReg", searchNoRegister);
		RestResponse rest = callWs(WS_URI+"/getRegisterByNoRegister", map, HttpMethod.POST);
		try {
			listRegisterSementaraDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlRegisterSementaraDto.class);
			if(listRegisterSementaraDto.size()==0||listRegisterSementaraDto.size()>1){
				Messagebox.show("Data Register Sementara Tidak Ditemukan");
				listRegisterSementaraDto = new ArrayList<>();
			}else{
				setListIndexWindow(true);
				BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
				BindUtils.postNotifyChange(null, null, this, "listRegisterSementaraDto");				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void deleteRegister(
			@BindingParam("item") final PlRegisterSementaraDto selected) {
		if (selected == null || selected.getNoRegister() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							restRespone = callWs(WS_URI + "/deleteRegisterSementara",
									selected, HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data successfully deleted");
								refresh();
							} else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});
	}
	
	public void refresh(){
		Map<String, Object> map = new HashMap<>();		
		map.put("noReg", searchNoRegister);
		RestResponse rest = callWs(WS_URI+"/getRegisterByNoRegister", map, HttpMethod.POST);
		try {
			listRegisterSementaraDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlRegisterSementaraDto.class);
			BindUtils.postNotifyChange(null, null, this, "listRegisterSementaraDto");				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	public String getSearchNoRegister() {
		return searchNoRegister;
	}

	public void setSearchNoRegister(String searchNoRegister) {
		this.searchNoRegister = searchNoRegister;
	}

	public boolean isListIndexWindow() {
		return listIndexWindow;
	}

	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}

	public List<PlRegisterSementaraDto> getListRegisterSementaraDto() {
		return listRegisterSementaraDto;
	}

	public void setListRegisterSementaraDto(
			List<PlRegisterSementaraDto> listRegisterSementaraDto) {
		this.listRegisterSementaraDto = listRegisterSementaraDto;
	}

}
