package ui.operasional.insidentil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import share.PlPengajuanSantunanDto;
import share.PlRegisterSementaraDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass=true)
public class HapusDataPengajuanVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String WS_URI = "/OpPengajuanSantunan";
	
	private PlPengajuanSantunanDto plPengajuanSantunanDto = new PlPengajuanSantunanDto();
	private List<PlPengajuanSantunanDto> listPengajuanSantunanDto = new ArrayList<>();

	private String noBerkas;
	private boolean listIndexWindow;
	
	protected void loadList(){
	}

	@Command("cari")
	public void cari(){
		Map<String, Object> map = new HashMap<>();		
		map.put("noBerkas", noBerkas);
		RestResponse rest = callWs(WS_URI+"/getIndexHapusPengajuan", map, HttpMethod.POST);
		try {
			listPengajuanSantunanDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
			if(listPengajuanSantunanDto.size()==0||listPengajuanSantunanDto.size()>1){
				showInfoMsgBox("Data Pengajuan Tidak Ditemukan !", "");
				listPengajuanSantunanDto = new ArrayList<>();
			}else{
				setListIndexWindow(true);
				BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
				BindUtils.postNotifyChange(null, null, this, "listPengajuanSantunanDto");								
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void deletePengajuan(
			@BindingParam("item") final PlPengajuanSantunanDto selected) {
		if (selected == null || selected.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							restRespone = callWs(WS_URI + "/deletePengajuan",
									selected, HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data successfully deleted");
								refresh();
							} else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});
	}

	public void refresh(){
		Map<String, Object> map = new HashMap<>();		
		map.put("noBerkas", noBerkas);
		RestResponse rest = callWs(WS_URI+"/getIndexHapusPengajuan", map, HttpMethod.POST);
		try {
			listPengajuanSantunanDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
			BindUtils.postNotifyChange(null, null, this, "listPengajuanSantunanDto");				
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public PlPengajuanSantunanDto getPlPengajuanSantunanDto() {
		return plPengajuanSantunanDto;
	}

	public void setPlPengajuanSantunanDto(
			PlPengajuanSantunanDto plPengajuanSantunanDto) {
		this.plPengajuanSantunanDto = plPengajuanSantunanDto;
	}

	public List<PlPengajuanSantunanDto> getListPengajuanSantunanDto() {
		return listPengajuanSantunanDto;
	}

	public void setListPengajuanSantunanDto(
			List<PlPengajuanSantunanDto> listPengajuanSantunanDto) {
		this.listPengajuanSantunanDto = listPengajuanSantunanDto;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public boolean isListIndexWindow() {
		return listIndexWindow;
	}

	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}

}
