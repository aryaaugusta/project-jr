package ui.operasional;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javassist.expr.NewArray;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import share.AuthUserDto;
import share.DasiJrRefCodeDto;
import share.DukcapilWinDto;
import share.FndCamatDto;
import share.FndKantorJasaraharjaDto;
import share.LaporanDto;
import share.PlAngkutanKecelakaanDto;
import share.PlDataKecelakaanDto;
import share.PlInstansiDto;
import share.PlJaminanDto;
import share.PlKorbanKecelakaanDto;
import share.PlPengajuanSantunanDto;
import share.PlRegisterSementaraDto;
import share.PlRumahSakitDto;
import share.PlTindakLanjutDto;
import share.PlTlRDto;
import ui.DukcapilVmd;

import com.fasterxml.jackson.databind.ObjectMapper;

import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass = true)
public class OpRegisterSementaraVmd extends BaseVmd implements Serializable {

	private static final long serialVersionUID = 1L;

	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpRegisterSementara/_index.zul";
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/CariDataKecelakaan.zul";
	private final String DETAIL_PAGE_PATH2 = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DataPengajuanSantunan.zul";
	private final String ADD_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/tambahRegisterSementara.zul";
	private final String DATA_LAKA_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DataKecelakaanDetail.zul";

	private final String WS_URI = "/OperasionalRegisterSementara";
	private final String WS_URI_LOV = "/Lov";
	private final String WS_URI_LAKA = "/OpDataKecelakaan";
	private final String WS_URI_GL_RS = "/MonitoringGlRs";

	private Form formMaster = new SimpleForm();
	private Form formDetail = new SimpleForm();

	PlRegisterSementaraDto registerSementaraDto = new PlRegisterSementaraDto();
	List<PlRegisterSementaraDto> listRegisterSementaraDto = new ArrayList<>();
	DasiJrRefCodeDto refCodeDto = new DasiJrRefCodeDto();
	List<DasiJrRefCodeDto> listRefCodeDto = new ArrayList<>();
	DasiJrRefCodeDto asalBerkasDto = new DasiJrRefCodeDto();
	List<DasiJrRefCodeDto> listAsalBerkasDto = new ArrayList<>();
	DasiJrRefCodeDto tindakLanjutDto = new DasiJrRefCodeDto();
	List<DasiJrRefCodeDto> listTindakLanjutDto = new ArrayList<>();
	DasiJrRefCodeDto sifatCideraDto = new DasiJrRefCodeDto();
	List<DasiJrRefCodeDto> listSifatCideraDto = new ArrayList<>();
	List<PlJaminanDto> listjaminanDto = new ArrayList<>();

	List<PlDataKecelakaanDto> listDataLakaDto = new ArrayList<>();
	private List<PlDataKecelakaanDto> listDataLakaDtoCopy = new ArrayList<>();

	List<PlKorbanKecelakaanDto> listDataKorbanDto = new ArrayList<>();

	PlDataKecelakaanDto dataLakaDto = new PlDataKecelakaanDto();
	List<PlInstansiDto> listInstansi = new ArrayList<>();
	PlInstansiDto instansiDto = new PlInstansiDto();
	DasiJrRefCodeDto jenisIdentitasDto = new DasiJrRefCodeDto();
	List<DasiJrRefCodeDto> listJenisIdentitasDto = new ArrayList<>();
	DasiJrRefCodeDto jenisHubunganDto = new DasiJrRefCodeDto();
	List<DasiJrRefCodeDto> listJenisHubunganDto = new ArrayList<>();
	private PlRumahSakitDto rumahSakitDto = new PlRumahSakitDto();
	List<PlRumahSakitDto> listRumahSakitDto = new ArrayList<>();
	List<FndKantorJasaraharjaDto> listKantorDto = new ArrayList<>();
	FndKantorJasaraharjaDto kantorDto = new FndKantorJasaraharjaDto();

	DasiJrRefCodeDto asalBerkasDetailDto = new DasiJrRefCodeDto();
	List<DasiJrRefCodeDto> listAsalBerkasDetailDto = new ArrayList<>();

	DasiJrRefCodeDto tindakLanjutDetailDto = new DasiJrRefCodeDto();
	List<DasiJrRefCodeDto> listTindakLanjutDetailDto = new ArrayList<>();
	DasiJrRefCodeDto sifatCideraDetailDto = new DasiJrRefCodeDto();
	List<DasiJrRefCodeDto> listSifatCideraDetailDto = new ArrayList<>();
	List<PlTindakLanjutDto> listTindakLanjutPrintDto = new ArrayList<>();
	PlTindakLanjutDto tindakLanjutPrintDto = new PlTindakLanjutDto();
	List<FndKantorJasaraharjaDto> listKantorPengajuanDto = new ArrayList<>();
	FndKantorJasaraharjaDto kantorPengajuanDto = new FndKantorJasaraharjaDto();
	List<AuthUserDto> listUserDto = new ArrayList<>();
	AuthUserDto userDto = new AuthUserDto();

	PlRegisterSementaraDto insertDto = new PlRegisterSementaraDto();

	PlJaminanDto jaminanDto = new PlJaminanDto();
	List<String> listMonthReg = new ArrayList<>();
	List<String> listMonthLaka = new ArrayList<>();

	String monthRegStr;
	String monthLakaStr;
	String month;
	String monthReg;
	String monthLaka;
	String yearReg;
	String yearLaka;
	String dayReg;
	String dayLaka;

	String kodeKantor;
	String namaKorban;
	String noReg;
	String namaInstansi;
	String noLaporan;
	String asalBerkas;
	String tindakLanjut;
	String sifatCidera;

	// detail
	Date detailKejadianStartDate;
	Date detailKejadianEndDate;
	String detailInstansi;
	Date detailLaporanStartDate;
	Date detailLaporanEndDate;
	String detailNoLaporanPolisi;
	String detailNamaKorban;
	String detailKodeJaminan;
	String statusLP;
	String detailSearch;
	Date timeTL;
	Date dateTL;
	String keteranganTL;
	private List<PlJaminanDto> listJenisPertanggunganDto = new ArrayList<>();
	private PlJaminanDto jenisPertanggunganDto = new PlJaminanDto();
	private List<String> listStatusLp = new ArrayList<>();

	// form add register sementara
	private String diajukanDi;
	private String nomerRegister;
	private Date detailTglRegister = new Date();
	String namaPemohon;
	String ditanganiRs;
	String noIdentitas;
	String telpPemohon;
	String alamatPemohon;
	private String statusKorban;
	private List<String> listStatusKorban = new ArrayList<>();
	String noSuratPanggilanTL;
	String noSuratJaminanTL;

	private List<PlInstansiDto> listInstansiDto = new ArrayList<>();
	private List<PlInstansiDto> listInstansiDtoCopy = new ArrayList<>();

	private boolean listIndex = false;
	private boolean listDetail = false;

	private int detailPageSize = 5;
	private int pageSize = 5;

	Boolean saveFlag = true;

	private boolean buttonAddForRegister;
	private boolean buttonAddForPengajuan;
	private boolean flagPengajuan;

	private String searchKantor;
	private String searchInstansi;
	private String searchAsalBerkas;
	private String searchTindakLanjut;
	private String searchSifatCidera;
	private String searchJenisHubungan;
	private String searchJenisIdentitas;
	private String searchAsalBerkasDetail;
	private String searchTindakLanjutDetail;
	private String searchSifatCideraPengajuan;
	private String searchRS;
	private String searchKantorPengajuan;
	private String searchUser;

	private Date tglMasukRs;
	private BigDecimal jmlPengajuanLuka = new BigDecimal(0);
	private BigDecimal jmlPengajuanKubur = new BigDecimal(0);
	private BigDecimal jmlPengajuanAmbl = new BigDecimal(0);
	private BigDecimal jmlPengajuanP3K = new BigDecimal(0);
	private String noRegForPrintTL;

	private boolean flagEdit = false;
	private boolean flagTindakLanjut = true;
	// pengajuan
	private String p1;
	private String p2;
	private String p3;
	private String p4;
	private boolean disable = false;
	private boolean flagPengajuanButton = true;
	private boolean flagTLRS = false;
	
	private DukcapilWinDto dukcapilWinDto = null;

	private List<FndCamatDto> listProvinsiDto = new ArrayList<>();
	private List<FndCamatDto> listKabkotaDto = new ArrayList<>();
	private List<FndCamatDto> listCamatDto = new ArrayList<>();

	private FndCamatDto provinsiDto = new FndCamatDto();
	private FndCamatDto kabKotaDto = new FndCamatDto();
	private FndCamatDto camatDto = new FndCamatDto();

	
	// ====================== CEK VALIDASI IDENTITAS===================
	
	private String searchProvinsi;
	private String searchCamat;
	private String searchKabkota;
	
	private String msgTlRs;
	private List<PlTlRDto> listPlTlRDtos = new ArrayList<>();
	private boolean disabledTl;

	private AMedia fileContent;
	private boolean flagEditTl=false;
	private PlTlRDto plTlRDto = new PlTlRDto();
	
	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent()
			.getAttribute(UIConstants.SESS_LOGIN_ID);

	@Command
	public void changeCidera() {
		if (getSifatCideraDetailDto() == null
				|| getSifatCideraDetailDto().getRvLowValue().isEmpty()
				|| getSifatCideraDetailDto().getRvLowValue() == null) {
			setP1("");
			setP2("");
			setP3("");
			setP4("");
			setDisable(false);
		} else {
			switch (getSifatCideraDetailDto().getRvLowValue()) {
			case "01":
				setP1("MD");
				setP2("");
				setP3("");
				setP4("P3K");
				setDisable(true);
				break;
			case "02":
				setP1("LL");
				setP2("");
				setP3("");
				setP4("P3K");
				setDisable(true);
				break;
			case "04":
				setP1("CT");
				setP2("");
				setP3("");
				setP4("P3K");
				setDisable(true);
				break;
			case "05":
				setP1("MD");
				setP2("LL");
				setP3("");
				setP4("P3K");
				setDisable(false);
				break;
			case "06":
				setP1("LL");
				setP2("CT");
				setP3("");
				setP4("P3K");
				setDisable(false);
				break;
			case "07":
				setP1("PG");
				setP2("");
				setP3("");
				setP4("P3K");
				setDisable(true);
				break;
			case "08":
				setP1("LL");
				setP2("PG");
				setP3("");
				setP4("P3K");
				setDisable(false);
				break;
			case "":
			default:
				break;
			}
		}
		BindUtils.postNotifyChange(null, null, this, "p1");
		BindUtils.postNotifyChange(null, null, this, "p2");
		BindUtils.postNotifyChange(null, null, this, "p3");
		BindUtils.postNotifyChange(null, null, this, "p4");
		BindUtils.postNotifyChange(null, null, this, "disable");
	}

	@NotifyChange({ "p1", "p2", "disable" })
	@Command
	public void rupiahPengajuan() {
		if (sifatCideraDetailDto.getRvLowValue() == "01"
				|| sifatCideraDetailDto.getRvLowValue().equalsIgnoreCase("01")) {
			setP1(sifatCideraDetailDto.getRvHighValue());
			setP2(null);
			setDisable(true);

		} else if (sifatCideraDetailDto.getRvLowValue() == "02"
				|| sifatCideraDetailDto.getRvLowValue().equalsIgnoreCase("02")) {
			setP1(sifatCideraDetailDto.getRvHighValue());
			setP2(null);
			setDisable(true);

		} else if (sifatCideraDetailDto.getRvLowValue() == "04"
				|| sifatCideraDetailDto.getRvLowValue().equalsIgnoreCase("04")) {
			setP1(sifatCideraDetailDto.getRvHighValue());
			setDisable(true);

		} else if (sifatCideraDetailDto.getRvLowValue() == "05"
				|| sifatCideraDetailDto.getRvLowValue().equalsIgnoreCase("05")) {
			String string = getSifatCideraDetailDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setP1(part1);
			setP2(part2);
			setDisable(false);

		} else if (sifatCideraDetailDto.getRvLowValue() == "06"
				|| sifatCideraDetailDto.getRvLowValue().equalsIgnoreCase("06")) {
			String string = getSifatCideraDetailDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setP1(part1);
			setP2(part2);
			setDisable(false);

		} else if (sifatCideraDetailDto.getRvLowValue() == "07"
				|| sifatCideraDetailDto.getRvLowValue().equalsIgnoreCase("07")) {
			setP1(sifatCideraDetailDto.getRvHighValue());
			setP2(null);
			setDisable(true);

		} else if (sifatCideraDetailDto.getRvLowValue() == "08"
				|| sifatCideraDetailDto.getRvLowValue().equalsIgnoreCase("08")) {
			String string = getSifatCideraDetailDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setP1(part1);
			setP2(part2);
			setDisable(false);
		}
	}

	@Command(value = "printTL")
	public void printDocument() throws Exception {
		SimpleDateFormat tglRegister = new SimpleDateFormat("DD/MM/YYYY");
		SimpleDateFormat jam = new SimpleDateFormat("DD/MM/YYYY HH:mm");

		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();

		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}
		String name = "DataTindakLanjutRegisterSementara.docx";

		// bag 1
		XWPFParagraph par = document.createParagraph();

		XWPFRun run1 = par.createRun();
		run1.setFontFamily("calibri");
		run1.setText("PT. JASA RAHARJA (Persero)");
		run1.addCarriageReturn();
//		run1.setText(getDataLakaDto().getNamaInstansi().toUpperCase());
		run1.setText(userSession.getNamaKantor());
		run1.addCarriageReturn();

		// bag 2
		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.CENTER);

		XWPFRun run2 = par2.createRun();
		run2.setText("DATA TINDAK LANJUT REGISTER SEMENTARA");

		// bag 3
		XWPFParagraph par3 = document.createParagraph();
		par3.setAlignment(ParagraphAlignment.RIGHT);

		XWPFRun run3 = par3.createRun();
		run3.setText("Hal: 1 / 1");

		// bag 4 (tabel)
		XWPFTable tabelRegister = document.createTable();

		// rows

		XWPFTableRow row1Data = tabelRegister.getRow(0);
		row1Data.getCell(0).setText(
				"No.Register: "
						+ getRegisterSementaraDto().getNoRegister()
						+ "	Tgl: "
						+ tglRegister.format(getRegisterSementaraDto()
								.getTglRegister()));
		row1Data.getCell(0).getCTTc().addNewTcPr();
		row1Data.getCell(0).getCTTc().getTcPr().addNewGridSpan();
		row1Data.getCell(0).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(2));
		row1Data.addNewTableCell().setText(
				"Korban: " + dataLakaDto.getNamaKorban());
		row1Data.getCell(1).getCTTc().addNewTcPr();
		row1Data.getCell(1).getCTTc().getTcPr().addNewGridSpan();
		row1Data.getCell(1).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(2));
		row1Data.addNewTableCell().setText(
				"Cidera: " + dataLakaDto.getCideraHighValue());

		XWPFTable tabel1 = document.createTable();

		// rows
		XWPFTableRow row1 = tabel1.getRow(0);
		row1.getCell(0).setText(
				"Asal Berkas: " + getRegisterSementaraDto().getAsalBerkas());
		row1.getCell(0).getCTTc().addNewTcPr();
		row1.getCell(0).getCTTc().getTcPr().addNewGridSpan();
		row1.getCell(0).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(2));
		row1.addNewTableCell().setText(
				"Pemohon: " + getRegisterSementaraDto().getNamaPemohon());
		row1.getCell(1).getCTTc().addNewTcPr();
		row1.getCell(1).getCTTc().getTcPr().addNewGridSpan();
		row1.getCell(1).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(2));
		row1.addNewTableCell().setText(
				"Hub: " + getRegisterSementaraDto().getHubunganKorban());

		// XWPFTableRow row2Data = tabelRegister.createRow();
		// row2Data.getCell(0).setText("Asal Berkas: "+getRegisterSementaraDto().getAsalBerkas());
		// row2Data.getCell(1).setText("Pemohon: "+getRegisterSementaraDto().getNamaPemohon());
		// row2Data.getCell(2).setText("Hub: "+getRegisterSementaraDto().getHubunganKorban());

		XWPFTable tabel2 = document.createTable();

		// rows
		XWPFTableRow row2 = tabel2.getRow(0);
		row2.getCell(0).setText(
				"Tgl Laka: " + jam.format(getDataLakaDto().getTglKejadian()));
		row2.getCell(0).getCTTc().addNewTcPr();
		row2.getCell(0).getCTTc().getTcPr().addNewGridSpan();
		row2.getCell(0).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(2));
		row2.addNewTableCell().setText(
				"Tgl LP: " + tglRegister.format(dataLakaDto.getTglLaporanPolisi()));
		row2.addNewTableCell().setText(
				"No LP: " + dataLakaDto.getNoLaporanPolisi());
		row2.addNewTableCell().setText(
				"Inst: " + dataLakaDto.getNamaInstansi());

		// XWPFTableRow row3Data = tabelRegister.createRow();
		// row3Data.getCell(0).setText("Tgl Laka: "+jam.format(getDataLakaDto().getTglKejadian()));
		// row3Data.getCell(1).setText("Tgl LP: "+tglRegister.format(getDataLakaDto().getTglLaporanPolisi()));
		// row3Data.getCell(2).setText("No LP: "+getDataLakaDto().getNoLaporanPolisi());
		// row3Data.getCell(3).setText("Inst: "+getDataLakaDto().getNamaInstansi());

		XWPFTable tabel3 = document.createTable();

		// rows
		XWPFTableRow row3 = tabel3.getRow(0);
		row3.getCell(0).setText(
				"Kasus: " + getDataLakaDto().getDeskripsiKecelakaan());
		row3.getCell(0).getCTTc().addNewTcPr();
		row3.getCell(0).getCTTc().getTcPr().addNewGridSpan();
		row3.getCell(0).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(3));
		row3.addNewTableCell().setText(
				"Status Korban: " + getDataLakaDto().getStatus());
		row3.addNewTableCell().setText(
				"Penjamin: " + getDataLakaDto().getKendaraanPenjamin());

		// XWPFTableRow row4Data = tabelRegister.createRow();
		// row4Data.getCell(0).setText("Kasus: "+getDataLakaDto().getDeskripsiKecelakaan());
		// row4Data.getCell(1).setText("Status Korban: "+getDataLakaDto().getStatus());
		// row4Data.getCell(2).setText("Penjamin: "+getDataLakaDto().getKendaraanPenjamin());

		XWPFTable tabel4 = document.createTable();

		// rows
		XWPFTableRow row4 = tabel4.getRow(0);
		row4.getCell(0).setText(
				"Lokasi Laka: " + getDataLakaDto().getDeskripsiLokasi());
		row4.getCell(0).getCTTc().addNewTcPr();
		row4.getCell(0).getCTTc().getTcPr().addNewGridSpan();
		row4.getCell(0).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(5));
		// XWPFTableRow row5Data = tabelRegister.createRow();
		// row5Data.getCell(0).setText("Lokasi Laka: "+getDataLakaDto().getDeskripsiLokasi());

		XWPFTable tabelTL = document.createTable();

		XWPFTableRow rowHeadTL = tabelTL.getRow(0);
		rowHeadTL.getCell(0).setText("No.");
		rowHeadTL.addNewTableCell().setText("Tgl Tindak Lanjut");
		rowHeadTL.addNewTableCell().setText(
				"Jenis Tindak Lanjut dan Catatan / Keterangan");
		rowHeadTL.getCell(2).getCTTc().addNewTcPr();
		rowHeadTL.getCell(2).getCTTc().getTcPr().addNewGridSpan();
		rowHeadTL.getCell(2).getCTTc().getTcPr().getGridSpan()
				.setVal(BigInteger.valueOf(3));
		// XWPFTableRow row6Data = tabelRegister.createRow();
		// row6Data.getCell(0).setText("No.");
		// row6Data.getCell(1).setText("Tgl Tindak Lanjut");
		// row6Data.getCell(2).setText("Jenis Tindak Lanjut dan Catatan / Keterangan");

		String tambahanTL = "";

		int i = 0;
		for (PlTindakLanjutDto a : getListTindakLanjutPrintDto()) {
			i++;
			switch (a.getTindakLanjutFlag()) {
			case "1":
			case "6":
				tambahanTL = ", No. Surat: " + a.getNoSuratPanggilan();
				break;
			case "2":
			case "4":
				tambahanTL = ", Nama Petugas: " + a.getPetugasSurvey();
				break;
			case "3":
			case "16":
				tambahanTL = "";
				break;
			case "15":
				tambahanTL = ", Dilimpahkan Ke: " + a.getDilimpahkanKe();
				break;
			default:
				break;
			}
			addRow(tabelTL, 3, 2, Integer.toString(a.getNoUrut()),
					jam.format(a.getTglTindakLanjut()), a.getDeskripsiTL()
							+ tambahanTL + "  " + a.getCatatanTindakLanjut());

		}

		// download dokumen
		File temp = File.createTempFile("Document JR", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Document JR", "docx", "application/file",
				fis));
		temp.delete();

		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");
	}

	private void addRow(XWPFTable table, int b, int c, String... a) {
		if (a.length > 0) {
			XWPFTableRow row = table.createRow();
			for (int x = 0; x < a.length; x++) {
				if (x > 0 && (row.getCell(x) == null)) {
					row.addNewTableCell().setText(a[x]);
					if (x == c) {
						row.getCell(x).getCTTc().addNewTcPr();
						row.getCell(x).getCTTc().getTcPr().addNewGridSpan();
						row.getCell(x).getCTTc().getTcPr().getGridSpan()
								.setVal(BigInteger.valueOf(b));
					}

				} else {
					row.getCell(x).setText(a[x]);
					if (x == c) {
						row.getCell(x).getCTTc().addNewTcPr();
						row.getCell(x).getCTTc().getTcPr().addNewGridSpan();
						row.getCell(x).getCTTc().getTcPr().getGridSpan()
								.setVal(BigInteger.valueOf(b));
					}
				}
			}
		}
	}

	public void loadTindakLanjutPrint() {
		Map<String, Object> mapinput = new HashMap<>();
		if (getPageInfo().isAddDetailMode()) {
			mapinput.put("noRegister", registerSementaraDto.getNoRegister());
		} else {
			mapinput.put("noRegister", getRegisterSementaraDto()
					.getNoRegister());
		}
		RestResponse rest = callWs(WS_URI + "/tindakLanjut", mapinput,
				HttpMethod.POST);
		try {
			listTindakLanjutPrintDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), PlTindakLanjutDto.class);
			BindUtils.postNotifyChange(null, null, this,
					"listTindakLanjutPrintDto");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Command
	public void deleteTindakLanjut(
			@BindingParam("item") final PlTindakLanjutDto selected) {
		if (selected == null || selected.getIdTlRegister() == null) {
			showSmartMsgBox("W001");
			return;
		}
		final Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("idTlReg", selected.getIdTlRegister());
		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							restRespone = callWs(WS_URI + "/deleteTL",
									mapInput, HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data successfully deleted");
								setNoRegForPrintTL(selected.getNoRegister());
								BindUtils.postNotifyChange(null, null, this,
										"noRegForPrintTL");
								loadTindakLanjutPrint();

							} else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});
	}

	@Command
	public void tambahTL() {
		setDateTL(new Date());
		setFlagEditTl(false);
		tindakLanjutDetailDto = new DasiJrRefCodeDto();
		kantorPengajuanDto = new FndKantorJasaraharjaDto();
		setNoSuratPanggilanTL("");
		userDto = new AuthUserDto();
		setNoSuratJaminanTL("");
		setKeteranganTL("");
		setFlagTindakLanjut(true);
		tindakLanjutPrintDto = new PlTindakLanjutDto();
		BindUtils.postNotifyChange(null, null, this, "tindakLanjutDetailDto");
		BindUtils.postNotifyChange(null, null, this, "kantorPengajuanDto");
		BindUtils.postNotifyChange(null, null, this, "userDto");
		BindUtils.postNotifyChange(null, null, this, "dateTL");
		BindUtils.postNotifyChange(null, null, this, "noSuratPanggilanTL");
		BindUtils.postNotifyChange(null, null, this, "keteranganTL");
		BindUtils.postNotifyChange(null, null, this, "flagTindakLanjut");
		BindUtils.postNotifyChange(null, null, this, "flagEditTl");
		BindUtils.postNotifyChange(null, null, this, "tindakLanjutPrintDto");
	}

	@Command
	public void batalTambahTL() {
		setFlagTindakLanjut(false);
		BindUtils.postNotifyChange(null, null, this, "flagTindakLanjut");

	}

	@Command("editRegister")
	public void editRegister(
			@BindingParam("item") PlRegisterSementaraDto selected,
			@BindingParam("popup") String popup,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Executions.getCurrent().setAttribute("obj", selected);
		getPageInfo().setEditMode(true);
		setListIndex(true);
		BindUtils.postNotifyChange(null, null, this, "listIndex");
		BindUtils.postNotifyChange(null, null, this, "listTindakLanjutPrintDto");
		Map<String, Object> args = new HashMap<>();

		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}

	}

	public void onEdit() {
		registerSementaraDto = (PlRegisterSementaraDto) Executions.getCurrent().getAttribute("obj");
		
		loadAsalBerkasDetail();
		loadTindakLanjutDetail();
		listStatusKorban();
		loadSifatCideraPengajuan();
		loadJenisHubungan();
		loadJenisIdentitas();

//		========================GET DIAJUKAN DI=============================
		Map<String, Object> mapDiajukan = new HashMap<>();
		mapDiajukan.put("search", registerSementaraDto.getKodeKantorJr());
		RestResponse restDiajukan = callWs(WS_URI_LOV+"/findAllKantor", mapDiajukan, HttpMethod.POST);
		try {
			listKantorDto = JsonUtil.mapJsonToListObject(restDiajukan.getContents(), FndKantorJasaraharjaDto.class);
			for(FndKantorJasaraharjaDto dto : listKantorDto){
				setDiajukanDi(dto.getNama());
			}
			BindUtils.postNotifyChange(null, null, this, "diajukanDi");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// =============================GET DATA LAKA=======================================
		Map<String, Object> mapLaka = new HashMap<>();
		mapLaka.put("idKecelakaan", registerSementaraDto.getIdKecelakaanPS());
		RestResponse restLaka = callWs(WS_URI_LAKA + "/dataLakaById", mapLaka,
				HttpMethod.POST);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			listDataLakaDto = JsonUtil.mapJsonToListObject(
					restLaka.getContents(), PlDataKecelakaanDto.class);
			for (PlDataKecelakaanDto a : listDataLakaDto) {
				dataLakaDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
				dataLakaDto.setTglKejadian(a.getTglKejadian());
				dataLakaDto.setTglLaporanPolisi(a.getTglLaporanPolisi());
				dataLakaDto.setKasusKecelakaanDesc(a.getKasusKecelakaanDesc());
				dataLakaDto.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		=======================GET DATA KORBAN===============================
		Map<String, Object> mapKorban = new HashMap<>();
		mapKorban.put("idKorban", registerSementaraDto.getIdKorbanKecelakaan());
		RestResponse restKorban = callWs(WS_URI_LAKA + "/korbanLakaByIdKorban", mapKorban, HttpMethod.POST);

		try {
			listDataKorbanDto = JsonUtil.mapJsonToListObject(restKorban.getContents(), PlKorbanKecelakaanDto.class);

			for (PlKorbanKecelakaanDto a : listDataKorbanDto) {
				dataLakaDto.setNamaKorban(a.getNama());
				dataLakaDto.setCidera(a.getCideraDesc());
				dataLakaDto.setJenisIdentitas(a.getJenisIdentitasDesc());
				dataLakaDto.setNoIdentitas(a.getNoIdentitas());
				dataLakaDto.setStatus(a.getStatusKorbanDesc());
				dataLakaDto.setProvinsiDesc(a.getNamaProvinsi());
				dataLakaDto.setKabKotaDesc(a.getNamaKabkota());
				dataLakaDto.setCamatDesc(a.getNamaCamat());
				dataLakaDto.setAlamat(a.getAlamat());
				dataLakaDto.setNoTelp(a.getNoTelp());
				dataLakaDto.setKodeJaminan(a.getKodeJaminan());
				dataLakaDto.setKodeSifatCidera(a.getKodeSifatCidera());
				dataLakaDto.setNama(a.getNama());
				dataLakaDto.setUmur(a.getUmur());
				dataLakaDto.setPenjamin(a.getPenjaminDesc());
				if (a.getJenisKelamin().equalsIgnoreCase("P")) {
					dataLakaDto.setJenisKelamin("Pria");
				} else if (a.getJenisKelamin().equalsIgnoreCase("W")) {
					dataLakaDto.setJenisKelamin("Wanita");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

//		=============================GET DATA REGISTER================================
		Map<String, Object> mapRegister = new HashMap<>();
		mapRegister.put("noReg", registerSementaraDto.getNoRegister());
		RestResponse restRegister = callWs(WS_URI+"/findOneRegister", mapRegister, HttpMethod.POST);
		try {
			listRegisterSementaraDto = JsonUtil.mapJsonToListObject(restRegister.getContents(), PlRegisterSementaraDto.class);
			for(PlRegisterSementaraDto dto : listRegisterSementaraDto){
				setNomerRegister(dto.getNoRegister());
				asalBerkasDetailDto.setRvLowValue(dto.getAsalBerkasFlag());
				registerSementaraDto.setTglRegister(dto.getTglRegister());
				registerSementaraDto.setCideraKorban(dto.getCideraKorban());
				registerSementaraDto.setKodeRumahSakit(dto.getKodeRumahSakit());
				registerSementaraDto.setTglMasukRs(dto.getTglMasukRs());
				registerSementaraDto.setJumlahPengajuan1(dto.getJumlahPengajuan1());
				registerSementaraDto.setJumlahPengajuan2(dto.getJumlahPengajuan2());
				registerSementaraDto.setJmlAmbl(dto.getJmlAmbl());
				registerSementaraDto.setJmlP3k(dto.getJmlP3k());
				registerSementaraDto.setStatusKrbnRs(dto.getStatusKrbnRs());
				registerSementaraDto.setNamaPemohon(dto.getNamaPemohon());
				registerSementaraDto.setKodeHubunganKorban(dto.getKodeHubunganKorban());
				registerSementaraDto.setJenisIdentitas(dto.getJenisIdentitas());
				registerSementaraDto.setNoIdentitas(dto.getNoIdentitas());
				registerSementaraDto.setAlamatPemohon(dto.getAlamatPemohon());
				registerSementaraDto.setTelpPemohon(dto.getTelpPemohon());
			}
			for(DasiJrRefCodeDto asalBerkas : listAsalBerkasDetailDto){
				if(asalBerkasDetailDto.getRvLowValue().equalsIgnoreCase(asalBerkas.getRvLowValue())){
					asalBerkasDetailDto = new DasiJrRefCodeDto();
					asalBerkasDetailDto = asalBerkas;
				}
			}
			if(registerSementaraDto.getCideraKorban()!=null){
				for(DasiJrRefCodeDto sifatCidera : listSifatCideraDetailDto){
					if(registerSementaraDto.getCideraKorban().equalsIgnoreCase(sifatCidera.getRvLowValue())){
						sifatCideraDetailDto = new DasiJrRefCodeDto();
						sifatCideraDetailDto = sifatCidera;
					}
				}
			}
			if(registerSementaraDto.getJenisIdentitas()!=null){
				for(DasiJrRefCodeDto jenisIdentitas : listJenisIdentitasDto){
					if(registerSementaraDto.getJenisIdentitas().equalsIgnoreCase(jenisIdentitas.getRvLowValue())){
						jenisIdentitasDto = new DasiJrRefCodeDto();
						jenisIdentitasDto = jenisIdentitas;
					}
				}
			}
			if(registerSementaraDto.getKodeHubunganKorban()!=null){
				for(DasiJrRefCodeDto jenisHubungan :listJenisHubunganDto){
					if(registerSementaraDto.getKodeHubunganKorban().equalsIgnoreCase(jenisHubungan.getRvLowValue())){
						jenisHubunganDto = new DasiJrRefCodeDto();
						jenisHubunganDto = jenisHubungan;
					}
				}
			}
			if(registerSementaraDto.getStatusKrbnRs().equalsIgnoreCase("S")){
				setStatusKorban("Korban Ttd Surat Pernyataan");
			}else if(registerSementaraDto.getStatusKrbnRs().equalsIgnoreCase("T")){
				setStatusKorban("Terbit Tagihan Dari RS");
			}else if(registerSementaraDto.getStatusKrbnRs().equalsIgnoreCase("-")||registerSementaraDto.getStatusKrbnRs().equalsIgnoreCase("")){
				setStatusKorban("-");
			}
			rupiahPengajuan();
			BindUtils.postNotifyChange(null, null, this, "asalBerkasDetailDto");
			BindUtils.postNotifyChange(null, null, this, "sifatCideraDetailDto");
			BindUtils.postNotifyChange(null, null, this, "jenisIdentitasDto");
			BindUtils.postNotifyChange(null, null, this, "jenisHubunganDto");
			BindUtils.postNotifyChange(null, null, this, "statusKorban");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
//		================================GET DATA RS========================
		if(registerSementaraDto.getKodeRumahSakit()==null){
			
		}else if(registerSementaraDto.getKodeRumahSakit()!=null){
			Map<String, Object> mapRs = new HashMap<>();
			mapRs.put("search", registerSementaraDto.getKodeRumahSakit());
			RestResponse restRS = callWs(WS_URI_LOV + "/findAllRS", mapRs,
					HttpMethod.POST);
			try {
				listRumahSakitDto = JsonUtil.mapJsonToListObject(restRS.getContents(), PlRumahSakitDto.class);
				for(PlRumahSakitDto dto : listRumahSakitDto){
					rumahSakitDto.setKodeRumahsakit(dto.getKodeRumahsakit());
				}
				for(PlRumahSakitDto rumahSakit:listRumahSakitDto){
					if(rumahSakitDto.getKodeRumahsakit().equalsIgnoreCase(rumahSakit.getKodeRumahsakit())){
						rumahSakitDto = new PlRumahSakitDto();
						rumahSakitDto = rumahSakit;
					}
				}
				
				BindUtils.postNotifyChange(null, null, this, "listRumahSakitDto");
				BindUtils.postNotifyChange(null, null, this, "rumahSakitDto");			
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}
		
		
		
//		Map<String, Object> mapInput = new HashMap<>();
//		mapInput.put("idKorban", getRegisterSementaraDto().getIdKorbanKecelakaan());
//		mapInput.put("idKecelakaan", getRegisterSementaraDto().getIdKecelakaanPS());
//		loadTindakLanjutPrint();
//		RestResponse rest = callWs(WS_URI + "/dataLaka", mapInput,HttpMethod.POST);
//		try {
//			listDataLakaDto = JsonUtil.mapJsonToListObject(rest.getContents(),
//					PlDataKecelakaanDto.class);
//			setDataLakaDto(getListDataLakaDto().get(0));
//			for (DasiJrRefCodeDto a : listAsalBerkasDetailDto) {
//				if (registerSementaraDto.getAsalBerkas().equalsIgnoreCase(
//						a.getRvMeaning())) {
//					asalBerkasDetailDto = a;
//				}
//			}
//			setSearchAsalBerkasDetail(getRegisterSementaraDto().getAsalBerkas());

//			BindUtils.postNotifyChange(null, null, this,"searchAsalBerkasDetail");


//			navigate(ADD_PAGE_PATH);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		loadTindakLanjutPrint();
		setFlagPengajuanButton(true);
		setFlagTindakLanjut(false);
		
		BindUtils.postNotifyChange(null, null, this, "flagTindakLanjut");
		BindUtils.postNotifyChange(null, null, this, "flagPengajuanButton");
		BindUtils.postNotifyChange(null, null, this, "asalBerkasDetailDto");
		BindUtils.postNotifyChange(null, null, this, "dataLakaDto");
		BindUtils.postNotifyChange(null, null, this, "nomerRegister");
		BindUtils.postNotifyChange(null, null, this, "registerSementaraDto");

	}

	@Command("editDataLakaRegister")
	public void editDataLakaRegister(
			@BindingParam("item") PlRegisterSementaraDto selected,
			@BindingParam("popup") String popup,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", selected.getIdKecelakaanPS());
		BindUtils.postGlobalCommand(null, null, "addForRegister", map);
		Map<String, Object> args = new HashMap<>();

		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	@Command
	public void back(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}

	@Command
	public void changePageSize() {
		setPageSize(getPageSize());
		setListIndex(true);
		BindUtils.postNotifyChange(null, null, this, "pageSize");
	}

	@Command
	public void changeDetailPageSize() {
		setPageSize(getDetailPageSize());
		setListIndex(true);
		BindUtils.postNotifyChange(null, null, this, "detailPageSize");
	}

	@Command
	public void testSaveTl() throws ParseException {
		DateFormat tgl = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat waktu = new SimpleDateFormat("HH:mm");
		String time = waktu.format(getTimeTL());
		String date = tgl.format(getDateTL());
		String tglKejadian = date + " " + time;
		Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm")
				.parse(tglKejadian);

		// no register
		String a = userSession.getKantor().substring(0, 2);
		String b = userSession.getKantor().substring(3, 5);
		Date date2 = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
		String c = sdf.format(date2);
		String d = a + b + "-" + c;
		System.err.println("luthfi " + d);
		String noReg = null;
		Map<String, Object> map = new HashMap<>();
		map.put("noReg", d);
		RestResponse rest = callWs(WS_URI + "/getRegisterByNoRegister", map,
				HttpMethod.POST);
		try {
			listRegisterSementaraDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), PlRegisterSementaraDto.class);
			if (listRegisterSementaraDto.size() != 0) {
				for (PlRegisterSementaraDto dto : listRegisterSementaraDto) {
					noReg = dto.getNoRegister();
				}
				System.err.println("luthfi5 " + noReg);
				if (noReg != null) {
					String incVal = noReg.substring(10, 16);
					int countVal = Integer.valueOf(incVal) + 1;
					noReg = d + "-" + String.format("%06d", countVal);
					System.err.println("luthfi " + noReg);
				}
			} else {
				noReg = d + "-" + "000001";
				System.err.println("luthfi " + noReg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// register not null
		System.err.println("luthfi " + asalBerkasDetailDto.getRvLowValue());
		System.err.println("luthfi " + dataLakaDto.getKodeSifatCidera());
		System.err.println("luthfi " + dataLakaDto.getIdKorbanKecelakaan());
		System.err.println("luthfi " + userSession.getKantor());
		System.err.println("luthfi " + registerSementaraDto.getTglRegister());

		System.err.println("luthfi " + registerSementaraDto.getNamaPemohon());
		System.err.println("luthfi " + jenisHubunganDto.getRvLowValue());
		System.err.println("luthfi " + jenisIdentitasDto.getRvLowValue());
		System.err.println("luthfi " + registerSementaraDto.getNoIdentitas());
		System.err.println("luthfi " + registerSementaraDto.getTelpPemohon());
		System.err.println("luthfi " + registerSementaraDto.getAlamatPemohon());
		System.err.println("luthfi " + rumahSakitDto.getKodeRumahsakit());
		System.err.println("luthfi "
				+ registerSementaraDto.getJumlahPengajuan1());
		System.err.println("luthfi "
				+ registerSementaraDto.getJumlahPengajuan2());
		System.err.println("luthfi " + userSession.getLoginID());
		System.err.println("luthfi " + new Date());
		System.err.println("luthfi " + registerSementaraDto.getJmlAmbl());
		System.err.println("luthfi " + registerSementaraDto.getJmlP3k());
		System.err.println("luthfi " + registerSementaraDto.getTglMasukRs());
		System.err.println("luthfi " + statusKorban);

		// ID TL
		Map<String, Object> map2 = new HashMap<>();
		map2.put("noRegister", noReg);
		RestResponse restTl = callWs(WS_URI + "/tindakLanjut", map2,
				HttpMethod.POST);
		String idTlRegister = null;
		int k = 0;
		try {
			listTindakLanjutPrintDto = JsonUtil.mapJsonToListObject(
					restTl.getContents(), PlTindakLanjutDto.class);
			if (listTindakLanjutPrintDto.size() > 0) {
				for (int j = 0; j <= listTindakLanjutPrintDto.size(); j++) {
					k = j++;
				}
				idTlRegister = noReg + "." + k;
				System.out.println("luthfi14 " + idTlRegister);
			} else {
				idTlRegister = noReg + "." + "1";
				System.out.println("luthfi15 " + idTlRegister);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// tl
		System.err.println("luthfi2 " + tindakLanjutDetailDto.getRvLowValue());
		System.err.println("luthfi2 " + date1);
		System.err.println("luthfi2 " + keteranganTL);
		System.err.println("luthfi2 " + noSuratPanggilanTL);
		System.err.println("luthfi2 " + noSuratJaminanTL);

	}

	@Command
	public void saveRegisterTL() throws ParseException {
		if (getTindakLanjutDetailDto().getRvLowValue().equalsIgnoreCase("5")
				&& getListTindakLanjutPrintDto().size() == 0) {
			showInfoMsgBox("Isi Data Pengajuan terlebih dahulu", "");
		} else {

			if (getDateTL() == null || getTimeTL() == null
					|| getTindakLanjutDetailDto() == null
					|| getKeteranganTL() == null) {
				showInfoMsgBox("I", "");
			} else {
				testSaveTl();
				// String noreg;
				// DateFormat tgl = new SimpleDateFormat("dd/MM/yyyy");
				// DateFormat waktu = new SimpleDateFormat("HH:mm");
				// String time = waku.format(getTimeTL());
				// String date = tgl.format(getDateTL());
				// String tglKejadian;
				// Date date1 = new Date();
				ObjectMapper oMapper = new ObjectMapper();
				// Map<String, Object> map = new HashMap<>();
				// tglKejadian = date + " " + time;
				// try {
				// date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm")
				// .parse(tglKejadian);
				// } catch (ParseException e) {
				// e.printStackTrace();
				// }
				//
				// RestResponse rest = new RestResponse();
				// // register sementara
				// if (getPageInfo().isEditMode()) {
				// insertDto.setNoRegister(getNomerRegister());
				// } else {
				// if (getListTindakLanjutPrintDto().size() != 0) {
				// noreg = getNoRegForPrintTL();
				// } else {
				// noreg = "";
				// }
				// insertDto.setNoRegister(noreg);
				// }
				// no register
				String a = userSession.getKantor().substring(0, 2);
				String b = userSession.getKantor().substring(3, 5);
				Date date2 = new Date();
				SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
				String c = sdf.format(date2);
				String d = a + b + "-" + c;
				System.err.println("luthfi " + d);
				String noReg = null;
				Map<String, Object> map = new HashMap<>();
				map.put("noReg", d);
				RestResponse rest = callWs(WS_URI + "/getRegisterByNoRegister",
						map, HttpMethod.POST);
				try {
					listRegisterSementaraDto = JsonUtil.mapJsonToListObject(
							rest.getContents(), PlRegisterSementaraDto.class);
					if (listRegisterSementaraDto.size() != 0) {
						for (PlRegisterSementaraDto dto : listRegisterSementaraDto) {
							noReg = dto.getNoRegister();
						}
						System.err.println("luthfi5 " + noReg);
						if (noReg != null) {
							String incVal = noReg.substring(10, 16);
							int countVal = Integer.valueOf(incVal) + 1;
							noReg = d + "-" + String.format("%06d", countVal);
							registerSementaraDto.setNoRegister(noReg);
							insertDto.setNoRegister(noReg);
						}
					} else {
						noReg = d + "-" + "000001";
						registerSementaraDto.setNoRegister(noReg);
						insertDto.setNoRegister(noReg);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				insertDto.setKodeKantorJr(userSession.getKantor());
				insertDto.setIdKorbanKecelakaan(dataLakaDto.getIdKorbanKecelakaan());
				insertDto.setTglRegister(registerSementaraDto.getTglRegister());
				insertDto.setAsalBerkasFlag(asalBerkasDetailDto.getRvLowValue());
				insertDto.setNamaPemohon(registerSementaraDto.getNamaPemohon());
				insertDto.setKodeHubunganKorban(jenisHubunganDto.getRvLowValue());
				insertDto.setJenisIdentitas(jenisIdentitasDto.getRvLowValue());
				insertDto.setNoIdentitas(registerSementaraDto.getNoIdentitas());
				insertDto.setTelpPemohon(registerSementaraDto.getTelpPemohon());
				insertDto.setAlamatPemohon(registerSementaraDto.getAlamatPemohon());
				insertDto.setCideraKorban(sifatCideraDto.getRvLowValue());
				insertDto.setKodeRumahSakit(rumahSakitDto.getKodeRumahsakit());
				insertDto.setJumlahPengajuan1(registerSementaraDto.getJumlahPengajuan1());
				insertDto.setJumlahPengajuan2(registerSementaraDto.getJumlahPengajuan2());
				insertDto.setCreatedBy(userSession.getLoginID());
				insertDto.setCreatedDate(new Date());
				insertDto.setJmlAmbl(registerSementaraDto.getJmlAmbl());
				insertDto.setJmlP3k(registerSementaraDto.getJmlP3k());
				if (statusKorban.equalsIgnoreCase("Terbit Tagihan Dari RS")) {
					insertDto.setStatusKrbnRs("T");
				} else if (statusKorban
						.equalsIgnoreCase("Korban Ttd Surat Pernyataan")) {
					insertDto.setStatusKrbnRs("S");
				} else if (statusKorban.equalsIgnoreCase("-")) {
					insertDto.setStatusKrbnRs("-");
				}

				// tindak lanjut
				DateFormat tgl = new SimpleDateFormat("dd/MM/yyyy");
				DateFormat waktu = new SimpleDateFormat("HH:mm");
				String time = null;
				if (getTimeTL() == null) {time = waktu.format(new Date());
				} else {time = waktu.format(getTimeTL());
				}
				String date = tgl.format(getDateTL());
				String tglKejadian = date + " " + time;
				Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm")
						.parse(tglKejadian);

				// ID TL
				Map<String, Object> map2 = new HashMap<>();
				map2.put("noRegister", noReg);
				RestResponse restTl = callWs(WS_URI + "/tindakLanjut", map2,
						HttpMethod.POST);
				String idTlRegister = null;
				int k = 0;
				try {
					listTindakLanjutPrintDto = JsonUtil.mapJsonToListObject(
							restTl.getContents(), PlTindakLanjutDto.class);
					if (listTindakLanjutPrintDto.size() > 0) {
						for (int j = 0; j <= listTindakLanjutPrintDto.size(); j++) {
							k = j++;
						}
						idTlRegister = noReg + "." + k;
						insertDto.setIdTlRegisterTL(idTlRegister);
					} else {
						idTlRegister = noReg + "." + "1";
						insertDto.setIdTlRegisterTL(idTlRegister);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

				insertDto.setTglTindakLanjutTL(date1);
				insertDto.setTindakLanjutFlagTL(tindakLanjutDetailDto.getRvLowValue());
				insertDto.setCatatanTindakLanjutTL(getKeteranganTL());
				insertDto.setCreatedByTL(userSession.getLoginID());
				insertDto.setCreatedDateTL(new Date());
				insertDto.setPetugasSurveyTL(userDto.getUserName());
				insertDto.setDilimpahkanKeTL(kantorPengajuanDto.getKodeKantorJr());
				insertDto.setNoSuratPanggilanTL(noSuratPanggilanTL);
				insertDto.setNoSuratJaminanTL(noSuratJaminanTL);

				RestResponse restSave = callWs(WS_URI + "/saveTL",
						insertDto, HttpMethod.POST);

				if (restSave.getStatus() == CommonConstants.OK_REST_STATUS) {
					showInfoMsgBox(restSave.getMessage());
					// map = oMapper.convertValue(rest.getContents(),
					// Map.class);
					// setNoRegForPrintTL((String) map.get("noRegister"));
					setFlagTindakLanjut(false);
					BindUtils.postNotifyChange(null, null, this,
							"flagTindakLanjut");
					BindUtils.postNotifyChange(null, null, this,
							"noRegForPrintTL");

					loadTindakLanjutPrint();
					// setDateTL(null);
					// setTimeTL(null);
					// setSearchTindakLanjutDetail("");
					// setKeteranganTL("");
					// insertDto = new PlRegisterSementaraDto();
					setFlagPengajuanButton(true);
					// BindUtils.postNotifyChange(null, null, this, "dateTL");
					// BindUtils.postNotifyChange(null, null, this, "timeTL");
					// BindUtils.postNotifyChange(null, null, this,
					// "searchTindakLanjutDetail");
					// BindUtils.postNotifyChange(null, null, this,
					// "keteranganTL");
					BindUtils.postNotifyChange(null, null, this, "insertDto");
					BindUtils.postNotifyChange(null, null, this,
							"registerSementaraDto");
					BindUtils.postNotifyChange(null, null, this,
							"flagPengajuanButton");

				} else {
					showErrorMsgBox(restSave.getMessage());
				}
			}
		}
	}
	
	@Command
	public void saveRegisterDanTl() throws ParseException {
		if(tindakLanjutDetailDto == null||tindakLanjutDetailDto.getRvLowValue()==null){
			showInfoMsgBox("Jenis Tindak Lanjut tidak boleh kosong!", "");			
		}else if(isFlagEditTl()){
			Map<String, Object> mapInput = new HashMap<>();
			mapInput.put("idTl", tindakLanjutPrintDto.getIdTlRegister());
			RestResponse restTl = callWs(WS_URI+"/plTindakLanjutByIdTl", mapInput, HttpMethod.POST);
			try{
				listTindakLanjutPrintDto = JsonUtil.mapJsonToListObject(restTl.getContents(), PlTindakLanjutDto.class);
				for(PlTindakLanjutDto a : listTindakLanjutPrintDto){
					tindakLanjutPrintDto = new PlTindakLanjutDto();
					tindakLanjutPrintDto = a;
					tindakLanjutPrintDto.setDilimpahkanKe(kantorPengajuanDto.getKodeKantorJr()==null?"":kantorPengajuanDto.getKodeKantorJr());
					tindakLanjutPrintDto.setNoSuratPanggilan(getNoSuratPanggilanTL()==null?"":getNoSuratPanggilanTL());
					tindakLanjutPrintDto.setPetugasSurvey(userDto.getLogin()==null?"":userDto.getLogin());
					tindakLanjutPrintDto.setNoSuratJaminan(getNoSuratJaminanTL()==null?"":getNoSuratJaminanTL());
					tindakLanjutPrintDto.setCatatanTindakLanjut(getKeteranganTL());
					tindakLanjutPrintDto.setLastUpdatedBy(userSession.getLoginID());
					tindakLanjutPrintDto.setLastUpdatedDate(new Date());
					tindakLanjutPrintDto.setIdJaminanTlRs(a.getIdJaminanTlRs());
				}				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			RestResponse restSave = new RestResponse();
			restSave = callWs(WS_URI + "/saveTindakLanjut", tindakLanjutPrintDto, HttpMethod.POST);

			if (restSave.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(restSave.getMessage());
				setFlagTindakLanjut(false);
				setFlagEditTl(false);
				loadTindakLanjutPrint();
				setFlagPengajuanButton(true);
				BindUtils.postNotifyChange(null, null, this,"flagTindakLanjut");
				BindUtils.postNotifyChange(null, null, this,"flagEditTl");
				BindUtils.postNotifyChange(null, null, this,"flagPengajuanButton");

			} else {
				showErrorMsgBox(restSave.getMessage());
			}
		}else{
//			if (tindakLanjutDetailDto.getRvLowValue().equalsIgnoreCase("5")
//					&& getListTindakLanjutPrintDto().size() == 0) {
//				showInfoMsgBox("Isi Data Pengajuan terlebih dahulu", "");
//			} else 
			if(keteranganTL == null){
				showInfoMsgBox("Keterangan Tindak Lanjut tidak boleh kosong!", "");				
			} else {
				if(registerSementaraDto.getNoRegister()==null){
//					testSaveTl();
//					===================================SAVE REGISTER SEMENTARA===================================
					// no register
					String a = userSession.getKantor().substring(0, 2);
					String b = userSession.getKantor().substring(3, 5);
					Date date2 = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
					String c = sdf.format(date2);
					String d = a + b + "-" + c;
					System.err.println("luthfi " + d);
					String noReg = null;
					Map<String, Object> map = new HashMap<>();
					map.put("noReg", d);
					RestResponse rest = callWs(WS_URI + "/getRegisterByNoRegister",
							map, HttpMethod.POST);
					try {
						listRegisterSementaraDto = JsonUtil.mapJsonToListObject(
								rest.getContents(), PlRegisterSementaraDto.class);
						if (listRegisterSementaraDto.size() != 0) {
							for (PlRegisterSementaraDto dto : listRegisterSementaraDto) {
								noReg = dto.getNoRegister();
							}
							System.err.println("luthfi5 " + noReg);
							if (noReg != null) {
								String incVal = noReg.substring(10, 16);
								int countVal = Integer.valueOf(incVal) + 1;
								noReg = d + "-" + String.format("%06d", countVal);
								registerSementaraDto.setNoRegister(noReg);
								insertDto.setNoRegister(noReg);
							}
						} else {
							noReg = d + "-" + "000001";
							registerSementaraDto.setNoRegister(noReg);
							insertDto.setNoRegister(noReg);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					insertDto.setKodeKantorJr(userSession.getKantor());
					insertDto.setIdKorbanKecelakaan(dataLakaDto.getIdKorbanKecelakaan());
					insertDto.setTglRegister(registerSementaraDto.getTglRegister());
					insertDto.setAsalBerkasFlag(asalBerkasDetailDto.getRvLowValue());
					insertDto.setNamaPemohon(registerSementaraDto.getNamaPemohon());
					insertDto.setKodeHubunganKorban(jenisHubunganDto.getRvLowValue());
					insertDto.setJenisIdentitas(jenisIdentitasDto.getRvLowValue());
					insertDto.setNoIdentitas(registerSementaraDto.getNoIdentitas());
					insertDto.setTelpPemohon(registerSementaraDto.getTelpPemohon());
					insertDto.setAlamatPemohon(registerSementaraDto.getAlamatPemohon());
					System.out.println("DataLaka : "+JsonUtil.getJson(dataLakaDto));
					System.out.println("Sifat cidera : "+JsonUtil.getJson(sifatCideraDetailDto));
					if(sifatCideraDetailDto.getRvLowValue()==null){
						insertDto.setCideraKorban(dataLakaDto.getKodeSifatCidera());
					}else{
						insertDto.setCideraKorban(sifatCideraDto.getRvLowValue());
					}
					insertDto.setCideraKorban(sifatCideraDto.getRvLowValue()==null?dataLakaDto.getKodeSifatCidera():sifatCideraDto.getRvLowValue());
					if(rumahSakitDto==null||rumahSakitDto.getKodeRumahsakit()==null){
						
					}else if(rumahSakitDto!=null||rumahSakitDto.getKodeRumahsakit()!=null){
						insertDto.setKodeRumahSakit(rumahSakitDto.getKodeRumahsakit());						
					}
					insertDto.setTglMasukRs(registerSementaraDto.getTglMasukRs());
					insertDto.setJumlahPengajuan1(registerSementaraDto.getJumlahPengajuan1());
					insertDto.setJumlahPengajuan2(registerSementaraDto.getJumlahPengajuan2());
					insertDto.setCreatedBy(userSession.getLoginID());
					insertDto.setCreatedDate(new Date());
					insertDto.setJmlAmbl(registerSementaraDto.getJmlAmbl());
					insertDto.setJmlP3k(registerSementaraDto.getJmlP3k());
					if (statusKorban.equalsIgnoreCase("Terbit Tagihan Dari RS")) {
						insertDto.setStatusKrbnRs("T");
					} else if (statusKorban
							.equalsIgnoreCase("Korban Ttd Surat Pernyataan")) {
						insertDto.setStatusKrbnRs("S");
					} else if (statusKorban.equalsIgnoreCase("-")) {
						insertDto.setStatusKrbnRs("-");
					}

//					===================================SAVE TINDAK LANJUT===================================
					// tindak lanjut
					DateFormat tgl = new SimpleDateFormat("dd/MM/yyyy");
					DateFormat waktu = new SimpleDateFormat("HH:mm");
					String time = null;
					String date = null;
					if (getTimeTL() == null) {
						time = waktu.format(new Date());
					} else {
						time = waktu.format(getTimeTL());
					}
					if(getDateTL()==null){
						date = tgl.format(new Date());
					}else{
						date = tgl.format(getDateTL());
					}
					String tglKejadian = date + " " + time;
					Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm")
							.parse(tglKejadian);

					// ID TL
					Map<String, Object> map2 = new HashMap<>();
					map2.put("noRegister", noReg);
					RestResponse restTl = callWs(WS_URI + "/tindakLanjut", map2,
							HttpMethod.POST);
					String idTlRegister = null;
					String idTlRegisterTemp = null;
					int k = 0;
					try {
						listTindakLanjutPrintDto = JsonUtil.mapJsonToListObject(
								restTl.getContents(), PlTindakLanjutDto.class);
						if (listTindakLanjutPrintDto.size() > 0) {
							System.err.println("luthfi1 "+listTindakLanjutPrintDto.size());
							for (PlTindakLanjutDto tl : listTindakLanjutPrintDto) {
								idTlRegisterTemp = tl.getIdTlRegister();
							}
							System.err.println("luthfi1 "+k);
							String incVal = idTlRegisterTemp.substring(17);
							int countVal = Integer.valueOf(incVal)+1;
							idTlRegister = registerSementaraDto.getNoRegister() + "." + countVal;
							tindakLanjutPrintDto.setIdTlRegister(idTlRegister);
						} else {
							idTlRegister = noReg + "." + "1";
							insertDto.setIdTlRegisterTL(idTlRegister);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}

					insertDto.setTglTindakLanjutTL(date1);
					insertDto.setNoRegisterTL(noReg);
					insertDto.setTindakLanjutFlagTL(tindakLanjutDetailDto.getRvLowValue());
					insertDto.setCatatanTindakLanjutTL(getKeteranganTL());
					insertDto.setCreatedByTL(userSession.getLoginID());
					insertDto.setCreatedDateTL(new Date());
					insertDto.setPetugasSurveyTL(userDto.getUserName());
					insertDto.setDilimpahkanKeTL(kantorPengajuanDto.getKodeKantorJr());
					insertDto.setNoSuratPanggilanTL(noSuratPanggilanTL);
					insertDto.setNoSuratJaminanTL(noSuratJaminanTL);
					
//					===================================SAVE TL RS===================================
					if(tindakLanjutDetailDto.getRvLowValue().equalsIgnoreCase("5")){
						if(registerSementaraDto.getTglMasukRs()==null){
							showInfoMsgBox("Tgl Masuk RS pada TAB Data Pengajuan tidak boleh kosong!", "");
						}else if(sifatCideraDto.getRvLowValue()==null){
							showInfoMsgBox("Sifat Cidera Korban tidak boleh kosong!", "");
						}
						String noSuratJaminan = null;
						String idJaminan = null;
						String idJaminanTemp = null;
						String noSuratJaminanTemp = null;
						List<PlTlRDto> listPlTlRDtosByNoRegister = new ArrayList<>();
						Date tahun = new Date();
						SimpleDateFormat year = new SimpleDateFormat("yyyy");
						
						Map<String, Object> mapPlTlRs = new HashMap<>();
						mapPlTlRs.put("noRegister", insertDto.getNoRegister().substring(0,9));
						RestResponse plTlSuratJaminan = callWs(WS_URI_GL_RS + "/getMaxNoSurat",	mapPlTlRs, HttpMethod.POST);
						try{
							listPlTlRDtosByNoRegister = JsonUtil.mapJsonToListObject(plTlSuratJaminan.getContents(), PlTlRDto.class);
							if(listPlTlRDtosByNoRegister.size()!=0){
								for(PlTlRDto o : listPlTlRDtosByNoRegister){
									String suratJaminan[] = o.getNoSuratJaminan().split("/");
									noSuratJaminanTemp = suratJaminan[2];
								}								
								int countValNoSurat = Integer.valueOf(noSuratJaminanTemp)+1;
								noSuratJaminan = "PP/R/"+String.format("%05d", countValNoSurat)+"/GL/"+year.format(tahun);
							}else{
								noSuratJaminan = "PP/R/00001/GL/"+year.format(tahun);
							}
						}catch(Exception e){
							e.printStackTrace();
						}

//						=========================================ID JAMINAN===================================================
						Map<String, Object> mapPlTlRsByNoRegister = new HashMap<>();
						mapPlTlRsByNoRegister.put("noRegister", insertDto.getNoRegister());
						RestResponse plTlRsByNoRegister = callWs(WS_URI + "/plTlByNoRegister",mapPlTlRsByNoRegister, HttpMethod.POST);
						try{
							listPlTlRDtosByNoRegister = JsonUtil.mapJsonToListObject(plTlRsByNoRegister.getContents(), PlTlRDto.class);
							if(listPlTlRDtosByNoRegister.size()!=0){
								for(PlTlRDto o : listPlTlRDtosByNoRegister){
									idJaminanTemp = o.getIdJaminan();
								}
								String val[] = idJaminanTemp.split("-");
								String noJaminan = val[3];
								String noUrut = val[4];
								int countNoUrut = Integer.valueOf(noUrut)+1;
								idJaminan = insertDto.getNoRegister()+"-"+noJaminan+"-"+String.format("%02d", countNoUrut);
							}else{
								Map<String, Object> mapGetSize = new HashMap<>();
								mapGetSize.put("noRegister", insertDto.getNoRegister().substring(0,9));
								RestResponse getSizeRest = callWs(WS_URI_GL_RS + "/getSizePlTlRs",mapGetSize, HttpMethod.POST);
								List<PlTlRDto> listDistinct = new ArrayList<>();
								try{
									listDistinct = JsonUtil.mapJsonToListObject(getSizeRest.getContents(), PlTlRDto.class);
									if(listDistinct.size()==0){
										idJaminan = insertDto.getNoRegister()+"-001-00";										
									}else{
										int countNoJaminan = listDistinct.size()+1;
										idJaminan = insertDto.getNoRegister()+"-"+countNoJaminan+"-00";
									}
								}catch(Exception e){
									e.printStackTrace();
								}
							}
						}catch(Exception e){
							e.printStackTrace();
						}
							
						insertDto.setIdJaminanTlRs(idJaminan==null?"":idJaminan);
						insertDto.setNoRegisterTlRs(insertDto.getNoRegister()==null?"":insertDto.getNoRegister());
						insertDto.setKodeKantorJrTlRs(userSession.getKantor()==null?"":userSession.getKantor());
						insertDto.setNoSuratJaminanTlRs(noSuratJaminan==null?"":noSuratJaminan);
						insertDto.setKodeRumahSakitTlRs(rumahSakitDto.getKodeRumahsakit()==null?"":rumahSakitDto.getKodeRumahsakit());
						insertDto.setTglMasukRsTlRs(registerSementaraDto.getTglMasukRs()==null?null:registerSementaraDto.getTglMasukRs());
						if(registerSementaraDto.getJumlahPengajuan2()==null){
							insertDto.setJumlahPengajuan1TlRs(registerSementaraDto.getJumlahPengajuan1()==null?new BigDecimal(0):registerSementaraDto.getJumlahPengajuan1());							
						}else{
							insertDto.setJumlahPengajuan1TlRs(registerSementaraDto.getJumlahPengajuan2()==null?new BigDecimal(0):registerSementaraDto.getJumlahPengajuan2());														
						}
						insertDto.setJmlAmblTlRs(registerSementaraDto.getJmlAmbl()==null?new BigDecimal(0):registerSementaraDto.getJmlAmbl());
						insertDto.setJmlP3kTlRs(registerSementaraDto.getJmlP3k()==null?new BigDecimal(0):registerSementaraDto.getJmlP3k());
						insertDto.setFlagBayarTlRs("0");
						insertDto.setFlagBayarAwalTlRs("0");
						if (statusKorban.equalsIgnoreCase("Terbit Tagihan Dari RS")) {
							insertDto.setJenisTagihanTlRs("T");
						} else if (statusKorban.equalsIgnoreCase("Korban Ttd Surat Pernyataan")) {
							insertDto.setJenisTagihanTlRs("S");
						} else if (statusKorban.equalsIgnoreCase("-")) {
							insertDto.setJenisTagihanTlRs("0");
						}
						insertDto.setOtorisasiAwalTlRs("0");
						insertDto.setOtorisasiAjuSmtTlRs("0");
						insertDto.setFlagLanjutanTlRs("0");
						insertDto.setFlagLimpahanTlRs("0");
						insertDto.setCreatedByTlRs(userSession.getLoginID());
						insertDto.setCreatedDateTlRs(new Date());

					}
//					=========================================================================================================
					RestResponse restSave = callWs(WS_URI + "/saveRegisterTl",
							insertDto, HttpMethod.POST);

					if (restSave.getStatus() == CommonConstants.OK_REST_STATUS) {
						showInfoMsgBox(restSave.getMessage());
						setFlagTindakLanjut(false);
						BindUtils.postNotifyChange(null, null, this,"flagTindakLanjut");
						BindUtils.postNotifyChange(null, null, this,"noRegForPrintTL");

						loadTindakLanjutPrint();
						setFlagPengajuanButton(true);
						BindUtils.postNotifyChange(null, null, this,"insertDto");
						BindUtils.postNotifyChange(null, null, this,"registerSementaraDto");
						BindUtils.postNotifyChange(null, null, this,"flagPengajuanButton");

					} else {
						showErrorMsgBox(restSave.getMessage());
					}
				}else if(registerSementaraDto.getNoRegister()!=null){
					// tindak lanjut
					DateFormat tgl = new SimpleDateFormat("dd/MM/yyyy");
					DateFormat waktu = new SimpleDateFormat("HH:mm");
					String time = null;
					if (getTimeTL() == null) {time = waktu.format(new Date());
					} else {time = waktu.format(getTimeTL());
					}
					String date = tgl.format(getDateTL());
					String tglKejadian = date + " " + time;
					Date date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm")
							.parse(tglKejadian);

					// ID TL
					Map<String, Object> map2 = new HashMap<>();
					map2.put("noRegister", registerSementaraDto.getNoRegister());
					RestResponse restTl = callWs(WS_URI + "/tindakLanjut", map2,
							HttpMethod.POST);
					String idTlRegister = null;
					String idTlRegisterTemp = null;
					int k = 0;
					try {
						listTindakLanjutPrintDto = JsonUtil.mapJsonToListObject(
								restTl.getContents(), PlTindakLanjutDto.class);
						if (listTindakLanjutPrintDto.size() > 0) {
							System.err.println("luthfi1 "+listTindakLanjutPrintDto.size());
							for (PlTindakLanjutDto a : listTindakLanjutPrintDto) {
								idTlRegisterTemp = a.getIdTlRegister();
							}
							System.err.println("luthfi1 "+k);
							String incVal = idTlRegisterTemp.substring(17);
							int countVal = Integer.valueOf(incVal)+1;
							idTlRegister = registerSementaraDto.getNoRegister() + "." + countVal;
							tindakLanjutPrintDto.setIdTlRegister(idTlRegister);
						} else {
							System.err.println("luthfi2 ");
							idTlRegister = registerSementaraDto.getNoRegister() + "." + "1";
							tindakLanjutPrintDto.setIdTlRegister(idTlRegister);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					System.out.println("user dto : ");
					System.out.println(JsonUtil.getJson(userDto));
//					return;
					
					tindakLanjutPrintDto.setTglTindakLanjut(date1);
					tindakLanjutPrintDto.setNoRegister(registerSementaraDto.getNoRegister());
					tindakLanjutPrintDto.setTindakLanjutFlag(tindakLanjutDetailDto.getRvLowValue());
					tindakLanjutPrintDto.setCatatanTindakLanjut(getKeteranganTL());
					tindakLanjutPrintDto.setCreatedBy(userSession.getLoginID());
					tindakLanjutPrintDto.setCreatedDate(new Date());
					tindakLanjutPrintDto.setPetugasSurvey(userDto.getUserName());
					tindakLanjutPrintDto.setDilimpahkanKe(kantorPengajuanDto.getKodeKantorJr());
					tindakLanjutPrintDto.setNoSuratPanggilan(noSuratPanggilanTL);
					tindakLanjutPrintDto.setNoSuratJaminan(noSuratJaminanTL);
					
//						===================================SAVE TL RS===================================
						if(tindakLanjutDetailDto.getRvLowValue().equalsIgnoreCase("5")){
							if(registerSementaraDto.getTglMasukRs()==null){
								showInfoMsgBox("Tgl Masuk RS pada TAB Data Pengajuan tidak boleh kosong!", "");
							}else if(sifatCideraDto.getRvLowValue()==null){
								showInfoMsgBox("Sifat Cidera Korban tidak boleh kosong!", "");
							}
							String noSuratJaminan = null;
							String idJaminan = null;
							String idJaminanTemp = null;
							String noSuratJaminanTemp = null;
							List<PlTlRDto> listPlTlRDtosByNoRegister = new ArrayList<>();
							Date tahun = new Date();
							SimpleDateFormat year = new SimpleDateFormat("yyyy");
							
							Map<String, Object> mapPlTlRs = new HashMap<>();
							mapPlTlRs.put("noRegister", insertDto.getNoRegister().substring(0,9));
							RestResponse plTlSuratJaminan = callWs(WS_URI_GL_RS + "/getMaxNoSurat",	mapPlTlRs, HttpMethod.POST);
							try{
								listPlTlRDtosByNoRegister = JsonUtil.mapJsonToListObject(plTlSuratJaminan.getContents(), PlTlRDto.class);
								if(listPlTlRDtosByNoRegister.size()!=0){
									for(PlTlRDto o : listPlTlRDtosByNoRegister){
										String suratJaminan[] = o.getNoSuratJaminan().split("/");
										noSuratJaminanTemp = suratJaminan[2];
									}								
									int countValNoSurat = Integer.valueOf(noSuratJaminanTemp)+1;
									noSuratJaminan = "PP/R/"+String.format("%05d", countValNoSurat)+"/GL/"+year.format(tahun);
								}else{
									noSuratJaminan = "PP/R/00001/GL/"+year.format(tahun);
								}
							}catch(Exception e){
								e.printStackTrace();
							}

//							=========================================ID JAMINAN===================================================
							Map<String, Object> mapPlTlRsByNoRegister = new HashMap<>();
							mapPlTlRsByNoRegister.put("noRegister", insertDto.getNoRegister());
							RestResponse plTlRsByNoRegister = callWs(WS_URI + "/plTlByNoRegister",mapPlTlRsByNoRegister, HttpMethod.POST);
							try{
								listPlTlRDtosByNoRegister = JsonUtil.mapJsonToListObject(plTlRsByNoRegister.getContents(), PlTlRDto.class);
								if(listPlTlRDtosByNoRegister.size()!=0){
									for(PlTlRDto o : listPlTlRDtosByNoRegister){
										idJaminanTemp = o.getIdJaminan();
									}
									String val[] = idJaminanTemp.split("-");
									String noJaminan = val[3];
									String noUrut = val[4];
									int countNoUrut = Integer.valueOf(noUrut)+1;
									idJaminan = insertDto.getNoRegister()+"-"+noJaminan+"-"+String.format("%02d", countNoUrut);
								}else{
									Map<String, Object> mapGetSize = new HashMap<>();
									mapGetSize.put("noRegister", insertDto.getNoRegister().substring(0,9));
									RestResponse getSizeRest = callWs(WS_URI_GL_RS + "/getSizePlTlRs",mapGetSize, HttpMethod.POST);
									List<PlTlRDto> listDistinct = new ArrayList<>();
									try{
										listDistinct = JsonUtil.mapJsonToListObject(getSizeRest.getContents(), PlTlRDto.class);
										if(listDistinct.size()==0){
											idJaminan = insertDto.getNoRegister()+"-001-00";										
										}else{
											int countNoJaminan = listDistinct.size()+1;
											idJaminan = insertDto.getNoRegister()+"-"+countNoJaminan+"-00";
										}
									}catch(Exception e){
										e.printStackTrace();
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
								
							tindakLanjutPrintDto.setIdJaminanTlRs(idJaminan==null?"":idJaminan);
							tindakLanjutPrintDto.setNoRegisterTlRs(insertDto.getNoRegister()==null?"":insertDto.getNoRegister());
							tindakLanjutPrintDto.setKodeKantorJrTlRs(userSession.getKantor()==null?"":userSession.getKantor());
							tindakLanjutPrintDto.setNoSuratJaminanTlRs(noSuratJaminan==null?"":noSuratJaminan);
							tindakLanjutPrintDto.setKodeRumahSakitTlRs(rumahSakitDto.getKodeRumahsakit()==null?"":rumahSakitDto.getKodeRumahsakit());
							tindakLanjutPrintDto.setTglMasukRsTlRs(registerSementaraDto.getTglMasukRs()==null?null:registerSementaraDto.getTglMasukRs());
							if(registerSementaraDto.getJumlahPengajuan2()==null){
								tindakLanjutPrintDto.setJumlahPengajuan1TlRs(registerSementaraDto.getJumlahPengajuan1()==null?new BigDecimal(0):registerSementaraDto.getJumlahPengajuan1());							
							}else{
								tindakLanjutPrintDto.setJumlahPengajuan1TlRs(registerSementaraDto.getJumlahPengajuan2()==null?new BigDecimal(0):registerSementaraDto.getJumlahPengajuan2());														
							}
							tindakLanjutPrintDto.setJmlAmblTlRs(registerSementaraDto.getJmlAmbl()==null?new BigDecimal(0):registerSementaraDto.getJmlAmbl());
							tindakLanjutPrintDto.setJmlP3kTlRs(registerSementaraDto.getJmlP3k()==null?new BigDecimal(0):registerSementaraDto.getJmlP3k());
							tindakLanjutPrintDto.setFlagBayarTlRs("0");
							tindakLanjutPrintDto.setFlagBayarAwalTlRs("0");
							if (statusKorban.equalsIgnoreCase("Terbit Tagihan Dari RS")) {
								tindakLanjutPrintDto.setJenisTagihanTlRs("T");
							} else if (statusKorban.equalsIgnoreCase("Korban Ttd Surat Pernyataan")) {
								tindakLanjutPrintDto.setJenisTagihanTlRs("S");
							} else if (statusKorban.equalsIgnoreCase("-")) {
								tindakLanjutPrintDto.setJenisTagihanTlRs("0");
							}
							tindakLanjutPrintDto.setOtorisasiAwalTlRs("0");
							tindakLanjutPrintDto.setOtorisasiAjuSmtTlRs("0");
							tindakLanjutPrintDto.setFlagLanjutanTlRs("0");
							tindakLanjutPrintDto.setFlagLimpahanTlRs("0");
							tindakLanjutPrintDto.setCreatedByTlRs(userSession.getLoginID());
							tindakLanjutPrintDto.setCreatedDateTlRs(new Date());
						}
//						=========================================================================================================
					RestResponse restSave = callWs(WS_URI + "/saveTindakLanjut", tindakLanjutPrintDto, HttpMethod.POST);

					if (restSave.getStatus() == CommonConstants.OK_REST_STATUS) {
						showInfoMsgBox(restSave.getMessage());
						setFlagTindakLanjut(false);
						BindUtils.postNotifyChange(null, null, this,"flagTindakLanjut");
						BindUtils.postNotifyChange(null, null, this,"noRegForPrintTL");

						loadTindakLanjutPrint();
						setFlagPengajuanButton(true);
						BindUtils.postNotifyChange(null, null, this,"insertDto");
						BindUtils.postNotifyChange(null, null, this,"registerSementaraDto");
						BindUtils.postNotifyChange(null, null, this,"flagPengajuanButton");

					} else {
						showErrorMsgBox(restSave.getMessage());
					}

				}
			}
		}
	}


	@Command
	public void saveRegisterPengajuan() {
		RestResponse rest = new RestResponse();
		String status = "";
		if (getStatusKorban().equalsIgnoreCase("Terbit Tagihan Dari RS")) {
			status = "T";
		} else if (getStatusKorban().equalsIgnoreCase(
				"Korban Ttd Surat Pernyataan")) {
			status = "S";
		} else if (getStatusKorban().equalsIgnoreCase("-")) {
			status = "";
		}

		// DateFormat tgl = new SimpleDateFormat("dd/MM/yyyy");
		// DateFormat waktu = new SimpleDateFormat("HH:mm");
		// String time = waktu.format(getTimeTL());
		// String date = tgl.format(getDateTL());
		// String tglKejadian;
		// Date date1 = new Date();
		// ObjectMapper oMapper = new ObjectMapper();
		// Map<String, Object> map = new HashMap<>();
		// tglKejadian = date+" "+time;
		// try {
		// date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm").parse(tglKejadian);
		// } catch (ParseException e) {
		// e.printStackTrace();
		// }

		if (getSifatCideraDetailDto() == null || getRumahSakitDto() == null
				|| getTglMasukRs() == null || getNamaPemohon() == null
				|| getJenisHubunganDto() == null
				|| getJenisIdentitasDto() == null || getNoIdentitas() == null
				|| getNoIdentitas().equalsIgnoreCase("")
				|| getAlamatPemohon() == null
				|| getAlamatPemohon().equalsIgnoreCase("")
				|| getTelpPemohon() == null
				|| getTelpPemohon().equalsIgnoreCase("")) {
			showInfoMsgBox("Data Harus Diisi", "");
		} else {

			// register sementara
			insertDto.setKodeKantorJr(userSession.getKantor());
			insertDto.setIdKorbanKecelakaan(dataLakaDto.getIdKorbanKecelakaan());
			insertDto.setAsalBerkasFlag(getAsalBerkasDetailDto().getRvLowValue());
			insertDto.setTglRegister(getDetailTglRegister());
			insertDto.setNamaPemohon(registerSementaraDto.getNamaPemohon());
			insertDto.setKodeHubunganKorban(jenisHubunganDto.getRvLowValue());
			insertDto.setJenisIdentitas(jenisIdentitasDto.getRvLowValue());
			insertDto.setNoIdentitas(getNoIdentitas());
			insertDto.setAlamatPemohon(getAlamatPemohon());
			insertDto.setTelpPemohon(getTelpPemohon());
			insertDto.setCideraKorban(sifatCideraDetailDto.getRvLowValue());
			insertDto.setKodeRumahSakit(getRumahSakitDto().getKodeRumahsakit());
			insertDto.setCreatedBy(userSession.getLoginID());
			insertDto.setCreatedDate(new Date());
			insertDto.setStatusKrbnRs(status);
			insertDto.setJumlahPengajuan1(getJmlPengajuanLuka());
			insertDto.setJumlahPengajuan2(getJmlPengajuanKubur());
			insertDto.setJmlAmbl(getJmlPengajuanAmbl());
			insertDto.setJmlP3k(getJmlPengajuanP3K());

			// //tindak lanjut
			// insertDto.setTglTindakLanjutTL(date1);
			// insertDto.setTindakLanjutFlagTL(getTindakLanjutDetailDto().getRvLowValue());
			// insertDto.setCatatanTindakLanjutTL(getKeteranganTL());
			// insertDto.setCreatedByTL(userSession.getLoginID());
			// insertDto.setCreatedDateTL(new Date());
			// insertDto.setPetugasSurveyTL(getUserDto().getUserName());
			// insertDto.setDilimpahkanKeTL(kantorPengajuanDto.getKodeKantorJr());
			// insertDto.setNoSuratPanggilanTL(getNoSuratPanggilanTL());
			// insertDto.setNoSuratJaminanTL(getNoSuratJaminanTL());

			// pengajuan
			insertDto.setIdKecelakaanPS(getDataLakaDto().getIdKecelakaan());
			insertDto.setIdKorbanKecelakaanPS(getDataLakaDto().getIdKorbanKecelakaan());
			insertDto.setKodeJaminanPS(getDataLakaDto().getKodeJaminan());
			insertDto.setDiajukanDiPS(getDataLakaDto().getKodeKantorJr());
			insertDto.setNamaPemohonPS(getNamaPemohon());
			insertDto.setJenisIdentitasPS(getJenisIdentitasDto().getRvLowValue());
			insertDto.setNoIdentitasPS(getNoIdentitas());
			insertDto.setAlamatPemohonPS(getAlamatPemohon());
			if (getSifatCideraDetailDto().getRvLowValue() == "01"
					|| getSifatCideraDetailDto().getRvLowValue()
							.equalsIgnoreCase("01")) {
				insertDto.setJumlahPengajuanPenguburanPS(getJmlPengajuanLuka());

			} else {
				insertDto.setJumlahPengajuanLukalukaPS(getJmlPengajuanLuka());
				insertDto
						.setJumlahPengajuanPenguburanPS(getJmlPengajuanKubur());
			}
			insertDto.setJumlahPengajuanMeninggalPS(new BigDecimal(0));
			insertDto.setCideraKorbanPS(getSifatCideraDetailDto()
					.getRvLowValue());
			insertDto.setKodeHubunganKorbanPS(getJenisHubunganDto()
					.getRvLowValue());
			insertDto.setTglPengajuanPS(new Date());

			// if
			// (getTindakLanjutDetailDto().getRvLowValue().equalsIgnoreCase("5"))
			// {
			// insertDto.setJenisSave("Rumah Sakit");
			// } else {
			// insertDto.setJenisSave("Non Rumah Sakit");
			// }

			insertDto.setLingkupJaminan(getDataLakaDto().getLingkupJaminan());

			rest = callWs(WS_URI + "/savePS", insertDto, HttpMethod.POST);

			if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(rest.getMessage());
			} else {
				showErrorMsgBox(rest.getMessage());
			}
		}
	}

	
	@Command
	public void saveRegisterSementara(){
		RestResponse rest = new RestResponse();
		try {
			insertDto.setNoRegister(registerSementaraDto.getNoRegister());
			insertDto.setKodeKantorJr(userSession.getKantor());
			if(dataLakaDto.getIdKorbanKecelakaan() ==null){
				insertDto.setIdKorbanKecelakaan(registerSementaraDto.getIdKorbanKecelakaan());								
			}else if(dataLakaDto.getIdKorbanKecelakaan()!=null){
				insertDto.setIdKorbanKecelakaan(dataLakaDto.getIdKorbanKecelakaan());				
			}
			insertDto.setTglRegister(registerSementaraDto.getTglRegister());
			insertDto.setAsalBerkasFlag(asalBerkasDetailDto.getRvLowValue());
			insertDto.setNamaPemohon(registerSementaraDto.getNamaPemohon());
			insertDto.setKodeHubunganKorban(jenisHubunganDto.getRvLowValue());
			insertDto.setJenisIdentitas(jenisIdentitasDto.getRvLowValue());
			insertDto.setNoIdentitas(registerSementaraDto.getNoIdentitas());
			insertDto.setTelpPemohon(registerSementaraDto.getTelpPemohon());
			insertDto.setAlamatPemohon(registerSementaraDto.getAlamatPemohon());
			if(sifatCideraDetailDto.getRvLowValue()==null){
				insertDto.setCideraKorban(dataLakaDto.getKodeSifatCidera());
			}else if(sifatCideraDetailDto.getRvLowValue()!=null){
				insertDto.setCideraKorban(sifatCideraDetailDto.getRvLowValue());
			}
			System.out.println("Data Laka Dto : \n"+JsonUtil.getJson(dataLakaDto));
			System.out.println("sifat cidera detail dto : \n"+JsonUtil.getJson(sifatCideraDetailDto));
			insertDto.setTglMasukRs(registerSementaraDto.getTglMasukRs());
			if(rumahSakitDto==null||rumahSakitDto.getKodeRumahsakit()==null){
				
			}else if(rumahSakitDto!=null||rumahSakitDto.getKodeRumahsakit()!=null){
				insertDto.setKodeRumahSakit(rumahSakitDto.getKodeRumahsakit());						
			}
			insertDto.setJumlahPengajuan1(registerSementaraDto.getJumlahPengajuan1());
			insertDto.setJumlahPengajuan2(registerSementaraDto.getJumlahPengajuan2());
			insertDto.setLastUpdatedBy(userSession.getLoginID());
			insertDto.setLastUpdatedDate(new Date());
			insertDto.setJmlAmbl(registerSementaraDto.getJmlAmbl());
			insertDto.setJmlP3k(registerSementaraDto.getJmlP3k());
			if (statusKorban.equalsIgnoreCase("Terbit Tagihan Dari RS")) {
				insertDto.setStatusKrbnRs("T");
			} else if (statusKorban
					.equalsIgnoreCase("Korban Ttd Surat Pernyataan")) {
				insertDto.setStatusKrbnRs("S");
			} else if (statusKorban.equalsIgnoreCase("-")) {
				insertDto.setStatusKrbnRs("-");
			}
			
			rest = callWs(WS_URI + "/saveRegisterSementara", insertDto, HttpMethod.POST);

			if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(rest.getMessage());
			} else {
				showErrorMsgBox(rest.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
	
	@Command("add")
	public void add(
			@BindingParam("popup") String popup,
			@BindingParam("item") PlPengajuanSantunanDto selected,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		getPageInfo().setAddMode(true);
		Map<String, Object> args = new HashMap<>();

		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	@NotifyChange("flagTLRS")
	@Command
	public void changeTL() {
		// Messagebox.show("test "+tindakLanjutDetailDto.getRvLowValue()+" "+rumahSakitDto.getKodeRumahsakit());
		// System.err.println("luthfi "+rumahSakitDto.getKodeRumahsakit());
		if(tindakLanjutDetailDto == null || tindakLanjutDetailDto.getRvLowValue()==null){
			
		}else if (tindakLanjutDetailDto != null
				|| tindakLanjutDetailDto.getRvLowValue() != null) {
			if ((tindakLanjutDetailDto.getRvLowValue().equalsIgnoreCase("5"))) {
				if(rumahSakitDto == null || rumahSakitDto.getKodeRumahsakit() == null){
					setFlagTLRS(true);
					setMsgTlRs("Kolom Ditangani Rumah Sakit Harus Diisi");
					BindUtils.postNotifyChange(null, null, this, "flagTLRS");					
					BindUtils.postNotifyChange(null, null, this, "msgTlRs");					
				}else if(dataLakaDto.getKodeSifatCidera().equalsIgnoreCase("01") || 
						dataLakaDto.getKodeSifatCidera().equalsIgnoreCase("04") || 
						dataLakaDto.getKodeSifatCidera().equalsIgnoreCase("07")){
					setFlagTLRS(true);
					setMsgTlRs("Sifat Cidera Tidak Sesuai dengan Tindak Lanjut ini !");
					BindUtils.postNotifyChange(null, null, this, "flagTLRS");										
					BindUtils.postNotifyChange(null, null, this, "msgTlRs");					
				}
			} else {
				setFlagTLRS(false);
				BindUtils.postNotifyChange(null, null, this, "flagTLRS");
			}
		}
	}

	public void listStatusKorban() {
		setStatusKorban("-");
		listStatusKorban.add("-");
		listStatusKorban.add("Terbit Tagihan Dari RS");
		listStatusKorban.add("Korban Ttd Surat Pernyataan");
		BindUtils.postNotifyChange(null, null, this, "statusKorban");
		BindUtils.postNotifyChange(null, null, this, "listStatusKorban");
	}

	@Command("addRegister")
	public void addRegister(
			@BindingParam("item") PlDataKecelakaanDto selected,
			@BindingParam("popup") String popup,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName,
			@BindingParam("window") Window win) {
		setButtonAddForRegister(true);
		BindUtils.postNotifyChange(null, null, this, "buttonAddForRegister");
		Executions.getCurrent().setAttribute("obj", selected);
		getPageInfo().setAddDetailMode(true);
		Map<String, Object> args = new HashMap<>();
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
			back(win);
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	public void onAddDetail() {
		dataLakaDto = (PlDataKecelakaanDto) Executions.getCurrent()
				.getAttribute("obj");
		loadAsalBerkasDetail();
		loadTindakLanjutDetail();
		listStatusKorban();
		loadSifatCideraPengajuan();
		loadJenisHubungan();
		loadJenisIdentitas();
		searchProvinsi();
		setDisable(true);

		setDateTL(new Date());
		setDiajukanDi(userSession.getNamaKantor());
		if (getListTindakLanjutPrintDto() == null
				|| getListTindakLanjutPrintDto().size() == 0) {
			setFlagPengajuanButton(false);
		} else {
			setFlagPengajuanButton(true);
		}
		// for (DasiJrRefCodeDto a : listAsalBerkasDetailDto) {
		// if (a.getRvMeaning().equalsIgnoreCase("Instansi Laka/Jemput Bola")) {
		// // getAsalBerkasDetailDto().setRvLowValue(a.getRvLowValue());
		// }
		// }
		asalBerkasDetailDto = listAsalBerkasDetailDto.get(0);
		setSearchAsalBerkasDetail(getAsalBerkasDetailDto().getRvMeaning());

		// =============================GET DATA LAKA=======================================
		Map<String, Object> mapLaka = new HashMap<>();
		mapLaka.put("idKecelakaan", dataLakaDto.getIdKecelakaan());
		RestResponse restLaka = callWs(WS_URI_LAKA + "/dataLakaById", mapLaka,
				HttpMethod.POST);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		try {
			listDataLakaDto = JsonUtil.mapJsonToListObject(
					restLaka.getContents(), PlDataKecelakaanDto.class);
			for (PlDataKecelakaanDto a : listDataLakaDto) {
				dataLakaDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
				dataLakaDto.setTglKejadian(a.getTglKejadian());
				dataLakaDto.setKasusKecelakaanDesc(a.getKasusKecelakaanDesc());
				dataLakaDto.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ==========================GET DATA KORBAN=====================================

		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("idKorban", dataLakaDto.getIdKorbanKecelakaan());
		RestResponse rest = callWs(WS_URI_LAKA + "/korbanLakaByIdKorban",
				mapInput, HttpMethod.POST);

		try {
			listDataKorbanDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), PlKorbanKecelakaanDto.class);

			for (PlKorbanKecelakaanDto a : listDataKorbanDto) {
				dataLakaDto.setNamaKorban(a.getNama());
				dataLakaDto.setCidera(a.getCideraDesc());
				dataLakaDto.setJenisIdentitas(a.getJenisIdentitasDesc());
				dataLakaDto.setNoIdentitas(a.getNoIdentitas());
				dataLakaDto.setStatus(a.getStatusKorbanDesc());
				dataLakaDto.setProvinsiDesc(a.getNamaProvinsi());
				dataLakaDto.setKabKotaDesc(a.getNamaKabkota());
				dataLakaDto.setCamatDesc(a.getNamaCamat());
				dataLakaDto.setAlamat(a.getAlamat());
				dataLakaDto.setNoTelp(a.getNoTelp());
				dataLakaDto.setKodeJaminan(a.getKodeJaminan());
				dataLakaDto.setKodeSifatCidera(a.getKodeSifatCidera());
				dataLakaDto.setNama(a.getNama());
				dataLakaDto.setUmur(a.getUmur());
				dataLakaDto.setPenjamin(a.getPenjaminDesc());
				sifatCideraDto.setRvLowValue(a.getKodeSifatCidera());
				if (a.getJenisKelamin().equalsIgnoreCase("P")) {
					dataLakaDto.setJenisKelamin("Pria");
				} else if (a.getJenisKelamin().equalsIgnoreCase("W")) {
					dataLakaDto.setJenisKelamin("Wanita");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		registerSementaraDto.setTglRegister(new Date());
//		jenisIdentitasDto.setRvMeaning(dataLakaDto.getJenisIdentitas());
		jenisHubunganDto.setRvLowValue("-");
//		if (jenisIdentitasDto != null || jenisIdentitasDto.getRvMeaning()!=null) {
//			for (DasiJrRefCodeDto jenisIdentitas : listJenisIdentitasDto) {
//				if (jenisIdentitasDto.getRvMeaning().equalsIgnoreCase(
//						jenisIdentitas.getRvLowValue())) {
//					jenisIdentitasDto = new DasiJrRefCodeDto();
//					jenisIdentitasDto = jenisIdentitas;
//				}
//			}
//		}
		if(jenisHubunganDto!=null||jenisHubunganDto.getRvLowValue()!=null){
			for (DasiJrRefCodeDto jenisHub : listJenisHubunganDto) {
				if (jenisHubunganDto.getRvLowValue().equalsIgnoreCase(
						jenisHub.getRvLowValue())) {
					jenisHubunganDto = new DasiJrRefCodeDto();
					jenisHubunganDto = jenisHub;
				}
			}
		}
		
		BindUtils.postNotifyChange(null, null, this, "dataLakaDto");
		BindUtils.postNotifyChange(null, null, this, "tindakLanjutDetailDto");
		BindUtils.postNotifyChange(null, null, this, "registerSementaraDto");
		BindUtils.postNotifyChange(null, null, this, "jenisIdentitasDto");
		BindUtils.postNotifyChange(null, null, this, "jenisHubunganDto");
		BindUtils.postNotifyChange(null, null, this, "searchAsalBerkasDetail");
		BindUtils.postNotifyChange(null, null, this, "asalBerkasDetailDto");
		BindUtils.postNotifyChange(null, null, this, "searchAsalBerkasDeta");
		BindUtils.postNotifyChange(null, null, this, "flagPengajuanButton");
		BindUtils.postNotifyChange(null, null, this, "disable");
		BindUtils.postNotifyChange(null, null, this, "diajukanDi");
		BindUtils.postNotifyChange(null, null, this, "dateTL");
		BindUtils.postNotifyChange(null, null, this, "timeTL");
		// loadAsalBerkasDetail();
		// loadTindakLanjutDetail();
		// loadSifatCideraPengajuan();
		// loadRS();
	}

	@NotifyChange({ "buttonAddForPengajuan", "buttonAddForRegister" })
	@GlobalCommand("addSantunan")
	public void addSantunan() {
		setButtonAddForPengajuan(true);
		setButtonAddForRegister(false);
		BindUtils.postNotifyChange(null, null, this, "buttonAddForPengajuan");
		navigate(DETAIL_PAGE_PATH);
	}

	@Command("addSantunan2")
	public void addSantunan2(@BindingParam("item") PlDataKecelakaanDto selected) {
		if (selected == null || selected.getIdKecelakaan() == null) {
			showSmartMsgBox("W001");
			return;
		}
		Executions.getCurrent().setAttribute("obj2", selected);
		getPageInfo().setAddDetailMode(true);
		navigate(DETAIL_PAGE_PATH2);
	}

	public void onAdd() {
		listJenisPertanggungan();
		listStatusLp();
		listInstansi();
		setButtonAddForRegister(true);
		BindUtils.postNotifyChange(null, null, this, "buttonAddForRegister");
	}

	@NotifyChange({ "statusLP", "listStatusLp" })
	public void listStatusLp() {
		listStatusLp.add("Y");
		listStatusLp.add("N");
	}

	@Command
	public void listJenisPertanggungan() {
		RestResponse listJenisPertanggunganRest = callWs(WS_URI_LOV
				+ "/getListJenisPertanggungan", new HashMap<String, Object>(),
				HttpMethod.POST);
		try {
			listJenisPertanggunganDto = JsonUtil.mapJsonToListObject(
					listJenisPertanggunganRest.getContents(),
					PlJaminanDto.class);
			BindUtils.postNotifyChange(null, null, this,
					"listJenisPertanggunganDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void loadList() {
		// String[] months = new DateFormatSymbols().getMonths();
		// for (int i = 0; i < months.length; i++) {
		// listMonthReg.add(months[i]);
		// listMonthLaka.add(months[i]);
		// }
		listMonthReg();
		listMonthLaka();
		loadTindakLanjut();
		listSifatCidera();
		loadAsalBerkas();
		setListIndex(true);
		setSearchKantor(userSession.getNamaKantor());
		loadOneKantor();
		setYearReg((new SimpleDateFormat("YYYY").format(new Date())));
		BindUtils.postNotifyChange(null, null, this, "yearReg");
	}

	public void listMonthReg() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		if (sdf.format(date).equalsIgnoreCase("01")) {
			setMonthRegStr("Januari");
		} else if (sdf.format(date).equalsIgnoreCase("02")) {
			setMonthRegStr("Februari");
		} else if (sdf.format(date).equalsIgnoreCase("03")) {
			setMonthRegStr("Maret");
		} else if (sdf.format(date).equalsIgnoreCase("04")) {
			setMonthRegStr("April");
		} else if (sdf.format(date).equalsIgnoreCase("05")) {
			setMonthRegStr("Mei");
		} else if (sdf.format(date).equalsIgnoreCase("06")) {
			setMonthRegStr("Juni");
		} else if (sdf.format(date).equalsIgnoreCase("07")) {
			setMonthRegStr("Juli");
		} else if (sdf.format(date).equalsIgnoreCase("08")) {
			setMonthRegStr("Agustus");
		} else if (sdf.format(date).equalsIgnoreCase("09")) {
			setMonthRegStr("September");
		} else if (sdf.format(date).equalsIgnoreCase("10")) {
			setMonthRegStr("Oktober");
		} else if (sdf.format(date).equalsIgnoreCase("11")) {
			setMonthRegStr("November");
		} else if (sdf.format(date).equalsIgnoreCase("12")) {
			setMonthRegStr("Desember");
		}
		listMonthReg.add("-");
		listMonthReg.add("Januari");
		listMonthReg.add("Februari");
		listMonthReg.add("Maret");
		listMonthReg.add("April");
		listMonthReg.add("Mei");
		listMonthReg.add("Juni");
		listMonthReg.add("Juli");
		listMonthReg.add("Agustus");
		listMonthReg.add("September");
		listMonthReg.add("Oktober");
		listMonthReg.add("November");
		listMonthReg.add("Desember");
		BindUtils.postNotifyChange(null, null, this, "monthRegStr");
		BindUtils.postNotifyChange(null, null, this, "listMonthReg");
	}

	public void listMonthLaka() {
		setMonthLakaStr("-");
		listMonthLaka.add("-");
		listMonthLaka.add("Januari");
		listMonthLaka.add("Februari");
		listMonthLaka.add("Maret");
		listMonthLaka.add("April");
		listMonthLaka.add("Mei");
		listMonthLaka.add("Juni");
		listMonthLaka.add("Juli");
		listMonthLaka.add("Agustus");
		listMonthLaka.add("September");
		listMonthLaka.add("Oktober");
		listMonthLaka.add("November");
		listMonthLaka.add("Desember");
		BindUtils.postNotifyChange(null, null, this, "monthLakaStr");
		BindUtils.postNotifyChange(null, null, this, "listMonthLaka");
	}

	@Command
	public void loadKantor() {
		Map<String, Object> mapinput = new HashMap<>();

		mapinput.put("search", searchKantor);

		RestResponse rest = callWs(WS_URI_LOV + "/findAllKantor", mapinput,
				HttpMethod.POST);
		try {
			listKantorDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndKantorJasaraharjaDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listKantorDto");

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Command
	public void loadOneKantor() {
		Map<String, Object> mapinput = new HashMap<>();

		mapinput.put("search", searchKantor);

		RestResponse rest = callWs(WS_URI_LOV + "/findAllKantor", mapinput,
				HttpMethod.POST);
		try {
			listKantorDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndKantorJasaraharjaDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listKantorDto");
			setKantorDto(listKantorDto.get(0));
			BindUtils.postNotifyChange(null, null, this, "kantorDto");

		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Command
	public void loadKantorPengajuan() {
		listKantorPengajuanDto.clear();
		Map<String, Object> mapinput = new HashMap<>();

		mapinput.put("search", getSearchKantorPengajuan());

		RestResponse rest = callWs(WS_URI_LOV + "/findAllKantor", mapinput,
				HttpMethod.POST);
		try {
			listKantorPengajuanDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), FndKantorJasaraharjaDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this,
					"listKantorPengajuanDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void loadJenisIdentitas() {
		// String revDomain = "%KODE JENIS IDENTITAS";
		// String flag = "";
		// Map<String, Object> mapinput = new HashMap<>();
		// if (getSearchJenisIdentitas() == null
		// || getSearchJenisIdentitas().equalsIgnoreCase("")) {
		// setSearchJenisIdentitas("");
		// }
		// mapinput.put("search", searchJenisIdentitas);
		// mapinput.put("rvDomain", revDomain);
		// mapinput.put("flag", flag);
		// RestResponse rest = callWs(WS_URI + "/refCode", mapinput,
		// HttpMethod.POST);
		RestResponse rest = callWs(WS_URI_LOV + "/getListId",
				new HashMap<String, Object>(), HttpMethod.POST);
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(),
					DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listJenisIdentitasDto.add(kosong);
			listJenisIdentitasDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this,
					"listJenisIdentitasDto");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Command
	public void loadJenisHubungan() {
		// String revDomain = "%KODE HUBUNGAN KORBAN";
		// String flag = "Y";
		// Map<String, Object> mapinput = new HashMap<>();
		// if (getSearchJenisHubungan() == null
		// || getSearchJenisHubungan().equalsIgnoreCase("")) {
		// setSearchJenisHubungan("");
		// }
		// mapinput.put("search", searchJenisHubungan);
		// mapinput.put("rvDomain", revDomain);
		// mapinput.put("flag", flag);
		// RestResponse rest = callWs(WS_URI + "/refCode", mapinput,
		// HttpMethod.POST);
		RestResponse rest = callWs(WS_URI_LOV + "/getListStatusHubungan",
				new HashMap<String, Object>(), HttpMethod.POST);
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(),
					DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listJenisHubunganDto.add(kosong);
			listJenisHubunganDto.addAll(listBaru);
			BindUtils
					.postNotifyChange(null, null, this, "listJenisHubunganDto");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Command
	public void loadAsalBerkas() {
		Map<String, Object> mapinput = new HashMap<>();

		if (getSearchAsalBerkas() == null
				|| getSearchAsalBerkas().equalsIgnoreCase("")) {
			setSearchAsalBerkas("");
		}
		mapinput.put("search", searchAsalBerkas);
		String revDomain = "%ASAL BERKAS REGISTER%";
		String flag = "";
		mapinput.put("rvDomain", revDomain);
		mapinput.put("flag", flag);
		RestResponse rest = callWs(WS_URI + "/refCode", mapinput,
				HttpMethod.POST);
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(),
					DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue(null);
			kosong.setRvMeaning("-");
			listAsalBerkasDto.add(kosong);
			listAsalBerkasDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listAsalBerkasDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void searchInstansi() {
		Map<String, Object> map = new HashMap<>();
		if (instansiDto == null || instansiDto.getKodeInstansi() == null) {
			map.put("search", searchInstansi);
		}
		map.put("search", "");
		RestResponse rest = callWs(WS_URI_LOV + "/getListInstansi", map,
				HttpMethod.POST);
		try {
			listInstansiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlInstansiDto.class);
			listInstansiDtoCopy = new ArrayList<>();
			listInstansiDtoCopy.addAll(listInstansiDto);
			BindUtils.postNotifyChange(null, null, this, "listInstansiDtoCopy");
			BindUtils.postNotifyChange(null, null, this, "listInstansiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange("listInstansiDtoCopy")
	@Command
	public void cariInstansi(@BindingParam("item") String cari) {
		if (listInstansiDtoCopy != null || listInstansiDtoCopy.size() > 0) {
			listInstansiDtoCopy.clear();
		}
		if (listInstansiDto != null && listInstansiDto.size() > 0) {
			for (PlInstansiDto dto : listInstansiDto) {
				if (dto.getKodeNama().toUpperCase()
						.contains(cari.toUpperCase())) {
					listInstansiDtoCopy.add(dto);
				}
			}
		}
	}

	@Command
	public void loadAsalBerkasDetail() {
		Map<String, Object> mapinput = new HashMap<>();

		if (getSearchAsalBerkasDetail() == null
				|| getSearchAsalBerkasDetail().equalsIgnoreCase("")) {
			setSearchAsalBerkasDetail("");
		}
		mapinput.put("search", getSearchAsalBerkasDetail());
		String revDomain = "%ASAL BERKAS REGISTER%";
		String flag = "";
		mapinput.put("rvDomain", revDomain);
		mapinput.put("flag", flag);
		RestResponse rest = callWs(WS_URI + "/refCode", mapinput,
				HttpMethod.POST);
		try {
			listAsalBerkasDetailDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), DasiJrRefCodeDto.class);
			setTotalSize(rest.getTotalRecords());

		} catch (Exception e) {
			e.printStackTrace();

		}
		BindUtils.postNotifyChange(null, null, this, "listAsalBerkasDetailDto");
	}

	@Command
	public void loadTindakLanjut() {
		String revDomain = "%TINDAK LANJUT REGISTER%";
		String flag = "Y";
		Map<String, Object> mapinput = new HashMap<>();

		if (getSearchTindakLanjut() == null
				|| getSearchTindakLanjut().equalsIgnoreCase("")) {
			setSearchTindakLanjut("");
		}
		mapinput.put("search", searchTindakLanjut);
		mapinput.put("rvDomain", revDomain);
		mapinput.put("flag", flag);
		RestResponse rest = callWs(WS_URI + "/refCode", mapinput,
				HttpMethod.POST);
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(),
					DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue(null);
			kosong.setRvMeaning("-");
			listTindakLanjutDto.add(kosong);
			listTindakLanjutDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listTindakLanjutDto");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Command
	public void loadTindakLanjutDetail() {
		listTindakLanjutDetailDto.clear();
		String revDomain = "%TINDAK LANJUT REGISTER%";
		String flag = "Y";
		Map<String, Object> mapinput = new HashMap<>();

		if (getSearchTindakLanjutDetail() == null
				|| getSearchTindakLanjutDetail().equalsIgnoreCase("")) {
			setSearchTindakLanjutDetail("");
		}
		mapinput.put("search", searchTindakLanjutDetail);
		mapinput.put("rvDomain", revDomain);
		mapinput.put("flag", flag);
		RestResponse rest = callWs(WS_URI + "/refCode", mapinput,
				HttpMethod.POST);
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(),
					DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue("");
			kosong.setRvMeaning("-");
			listTindakLanjutDetailDto.add(kosong);
			listTindakLanjutDetailDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this,
					"listTindakLanjutDetailDto");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	// @Command
	// public void loadSifatCidera(){
	// String revDomain = "%KODE SIFAT CIDERA%";
	// String flag = "Y";
	// Map<String, Object> mapinput = new HashMap<>();
	//
	// if (getSearchSifatCidera() == null
	// || getSearchSifatCidera().equalsIgnoreCase("")) {
	// setSearchSifatCidera("");
	// }
	// mapinput.put("search", searchSifatCidera);
	// mapinput.put("rvDomain", revDomain);
	// mapinput.put("flag", flag);
	// RestResponse rest = callWs(WS_URI+"/refCode", mapinput, HttpMethod.POST);
	// try {
	// listSifatCideraDto = JsonUtil.mapJsonToListObject(rest.getContents(),
	// DasiJrRefCodeDto.class);
	// setTotalSize(rest.getTotalRecords());
	// BindUtils.postNotifyChange(null, null, this, "listSifatCideraDto");
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }

	@Command
	public void listSifatCidera() {
		RestResponse listSifatCideraRest = callWs(WS_URI_LOV
				+ "/getListSifatCidera", new HashMap<String, Object>(),
				HttpMethod.POST);
		listSifatCideraDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(
					listSifatCideraRest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listSifatCideraDto.add(kosong);
			listSifatCideraDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listSifatCideraDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void loadSifatCideraPengajuan() {
		// String revDomain = "%KODE SIFAT CIDERA%";
		// String flag = "Y";
		// Map<String, Object> mapinput = new HashMap<>();
		//
		// if (getSearchSifatCideraPengajuan() == null
		// || getSearchSifatCideraPengajuan().equalsIgnoreCase("")) {
		// setSearchSifatCideraPengajuan("");
		// }
		// mapinput.put("search", searchSifatCideraPengajuan);
		// mapinput.put("rvDomain", revDomain);
		// mapinput.put("flag", flag);
		// RestResponse rest = callWs(WS_URI + "/refCode", mapinput,
		// HttpMethod.POST);
		RestResponse rest = callWs(WS_URI_LOV + "/getListSifatCidera",
				new HashMap<String, Object>(), HttpMethod.POST);

		try {
			listSifatCideraDetailDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), DasiJrRefCodeDto.class);
			BindUtils.postNotifyChange(null, null, this,
					"listSifatCideraDetailDto");
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	public int convertMonth(String month) {
		Date date = null;
		try {
			date = (Date) new SimpleDateFormat("MMM", Locale.ENGLISH)
					.parse(month.substring(0, 2));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		int bulan = cal.get(Calendar.MONTH);
		return bulan;
	}

	@Command("cari")
	public void loadRegisterSementara() {
		Map<String, Object> mapInput = new HashMap<>();
		String rvDomainCidera = "KODE SIFAT CIDERA";
		String rvDomainTindakLanjut = "TINDAK LANJUT REGISTER";

		if (getMonthLakaStr() == null || getMonthLakaStr().equalsIgnoreCase("")) {
			setMonthLakaStr("");
		} else {
			setMonthLakaStr(getMonthLakaStr());
		}
		if (getMonthRegStr() == null || getMonthRegStr().equalsIgnoreCase("")) {
			setMonthRegStr("");
		} else {
			setMonthRegStr(getMonthRegStr());
		}

		switch (getMonthRegStr()) {
		case "Januari":
			setMonthReg("1");
			break;
		case "Februari":
			setMonthReg("2");
			break;
		case "Maret":
			setMonthReg("3");
			break;
		case "April":
			setMonthReg("4");
			break;
		case "Mei":
			setMonthReg("5");
			break;
		case "Juni":
			setMonthReg("6");
			break;
		case "Juli":
			setMonthReg("7");
			break;
		case "Agustus":
			setMonthReg("8");
			break;
		case "September":
			setMonthReg("9");
			break;
		case "Oktober":
			setMonthReg("10");
			break;
		case "November":
			setMonthReg("11");
			break;
		case "Desember":
			setMonthReg("12");
			break;
		default:
			setMonthReg("");
			break;
		}

		switch (getMonthLakaStr()) {
		case "Januari":
			setMonthLaka("1");
			break;
		case "Februari":
			setMonthLaka("2");
			break;
		case "Maret":
			setMonthLaka("3");
			break;
		case "April":
			setMonthLaka("4");
			break;
		case "Mei":
			setMonthLaka("5");
			break;
		case "Juni":
			setMonthLaka("6");
			break;
		case "Juli":
			setMonthLaka("7");
			break;
		case "Agustus":
			setMonthLaka("8");
			break;
		case "September":
			setMonthLaka("9");
			break;
		case "Oktober":
			setMonthLaka("10");
			break;
		case "November":
			setMonthLaka("11");
			break;
		case "Desember":
			setMonthLaka("12");
			break;
		default:
			setMonthLaka("");
			break;
		}

		if (getKodeKantor() == null || getKodeKantor().equalsIgnoreCase("")) {
			setKodeKantor("");
		} else {
			setKodeKantor(getKodeKantor());
		}

		if (getNoLaporan() == null || getNoLaporan().equalsIgnoreCase("")) {
			setNoLaporan("");
		} else {
			setNoLaporan(getNoLaporan());
		}

		if (getNoReg() == null || getNoReg().equalsIgnoreCase("")) {
			setNoReg("");
		} else {
			setNoReg(getNoReg());
		}

		if (getAsalBerkasDto() == null) {
			setAsalBerkas("");
		} else {
			setAsalBerkas(getAsalBerkasDto().getRvLowValue());
		}

		if (getSifatCideraDto() == null) {
			setSifatCidera("");
		} else {
			setSifatCidera(getSifatCideraDto().getRvLowValue());
		}

		if (getNamaInstansi() == null || getNamaInstansi().equalsIgnoreCase("")) {
			setNamaInstansi("");
		} else {
			setNamaInstansi(getNamaInstansi());
		}

		if (getTindakLanjutDto() == null) {
			setTindakLanjut("");
		} else {
			setTindakLanjut(getTindakLanjutDto().getRvLowValue());
		}

		if (getDayReg() == null || getDayReg().equalsIgnoreCase("")) {
			setDayReg("");
		} else {
			setDayReg(getDayReg());
		}
		if (getMonthReg() == null || getMonthReg().equalsIgnoreCase("")) {
			setMonthReg("");
		} else {
			setMonthReg(getMonthReg());
		}
		if (getYearReg() == null || getYearReg().equalsIgnoreCase("")) {
			setYearReg("");
		} else {
			setYearReg(getYearReg());
		}

		if (getDayLaka() == null || getDayLaka().equalsIgnoreCase("")) {
			setDayLaka("");
		} else {
			setDayLaka(getDayLaka());
		}
		if (getMonthLaka() == null || getMonthLaka().equalsIgnoreCase("")) {
			setMonthLaka("");
		} else {
			setMonthLaka(getMonthLaka());
		}
		if (getYearLaka() == null || getYearLaka().equalsIgnoreCase("")) {
			setYearLaka("");
		} else {
			setYearLaka(getYearLaka());
		}
		
		mapInput.put("kodeKantorJr", getKodeKantor());
		mapInput.put("namaKorban", getNamaKorban());
		mapInput.put("noLaporan", getNoLaporan());
		mapInput.put("asalBerkasFlag", getAsalBerkas());
		mapInput.put("tindakLanjutFlag", getTindakLanjut());
		mapInput.put("cideraKorban", getSifatCidera());
		mapInput.put("namaInstansi", getNamaInstansi());
		mapInput.put("rvDomainCidera", rvDomainCidera);
		mapInput.put("rvDomainTindakLanjut", rvDomainTindakLanjut);
		mapInput.put("regDay", getDayReg());
		mapInput.put("regMonth", getMonthReg());
		mapInput.put("regYear", getYearReg());
		mapInput.put("lakaDay", getDayLaka());
		mapInput.put("lakaMonth", getMonthLaka());
		mapInput.put("lakaYear", getYearLaka());
		mapInput.put("search", getSearch());
		mapInput.put("noRegister", getNoReg());

		BindUtils.postNotifyChange(null, null, this, "monthReg");
		BindUtils.postNotifyChange(null, null, this, "monthRegStr");
		BindUtils.postNotifyChange(null, null, this, "monthLaka");
		BindUtils.postNotifyChange(null, null, this, "monthLakaStr");

		RestResponse rest = callWs(WS_URI + "/all", mapInput, HttpMethod.POST);
		try {
			listRegisterSementaraDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), PlRegisterSementaraDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this,
					"listRegisterSementaraDto");
			BindUtils.postNotifyChange(null, null, this, "listIndex");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	public void getMonthReg(String monthReg){
//		if(monthReg.equalsIgnoreCase("Ja"));
//	}

	@Command("cariSemua")
	public void loadRegistelementaraAll() {
		Map<String, Object> mapInput = new HashMap<>();
		String rvDomainCidera = "KODE SIFAT CIDERA";
		String rvDomainTindakLanjut = "TINDAK LANJUT REGISTER";

		setKodeKantor("");
		setNamaKorban("");
		setNoLaporan("");
		setAsalBerkas("");
		setTindakLanjut("");
		setSifatCidera("");
		setNamaInstansi("");
		setDayReg("");
		setMonthReg("");
		setYearReg("");
		setDayLaka("");
		setMonthLaka("");
		setYearLaka("");

		mapInput.put("kodeKantorJr", getKodeKantor());
		mapInput.put("namaKorban", getNamaKorban());
		mapInput.put("noLaporan", getNoLaporan());
		mapInput.put("asalBerkasFlag", getAsalBerkas());
		mapInput.put("tindakLanjutFlag", getTindakLanjut());
		mapInput.put("cideraKorban", getSifatCidera());
		mapInput.put("namaInstansi", getNamaInstansi());
		mapInput.put("rvDomainCidera", rvDomainCidera);
		mapInput.put("rvDomainTindakLanjut", rvDomainTindakLanjut);
		mapInput.put("regDay", getDayReg());
		mapInput.put("regMonth", getMonthReg());
		mapInput.put("regYear", getYearReg());
		mapInput.put("lakaDay", getDayLaka());
		mapInput.put("lakaMonth", getMonthLaka());
		mapInput.put("lakaYear", getYearLaka());

		RestResponse rest = callWs(WS_URI + "/all", mapInput, HttpMethod.POST);
		try {
			listRegisterSementaraDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), PlRegisterSementaraDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this,
					"listRegisterSementaraDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command("cariDataLaka")
	public void loadDataLaka() throws ParseException {
		setListDetail(true);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		detailKejadianStartDate = detailKejadianStartDate==null?sdf.parse("01/01/2000"):detailKejadianStartDate;
		detailKejadianEndDate = detailKejadianEndDate==null?sdf.parse("01/01/9999"):detailKejadianEndDate;
		detailLaporanStartDate = detailLaporanStartDate==null?sdf.parse("01/01/2000"):detailLaporanStartDate;
		detailLaporanEndDate = detailLaporanEndDate ==null?sdf.parse("01/01/9999"):detailLaporanEndDate;
		
		Map<String, Object> map = new HashMap<>();
		map.put("kejadianStartDate", dateToString(detailKejadianStartDate));
		map.put("kejadianEndDate", dateToString(detailKejadianEndDate));
		map.put("laporanStartDate", dateToString(detailLaporanStartDate));
		map.put("laporanEndDate", dateToString(detailLaporanEndDate));
		map.put("instansi", instansiDto.getKodeInstansi());
		map.put("noLaporan", detailNoLaporanPolisi);
		map.put("namaKorban", detailNamaKorban);
		map.put("jenisPertanggungan", jenisPertanggunganDto.getKodeJaminan());
		map.put("statusLp", statusLP);
		map.put("menu", "register");
		RestResponse rest = callWs(WS_URI_LOV + "/getListCariDataLaka", map,
				HttpMethod.POST);
		try {
			listDataLakaDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlDataKecelakaanDto.class);
			listDataLakaDtoCopy = new ArrayList<>();
			listDataLakaDtoCopy.addAll(listDataLakaDto);
			BindUtils.postNotifyChange(null, null, this, "listDataLakaDtoCopy");
			BindUtils.postNotifyChange(null, null, this, "listDataLakaDto");
			BindUtils.postNotifyChange(null, null, this, "listDetail");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange("listDataLakaDtoCopy")
	@Command("cariFilterDataLaka")
	public void cariFilterDataLaka(@BindingParam("item") String cari) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		if (listDataLakaDtoCopy != null || listDataLakaDtoCopy.size() > 0) {
			listDataLakaDtoCopy.clear();
		}
		if (listDataLakaDto != null && listDataLakaDto.size() > 0) {
			for (PlDataKecelakaanDto dto : listDataLakaDto) {
				System.out.println("+++");
				if (sdf.format(dto.getTglKejadian()).equals(cari)
						|| date.format(dto.getTglKejadian()).equals(cari)
						|| month.format(dto.getTglKejadian()).equals(cari)
						|| year.format(dto.getTglKejadian()).equals(cari)
						|| dto.getNoLaporanPolisi().toUpperCase()
								.contains(cari)
						|| dto.getNama().toUpperCase().contains(cari)
						|| dto.getDeskripsiJaminan().toUpperCase()
								.contains(cari)
						|| dto.getStatusLaporanPolisi().toUpperCase()
								.contains(cari)) {
					listDataLakaDtoCopy.add(dto);
				}
			}
		}
	}

	@Command
	public void listInstansi() {
		Map<String, Object> map = new HashMap<>();
		if (getSearchInstansi() == null
				|| getSearchInstansi().equalsIgnoreCase("")) {
			setSearchInstansi("");
		}
		map.put("search", searchInstansi);
		RestResponse restInstansi = callWs(WS_URI_LOV + "/getListInstansi",
				map, HttpMethod.POST);
		try {
			listInstansi = JsonUtil.mapJsonToListObject(
					restInstansi.getContents(), PlInstansiDto.class);

			setTotalSize(restInstansi.getTotalRecords());

			BindUtils.postNotifyChange(null, null, this, "listInstansi");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void loadRS() {
		Map<String, Object> map = new HashMap<>();

		// if
		// (getSearchRS().equalsIgnoreCase("")||getSearchRS().isEmpty()||getSearchRS()
		// == null) {
		// setSearchRS("");
		// } else {
		// setSearchRS(getSearchRS());
		// }
		map.put("search", searchRS);
		RestResponse restRS = callWs(WS_URI_LOV + "/findAllRS", map,
				HttpMethod.POST);
		try {
			listRumahSakitDto = JsonUtil.mapJsonToListObject(
					restRS.getContents(), PlRumahSakitDto.class);
//			for(PlRumahSakitDto dto : listRumahSakitDto){
//				rumahSakitDto.setKodeRumahsakit(dto.getKodeRumahsakit());
//				rumahSakitDto.setKodeNamaRs(dto.getKodeNamaRs());
//				rumahSakitDto.setDeskripsi(dto.getDeskripsi());
//			}
			BindUtils.postNotifyChange(null, null, this, "listRumahSakitDto");
//			BindUtils.postNotifyChange(null, null, this, "rumahSakitDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void loadUser() {
		listUserDto.clear();
		Map<String, Object> map = new HashMap<>();

		map.put("search", getSearchUser());
		RestResponse restRS = callWs(WS_URI_LOV + "/allUser", map,
				HttpMethod.POST);
		try {

			listUserDto = JsonUtil.mapJsonToListObject(restRS.getContents(),
					AuthUserDto.class);

			setTotalSize(restRS.getTotalRecords());

			BindUtils.postNotifyChange(null, null, this, "listUserDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void checkHubPemohon() {
		try{
			
		
		if (jenisHubunganDto == null
				|| jenisHubunganDto.getRvLowValue() == null) {
			registerSementaraDto = new PlRegisterSementaraDto();
		} else {
			if (jenisHubunganDto.getRvLowValue().equalsIgnoreCase("01")
					&& sifatCideraDto.getRvLowValue().equalsIgnoreCase("01")||jenisHubunganDto.getRvLowValue().equalsIgnoreCase("01")
					&& sifatCideraDto.getRvLowValue().equalsIgnoreCase("05")) {
				showInfoMsgBox(
						"Korban Meninggal Tidak Dapat Mengajukan Diri Sendiri",
						"");
				listStatusHubungan();
			} else if (jenisHubunganDto.getRvLowValue().equalsIgnoreCase("01")) {
				registerSementaraDto.setNamaPemohon(dataLakaDto.getNamaKorban());
				jenisIdentitasDto.setRvLowValue(dataLakaDto.getJenisIdentitas());
				registerSementaraDto.setNoIdentitas(dataLakaDto.getNoIdentitas());
				registerSementaraDto.setAlamatPemohon(dataLakaDto.getAlamat());
				registerSementaraDto.setTelpPemohon(dataLakaDto.getNoTelp());
				if (jenisIdentitasDto.getRvLowValue() != null) {
					for (DasiJrRefCodeDto jenisIdentitas : listJenisIdentitasDto) {
						if (jenisIdentitasDto.getRvLowValue().equalsIgnoreCase(
								jenisIdentitas.getRvLowValue())) {
							jenisIdentitasDto = new DasiJrRefCodeDto();
							jenisIdentitasDto = jenisIdentitas;
						}
				}
				}
			}
		}
		BindUtils.postNotifyChange(null, null, this, "registerSementaraDto");
		BindUtils.postNotifyChange(null, null, this, "jenisIdentitasDto");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void listStatusHubungan() {

		RestResponse restStatusHubungan = callWs(WS_URI_LOV
				+ "/getListStatusHubungan", new HashMap<String, Object>(),
				HttpMethod.POST);
		listJenisHubunganDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(
					restStatusHubungan.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listJenisHubunganDto.add(kosong);
			listJenisHubunganDto.addAll(listBaru);
			setTotalSize(restStatusHubungan.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listJenisHubunganDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void cekKtp(){
		try{
			dukcapilWinDto = getDataFromDukcapil(registerSementaraDto.getNoIdentitas());
			registerSementaraDto.setNamaPemohon(dukcapilWinDto.getNamaLengkap());
			registerSementaraDto.setAlamatPemohon(dukcapilWinDto.getAlamat());
			
			String kodeProv = "";
			String kodeKab = "";
			String kodeCamat = "";
			if(dukcapilWinDto.getKodePropinsi().length()==1){
				kodeProv = "0"+dukcapilWinDto.getKodePropinsi();
			}else{
				kodeProv = dukcapilWinDto.getKodePropinsi();
			}

			if(dukcapilWinDto.getKodeKabupaten().length()==1){
				kodeKab = "0"+dukcapilWinDto.getKodeKabupaten();
			}else{
				kodeKab = dukcapilWinDto.getKodeKabupaten();
			}
			
			if(dukcapilWinDto.getKodeKecamatan().length()==1){
				kodeCamat = "0"+dukcapilWinDto.getKodeKecamatan();
			}else{
				kodeCamat = dukcapilWinDto.getKodeKecamatan();
			}

			
			provinsiDto.setKodeProvinsi(kodeProv);
			kabKotaDto.setKodeKabkota(kodeProv+kodeKab);
			camatDto.setKodeCamat(kodeProv+kodeKab+kodeCamat);
			searchProvinsi();			
			for(FndCamatDto prov : listProvinsiDto){
				if(provinsiDto.getKodeProvinsi().equalsIgnoreCase(prov.getKodeProvinsi())){
					provinsiDto = new FndCamatDto();
					provinsiDto = prov;
				}
			}
			getKabkota();
			
			for(FndCamatDto kab : listKabkotaDto){
				if(kabKotaDto.getKodeKabkota().equalsIgnoreCase(kab.getKodeKabkota())){
					kabKotaDto = new FndCamatDto();
					kabKotaDto = kab;
				}
			}
			getCamat();

			for(FndCamatDto camat : listCamatDto){
				if(camatDto.getKodeCamat().equalsIgnoreCase(camat.getKodeCamat())){
					camatDto = new FndCamatDto();
					camatDto = camat;
				}
			}

			BindUtils.postNotifyChange(null, null, this, "provinsiDto");
			BindUtils.postNotifyChange(null, null, this, "kabKotaDto");
			BindUtils.postNotifyChange(null, null, this, "camatDto");
			BindUtils.postNotifyChange(null, null, this, "dukcapilWinDto");
			BindUtils.postNotifyChange(null, null, this, "registerSementaraDto");
		}catch(Exception s){
			s.printStackTrace();
		}
	}
	
	@Command
	public void searchProvinsi() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchProvinsi);
		RestResponse rest = callWs(WS_URI_LOV + "/getListProvinsi", map,
				HttpMethod.POST);
		try {
			listProvinsiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listProvinsiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	@NotifyChange({ "searchProvinsi" })
	@Command
	public void getKabkota() {
		camatDto.setKodeNamaCamat("");
		BindUtils.postNotifyChange(null, null, this, "camatDto");
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchKabkota);
		RestResponse listKabkotaRest = callWs(WS_URI_LOV + "/getListKabkota/"
				+ provinsiDto.getKodeProvinsi(), map, HttpMethod.POST);

		try {
			listKabkotaDto = JsonUtil.mapJsonToListObject(
					listKabkotaRest.getContents(), FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKabkotaDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange({ "kabKotaDto", "searchCamat", "searchKabkota" })
	@Command
	public void getCamat() {
		HashMap<String, Object> map = new HashMap<String, Object>();
		map.put("search", searchCamat);
		RestResponse listCamatRest = callWs(WS_URI_LOV + "/getListCamat/"
				+ kabKotaDto.getKodeKabkota(), map, HttpMethod.POST);
		try {
			listCamatDto = JsonUtil.mapJsonToListObject(
					listCamatRest.getContents(), FndCamatDto.class);
			BindUtils.postNotifyChange(null, null, this, "listCamatDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void editTl(@BindingParam("item")PlTindakLanjutDto selectedDto){
		try{
			loadUser();
			loadTindakLanjutDetail();
			setFlagEditTl(true);
			setFlagTindakLanjut(true);
			setDisabledTl(true);
			tindakLanjutPrintDto = selectedDto;
			dateTL = selectedDto.getTglTindakLanjut();
			timeTL = selectedDto.getTglTindakLanjut();
			tindakLanjutDetailDto.setRvLowValue((selectedDto.getDeskripsiTL()==null?"":selectedDto.getTindakLanjutFlag()));
			kantorPengajuanDto.setKodeKantorJr(selectedDto.getDilimpahkanKe()==null?"":selectedDto.getDilimpahkanKe());
			setNoSuratJaminanTL(selectedDto.getNoSuratJaminan()==null?"":selectedDto.getNoSuratJaminan());
			setKeteranganTL(selectedDto.getCatatanTindakLanjut()==null?"":selectedDto.getCatatanTindakLanjut());
			setNoSuratPanggilanTL(selectedDto.getNoSuratPanggilan()==null?"":selectedDto.getNoSuratPanggilan());
			userDto.setLogin(selectedDto.getPetugasSurvey()==null?"":selectedDto.getPetugasSurvey());
			findOneKantorPengajuan(kantorPengajuanDto.getKodeKantorJr());
			for(DasiJrRefCodeDto a : listTindakLanjutDetailDto){
				if(tindakLanjutDetailDto.getRvLowValue().equalsIgnoreCase(a.getRvLowValue())){
					tindakLanjutDetailDto = new DasiJrRefCodeDto();
					tindakLanjutDetailDto = a;
				}
			}                           
			for(AuthUserDto b : listUserDto){
				if(userDto.getLogin().equalsIgnoreCase(b.getLogin())){
					userDto = new AuthUserDto();
					userDto = b;
				}
			}
			BindUtils.postNotifyChange(null, null, this, "tindakLanjutDetailDto");
			BindUtils.postNotifyChange(null, null, this, "tindakLanjutDto");
			BindUtils.postNotifyChange(null, null, this, "tindakLanjutPrintDto");
			BindUtils.postNotifyChange(null, null, this, "kantorPengajuanDto");
			BindUtils.postNotifyChange(null, null, this, "userDto");
			BindUtils.postNotifyChange(null, null, this, "dateTL");
			BindUtils.postNotifyChange(null, null, this, "timeTL");
			BindUtils.postNotifyChange(null, null, this, "noSuratPanggilanTL");
			BindUtils.postNotifyChange(null, null, this, "noSuratJaminanTl");
			BindUtils.postNotifyChange(null, null, this, "keteranganTL");
			BindUtils.postNotifyChange(null, null, this, "flagTindakLanjut");
			BindUtils.postNotifyChange(null, null, this, "disableTl");
			BindUtils.postNotifyChange(null, null, this, "userDto");
			BindUtils.postNotifyChange(null, null, this, "flagEditTl");
			
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public void findOneKantorPengajuan(String kodeKantorJr){
		try{
			Map<String, Object> mapInput = new HashMap<>();
			mapInput.put("kodeKantorJr", kodeKantorJr);
			RestResponse restKantorJr = callWs(WS_URI_LOV+"/kantorByKode", mapInput, HttpMethod.POST);
			listKantorPengajuanDto = JsonUtil.mapJsonToListObject(restKantorJr.getContents(), FndKantorJasaraharjaDto.class);
			for(FndKantorJasaraharjaDto a : listKantorPengajuanDto){
				if(kodeKantorJr.equalsIgnoreCase(a.getKodeKantorJr())){
					kantorPengajuanDto = new FndKantorJasaraharjaDto();
					kantorPengajuanDto = a;					
				}
			}
			BindUtils.postNotifyChange(null, null, this, "kantorPengajuanDto");
			BindUtils.postNotifyChange(null, null, this, "listKantorPengajuanDto");

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("deprecation")
	@Command("cetak")
	public void cete(@BindingParam("popup")String popup) throws ClassNotFoundException, SQLException, IOException, JRException, Exception{
		try{
			nomerRegister = registerSementaraDto.getNoRegister()==null?nomerRegister:registerSementaraDto.getNoRegister();
			System.err.println("luthf70 "+nomerRegister);
			PlRegisterSementaraDto dto = new PlRegisterSementaraDto();
			List<PlTindakLanjutDto> listTl = new ArrayList<>();
			AuthUserDto petugas = new AuthUserDto();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			=====================================DATA 1====================================================
			Map<String, Object> mapInput = new HashMap<String, Object>();
			mapInput.put("noRegister", nomerRegister);
			RestResponse restData1 = callWs(WS_URI+"/dataCetakTl1", mapInput, HttpMethod.POST);
			List<PlRegisterSementaraDto> dtos = JsonUtil.mapJsonToListObject(restData1.getContents(), PlRegisterSementaraDto.class);
			for(PlRegisterSementaraDto a : dtos){
				dto = a;
			}
			
//			====================================DATA LIST=================================================
			RestResponse rest = callWs(WS_URI + "/tindakLanjut", mapInput,	HttpMethod.POST);
			listTindakLanjutPrintDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlTindakLanjutDto.class);
			for(PlTindakLanjutDto b : listTindakLanjutPrintDto){
				PlTindakLanjutDto tl = new PlTindakLanjutDto();
				tl.setTglTindakLanjut(b.getTglTindakLanjut());
				if(b.getTindakLanjutFlag().equalsIgnoreCase("2")||b.getTindakLanjutFlag().equalsIgnoreCase("4")){
//					petugas.setLogin(b.getPetugasSurvey()==null?"":b.getPetugasSurvey());
//					Map<String, Object> mapPetugas = new HashMap<>();
//					mapPetugas.put("login", petugas.getLogin());
//					RestResponse restPetugas = callWs(WS_URI_LOV+ "/getUserByLogin", mapPetugas,	HttpMethod.POST);
//					List<AuthUserDto> listPetugas = JsonUtil.mapJsonToListObject(restPetugas.getContents(), AuthUserDto.class);
//					for(AuthUserDto a : listPetugas){
//						petugas.setUserName(a.getUserName()==null?"":a.getUserName());
//					}
					tl.setDeskripsiTL(b.getDeskripsiTL()+", Nama Petugas : "+b.getPetugasSurvey());
				}else if(b.getTindakLanjutFlag().equalsIgnoreCase("1")){
					tl.setDeskripsiTL(b.getDeskripsiTL()+", No Surat : "+b.getNoSuratPanggilan());
				}else{
					tl.setDeskripsiTL(b.getDeskripsiTL());
				}
				tl.setCatatanTindakLanjut(b.getCatatanTindakLanjut());
				listTl.add(tl);
			}
			
			Map<String, Object> parameters = new HashMap<String, Object>();

			JRBeanCollectionDataSource listTL  = new JRBeanCollectionDataSource(listTl);

			try{
				parameters.put("PARAM_KANTOR_USER", userSession.getNamaKantor());
				parameters.put("PARAM_NO_REGISTER", dto.getNoRegister());
				parameters.put("PARAM_TGL_REGISTER", sdf.format(dto.getTglRegister()));
				parameters.put("PARAM_NAMA_KORBAN", dto.getNamaKorban());
				parameters.put("PARAM_KODE_CIDERA", dto.getCideraKorban());
				parameters.put("PARAM_ASAL_BERKAS", dto.getAsalBerkas());
				parameters.put("PARAM_PEMOHON", dto.getNamaPemohon());
				parameters.put("PARAM_HUB", dto.getHubunganKorban());
				parameters.put("PARAM_TGL_LAKA", sdf2.format(dto.getTglKejadianLaka()));
				parameters.put("PARAM_NO_LP", dto.getNoLaporanPolisiLaka());
				parameters.put("PARAM_TGL_LP", sdf.format(dto.getTglLaporanPolisiLaka()));
				parameters.put("PARAM_INSTANSI", dto.getNamaInstansi());
				parameters.put("PARAM_KASUS", dto.getKasusLaka());
				parameters.put("PARAM_PENJAMIN", dto.getPenjamin());
				parameters.put("PARAM_STATUS_KORBAN", dto.getStatusKorbanLaka());
				parameters.put("PARAM_LOKASI_LAKA", dto.getDeskripsiLokasiLaka());
				parameters.put("PARAM_LIST_TL", listTL);				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			

			File temp = File.createTempFile("Cetak Tl", ".pdf");
			FileOutputStream outs = new FileOutputStream(temp);
			File tempJsp = File.createTempFile("Jasper", ".jrxml");
			JasperReport jasper = null;
			try{
				ByteArrayOutputStream baos = getByteArrayOutputStream("ReportCetakTl.jrxml");
				try (OutputStream outputStream = new FileOutputStream(tempJsp)) {
					baos.writeTo(outputStream);
					outputStream.close();
				} catch (Exception x) {
					x.printStackTrace();
				}
				jasper = JasperCompileManager.compileReport(tempJsp
						.getAbsolutePath());
			}catch(Exception s){
				jasper = JasperCompileManager.compileReport("/home/glassfish/report/DataLakaReport.jrxml");
			}
			JasperPrint jasperReport = JasperFillManager.fillReport(jasper, parameters, new JREmptyDataSource(1));
			jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql"
	                ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
		
			// PDF-Export
			JRPdfExporter exportPdf = new JRPdfExporter();
			System.out.println(temp.getAbsolutePath());
			exportPdf.setParameter(JRExporterParameter.JASPER_PRINT, jasperReport);
			exportPdf.setParameter(JRExporterParameter.OUTPUT_STREAM, outs);
			exportPdf.exportReport();
		
			// show exported pdf 
			File f = new File(temp.getPath());
			byte[] buffer = new byte[(int) f.length()];
			FileInputStream fs = new FileInputStream(f);
			fs.read(buffer);
			fs.close();
			ByteArrayInputStream is = new ByteArrayInputStream(buffer);
			this.fileContent = new AMedia("Cetak TL", "pdf", "application/pdf", is);


		}catch(Exception p){
			p.printStackTrace();
		}
		Map<String, Object> args = new HashMap<>();
		args.put("media", fileContent);
		((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
//		System.out.println(JsonUtil.getJson(map));
		
	}
	
	private ByteArrayOutputStream getByteArrayOutputStream(String... param)
			throws IOException, FileNotFoundException {
		URL url;
		String reportName = "";
		if(param.length>0){
			url = new URL(getLoc() + "/" + param[0]);
		}else{
			url = new URL(getLoc() + "/" + param[0]);
			
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			is = url.openStream();
			byte[] byteChunk = new byte[4096];
			int n;
			while ((n = is.read(byteChunk)) > 0) {
				baos.write(byteChunk, 0, n);
			}
		} catch (IOException e) {
			System.err.printf("Failed while reading bytes from %s: %s",
					url.toExternalForm(), e.getMessage());
			e.printStackTrace();
		} finally {
			if (is != null) {
				is.close();
			}
		}
	
		return baos;
	}
	
	private String getLoc() {
		String port = (Executions.getCurrent().getServerPort() == 80) ? ""
				: (":" + Executions.getCurrent().getServerPort());
		/*
		 * String url = Executions.getCurrent().getScheme() + "://" +
		 * Executions.getCurrent().getServerName() + port +
		 * Executions.getCurrent().getContextPath() +
		 * Executions.getCurrent().getDesktop().getRequestPath();
		 */
		String x = "";
		if(System.getProperty("location")!=null){
			if(System.getProperty("location").equalsIgnoreCase("linux")){
				x = "/home/glassfish/report";
			} else{
				x = Executions.getCurrent().getScheme() + "://"
						+ Executions.getCurrent().getServerName() + port
						+ Executions.getCurrent().getContextPath()+"/report";
			}
		}else{
			x = Executions.getCurrent().getScheme() + "://"
					+ Executions.getCurrent().getServerName() + port
					+ Executions.getCurrent().getContextPath()+"/report";
		}
		return x;
	}

	protected String hari(Date tgl){
		if (tgl == null){
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("E", Locale.ENGLISH);
		String ha = sdf.format(tgl);
		return 	ha.equalsIgnoreCase("Mon")?"Senin":
				ha.equalsIgnoreCase("Tue")?"Selasa":
				ha.equalsIgnoreCase("Wed")?"Rabu":
				ha.equalsIgnoreCase("Thu")?"Kamis":
				ha.equalsIgnoreCase("Fri")?"Jum'at":
				ha.equalsIgnoreCase("Sat")?"Sabtu":
				ha.equalsIgnoreCase("Sun")?"Minggu":"";
	}


	public Form getFormMaster() {
		return formMaster;
	}

	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}

	public Form getFormDetail() {
		return formDetail;
	}

	public void setFormDetail(Form formDetail) {
		this.formDetail = formDetail;
	}

	public DasiJrRefCodeDto getRefCodeDto(){ 
		return refCodeDto;
	}

	public void setRefCodeDto(DasiJrRefCodeDto refCodeDto) {
		this.refCodeDto = refCodeDto;
	}

	public List<DasiJrRefCodeDto> getListRefCodeDto() {
		return listRefCodeDto;
	}

	public void setListRefCodeDto(List<DasiJrRefCodeDto> listRefCodeDto) {
		this.listRefCodeDto = listRefCodeDto;
	}

	public DasiJrRefCodeDto getAsalBerkasDto() {
		return asalBerkasDto;
	}

	public void setAsalBerkasDto(DasiJrRefCodeDto asalBerkasDto) {
		this.asalBerkasDto = asalBerkasDto;
	}

	public List<DasiJrRefCodeDto> getListAsalBerkasDto() {
		return listAsalBerkasDto;
	}

	public void setListAsalBerkasDto(List<DasiJrRefCodeDto> listAsalBerkasDto) {
		this.listAsalBerkasDto = listAsalBerkasDto;
	}

	public DasiJrRefCodeDto getTindakLanjutDto() {
		return tindakLanjutDto;
	}

	public void setTindakLanjutDto(DasiJrRefCodeDto tindakLanjutDto) {
		this.tindakLanjutDto = tindakLanjutDto;
	}

	public List<DasiJrRefCodeDto> getListTindakLanjutDto() {
		return listTindakLanjutDto;
	}

	public void setListTindakLanjutDto(
			List<DasiJrRefCodeDto> listTindakLanjutDto) {
		this.listTindakLanjutDto = listTindakLanjutDto;
	}

	public DasiJrRefCodeDto getSifatCideraDto() {
		return sifatCideraDto;
	}

	public void setSifatCideraDto(DasiJrRefCodeDto sifatCideraDto) {
		this.sifatCideraDto = sifatCideraDto;
	}

	public List<DasiJrRefCodeDto> getListSifatCideraDto() {
		return listSifatCideraDto;
	}

	public void setListSifatCideraDto(List<DasiJrRefCodeDto> listSifatCideraDto) {
		this.listSifatCideraDto = listSifatCideraDto;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public boolean isListIndex() {
		return listIndex;
	}

	public void setListIndex(boolean listIndex) {
		this.listIndex = listIndex;
	}

	public String getMonthReg() {
		return monthReg;
	}

	public void setMonthReg(String monthReg) {
		this.monthReg = monthReg;
	}

	public String getMonthLaka() {
		return monthLaka;
	}

	public void setMonthLaka(String monthLaka) {
		this.monthLaka = monthLaka;
	}

	public String getYearReg() {
		return yearReg;
	}

	public void setYearReg(String yearReg) {
		this.yearReg = yearReg;
	}

	public String getYearLaka() {
		return yearLaka;
	}

	public void setYearLaka(String yearLaka) {
		this.yearLaka = yearLaka;
	}

	public String getDayReg() {
		return dayReg;
	}

	public void setDayReg(String dayReg) {
		this.dayReg = dayReg;
	}

	public String getDayLaka() {
		return dayLaka;
	}

	public void setDayLaka(String dayLaka) {
		this.dayLaka = dayLaka;
	}

	public String getKodeKantor() {
		return kodeKantor;
	}

	public void setKodeKantor(String kodeKantor) {
		this.kodeKantor = kodeKantor;
	}

	public String getNamaKorban() {
		return namaKorban;
	}

	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}

	public String getNoReg() {
		return noReg;
	}

	public void setNoReg(String noReg) {
		this.noReg = noReg;
	}

	public String getNamaInstansi() {
		return namaInstansi;
	}

	public void setNamaInstansi(String namaInstansi) {
		this.namaInstansi = namaInstansi;
	}

	public String getNoLaporan() {
		return noLaporan;
	}

	public void setNoLaporan(String noLaporan) {
		this.noLaporan = noLaporan;
	}

	public String getAsalBerkas() {
		return asalBerkas;
	}

	public void setAsalBerkas(String asalBerkas) {
		this.asalBerkas = asalBerkas;
	}

	public String getTindakLanjut() {
		return tindakLanjut;
	}

	public void setTindakLanjut(String tindakLanjut) {
		this.tindakLanjut = tindakLanjut;
	}

	public String getSifatCidera() {
		return sifatCidera;
	}

	public void setSifatCidera(String sifatCidera) {
		this.sifatCidera = sifatCidera;
	}

	public PlRegisterSementaraDto getRegisterSementaraDto() {
		return registerSementaraDto;
	}

	public void setRegisterSementaraDto(
			PlRegisterSementaraDto registerSementaraDto) {
		this.registerSementaraDto = registerSementaraDto;
	}

	public List<PlRegisterSementaraDto> getListRegisterSementaraDto() {
		return listRegisterSementaraDto;
	}

	public void setListRegisterSementaraDto(
			List<PlRegisterSementaraDto> listRegisterSementaraDto) {
		this.listRegisterSementaraDto = listRegisterSementaraDto;
	}

	public List<String> getListMonthReg() {
		return listMonthReg;
	}

	public void setListMonthReg(List<String> listMonthReg) {
		this.listMonthReg = listMonthReg;
	}

	public List<String> getListMonthLaka() {
		return listMonthLaka;
	}

	public void setListMonthLaka(List<String> listMonthLaka) {
		this.listMonthLaka = listMonthLaka;
	}

	public String getMonthRegStr() {
		return monthRegStr;
	}

	public void setMonthRegStr(String monthRegStr) {
		this.monthRegStr = monthRegStr;
	}

	public String getMonthLakaStr() {
		return monthLakaStr;
	}

	public void setMonthLakaStr(String monthLakaStr) {
		this.monthLakaStr = monthLakaStr;
	}

	public List<PlJaminanDto> getListjaminanDto() {
		return listjaminanDto;
	}

	public void setListjaminanDto(List<PlJaminanDto> listjaminanDto) {
		this.listjaminanDto = listjaminanDto;
	}

	public PlJaminanDto getJaminanDto() {
		return jaminanDto;
	}

	public void setJaminanDto(PlJaminanDto jaminanDto) {
		this.jaminanDto = jaminanDto;
	}

	public Date getDetailKejadianStartDate() {
		return detailKejadianStartDate;
	}

	public void setDetailKejadianStartDate(Date detailKejadianStartDate) {
		this.detailKejadianStartDate = detailKejadianStartDate;
	}

	public Date getDetailKejadianEndDate() {
		return detailKejadianEndDate;
	}

	public void setDetailKejadianEndDate(Date detailKejadianEndDate) {
		this.detailKejadianEndDate = detailKejadianEndDate;
	}

	public String getDetailInstansi() {
		return detailInstansi;
	}

	public void setDetailInstansi(String detailInstansi) {
		this.detailInstansi = detailInstansi;
	}

	public Date getDetailLaporanStartDate() {
		return detailLaporanStartDate;
	}

	public void setDetailLaporanStartDate(Date detailLaporanStartDate) {
		this.detailLaporanStartDate = detailLaporanStartDate;
	}

	public Date getDetailLaporanEndDate() {
		return detailLaporanEndDate;
	}

	public void setDetailLaporanEndDate(Date detailLaporanEndDate) {
		this.detailLaporanEndDate = detailLaporanEndDate;
	}

	public String getDetailNoLaporanPolisi() {
		return detailNoLaporanPolisi;
	}

	public void setDetailNoLaporanPolisi(String detailNoLaporanPolisi) {
		this.detailNoLaporanPolisi = detailNoLaporanPolisi;
	}

	public String getDetailNamaKorban() {
		return detailNamaKorban;
	}

	public void setDetailNamaKorban(String detailNamaKorban) {
		this.detailNamaKorban = detailNamaKorban;
	}

	public String getDetailKodeJaminan() {
		return detailKodeJaminan;
	}

	public void setDetailKodeJaminan(String detailKodeJaminan) {
		this.detailKodeJaminan = detailKodeJaminan;
	}

	public String getStatusLP() {
		return statusLP;
	}

	public void setStatusLP(String statusLP) {
		this.statusLP = statusLP;
	}

	public String getDetailSearch() {
		return detailSearch;
	}

	public void setDetailSearch(String detailSearch) {
		this.detailSearch = detailSearch;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDto() {
		return listDataLakaDto;
	}

	public void setListDataLakaDto(List<PlDataKecelakaanDto> listDataLakaDto) {
		this.listDataLakaDto = listDataLakaDto;
	}

	public PlDataKecelakaanDto getDataLakaDto() {
		return dataLakaDto;
	}

	public void setDataLakaDto(PlDataKecelakaanDto dataLakaDto) {
		this.dataLakaDto = dataLakaDto;
	}

	public boolean isListDetail() {
		return listDetail;
	}

	public void setListDetail(boolean listDetail) {
		this.listDetail = listDetail;
	}

	public int getDetailPageSize() {
		return detailPageSize;
	}

	public void setDetailPageSize(int detailPageSize) {
		this.detailPageSize = detailPageSize;
	}

	public List<PlInstansiDto> getListInstansi() {
		return listInstansi;
	}

	public void setListInstansi(List<PlInstansiDto> listInstansi) {
		this.listInstansi = listInstansi;
	}

	public PlInstansiDto getInstansiDto() {
		return instansiDto;
	}

	public void setInstansiDto(PlInstansiDto instansiDto) {
		this.instansiDto = instansiDto;
	}

	public Date getDetailTglRegister() {
		return detailTglRegister;
	}

	public void setDetailTglRegister(Date detailTglRegister) {
		this.detailTglRegister = detailTglRegister;
	}

	public DasiJrRefCodeDto getJenisIdentitasDto() {
		return jenisIdentitasDto;
	}

	public void setJenisIdentitasDto(DasiJrRefCodeDto jenisIdentitasDto) {
		this.jenisIdentitasDto = jenisIdentitasDto;
	}

	public List<DasiJrRefCodeDto> getListJenisIdentitasDto() {
		return listJenisIdentitasDto;
	}

	public void setListJenisIdentitasDto(
			List<DasiJrRefCodeDto> listJenisIdentitasDto) {
		this.listJenisIdentitasDto = listJenisIdentitasDto;
	}

	public DasiJrRefCodeDto getJenisHubunganDto() {
		return jenisHubunganDto;
	}

	public void setJenisHubunganDto(DasiJrRefCodeDto jenisHubunganDto) {
		this.jenisHubunganDto = jenisHubunganDto;
	}

	public List<DasiJrRefCodeDto> getListJenisHubunganDto() {
		return listJenisHubunganDto;
	}

	public void setListJenisHubunganDto(
			List<DasiJrRefCodeDto> listJenisHubunganDto) {
		this.listJenisHubunganDto = listJenisHubunganDto;
	}

	public PlRegisterSementaraDto getInsertDto() {
		return insertDto;
	}

	public void setInsertDto(PlRegisterSementaraDto insertDto) {
		this.insertDto = insertDto;
	}

	public String getNomerRegister() {
		return nomerRegister;
	}

	public void setNomerRegister(String nomerRegister) {
		this.nomerRegister = nomerRegister;
	}

	public String getNamaPemohon() {
		return namaPemohon;
	}

	public void setNamaPemohon(String namaPemohon) {
		this.namaPemohon = namaPemohon;
	}

	public String getDitanganiRs() {
		return ditanganiRs;
	}

	public void setDitanganiRs(String ditanganiRs) {
		this.ditanganiRs = ditanganiRs;
	}

	public String getNoIdentitas() {
		return noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getTelpPemohon() {
		return telpPemohon;
	}

	public void setTelpPemohon(String telpPemohon) {
		this.telpPemohon = telpPemohon;
	}

	public String getAlamatPemohon() {
		return alamatPemohon;
	}

	public void setAlamatPemohon(String alamatPemohon) {
		this.alamatPemohon = alamatPemohon;
	}

	public String getStatusKorban() {
		return statusKorban;
	}

	public void setStatusKorban(String statusKorban) {
		this.statusKorban = statusKorban;
	}

	public Boolean getSaveFlag() {
		return saveFlag;
	}

	public void setSaveFlag(Boolean saveFlag) {
		this.saveFlag = saveFlag;
	}

	public Date getDateTL() {
		return dateTL;
	}

	public void setDateTL(Date dateTL) {
		this.dateTL = dateTL;
	}

	public String getKeteranganTL() {
		return keteranganTL;
	}

	public void setKeteranganTL(String keteranganTL) {
		this.keteranganTL = keteranganTL;
	}

	public PlRumahSakitDto getRumahSakitDto() {
		return rumahSakitDto;
	}

	public void setRumahSakitDto(PlRumahSakitDto rumahSakitDto) {
		this.rumahSakitDto = rumahSakitDto;
	}

	public List<PlRumahSakitDto> getListRumahSakitDto() {
		return listRumahSakitDto;
	}

	public void setListRumahSakitDto(List<PlRumahSakitDto> listRumahSakitDto) {
		this.listRumahSakitDto = listRumahSakitDto;
	}

	public boolean isButtonAddForPengajuan() {
		return buttonAddForPengajuan;
	}

	public void setButtonAddForPengajuan(boolean buttonAddForPengajuan) {
		this.buttonAddForPengajuan = buttonAddForPengajuan;
	}

	public boolean isFlagPengajuan() {
		return flagPengajuan;
	}

	public void setFlagPengajuan(boolean flagPengajuan) {
		this.flagPengajuan = flagPengajuan;
	}

	public boolean isButtonAddForRegister() {
		return buttonAddForRegister;
	}

	public void setButtonAddForRegister(boolean buttonAddForRegister) {
		this.buttonAddForRegister = buttonAddForRegister;
	}

	public List<FndKantorJasaraharjaDto> getListKantorDto() {
		return listKantorDto;
	}

	public void setListKantorDto(List<FndKantorJasaraharjaDto> listKantorDto) {
		this.listKantorDto = listKantorDto;
	}

	public FndKantorJasaraharjaDto getKantorDto() {
		return kantorDto;
	}

	public void setKantorDto(FndKantorJasaraharjaDto kantorDto) {
		this.kantorDto = kantorDto;
	}

	public String getSearchKantor() {
		return searchKantor;
	}

	public void setSearchKantor(String searchKantor) {
		this.searchKantor = searchKantor;
	}

	public String getSearchInstansi() {
		return searchInstansi;
	}

	public void setSearchInstansi(String searchInstansi) {
		this.searchInstansi = searchInstansi;
	}

	public String getSearchAsalBerkas() {
		return searchAsalBerkas;
	}

	public void setSearchAsalBerkas(String searchAsalBerkas) {
		this.searchAsalBerkas = searchAsalBerkas;
	}

	public String getSearchTindakLanjut() {
		return searchTindakLanjut;
	}

	public void setSearchTindakLanjut(String searchTindakLanjut) {
		this.searchTindakLanjut = searchTindakLanjut;
	}

	public String getSearchSifatCidera() {
		return searchSifatCidera;
	}

	public void setSearchSifatCidera(String searchSifatCidera) {
		this.searchSifatCidera = searchSifatCidera;
	}

	public String getSearchJenisHubungan() {
		return searchJenisHubungan;
	}

	public void setSearchJenisHubungan(String searchJenisHubungan) {
		this.searchJenisHubungan = searchJenisHubungan;
	}

	public String getSearchJenisIdentitas() {
		return searchJenisIdentitas;
	}

	public void setSearchJenisIdentitas(String searchJenisIdentitas) {
		this.searchJenisIdentitas = searchJenisIdentitas;
	}

	public String getSearchAsalBerkasDetail() {
		return searchAsalBerkasDetail;
	}

	public void setSearchAsalBerkasDetail(String searchAsalBerkasDetail) {
		this.searchAsalBerkasDetail = searchAsalBerkasDetail;
	}

	public String getSearchTindakLanjutDetail() {
		return searchTindakLanjutDetail;
	}

	public void setSearchTindakLanjutDetail(String searchTindakLanjutDetail) {
		this.searchTindakLanjutDetail = searchTindakLanjutDetail;
	}

	public DasiJrRefCodeDto getAsalBerkasDetailDto() {
		return asalBerkasDetailDto;
	}

	public void setAsalBerkasDetailDto(DasiJrRefCodeDto asalBerkasDetailDto) {
		this.asalBerkasDetailDto = asalBerkasDetailDto;
	}

	public List<DasiJrRefCodeDto> getListAsalBerkasDetailDto() {
		return listAsalBerkasDetailDto;
	}

	public void setListAsalBerkasDetailDto(
			List<DasiJrRefCodeDto> listAsalBerkasDetailDto) {
		this.listAsalBerkasDetailDto = listAsalBerkasDetailDto;
	}

	public DasiJrRefCodeDto getTindakLanjutDetailDto() {
		return tindakLanjutDetailDto;
	}

	public void setTindakLanjutDetailDto(DasiJrRefCodeDto tindakLanjutDetailDto) {
		this.tindakLanjutDetailDto = tindakLanjutDetailDto;
	}

	public List<DasiJrRefCodeDto> getListTindakLanjutDetailDto() {
		return listTindakLanjutDetailDto;
	}

	public void setListTindakLanjutDetailDto(
			List<DasiJrRefCodeDto> listTindakLanjutDetailDto) {
		this.listTindakLanjutDetailDto = listTindakLanjutDetailDto;
	}

	public DasiJrRefCodeDto getSifatCideraDetailDto() {
		return sifatCideraDetailDto;
	}

	public void setSifatCideraDetailDto(DasiJrRefCodeDto sifatCideraDetailDto) {
		this.sifatCideraDetailDto = sifatCideraDetailDto;
	}

	public List<DasiJrRefCodeDto> getListSifatCideraDetailDto() {
		return listSifatCideraDetailDto;
	}

	public void setListSifatCideraDetailDto(
			List<DasiJrRefCodeDto> listSifatCideraDetailDto) {
		this.listSifatCideraDetailDto = listSifatCideraDetailDto;
	}

	public String getSearchSifatCideraPengajuan() {
		return searchSifatCideraPengajuan;
	}

	public void setSearchSifatCideraPengajuan(String searchSifatCideraPengajuan) {
		this.searchSifatCideraPengajuan = searchSifatCideraPengajuan;
	}

	public Date getTglMasukRs() {
		return tglMasukRs;
	}

	public void setTglMasukRs(Date tglMasukRs) {
		this.tglMasukRs = tglMasukRs;
	}

	public BigDecimal getJmlPengajuanLuka() {
		return jmlPengajuanLuka;
	}

	public void setJmlPengajuanLuka(BigDecimal jmlPengajuanLuka) {
		this.jmlPengajuanLuka = jmlPengajuanLuka;
	}

	public BigDecimal getJmlPengajuanKubur() {
		return jmlPengajuanKubur;
	}

	public void setJmlPengajuanKubur(BigDecimal jmlPengajuanKubur) {
		this.jmlPengajuanKubur = jmlPengajuanKubur;
	}

	public BigDecimal getJmlPengajuanAmbl() {
		return jmlPengajuanAmbl;
	}

	public void setJmlPengajuanAmbl(BigDecimal jmlPengajuanAmbl) {
		this.jmlPengajuanAmbl = jmlPengajuanAmbl;
	}

	public BigDecimal getJmlPengajuanP3K() {
		return jmlPengajuanP3K;
	}

	public void setJmlPengajuanP3K(BigDecimal jmlPengajuanP3K) {
		this.jmlPengajuanP3K = jmlPengajuanP3K;
	}

	public String getSearchRS() {
		return searchRS;
	}

	public void setSearchRS(String searchRS) {
		this.searchRS = searchRS;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isFlagEdit() {
		return flagEdit;
	}

	public void setFlagEdit(boolean flagEdit) {
		this.flagEdit = flagEdit;
	}

	public List<PlTindakLanjutDto> getListTindakLanjutPrintDto() {
		return listTindakLanjutPrintDto;
	}

	public void setListTindakLanjutPrintDto(
			List<PlTindakLanjutDto> listTindakLanjutPrintDto) {
		this.listTindakLanjutPrintDto = listTindakLanjutPrintDto;
	}

	public PlTindakLanjutDto getTindakLanjutPrintDto() {
		return tindakLanjutPrintDto;
	}

	public void setTindakLanjutPrintDto(PlTindakLanjutDto tindakLanjutPrintDto) {
		this.tindakLanjutPrintDto = tindakLanjutPrintDto;
	}

	public boolean isFlagTindakLanjut() {
		return flagTindakLanjut;
	}

	public void setFlagTindakLanjut(boolean flagTindakLanjut) {
		this.flagTindakLanjut = flagTindakLanjut;
	}

	public FndKantorJasaraharjaDto getKantorPengajuanDto() {
		return kantorPengajuanDto;
	}

	public void setKantorPengajuanDto(FndKantorJasaraharjaDto kantorPengajuanDto) {
		this.kantorPengajuanDto = kantorPengajuanDto;
	}

	public List<FndKantorJasaraharjaDto> getListKantorPengajuanDto() {
		return listKantorPengajuanDto;
	}

	public void setListKantorPengajuanDto(
			List<FndKantorJasaraharjaDto> listKantorPengajuanDto) {
		this.listKantorPengajuanDto = listKantorPengajuanDto;
	}

	public String getSearchKantorPengajuan() {
		return searchKantorPengajuan;
	}

	public void setSearchKantorPengajuan(String searchKantorPengajuan) {
		this.searchKantorPengajuan = searchKantorPengajuan;
	}

	public List<AuthUserDto> getListUserDto() {
		return listUserDto;
	}

	public void setListUserDto(List<AuthUserDto> listUserDto) {
		this.listUserDto = listUserDto;
	}

	public AuthUserDto getUserDto() {
		return userDto;
	}

	public void setUserDto(AuthUserDto userDto) {
		this.userDto = userDto;
	}

	public String getSearchUser() {
		return searchUser;
	}

	public void setSearchUser(String searchUser) {
		this.searchUser = searchUser;
	}

	public String getNoRegForPrintTL() {
		return noRegForPrintTL;
	}

	public void setNoRegForPrintTL(String noRegForPrintTL) {
		this.noRegForPrintTL = noRegForPrintTL;
	}

	public Date getTimeTL() {
		return timeTL;
	}

	public void setTimeTL(Date timeTL) {
		this.timeTL = timeTL;
	}

	public String getNoSuratPanggilanTL() {
		return noSuratPanggilanTL;
	}

	public void setNoSuratPanggilanTL(String noSuratPanggilanTL) {
		this.noSuratPanggilanTL = noSuratPanggilanTL;
	}

	public String getNoSuratJaminanTL() {
		return noSuratJaminanTL;
	}

	public void setNoSuratJaminanTL(String noSuratJaminanTL) {
		this.noSuratJaminanTL = noSuratJaminanTL;
	}

	public String getP1() {
		return p1;
	}

	public void setP1(String p1) {
		this.p1 = p1;
	}

	public String getP2() {
		return p2;
	}

	public void setP2(String p2) {
		this.p2 = p2;
	}

	public String getP3() {
		return p3;
	}

	public void setP3(String p3) {
		this.p3 = p3;
	}

	public String getP4() {
		return p4;
	}

	public void setP4(String p4) {
		this.p4 = p4;
	}

	public boolean isDisable() {
		return disable;
	}

	public void setDisable(boolean disable) {
		this.disable = disable;
	}

	public boolean isFlagPengajuanButton() {
		return flagPengajuanButton;
	}

	public void setFlagPengajuanButton(boolean flagPengajuanButton) {
		this.flagPengajuanButton = flagPengajuanButton;
	}

	public boolean isFlagTLRS() {
		return flagTLRS;
	}

	public void setFlagTLRS(boolean flagTLRS) {
		this.flagTLRS = flagTLRS;
	}

	public List<PlInstansiDto> getListInstansiDto() {
		return listInstansiDto;
	}

	public void setListInstansiDto(List<PlInstansiDto> listInstansiDto) {
		this.listInstansiDto = listInstansiDto;
	}

	public List<PlInstansiDto> getListInstansiDtoCopy() {
		return listInstansiDtoCopy;
	}

	public void setListInstansiDtoCopy(List<PlInstansiDto> listInstansiDtoCopy) {
		this.listInstansiDtoCopy = listInstansiDtoCopy;
	}

	public List<PlJaminanDto> getListJenisPertanggunganDto() {
		return listJenisPertanggunganDto;
	}

	public void setListJenisPertanggunganDto(
			List<PlJaminanDto> listJenisPertanggunganDto) {
		this.listJenisPertanggunganDto = listJenisPertanggunganDto;
	}

	public PlJaminanDto getJenisPertanggunganDto() {
		return jenisPertanggunganDto;
	}

	public void setJenisPertanggunganDto(PlJaminanDto jenisPertanggunganDto) {
		this.jenisPertanggunganDto = jenisPertanggunganDto;
	}

	public List<String> getListStatusLp() {
		return listStatusLp;
	}

	public void setListStatusLp(List<String> listStatusLp) {
		this.listStatusLp = listStatusLp;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDtoCopy() {
		return listDataLakaDtoCopy;
	}

	public void setListDataLakaDtoCopy(
			List<PlDataKecelakaanDto> listDataLakaDtoCopy) {
		this.listDataLakaDtoCopy = listDataLakaDtoCopy;
	}

	public List<PlKorbanKecelakaanDto> getListDataKorbanDto() {
		return listDataKorbanDto;
	}

	public void setListDataKorbanDto(
			List<PlKorbanKecelakaanDto> listDataKorbanDto) {
		this.listDataKorbanDto = listDataKorbanDto;
	}

	public List<String> getListStatusKorban() {
		return listStatusKorban;
	}

	public void setListStatusKorban(List<String> listStatusKorban) {
		this.listStatusKorban = listStatusKorban;
	}

	public String getDiajukanDi() {
		return diajukanDi;
	}

	public void setDiajukanDi(String diajukanDi) {
		this.diajukanDi = diajukanDi;
	}

	public DukcapilWinDto getDukcapilWinDto() {
		return dukcapilWinDto;
	}

	public void setDukcapilWinDto(DukcapilWinDto dukcapilWinDto) {
		this.dukcapilWinDto = dukcapilWinDto;
	}
	public List<FndCamatDto> getListProvinsiDto() {
		return listProvinsiDto;
	}

	public void setListProvinsiDto(List<FndCamatDto> listProvinsiDto) {
		this.listProvinsiDto = listProvinsiDto;
	}

	public List<FndCamatDto> getListKabkotaDto() {
		return listKabkotaDto;
	}

	public void setListKabkotaDto(List<FndCamatDto> listKabkotaDto) {
		this.listKabkotaDto = listKabkotaDto;
	}

	public List<FndCamatDto> getListCamatDto() {
		return listCamatDto;
	}

	public void setListCamatDto(List<FndCamatDto> listCamatDto) {
		this.listCamatDto = listCamatDto;
	}

	public FndCamatDto getProvinsiDto() {
		return provinsiDto;
	}

	public void setProvinsiDto(FndCamatDto provinsiDto) {
		this.provinsiDto = provinsiDto;
	}

	public FndCamatDto getKabKotaDto() {
		return kabKotaDto;
	}

	public void setKabKotaDto(FndCamatDto kabKotaDto) {
		this.kabKotaDto = kabKotaDto;
	}

	public FndCamatDto getCamatDto() {
		return camatDto;
	}

	public void setCamatDto(FndCamatDto camatDto) {
		this.camatDto = camatDto;
	}

	public String getMsgTlRs() {
		return msgTlRs;
	}

	public void setMsgTlRs(String msgTlRs) {
		this.msgTlRs = msgTlRs;
	}

	public List<PlTlRDto> getListPlTlRDtos() {
		return listPlTlRDtos;
	}

	public void setListPlTlRDtos(List<PlTlRDto> listPlTlRDtos) {
		this.listPlTlRDtos = listPlTlRDtos;
	}

	public boolean isDisabledTl() {
		return disabledTl;
	}

	public void setDisabledTl(boolean disabledTl) {
		this.disabledTl = disabledTl;
	}

	public boolean isFlagEditTl() {
		return flagEditTl;
	}

	public void setFlagEditTl(boolean flagEditTl) {
		this.flagEditTl = flagEditTl;
	}

	public AMedia getFileContent() {
		return fileContent;
	}

	public void setFileContent(AMedia fileContent) {
		this.fileContent = fileContent;
	}

	public PlTlRDto getPlTlRDto() {
		return plTlRDto;
	}

	public void setPlTlRDto(PlTlRDto plTlRDto) {
		this.plTlRDto = plTlRDto;
	}
}
