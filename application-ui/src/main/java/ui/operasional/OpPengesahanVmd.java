package ui.operasional;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import Bpm.ListTaskByUser.UserTask;
import Bpm.ListTaskByUser.UserTaskService;
import share.DasiJrRefCodeDto;
import share.FndBankDto;
import share.FndKantorJasaraharjaDto;
import share.PlAdditionalDescDto;
import share.PlDataKecelakaanDto;
import share.PlDisposisiDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import share.PlRegisterSementaraDto;
import ui.operasional.dokumen.CetakLdpb;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass = true)
public class OpPengesahanVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpPengesahan/_index.zul";
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/Pengesahan.zul";
	private final String RESUME_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DetailRingkasanPengajuan.zul";

	private final String WS_URI = "/OpPengesahan";
	private final String WS_URI_LOV = "/Lov";
	private final String WS_URI_SANTUNAN = "/OpPengajuanSantunan";
	private final String WS_URI_LAKA = "/OpDataKecelakaan";

	private List<String> listPilihanPengajuan = new ArrayList<>();
	private List<PlPengajuanSantunanDto> listIndex = new ArrayList<>();
	private List<PlPengajuanSantunanDto> listIndexCopy = new ArrayList<>();
	private PlPengajuanSantunanDto plPengajuanSantunanDto = new PlPengajuanSantunanDto();

	private List<PlPengajuanSantunanDto> listPlPengajuanSantunanDto = new ArrayList<>();

	private List<PlDisposisiDto> listDisposisiDto = new ArrayList<>();
	private PlDisposisiDto plDisposisiDto = new PlDisposisiDto();

	private List<PlDisposisiDto> listDariDisposisiDto = new ArrayList<>();

	private List<DasiJrRefCodeDto> listDisposisiRefCodeDto = new ArrayList<>();
	private DasiJrRefCodeDto disposisiRefCodeDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listSifatCidera = new ArrayList<>();
	private DasiJrRefCodeDto sifatCideraDto = new DasiJrRefCodeDto();

	private FndKantorJasaraharjaDto fndKantorJasaraharjaDto = new FndKantorJasaraharjaDto();

	private List<DasiJrRefCodeDto> listKodeDisposisi = new ArrayList<>();
	private DasiJrRefCodeDto kodeDisposisiDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listStatusProses = new ArrayList<>();
	private DasiJrRefCodeDto statusProsesDto = new DasiJrRefCodeDto();
	
	private List<PlDataKecelakaanDto> listDataLakaDto = new ArrayList<>();
	private PlDataKecelakaanDto plDataKecelakaanDto = new PlDataKecelakaanDto();
	private PlDataKecelakaanDto selectedKecelakaan = new PlDataKecelakaanDto();
	
	private List<DasiJrRefCodeDto> listOtorisasiFlag = new ArrayList<>();
	
	private List<PlDisposisiDto> listDisposisi = new ArrayList<>();
	private List<PlPengajuanSantunanDto> listDto = new ArrayList<>();
	
	private List<PlPenyelesaianSantunanDto> listPlPenyelesaianSantunanDto = new ArrayList<>();
	private PlPenyelesaianSantunanDto plPenyelesaianSantunanDto = new PlPenyelesaianSantunanDto();

	// for search and filter
	private Date tglPengajuan;
	private String noBerkas;
	private String searchIndex;
	private String pilihPengajuan;
	private int pageSize = 5;

	private String dariDetail;
	private Date tglDisposisiDetail;
	private String disposisiDetail;

	private BigDecimal biaya1;
	private BigDecimal biaya2;
	private String cideraKode;
	private String cideraKode2;
	private double total;

	private boolean verifikasiFlag = true;

	private boolean flagDari;
	
	private boolean listIndexWindow = false;

	private PlDisposisiDto disposisiDto = new PlDisposisiDto();
	
	private boolean modeVisibleTambah = false;
	private boolean modeVisibleBatal = true;
	private boolean modeVisibleMetodePembayaran;
	private boolean modeVisibleHapusEdit = false;
	private boolean modeReadonlyDisposisi = true;
	
	private PlAdditionalDescDto plAdditionalDescDto= new PlAdditionalDescDto();
	private FndBankDto fndBankDto=new FndBankDto();
	
	private DasiJrRefCodeDto jenisPembayaranDto = new DasiJrRefCodeDto();

	private UserTask userTask;
	
	
	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent()
			.getAttribute(UIConstants.SESS_LOGIN_ID);

	protected void loadList() {
		listPilihanPengajuan();
		UserTask ut = (UserTask) Executions.getCurrent().getAttribute("obj2");
		if (ut != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			setNoBerkas(ut.getNomorPermohonan());
			userTask = ut;
			Date tgl = null;
			try {
				tgl = sdf.parse(ut.getCreationDate());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			setTglPengajuan(tgl);
			setPilihPengajuan("Pengajuan Seluruhnya");
			//seacrh by no berkas
			if (getSearchIndex() == null || getSearchIndex().equalsIgnoreCase("")) {
				setSearchIndex("%%");
			}
			if (getNoBerkas() == null || getNoBerkas().equalsIgnoreCase("")) {
				setNoBerkas("%%");
			}
			if (getPilihPengajuan() == null
					|| getPilihPengajuan().equalsIgnoreCase("")) {
				setPilihPengajuan("%%");
			}
			if (getPilihPengajuan().contains("Diproses")) {
				setPilihPengajuan("BV");
			}

			HashMap<String, Object> filter = new HashMap<>();
			filter.put("pilihPengajuan", "Pengajuan Seluruhnya");
			filter.put("tglPengajuan", dateToString(tglPengajuan));
			filter.put("noBerkas", noBerkas);
			filter.put("search", searchIndex);
			if (searchIndex == null)
				searchIndex = "%%";

			RestResponse rest = callWs(WS_URI + "/all", filter, HttpMethod.POST);
			try {
				listIndex = JsonUtil.mapJsonToListObject(rest.getContents(),
						PlPengajuanSantunanDto.class);
				listIndexCopy=new ArrayList<>();
				listIndexCopy.addAll(listIndex);
				setListIndexWindow(true);
				for(PlPengajuanSantunanDto a : listIndex){
					plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
				}
				BindUtils.postNotifyChange(null, null, this, "listIndex");
				BindUtils.postNotifyChange(null, null, this, "listIndexCopy");
				BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
				BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			setNoBerkas(null);
			setTglPengajuan(null);
		}
	}

	@Command("search")
	public void searchIndex() {

		if (getSearchIndex() == null || getSearchIndex().equalsIgnoreCase("")) {
			setSearchIndex("%%");
		}
		if (getNoBerkas() == null || getNoBerkas().equalsIgnoreCase("")) {
			setNoBerkas("%%");
		}
		if (getPilihPengajuan() == null
				|| getPilihPengajuan().equalsIgnoreCase("")) {
			setPilihPengajuan("%%");
		}
		if (getPilihPengajuan().contains("Diproses")) {
			setPilihPengajuan("BV");
		}

		HashMap<String, Object> filter = new HashMap<>();
		filter.put("pilihPengajuan", pilihPengajuan);
		filter.put("tglPengajuan", dateToString(tglPengajuan));
		filter.put("noBerkas", noBerkas);
		filter.put("search", searchIndex);
		if (searchIndex == null)
			searchIndex = "%%";

		RestResponse rest = callWs(WS_URI + "/all", filter, HttpMethod.POST);
		try {
			listIndex = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlPengajuanSantunanDto.class);
			listIndexCopy=new ArrayList<>();
			listIndexCopy.addAll(listIndex);
			setListIndexWindow(true);
			for(PlPengajuanSantunanDto a : listIndex){
				plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
			}
			BindUtils.postNotifyChange(null, null, this, "listIndex");
			BindUtils.postNotifyChange(null, null, this, "listIndexCopy");
			BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
			BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command("pengesahan")
	public void prosesPengesahan(@BindingParam("popup") String popup,
			@BindingParam("item") PlPengajuanSantunanDto selectedPengajuan) {
		if (selectedPengajuan == null
				|| selectedPengajuan.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}
		Map<String, Object> args = new HashMap<>();
		Executions.getCurrent().setAttribute("obj", selectedPengajuan);
		Executions.getCurrent().setAttribute("obj2", userTask);
		getPageInfo().setAddMode(true);
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	public void listPilihanPengajuan() {
		setPilihPengajuan("Pengajuan Untuk Diproses");
		listPilihanPengajuan.add("Pengajuan Untuk Diproses");
		listPilihanPengajuan.add("Pengajuan Seluruhnya");
		BindUtils.postNotifyChange(null, null, this, "pilihPengajuan");
		BindUtils.postNotifyChange(null, null, this, "listPilihanPengajuan");
	}
	
	public void onAdd() {
		plPengajuanSantunanDto = (PlPengajuanSantunanDto) Executions
				.getCurrent().getAttribute("obj");
		userTask = (UserTask) Executions.getCurrent().getAttribute("obj2");
		listDisposisi();
		listDariDisposisi();
		sifatCidera();

		// ================================= DATA
		// SANTUNAN==========================================================================
		HashMap<String, Object> map = new HashMap<>();
		map.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse rest = callWs(WS_URI_SANTUNAN + "/findByNoBerkas", map,
				HttpMethod.POST);
		try {
			listPlPengajuanSantunanDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), PlPengajuanSantunanDto.class);
			for (PlPengajuanSantunanDto a : listPlPengajuanSantunanDto) {
				plPengajuanSantunanDto.setIdKecelakaan(a.getIdKecelakaan());
				plPengajuanSantunanDto.setJumlahPengajuanMeninggal(a
						.getJumlahPengajuanMeninggal());
				plPengajuanSantunanDto.setJumlahPengajuanLukaluka(a
						.getJumlahPengajuanLukaluka());
				plPengajuanSantunanDto.setJmlPengajuanAmbl(a
						.getJmlPengajuanAmbl());
				plPengajuanSantunanDto.setJmlPengajuanP3k(a
						.getJmlPengajuanP3k());
				sifatCideraDto.setRvLowValue(a.getCideraKorban());
				statusProsesDto.setRvLowValue(a.getStatusProses());
			}
			plPengajuanSantunanDto.setStatusProses(plPengajuanSantunanDto
					.getStatusProses());
			for (DasiJrRefCodeDto statusProses : listStatusProses) {
				if (statusProsesDto.getRvLowValue().equalsIgnoreCase(
						statusProses.getRvLowValue())) {
					statusProsesDto = new DasiJrRefCodeDto();
					statusProsesDto = statusProses;
				}
			}

			for (DasiJrRefCodeDto sifatCidera : listSifatCidera) {
				if (sifatCideraDto.getRvLowValue().equalsIgnoreCase(
						sifatCidera.getRvLowValue())) {
					sifatCideraDto = new DasiJrRefCodeDto();
					sifatCideraDto = sifatCidera;
				}
			}
			rupiahPengajuan();
			metodePembayaran();
			if(fndBankDto.getNamaBank()!=null){
				setModeVisibleMetodePembayaran(true);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	//========================DATA  PENYELESAIAN==================================================	
		HashMap<String, Object> map2 = new HashMap<>();
		map2.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());		
		RestResponse rest2 = callWs(WS_URI_LOV+"/getPenyelesaianSantunanByNoBerkas", map2, HttpMethod.POST);
		try {
			listPlPenyelesaianSantunanDto = JsonUtil.mapJsonToListObject(rest2.getContents(), PlPenyelesaianSantunanDto.class);
			for(PlPenyelesaianSantunanDto a : listPlPenyelesaianSantunanDto){
				setBiaya1(a.getJumlahDibayarMeninggal());
				setBiaya2(a.getJumlahDibayarLukaluka());
				plPenyelesaianSantunanDto.setJmlByrAmbl(a.getJmlByrAmbl());
				plPenyelesaianSantunanDto.setJmlByrP3k(a.getJmlByrP3k());
			}
			int i = getBiaya1().intValue()+getBiaya2().intValue()+plPenyelesaianSantunanDto.getJmlByrAmbl().intValue()+plPenyelesaianSantunanDto.getJmlByrP3k().intValue();
			setTotal(i);
			BindUtils.postNotifyChange(null, null, this, "plPenyelesaianSantunanDto");			
			BindUtils.postNotifyChange(null, null, this, "biaya1");
			BindUtils.postNotifyChange(null, null, this, "biaya2");
			BindUtils.postNotifyChange(null, null, this, "total");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	

	public void sifatCidera() {
		RestResponse restCidera = callWs(WS_URI_LOV + "/getListSifatCidera",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listSifatCidera = JsonUtil.mapJsonToListObject(
					restCidera.getContents(), DasiJrRefCodeDto.class);
			for (DasiJrRefCodeDto o : listSifatCidera) {
				System.out.println("Test" + listSifatCidera);
			}
			setTotalSize(restCidera.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listSifatCidera");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange({ "cideraKode", "cideraKode2" })
	@Command
	public void rupiahPengajuan() {
		sifatCidera();
		if (sifatCideraDto.getRvLowValue() == "01"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("01")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);

		} else if (sifatCideraDto.getRvLowValue() == "02"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("02")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);

		} else if (sifatCideraDto.getRvLowValue() == "04"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("04")) {
			setCideraKode(sifatCideraDto.getRvHighValue());

		} else if (sifatCideraDto.getRvLowValue() == "05"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("05")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);

		} else if (sifatCideraDto.getRvLowValue() == "06"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("06")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);

		} else if (sifatCideraDto.getRvLowValue() == "07"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("07")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
		} else if (sifatCideraDto.getRvLowValue() == "08"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("08")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
		}
	}

	
	
	@Command
	public void listDisposisi() {
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse restDisposisi = callWs(WS_URI_LOV + "/getListDisposisi",
				mapInput, HttpMethod.POST);

		try {
			listDisposisiDto = JsonUtil.mapJsonToListObject(
					restDisposisi.getContents(), PlDisposisiDto.class);
			for (PlDisposisiDto a : listDisposisiDto) {
				plDisposisiDto.setDari(a.getDari());
				plDisposisiDto.setTglDisposisi(a.getTglDisposisi());
				plDisposisiDto.setDisposisi(a.getDisposisi());
				plDisposisiDto.setIdDisposisi(a.getIdDisposisi());
			}

			BindUtils.postNotifyChange(null, null, this, "plDisposisiDto");
			BindUtils.postNotifyChange(null, null, this, "listDisposisiDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void listDariDisposisi() {
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse restDisposisi = callWs(WS_URI_LOV + "/getListDisposisi",
				mapInput, HttpMethod.POST);

		try {
			listDariDisposisiDto = JsonUtil.mapJsonToListObject(
					restDisposisi.getContents(), PlDisposisiDto.class);
			for (PlDisposisiDto a : listDariDisposisiDto) {
				disposisiDto.setDari(a.getDari());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command("entryDisposisi")
	public void entryDisposisi(
			@BindingParam("item2") PlDisposisiDto selectedDisposisi) {
		listDariDisposisi();
		listKodeDisposisi();
		kodeDisposisi();
		if(disposisiRefCodeDto.getRvMeaning().equalsIgnoreCase(selectedDisposisi.getDari())){
			setModeVisibleHapusEdit(true);
			BindUtils.postNotifyChange(null, null, this, "modeVisibleHapusEdit");
		}else{
			setModeVisibleHapusEdit(false);
			BindUtils.postNotifyChange(null, null, this, "modeVisibleHapusEdit");
		}
		disposisiRefCodeDto.setRvMeaning(selectedDisposisi.getDari());
		setTglDisposisiDetail(selectedDisposisi.getTglDisposisi());
		setDisposisiDetail(selectedDisposisi.getDisposisi());
		for (DasiJrRefCodeDto dariDisposisi : listDisposisiRefCodeDto) {
			if (disposisiRefCodeDto.getRvMeaning().equalsIgnoreCase(dariDisposisi.getRvMeaning())) {
				disposisiRefCodeDto = new DasiJrRefCodeDto();
				disposisiRefCodeDto = dariDisposisi;
			}
		}
		setModeVisibleTambah(false);
		setModeVisibleBatal(true);
		BindUtils.postNotifyChange(null, null, this, "modeVisibleTambah");
		BindUtils.postNotifyChange(null, null, this, "modeVisibleBatal");

		BindUtils.postNotifyChange(null, null, this, "disposisiRefCodeDto");
		BindUtils.postNotifyChange(null, null, this, "tglDisposisiDetail");
		BindUtils.postNotifyChange(null, null, this, "disposisiDetail");
		BindUtils.postNotifyChange(null, null, this, "listDariDisposisiDto");
//		BindUtils.postNotifyChange(null, null, this, "listDisposisiDto");
		BindUtils.postNotifyChange(null, null, this, "plDisposisiDto");

	}

	@Command("tambahDisposisi")
	public void tambah() {
		kodeDisposisi();
		tglDisposisiDetail = new Date();
		disposisiDetail = new String();
		setModeVisibleTambah(true);
		setModeVisibleBatal(false);
		setModeReadonlyDisposisi(false);
		getPageInfo().setAddMode(true);
		BindUtils.postNotifyChange(null, null, this, "tglDisposisiDetail");
		BindUtils.postNotifyChange(null, null, this, "disposisiDetail");
		BindUtils.postNotifyChange(null, null, this, "modeVisibleTambah");
		BindUtils.postNotifyChange(null, null, this, "modeVisibleBatal");
		BindUtils.postNotifyChange(null, null, this, "modeReadonlyDisposisi");
	}
	
	@Command("saveDisposisi")
	public void saveDisposisi() {
		RestResponse restResponse = null;
//		Messagebox.show("test "+getPageInfo().isAddMode());
		if(getPageInfo().isAddMode()==true){
			String str = plPengajuanSantunanDto.getNoBerkas();
			String a= str.replace("-", "");
			
			Date today=new Date();
			SimpleDateFormat formatter= new SimpleDateFormat("ddMMyyyyHHmmss");
			String c = formatter.format(today);
			plDisposisiDto.setDari(disposisiRefCodeDto.getRvLowValue());
			plDisposisiDto.setTglDisposisi(getTglDisposisiDetail());
			plDisposisiDto.setDisposisi(getDisposisiDetail());
			plDisposisiDto.setIdDisposisi(a+"."+disposisiRefCodeDto.getRvLowValue()+"."+c);
			plDisposisiDto.setLevelCabangDisp(disposisiRefCodeDto.getRvHighValue());
			plDisposisiDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
			plDisposisiDto.setCreatedBy(userSession.getLoginID());
			plDisposisiDto.setCreationDate(new Date());
			restResponse = callWs(WS_URI_SANTUNAN + "/saveDisposisi", plDisposisiDto,
					HttpMethod.POST);

			if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(restResponse.getMessage());
				listDisposisi();
			} else {
				showErrorMsgBox(restResponse.getMessage());
			}			
		}else {
			plDisposisiDto.setCreatedBy(plDisposisiDto.getCreatedBy());
			plDisposisiDto.setCreationDate(plDisposisiDto.getCreationDate());
			plDisposisiDto.setDari(plDisposisiDto.getDari());
			plDisposisiDto.setDisposisi(getDisposisiDetail());
			plDisposisiDto.setIdDisposisi(plDisposisiDto.getIdDisposisi());
			plDisposisiDto.setIdGuid(plDisposisiDto.getIdGuid());
			plDisposisiDto.setJumlahDisetujui(plDisposisiDto.getJumlahDisetujui());
			plDisposisiDto.setLevelCabangDisp(plDisposisiDto.getLevelCabangDisp());
			plDisposisiDto.setNoBerkas(plDisposisiDto.getNoBerkas());
			plDisposisiDto.setTglDisposisi(plDisposisiDto.getTglDisposisi());
			plDisposisiDto.setLastUpdatedBy(userSession.getLoginID());
			plDisposisiDto.setLastUpdatedDate(new Date());
			restResponse = callWs(WS_URI_SANTUNAN + "/saveDisposisi", plDisposisiDto,
					HttpMethod.POST);
			if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(restResponse.getMessage());
				listDisposisi();
			} else {
				showErrorMsgBox(restResponse.getMessage());
			}			

		}

	}

	
	@Command("batalTambah")
	public void batalTambah(){
		listKodeDisposisi();
		disposisiRefCodeDto.setRvHighValue(null);
		setTglDisposisiDetail(null);
		setDisposisiDetail(null);
		setModeVisibleTambah(false);
		setModeVisibleBatal(true);
		setModeVisibleHapusEdit(false);
		setModeReadonlyDisposisi(true);
		BindUtils.postNotifyChange(null, null, this, "modeVisibleHapusEdit");
		BindUtils.postNotifyChange(null, null, this, "modeVisibleTambah");
		BindUtils.postNotifyChange(null, null, this, "modeVisibleBatal");		
		BindUtils.postNotifyChange(null, null, this, "disposisiRefCodeDto");
		BindUtils.postNotifyChange(null, null, this, "tglDisposisiDetail");
		BindUtils.postNotifyChange(null, null, this, "disposisiDetail");
		BindUtils.postNotifyChange(null, null, this, "modeReadonlyDisposisi");
	}
	
	@Command("cetakDisposisi")
	public void cetakDisposisi() throws IOException {		
		CetakLdpb cetak = new CetakLdpb();
		cetak.cetakDisposisi(plPengajuanSantunanDto);
	}
	
	@NotifyChange("listIndexCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listIndexCopy));
		if(listIndexCopy != null || listIndexCopy.size() >0 ){
			listIndexCopy.clear();
		}
		if(listIndex != null && listIndex.size() > 0){
			for(PlPengajuanSantunanDto dto : listIndex){
				System.out.println("+++");
				if (dto.getTglPengajuan() != null) {
					if (sdf.format(dto.getTglPenerimaan()).equals(cari)
							|| date.format(dto.getTglPenerimaan()).equals(cari)
							|| month.format(dto.getTglPenerimaan())
									.equals(cari)
							|| year.format(dto.getTglPenerimaan()).equals(cari)
							|| sdf.format(dto.getTglPengajuan()).equals(cari)
							|| date.format(dto.getTglPengajuan()).equals(cari)
							|| month.format(dto.getTglPengajuan()).equals(cari)
							|| year.format(dto.getTglPengajuan()).equals(cari)
							|| dto.getNoBerkas().toUpperCase().contains(cari)
							|| dto.getStatusProses().toUpperCase()
									.contains(cari)
							|| dto.getNama().toUpperCase().contains(cari)
							|| dto.getAlamatKorban().toUpperCase()
									.contains(cari)) 
					{
						listIndexCopy.add(dto);
					}
				} else {
					if (sdf.format(dto.getTglPenerimaan()).equals(cari)
							|| date.format(dto.getTglPenerimaan()).equals(cari)
							|| month.format(dto.getTglPenerimaan())
									.equals(cari)
							|| year.format(dto.getTglPenerimaan()).equals(cari)
							|| dto.getNoBerkas().toUpperCase().contains(cari)
							|| dto.getStatusProses().toUpperCase()
									.contains(cari)
							|| dto.getNama().toUpperCase().contains(cari)
							|| dto.getAlamatKorban().toUpperCase()
									.contains(cari)) 
					{
						listIndexCopy.add(dto);
					}
				}
			}
		}
	}
	
	@Command
	public void prosesSelanjutnya() throws InterruptedException{
		RestResponse restResponse = null;
		plPengajuanSantunanDto.setStatusProses("BD");
		plPengajuanSantunanDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
		
		restResponse = callWs(WS_URI_SANTUNAN + "/saveSantunanProsesSelanjutnya",plPengajuanSantunanDto, HttpMethod.POST);
		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox("Data berhasil dikirim ke Proses Selanjutnya", null);
			// update BPM ke proses selanjutnya
			 try {
				 String pattern = "dd-MM-yyyy HH:mm:ss";
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

					String dateFormat = simpleDateFormat.format(new Date());
//			 UserTaskService.updateUserTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
//			 plPengajuanSantunanDto.getNoBerkas(), "Entry Pengesahan Berkas","APPROVE",
//			 "", "",userSession.getUserName(), dateFormat, plPengajuanSantunanDto.getIdKecelakaan(),userSession.getUserLdap());
//			 TimeUnit.SECONDS.sleep(3);
					
//			 UserTaskService.updateUserTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
//					 plPengajuanSantunanDto.getNoBerkas(), "Entry Jurnal Pelayanan Pembayaran","APPROVE",
//					 "", "", userSession.getUserName(), dateFormat, plPengajuanSantunanDto.getIdKecelakaan(),Labels.getLabel("userBPM"));
			 
			 	UserTask ut = (UserTask) Executions.getCurrent().getAttribute("obj2");
				String instanceId = "";
				 if(userTask != null){
					 instanceId = userTask.getOtherInfo2().substring(5);
					 UserTask uTask = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), "Entry Pengesahan Berkas", instanceId);
					 uTask.setNomorPermohonan(userTask.getNomorPermohonan());
				 	 uTask.setPengajuanType(userTask.getPengajuanType());
				 	 uTask.setKodeCabang(userTask.getKodeCabang());
					 uTask.setCreationDate(userTask.getLastUpdateDate());
					 uTask.setCreatedBy(userSession.getUserName());
					 uTask.setLastUpdatedBy(userSession.getUserName());
					 uTask.setLastUpdateDate(dateFormat);
					 uTask.setIdGUID(userTask.getIdGUID());
					 uTask.setOtherInfo1("Entry Pengesahan Berkas");
					 uTask.setKodeKantor(userTask.getKodeKantor());
					 uTask.setOtherInfo2(userTask.getOtherInfo2());
					 uTask.setOtherInfo3(userTask.getOtherInfo3());
					 uTask.setOtherInfo4(userTask.getOtherInfo4());
					 uTask.setOtherInfo5(userTask.getOtherInfo5());
					 uTask.setTaskNumber(userTask.getTaskNumber());
					 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), userSession.getUserLdap(),"APPROVE", uTask, "");
					 
					 TimeUnit.SECONDS.sleep(3);
					 UserTaskService.updateUserTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
							 plPengajuanSantunanDto.getNoBerkas(), "Entry Jurnal Pelayanan Pembayaran","APPROVE",
							 "", "", userSession.getUserName(), dateFormat, plPengajuanSantunanDto.getIdKecelakaan(),Labels.getLabel("userBPM"));
				 }else{
					 instanceId = UserTaskService.getInstanceId("Entry Verifikasi Berkas", plPengajuanSantunanDto.getNoBerkas());
					 UserTaskService.updateUserTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
					 plPengajuanSantunanDto.getNoBerkas(), "Entry Pengesahan Berkas","APPROVE",
					 "", "",userSession.getUserName(), dateFormat, plPengajuanSantunanDto.getIdKecelakaan(),userSession.getUserLdap());
					 TimeUnit.SECONDS.sleep(3);
					 
					 UserTaskService.updateUserTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),
					 plPengajuanSantunanDto.getNoBerkas(), "Entry Jurnal Pelayanan Pembayaran","APPROVE",
					 "", "", userSession.getUserName(), dateFormat, plPengajuanSantunanDto.getIdKecelakaan(),Labels.getLabel("userBPM"));
				 }
				 
//				 System.out.println("INSTANCEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE IDNYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA : " + instanceId);
//				 UserTask uTask = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), "Entry Pengesahan Berkas", instanceId);
//				 uTask.setNomorPermohonan(userTask.getNomorPermohonan());
//			 	 uTask.setPengajuanType(userTask.getPengajuanType());
//			 	 uTask.setKodeCabang(userTask.getKodeCabang());
//				 uTask.setCreationDate(userTask.getLastUpdateDate());
//				 uTask.setCreatedBy(userSession.getUserName());
//				 uTask.setLastUpdatedBy(userSession.getUserName());
//				 uTask.setLastUpdateDate(dateFormat);
//				 uTask.setIdGUID(userTask.getIdGUID());
//				 uTask.setOtherInfo1("Entry Pengesahan Berkas");
//				 uTask.setKodeKantor(userTask.getKodeKantor());
//				 uTask.setOtherInfo2(userTask.getOtherInfo2());
//				 uTask.setOtherInfo3(userTask.getOtherInfo3());
//				 uTask.setOtherInfo4(userTask.getOtherInfo4());
//				 uTask.setOtherInfo5(userTask.getOtherInfo5());
//				 uTask.setTaskNumber(userTask.getTaskNumber());
//				 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), userSession.getUserLdap(),"APPROVE", uTask, "");
//				 
//				 TimeUnit.SECONDS.sleep(3);
				 
//				 UserTask uTask2 = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), "Entry Jurnal Pelayanan Pembayaran", instanceId);
//				 uTask2.setNomorPermohonan(userTask.getNomorPermohonan());
//			 	 uTask2.setPengajuanType(userTask.getPengajuanType());
//			 	 uTask2.setKodeCabang(userTask.getKodeCabang());
//				 uTask2.setOtherInfo1("Entry Jurnal Pelayanan Pembayaran");
//				 uTask2.setCreationDate(userTask.getLastUpdateDate());
//				 uTask2.setCreatedBy(userSession.getUserName());
//				 uTask2.setLastUpdatedBy(userSession.getUserName());
//				 uTask2.setLastUpdateDate(dateFormat);
//				 uTask2.setIdGUID(userTask.getIdGUID());
//				 uTask2.setKodeKantor(userTask.getKodeKantor());
//				 uTask2.setOtherInfo2(userTask.getOtherInfo2());
//				 uTask2.setOtherInfo3(userTask.getOtherInfo3());
//				 uTask2.setOtherInfo4(userTask.getOtherInfo4());
//				 uTask2.setOtherInfo5(userTask.getOtherInfo5());
//				 uTask2.setTaskNumber(userTask.getTaskNumber());
//				 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), Labels.getLabel("userBPM"),"APPROVE", uTask2, "");
			 
			 } catch (Exception e) {
			 // TODO Auto-generated catch block
			 e.printStackTrace();
			 }
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}		
	}


	@Command
	public void kodeDisposisi() {

		listKodeDisposisi();
		
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("statusProses", "BD");
		mapInput.put("levelKantor", userSession.getLevelKantor());
		RestResponse restDisposisi = callWs(WS_URI_LOV + "/getKodeDisposisi",
				mapInput, HttpMethod.POST);
		try {
			listKodeDisposisi = JsonUtil.mapJsonToListObject(
					restDisposisi.getContents(), DasiJrRefCodeDto.class);
			for (DasiJrRefCodeDto a : listKodeDisposisi) {
				disposisiRefCodeDto.setRvMeaning(a.getRvMeaning());
				disposisiRefCodeDto.setRvLowValue(a.getRvLowValue());
				disposisiRefCodeDto.setRvHighValue(a.getRvHighValue());
				disposisiRefCodeDto.setRvAbbreviation(a.getRvAbbreviation());
			}
			for(DasiJrRefCodeDto kodeDisposisi : listDisposisiRefCodeDto){
				if(disposisiRefCodeDto.getRvHighValue().equalsIgnoreCase(kodeDisposisi.getRvHighValue()) && 
						disposisiRefCodeDto.getRvLowValue().equalsIgnoreCase(kodeDisposisi.getRvLowValue()) && 
						disposisiRefCodeDto.getRvAbbreviation().equalsIgnoreCase(kodeDisposisi.getRvAbbreviation())){
						disposisiRefCodeDto = new DasiJrRefCodeDto();
						disposisiRefCodeDto = kodeDisposisi;
				}
			}
			BindUtils.postNotifyChange(null, null, this, "disposisiRefCodeDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listKodeDisposisi(){
		Map<String,Object> map = new HashMap<>();
		RestResponse rest = callWs(WS_URI_LOV+"/getListKodeDisposisi", map, HttpMethod.POST);
		listDisposisiRefCodeDto = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
//			for(DasiJrRefCodeDto a : listDisposisiRefCodeDto){
//				disposisiRefCodeDto.setRvAbbreviation(a.getRvAbbreviation());
//				disposisiRefCodeDto.setRvLowValue(a.getRvLowValue());
//				disposisiRefCodeDto.setRvHighValue(a.getRvHighValue());
//				disposisiRefCodeDto.setRvMeaning(a.getRvMeaning());
//			}
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("");
			kosong.setRvLowValue(null);
			kosong.setRvHighValue(null);
			kosong.setRvAbbreviation(null);
			listDisposisiRefCodeDto.add(kosong);
			listDisposisiRefCodeDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listDisposisiRefCodeDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void metodePembayaran(){
		Map<String,Object> map = new HashMap<>();
	//	Messagebox.show("test "+plPengajuanSantunanDto.getNoBerkas());
		map.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());
		RestResponse rest = callWs(WS_URI_LOV+"/getMetodePembayaran", map, HttpMethod.POST);
		try {
			listPlPenyelesaianSantunanDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlPenyelesaianSantunanDto.class);
			for(PlPenyelesaianSantunanDto a : listPlPenyelesaianSantunanDto){
				jenisPembayaranDto.setRvMeaning(a.getJenisPembayaran());
				fndBankDto.setNamaBank(a.getNamaBank());
				plAdditionalDescDto.setNamaRekening(a.getNamaRekening());
				plPenyelesaianSantunanDto.setNoRekening(a.getNoRekening());
			}
		//	Messagebox.show("Tampil list metodePembayaran"+plPenyelesaianSantunanDto.getNoRekening());
			BindUtils.postNotifyChange(null, null, this, "listPlPenyelesaianSantunanDto");
			BindUtils.postNotifyChange(null, null, this, "fndBankDto");
			BindUtils.postNotifyChange(null, null, this, "plAdditionalDescDto");
			BindUtils.postNotifyChange(null, null, this, "jenisPembayaranDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void editDisposisi(){
		if(plDisposisiDto.getIdDisposisi()==null){
			Messagebox.show("Id Disposisi null !");
		}
		setModeReadonlyDisposisi(false);
		setModeVisibleTambah(true);
		setModeVisibleBatal(false);
		setModeVisibleHapusEdit(false);
		getPageInfo().setAddMode(false);
		Map<String, Object> map = new HashMap<>();
		map.put("idDisposisi", plDisposisiDto.getIdDisposisi());
		RestResponse rest = callWs(WS_URI_LOV+"/getDisposisiById", map, HttpMethod.POST);
//		Messagebox.show("test "+plDisposisiDto.getIdDisposisi());
		try {
			listDisposisiDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlDisposisiDto.class);
			for(PlDisposisiDto a : listDisposisiDto){
				plDisposisiDto.setCreatedBy(a.getCreatedBy());
				plDisposisiDto.setCreationDate(a.getCreationDate());
				plDisposisiDto.setDari(a.getDari());
				plDisposisiDto.setDisposisi(a.getDisposisi());
				plDisposisiDto.setIdDisposisi(a.getIdDisposisi());
				plDisposisiDto.setIdGuid(a.getIdGuid());
				plDisposisiDto.setJumlahDisetujui(a.getJumlahDisetujui());
				plDisposisiDto.setLevelCabangDisp(a.getLevelCabangDisp());
				plDisposisiDto.setNoBerkas(a.getNoBerkas());
				plDisposisiDto.setTglDisposisi(a.getTglDisposisi());
			}
			BindUtils.postNotifyChange(null, null, this, "plDisposisiDto");
			BindUtils.postNotifyChange(null, null, this, "modeReadonlyDisposisi");
			BindUtils.postNotifyChange(null, null, this, "modeVisibleTambah");
			BindUtils.postNotifyChange(null, null, this, "modeVisibleBatal");
			BindUtils.postNotifyChange(null, null, this, "modeVisibleHapusEdit");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Command
	public void deleteDisposisi() {
		if(plDisposisiDto.getIdDisposisi()==null){
			Messagebox.show("Id Disposisi null !");
		}

		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							restRespone = callWs(WS_URI_LOV + "/deleteDisposisi",
									plDisposisiDto, HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data successfully deleted");
								listDisposisi();
							} else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});
	}



	@Command("back")
	public void back(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}

	@Command("detailRingkasanPengajuan")
	public void detailRingkasanPengajuan(
	@BindingParam("item") PlPengajuanSantunanDto selected) {
		navigate(RESUME_PAGE_PATH);

		Map<String, Object> map = new HashMap<>();
		map.put("noBerkas", selected.getNoBerkas());
		map.put("flagVerifikasi", verifikasiFlag);
		BindUtils.postGlobalCommand(null, null, "viewRingkasan", map);
	}
	
	public List<String> getListPilihanPengajuan() {
		return listPilihanPengajuan;
	}

	public void setListPilihanPengajuan(List<String> listPilihanPengajuan) {
		this.listPilihanPengajuan = listPilihanPengajuan;
	}

	public List<PlPengajuanSantunanDto> getListIndex() {
		return listIndex;
	}

	public void setListIndex(List<PlPengajuanSantunanDto> listIndex) {
		this.listIndex = listIndex;
	}

	public PlPengajuanSantunanDto getPlPengajuanSantunanDto() {
		return plPengajuanSantunanDto;
	}

	public void setPlPengajuanSantunanDto(
			PlPengajuanSantunanDto plPengajuanSantunanDto) {
		this.plPengajuanSantunanDto = plPengajuanSantunanDto;
	}

	public List<PlPengajuanSantunanDto> getListPlPengajuanSantunanDto() {
		return listPlPengajuanSantunanDto;
	}

	public void setListPlPengajuanSantunanDto(
			List<PlPengajuanSantunanDto> listPlPengajuanSantunanDto) {
		this.listPlPengajuanSantunanDto = listPlPengajuanSantunanDto;
	}

	public List<PlDisposisiDto> getListDisposisiDto() {
		return listDisposisiDto;
	}

	public void setListDisposisiDto(List<PlDisposisiDto> listDisposisiDto) {
		this.listDisposisiDto = listDisposisiDto;
	}

	public PlDisposisiDto getPlDisposisiDto() {
		return plDisposisiDto;
	}

	public void setPlDisposisiDto(PlDisposisiDto plDisposisiDto) {
		this.plDisposisiDto = plDisposisiDto;
	}

	public List<PlDisposisiDto> getListDariDisposisiDto() {
		return listDariDisposisiDto;
	}

	public void setListDariDisposisiDto(List<PlDisposisiDto> listDariDisposisiDto) {
		this.listDariDisposisiDto = listDariDisposisiDto;
	}

	public List<DasiJrRefCodeDto> getListDisposisiRefCodeDto() {
		return listDisposisiRefCodeDto;
	}

	public void setListDisposisiRefCodeDto(
			List<DasiJrRefCodeDto> listDisposisiRefCodeDto) {
		this.listDisposisiRefCodeDto = listDisposisiRefCodeDto;
	}

	public DasiJrRefCodeDto getDisposisiRefCodeDto() {
		return disposisiRefCodeDto;
	}

	public void setDisposisiRefCodeDto(DasiJrRefCodeDto disposisiRefCodeDto) {
		this.disposisiRefCodeDto = disposisiRefCodeDto;
	}

	public List<DasiJrRefCodeDto> getListSifatCidera() {
		return listSifatCidera;
	}

	public void setListSifatCidera(List<DasiJrRefCodeDto> listSifatCidera) {
		this.listSifatCidera = listSifatCidera;
	}

	public DasiJrRefCodeDto getSifatCideraDto() {
		return sifatCideraDto;
	}

	public void setSifatCideraDto(DasiJrRefCodeDto sifatCideraDto) {
		this.sifatCideraDto = sifatCideraDto;
	}

	public FndKantorJasaraharjaDto getFndKantorJasaraharjaDto() {
		return fndKantorJasaraharjaDto;
	}

	public void setFndKantorJasaraharjaDto(
			FndKantorJasaraharjaDto fndKantorJasaraharjaDto) {
		this.fndKantorJasaraharjaDto = fndKantorJasaraharjaDto;
	}

	public List<DasiJrRefCodeDto> getListKodeDisposisi() {
		return listKodeDisposisi;
	}

	public void setListKodeDisposisi(List<DasiJrRefCodeDto> listKodeDisposisi) {
		this.listKodeDisposisi = listKodeDisposisi;
	}

	public DasiJrRefCodeDto getKodeDisposisiDto() {
		return kodeDisposisiDto;
	}

	public void setKodeDisposisiDto(DasiJrRefCodeDto kodeDisposisiDto) {
		this.kodeDisposisiDto = kodeDisposisiDto;
	}

	public List<DasiJrRefCodeDto> getListStatusProses() {
		return listStatusProses;
	}

	public void setListStatusProses(List<DasiJrRefCodeDto> listStatusProses) {
		this.listStatusProses = listStatusProses;
	}

	public DasiJrRefCodeDto getStatusProsesDto() {
		return statusProsesDto;
	}

	public void setStatusProsesDto(DasiJrRefCodeDto statusProsesDto) {
		this.statusProsesDto = statusProsesDto;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDto() {
		return listDataLakaDto;
	}

	public void setListDataLakaDto(List<PlDataKecelakaanDto> listDataLakaDto) {
		this.listDataLakaDto = listDataLakaDto;
	}

	public PlDataKecelakaanDto getPlDataKecelakaanDto() {
		return plDataKecelakaanDto;
	}

	public void setPlDataKecelakaanDto(PlDataKecelakaanDto plDataKecelakaanDto) {
		this.plDataKecelakaanDto = plDataKecelakaanDto;
	}

	public PlDataKecelakaanDto getSelectedKecelakaan() {
		return selectedKecelakaan;
	}

	public void setSelectedKecelakaan(PlDataKecelakaanDto selectedKecelakaan) {
		this.selectedKecelakaan = selectedKecelakaan;
	}

	public List<DasiJrRefCodeDto> getListOtorisasiFlag() {
		return listOtorisasiFlag;
	}

	public void setListOtorisasiFlag(List<DasiJrRefCodeDto> listOtorisasiFlag) {
		this.listOtorisasiFlag = listOtorisasiFlag;
	}

	public List<PlDisposisiDto> getListDisposisi() {
		return listDisposisi;
	}

	public void setListDisposisi(List<PlDisposisiDto> listDisposisi) {
		this.listDisposisi = listDisposisi;
	}

	public List<PlPengajuanSantunanDto> getListDto() {
		return listDto;
	}

	public void setListDto(List<PlPengajuanSantunanDto> listDto) {
		this.listDto = listDto;
	}

	public List<PlPenyelesaianSantunanDto> getListPlPenyelesaianSantunanDto() {
		return listPlPenyelesaianSantunanDto;
	}

	public void setListPlPenyelesaianSantunanDto(
			List<PlPenyelesaianSantunanDto> listPlPenyelesaianSantunanDto) {
		this.listPlPenyelesaianSantunanDto = listPlPenyelesaianSantunanDto;
	}

	public PlPenyelesaianSantunanDto getPlPenyelesaianSantunanDto() {
		return plPenyelesaianSantunanDto;
	}

	public void setPlPenyelesaianSantunanDto(
			PlPenyelesaianSantunanDto plPenyelesaianSantunanDto) {
		this.plPenyelesaianSantunanDto = plPenyelesaianSantunanDto;
	}

	public Date getTglPengajuan() {
		return tglPengajuan;
	}

	public void setTglPengajuan(Date tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public String getPilihPengajuan() {
		return pilihPengajuan;
	}

	public void setPilihPengajuan(String pilihPengajuan) {
		this.pilihPengajuan = pilihPengajuan;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getDariDetail() {
		return dariDetail;
	}

	public void setDariDetail(String dariDetail) {
		this.dariDetail = dariDetail;
	}

	public Date getTglDisposisiDetail() {
		return tglDisposisiDetail;
	}

	public void setTglDisposisiDetail(Date tglDisposisiDetail) {
		this.tglDisposisiDetail = tglDisposisiDetail;
	}

	public String getDisposisiDetail() {
		return disposisiDetail;
	}

	public void setDisposisiDetail(String disposisiDetail) {
		this.disposisiDetail = disposisiDetail;
	}

	public BigDecimal getBiaya1() {
		return biaya1;
	}

	public void setBiaya1(BigDecimal biaya1) {
		this.biaya1 = biaya1;
	}

	public BigDecimal getBiaya2() {
		return biaya2;
	}

	public void setBiaya2(BigDecimal biaya2) {
		this.biaya2 = biaya2;
	}

	public String getCideraKode() {
		return cideraKode;
	}

	public void setCideraKode(String cideraKode) {
		this.cideraKode = cideraKode;
	}

	public String getCideraKode2() {
		return cideraKode2;
	}

	public void setCideraKode2(String cideraKode2) {
		this.cideraKode2 = cideraKode2;
	}

	public boolean isVerifikasiFlag() {
		return verifikasiFlag;
	}

	public void setVerifikasiFlag(boolean verifikasiFlag) {
		this.verifikasiFlag = verifikasiFlag;
	}

	public boolean isFlagDari() {
		return flagDari;
	}

	public void setFlagDari(boolean flagDari) {
		this.flagDari = flagDari;
	}

	public PlDisposisiDto getDisposisiDto() {
		return disposisiDto;
	}

	public void setDisposisiDto(PlDisposisiDto disposisiDto) {
		this.disposisiDto = disposisiDto;
	}

	public boolean isListIndexWindow() {
		return listIndexWindow;
	}

	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}

	public boolean isModeVisibleTambah() {
		return modeVisibleTambah;
	}

	public void setModeVisibleTambah(boolean modeVisibleTambah) {
		this.modeVisibleTambah = modeVisibleTambah;
	}

	public boolean isModeVisibleBatal() {
		return modeVisibleBatal;
	}

	public void setModeVisibleBatal(boolean modeVisibleBatal) {
		this.modeVisibleBatal = modeVisibleBatal;
	}

	public PlAdditionalDescDto getPlAdditionalDescDto() {
		return plAdditionalDescDto;
	}

	public void setPlAdditionalDescDto(PlAdditionalDescDto plAdditionalDescDto) {
		this.plAdditionalDescDto = plAdditionalDescDto;
	}

	public FndBankDto getFndBankDto() {
		return fndBankDto;
	}

	public void setFndBankDto(FndBankDto fndBankDto) {
		this.fndBankDto = fndBankDto;
	}

	public DasiJrRefCodeDto getJenisPembayaranDto() {
		return jenisPembayaranDto;
	}

	public void setJenisPembayaranDto(DasiJrRefCodeDto jenisPembayaranDto) {
		this.jenisPembayaranDto = jenisPembayaranDto;
	}

	public boolean isModeVisibleMetodePembayaran() {
		return modeVisibleMetodePembayaran;
	}

	public void setModeVisibleMetodePembayaran(boolean modeVisibleMetodePembayaran) {
		this.modeVisibleMetodePembayaran = modeVisibleMetodePembayaran;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public boolean isModeVisibleHapusEdit() {
		return modeVisibleHapusEdit;
	}

	public void setModeVisibleHapusEdit(boolean modeVisibleHapusEdit) {
		this.modeVisibleHapusEdit = modeVisibleHapusEdit;
	}

	public boolean isModeReadonlyDisposisi() {
		return modeReadonlyDisposisi;
	}

	public void setModeReadonlyDisposisi(boolean modeReadonlyDisposisi) {
		this.modeReadonlyDisposisi = modeReadonlyDisposisi;
	}

	public List<PlPengajuanSantunanDto> getListIndexCopy() {
		return listIndexCopy;
	}

	public void setListIndexCopy(List<PlPengajuanSantunanDto> listIndexCopy) {
		this.listIndexCopy = listIndexCopy;
	}


	
	
}
