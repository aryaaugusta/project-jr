package ui.operasional;

import java.io.IOException;
import java.sql.SQLException;

import net.sf.jasperreports.engine.JRException;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

public class PopUpDukcapilVmd {
	private AMedia fileContent;

	@Wire("#opAbsahDet")
	private Window winLov;

	@Command
	public void close() {
		if (winLov == null)
			throw new RuntimeException("id popUp tidak sama dengan viewModel");
		winLov.detach();
	}

	@Init
	public void baseInit(@ExecutionArgParam("media") AMedia fileContents,
			@ContextParam(ContextType.VIEW) Component view)
			throws ClassNotFoundException, SQLException, IOException,
			JRException, Exception {
		Selectors.wireComponents(view, this, false);
		this.fileContent = fileContents;
		System.out.println("Base init called");
		BindUtils.postNotifyChange(null, null, this, "fileContent");
	}

	public AMedia getFileContent() {
		return fileContent;
	}

	public void setFileContent(AMedia fileContent) {
		this.fileContent = fileContent;
	}

}
