package ui.operasional;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import Bpm.ListTaskByUser.UserTask;
import Bpm.ListTaskByUser.UserTaskService;
import share.DasiJrRefCodeDto;
import share.FndKantorJasaraharjaDto;
import share.PlDataKecelakaanDto;
import share.PlPengajuanSantunanDto;
import share.PlPenyelesaianSantunanDto;
import ui.operasional.dokumen.CetakLdpb;
import ui.operasionalDetail.DisposisiBerkasVmd;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;
import core.model.FndKantorJasaraharja;
import core.model.PlPenyelesaianSantunan;

@Init(superclass = true)
public class OpOtorisasiVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasional/OpOtorisasi/_index.zul";
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/ProsesOtorisasi.zul";
	private final String RINGKASAN_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/operasionalDetail/DetailRingkasanPengajuan.zul";

	private final String WS_URI = "/OpOtorisasi";
	private final String WS_URI_SANTUNAN = "/OpPengajuanSantunan";
	private final String WS_URI_LOV = "/Lov";
	
	private List<PlPengajuanSantunanDto> listIndex = new ArrayList<>();
	private List<PlPengajuanSantunanDto> listIndexCopy = new ArrayList<>();
	private List<String> listPilihanPengajuan = new ArrayList<>();
	
	private PlPengajuanSantunanDto plPengajuanSantunanDto = new PlPengajuanSantunanDto();
	private List<PlPengajuanSantunanDto> listPlPengajuanSantunanDto = new ArrayList<>();
	
	private List<DasiJrRefCodeDto> listKesimpulanSementara = new ArrayList<>();
	private DasiJrRefCodeDto kesimpulanSementaraDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listOtorisasiFlag = new ArrayList<>();
	private DasiJrRefCodeDto otorisasiFlagDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listKodeJenisPembayaran = new ArrayList<>();
	private DasiJrRefCodeDto kodeJenisPembayaranDto = new DasiJrRefCodeDto();

	private List<DasiJrRefCodeDto> listStatusProses = new ArrayList<>();
	private DasiJrRefCodeDto statusProsesDto = new DasiJrRefCodeDto();
	
	private List<FndKantorJasaraharjaDto> listOtorisasiTujuan = new ArrayList<>();
	private FndKantorJasaraharjaDto otorisasiTujuanDto = new FndKantorJasaraharjaDto();
	
	private List<PlPenyelesaianSantunanDto> listPlPenyelesaianSantunanDto = new ArrayList<>();
	private PlPenyelesaianSantunanDto plPenyelesaianSantunanDto = new PlPenyelesaianSantunanDto();
	
	private boolean listIndexWindow = false;
	private UserSessionJR ujr;
	
//	for search and filter
	private Date tglPenerimaan;
	private String noBerkas;
	private String searchIndex; 
	private String pilihPengajuan;
	private int pageSize = 5;
	
	private String searchKantor;
	
	//pengajuan rupiah
	private String cideraKode;
	private String cideraKode2;
	private BigDecimal biaya1;
	private BigDecimal biaya2;
	private DasiJrRefCodeDto sifatCideraDto = new DasiJrRefCodeDto();
	private List<DasiJrRefCodeDto> listSifatCidera = new ArrayList<>(); 
	
	//kesimpulan
	private boolean disableKesimpulan;
	private boolean disableJenisPembayaran;
	
	private boolean disableDilimpahkan;
	private double total;
	
	//akumulasi 
	private double akmMeninggal;
	private double akmLuka;
	private double akmCacat;
	private double akmPenguburan;
	private double akmAmbulance;
	private double akmP3k;
	//total
	private double totalMeninggal;
	private double totalLuka;
	private double totalCacat;
	private double totalPenguburan;
	private double totalAmbulance;
	private double totalP3k;
	
	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent().getAttribute(
			UIConstants.SESS_LOGIN_ID);
	
	private UserTask userTask;
    
	
	public UserTask getUserTask() {
		return userTask;
	}

	public void setUserTask(UserTask userTask) {
		this.userTask = userTask;
	}

	protected void loadList(){
		listPilihanPengajuan();
		UserTask ut = (UserTask) Executions.getCurrent().getAttribute("obj2");
		if (ut != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			setNoBerkas(ut.getNomorPermohonan());
			Date tgl = null;
			userTask = ut;
			try {
				tgl = sdf.parse(ut.getCreationDate());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			setTglPenerimaan(tgl);
			setPilihPengajuan("Pengajuan Seluruhnya");
			//list search berdasarkan nomor berkas
			if (getSearchIndex() == null || getSearchIndex().equalsIgnoreCase("")) {
				setSearchIndex("%%");
			}
			if (getNoBerkas() == null || getNoBerkas().equalsIgnoreCase("")) {
				setNoBerkas("%%");
			}
			if (getPilihPengajuan() == null
					|| getPilihPengajuan().equalsIgnoreCase("")) {
				setPilihPengajuan("%%");
			}
			if (getPilihPengajuan().contains("Diproses")) {
				setPilihPengajuan("BA");
			}

			HashMap<String, Object> filter = new HashMap<>();
			filter.put("pilihPengajuan", "Pengajuan Seluruhnya");
			filter.put("tglPenerimaan", dateToString(tglPenerimaan));
			filter.put("noBerkas", noBerkas);
			filter.put("search", searchIndex);
			RestResponse rest = callWs(WS_URI+"/all", filter, HttpMethod.POST);
			try{
				listIndex = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
				listIndexCopy = new ArrayList<>();
				listIndexCopy.addAll(listIndex);
				setListIndexWindow(true);
				for(PlPengajuanSantunanDto a : listIndex){
					plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
					plPengajuanSantunanDto.setIdKecelakaan(a.getIdKecelakaan());
				}
				BindUtils.postNotifyChange(null, null, this, "listIndex");
				BindUtils.postNotifyChange(null, null, this, "listIndexCopy");
				BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
				BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
			}catch(Exception e){
				e.printStackTrace();
			}
		
		} else {
			setNoBerkas(null);
			setTglPenerimaan(null);
		}
	}
	
	@Command("search")
	public void searchIndex(){
		
		if (getSearchIndex() == null || getSearchIndex().equalsIgnoreCase("")) {
			setSearchIndex("%%");
		}
		if (getNoBerkas() == null || getNoBerkas().equalsIgnoreCase("")) {
			setNoBerkas("%%");
		}
		if (getPilihPengajuan() == null
				|| getPilihPengajuan().equalsIgnoreCase("")) {
			setPilihPengajuan("%%");
		}
		if (getPilihPengajuan().contains("Diproses")) {
			setPilihPengajuan("BA");
		}

		HashMap<String, Object> filter = new HashMap<>();
		filter.put("pilihPengajuan", pilihPengajuan);
		filter.put("tglPenerimaan", dateToString(tglPenerimaan));
		filter.put("noBerkas", noBerkas);
		filter.put("search", searchIndex);
		RestResponse rest = callWs(WS_URI+"/all", filter, HttpMethod.POST);
		try{
			listIndex = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
			listIndexCopy = new ArrayList<>();
			listIndexCopy.addAll(listIndex);
			setListIndexWindow(true);
			for(PlPengajuanSantunanDto a : listIndex){
				plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
				plPengajuanSantunanDto.setIdKecelakaan(a.getIdKecelakaan());
			}
			BindUtils.postNotifyChange(null, null, this, "listIndex");
			BindUtils.postNotifyChange(null, null, this, "listIndexCopy");
			BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
			BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void listPilihanPengajuan(){
		setPilihPengajuan("Pengajuan Untuk Diproses");
		listPilihanPengajuan.add("Pengajuan Untuk Diproses");
		listPilihanPengajuan.add("Pengajuan Seluruhnya");
		BindUtils.postNotifyChange(null, null, this, "listPilihanPengajuan");
	}
	
	@Command("edit")
	public void edit(@BindingParam("popup") String popup, 
			@BindingParam("item")PlPengajuanSantunanDto selected){
		if (selected == null || selected.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}else if(selected.getStatusProses().equalsIgnoreCase("DT")||selected.getStatusProses().equalsIgnoreCase("BL")){
			showInfoMsgBox("Proses Belum Sampai Tahap Ini!", "");
			return;
		}
		Map<String, Object> args = new HashMap<>();
		Executions.getCurrent().setAttribute("obj", selected);
		Executions.getCurrent().setAttribute("obj2", userTask);
		getPageInfo().setEditMode(true);
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
//		navigate(DETAIL_PAGE_PATH);
	}
	
	public void onEdit(){
		plPengajuanSantunanDto = (PlPengajuanSantunanDto) Executions.getCurrent()
				.getAttribute("obj");
		userTask = (UserTask) Executions.getCurrent().getAttribute("obj2");
		listStatusProses();
		listKesimpulanSementara();
		listOtorisasiFlag();
		listKodeJenisPembayaran();
		findAllKantorJr();
		sifatCidera();
//		========================================GET DATA SANTUNAN====================================================
		HashMap<String, Object> map = new HashMap<>();
		map.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());		
		RestResponse rest = callWs(WS_URI_SANTUNAN+"/findByNoBerkas", map, HttpMethod.POST);
		try {
			listPlPengajuanSantunanDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
			for(PlPengajuanSantunanDto a : listPlPengajuanSantunanDto){
				plPengajuanSantunanDto.setTglPengajuan(a.getTglPengajuan());
				statusProsesDto.setRvLowValue(a.getStatusProses());
				kesimpulanSementaraDto.setRvLowValue(a.getKesimpulanSementara());
				otorisasiFlagDto.setRvLowValue(a.getOtorisasiFlag());
				sifatCideraDto.setRvLowValue(a.getCideraKorban());
				kodeJenisPembayaranDto.setRvLowValue(a.getJaminanPembayaran());
				plPengajuanSantunanDto.setJumlahPengajuanMeninggal(a.getJumlahPengajuanMeninggal());
				plPengajuanSantunanDto.setJumlahPengajuanLukaluka(a.getJumlahPengajuanLukaluka());
				plPengajuanSantunanDto.setJmlPengajuanAmbl(a.getJmlPengajuanAmbl());
				plPengajuanSantunanDto.setJmlPengajuanP3k(a.getJmlPengajuanP3k());
				plPengajuanSantunanDto.setDilimpahkanKe(a.getDilimpahkanKe());
				plPengajuanSantunanDto.setCideraKorban(a.getCideraKorban());
				plPengajuanSantunanDto.setStatusProses(a.getStatusProses());
				plPengajuanSantunanDto.setIdKorbanKecelakaan(a.getIdKorbanKecelakaan());
			}
			plPengajuanSantunanDto.setStatusProses(plPengajuanSantunanDto.getStatusProses());
			plPengajuanSantunanDto.setTglPengajuan(new Date());
			plPengajuanSantunanDto.setMenuOtorisasi(true);
			BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");
			for(DasiJrRefCodeDto statusProses : listStatusProses){
				if(statusProsesDto.getRvLowValue().equalsIgnoreCase(statusProses.getRvLowValue())){
					statusProsesDto = new DasiJrRefCodeDto();
					statusProsesDto = statusProses;
				}
			}
			for(DasiJrRefCodeDto kesimpulan : listKesimpulanSementara){
				if(kesimpulanSementaraDto.getRvLowValue().equalsIgnoreCase(kesimpulan.getRvLowValue())){
					kesimpulanSementaraDto = new DasiJrRefCodeDto();
					kesimpulanSementaraDto = kesimpulan;
				}
			}
			for(DasiJrRefCodeDto otorisasi : listOtorisasiFlag){
				if(otorisasiFlagDto.getRvLowValue().equalsIgnoreCase(otorisasi.getRvLowValue())){
					otorisasiFlagDto = new DasiJrRefCodeDto();
					otorisasiFlagDto = otorisasi;
				}
			}
			for(DasiJrRefCodeDto jenisPembayaran : listKodeJenisPembayaran){
				if(kodeJenisPembayaranDto.getRvLowValue().equalsIgnoreCase(jenisPembayaran.getRvLowValue())){
					kodeJenisPembayaranDto = new DasiJrRefCodeDto();
					kodeJenisPembayaranDto = jenisPembayaran;
				}
			}
			for(DasiJrRefCodeDto sifatCidera : listSifatCidera){
				if(sifatCideraDto.getRvLowValue().equalsIgnoreCase(sifatCidera.getRvLowValue())){
					sifatCideraDto = new DasiJrRefCodeDto();
					sifatCideraDto = sifatCidera;
				}
			}
			BindUtils.postNotifyChange(null, null, this, "kesimpulanSementaraDto");
			BindUtils.postNotifyChange(null, null, this, "statusProsesDto");
			BindUtils.postNotifyChange(null, null, this, "otorisasiFlagDto");
			BindUtils.postNotifyChange(null, null, this, "kodeJenisPembayaranDto");
			BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");

		} catch (Exception e) {
			e.printStackTrace();
		}			
//		========================================GET KANTOR DILIMPAHKAN==================================================
		getKantorDilimpahkan();
		if(otorisasiTujuanDto.getKodeNama()!=null){
			for(FndKantorJasaraharjaDto dilimpahkan : listOtorisasiTujuan){
				if(otorisasiTujuanDto.getKodeNama().equalsIgnoreCase(dilimpahkan.getKodeNama())){
					otorisasiTujuanDto = new FndKantorJasaraharjaDto();
					otorisasiTujuanDto = dilimpahkan;
				}
			}					
		}
		BindUtils.postNotifyChange(null, null, this, "otorisasiTujuanDto");	
		BindUtils.postNotifyChange(null, null, this, "listOtorisasiTujuan");	

//		========================================GET DATA PENYELESAIAN====================================================
		HashMap<String, Object> map2 = new HashMap<>();
		map2.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());		
		RestResponse rest2 = callWs(WS_URI_LOV+"/getPenyelesaianSantunanByNoBerkas", map2, HttpMethod.POST);
		try {
			listPlPenyelesaianSantunanDto = JsonUtil.mapJsonToListObject(rest2.getContents(), PlPenyelesaianSantunanDto.class);
			for(PlPenyelesaianSantunanDto a : listPlPenyelesaianSantunanDto){
				setBiaya1(a.getJumlahDibayarMeninggal()==null?new BigDecimal(0):a.getJumlahDibayarMeninggal());
				setBiaya2(a.getJumlahDibayarLukaluka()==null?new BigDecimal(0):a.getJumlahDibayarLukaluka());
				plPenyelesaianSantunanDto.setJmlByrAmbl(a.getJmlByrAmbl()==null?new BigDecimal(0):a.getJmlByrAmbl());
				plPenyelesaianSantunanDto.setJmlByrP3k(a.getJmlByrP3k()==null?new BigDecimal(0):a.getJmlByrAmbl());
				setTotalAmbulance(a.getJmlByrAmbl().doubleValue());
				setTotalP3k(a.getJmlByrP3k().doubleValue());
			}
			int i = getBiaya1().intValue()+getBiaya2().intValue()+plPenyelesaianSantunanDto.getJmlByrAmbl().intValue()+plPenyelesaianSantunanDto.getJmlByrP3k().intValue();
			setTotal(i);

			BindUtils.postNotifyChange(null, null, this, "plPenyelesaianSantunanDto");			
			BindUtils.postNotifyChange(null, null, this, "biaya1");
			BindUtils.postNotifyChange(null, null, this, "biaya2");
			BindUtils.postNotifyChange(null, null, this, "totalAmbulance");
			BindUtils.postNotifyChange(null, null, this, "totalP3k");
			rupiahPengajuan();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@NotifyChange("listIndexCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listIndexCopy));
		if(listIndexCopy != null || listIndexCopy.size() >0 ){
			listIndexCopy.clear();
		}
		if(listIndex != null && listIndex.size() > 0){
			for(PlPengajuanSantunanDto dto : listIndex){
				System.out.println("+++");
				if(
						sdf.format(dto.getTglPenerimaan()).equals(cari)||
						date.format(dto.getTglPenerimaan()).equals(cari)||
						month.format(dto.getTglPenerimaan()).equals(cari)||
						year.format(dto.getTglPenerimaan()).equals(cari)||
						dto.getNoBerkas().toUpperCase().contains(cari)||
						dto.getStatusProses().toUpperCase().contains(cari)||
						dto.getNama().toUpperCase().contains(cari)||
						dto.getAlamatKorban().toUpperCase().contains(cari))
				{
							listIndexCopy.add(dto);
				}
			}
		}
	}
	
//	public void otorisasiTujuan(){
//		HashMap<String, Object> map = new HashMap<>();
//		map.put("kodeKantor", plPengajuanSantunanDto.getDilimpahkanKe());
//		RestResponse rest = callWs(WS_URI_LOV + "/getKantorByKodeKantor",
//				map, HttpMethod.POST);
//		try {
//			listOtorisasiTujuan = JsonUtil.mapJsonToListObject(
//					rest.getContents(), FndKantorJasaraharjaDto.class);
//			for (FndKantorJasaraharjaDto o : listOtorisasiTujuan) {
//				otorisasiTujuanDto.setKodeKantorJr(o.getKodeKantorJr());
//				otorisasiTujuanDto.setNama(o.getNama());
//				otorisasiTujuanDto.setKodeNama(o.getKodeKantorJr()+" - "+o.getNama());
//			}
//			BindUtils.postNotifyChange(null, null, this, "listOtorisasiTujuan");
//			BindUtils.postNotifyChange(null, null, this, "otorisasiTujuanDto");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	public void sifatCidera() {
		RestResponse restCidera = callWs(WS_URI_LOV + "/getListSifatCidera",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listSifatCidera = JsonUtil.mapJsonToListObject(
					restCidera.getContents(), DasiJrRefCodeDto.class);
			for (DasiJrRefCodeDto o : listSifatCidera) {
				sifatCideraDto.setRvHighValue(o.getRvHighValue());
				sifatCideraDto.setRvLowValue(o.getRvLowValue());
				sifatCideraDto.setRvMeaning(o.getRvMeaning());
			}
			BindUtils.postNotifyChange(null, null, this, "listSifatCidera");
			BindUtils.postNotifyChange(null, null, this, "sifatCideraDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@NotifyChange({ "cideraKode", "cideraKode2" })
	@Command
	public void rupiahPengajuan() {		
		for(DasiJrRefCodeDto sifatCidera : listSifatCidera){
			if(sifatCideraDto.getRvLowValue().equalsIgnoreCase(sifatCidera.getRvLowValue())){
				sifatCideraDto = new DasiJrRefCodeDto();
				sifatCideraDto = sifatCidera;
			}
		}
		if (sifatCideraDto.getRvLowValue() == "01"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("01")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setTotalMeninggal(getBiaya1().doubleValue());

		} else if (sifatCideraDto.getRvLowValue() == "02"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("02")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setTotalLuka((getBiaya1().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "04"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("04")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setTotalCacat((getBiaya1().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "05"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("05")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setTotalMeninggal((getBiaya1().doubleValue()));
			setTotalLuka((getBiaya2().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "06"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("06")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setTotalLuka((getBiaya1().doubleValue()));
			setTotalCacat((getBiaya2().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "07"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("07")) {
			setCideraKode(sifatCideraDto.getRvHighValue());
			setCideraKode2(null);
			setTotalPenguburan((getBiaya1().doubleValue()));

		} else if (sifatCideraDto.getRvLowValue() == "08"
				|| sifatCideraDto.getRvLowValue().equalsIgnoreCase("08")) {
			String string = getSifatCideraDto().getRvHighValue();
			String[] parts = string.split("-");
			String part1 = parts[0];
			String part2 = parts[1];
			setCideraKode(part1);
			setCideraKode2(part2);
			setTotalLuka((getBiaya1().doubleValue()));
			setTotalPenguburan((getBiaya2().doubleValue()));

		}
		BindUtils.postNotifyChange(null, null, this, "totalMeninggal");
		BindUtils.postNotifyChange(null, null, this, "totalLuka");
		BindUtils.postNotifyChange(null, null, this, "totalCacat");
		BindUtils.postNotifyChange(null, null, this, "totalPenguburan");

	}
	
	@Command("save")
	public void save(){
		RestResponse restResponse = null;
		if(getPageInfo().isEditMode()){
			plPengajuanSantunanDto.setKesimpulanSementara(kesimpulanSementaraDto.getRvLowValue());
			plPengajuanSantunanDto.setOtorisasiFlag(otorisasiFlagDto.getRvLowValue());
			plPengajuanSantunanDto.setJaminanPembayaran(kodeJenisPembayaranDto.getRvLowValue());
			plPengajuanSantunanDto.setTglPengajuan(plPengajuanSantunanDto.getTglPengajuan());
			plPengajuanSantunanDto.setDilimpahkanKe(otorisasiTujuanDto.getKodeKantorJr());
			plPengajuanSantunanDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
			
			restResponse = callWs(WS_URI_SANTUNAN + "/saveSantunanOtorisasi",plPengajuanSantunanDto, HttpMethod.POST);
			if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(restResponse.getMessage());
				// update BPM ke proses selanjutnya
				String pengajuan = "";
				
				if(!(kodeJenisPembayaranDto.getRvMeaning().equalsIgnoreCase("normal"))){
					pengajuan = "EXGRATIA";
				}
				 try {
				 System.out.println("ID Kecelakaan : " + plPengajuanSantunanDto.getIdKecelakaan());
				 UserTask ut = (UserTask) Executions.getCurrent().getAttribute("obj2");
				 //String taskInstanceId = ut.getOtherInfo2().substring(5);
				 String instanceId = "";
				 if(userTask != null){
					 instanceId = userTask.getOtherInfo2().substring(5);
					 UserTask uTask = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), "Entry Otorisasi Pengajuan Berkas", instanceId);
					 String pattern = "dd-MM-yyyy HH:mm:ss";
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

						String dateFormat = simpleDateFormat.format(new Date());
					 if(pengajuan.equalsIgnoreCase("EXGRATIA")){
						 //uTask.setKodeCabang(otorisasiTujuanDto.getKodeKantorJr());
						 
						 uTask.setNomorPermohonan(userTask.getNomorPermohonan());
						 uTask.setPengajuanType(pengajuan);
						 uTask.setKodeCabang(userTask.getKodeCabang());
						 uTask.setCreationDate(userTask.getLastUpdateDate());
						 uTask.setCreatedBy(userSession.getUserName());
						 uTask.setLastUpdatedBy(userSession.getUserName());
						 uTask.setLastUpdateDate(dateFormat);
						 uTask.setOtherInfo1("Entry Otorisasi Pengajuan Berkas");
						 uTask.setIdGUID(userTask.getIdGUID());
						 uTask.setKodeKantor(userTask.getKodeKantor());
						 uTask.setOtherInfo2(userTask.getOtherInfo2());
						 uTask.setOtherInfo3(userTask.getOtherInfo3());
						 uTask.setOtherInfo4(userTask.getOtherInfo4());
						 uTask.setOtherInfo5(userTask.getOtherInfo5());
						 uTask.setTaskNumber(userTask.getTaskNumber());
						 if(otorisasiTujuanDto.getKodeKantorJr()!=null){
							 uTask.setOtherInfo1("Entry Otorisasi Pengajuan Berkas");
							 uTask.setOtherInfo5("PELIMPAHAN");
							 uTask.setKodeCabang(otorisasiTujuanDto.getKodeKantorJr());
						 }
						 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),userSession.getUserLdap(), "APPROVE", uTask, "");
					 } else {
						 uTask.setNomorPermohonan(userTask.getNomorPermohonan());
					 	 uTask.setPengajuanType("NORMAL");
					 	 uTask.setKodeCabang(userTask.getKodeCabang());
					 	 uTask.setCreationDate(userTask.getLastUpdateDate());
						 uTask.setCreatedBy(userSession.getUserName());
					 	 uTask.setLastUpdatedBy(userSession.getUserName());
						 uTask.setLastUpdateDate(dateFormat);
						 uTask.setOtherInfo1("Entry Otorisasi Pengajuan Berkas");
						 uTask.setIdGUID(userTask.getIdGUID());
						 uTask.setKodeKantor(userTask.getKodeKantor());
						 uTask.setOtherInfo2(userTask.getOtherInfo2());
						 uTask.setOtherInfo3(userTask.getOtherInfo3());
						 uTask.setOtherInfo4(userTask.getOtherInfo4());
						 uTask.setOtherInfo5(userTask.getOtherInfo5());
						 uTask.setTaskNumber(userTask.getTaskNumber());
						 if(otorisasiTujuanDto.getKodeKantorJr()!=null){
							 uTask.setOtherInfo1("Entry Otorisasi Pengajuan Berkas");
							 uTask.setOtherInfo5("PELIMPAHAN");
							 uTask.setKodeCabang(otorisasiTujuanDto.getKodeKantorJr());
						 }
					 	 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),userSession.getUserLdap(), "APPROVE", uTask, "");
					 }
				 }else{
					 instanceId = UserTaskService.getInstanceId("Identifikasi Kelengkapan dan Keabsahan Berkas", plPengajuanSantunanDto.getNoBerkas());
					 UserTask uTask = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), "Entry Otorisasi Pengajuan Berkas", instanceId);
					 String pattern = "dd-MM-yyyy HH:mm:ss";
						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

						String dateFormat = simpleDateFormat.format(new Date());
					 if(pengajuan.equalsIgnoreCase("EXGRATIA")){
						 //uTask.setKodeCabang(otorisasiTujuanDto.getKodeKantorJr());
						 
						 uTask.setNomorPermohonan(uTask.getNomorPermohonan());
						 uTask.setPengajuanType(pengajuan);
						 uTask.setKodeCabang(uTask.getKodeCabang());
						 uTask.setCreationDate(uTask.getLastUpdateDate());
						 uTask.setCreatedBy(userSession.getUserName());
						 uTask.setLastUpdatedBy(userSession.getUserName());
						 uTask.setLastUpdateDate(dateFormat);
						 uTask.setOtherInfo1("Entry Otorisasi Pengajuan Berkas");
						 uTask.setIdGUID(uTask.getIdGUID());
						 uTask.setKodeKantor(uTask.getKodeKantor());
						 uTask.setOtherInfo2(uTask.getOtherInfo2());
						 uTask.setOtherInfo3(uTask.getOtherInfo3());
						 uTask.setOtherInfo4(uTask.getOtherInfo4());
						 uTask.setOtherInfo5(uTask.getOtherInfo5());
						 uTask.setTaskNumber(uTask.getTaskNumber());
						 if(otorisasiTujuanDto.getKodeKantorJr()!=null){
							 uTask.setOtherInfo1("Entry Data Pengajuan");
							 uTask.setOtherInfo5("PELIMPAHAN");
							 uTask.setKodeCabang(otorisasiTujuanDto.getKodeKantorJr());
						 }
						 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),userSession.getUserLdap(), "APPROVE", uTask, "");
					 } else {
						 uTask.setNomorPermohonan(userTask.getNomorPermohonan());
					 	 uTask.setPengajuanType("NORMAL");
					 	 uTask.setKodeCabang(userTask.getKodeCabang());
					 	 uTask.setCreationDate(userTask.getLastUpdateDate());
						 uTask.setCreatedBy(userSession.getUserName());
					 	 uTask.setLastUpdatedBy(userSession.getUserName());
						 uTask.setLastUpdateDate(dateFormat);
						 uTask.setOtherInfo1("Entry Otorisasi Pengajuan Berkas");
						 uTask.setIdGUID(userTask.getIdGUID());
						 uTask.setKodeKantor(userTask.getKodeKantor());
						 uTask.setOtherInfo2(userTask.getOtherInfo2());
						 uTask.setOtherInfo3(userTask.getOtherInfo3());
						 uTask.setOtherInfo4(userTask.getOtherInfo4());
						 uTask.setOtherInfo5(userTask.getOtherInfo5());
						 uTask.setTaskNumber(userTask.getTaskNumber());
						 if(otorisasiTujuanDto.getKodeKantorJr()!=null){
							 uTask.setOtherInfo1("Entry Data Pengajuan");
							 uTask.setOtherInfo5("PELIMPAHAN");
							 uTask.setKodeCabang(otorisasiTujuanDto.getKodeKantorJr());
						 }
					 	 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),userSession.getUserLdap(), "APPROVE", uTask, "");
					 }
				 }
//				 UserTask uTask = UserTaskService.getTaskByInstanceId(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), "Entry Otorisasi Pengajuan Berkas", instanceId);
//				 String pattern = "dd-MM-yyyy HH:mm:ss";
//					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//
//					String dateFormat = simpleDateFormat.format(new Date());
//				 if(pengajuan.equalsIgnoreCase("EXGRATIA")){
//					 //uTask.setKodeCabang(otorisasiTujuanDto.getKodeKantorJr());
//					 
//					 uTask.setNomorPermohonan(userTask.getNomorPermohonan());
//					 uTask.setPengajuanType(pengajuan);
//					 uTask.setKodeCabang(userTask.getKodeCabang());
//					 uTask.setCreationDate(userTask.getLastUpdateDate());
//					 uTask.setCreatedBy(userSession.getUserName());
//					 uTask.setLastUpdatedBy(userSession.getUserName());
//					 uTask.setLastUpdateDate(dateFormat);
//					 uTask.setOtherInfo1("Entry Otorisasi Pengajuan Berkas");
//					 uTask.setIdGUID(userTask.getIdGUID());
//					 uTask.setKodeKantor(userTask.getKodeKantor());
//					 uTask.setOtherInfo2(userTask.getOtherInfo2());
//					 uTask.setOtherInfo3(userTask.getOtherInfo3());
//					 uTask.setOtherInfo4(userTask.getOtherInfo4());
//					 uTask.setOtherInfo5(userTask.getOtherInfo5());
//					 uTask.setTaskNumber(userTask.getTaskNumber());
//					 if(otorisasiTujuanDto.getKodeKantorJr()!=null){
//						 uTask.setOtherInfo1("Entry Data Pengajuan");
//						 uTask.setOtherInfo5("PELIMPAHAN");
//						 uTask.setKodeCabang(otorisasiTujuanDto.getKodeKantorJr());
//					 }
//					 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),userSession.getUserLdap(), "APPROVE", uTask, "");
//				 } else {
//					 uTask.setNomorPermohonan(userTask.getNomorPermohonan());
//				 	 uTask.setPengajuanType("NORMAL");
//				 	 uTask.setKodeCabang(userTask.getKodeCabang());
//				 	 uTask.setCreationDate(userTask.getLastUpdateDate());
//					 uTask.setCreatedBy(userSession.getUserName());
//				 	 uTask.setLastUpdatedBy(userSession.getUserName());
//					 uTask.setLastUpdateDate(dateFormat);
//					 uTask.setOtherInfo1("Entry Otorisasi Pengajuan Berkas");
//					 uTask.setIdGUID(userTask.getIdGUID());
//					 uTask.setKodeKantor(userTask.getKodeKantor());
//					 uTask.setOtherInfo2(userTask.getOtherInfo2());
//					 uTask.setOtherInfo3(userTask.getOtherInfo3());
//					 uTask.setOtherInfo4(userTask.getOtherInfo4());
//					 uTask.setOtherInfo5(userTask.getOtherInfo5());
//					 uTask.setTaskNumber(userTask.getTaskNumber());
//					 if(otorisasiTujuanDto.getKodeKantorJr()!=null){
//						 uTask.setOtherInfo1("Entry Data Pengajuan");
//						 uTask.setOtherInfo5("PELIMPAHAN");
//						 uTask.setKodeCabang(otorisasiTujuanDto.getKodeKantorJr());
//					 }
//				 	 UserTaskService.updateTask(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"),userSession.getUserLdap(), "APPROVE", uTask, "");
//				 }
//				 UserTaskService.updateUserTask("adminwls", "welcome1",
//				 plPengajuanSantunanDto.getNoBerkas(), "Entry Otorisasi Pengajuan Berkas","APPROVE",
//				 "", "", "", "",plPengajuanSantunanDto.getIdKecelakaan());
				 
				 } catch (RemoteException e) {
				 e.printStackTrace();
				 }

			} else {
				showErrorMsgBox(restResponse.getMessage());
			}

		}		
	}
	
	@Command
	public void prosesSelanjutnya(){
		try{
//			========================================GET DATA SANTUNAN====================================================
			HashMap<String, Object> map = new HashMap<>();
			map.put("noBerkas", plPengajuanSantunanDto.getNoBerkas());		
			RestResponse rest = callWs(WS_URI_SANTUNAN+"/findByNoBerkas", map, HttpMethod.POST);
			try {
				listPlPengajuanSantunanDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlPengajuanSantunanDto.class);
				for(PlPengajuanSantunanDto a : listPlPengajuanSantunanDto){
					plPengajuanSantunanDto.setTglPengajuan(a.getTglPengajuan());
				}
				if(plPengajuanSantunanDto.getTglPengajuan()==null){
					showInfoMsgBox("Data belum disimpan!", "");
				}else{
					RestResponse restResponse = null;
					plPengajuanSantunanDto.setStatusProses(otorisasiFlagDto.getRvHighValue());
					plPengajuanSantunanDto.setNoBerkas(plPengajuanSantunanDto.getNoBerkas());
					restResponse = callWs(WS_URI_SANTUNAN + "/saveSantunanProsesSelanjutnya",plPengajuanSantunanDto, HttpMethod.POST);
					if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
						showInfoMsgBox("Data berhasil dikirim ke Proses Selanjutnya", null);
						back();
					} else {
						showErrorMsgBox(restResponse.getMessage());
					}							
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	public void checkOtorisasi(){
		if(otorisasiFlagDto.getRvHighValue().equalsIgnoreCase("OL")){
			setDisableDilimpahkan(false);
			setDisableJenisPembayaran(false);
			setDisableKesimpulan(false);
			getKantorDilimpahkan();
		}else if(otorisasiFlagDto.getRvHighValue().equalsIgnoreCase("OR")){
			listKesimpulanSementara();
			listKodeJenisPembayaran();
			kesimpulanSementaraDto.setRvLowValue("1");
			kodeJenisPembayaranDto.setRvLowValue("1");			
			for(DasiJrRefCodeDto kesimpulan : listKesimpulanSementara){
				if(kesimpulanSementaraDto.getRvLowValue().equalsIgnoreCase(kesimpulan.getRvLowValue())){
					kesimpulanSementaraDto = new DasiJrRefCodeDto();
					kesimpulanSementaraDto = kesimpulan;
				}
			}
			for(DasiJrRefCodeDto jenisPembayaran : listKodeJenisPembayaran){
				if(kodeJenisPembayaranDto.getRvLowValue().equalsIgnoreCase(jenisPembayaran.getRvLowValue())){
					kodeJenisPembayaranDto = new DasiJrRefCodeDto();
					kodeJenisPembayaranDto = jenisPembayaran;
				}
			}
			otorisasiTujuanDto.setKodeNama("");
			setDisableDilimpahkan(true);
			for(FndKantorJasaraharjaDto dilimpahkan : listOtorisasiTujuan){
				if(otorisasiTujuanDto.getKodeNama().equalsIgnoreCase(dilimpahkan.getKodeNama())){
					otorisasiTujuanDto = new FndKantorJasaraharjaDto();
					otorisasiTujuanDto = dilimpahkan;
				}
			}		
			setDisableJenisPembayaran(true);
			setDisableKesimpulan(true);
		}else{
			setDisableJenisPembayaran(false);
			setDisableKesimpulan(false);
			otorisasiTujuanDto.setKodeNama("");
			setDisableDilimpahkan(true);
			for(FndKantorJasaraharjaDto dilimpahkan : listOtorisasiTujuan){
				if(otorisasiTujuanDto.getKodeNama().equalsIgnoreCase(dilimpahkan.getKodeNama())){
					otorisasiTujuanDto = new FndKantorJasaraharjaDto();
					otorisasiTujuanDto = dilimpahkan;
				}
			}		
		}
		BindUtils.postNotifyChange(null, null, this, "otorisasiTujuanDto");
		BindUtils.postNotifyChange(null, null, this, "listOtorisasiTujuan");
		BindUtils.postNotifyChange(null, null, this, "disableDilimpahkan");
		BindUtils.postNotifyChange(null, null, this, "disableKesimpulan");
		BindUtils.postNotifyChange(null, null, this, "disableJenisPembayaran");
		BindUtils.postNotifyChange(null, null, this, "kesimpulanSementaraDto");
		BindUtils.postNotifyChange(null, null, this, "kodeJenisPembayaranDto");

	}
	
	@Command("searchKantorDilimpahkan")
	public void findAllKantorJr(){
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchKantor);
		RestResponse restStatusProses = callWs(WS_URI_LOV+ "/findAllKantor", map,HttpMethod.POST);
		listOtorisasiTujuan = new ArrayList<>();
		List<FndKantorJasaraharjaDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(restStatusProses.getContents(), FndKantorJasaraharjaDto.class);
			for(FndKantorJasaraharjaDto a : listOtorisasiTujuan){
				otorisasiTujuanDto.setKodeKantorJr(a.getKodeKantorJr());
				otorisasiTujuanDto.setNama(a.getNama());
				otorisasiTujuanDto.setKodeNama(a.getKodeKantorJr()+" - "+a.getNama());
			}
			FndKantorJasaraharjaDto kosong = new FndKantorJasaraharjaDto();
			kosong.setKodeNama("");
			kosong.setKodeKantorJr(null);
			listOtorisasiTujuan.add(kosong);
			listOtorisasiTujuan.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "otorisasiTujuan");
			BindUtils.postNotifyChange(null, null, this, "listOtorisasiTujuan");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public void getKantorDilimpahkan(){
		HashMap<String, Object> map3 = new HashMap<>();
		map3.put("idKorbanKecelakaan", plPengajuanSantunanDto.getIdKorbanKecelakaan());		
		RestResponse rest3 = callWs(WS_URI_LOV+"/getKantorDilimpahkan", map3, HttpMethod.POST);
		try {
			listPlPengajuanSantunanDto = JsonUtil.mapJsonToListObject(rest3.getContents(), PlPengajuanSantunanDto.class);
			for(PlPengajuanSantunanDto a : listPlPengajuanSantunanDto){
				otorisasiTujuanDto.setKodeKantorJr(a.getKodeKantorDilimpahkan());
				otorisasiTujuanDto.setKodeNama(a.getKodeKantorDilimpahkan()+" - "+a.getNamaKantorDilimpahkan());
				otorisasiTujuanDto.setNama(a.getNamaKantorDilimpahkan());
			}
			BindUtils.postNotifyChange(null, null, this, "otorisasiTujuanDto");	
			BindUtils.postNotifyChange(null, null, this, "listOtorisasiTujuan");	
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public void listStatusProses() {

		RestResponse restStatusProses = callWs(WS_URI_LOV
				+ "/getListStatusProses", new HashMap<String, Object>(),
				HttpMethod.POST);
		listStatusProses = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(
					restStatusProses.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listStatusProses.add(kosong);
			listStatusProses.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listStatusProses");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listKesimpulanSementara(){
		RestResponse rest = callWs(WS_URI_LOV+"/getListKesimpulanSementara", new HashMap<>(), HttpMethod.POST);
		listKesimpulanSementara = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listKesimpulanSementara.add(kosong);
			listKesimpulanSementara.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listKesimpulanSementara");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listOtorisasiFlag(){
		RestResponse rest = callWs(WS_URI_LOV+"/getListOtorisasiFlag", new HashMap<>(), HttpMethod.POST);
		listOtorisasiFlag = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("-");
			kosong.setRvLowValue(null);
			listOtorisasiFlag.add(kosong);
			listOtorisasiFlag.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listOtorisasiFlag");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listKodeJenisPembayaran(){
		RestResponse rest = callWs(WS_URI_LOV+"/getListKodeJenisPembayaran", new HashMap<>(), HttpMethod.POST);
		try {
			listKodeJenisPembayaran = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKodeJenisPembayaran");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command("disposisiBerkas")
	public void showPopup(
			@BindingParam("popup") String popup,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();
		
		args.put("popUpHandler", globalHandleMethodName);
		
		if (!beforePopup(args, popup))
			return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	protected boolean beforePopup(Map<String, Object> args, String popup) {
		args.put("plPengajuanSantunanDto", plPengajuanSantunanDto);
		return true;
	}
	
	@GlobalCommand("iniHandler")
	public void titleHandler(@BindingParam("noBerkas") PlPengajuanSantunanDto selected) {
		if(plPengajuanSantunanDto != null){
			this.plPengajuanSantunanDto = selected;
		}
	}


	@Command("detailRingkasanPengajuan")
	public void showPopupRingkasan(
			@BindingParam("popup") String popup, @BindingParam("item") PlPengajuanSantunanDto plPengajuanSantunanDto,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();
		
		args.put("popUpHandler", globalHandleMethodName);
		args.put("plPengajuanSantunanDto", plPengajuanSantunanDto);
		
//		if (!beforePopup(args, popup))
//			return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	protected boolean beforePopupRingkasan(Map<String, Object> args, String popup) {
		args.put("plPengajuanSantunanDto", plPengajuanSantunanDto);
		return true;
	}
	
	@GlobalCommand("ringkasanHandler")
	public void titleHandlerRingkasan(@BindingParam("noBerkas") PlPengajuanSantunanDto selected) {
		if(plPengajuanSantunanDto != null){		
			this.plPengajuanSantunanDto = selected;
		}
	}

	
	@Command("back")
	public void back(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}
	
	@Command("cetakDisposisi")
	public void cetakDisposisi(@BindingParam("item") PlPengajuanSantunanDto selected)
			throws IOException {
		if (plPengajuanSantunanDto == null
				|| plPengajuanSantunanDto.getNoBerkas() == null) {
			showSmartMsgBox("W001");
			return;
		}		
		CetakLdpb cetak = new CetakLdpb();
		cetak.cetakDisposisi(selected);
	}


	public Date getTglPenerimaan() {
		return tglPenerimaan;
	}


	public void setTglPenerimaan(Date tglPenerimaan) {
		this.tglPenerimaan = tglPenerimaan;
	}


	public String getNoBerkas() {
		return noBerkas;
	}


	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}


	public String getSearchIndex() {
		return searchIndex;
	}


	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}


	public List<PlPengajuanSantunanDto> getListIndex() {
		return listIndex;
	}


	public void setListIndex(List<PlPengajuanSantunanDto> listIndex) {
		this.listIndex = listIndex;
	}


	public List<String> getListPilihanPengajuan() {
		return listPilihanPengajuan;
	}


	public void setListPilihanPengajuan(List<String> listPilihanPengajuan) {
		this.listPilihanPengajuan = listPilihanPengajuan;
	}

	public boolean isListIndexWindow() {
		return listIndexWindow;
	}

	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public PlPengajuanSantunanDto getPlPengajuanSantunanDto() {
		return plPengajuanSantunanDto;
	}

	public void setPlPengajuanSantunanDto(
			PlPengajuanSantunanDto plPengajuanSantunanDto) {
		this.plPengajuanSantunanDto = plPengajuanSantunanDto;
	}

	public List<PlPengajuanSantunanDto> getListPlPengajuanSantunanDto() {
		return listPlPengajuanSantunanDto;
	}

	public void setListPlPengajuanSantunanDto(
			List<PlPengajuanSantunanDto> listPlPengajuanSantunanDto) {
		this.listPlPengajuanSantunanDto = listPlPengajuanSantunanDto;
	}

	public List<DasiJrRefCodeDto> getListKesimpulanSementara() {
		return listKesimpulanSementara;
	}

	public void setListKesimpulanSementara(
			List<DasiJrRefCodeDto> listKesimpulanSementara) {
		this.listKesimpulanSementara = listKesimpulanSementara;
	}

	public DasiJrRefCodeDto getKesimpulanSementaraDto() {
		return kesimpulanSementaraDto;
	}

	public void setKesimpulanSementaraDto(DasiJrRefCodeDto kesimpulanSementaraDto) {
		this.kesimpulanSementaraDto = kesimpulanSementaraDto;
	}

	public List<DasiJrRefCodeDto> getListOtorisasiFlag() {
		return listOtorisasiFlag;
	}

	public void setListOtorisasiFlag(List<DasiJrRefCodeDto> listOtorisasiFlag) {
		this.listOtorisasiFlag = listOtorisasiFlag;
	}

	public DasiJrRefCodeDto getOtorisasiFlagDto() {
		return otorisasiFlagDto;
	}

	public void setOtorisasiFlagDto(DasiJrRefCodeDto otorisasiFlagDto) {
		this.otorisasiFlagDto = otorisasiFlagDto;
	}

	public List<DasiJrRefCodeDto> getListKodeJenisPembayaran() {
		return listKodeJenisPembayaran;
	}

	public void setListKodeJenisPembayaran(
			List<DasiJrRefCodeDto> listKodeJenisPembayaran) {
		this.listKodeJenisPembayaran = listKodeJenisPembayaran;
	}

	public DasiJrRefCodeDto getKodeJenisPembayaranDto() {
		return kodeJenisPembayaranDto;
	}

	public void setKodeJenisPembayaranDto(DasiJrRefCodeDto kodeJenisPembayaranDto) {
		this.kodeJenisPembayaranDto = kodeJenisPembayaranDto;
	}

	public List<DasiJrRefCodeDto> getListStatusProses() {
		return listStatusProses;
	}

	public void setListStatusProses(List<DasiJrRefCodeDto> listStatusProses) {
		this.listStatusProses = listStatusProses;
	}

	public DasiJrRefCodeDto getStatusProsesDto() {
		return statusProsesDto;
	}

	public void setStatusProsesDto(DasiJrRefCodeDto statusProsesDto) {
		this.statusProsesDto = statusProsesDto;
	}

	public String getCideraKode() {
		return cideraKode;
	}

	public void setCideraKode(String cideraKode) {
		this.cideraKode = cideraKode;
	}

	public String getCideraKode2() {
		return cideraKode2;
	}

	public void setCideraKode2(String cideraKode2) {
		this.cideraKode2 = cideraKode2;
	}

	public DasiJrRefCodeDto getSifatCideraDto() {
		return sifatCideraDto;
	}

	public void setSifatCideraDto(DasiJrRefCodeDto sifatCideraDto) {
		this.sifatCideraDto = sifatCideraDto;
	}

	public void setBiaya1(BigDecimal biaya1) {
		this.biaya1 = biaya1;
	}

	public void setBiaya2(BigDecimal biaya2) {
		this.biaya2 = biaya2;
	}

	public BigDecimal getBiaya1() {
		return biaya1;
	}

	public BigDecimal getBiaya2() {
		return biaya2;
	}

	public List<DasiJrRefCodeDto> getListSifatCidera() {
		return listSifatCidera;
	}

	public void setListSifatCidera(List<DasiJrRefCodeDto> listSifatCidera) {
		this.listSifatCidera = listSifatCidera;
	}

	public String getPilihPengajuan() {
		return pilihPengajuan;
	}

	public void setPilihPengajuan(String pilihPengajuan) {
		this.pilihPengajuan = pilihPengajuan;
	}

	public PlPenyelesaianSantunanDto getPlPenyelesaianSantunanDto() {
		return plPenyelesaianSantunanDto;
	}

	public void setPlPenyelesaianSantunanDto(
			PlPenyelesaianSantunanDto plPenyelesaianSantunanDto) {
		this.plPenyelesaianSantunanDto = plPenyelesaianSantunanDto;
	}

	public List<PlPenyelesaianSantunanDto> getListPlPenyelesaianSantunanDto() {
		return listPlPenyelesaianSantunanDto;
	}

	public void setListPlPenyelesaianSantunanDto(
			List<PlPenyelesaianSantunanDto> listPlPenyelesaianSantunanDto) {
		this.listPlPenyelesaianSantunanDto = listPlPenyelesaianSantunanDto;
	}

	public String getSearchKantor() {
		return searchKantor;
	}

	public void setSearchKantor(String searchKantor) {
		this.searchKantor = searchKantor;
	}

	public List<FndKantorJasaraharjaDto> getListOtorisasiTujuan() {
		return listOtorisasiTujuan;
	}

	public void setListOtorisasiTujuan(
			List<FndKantorJasaraharjaDto> listOtorisasiTujuan) {
		this.listOtorisasiTujuan = listOtorisasiTujuan;
	}

	public FndKantorJasaraharjaDto getOtorisasiTujuanDto() {
		return otorisasiTujuanDto;
	}

	public void setOtorisasiTujuanDto(FndKantorJasaraharjaDto otorisasiTujuanDto) {
		this.otorisasiTujuanDto = otorisasiTujuanDto;
	}

	public boolean isDisableKesimpulan() {
		return disableKesimpulan;
	}

	public void setDisableKesimpulan(boolean disableKesimpulan) {
		this.disableKesimpulan = disableKesimpulan;
	}

	public boolean isDisableJenisPembayaran() {
		return disableJenisPembayaran;
	}

	public void setDisableJenisPembayaran(boolean disableJenisPembayaran) {
		this.disableJenisPembayaran = disableJenisPembayaran;
	}

	public boolean isDisableDilimpahkan() {
		return disableDilimpahkan;
	}

	public void setDisableDilimpahkan(boolean disableDilimpahkan) {
		this.disableDilimpahkan = disableDilimpahkan;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getAkmMeninggal() {
		return akmMeninggal;
	}

	public void setAkmMeninggal(double akmMeninggal) {
		this.akmMeninggal = akmMeninggal;
	}

	public double getAkmLuka() {
		return akmLuka;
	}

	public void setAkmLuka(double akmLuka) {
		this.akmLuka = akmLuka;
	}

	public double getAkmCacat() {
		return akmCacat;
	}

	public void setAkmCacat(double akmCacat) {
		this.akmCacat = akmCacat;
	}

	public double getAkmPenguburan() {
		return akmPenguburan;
	}

	public void setAkmPenguburan(double akmPenguburan) {
		this.akmPenguburan = akmPenguburan;
	}

	public double getAkmAmbulance() {
		return akmAmbulance;
	}

	public void setAkmAmbulance(double akmAmbulance) {
		this.akmAmbulance = akmAmbulance;
	}

	public double getAkmP3k() {
		return akmP3k;
	}

	public void setAkmP3k(double akmP3k) {
		this.akmP3k = akmP3k;
	}

	public double getTotalMeninggal() {
		return totalMeninggal;
	}

	public void setTotalMeninggal(double totalMeninggal) {
		this.totalMeninggal = totalMeninggal;
	}

	public double getTotalLuka() {
		return totalLuka;
	}

	public void setTotalLuka(double totalLuka) {
		this.totalLuka = totalLuka;
	}

	public double getTotalCacat() {
		return totalCacat;
	}

	public void setTotalCacat(double totalCacat) {
		this.totalCacat = totalCacat;
	}

	public double getTotalPenguburan() {
		return totalPenguburan;
	}

	public void setTotalPenguburan(double totalPenguburan) {
		this.totalPenguburan = totalPenguburan;
	}

	public double getTotalAmbulance() {
		return totalAmbulance;
	}

	public void setTotalAmbulance(double totalAmbulance) {
		this.totalAmbulance = totalAmbulance;
	}

	public double getTotalP3k() {
		return totalP3k;
	}

	public void setTotalP3k(double totalP3k) {
		this.totalP3k = totalP3k;
	}

	public List<PlPengajuanSantunanDto> getListIndexCopy() {
		return listIndexCopy;
	}

	public void setListIndexCopy(List<PlPengajuanSantunanDto> listIndexCopy) {
		this.listIndexCopy = listIndexCopy;
	}

}
