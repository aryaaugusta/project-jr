package ui;

import java.io.Serializable;
import java.util.Locale;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.Locales;
import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.Executions;

import share.common.LoginData;

import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.util.JsonUtil;

public class LoginVmd extends BaseVmd implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	private MstUserDto mstUserHdrDto = new MstUserDto();
	private LoginData loginData = new LoginData();
	
	private Form formMaster = new SimpleForm();
	private String message;
//	private String 
	
	@Init
	public void loadList(){
		
	}
	
	/*@Command
	public void login(){
		RestResponse rest = callCustomWs("/template/getLogin", mstUserHdrDto, HttpMethod.POST);
		
		try {
			UserSession userSession = JsonUtil.mapJsonToSingleObject(rest.getContents(), UserSession.class);
			
			Locale locale = Locales.getLocale(userSession.getUserLocale());
			getCurrentSession().setAttribute(Attributes.PREFERRED_LOCALE,
					locale);

			getCurrentSession().setAttribute("user",
					userSession);
			Executions.sendRedirect("/index.zul");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			showSmartMsgBox("Login Failed");
		}
	}*/
	
	@Command
	public void login(){
		if(loginData.getLogin() == null || loginData.getPassword() == null || loginData.getLogin().isEmpty() || loginData.getPassword().isEmpty()){
			showSmartMsgBox("Tolong Lengkapi Data");
			return;
		}
		
		RestResponse rest = callCustomWs("/auth/login", loginData, HttpMethod.POST);
		try {
			UserSessionJR userSessionJR = JsonUtil.mapJsonToSingleObject(rest.getContents(), UserSessionJR.class);
			
			Locale locale = Locales.getLocale(userSessionJR.getUserLocale());
			getCurrentSession().setAttribute(Attributes.PREFERRED_LOCALE,
					locale);
			
			
			getCurrentSession().setAttribute("user",
					userSessionJR);
			Executions.sendRedirect("/index.zul");
		} catch (Exception e) {
			e.printStackTrace();
			showSmartMsgBox("Login atau Password salah");
		}
	}


	public Form getFormMaster() {
		return formMaster;
	}

	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}

	public LoginData getLoginData() {
		return loginData;
	}

	public void setLoginData(LoginData loginData) {
		this.loginData = loginData;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}