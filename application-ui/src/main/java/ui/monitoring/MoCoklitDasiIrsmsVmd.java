package ui.monitoring;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import share.DasiJrRefCodeDto;
import share.KorlantasDistrictDto;
import share.KorlantasProvinceDto;
import share.PlDataKecelakaanDto;
import share.PlMappingPoldaDto;
import ui.monitoring.dokumen.DokumenCoklitDasiIRSMS;

import share.PlPengajuanSantunanDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.util.JsonUtil;

@Init(superclass =true)
public class MoCoklitDasiIrsmsVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final PlDataKecelakaanDto PlDataKecelakaanDto = new PlDataKecelakaanDto();
	
	private final String WS_URI = "/MoCoklitDasiIrsms";
	private final String WS_URI_LOV = "/Lov";

	private List<KorlantasProvinceDto> listKorProvinceDto = new ArrayList<>();
	private KorlantasProvinceDto korlantasProvinceDto = new KorlantasProvinceDto();
	private String searchKorProvince;
	private DokumenCoklitDasiIRSMS dokumen = new DokumenCoklitDasiIRSMS();
	
	private List<KorlantasDistrictDto> listKorDistrictDto = new ArrayList<>();
	private KorlantasDistrictDto korlantasDistrictDto = new KorlantasDistrictDto();
	private String searchKorDistrict;
	
	private List<PlMappingPoldaDto> listMappingPoldaDtos = new ArrayList<>();
	private PlMappingPoldaDto plMappingPoldaDto = new PlMappingPoldaDto();
	
	private List<PlDataKecelakaanDto> listIndex = new ArrayList<>();
	private List<PlDataKecelakaanDto> listIndexCopy = new ArrayList<>();
	private PlDataKecelakaanDto plDataKecelakaanDto = new PlDataKecelakaanDto();
	
	private List<DasiJrRefCodeDto> listKeteranganCoklitDto = new ArrayList<>();
	private DasiJrRefCodeDto keteranganCoklitDto = new DasiJrRefCodeDto();
	
	private List<String> listJenisPeriode = new ArrayList<>();
	private String jenisPeriode;
	
	//filter
	private Date startDate;
	private Date endDate;
	
	private boolean listIndexWindow;
	
	private String namaKantorJr;
	private String jabatanJr;
	private String namaPejabatJr;
	
	private String namaKantorPolri;
	private String jabatanPolri;
	private String namaPejabatPolri;
	
	private boolean popupEditData;
	
	private boolean listCariDataEdit;
	
	private boolean listKeteranganVisible;
	
	protected void loadList() {
		listJenisPeriode();
		listPolda();
		listPolres();
	}
	
	@Command("searchPolda")
	public void listPolda(){
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchKorProvince);
		RestResponse rest = callWs(WS_URI+"/getListProvince", map, HttpMethod.POST);
		try {
			listKorProvinceDto = JsonUtil.mapJsonToListObject(rest.getContents(), KorlantasProvinceDto.class);
			for(KorlantasProvinceDto a : listKorProvinceDto){
				korlantasProvinceDto.setProvinceNumber(a.getProvinceNumber());
				korlantasProvinceDto.setName(a.getName());
			}
			BindUtils.postNotifyChange(null, null, this, "listKorProvinceDto");
			BindUtils.postNotifyChange(null, null, this, "korlantasProvinceDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@NotifyChange("korlantasProvinceDto")
	@Command("searchPolres")	
	public void listPolres(){
		if(korlantasProvinceDto.getProvinceNumber()==null){
			korlantasProvinceDto.setProvinceNumber("");
		}
		Map<String, Object> map = new HashMap<>();
//		map.put("search", searchKorProvince);
		map.put("provinceId", korlantasProvinceDto.getProvinceNumber());
		RestResponse rest = callWs(WS_URI+"/getListDistrictByProvince", map, HttpMethod.POST);
		try {
			listKorDistrictDto = JsonUtil.mapJsonToListObject(rest.getContents(), KorlantasDistrictDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKorDistrictDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listJenisPeriode(){
		setJenisPeriode("Tgl Kejadian");
		listJenisPeriode.add("Tgl Kejadian");
		listJenisPeriode.add("Tgl Laporan");
		BindUtils.postNotifyChange(null, null, this, "jenisPeriode");
		BindUtils.postNotifyChange(null, null, this, "listJenisPeriode");
	}
	
	@Command
	public void cari(){
		if(korlantasDistrictDto.getId()!=null){
			getPoldaByDistrict();
		}
		Map<String, Object> map = new HashMap<>();
		map.put("kodeInstansi", plMappingPoldaDto.getKodeInstansi());
		map.put("startDate", dateToString(startDate));
		map.put("endDate", dateToString(endDate));
		map.put("jenisPeriode", "");
		RestResponse rest = callWs(WS_URI+"/all", map, HttpMethod.POST);
		try {
			listIndex = JsonUtil.mapJsonToListObject(rest.getContents(), PlDataKecelakaanDto.class);
			setListIndexWindow(true);
			listIndexCopy = new ArrayList<>();
			listIndexCopy.addAll(listIndex);
			BindUtils.postNotifyChange(null, null, this, "listIndexCopy");
			BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void getPoldaByDistrict(){
		Map<String, Object> map = new HashMap<>();
		map.put("idDistrict", korlantasDistrictDto.getId().toString());
		RestResponse rest = callWs(WS_URI+"/getPoldaByDistrict", map, HttpMethod.POST);
		try {
			listMappingPoldaDtos = JsonUtil.mapJsonToListObject(rest.getContents(), PlMappingPoldaDto.class);
			for(PlMappingPoldaDto a : listMappingPoldaDtos){
				plMappingPoldaDto.setKodeInstansi(a.getKodeInstansi());
			}
			BindUtils.postNotifyChange(null, null, this, "plMappingPoldaDto");
			BindUtils.postNotifyChange(null, null, this, "listMappingPoldaDtos");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void editDataLakaSantunan(@BindingParam("popup") String popup,
			@BindingParam("item") PlDataKecelakaanDto selected) {
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", selected.getIdKecelakaan());
		BindUtils.postGlobalCommand(null, null, "addForSantunan", map);

		try {
			((Window) Executions.createComponents(popup, null, map)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	@Command
	public void editCoklit(@BindingParam("item")PlDataKecelakaanDto selected){
		plDataKecelakaanDto = new PlDataKecelakaanDto();
		plDataKecelakaanDto = selected;
		setPopupEditData(true);
		BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
		BindUtils.postNotifyChange(null, null, this, "popupEditData");
	}
	
	@NotifyChange({"listKeteranganVisible","plDataKecelakaanDto"})
	@Command
	public void showKeteranganCoklit(@BindingParam("item")PlDataKecelakaanDto selected){
		if(selected.isVisibleKetCoklit()==true){
			plDataKecelakaanDto.setVisibleKetCoklit(false);
//			selected.setVisibleKetCoklit(false);
		}else{
			plDataKecelakaanDto.setVisibleKetCoklit(true);
//			selected.setVisibleKetCoklit(true);
			listKeteranganCoklit();			
		}
	}
	
	public void listKeteranganCoklit(){
		RestResponse rest = callWs(WS_URI_LOV+"/getListKetCoklit", new HashMap<>(), HttpMethod.POST);
		try {
			listKeteranganCoklitDto = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKeteranganCoklitDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void tutupEditCoklit(){
		setPopupEditData(false);
		BindUtils.postNotifyChange(null, null, this, "popupEditData");
	}

	@Command
	public void cetakCoklit() throws Exception {
		Map<String, Object> map = new HashMap<>();
		map.put("polda", korlantasProvinceDto);
		map.put("polres", korlantasDistrictDto);
		map.put("startDate", startDate);
		map.put("endDate", endDate);
		map.put("jenisPeriode", jenisPeriode);
		
		map.put("listIndex", listIndex);
		
		map.put("namaKantorJr", namaKantorJr);
		map.put("jabatanJr", jabatanJr);
		map.put("namaPejabatJr", namaPejabatJr);
		
		map.put("namaKantorPolri", namaKantorPolri);
		map.put("jabatanPolri", jabatanPolri);
		map.put("namaPejabatPolri", namaPejabatPolri);
		
		if (listIndex==null||listIndex.size()==0) {
		} else {
			dokumen.createFileExcelCoklit(map);
		}
		
		
	}
	
	
	public List<KorlantasProvinceDto> getListKorProvinceDto() {
		return listKorProvinceDto;
	}

	public void setListKorProvinceDto(List<KorlantasProvinceDto> listKorProvinceDto) {
		this.listKorProvinceDto = listKorProvinceDto;
	}

	public KorlantasProvinceDto getKorlantasProvinceDto() {
		return korlantasProvinceDto;
	}

	public void setKorlantasProvinceDto(KorlantasProvinceDto korlantasProvinceDto) {
		this.korlantasProvinceDto = korlantasProvinceDto;
	}

	public List<KorlantasDistrictDto> getListKorDistrictDto() {
		return listKorDistrictDto;
	}

	public void setListKorDistrictDto(List<KorlantasDistrictDto> listKorDistrictDto) {
		this.listKorDistrictDto = listKorDistrictDto;
	}

	public KorlantasDistrictDto getKorlantasDistrictDto() {
		return korlantasDistrictDto;
	}

	public void setKorlantasDistrictDto(KorlantasDistrictDto korlantasDistrictDto) {
		this.korlantasDistrictDto = korlantasDistrictDto;
	}

	public List<String> getListJenisPeriode() {
		return listJenisPeriode;
	}

	public void setListJenisPeriode(List<String> listJenisPeriode) {
		this.listJenisPeriode = listJenisPeriode;
	}

	public String getJenisPeriode() {
		return jenisPeriode;
	}

	public void setJenisPeriode(String jenisPeriode) {
		this.jenisPeriode = jenisPeriode;
	}

	public String getSearchKorProvince() {
		return searchKorProvince;
	}

	public void setSearchKorProvince(String searchKorProvince) {
		this.searchKorProvince = searchKorProvince;
	}

	public String getSearchKorDistrict() {
		return searchKorDistrict;
	}

	public void setSearchKorDistrict(String searchKorDistrict) {
		this.searchKorDistrict = searchKorDistrict;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isListIndexWindow() {
		return listIndexWindow;
	}

	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}

	public List<PlMappingPoldaDto> getListMappingPoldaDtos() {
		return listMappingPoldaDtos;
	}

	public void setListMappingPoldaDtos(List<PlMappingPoldaDto> listMappingPoldaDtos) {
		this.listMappingPoldaDtos = listMappingPoldaDtos;
	}

	public PlMappingPoldaDto getPlMappingPoldaDto() {
		return plMappingPoldaDto;
	}

	public void setPlMappingPoldaDto(PlMappingPoldaDto plMappingPoldaDto) {
		this.plMappingPoldaDto = plMappingPoldaDto;
	}

	public List<PlDataKecelakaanDto> getListIndex() {
		return listIndex;
	}

	public void setListIndex(List<PlDataKecelakaanDto> listIndex) {
		this.listIndex = listIndex;
	}

	public List<PlDataKecelakaanDto> getListIndexCopy() {
		return listIndexCopy;
	}

	public void setListIndexCopy(List<PlDataKecelakaanDto> listIndexCopy) {
		this.listIndexCopy = listIndexCopy;
	}

	public PlDataKecelakaanDto getPlDataKecelakaanDto() {
		return plDataKecelakaanDto;
	}

	public void setPlDataKecelakaanDto(PlDataKecelakaanDto plDataKecelakaanDto) {
		this.plDataKecelakaanDto = plDataKecelakaanDto;
	}

	public String getNamaKantorJr() {
		return namaKantorJr;
	}

	public void setNamaKantorJr(String namaKantorJr) {
		this.namaKantorJr = namaKantorJr;
	}

	public String getJabatanJr() {
		return jabatanJr;
	}

	public void setJabatanJr(String jabatanJr) {
		this.jabatanJr = jabatanJr;
	}

	public String getNamaPejabatJr() {
		return namaPejabatJr;
	}

	public void setNamaPejabatJr(String namaPejabatJr) {
		this.namaPejabatJr = namaPejabatJr;
	}

	public String getNamaKantorPolri() {
		return namaKantorPolri;
	}

	public void setNamaKantorPolri(String namaKantorPolri) {
		this.namaKantorPolri = namaKantorPolri;
	}

	public String getJabatanPolri() {
		return jabatanPolri;
	}

	public void setJabatanPolri(String jabatanPolri) {
		this.jabatanPolri = jabatanPolri;
	}

	public String getNamaPejabatPolri() {
		return namaPejabatPolri;
	}

	public void setNamaPejabatPolri(String namaPejabatPolri) {
		this.namaPejabatPolri = namaPejabatPolri;
	}

	public boolean isPopupEditData() {
		return popupEditData;
	}

	public void setPopupEditData(boolean popupEditData) {
		this.popupEditData = popupEditData;
	}

	public boolean isListCariDataEdit() {
		return listCariDataEdit;
	}

	public void setListCariDataEdit(boolean listCariDataEdit) {
		this.listCariDataEdit = listCariDataEdit;
	}

	public List<DasiJrRefCodeDto> getListKeteranganCoklitDto() {
		return listKeteranganCoklitDto;
	}

	public void setListKeteranganCoklitDto(
			List<DasiJrRefCodeDto> listKeteranganCoklitDto) {
		this.listKeteranganCoklitDto = listKeteranganCoklitDto;
	}

	public DasiJrRefCodeDto getKeteranganCoklitDto() {
		return keteranganCoklitDto;
	}

	public void setKeteranganCoklitDto(DasiJrRefCodeDto keteranganCoklitDto) {
		this.keteranganCoklitDto = keteranganCoklitDto;
	}

	public boolean isListKeteranganVisible() {
		return listKeteranganVisible;
	}

	public void setListKeteranganVisible(boolean listKeteranganVisible) {
		this.listKeteranganVisible = listKeteranganVisible;
	}
	
	
	
}
