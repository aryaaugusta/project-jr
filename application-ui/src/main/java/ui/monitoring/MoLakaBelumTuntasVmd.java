package ui.monitoring;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.store.BufferedIndexInput;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Sessions;

import share.DasiJrRefCodeDto;
import share.FndKantorJasaraharjaDto;
import share.PlPengajuanSantunanDto;
import common.model.RestResponse;
import common.model.UserSession;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.JsonUtil;

@Init(superclass=true)
public class MoLakaBelumTuntasVmd extends BaseVmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String WS_URI = "/LakaBelumTuntas";
	private final String WS_URI_LOV = "/Lov";
	
	private int pageSize = 5;
	private List<String> listTipePeriode = new ArrayList<>();
	private List<String> listJenisLaporan = new ArrayList<>();
	
	private List<PlPengajuanSantunanDto> listLakaBT= new ArrayList<>();
	private PlPengajuanSantunanDto plPengajuanSantunanDto= new PlPengajuanSantunanDto();
	private List<PlPengajuanSantunanDto> listLakaBTCopy= new ArrayList<>();
	
	private List<DasiJrRefCodeDto> jenisLaporan = new ArrayList<>();
	private DasiJrRefCodeDto jenisLaporanDto = new DasiJrRefCodeDto();
	
	private List<FndKantorJasaraharjaDto> listKantorDto = new ArrayList<>();
	private FndKantorJasaraharjaDto fndKantorJasaraharjaDto = new FndKantorJasaraharjaDto();

	
	private boolean visibleKantor;
	private String pilihTipePeriode;
	private String pilihJenisLaporan;
	private String searchIndex;
	private String searchKantor;
	private Date startDate;
	private Date endDate;
	
	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent()
			.getAttribute(UIConstants.SESS_LOGIN_ID);
	
	protected void loadList() {
		tipePeriode();
		jenisLaporan();
		setVisibleKantor(false);
		
		BindUtils.postNotifyChange(null, null, this, "visibleKantor");
		BindUtils.postNotifyChange(null, null, this, "jenisLaporan");
	}

	
	public void tipePeriode(){
		setPilihTipePeriode("Tanggal Entry");
		listTipePeriode.add("Tanggal Entry");
		listTipePeriode.add("Tanggal Kecelakaan");
		BindUtils.postNotifyChange(null, null, this, "pilihTipePeriode");
		BindUtils.postNotifyChange(null, null, this, "listTipePeriode");
		
	}
	
	public void jenisLaporan(){
		setPilihJenisLaporan("CABANG");
		listJenisLaporan.add("CABANG");
		listJenisLaporan.add("PERWAKILAN");
		listJenisLaporan.add("LOKET");
		BindUtils.postNotifyChange(null, null, this, "pilihJenisLaporan");
		BindUtils.postNotifyChange(null, null, this, "listJenisLaporan");
		
	}
	@NotifyChange("visibleKantor")
	@Command("showKantor")
	public void showKantor(){
		if(pilihJenisLaporan.equalsIgnoreCase("CABANG") || pilihJenisLaporan.equalsIgnoreCase("PERWAKILAN")){
			setVisibleKantor(true);
					
		}else if(pilihJenisLaporan.equalsIgnoreCase("LOKET")){
			setVisibleKantor(false);
			
		}
			
	}
	
	
	@Command("cari")
	public void cari(){
		Map<String, Object> filter = new HashMap<>();
		if (startDate == null
				|| dateToString(startDate).equalsIgnoreCase("")) {
			setStartDate(new Date());
		}
		if (endDate == null
				|| dateToString(endDate).equalsIgnoreCase("")) {
			setEndDate(new Date());
		}
	//	Messagebox.show("test "+fndKantorJasaraharjaDto.getKodeKantorJr());
	//	Messagebox.show("a "+userSession.getKantor());
		
		if(fndKantorJasaraharjaDto.getKodeKantorJr()==null){
		//	Messagebox.show("lona"+fndKantorJasaraharjaDto.getNama());
			if(pilihJenisLaporan.contains("CABANG")){
				filter.put("jenisLaporan", userSession.getKantor().substring(0, 2));
			}else if(pilihJenisLaporan.contains("PERWAKILAN")){
				filter.put("jenisLaporan", userSession.getKantor().substring(0, 4)+"1");
			}else if(pilihJenisLaporan.contains("LOKET")){
				filter.put("jenisLaporan", userSession.getKantor().substring(0, 6)+"1");
			}
		}else if(fndKantorJasaraharjaDto.getKodeKantorJr()!=null){
			if(pilihJenisLaporan.contains("CABANG")){
				filter.put("jenisLaporan", fndKantorJasaraharjaDto.getKodeKantorJr().substring(0, 2));
			}else if(pilihJenisLaporan.contains("PERWAKILAN")){
				filter.put("jenisLaporan", fndKantorJasaraharjaDto.getKodeKantorJr().substring(0, 4)+"1");
			}else if(pilihJenisLaporan.contains("LOKET")){
				filter.put("jenisLaporan", fndKantorJasaraharjaDto.getKodeKantorJr().substring(0, 6)+"1");
			}
		}
		filter.put("startDate", dateToString(startDate));
		filter.put("endDate", dateToString(endDate));
		filter.put("tipePeriode", pilihTipePeriode);
//		filter.put("laporan", pilihJenisLaporan);		
		filter.put("search", searchIndex);
		if (searchIndex == null){
			searchIndex="%%";
		}
		RestResponse rest = callWs(WS_URI + "/all", filter, HttpMethod.POST);
		try {
			listLakaBT = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlPengajuanSantunanDto.class);
			listLakaBTCopy=new ArrayList<>();
			listLakaBTCopy.addAll(listLakaBT);
			for(PlPengajuanSantunanDto a : listLakaBT){
				plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
			}
			BindUtils.postNotifyChange(null, null, this, "listLakaBT");
			BindUtils.postNotifyChange(null, null, this, "listLakaBTCopy");
			BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@NotifyChange("listLakaBTCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listLakaBTCopy));
		if(listLakaBTCopy != null || listLakaBTCopy.size() >0 ){
			listLakaBTCopy.clear();
		}
		if(listLakaBT != null && listLakaBT.size() > 0){
			for(PlPengajuanSantunanDto dto : listLakaBT){
				System.out.println("+++");
				if (dto.getTglKejadian() != null) {
					if (sdf.format(dto.getTglKejadian()).equals(cari)
							|| date.format(dto.getTglKejadian()).equals(cari)
							|| month.format(dto.getTglKejadian())
									.equals(cari)
							|| year.format(dto.getTglKejadian()).equals(cari)
							|| sdf.format(dto.getTglTindakLanjut()).equals(cari)
							|| date.format(dto.getTglTindakLanjut()).equals(cari)
							|| month.format(dto.getTglTindakLanjut()).equals(cari)
							|| year.format(dto.getTglTindakLanjut()).equals(cari)
							|| dto.getCideraDes().toUpperCase()
									.contains(cari)
							|| dto.getDeskripsiInstansi().toUpperCase().contains(cari)
							|| dto.getNamaKorban().toUpperCase().contains(cari)
							|| dto.getTindakLanjutDesc().toUpperCase().contains(cari)
							|| dto.getKasusKecelakaan().toUpperCase()
									.contains(cari)) 
					{
						listLakaBTCopy.add(dto);
					}
				} else {
					if (sdf.format(dto.getTglTindakLanjut()).equals(cari)
							|| date.format(dto.getTglTindakLanjut()).equals(cari)
							|| month.format(dto.getTglTindakLanjut())
									.equals(cari)
							|| year.format(dto.getTglTindakLanjut()).equals(cari)
							|| dto.getCideraDes().toUpperCase()
									.contains(cari)
							|| dto.getDeskripsiInstansi().toUpperCase().contains(cari)
							|| dto.getNamaKorban().toUpperCase().contains(cari)
							|| dto.getTindakLanjutDesc().toUpperCase().contains(cari)
							|| dto.getKasusKecelakaan().toUpperCase()
									.contains(cari))
					{
						listLakaBTCopy.add(dto);
					}
				}
			}
		}
	}
	
	
	public void jenisLaporanDasi() {
		RestResponse restLaporan = callWs(WS_URI_LOV
				+ "/getJenisLaporan", new HashMap<String, Object>(),
				HttpMethod.POST);
		// Messagebox.show("MUNCUL ID");

		try {
			jenisLaporan = JsonUtil.mapJsonToListObject(
					restLaporan.getContents(), DasiJrRefCodeDto.class);
			setTotalSize(restLaporan.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "jenisLaporan");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void searchKantor() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchKantor);

		if (getSearchKantor() == null
				|| getSearchKantor().equalsIgnoreCase("")) {
			setSearchKantor("");
		}
		RestResponse rest = callWs(WS_URI_LOV + "/getAsalBerkas", map,
				HttpMethod.POST);
		try {
			listKantorDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndKantorJasaraharjaDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listKantorDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<String> getListTipePeriode() {
		return listTipePeriode;
	}

	public void setListTipePeriode(List<String> listTipePeriode) {
		this.listTipePeriode = listTipePeriode;
	}

	public List<String> getListJenisLaporan() {
		return listJenisLaporan;
	}

	public void setListJenisLaporan(List<String> listJenisLaporan) {
		this.listJenisLaporan = listJenisLaporan;
	}

	public String getPilihTipePeriode() {
		return pilihTipePeriode;
	}

	public void setPilihTipePeriode(String pilihTipePeriode) {
		this.pilihTipePeriode = pilihTipePeriode;
	}

	public String getPilihJenisLaporan() {
		return pilihJenisLaporan;
	}

	public void setPilihJenisLaporan(String pilihJenisLaporan) {
		this.pilihJenisLaporan = pilihJenisLaporan;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}


	public List<PlPengajuanSantunanDto> getListLakaBT() {
		return listLakaBT;
	}


	public void setListLakaBT(List<PlPengajuanSantunanDto> listLakaBT) {
		this.listLakaBT = listLakaBT;
	}


	public PlPengajuanSantunanDto getPlPengajuanSantunanDto() {
		return plPengajuanSantunanDto;
	}


	public void setPlPengajuanSantunanDto(
			PlPengajuanSantunanDto plPengajuanSantunanDto) {
		this.plPengajuanSantunanDto = plPengajuanSantunanDto;
	}


	public List<DasiJrRefCodeDto> getJenisLaporan() {
		return jenisLaporan;
	}


	public void setJenisLaporan(List<DasiJrRefCodeDto> jenisLaporan) {
		this.jenisLaporan = jenisLaporan;
	}


	public DasiJrRefCodeDto getJenisLaporanDto() {
		return jenisLaporanDto;
	}


	public void setJenisLaporanDto(DasiJrRefCodeDto jenisLaporanDto) {
		this.jenisLaporanDto = jenisLaporanDto;
	}


	public List<FndKantorJasaraharjaDto> getListKantorDto() {
		return listKantorDto;
	}


	public void setListKantorDto(List<FndKantorJasaraharjaDto> listKantorDto) {
		this.listKantorDto = listKantorDto;
	}


	public FndKantorJasaraharjaDto getFndKantorJasaraharjaDto() {
		return fndKantorJasaraharjaDto;
	}


	public void setFndKantorJasaraharjaDto(
			FndKantorJasaraharjaDto fndKantorJasaraharjaDto) {
		this.fndKantorJasaraharjaDto = fndKantorJasaraharjaDto;
	}


	public String getSearchKantor() {
		return searchKantor;
	}


	public void setSearchKantor(String searchKantor) {
		this.searchKantor = searchKantor;
	}


	

	public boolean isVisibleKantor() {
		return visibleKantor;
	}


	public void setVisibleKantor(boolean visibleKantor) {
		this.visibleKantor = visibleKantor;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public List<PlPengajuanSantunanDto> getListLakaBTCopy() {
		return listLakaBTCopy;
	}


	public void setListLakaBTCopy(List<PlPengajuanSantunanDto> listLakaBTCopy) {
		this.listLakaBTCopy = listLakaBTCopy;
	}

}
