package ui.monitoring;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.mapping.Array;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import share.AuthUserDto;
import share.DasiJrRefCodeDto;
import share.FndKantorJasaraharjaDto;
import share.KorlantasAccidentDto;
import share.PlDataKecelakaanDto;
import share.PlInstansiDto;
import share.PlKorbanRsDto;
import share.PlPengajuanSantunanDto;
import share.PlRumahSakitDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.util.JsonUtil;

@Init(superclass=true)
public class MoDataLakaRsVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String WS_URI = "/LakaRs";
	private final String WS_URI_LOV = "/Lov";
	
	private int pageSize = 5;
	private List<String> listSumberData = new ArrayList<>();
	
	private List<PlKorbanRsDto> listLakaRs= new ArrayList<>();
	private PlKorbanRsDto plKorbanRsDto= new PlKorbanRsDto();
	
	private List<PlKorbanRsDto> listLakaRsCopy= new ArrayList<>();
	
	private List<PlRumahSakitDto> listRs = new ArrayList<>();
	private PlRumahSakitDto plRumahSakitDto = new PlRumahSakitDto();
	
	private List<FndKantorJasaraharjaDto> listKantor= new ArrayList<>();
	private FndKantorJasaraharjaDto fndKantorJasaraharjaDto= new FndKantorJasaraharjaDto();
	 
	private List<DasiJrRefCodeDto> listResponse= new ArrayList<>();
	private DasiJrRefCodeDto responseDto =new DasiJrRefCodeDto();
	
	private List<PlInstansiDto> listInstansiDto = new ArrayList<>();
	private PlInstansiDto instansiDto = new PlInstansiDto();
	
	private List<AuthUserDto> listPicRs = new ArrayList<>();
	private AuthUserDto picRsDto = new AuthUserDto();
	
	private List<DasiJrRefCodeDto> addListResponse= new ArrayList<>();
	private DasiJrRefCodeDto addListResponseDto =new DasiJrRefCodeDto();
	
	private List<DasiJrRefCodeDto> listKesimpulanSurvey= new ArrayList<>();
	private DasiJrRefCodeDto kesimpulanSurveyDto =new DasiJrRefCodeDto();
	private boolean showListResponse = false;
	private boolean showFormResponse = false;
	
	private String pilihSumberData;
	private String searchRs;
	private String searchKantor;
	private String searchStatus;
	private Date startDate;
	private Date endDate;
	private String searchIndex;
	private String namaKorban;
	private String searchInstansi;
	private String searchPic;
	
//	@Wire("#popupResponse")
//	private Window winLov;
	
	protected void loadList() {
		sumberData();
		searchStatus();
		responseDto.setRvMeaning("Semua");
		addListResponseDto.setRvMeaning("--Pilih--");
		
		BindUtils.postNotifyChange(null, null, this, "responseDto");
	}
	public void sumberData(){
		
		setPilihSumberData("Semua");
		listSumberData.add("Semua");
		listSumberData.add("Rumah Sakit");
		listSumberData.add("Bpjs Kesehatan");
	
		BindUtils.postNotifyChange(null, null, this, "pilihSumberData");
		BindUtils.postNotifyChange(null, null, this, "listSumberData");
		
	}
	
	@Command("cari")
	public void cari(){
		Map<String, Object> filter = new HashMap<>();
//		if (startDate == null
//				|| dateToString(startDate).equalsIgnoreCase("")) {
//			setStartDate();
//		}
//		if (endDate == null
//				|| dateToString(endDate).equalsIgnoreCase("")) {
//			setEndDate(new Date());
//		}
		if (getNamaKorban() == null || getNamaKorban().equalsIgnoreCase("")) {
			setNamaKorban("%%");
		}
		if(startDate!=null && endDate !=null){
			filter.put("startDate", dateToString(startDate));
			filter.put("endDate", dateToString(endDate));
		} 
		
		
		filter.put("sumberData" ,pilihSumberData);
		filter.put("namaKorban", namaKorban);
		filter.put("namaRs", plRumahSakitDto.getDeskripsi());
		filter.put("kodeKantor", fndKantorJasaraharjaDto.getKodeKantorJr());
		if(responseDto.getRvMeaning().equalsIgnoreCase("Semua")){
			filter.put("statusResponse","");
		}else {
			filter.put("statusResponse",responseDto.getRvMeaning());
		}
		filter.put("search", searchIndex);
		if (searchIndex == null){
			searchIndex="%%";
		}
		
		RestResponse rest = callWs(WS_URI + "/all", filter, HttpMethod.POST);
		try {
			listLakaRs= JsonUtil.mapJsonToListObject(rest.getContents(),
					PlKorbanRsDto.class);
			listLakaRsCopy= new ArrayList<>();
			listLakaRsCopy.addAll(listLakaRs);
			BindUtils.postNotifyChange(null, null, this, "listLakaRs");
			BindUtils.postNotifyChange(null, null, this, "listLakaRsCopy");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@NotifyChange("listLakaRsCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listLakaRsCopy));
		if(listLakaRsCopy != null || listLakaRsCopy.size() >0 ){
			listLakaRsCopy.clear();
		}
		if(listLakaRs != null && listLakaRs.size() > 0){
			for(PlKorbanRsDto dto : listLakaRs){
				System.out.println("+++");
				if (dto.getStatus() != null) {
					if (
							sdf.format(dto.getTglKejadian()).equals(cari)
							|| date.format(dto.getTglKejadian()).equals(cari)
							|| month.format(dto.getTglKejadian())
									.equals(cari)
							|| year.format(dto.getTglKejadian()).equals(cari)
							|| sdf.format(dto.getTglMasukRs()).equals(cari)
							|| date.format(dto.getTglMasukRs()).equals(cari)
							|| month.format(dto.getTglMasukRs()).equals(cari)
							|| year.format(dto.getTglMasukRs()).equals(cari)
							|| sdf.format(dto.getCreatedDate()).equals(cari)
							|| date.format(dto.getCreatedDate()).equals(cari)
							|| month.format(dto.getCreatedDate())
									.equals(cari)
							|| year.format(dto.getCreatedDate()).equals(cari)
							|| dto.getBatasKesimpulan().contains(cari)
							|| dto.getBatasKunjungan().contains(cari)
							|| dto.getNamaRs().toUpperCase().contains(cari)
							|| dto.getStatus().toUpperCase().contains(cari)
							|| dto.getKodeKejadian().toUpperCase().contains(cari)
							|| dto.getNamaKorban().toUpperCase()
									.contains(cari))
							
					{
						listLakaRsCopy.add(dto);
					}
				} else {
					if (
							sdf.format(dto.getTglKejadian()).equals(cari)
							|| date.format(dto.getTglKejadian()).equals(cari)
							|| month.format(dto.getTglKejadian()).equals(cari)
							|| year.format(dto.getTglKejadian()).equals(cari)
							|| sdf.format(dto.getTglMasukRs()).equals(cari)
							|| date.format(dto.getTglMasukRs()).equals(cari)
							|| month.format(dto.getTglMasukRs()).equals(cari)
							|| year.format(dto.getTglMasukRs()).equals(cari)
							|| sdf.format(dto.getCreatedDate()).equals(cari)
							|| date.format(dto.getCreatedDate()).equals(cari)
							|| month.format(dto.getCreatedDate()).equals(cari)
							|| year.format(dto.getCreatedDate()).equals(cari)
							|| dto.getBatasKesimpulan().contains(cari)
							|| dto.getBatasKunjungan().contains(cari)
							|| dto.getNamaRs().toUpperCase().contains(cari)
							|| dto.getKodeKejadian().toUpperCase().contains(cari)
							|| dto.getNamaKorban().toUpperCase().contains(cari))

					{
						listLakaRsCopy.add(dto);
					}
				}
			}
		}
	}
	
	@Command
	public void searchRs() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchRs);
		if (getSearchRs() == null || getSearchRs().equalsIgnoreCase("")) {
			setSearchRs("");
		}

		RestResponse restRs = callWs(WS_URI_LOV + "/findAllRS", map,
				HttpMethod.POST);
		try {

			listRs = JsonUtil.mapJsonToListObject(restRs.getContents(),
					PlRumahSakitDto.class);

			setTotalSize(restRs.getTotalRecords());

			BindUtils.postNotifyChange(null, null, this, "listRs");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void searchKantor() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchKantor);
		if (getSearchKantor() == null || getSearchKantor().equalsIgnoreCase("")) {
			setSearchKantor("");
		}

		RestResponse restKantor = callWs(WS_URI_LOV + "/findAllKantor", map,
				HttpMethod.POST);
		try {

			listKantor = JsonUtil.mapJsonToListObject(restKantor.getContents(),
					FndKantorJasaraharjaDto.class);

			setTotalSize(restKantor.getTotalRecords());

			BindUtils.postNotifyChange(null, null, this, "listKantor");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void searchStatus() {
		Map<String, Object> map = new HashMap<>();

		RestResponse restStatus = callWs(WS_URI_LOV + "/getResponse", map,
				HttpMethod.POST);
		listResponse = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();
		try {

			listBaru = JsonUtil.mapJsonToListObject(restStatus.getContents(),
					DasiJrRefCodeDto.class);

			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("Semua");
			kosong.setRvLowValue(null);
			listResponse.add(kosong);
			listResponse.addAll(listBaru);
			

			BindUtils.postNotifyChange(null, null, this, "listResponse");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	@Command
	public void searchInstansi() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchInstansi);
		if (getSearchInstansi() == null
				|| getSearchInstansi().equalsIgnoreCase("")) {
			setSearchInstansi("");
		}
		RestResponse rest = callWs(WS_URI_LOV + "/getListInstansi", map,
				HttpMethod.POST);
		try {
			listInstansiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlInstansiDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listInstansiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void searchPicRS(){
		Map<String, Object> map= new HashMap<>();
		map.put("search", searchPic);
		if (getSearchPic() == null
				|| getSearchPic().equalsIgnoreCase("")) {
			setSearchPic("");
		}
		
		RestResponse restPic= callWs(WS_URI_LOV +"/getPicLakaRs", map, HttpMethod.POST);
		
		try {
			listPicRs = JsonUtil.mapJsonToListObject(restPic.getContents(), AuthUserDto.class);
			setTotalSize(restPic.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listPicRs");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	@Command
	public void response(@BindingParam("popup") String popup,
			@BindingParam("item") PlKorbanRsDto selected) {
		Executions.getCurrent().setAttribute("obj", selected);
		//getPageInfo().setAddMode(true);

		if (selected.getKodeRumahSakit() == null) {
			showInfoMsgBox("Select Data !", "");
		} else {

			Map<String, Object> args = new HashMap<>();
			Executions.getCurrent().setAttribute("obj", selected);
			getPageInfo().setAddMode(true);

			try {
				((Window) Executions.createComponents(popup, null, args)).doModal();
			} catch (UiException u) {
				u.printStackTrace();
			}
		}
	}
	
	
	public void addListResponse() {
		RestResponse restAddListResponse = callWs(WS_URI_LOV
				+ "/getAddResponseList", new HashMap<String, Object>(),
				HttpMethod.POST);
		// Messagebox.show("MUNCUL ID");
		
		listResponse = new ArrayList<>();
		List<DasiJrRefCodeDto> listBaru = new ArrayList<>();

		try {
			addListResponse = JsonUtil.mapJsonToListObject(
					restAddListResponse.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvMeaning("--Pilih--");
			kosong.setRvLowValue(null);
			listResponse.add(kosong);
			listResponse.addAll(listBaru);
			
			BindUtils.postNotifyChange(null, null, this, "addListResponse");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void kesimpulanSurvey() {
		RestResponse restKesimpulanSurvey = callWs(WS_URI_LOV
				+ "/getListKesimpulanSurvey", new HashMap<String, Object>(),
				HttpMethod.POST);
		// Messagebox.show("MUNCUL ID");

		try {
			listKesimpulanSurvey = JsonUtil.mapJsonToListObject(
					restKesimpulanSurvey.getContents(), DasiJrRefCodeDto.class);
			setTotalSize(restKesimpulanSurvey.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listKesimpulanSurvey");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	public void onAdd(){
		plKorbanRsDto = (PlKorbanRsDto) Executions.getCurrent()
				.getAttribute("obj");	
		Map<String, Object> map = new HashMap<>();
		map.put("kodeRumahSakit", plKorbanRsDto.getKodeRumahSakit());
		map.put("kodeKejadian", plKorbanRsDto.getKodeKejadian());
		responseDto = new DasiJrRefCodeDto();
		responseDto.setRvMeaning("Semua");
		RestResponse restLaka = callWs(WS_URI+"/getLakaRsById", map, HttpMethod.POST);
		try {
			listLakaRs = JsonUtil.mapJsonToListObject(restLaka.getContents(), PlKorbanRsDto.class);
		//	Messagebox.show("test "+listLakaRs.size());
			for(PlKorbanRsDto dto : listLakaRs){
				plKorbanRsDto.setKodeKejadian(dto.getKodeKejadian());
				plKorbanRsDto.setNamaRs(dto.getNamaRs());
				plKorbanRsDto.setTglMasukRs(dto.getTglMasukRs());
				plKorbanRsDto.setRuangan(dto.getRuangan());
				plKorbanRsDto.setTglKejadian(dto.getTglKejadian());
				plKorbanRsDto.setNamaKorban(dto.getNamaKorban());
				plKorbanRsDto.setAlamat(dto.getAlamat());
				plKorbanRsDto.setPic(dto.getPic());
				plKorbanRsDto.setCreatedDate(dto.getCreatedDate());
				plKorbanRsDto.setNamaInstansi(dto.getNamaInstansi());
				plKorbanRsDto.setTglResponse(dto.getTglResponse());
				plKorbanRsDto.setPetugas(dto.getPetugas());
				plKorbanRsDto.setStatus(dto.getStatus());
				//loadList();
			}
			kesimpulanSurvey();
			addListResponse();
			BindUtils.postNotifyChange(null, null, this, "plKorbanRsDto");
			BindUtils.postNotifyChange(null, null, this, "listLakaRs");
			BindUtils.postNotifyChange(null, null, this, "kesimpulanSurveyDto");
			BindUtils.postNotifyChange(null, null, this, "addListResponseDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	
	}
	
	@Command
	public void doubleEntry(@BindingParam("popup") String popup,
			@BindingParam("item") PlKorbanRsDto selected) {
		Executions.getCurrent().setAttribute("obj", selected);
		//getPageInfo().setAddMode(true);

		if (selected.getKodeRumahSakit() == null) {
			showInfoMsgBox("Select Data !", "");
		} else {

			Map<String, Object> args = new HashMap<>();
			Executions.getCurrent().setAttribute("obj", selected);
			getPageInfo().setAddMode(true);

			try {
				((Window) Executions.createComponents(popup, null, args)).doModal();
			} catch (UiException u) {
				u.printStackTrace();
			}
		}

	}
	

	@Command
	public void close(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}

	
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public List<String> getListSumberData() {
		return listSumberData;
	}
	public void setListSumberData(List<String> listSumberData) {
		this.listSumberData = listSumberData;
	}
	public String getPilihSumberData() {
		return pilihSumberData;
	}
	public void setPilihSumberData(String pilihSumberData) {
		this.pilihSumberData = pilihSumberData;
	}
	
	public PlKorbanRsDto getPlKorbanRsDto() {
		return plKorbanRsDto;
	}
	public void setPlKorbanRsDto(PlKorbanRsDto plKorbanRsDto) {
		this.plKorbanRsDto = plKorbanRsDto;
	}
	public List<PlKorbanRsDto> getListLakaRs() {
		return listLakaRs;
	}
	public void setListLakaRs(List<PlKorbanRsDto> listLakaRs) {
		this.listLakaRs = listLakaRs;
	}
	public String getSearchRs() {
		return searchRs;
	}
	public void setSearchRs(String searchRs) {
		this.searchRs = searchRs;
	}
	
	public List<PlRumahSakitDto> getListRs() {
		return listRs;
	}
	public void setListRs(List<PlRumahSakitDto> listRs) {
		this.listRs = listRs;
	}
	public PlRumahSakitDto getPlRumahSakitDto() {
		return plRumahSakitDto;
	}
	public void setPlRumahSakitDto(PlRumahSakitDto plRumahSakitDto) {
		this.plRumahSakitDto = plRumahSakitDto;
	}
	public String getSearchKantor() {
		return searchKantor;
	}
	public void setSearchKantor(String searchKantor) {
		this.searchKantor = searchKantor;
	}
	public List<FndKantorJasaraharjaDto> getListKantor() {
		return listKantor;
	}
	public void setListKantor(List<FndKantorJasaraharjaDto> listKantor) {
		this.listKantor = listKantor;
	}
	public FndKantorJasaraharjaDto getFndKantorJasaraharjaDto() {
		return fndKantorJasaraharjaDto;
	}
	public void setFndKantorJasaraharjaDto(
			FndKantorJasaraharjaDto fndKantorJasaraharjaDto) {
		this.fndKantorJasaraharjaDto = fndKantorJasaraharjaDto;
	}
	public List<DasiJrRefCodeDto> getListResponse() {
		return listResponse;
	}
	public void setListResponse(List<DasiJrRefCodeDto> listResponse) {
		this.listResponse = listResponse;
	}
	public DasiJrRefCodeDto getResponseDto() {
		return responseDto;
	}
	public void setResponseDto(DasiJrRefCodeDto responseDto) {
		this.responseDto = responseDto;
	}
	public String getSearchStatus() {
		return searchStatus;
	}
	public void setSearchStatus(String searchStatus) {
		this.searchStatus = searchStatus;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getSearchIndex() {
		return searchIndex;
	}
	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}
	public String getNamaKorban() {
		return namaKorban;
	}
	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}
	public List<PlKorbanRsDto> getListLakaRsCopy() {
		return listLakaRsCopy;
	}
	public void setListLakaRsCopy(List<PlKorbanRsDto> listLakaRsCopy) {
		this.listLakaRsCopy = listLakaRsCopy;
	}
	public List<PlInstansiDto> getListInstansiDto() {
		return listInstansiDto;
	}
	public void setListInstansiDto(List<PlInstansiDto> listInstansiDto) {
		this.listInstansiDto = listInstansiDto;
	}
	public String getSearchInstansi() {
		return searchInstansi;
	}
	public void setSearchInstansi(String searchInstansi) {
		this.searchInstansi = searchInstansi;
	}
	public PlInstansiDto getInstansiDto() {
		return instansiDto;
	}
	public void setInstansiDto(PlInstansiDto instansiDto) {
		this.instansiDto = instansiDto;
	}
	public List<AuthUserDto> getListPicRs() {
		return listPicRs;
	}
	public void setListPicRs(List<AuthUserDto> listPicRs) {
		this.listPicRs = listPicRs;
	}
	public AuthUserDto getPicRsDto() {
		return picRsDto;
	}
	public void setPicRsDto(AuthUserDto picRsDto) {
		this.picRsDto = picRsDto;
	}
	public String getSearchPic() {
		return searchPic;
	}
	public void setSearchPic(String searchPic) {
		this.searchPic = searchPic;
	}
	public List<DasiJrRefCodeDto> getAddListResponse() {
		return addListResponse;
	}
	public void setAddListResponse(List<DasiJrRefCodeDto> addListResponse) {
		this.addListResponse = addListResponse;
	}
	public DasiJrRefCodeDto getAddListResponseDto() {
		return addListResponseDto;
	}
	public void setAddListResponseDto(DasiJrRefCodeDto addListResponseDto) {
		this.addListResponseDto = addListResponseDto;
	}
	public List<DasiJrRefCodeDto> getListKesimpulanSurvey() {
		return listKesimpulanSurvey;
	}
	public void setListKesimpulanSurvey(List<DasiJrRefCodeDto> listKesimpulanSurvey) {
		this.listKesimpulanSurvey = listKesimpulanSurvey;
	}
	public DasiJrRefCodeDto getKesimpulanSurveyDto() {
		return kesimpulanSurveyDto;
	}
	public void setKesimpulanSurveyDto(DasiJrRefCodeDto kesimpulanSurveyDto) {
		this.kesimpulanSurveyDto = kesimpulanSurveyDto;
	}
	public boolean isShowFormResponse() {
		return showFormResponse;
	}
	public void setShowFormResponse(boolean showFormResponse) {
		this.showFormResponse = showFormResponse;
	}
	public boolean isShowListResponse() {
		return showListResponse;
	}
	public void setShowListResponse(boolean showListResponse) {
		this.showListResponse = showListResponse;
	}
	

}
