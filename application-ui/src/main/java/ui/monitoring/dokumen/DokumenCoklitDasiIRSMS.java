package ui.monitoring.dokumen;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.TextAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;

import share.KorlantasDistrictDto;
import share.KorlantasProvinceDto;
import share.PlDataKecelakaanDto;
import common.ui.DocumentUtil;

public class DokumenCoklitDasiIRSMS extends DocumentUtil implements Serializable{

	
	private static Logger logger = LoggerFactory.getLogger(DokumenCoklitDasiIRSMS.class);
	
	@Init
	public void init(){
		logger.debug("Comencing Print VM");
	}
	
	private static final long serialVersionUID = 1L;
	private CellStyle csAllBorder = null;
	private CellStyle csBold = null;
	private CellStyle csTop = null;
	private CellStyle csVertical = null;
	private CellStyle csHorizontalCenter = null;
	Font bold = null;
	
	
	
//	@GlobalCommand("printCoklitIRSMS")
//	public void cetakCoklitIRSMS(PlDataKecelakaanDto dto)throws Exception{
//		logger.debug("Entering document creating process");
//		SimpleDateFormat waktu = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//		SimpleDateFormat tgl = new SimpleDateFormat("dd/MM/YYYY");
//		DecimalFormat formatUang = new DecimalFormat("###,###,###");
//		// Blank Document
//		XWPFDocument document = new XWPFDocument();
//		CTDocument1 doc = document.getDocument();
//		CTBody body = doc.getBody();
//		if (!body.isSetSectPr()) {
//			body.addNewSectPr();
//		}
//
//		CTPageSz pageSize;
//		CTSectPr section = body.getSectPr();
//
//		if (section.isSetPgSz()) {
//			pageSize = section.getPgSz();
//		} else {
//			pageSize = section.addNewPgSz();
//		}
//		
//		String name = "DokumenCoklitIRSMS.docx";
//		
//		XWPFParagraph par1 = document.createParagraph();
//		par1.setAlignment(ParagraphAlignment.LEFT);
//		XWPFRun run1 = par1.createRun();
//		run1.setText("Laporan Coklit Data Kecelakaan DASIJR - IRSMS");
//		run1.setFontSize(16);
//		run1.setBold(true);
//		
//		XWPFParagraph par2 = document.createParagraph();
//		par2.setAlignment(ParagraphAlignment.RIGHT);
//		XWPFRun run2 = par2.createRun();
//		run2.setText("Tgl Cetak : "+tgl.format(new Date()));//tgl print
//		
//		XWPFTable tabel1 = document.createTable();
//		
//		XWPFTableRow row1 = tabel1.getRow(0);
//		row1.getCell(0).getParagraphArray(0).setAlignment(ParagraphAlignment.RIGHT);
//		XWPFRun runTabel1Row1Cell0 = row1.getCell(0).getParagraphArray(0).createRun();
//		runTabel1Row1Cell0.setText("POLDA");
//		setRunFontText(runTabel1Row1Cell0, 1);
//		setCellWidth(tabel1, 0, 1000);
//		
//		XWPFRun runTabel1Row1Cell1 = row1.addNewTableCell().getParagraphArray(0).createRun();
//		runTabel1Row1Cell1.setText("JABAR"); //polda
//		setCellWidth(tabel1, 1, 4000);
//		
//		XWPFTableRow row2 = tabel1.createRow();
//		row2.getCell(0).getParagraphArray(0).setAlignment(ParagraphAlignment.RIGHT);
//		XWPFRun runTabel1Row2Cell0 = row2.getCell(0).getParagraphArray(0).createRun();
//		runTabel1Row2Cell0.setText("POLRES");
//		setRunFontText(runTabel1Row2Cell0, 1);
//		
//		XWPFRun runTabel1Row2Cell1 = row2.getCell(1).getParagraphArray(0).createRun();
//		runTabel1Row2Cell1.setText("BANDUNG"); //polres
//		
//		XWPFTableRow row3 = tabel1.createRow();
//		row3.getCell(0).getParagraphArray(0).setAlignment(ParagraphAlignment.RIGHT);
//		XWPFRun runTabel1Row3Cell0 = row3.getCell(0).getParagraphArray(0).createRun();
//		runTabel1Row3Cell0.setText("Periode Tgl Kejadian");
//		setRunFontText(runTabel1Row3Cell0, 1);
//		
//		XWPFRun runTabel1Row3Cell1 = row3.getCell(1).getParagraphArray(0).createRun();
//		runTabel1Row3Cell1.setText("01/01/2016 s.d. 27/03/2019"); //periode kecelakaan
//		
//		XWPFParagraph parKosong1 = document.createParagraph();
//		parKosong1.createRun().setText("");
//		
//		XWPFTable tabel2 = document.createTable();
//
//		XWPFTableRow row1Tabel2 = tabel2.getRow(0);
//		
//		row1Tabel2.getCell(0).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row1Cell0 = row1Tabel2.getCell(0).getParagraphArray(0).createRun();
//		runTabel2Row1Cell0.setText("No");
//		setRunFontText(runTabel2Row1Cell0, 1);
//		
//		row1Tabel2.addNewTableCell();
//		row1Tabel2.getCell(1).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row1Cell1 = row1Tabel2.getCell(1).getParagraphArray(0).createRun();
//		runTabel2Row1Cell1.setText("DASI-JR");
//		setRunFontText(runTabel2Row1Cell1, 1);
//		
//		row1Tabel2.addNewTableCell();
//		row1Tabel2.addNewTableCell();
//		row1Tabel2.addNewTableCell();
//		row1Tabel2.addNewTableCell();
//		setHMerge(tabel2, 0, 1, 5);
//
//		row1Tabel2.addNewTableCell();
//		row1Tabel2.getCell(6).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row1Cell6 = row1Tabel2.getCell(6).getParagraphArray(0).createRun();
//		runTabel2Row1Cell6.setText("IRSMS");
//		setRunFontText(runTabel2Row1Cell6, 1);
//		
//		row1Tabel2.addNewTableCell();
//		row1Tabel2.addNewTableCell();
//		row1Tabel2.addNewTableCell();
//		row1Tabel2.addNewTableCell();
//		setHMerge(tabel2, 0, 6, 10);
//		
//		row1Tabel2.addNewTableCell();
//		row1Tabel2.getCell(11).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row1Cell11 = row1Tabel2.getCell(11).getParagraphArray(0).createRun();
//		runTabel2Row1Cell11.setText("Keterangan");
//		setRunFontText(runTabel2Row1Cell11, 1);
//		
//		
//		XWPFTableRow row2Tabel2 = tabel2.createRow();
//		setVMerge(tabel2, 0, 0, 1);
//		setVMerge(tabel2, 11, 0, 1);
//		
//		row2Tabel2.getCell(1).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell1 = row2Tabel2.getCell(1).getParagraphArray(0).createRun();
//		runTabel2Row2Cell1.setText("Tgl Kejadian");
//		setRunFontText(runTabel2Row2Cell1, 1);
//		
//		row2Tabel2.getCell(2).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell2 = row2Tabel2.getCell(2).getParagraphArray(0).createRun();
//		runTabel2Row2Cell2.setText("No Laporan");
//		setRunFontText(runTabel2Row2Cell2, 1);
//		
//		row2Tabel2.getCell(3).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell3 = row2Tabel2.getCell(3).getParagraphArray(0).createRun();
//		runTabel2Row2Cell3.setText("Tgl Laporan");
//		setRunFontText(runTabel2Row2Cell3, 1);
//		
//		row2Tabel2.getCell(4).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell4 = row2Tabel2.getCell(4).getParagraphArray(0).createRun();
//		runTabel2Row2Cell4.setText("Nama Korban");
//		setRunFontText(runTabel2Row2Cell4, 1);
//		
//		row2Tabel2.getCell(5).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell5 = row2Tabel2.getCell(5).getParagraphArray(0).createRun();
//		runTabel2Row2Cell5.setText("Cidera");
//		setRunFontText(runTabel2Row2Cell5, 1);
//		
//		row2Tabel2.getCell(6).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell6 = row2Tabel2.getCell(6).getParagraphArray(0).createRun();
//		runTabel2Row2Cell6.setText("Tgl Kejadian");
//		setRunFontText(runTabel2Row2Cell6, 1);
//		
//		row2Tabel2.getCell(7).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell7 = row2Tabel2.getCell(7).getParagraphArray(0).createRun();
//		runTabel2Row2Cell7.setText("No Laporan");
//		setRunFontText(runTabel2Row2Cell7, 1);
//		
//		row2Tabel2.getCell(8).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell8 = row2Tabel2.getCell(8).getParagraphArray(0).createRun();
//		runTabel2Row2Cell8.setText("Tgl Laporan");
//		setRunFontText(runTabel2Row2Cell8, 1);
//		
//		row2Tabel2.getCell(9).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell9 = row2Tabel2.getCell(9).getParagraphArray(0).createRun();
//		runTabel2Row2Cell9.setText("Nama Korban");
//		setRunFontText(runTabel2Row2Cell9, 1);
//		
//		row2Tabel2.getCell(10).getParagraphArray(0).setAlignment(ParagraphAlignment.CENTER);
//		XWPFRun runTabel2Row2Cell10 = row2Tabel2.getCell(10).getParagraphArray(0).createRun();
//		runTabel2Row2Cell10.setText("Cidera");
//		setRunFontText(runTabel2Row2Cell10, 1);
//		
//		
//		
//		
//		
//		
//		// download dokumen
//		File temp = File.createTempFile("DokumenCoklitIRSMS", ".docx");
//		FileOutputStream outs = new FileOutputStream(temp);
//		document.write(outs);
//		document.close();
//		// out.close();
//		InputStream fis = new FileInputStream(temp);
//		Filedownload.save(new AMedia("DokumenCoklitIRSMS", ".docx", "application/file",fis));
//		temp.delete();
//		
//		
//		
//		// save document
//		// document.write(out);
//		// out.close();
//		System.out.println("document created successfuly");
//	}
	
	private void mergedBorder(Sheet sheet, int ...reg) {
	    int numMerged = sheet.getNumMergedRegions();
	    if(reg.length==0){
	    	for (int i = 0; i < numMerged; i++) {
	    		CellRangeAddress mergedRegions = sheet.getMergedRegion(i);
	    		RegionUtil.setBorderLeft(BorderStyle.THIN, mergedRegions, sheet);
	    		RegionUtil.setBorderRight(BorderStyle.THIN, mergedRegions, sheet);
	    		RegionUtil.setBorderTop(BorderStyle.THIN, mergedRegions, sheet);
	    		RegionUtil.setBorderBottom(BorderStyle.THIN, mergedRegions, sheet);
	    	}
	    }else if(reg.length==1){
	    	CellRangeAddress mergedRegions = sheet.getMergedRegion(reg[0]);
	    	RegionUtil.setBorderLeft(BorderStyle.THIN, mergedRegions, sheet);
	    	RegionUtil.setBorderRight(BorderStyle.THIN, mergedRegions, sheet);
	    	RegionUtil.setBorderTop(BorderStyle.THIN, mergedRegions, sheet);
	    	RegionUtil.setBorderBottom(BorderStyle.THIN, mergedRegions, sheet);
	    }
	}
	
	private CellRangeAddress getRange(int row1, int row2, int col1, int col2){
		return new CellRangeAddress(row1, row2, col1, col2);
	}
	
	private void setCellStyles(Workbook wb) {

		// Bold Font
		bold = wb.createFont();
		bold.setBold(true);
		bold.setFontHeightInPoints((short) 11);

		// AllBorder
		csAllBorder = wb.createCellStyle();
		csAllBorder.setBorderTop(BorderStyle.THIN);
		csAllBorder.setBorderLeft(BorderStyle.THIN);
		csAllBorder.setBorderBottom(BorderStyle.THIN);
		csAllBorder.setBorderRight(BorderStyle.THIN);
		csAllBorder.setFont(bold);
		// Bold style
		csBold = wb.createCellStyle();
		csBold.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		csBold.setFont(bold);
		//Vertical Border Left and Right
		csVertical = wb.createCellStyle();
		csVertical.setBorderLeft(BorderStyle.THIN);
		csVertical.setBorderRight(BorderStyle.THIN);
		//Top Border
		csTop = wb.createCellStyle();
		csTop.setBorderTop(BorderStyle.THIN);
		//Normal
		csHorizontalCenter = wb.createCellStyle();
		csHorizontalCenter.setAlignment(HorizontalAlignment.CENTER);
		

	}
	
	
	@Command("excelCoklit")
	public void createFileExcelCoklit(Map<String, Object> map) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
		Workbook workbook = new XSSFWorkbook();
		setCellStyles(workbook);
		Sheet testExcel = workbook.createSheet("Lembar1");
		int rowIndex = 0;
		CellRangeAddress cellRange1 = new CellRangeAddress(0, 0, 0, 10);
		CellRangeAddress cellRange2 = new CellRangeAddress(1, 1, 0, 2);
		CellRangeAddress cellRange3 = new CellRangeAddress(1, 1, 3, 12);
		CellRangeAddress cellRange4 = new CellRangeAddress(2, 2, 0, 2);
		CellRangeAddress cellRange5 = new CellRangeAddress(2, 2, 3, 12);
		CellRangeAddress cellRange6 = new CellRangeAddress(3, 3, 0, 2);
		CellRangeAddress cellRange7 = new CellRangeAddress(3, 3, 3, 12);
		CellRangeAddress cellRange8 = new CellRangeAddress(5, 6, 0, 0);
		CellRangeAddress cellRange9 = new CellRangeAddress(5, 5, 1, 5);
		CellRangeAddress cellRange10 = new CellRangeAddress(5, 5, 6, 10);
		CellRangeAddress cellRange11 = new CellRangeAddress(5, 6, 11, 12);

		Row row = testExcel.createRow(rowIndex++); //1
		Cell c = null;
		c = row.createCell(0);
		c.setCellValue("Laporan Coklit Data Kecelakaan IRSMS");
		c.setCellStyle(csBold);
		c.setCellStyle(csHorizontalCenter);
		testExcel.addMergedRegion(cellRange1);
		
		c = row.createCell(11);
		c.setCellValue("Tgl Cetak :");
		c = row.createCell(12);
		c.getCellStyle().setAlignment(HorizontalAlignment.RIGHT);
		c.setCellValue(sdf.format(new Date()));

		row = testExcel.createRow(rowIndex++); //2
		c = row.createCell(0);
		c.setCellValue("POLDA");
		c.setCellStyle(csBold);
		c.getCellStyle().setAlignment(HorizontalAlignment.RIGHT);
		testExcel.addMergedRegion(cellRange2);
		
		c = row.createCell(3);
		KorlantasProvinceDto polda = (KorlantasProvinceDto)map.get("polda");
		c.setCellValue(polda.getName());
		testExcel.addMergedRegion(cellRange3);
		
		row = testExcel.createRow(rowIndex++); //3
		c = row.createCell(0);
		c.setCellValue("POLRES");
		c.setCellStyle(csBold);
		c.getCellStyle().setAlignment(HorizontalAlignment.RIGHT);
		testExcel.addMergedRegion(cellRange4);
		
		c = row.createCell(3);
		KorlantasDistrictDto polres = (KorlantasDistrictDto)map.get("polres");
		c.setCellValue(polres.getName());
		testExcel.addMergedRegion(cellRange5);
		
		row = testExcel.createRow(rowIndex++); //4
		c = row.createCell(0);
		String jenisPeriode = (String)map.get("jenisPeriode");
		c.setCellValue("Periode "+jenisPeriode);
		c.setCellStyle(csBold);
		c.getCellStyle().setAlignment(HorizontalAlignment.RIGHT);
		testExcel.addMergedRegion(cellRange6);
		
		c = row.createCell(3);
		Date startDate = null;
		Date endDate = null;
		String starDateStr = "";
		String endDateStr = "";
		if ((Date)map.get("startDate")==null) {
			starDateStr = "";
		} else {
			starDateStr = sdf.format(startDate);
		}
		if ((Date)map.get("endDate")==null) {
			endDateStr = "";
		} else {
			endDateStr = sdf.format(endDate);
		}
		c.setCellValue(starDateStr+" s.d. "+endDateStr);
		testExcel.addMergedRegion(cellRange7);

		row = testExcel.createRow(rowIndex++); //5
		c = row.createCell(0);
		
		row = testExcel.createRow(rowIndex++); //6
		c = row.createCell(0);
		c.setCellValue("No");
		c.setCellStyle(csBold);
		c.setCellStyle(csHorizontalCenter);
		c.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
		testExcel.addMergedRegion(cellRange8);
		System.out.println(0+" : "+rowIndex);
		mergedBorder(row.getSheet(), 7);
		
		c = row.createCell(1);
		c.setCellValue("DASI-JR");
		c.setCellStyle(csHorizontalCenter);
		testExcel.addMergedRegion(cellRange9);
		System.out.println(1+" : "+rowIndex);
		mergedBorder(row.getSheet(), 8);
		
		c = row.createCell(6);
		c.setCellValue("IR-SMS");
		c.setCellStyle(csHorizontalCenter);
		testExcel.addMergedRegion(cellRange10);
		System.out.println(6+" : "+rowIndex);
		mergedBorder(row.getSheet(), 9);
		
		c = row.createCell(11);
		c.setCellValue("KETERANGAN");
		c.setCellStyle(csHorizontalCenter);
		c.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
		testExcel.addMergedRegion(cellRange11);
		System.out.println(11+" : "+rowIndex);
		mergedBorder(row.getSheet(), 10);
		
		int cellIndex = 0;
		
		row = testExcel.createRow(rowIndex++); //7
		c = row.createCell(cellIndex++);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Tgl Kejadian");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("No Laporan");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Tgl Laporan");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Nama Korban");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Cidera");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Tgl Kejadian");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("No Laporan");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Tgl Laporan");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Nama Korban");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Cidera");
		c.setCellStyle(csHorizontalCenter);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellStyle(csAllBorder);
		
		// DATA BELOW 
		// Start Looping Data 
		//========================================================================================================
		@SuppressWarnings("unchecked")
		List<PlDataKecelakaanDto> listIndex = (List<PlDataKecelakaanDto>)map.get("listIndex");
		int number = 1;
		for (PlDataKecelakaanDto dto : listIndex) {
			cellIndex = 0;
			row = testExcel.createRow(rowIndex++); //8
			c = row.createCell(cellIndex++);
			c.setCellValue(number++);
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			c.setCellValue(sdf.format(dto.getTglKejadian()));
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			c.setCellValue(dto.getNoLaporanPolisi());
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			c.setCellValue(sdf.format(dto.getTglLaporanPolisi()));
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			c.setCellValue(dto.getNamaKorban());
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			c.setCellValue(dto.getCideraHighValue());
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			if (dto.getTglKejadianIrsms()==null) {
				c.setCellValue(" ");
			} else {
				c.setCellValue(sdf.format(dto.getTglKejadianIrsms()));
			}
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			c.setCellValue(dto.getNoLaporanPolisiIrsms());	
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			if (dto.getTglLaporanPolisiIrsms()==null) {
				c.setCellValue("-");
			} else {
				c.setCellValue(sdf.format(dto.getTglLaporanPolisiIrsms()));
			}
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			if (dto.getNamaKorbanIrsms().equalsIgnoreCase("")||dto.getNamaKorbanIrsms()==null) {
				c.setCellValue("-");
			} else {
				c.setCellValue(dto.getNamaKorbanIrsms());
			}
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			if (dto.getCideraKorbanIrsms().equalsIgnoreCase("")||dto.getCideraKorbanIrsms()==null) {
				c.setCellValue("-");
			} else {
				c.setCellValue(dto.getCideraKorbanIrsms());
			}
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			c.setCellValue("");
			c.setCellStyle(csVertical);
			c = row.createCell(cellIndex++);
			c.setCellStyle(csVertical);
			testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,cellIndex-2,cellIndex-1)); //12
		}
		
		
		//========================================================================================================
		//end looping

		//Closing Border 
		row = testExcel.createRow(rowIndex++);
		for(int i=0; i<=cellIndex-1; i++){
			c=row.createCell(i);
			c.setCellStyle(csTop);
		}

		row = testExcel.createRow(rowIndex++);
		
		
		row = testExcel.createRow(rowIndex++);
		c=row.createCell(0);
		c.setCellValue("PT. Jasa Raharja (Persero)");
		c.setCellStyle(csHorizontalCenter);
		c.getCellStyle().setFont(bold);
		c=row.createCell(6);
		c.setCellValue("KORLANTAS POLRI");
		c.setCellStyle(csHorizontalCenter);
		c.getCellStyle().setFont(bold);
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,0,5)); //13
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,6,12)); //14
		row = testExcel.createRow(rowIndex++);
		c=row.createCell(0);
		c.setCellValue((String)map.get("namaKantorJr"));
		c.setCellStyle(csHorizontalCenter);
		c=row.createCell(6);
		c.setCellValue((String)map.get("namaKantorPolri"));
		c.setCellStyle(csHorizontalCenter);
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,0,5)); //15
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,6,12)); //16
		row = testExcel.createRow(rowIndex++);
		c=row.createCell(0);
		c.setCellValue((String)map.get("jabatanJr"));
		c.setCellStyle(csHorizontalCenter);
		c=row.createCell(6);
		c.setCellValue((String)map.get("jabatanPolri"));
		c.setCellStyle(csHorizontalCenter);
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,0,5)); //17
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,6,12)); //18
		
		for(int j=0;j<=3;j++){
			row = testExcel.createRow(rowIndex++);
		}
		
		row = testExcel.createRow(rowIndex++);
		c=row.createCell(0);
		c.setCellValue((String)map.get("namaPejabatJr"));
		c.setCellStyle(csHorizontalCenter);
		c=row.createCell(6);
		c.setCellValue((String)map.get("namaPejabatPolri"));
		c.setCellStyle(csHorizontalCenter);
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,0,5)); //19
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,6,12)); //20
		
		
		//Auto size column
		for (int i = 0; i <= cellIndex; i++) {
			testExcel.autoSizeColumn(i);
		}
		try {
			// Write the workbook in file system
			File temp = File.createTempFile("testExcel", ".xlsx");
			FileOutputStream out = new FileOutputStream(temp);
			workbook.write(out);
			out.close();
			InputStream fis = new FileInputStream(temp);
			Filedownload.save(new AMedia("testExcel", "xlsx", "application/file", fis));
			temp.delete();
			
			
		} catch (Exception e) {
			
		}
	}
	
}
