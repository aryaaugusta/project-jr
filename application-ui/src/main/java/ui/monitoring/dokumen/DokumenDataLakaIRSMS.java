package ui.monitoring.dokumen;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;

import common.ui.DocumentUtil;

public class DokumenDataLakaIRSMS extends DocumentUtil{

	private static Logger logger = LoggerFactory.getLogger(DokumenDataLakaIRSMS.class);
	
	@Init
	public void init(){
		logger.debug("Comenching Print VM");
	}
	@SuppressWarnings("unchecked")
	@GlobalCommand("printDokumenDataLakaIRSMS")
	public void cetakDokumenDataLakaIRSMS()throws IOException{
		logger.debug("Entering document creating process");
		SimpleDateFormat waktu = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		SimpleDateFormat tgl = new SimpleDateFormat("dd/MM/YYYY");
		DecimalFormat formatUang = new DecimalFormat("###,###,###");
		
		// Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();
		if (!body.isSetSectPr()) {
			body.addNewSectPr();
		}

		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if (section.isSetPgSz()) {
			pageSize = section.getPgSz();
		} else {
			pageSize = section.addNewPgSz();
		}
		String name = "Dokumen Data Laka IRSMS.docx";
		
		XWPFParagraph par1 = document.createParagraph();
		par1.setAlignment(ParagraphAlignment.LEFT);

		XWPFRun run1 = par1.createRun();
		run1.setText("<<NamaKantorPolisi>>");
		run1.addBreak();
		run1.setText("<<DirektoratKantorPolisi>>");
		run1.addBreak();
		run1.setText("<<WilayahKantorPolisi>>");
		run1.addBreak();
		run1.setText("<<AlamatKantorPolisi>>");
		
		XWPFParagraph par2 = document.createParagraph();
		par2.setAlignment(ParagraphAlignment.CENTER);
		XWPFRun run2 = par2.createRun();
		par2.setBorderBottom(Borders.SINGLE);
		run2.setText("LAPORAN-POLISI");
		setRunFontText(run2, 1);
		
		XWPFParagraph par3 = document.createParagraph();
		par3.setAlignment(ParagraphAlignment.CENTER);
		XWPFRun run3 = par3.createRun();
		run3.setText("Nomor : <<NomorLP>>");
		
		XWPFParagraph par4 = document.createParagraph();
		par4.setAlignment(ParagraphAlignment.LEFT);
		XWPFRun run4 = par4.createRun();
		run4.setText("Pada hari ini <<Hari>>, tanggal <<Tanggal>> pukul <<pukul>>, Saya yang bertanda tangan di bawah ini : ");
		run4.addBreak();
		run4.addTab();
		run4.setText("Nama");
		run4.addTab();
		run4.setText(": <<NamaPetugasPolisi>>");
		run4.addBreak();
		run4.addTab();
		run4.setText("Pangkat");
		run4.addTab();
		run4.setText(": <<PangkatPetugasPolisi>> NRP <<KodeNRP>>");
		run4.addBreak();
		run4.setText("Yang dipekerjakan pada kantor Polisi tersebut di atas, telah menerima berita / pemberitahuan terjadinya kecelakaan lalu lintas dari <<PetugasPolisi2>> Piket Operator <<WilayahKantorPolisi2>>.");
		
		XWPFTable tabelA = document.createTable();
		
		XWPFTableRow row1A = tabelA.getRow(0);
		row1A.getCell(0).setText("1");
		setCellWidth(row1A, 0, 250);
		row1A.addNewTableCell().setText("Hari, Tanggal Kejadian dan jam kejadian");
		setCellWidth(row1A, 1, 3000);
		row1A.addNewTableCell().setText("<<rWaktuKejadian>>");
		setCellWidth(row1A, 2, 5000);
		
		XWPFTableRow row2A = tabelA.createRow();
		row2A.getCell(0).setText("2");
		row2A.getCell(1).setText("Nama Tempat Kejadian Perkara");
		row2A.getCell(2).setText("<<rTempatKejadian>>");
		
		XWPFTableRow row3A = tabelA.createRow();
		row3A.getCell(0).setText("3");
		row3A.getCell(1).setText("Pokok Kejadian");
		row3A.getCell(2).setText("");

		XWPFTableRow row4A = tabelA.createRow();
		row4A.getCell(0).setText("");
		XWPFRun runRow4A1 = row4A.getCell(1).getParagraphArray(0).createRun();
		runRow4A1.setText("- Tabrakan antara kendaraan, slip / lepas kendali");
		runRow4A1.addBreak();
		runRow4A1.setText("- Jenis Kendaraan yang terlibat");
		runRow4A1.addBreak();
		runRow4A1.setText("- Korban, dsb");
		XWPFRun runRow4A2 = row4A.getCell(2).getParagraphArray(0).createRun();
		runRow4A2.setText("- Kendaraan Terlibat : -");
		runRow4A2.addBreak();
		runRow4A2.setText("- Korban : -");

		XWPFTableRow row5A = tabelA.createRow();
		row5A.getCell(0).setText("4");
		row5A.getCell(1).setText("Identitas pengemudi yang tersangkut (Nama, Umur, Jenis Kelamin, Pekerjaan, Alamat dsb)");
		row5A.getCell(2).setText("-	Rahmat Saputro, 40 Tahun, Pria");
		
		XWPFTableRow row6A = tabelA.createRow();
		row6A.getCell(0).setText("5");
		row6A.getCell(1).setText("Keadaan Jasmani dan rohani pengemudi / penumpang");
		row6A.getCell(2).setText("-	Rahmat Saputro : -");
		
		XWPFTableRow row7A = tabelA.createRow();
		row7A.getCell(0).setText("6");
		row7A.getCell(1).setText("Keadaan cuaca, jalan dan sebagainya");
		row7A.getCell(2).setText("<<rKeadaanCuacaJalan>>");
		
		XWPFTableRow row8A = tabelA.createRow();
		row8A.getCell(0).setText("7");
		row8A.getCell(1).setText("Gambar posisi kendaraan di Tempat Kejadian Perkara");
		row8A.getCell(2).setText("<<rGambar>>");
		
		XWPFTableRow row9A = tabelA.createRow();
		row9A.getCell(0).setText("8");
		row9A.getCell(1).setText("Identitas Saksi, nama, umur, jenis kelamin, pekerjaan dsb");
		row9A.getCell(2).setText("-");
		
		XWPFTableRow row10A = tabelA.createRow();
		row10A.getCell(0).setText("9");
		row10A.getCell(1).setText(" Akibat tabrakan korban manusia (nama, umur, jenis kelamin, pekerjaan, dan alamat)");
		row10A.getCell(2).setText("-");
		
		XWPFTableRow row11A = tabelA.createRow();
		row11A.getCell(0).setText("10");
		row11A.getCell(1).setText("Kerusakan benda atau kendaraan");
		row11A.getCell(2).setText("<<rKerusakan>>");
		
		XWPFTableRow row12A = tabelA.createRow();
		row12A.getCell(0).setText("11");
		row12A.getCell(1).setText("Kerugian dinilai dengan uang");
		row12A.getCell(2).setText("<<rKerugian>>");
		
		XWPFTableRow row13A = tabelA.createRow();
		row13A.getCell(0).setText("12");
		row13A.getCell(1).setText("Keterangan singkat tentang terjadinya kecelakaan lalu lintas");
		row13A.getCell(2).setText("<<rKeteranganSingkat>>");
		
		XWPFTableRow row14A = tabelA.createRow();
		row14A.getCell(0).setText("13");
		row14A.getCell(1).setText("Kesimpulan Sementara"); 
		row14A.getCell(2).setText("<<rKesimpulanSementara>>");
		
		XWPFTableRow row15A = tabelA.createRow();
		row15A.getCell(0).setText("14");
		row15A.getCell(1).setText("Barang bukti yang disita");
		row15A.getCell(2).setText("-");
		
		XWPFTableRow row16A = tabelA.createRow();
		row16A.getCell(0).setText("15");
		row16A.getCell(1).setText("Orang yang ditangkap atau ditahan");
		row16A.getCell(2).setText("-");
		
		XWPFParagraph para = document.createParagraph();
		XWPFRun runa = para.createRun();
		runa.setText("");
		
		XWPFTable tabelB = document.createTable();
		setTableBorder(tabelB, 0);

		XWPFTableRow row1B = tabelB.getRow(0);
		XWPFRun runRow1B0 = row1B.getCell(0).getParagraphArray(0).createRun();
		runRow1B0.setText("Dicetak di loket");
		runRow1B0.addBreak();
		runRow1B0.setText("Tanggal & Jam");
		runRow1B0.addBreak();
		runRow1B0.setText("ID User");
		runRow1B0.addBreak();
		runRow1B0.setText("Nama User");
		setCellWidth(row1B, 0, 2500);
		
		XWPFRun runRow1B1 = row1B.addNewTableCell().getParagraphArray(0).createRun();
		runRow1B1.setText(":");
		runRow1B1.addBreak();
		runRow1B1.setText(":");
		runRow1B1.addBreak();
		runRow1B1.setText(":");
		runRow1B1.addBreak();
		runRow1B1.setText(":");
		setCellWidth(row1B, 1, 25);
		
		XWPFRun runRow1B2 = row1B.addNewTableCell().getParagraphArray(0).createRun();
		runRow1B2.setText("<<NamaLoket>>");
		runRow1B2.addBreak();
		runRow1B2.setText("<<TanggalJam>>");
		runRow1B2.addBreak();
		runRow1B2.setText("<<IDUser>>");
		runRow1B2.addBreak();
		runRow1B2.setText("<<NamaUser>>");
		setCellWidth(row1B, 2, 3000);
		
		XWPFParagraph par5 = document.createParagraph();
		XWPFRun run5 = par5.createRun();
		run5.addBreak();
		run5.addBreak();
		run5.addBreak();
		run5.setText("Tanda Tangan	:   ............................... ");
		
		// download dokumen
		File temp = File.createTempFile("Dokumen Data Laka IRSMS", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
		// out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Dokumen Data Laka IRSMS", ".docx", "application/file",fis));
		temp.delete();
		
		
		
		// save document
		// document.write(out);
		// out.close();
		System.out.println("document created successfuly");
	}
	
}
