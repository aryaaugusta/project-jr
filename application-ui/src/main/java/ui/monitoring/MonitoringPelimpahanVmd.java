package ui.monitoring;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.store.BufferedIndexInput;
import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;

import share.FndKantorJasaraharjaDto;
import share.PlPengajuanSantunanDto;
import share.PlRequestPerubahanDto;
import common.model.RestResponse;
import common.model.UserSession;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.JsonUtil;

@Init(superclass=true)
public class MonitoringPelimpahanVmd extends BaseVmd implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private final String MONITORING_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/monitoring/MonitoringPelimpahan.zul";
	
	private final String WS_URI = "/MonitoringPelimpahan";
	private final String WS_URI_LOV = "/Lov";
	
	
	private int pageSize = 5;
	private List<PlPengajuanSantunanDto> listMonitoring=new ArrayList<>();
	private List<PlPengajuanSantunanDto> listMonitoringCopy=new ArrayList<>();
	private PlPengajuanSantunanDto plPengajuanSantunanDto= new PlPengajuanSantunanDto();
	
	private List<FndKantorJasaraharjaDto> listKantorDto = new ArrayList<>();
	private FndKantorJasaraharjaDto fndKantorJasaraharjaDto = new FndKantorJasaraharjaDto();
	
	private boolean listIndexWindow = false;
	
	//filter dan cari
	private String noBerkas;
	private Date tglKejadian;
	private Date tglPenerimaan;
	private Date tglPengajuan;
	private String namaKorban;
	private String diajukanDi;
	private String dilimpahkanKe;
	private String searchIndex;
	private String searchKantorDiajukan;
	private FndKantorJasaraharjaDto diajukanDiDto = new FndKantorJasaraharjaDto();
	private FndKantorJasaraharjaDto dilimpahkanDto = new FndKantorJasaraharjaDto();
	
	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent().getAttribute(
			UIConstants.SESS_LOGIN_ID);

	@Command("cari")
	public void cari() throws ParseException{
		
//		if (getNoBerkas() == null || getNoBerkas().equalsIgnoreCase("")) {
//			setNoBerkas("%%");
//		}
//		if (getNamaKorban() == null || getNamaKorban().equalsIgnoreCase("")) {
//			setNamaKorban("%%");
//		}
//		if (getSearchIndex() == null || getSearchIndex().equalsIgnoreCase("")) {
//			setSearchIndex("%%");
//		}
//		if(diajukanDiDto==null||diajukanDiDto.getKodeKantorJr()==null||diajukanDiDto.getKodeKantorJr().equalsIgnoreCase("")){
//			diajukanDiDto.setKodeKantorJr("%%");
//		}
//		if(dilimpahkanDto==null||dilimpahkanDto.getKodeKantorJr()==null||dilimpahkanDto.getKodeKantorJr().equalsIgnoreCase("")){
//			dilimpahkanDto.setKodeKantorJr("%%");
//		}
//		if (searchIndex == null){
//			searchIndex="%%";
//		}
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String tglKejadianStr = "";
		String tglPenerimaanStr = "";
		noBerkas = noBerkas==null?"":noBerkas;
		namaKorban = namaKorban==null?"":namaKorban;
		tglKejadianStr = tglKejadian==null?"%":sdf.format(tglKejadian);
		tglPenerimaanStr = tglPenerimaan==null?"%":sdf.format(tglPenerimaan);
		diajukanDiDto.setKodeKantorJr(diajukanDiDto.getKodeKantorJr()==null?userSession.getKantor():diajukanDiDto.getKodeKantorJr());
		dilimpahkanDto.setKodeKantorJr(dilimpahkanDto.getKodeKantorJr()==null?userSession.getKantor():dilimpahkanDto.getKodeKantorJr());
		searchIndex = searchIndex==null?"":searchIndex;
		Map<String, Object> filter = new HashMap<>();
		filter.put("noBerkas", noBerkas);
		filter.put("namaKorban", namaKorban);
		filter.put("tglKejadian", tglKejadianStr);
		filter.put("tglPenerimaan", tglPenerimaanStr);
		filter.put("diajukanDi", diajukanDiDto.getKodeKantorJr());			
		filter.put("dilimpahkanKe", dilimpahkanDto.getKodeKantorJr());			
		filter.put("search", searchIndex);
		RestResponse rest = callWs(WS_URI + "/all", filter, HttpMethod.POST);
		try {
			listMonitoring = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlPengajuanSantunanDto.class);
			
			listMonitoringCopy = new ArrayList<>();
			listMonitoringCopy.addAll(listMonitoring);
			setListIndexWindow(true);
			for(PlPengajuanSantunanDto a : listMonitoring){
				plPengajuanSantunanDto.setNoBerkas(a.getNoBerkas());
			}
			BindUtils.postNotifyChange(null, null, this, "listMonitoring");
			BindUtils.postNotifyChange(null, null, this, "listMonitoringCopy");
			BindUtils.postNotifyChange(null, null, this, "listIndexWindow");
			BindUtils.postNotifyChange(null, null, this, "plPengajuanSantunanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@NotifyChange("listMonitoringCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listMonitoringCopy));
		if(listMonitoringCopy != null || listMonitoringCopy.size() >0 ){
			listMonitoringCopy.clear();
		}
		if(listMonitoring != null && listMonitoring.size() > 0){
			for(PlPengajuanSantunanDto dto : listMonitoring){
				System.out.println("+++");
				
				if (dto.getTglPelimpahan() != null) {
					if(dto.getNamaKantorDiajukanDi() != null){
						if(
								dto.getNoBerkas().toUpperCase().contains(cari)||
								dto.getTglTerjadi().equals(cari)||
								
								dto.getTglPelimpahan().equals(cari)||
								dto.getNamaKorban().toUpperCase().contains(cari)||
								dto.getNamaKantorDiajukanDi().toUpperCase().contains(cari)||
								dto.getNamaKantorDilimpahkan().toUpperCase().contains(cari) ||
								dto.getJeda().toString().contains(cari))
						{
							listMonitoringCopy.add(dto);
						} 
					}else{
						if(
								dto.getNoBerkas().toUpperCase().contains(cari)||
								dto.getTglTerjadi().equals(cari)||
								dto.getTglPelimpahan().equals(cari)||
								dto.getNamaKorban().toUpperCase().contains(cari)||
								dto.getNamaKantorDilimpahkan().toUpperCase().contains(cari)||
								dto.getJeda().toString().contains(cari))
						{
							listMonitoringCopy.add(dto);
						} 
					}
			} else {
				if(dto.getNamaKantorDiajukanDi() != null){
					if(
							dto.getNoBerkas().toUpperCase().contains(cari)||
							dto.getTglTerjadi().equals(cari)||
							dto.getTglPelimpahan().equals(cari)||
							dto.getNamaKorban().toUpperCase().contains(cari)||
							dto.getNamaKantorDiajukanDi().toUpperCase().contains(cari)||
							dto.getNamaKantorDilimpahkan().toUpperCase().contains(cari) ||
							dto.getJeda().toString().contains(cari))
					{
						listMonitoringCopy.add(dto);
					} 
				}else{
					if(
							dto.getNoBerkas().toUpperCase().contains(cari)||
							dto.getTglTerjadi().equals(cari)||
							dto.getTglPelimpahan().equals(cari)||
							dto.getNamaKorban().toUpperCase().contains(cari)||
							dto.getNamaKantorDilimpahkan().toUpperCase().contains(cari)||
							dto.getJeda().toString().contains(cari))
					{
						listMonitoringCopy.add(dto);
					} 
				}
			}
		}
	}
}

	
	@Command
	public void searchKantorDiajukan() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchKantorDiajukan);

		if (getSearchKantorDiajukan() == null
				|| getSearchKantorDiajukan().equalsIgnoreCase("")) {
			setSearchKantorDiajukan("");
		}
		RestResponse rest = callWs(WS_URI_LOV + "/getAsalBerkas", map,
				HttpMethod.POST);
		try {
			listKantorDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndKantorJasaraharjaDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listKantorDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	
	

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	
	public PlPengajuanSantunanDto getPlPengajuanSantunanDto() {
		return plPengajuanSantunanDto;
	}

	public void setPlPengajuanSantunanDto(
			PlPengajuanSantunanDto plPengajuanSantunanDto) {
		this.plPengajuanSantunanDto = plPengajuanSantunanDto;
	}


	public List<PlPengajuanSantunanDto> getListMonitoring() {
		return listMonitoring;
	}


	public void setListMonitoring(List<PlPengajuanSantunanDto> listMonitoring) {
		this.listMonitoring = listMonitoring;
	}


	public String getNoBerkas() {
		return noBerkas;
	}


	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}


	public Date getTglKejadian() {
		return tglKejadian;
	}


	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}


	public Date getTglPenerimaan() {
		return tglPenerimaan;
	}


	public void setTglPenerimaan(Date tglPenerimaan) {
		this.tglPenerimaan = tglPenerimaan;
	}


	public Date getTglPengajuan() {
		return tglPengajuan;
	}


	public void setTglPengajuan(Date tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}


	public String getNamaKorban() {
		return namaKorban;
	}

	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}


	public String getDiajukanDi() {
		return diajukanDi;
	}


	public void setDiajukanDi(String diajukanDi) {
		this.diajukanDi = diajukanDi;
	}


	public String getDilimpahkanKe() {
		return dilimpahkanKe;
	}


	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}







	public String getSearchIndex() {
		return searchIndex;
	}







	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	
	public String getSearchKantorDiajukan() {
		return searchKantorDiajukan;
	}

	public void setSearchKantorDiajukan(String searchKantorDiajukan) {
		this.searchKantorDiajukan = searchKantorDiajukan;
	}

	public List<FndKantorJasaraharjaDto> getListKantorDto() {
		return listKantorDto;
	}

	public void setListKantorDto(List<FndKantorJasaraharjaDto> listKantorDto) {
		this.listKantorDto = listKantorDto;
	}

	public FndKantorJasaraharjaDto getFndKantorJasaraharjaDto() {
		return fndKantorJasaraharjaDto;
	}

	public void setFndKantorJasaraharjaDto(
			FndKantorJasaraharjaDto fndKantorJasaraharjaDto) {
		this.fndKantorJasaraharjaDto = fndKantorJasaraharjaDto;
	}

	public FndKantorJasaraharjaDto getDiajukanDiDto() {
		return diajukanDiDto;
	}

	public void setDiajukanDiDto(FndKantorJasaraharjaDto diajukanDiDto) {
		this.diajukanDiDto = diajukanDiDto;
	}

	public FndKantorJasaraharjaDto getDilimpahkanDto() {
		return dilimpahkanDto;
	}

	public void setDilimpahkanDto(FndKantorJasaraharjaDto dilimpahkanDto) {
		this.dilimpahkanDto = dilimpahkanDto;
	}

	public List<PlPengajuanSantunanDto> getListMonitoringCopy() {
		return listMonitoringCopy;
	}

	public void setListMonitoringCopy(
			List<PlPengajuanSantunanDto> listMonitoringCopy) {
		this.listMonitoringCopy = listMonitoringCopy;
	}

	public boolean isListIndexWindow() {
		return listIndexWindow;
	}

	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}
	

}
