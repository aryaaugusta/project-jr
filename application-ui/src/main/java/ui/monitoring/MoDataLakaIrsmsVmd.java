package ui.monitoring;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import share.KorlantasAccidentDto;
import share.PlAngkutanKecelakaanDto;
import share.PlDataKecelakaanDto;
import share.PlInstansiDto;
import share.PlJaminanDto;
import share.PlKorbanKecelakaanDto;
import share.PlMappingPoldaDto;
import share.PlRegisterSementaraDto;
import ui.monitoring.dokumen.DokumenDataLakaIRSMS;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.util.JsonUtil;

@Init(superclass = true)
public class MoDataLakaIrsmsVmd extends BaseVmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String WS_URI = "/MoLakaIrsms";
	private final String WS_URI_LOV = "/Lov";
	private final String WS_URI_REGISTER = "/OperasionalRegisterSementara";
	private final String WS_URI_LAKA = "/OpDataKecelakaan";

	@Wire("#popupMapping")
	private Window winLov;

	// SEARCH & FILTER
	private int pageSize = 5;
	private List<String> listJenisData = new ArrayList<>();
	private String jenisData = new String();

	private List<PlMappingPoldaDto> listMappingPoldaDto = new ArrayList<>();
	private PlMappingPoldaDto plMappingPoldaDto = new PlMappingPoldaDto();

	private List<PlInstansiDto> listInstansiDto = new ArrayList<>();
	private PlInstansiDto plInstansiDto = new PlInstansiDto();
	private String searchInstansi;

	private List<KorlantasAccidentDto> listIndex = new ArrayList<>();
	private List<KorlantasAccidentDto> listIndexCopy = new ArrayList<>();
	private KorlantasAccidentDto korlantasAccidentDto = new KorlantasAccidentDto();
	private DokumenDataLakaIRSMS dokumenDataLakaIRSMS = new DokumenDataLakaIRSMS();
	
	private boolean listIndexWindow = false;

	private String filterNamaKorban;
	private String filterNoLaporan;
	private Date tglKejadianStart;
	private Date tglKejadianEnd;
	private Date tglLaporanStart;
	private Date tglLaporanEnd;

	private String searchIndex;
	
	//detail
	private Date detailKejadianStartDate;
	private Date detailKejadianEndDate;
	private String detailInstansi;
	private Date detailLaporanStartDate;
	private Date detailLaporanEndDate;
	private String detailNoLaporanPolisi;
	private String detailNamaKorban;
	private String detailKodeJaminan;
	private String statusLP;
	private String detailSearch;
	private Date detailTglRegister = new Date();
	private Date timeTL;
	private Date dateTL;
	private String keteranganTL;
	private PlInstansiDto instansiDto = new PlInstansiDto();
	private PlJaminanDto jaminanDto = new PlJaminanDto();

	private List<PlDataKecelakaanDto> listDataLakaDto = new ArrayList<>();
 	private PlDataKecelakaanDto dataLakaDto = new PlDataKecelakaanDto();

 	private List<PlKorbanKecelakaanDto> listKorbanLakaDto = new ArrayList<>();
 	private List<PlKorbanKecelakaanDto> listKorbanLakaMappingDto = new ArrayList<>();
 	private PlKorbanKecelakaanDto korbanLakaDto = new PlKorbanKecelakaanDto();
 	
 	private List<KorlantasAccidentDto> listKorbanIrsmsDto = new ArrayList<>();
 	private KorlantasAccidentDto korbanIrsmsDto = new KorlantasAccidentDto();
 	
 	private List<PlKorbanKecelakaanDto> listKorbanDasiDto = new ArrayList<>();
 	private PlKorbanKecelakaanDto korbanDasiDto = new PlKorbanKecelakaanDto();
 	
 	private List<PlAngkutanKecelakaanDto> listAngkutanLakaDto = new ArrayList<>();
 	private PlAngkutanKecelakaanDto angkutanLakaDto = new PlAngkutanKecelakaanDto();
 	
	private boolean listDetail = false;

	protected void loadList() {
		listJenis();
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, 1);
		setTglKejadianStart(c.getTime());
		setTglKejadianEnd(new Date());
		BindUtils.postNotifyChange(null, null, this, "tglKejadianStart");
		BindUtils.postNotifyChange(null, null, this, "tglKejadianEnd");
	}

	@Command("searchInstansi")
	public void listInstansi() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchInstansi);
		map.put("kodeInstansi", "%");
		RestResponse rest = callWs(WS_URI_LOV + "/getInstansiIrsms", map,
				HttpMethod.POST);
		try {
			listMappingPoldaDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), PlMappingPoldaDto.class);
			BindUtils.postNotifyChange(null, null, this, "listMappingPoldaDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command("search")
	public void searchIndex() {
		Map<String, Object> map = new HashMap<>();
		if (tglKejadianStart != null && tglKejadianEnd != null) {
			map.put("kejadianStartDate", dateToString(tglKejadianStart));
			map.put("kejadianEndDate", dateToString(tglKejadianEnd));
		}
		if (tglLaporanStart != null && tglLaporanEnd != null) {
			map.put("laporanStartDate", dateToString(tglLaporanStart));
			map.put("laporanEndDate", dateToString(tglLaporanEnd));
		}
		if (plMappingPoldaDto.getKodeInstansi() == null
				|| plMappingPoldaDto.getKodeInstansi().equalsIgnoreCase("")) {
			plMappingPoldaDto.setKodeInstansi("");
		} else {
			map.put("instansi", plMappingPoldaDto.getKodeInstansi());
		}
		map.put("namaKorban", filterNamaKorban);
		map.put("noLaporan", filterNoLaporan);
		map.put("jenisData", jenisData);

		RestResponse rest = callCustomWs(WS_URI + "/all", map, HttpMethod.POST);
		try {
			listIndex = JsonUtil.mapJsonToListObject(rest.getContents(),
					KorlantasAccidentDto.class);
			listIndexCopy = new ArrayList<>();
			listIndexCopy.addAll(listIndex);
			setListIndexWindow(true);
			BindUtils.postNotifyChange(null, null, this, "listIndex");
			BindUtils.postNotifyChange(null, null, this, "listIndexCopy");
			BindUtils.postNotifyChange(null, null, this, "listIndexWindow");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command("cariDataLaka")
	public void loadDataLaka(){
		setListDetail(true);

		Map<String,Object> mapInput = new HashMap<>();
		
		try {
			try {
				mapInput.put("kejadianStartDate", dateToString(getDetailKejadianStartDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				mapInput.put("kejadianEndDate", dateToString(getDetailKejadianEndDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				mapInput.put("laporStartDate", dateToString(getDetailLaporanStartDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				mapInput.put("laporEndDate", dateToString(getDetailLaporanEndDate()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		mapInput.put("namaKantorInstansi", getInstansiDto().getDeskripsi());
		mapInput.put("noLaporPolisi", getDetailNoLaporanPolisi());
		mapInput.put("namaKorbanLaka", getDetailNamaKorban());
		mapInput.put("kodeJaminan", getJaminanDto().getKodeJaminan());
		mapInput.put("statusLP", getStatusLP());
		mapInput.put("search", getDetailSearch());
		mapInput.put("idKorban", "");
		mapInput.put("idKecelakaan", "");
		mapInput.put("tipe", "Y");

		RestResponse rest = callWs(WS_URI_REGISTER+"/dataLaka", mapInput, HttpMethod.POST);
		try {
			listDataLakaDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlDataKecelakaanDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listDataLakaDto");
			BindUtils.postNotifyChange(null, null, this, "listDetail");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@NotifyChange("listIndexCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat date = new SimpleDateFormat("dd");
		SimpleDateFormat month = new SimpleDateFormat("MM");
		SimpleDateFormat year = new SimpleDateFormat("yyyy");
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listIndexCopy));
		if (listIndexCopy != null || listIndexCopy.size() > 0) {
			listIndexCopy.clear();
		}
		if (listIndex != null && listIndex.size() > 0) {
			for (KorlantasAccidentDto dto : listIndex) {
				System.out.println("+++");
				if (sdf.format(dto.getTglKejadian()).equals(cari)
						|| date.format(dto.getTglKejadian()).equals(cari)
						|| month.format(dto.getTglKejadian()).equals(cari)
						|| year.format(dto.getTglKejadian()).equals(cari)
						|| dto.getInstansiDesc().toUpperCase().contains(cari)
						|| dto.getNoLaporan().toUpperCase().contains(cari)
						|| dto.getLokasi().toUpperCase().contains(cari)
						|| dto.getKorban().toUpperCase().contains(cari)) {
					listIndexCopy.add(dto);
				}
			}
		}
	}

	@Command
	public void mapping(@BindingParam("popup") String popup,
			@BindingParam("item") KorlantasAccidentDto selected) {

		if (selected.getId() == null) {
			showInfoMsgBox("Select Data !", "");
		} else {

			Map<String, Object> args = new HashMap<>();
			Executions.getCurrent().setAttribute("obj", selected);
			getPageInfo().setAddMode(true);

			try {
				((Window) Executions.createComponents(popup, null, args)).doModal();
			} catch (UiException u) {
				u.printStackTrace();
			}
		}
	}

	public void onAdd(){
		korlantasAccidentDto = (KorlantasAccidentDto) Executions.getCurrent()
				.getAttribute("obj");	
		setListDetail(false);
		BindUtils.postNotifyChange(null, null, this,
				"korlantasAccidentDto");
		BindUtils.postNotifyChange(null, null, this,
				"listDetail");

	}
	@Command
	public void unmapping(@BindingParam("popup") String popup,
			@BindingParam("item") KorlantasAccidentDto selected) {
		Map<String, Object> args = new HashMap<>();

		Executions.getCurrent().setAttribute("obj", selected);
		getPageInfo().setEditMode(true);

		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}

	}
	
	public void onEdit(){
		korlantasAccidentDto = (KorlantasAccidentDto) Executions.getCurrent()
				.getAttribute("obj");	
//		=====================================GET DATA LAKA=============================== 
		Map<String, Object> map = new HashMap<>();
		map.put("idKecelakaan", korlantasAccidentDto.getIdLakaJr());
		RestResponse restLaka = callWs(WS_URI_LAKA+"/dataLakaById", map, HttpMethod.POST);
		try {
			listDataLakaDto = JsonUtil.mapJsonToListObject(restLaka.getContents(), PlDataKecelakaanDto.class);
			for(PlDataKecelakaanDto dto : listDataLakaDto){
				dataLakaDto.setKodeNamaAsalBerkas(dto.getAsalBerkas()+" - "+dto.getAsalBerkasDesc());
				dataLakaDto.setKodeNamaInstansi(dto.getKodeInstansi()+" - "+dto.getInstansiDesc());
				dataLakaDto.setNoLaporanPolisi(dto.getNoLaporanPolisi());
				dataLakaDto.setTglLaporanPolisi(dto.getTglLaporanPolisi());
				dataLakaDto.setNamaPetugas(dto.getNamaPetugas());
				dataLakaDto.setTglKejadian(dto.getTglKejadian());
				dataLakaDto.setDeskripsiLokasi(dto.getDeskripsiLokasi());
				dataLakaDto.setKasusKecelakaanDesc(dto.getKasusKecelakaanDesc());
				dataLakaDto.setSifatKecelakaanDesc(dto.getSifatKecelakaanDesc());
				dataLakaDto.setDeskripsiKecelakaan(dto.getDeskripsiKecelakaan());
			}
			BindUtils.postNotifyChange(null, null, this, "dataLakaDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
//		===================================GET DATA KORBAN DASI-JR================================
		Map<String, Object> map3 = new HashMap<>();
		map3.put("idKecelakaan", korlantasAccidentDto.getIdLakaJr());
		List<PlKorbanKecelakaanDto> listBaru = new ArrayList<>();
		RestResponse restListKorbanDasi = callWs(WS_URI_LAKA+"/korbanLakaById",map3,HttpMethod.POST);
		try {
			listBaru = JsonUtil.mapJsonToListObject(restListKorbanDasi.getContents(), PlKorbanKecelakaanDto.class);
			PlKorbanKecelakaanDto kosong = new PlKorbanKecelakaanDto();
			kosong.setNamaCidera("-");
			kosong.setIdKorbanKecelakaan(null);
			listKorbanDasiDto.add(kosong);
			listKorbanDasiDto.addAll(listBaru);
			BindUtils.postNotifyChange(null, null, this, "listKorbanDasiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
//		===================================GET DATA IRSMS KORBAN================================= 
		Map<String, Object> map2 = new HashMap<>();
		map2.put("idKejadian", korlantasAccidentDto.getId().toString());
		map2.put("instansi", korlantasAccidentDto.getKodeInstansi());

		RestResponse restListKorbanIrsms = callWs(WS_URI+"/findByIdKejadian",map2,HttpMethod.POST);
		try {
			listKorbanIrsmsDto = JsonUtil.mapJsonToListObject(restListKorbanIrsms.getContents(), KorlantasAccidentDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKorbanIrsmsDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this,"korlantasAccidentDto");
	}

	public void listJenis() {
		setJenisData("Semua");
		listJenisData.add("Semua");
		listJenisData.add("Belum Di Proses Mapping");
		listJenisData.add("Sudah Di Proses Mapping");
		BindUtils.postNotifyChange(null, null, this, "listJenisData");
		BindUtils.postNotifyChange(null, null, this, "jenisData");
	}

	@Command
	public void close(@BindingParam("window") Window win) {
		if (win != null)
			win.detach();
	}

	@Command
	public void cetakDataLakaIRSMS(
			@BindingParam("item") KorlantasAccidentDto selected)
			throws IOException {
		dokumenDataLakaIRSMS.cetakDokumenDataLakaIRSMS();
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public List<String> getListJenisData() {
		return listJenisData;
	}

	public void setListJenisData(List<String> listJenisData) {
		this.listJenisData = listJenisData;
	}

	public String getJenisData() {
		return jenisData;
	}

	public void setJenisData(String jenisData) {
		this.jenisData = jenisData;
	}

	public List<PlInstansiDto> getListInstansiDto() {
		return listInstansiDto;
	}

	public void setListInstansiDto(List<PlInstansiDto> listInstansiDto) {
		this.listInstansiDto = listInstansiDto;
	}

	public PlInstansiDto getPlInstansiDto() {
		return plInstansiDto;
	}

	public void setPlInstansiDto(PlInstansiDto plInstansiDto) {
		this.plInstansiDto = plInstansiDto;
	}

	public String getSearchInstansi() {
		return searchInstansi;
	}

	public void setSearchInstansi(String searchInstansi) {
		this.searchInstansi = searchInstansi;
	}

	public String getFilterNamaKorban() {
		return filterNamaKorban;
	}

	public void setFilterNamaKorban(String filterNamaKorban) {
		this.filterNamaKorban = filterNamaKorban;
	}

	public String getFilterNoLaporan() {
		return filterNoLaporan;
	}

	public void setFilterNoLaporan(String filterNoLaporan) {
		this.filterNoLaporan = filterNoLaporan;
	}

	public Date getTglKejadianStart() {
		return tglKejadianStart;
	}

	public void setTglKejadianStart(Date tglKejadianStart) {
		this.tglKejadianStart = tglKejadianStart;
	}

	public Date getTglKejadianEnd() {
		return tglKejadianEnd;
	}

	public void setTglKejadianEnd(Date tglKejadianEnd) {
		this.tglKejadianEnd = tglKejadianEnd;
	}

	public Date getTglLaporanStart() {
		return tglLaporanStart;
	}

	public void setTglLaporanStart(Date tglLaporanStart) {
		this.tglLaporanStart = tglLaporanStart;
	}

	public Date getTglLaporanEnd() {
		return tglLaporanEnd;
	}

	public void setTglLaporanEnd(Date tglLaporanEnd) {
		this.tglLaporanEnd = tglLaporanEnd;
	}

	public List<PlMappingPoldaDto> getListMappingPoldaDto() {
		return listMappingPoldaDto;
	}

	public void setListMappingPoldaDto(
			List<PlMappingPoldaDto> listMappingPoldaDto) {
		this.listMappingPoldaDto = listMappingPoldaDto;
	}

	public PlMappingPoldaDto getPlMappingPoldaDto() {
		return plMappingPoldaDto;
	}

	public void setPlMappingPoldaDto(PlMappingPoldaDto plMappingPoldaDto) {
		this.plMappingPoldaDto = plMappingPoldaDto;
	}

	public List<KorlantasAccidentDto> getListIndex() {
		return listIndex;
	}

	public void setListIndex(List<KorlantasAccidentDto> listIndex) {
		this.listIndex = listIndex;
	}

	public List<KorlantasAccidentDto> getListIndexCopy() {
		return listIndexCopy;
	}

	public void setListIndexCopy(List<KorlantasAccidentDto> listIndexCopy) {
		this.listIndexCopy = listIndexCopy;
	}

	public KorlantasAccidentDto getKorlantasAccidentDto() {
		return korlantasAccidentDto;
	}

	public void setKorlantasAccidentDto(
			KorlantasAccidentDto korlantasAccidentDto) {
		this.korlantasAccidentDto = korlantasAccidentDto;
	}

	public boolean isListIndexWindow() {
		return listIndexWindow;
	}

	public void setListIndexWindow(boolean listIndexWindow) {
		this.listIndexWindow = listIndexWindow;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public DokumenDataLakaIRSMS getDokumenDataLakaIRSMS() {
		return dokumenDataLakaIRSMS;
	}

	public void setDokumenDataLakaIRSMS(
			DokumenDataLakaIRSMS dokumenDataLakaIRSMS) {
		this.dokumenDataLakaIRSMS = dokumenDataLakaIRSMS;
	}

	public Date getDetailKejadianStartDate() {
		return detailKejadianStartDate;
	}

	public void setDetailKejadianStartDate(Date detailKejadianStartDate) {
		this.detailKejadianStartDate = detailKejadianStartDate;
	}

	public Date getDetailKejadianEndDate() {
		return detailKejadianEndDate;
	}

	public void setDetailKejadianEndDate(Date detailKejadianEndDate) {
		this.detailKejadianEndDate = detailKejadianEndDate;
	}

	public String getDetailInstansi() {
		return detailInstansi;
	}

	public void setDetailInstansi(String detailInstansi) {
		this.detailInstansi = detailInstansi;
	}

	public Date getDetailLaporanStartDate() {
		return detailLaporanStartDate;
	}

	public void setDetailLaporanStartDate(Date detailLaporanStartDate) {
		this.detailLaporanStartDate = detailLaporanStartDate;
	}

	public Date getDetailLaporanEndDate() {
		return detailLaporanEndDate;
	}

	public void setDetailLaporanEndDate(Date detailLaporanEndDate) {
		this.detailLaporanEndDate = detailLaporanEndDate;
	}

	public String getDetailNoLaporanPolisi() {
		return detailNoLaporanPolisi;
	}

	public void setDetailNoLaporanPolisi(String detailNoLaporanPolisi) {
		this.detailNoLaporanPolisi = detailNoLaporanPolisi;
	}

	public String getDetailNamaKorban() {
		return detailNamaKorban;
	}

	public void setDetailNamaKorban(String detailNamaKorban) {
		this.detailNamaKorban = detailNamaKorban;
	}

	public String getDetailKodeJaminan() {
		return detailKodeJaminan;
	}

	public void setDetailKodeJaminan(String detailKodeJaminan) {
		this.detailKodeJaminan = detailKodeJaminan;
	}

	public String getStatusLP() {
		return statusLP;
	}

	public void setStatusLP(String statusLP) {
		this.statusLP = statusLP;
	}

	public String getDetailSearch() {
		return detailSearch;
	}

	public void setDetailSearch(String detailSearch) {
		this.detailSearch = detailSearch;
	}

	public Date getDetailTglRegister() {
		return detailTglRegister;
	}

	public void setDetailTglRegister(Date detailTglRegister) {
		this.detailTglRegister = detailTglRegister;
	}

	public Date getTimeTL() {
		return timeTL;
	}

	public void setTimeTL(Date timeTL) {
		this.timeTL = timeTL;
	}

	public Date getDateTL() {
		return dateTL;
	}

	public void setDateTL(Date dateTL) {
		this.dateTL = dateTL;
	}

	public String getKeteranganTL() {
		return keteranganTL;
	}

	public void setKeteranganTL(String keteranganTL) {
		this.keteranganTL = keteranganTL;
	}

	public boolean isListDetail() {
		return listDetail;
	}

	public void setListDetail(boolean listDetail) {
		this.listDetail = listDetail;
	}

	public List<PlDataKecelakaanDto> getListDataLakaDto() {
		return listDataLakaDto;
	}

	public void setListDataLakaDto(List<PlDataKecelakaanDto> listDataLakaDto) {
		this.listDataLakaDto = listDataLakaDto;
	}

	public PlDataKecelakaanDto getDataLakaDto() {
		return dataLakaDto;
	}

	public void setDataLakaDto(PlDataKecelakaanDto dataLakaDto) {
		this.dataLakaDto = dataLakaDto;
	}

	public PlInstansiDto getInstansiDto() {
		return instansiDto;
	}

	public void setInstansiDto(PlInstansiDto instansiDto) {
		this.instansiDto = instansiDto;
	}

	public PlJaminanDto getJaminanDto() {
		return jaminanDto;
	}

	public void setJaminanDto(PlJaminanDto jaminanDto) {
		this.jaminanDto = jaminanDto;
	}

	public List<PlKorbanKecelakaanDto> getListKorbanLakaDto() {
		return listKorbanLakaDto;
	}

	public void setListKorbanLakaDto(List<PlKorbanKecelakaanDto> listKorbanLakaDto) {
		this.listKorbanLakaDto = listKorbanLakaDto;
	}

	public PlKorbanKecelakaanDto getKorbanLakaDto() {
		return korbanLakaDto;
	}

	public void setKorbanLakaDto(PlKorbanKecelakaanDto korbanLakaDto) {
		this.korbanLakaDto = korbanLakaDto;
	}

	public List<PlAngkutanKecelakaanDto> getListAngkutanLakaDto() {
		return listAngkutanLakaDto;
	}

	public void setListAngkutanLakaDto(
			List<PlAngkutanKecelakaanDto> listAngkutanLakaDto) {
		this.listAngkutanLakaDto = listAngkutanLakaDto;
	}

	public PlAngkutanKecelakaanDto getAngkutanLakaDto() {
		return angkutanLakaDto;
	}

	public void setAngkutanLakaDto(PlAngkutanKecelakaanDto angkutanLakaDto) {
		this.angkutanLakaDto = angkutanLakaDto;
	}

	public List<PlKorbanKecelakaanDto> getListKorbanLakaMappingDto() {
		return listKorbanLakaMappingDto;
	}

	public void setListKorbanLakaMappingDto(
			List<PlKorbanKecelakaanDto> listKorbanLakaMappingDto) {
		this.listKorbanLakaMappingDto = listKorbanLakaMappingDto;
	}

	public List<KorlantasAccidentDto> getListKorbanIrsmsDto() {
		return listKorbanIrsmsDto;
	}

	public void setListKorbanIrsmsDto(List<KorlantasAccidentDto> listKorbanIrsmsDto) {
		this.listKorbanIrsmsDto = listKorbanIrsmsDto;
	}

	public KorlantasAccidentDto getKorbanIrsmsDto() {
		return korbanIrsmsDto;
	}

	public void setKorbanIrsmsDto(KorlantasAccidentDto korbanIrsmsDto) {
		this.korbanIrsmsDto = korbanIrsmsDto;
	}

	public List<PlKorbanKecelakaanDto> getListKorbanDasiDto() {
		return listKorbanDasiDto;
	}

	public void setListKorbanDasiDto(List<PlKorbanKecelakaanDto> listKorbanDasiDto) {
		this.listKorbanDasiDto = listKorbanDasiDto;
	}

	public PlKorbanKecelakaanDto getKorbanDasiDto() {
		return korbanDasiDto;
	}

	public void setKorbanDasiDto(PlKorbanKecelakaanDto korbanDasiDto) {
		this.korbanDasiDto = korbanDasiDto;
	}

}
