package ui.monitoring;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.expr.NewArray;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Li;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zhtml.S;
import org.zkoss.zk.ui.Sessions;

import share.DasiJrRefCodeDto;
import share.FndKantorJasaraharjaDto;
import share.FndLokasiDto;
import share.PlDataKecelakaanDto;
import share.PlKorbanKecelakaanDto;
import share.PlRegisterSementaraDto;
import share.PlRumahSakitDto;
import share.PlTlRDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;
import core.model.PlRumahSakit;

@Init(superclass = true)
public class MoGLRumahSakitVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent().getAttribute(
			UIConstants.SESS_LOGIN_ID);
	
	private final String WS_URI_LOV = "/Lov";
	private final String WS_URI_REGISTER_SEMENTARA = "/OperasionalRegisterSementara";
	private final String WS_URI = "/MonitoringGlRs";
	private final String WS_URI_LAKA = "/OpDataKecelakaan";
	
	private List<DasiJrRefCodeDto> listKodeJenisPembayaran = new ArrayList<>();
	private DasiJrRefCodeDto kodeJenisPembayaran = new DasiJrRefCodeDto();
	private List<PlRumahSakitDto> listRS = new ArrayList<>();
	private PlRumahSakitDto rumahSakitDto = new PlRumahSakitDto();
	private List<PlTlRDto> listIndexDto = new ArrayList<>();
	private List<PlTlRDto> listIndexDtoCopy = new ArrayList<>();
	private List<PlTlRDto> plTlRDtos = new ArrayList<>();
	private PlTlRDto plTlRDto = new PlTlRDto();
	
	private List<PlTlRDto> listJaminanAll = new ArrayList<>();
	private List<PlTlRDto> listJaminanBayar = new ArrayList<>();
	private List<PlTlRDto> listJaminanHutang = new ArrayList<>();
	private List<PlTlRDto> listJaminanBatal = new ArrayList<>();
	
	private PlTlRDto jaminanAll = new PlTlRDto();
	private PlTlRDto jaminanBayar = new PlTlRDto();
	private PlTlRDto jaminanHutang = new PlTlRDto();
	private PlTlRDto jaminanBatal = new PlTlRDto();
	
	private boolean listIndex = false;
	private int pagesize = 5;
	private String search;
	
	private String searchRS;
	private Date dateAwal;
	private Date dateAkhir;
	private String statusJaminan;
	private String jenisTgl;
	
	private boolean flagBuatJaminanLanjutan;
	private boolean flagOtorisasiJaminan;
	
	private List<PlDataKecelakaanDto> listPlDataKecelakaanDtos = new ArrayList<>();
	private PlDataKecelakaanDto plDataKecelakaanDto = new PlDataKecelakaanDto();

	private List<PlKorbanKecelakaanDto> listKorban = new ArrayList<>();
	private PlKorbanKecelakaanDto plKorbanKecelakaanDto = new PlKorbanKecelakaanDto();

	private List<FndLokasiDto> fndLokasiDtos = new ArrayList<>();
	private List<String> listJenisOtorisasi = new ArrayList<>();
	private String jenisOtorisasi;
	private List<String> listOtorisasi = new ArrayList<>();
	private String otorisasi;
	private String statusOtorisasi;
	private boolean flagOtorisasiWarning;
	private boolean flagOtorisasiView;
	private boolean flagButtonSimpanOtorisasi;
	private boolean flagButtonBatalOtorisasi;
	
	private boolean flagEditJaminan;
	private boolean flagSimpanJaminan;
	
	private List<String> listStatusKorban = new ArrayList<>();
	private String statusKorban;
	
	private List<DasiJrRefCodeDto> listJenisTlDto = new ArrayList<>();
	private DasiJrRefCodeDto jenisTlDto = new DasiJrRefCodeDto();

	private boolean flagRsJaminanLanjutan;
	private boolean flagNominalJaminanLanjutan;
	private boolean flagLoketKantorJaminanLanjutan;
	private boolean flagTglMasukRsJaminanLanjutan;
	private boolean flagCatatanJaminanLanjutan;
	private boolean flagSimpanJaminanLanjutan;
	private boolean flagJenisTlWarning;
	private String flagJenisTlWarningMsg;
	
	private boolean flagDisableSimpanPengajuan;
	private Date tanggalTl = new Date();
	
	private FndKantorJasaraharjaDto loketKantorDto = new FndKantorJasaraharjaDto();
	private List<FndKantorJasaraharjaDto> listLoketKantor = new ArrayList<>();
	private String searchLoketKantor;
	public int totalJmlPengajuan;
	private boolean otorisasiVisible=false;
	private boolean jaminanVisible=true;
	private String jaminanOption="";
	
	protected void loadList(){
		jenisTgl = "Tgl Register";
		loadRumahSakit(userSession.getKantor().substring(0,2));
		loadJenisPembayaran();
		setKodeJenisPembayaran(listKodeJenisPembayaran.get(0));
		if(userSession.getLoginDesc().toLowerCase().contains("otorisator") 
			||userSession.getLoginDesc().toLowerCase().contains("kabag. pelayanan") 
			||userSession.getLoginDesc().toLowerCase().contains("kepala kpjr")){
			setOtorisasiVisible(true);
		}
		BindUtils.postNotifyChange(null, null, this, "kodeJenisPembayaran");
		BindUtils.postNotifyChange(null, null, this, "otorisasiVisible");
		loadJaminanHeader();
	}

	@Command
	public void findAllRS(){
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("search", searchRS);
		RestResponse rest = callWs(WS_URI_LOV+"/findAllRS", mapInput, HttpMethod.POST);
		try {
			
			listRS = JsonUtil.mapJsonToListObject(rest.getContents(), PlRumahSakitDto.class);
			BindUtils.postNotifyChange(null, null, this, "listRS");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command("cariIndex")
	public void loadIndex() throws ParseException{
		SimpleDateFormat date = new SimpleDateFormat("dd/MM/YYYY");
		setDateAwal(getDateAwal()==null?date.parse("01/01/1900"):getDateAwal());
		setDateAkhir(getDateAkhir()==null?date.parse("31/12/9999"):getDateAkhir());
		rumahSakitDto.setKodeRumahsakit(rumahSakitDto.getKodeRumahsakit()==null?userSession.getKantor().substring(0,2):rumahSakitDto.getKodeRumahsakit());
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("kodeKantor", userSession.getKantor().substring(0,5));
		mapInput.put("jenisTgl", jenisTgl);
		mapInput.put("tglAwal", date.format(getDateAwal()));
		mapInput.put("tglAkhir", date.format(getDateAkhir()));
		mapInput.put("kodeRS", rumahSakitDto.getKodeRumahsakit());
		mapInput.put("flagBayar", "");
		
		RestResponse rest = callWs(WS_URI+"/getIndexMonitoringGlRs", mapInput, HttpMethod.POST);
		try {
			List<PlTlRDto> listBaru = new ArrayList<>();
			listBaru = JsonUtil.mapJsonToListObject(rest.getContents(), PlTlRDto.class);
			listIndexDto.clear();
			int tmp = 0;
			for(PlTlRDto o : listBaru){
				PlTlRDto dto = new PlTlRDto();
				dto = o;
//				if(dto.getOtorisasiAwal().equalsIgnoreCase("0")){
//					dto.setCetakVisible(false);
//				}else if(!dto.getOtorisasiAwal().equalsIgnoreCase("0")&&dto.getOtorisasiAjuSmt().equalsIgnoreCase("0")){
//					dto.setCetakVisible(false);
//				}else{
//					dto.setCetakVisible(true);
//				}
				listIndexDto.add(dto);
			}
			listIndexDtoCopy.clear();
			setListIndex(true);
			listIndexDtoCopy.addAll(listIndexDto);
			BindUtils.postNotifyChange(null, null, this, "listIndexDto");
			BindUtils.postNotifyChange(null, null, this, "listIndexDtoCopy");
			BindUtils.postNotifyChange(null, null, this, "totalJmlPengajuan");
			BindUtils.postNotifyChange(null, null, this, "listIndex");
			BindUtils.postNotifyChange(null, null, this, "cetakJaminan");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void changePageSize(){
		setPageSize(getPageSize());
		setListIndex(true);
		BindUtils.postNotifyChange(null, null, this, "pageSize");
	}
	
	public void loadJenisPembayaran(){
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("flag", "Y");
		mapInput.put("rvDomain", "STATUS JAMINAN GL");
		mapInput.put("search", "");
		RestResponse rest = callWs(WS_URI_LOV+"/refCode", mapInput, HttpMethod.POST);
		try {
			listKodeJenisPembayaran = JsonUtil.mapJsonToListObject(rest.getContents(), DasiJrRefCodeDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKodeJenisPembayaran");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loadJaminanHeader(){
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("kodeKantor", userSession.getKantor().substring(0,2)+"%");
		
		RestResponse restAll = callWs(WS_URI+"/jaminanAll", mapInput, HttpMethod.POST);
		RestResponse restBayar = callWs(WS_URI+"/jaminanBayar", mapInput, HttpMethod.POST);
		RestResponse restHutang = callWs(WS_URI+"/jaminanHutang", mapInput, HttpMethod.POST);
		RestResponse restBatal = callWs(WS_URI+"/jaminanBatal", mapInput, HttpMethod.POST);
		
		try {
			listJaminanAll = JsonUtil.mapJsonToListObject(restAll.getContents(), PlTlRDto.class);
			jaminanAll = listJaminanAll.get(0);
			listJaminanBayar = JsonUtil.mapJsonToListObject(restBayar.getContents(), PlTlRDto.class);
			jaminanBayar = listJaminanBayar.get(0);
			listJaminanHutang = JsonUtil.mapJsonToListObject(restHutang.getContents(), PlTlRDto.class);
			jaminanHutang = listJaminanHutang.get(0);
			listJaminanBatal = JsonUtil.mapJsonToListObject(restBatal.getContents(), PlTlRDto.class);
			jaminanBatal = listJaminanBatal.get(0);
			BindUtils.postNotifyChange(null, null, this, "jaminanAll");
			BindUtils.postNotifyChange(null, null, this, "jaminanBayar");
			BindUtils.postNotifyChange(null, null, this, "jaminanHutang");
			BindUtils.postNotifyChange(null, null, this, "jaminanBatal");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
//	====================================BANDBOX RUMAH SAKIT================================================
	@Command
	public void searchRumahSakit(@BindingParam("val")String search){
		loadRumahSakit(search);
	}
	
	private void loadRumahSakit(String search){
		Map<String, Object> map = new HashMap<>();
		map.put("search", search);
		RestResponse rest = callWs(WS_URI_LOV + "/findAllRS", map,
				HttpMethod.POST);
		try {
			listRS = JsonUtil.mapJsonToListObject(rest.getContents(), PlRumahSakitDto.class);
			BindUtils.postNotifyChange(null, null, this, "listRS");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void pilihRs(@BindingParam("item")PlRumahSakitDto rumahSakitDto){
		this.rumahSakitDto = rumahSakitDto;
		BindUtils.postNotifyChange(null, null, this, "rumahSakitDto");
	}
	
//	====================================BANDBOX LOKET KANTOR================================================
	@Command
	public void searchLoketKantor(@BindingParam("val")String search){
		loadLoketKantor(search);
	}
	
	private void loadLoketKantor(String search){
		Map<String, Object> map = new HashMap<>();
		map.put("search", search);
		RestResponse rest = callWs(WS_URI_LOV + "/getListLoketKantor", map,
				HttpMethod.POST);
		try {
			listLoketKantor = JsonUtil.mapJsonToListObject(rest.getContents(), FndKantorJasaraharjaDto.class);
			BindUtils.postNotifyChange(null, null, this, "listLoketKantor");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void pilihLoketKantor(@BindingParam("item")FndKantorJasaraharjaDto loketKantor){
		this.loketKantorDto = loketKantor;
		BindUtils.postNotifyChange(null, null, this, "loketKantorDto");
	}
	
//	=================================BUAT JAMINAN LANJUTAN==========================================================
	@Command
	public void buatJaminanLanjutan(@BindingParam("item")PlTlRDto selectedDto){
		setFlagBuatJaminanLanjutan(true);
		setFlagEditJaminan(true);
		listStatusKorban();
		listJenisTl();
		BindUtils.postNotifyChange(null, null, this, "flagBuatJaminanLanjutan");
		BindUtils.postNotifyChange(null, null, this, "flagEditJaminan");
		plTlRDto = selectedDto;

		// =============================GET DATA LAKA=======================================
		Map<String, Object> mapLaka = new HashMap<>();
		mapLaka.put("idKecelakaan", selectedDto.getIdLaka());
		RestResponse restLaka = callWs(WS_URI_LAKA + "/dataLakaById", mapLaka,
			HttpMethod.POST);
		try {
			listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(restLaka.getContents(), PlDataKecelakaanDto.class);
			for (PlDataKecelakaanDto a : listPlDataKecelakaanDtos) {
				plDataKecelakaanDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
				plDataKecelakaanDto.setKodeLokasi(a.getKodeLokasi());
				plDataKecelakaanDto.setTglKejadian(a.getTglKejadian());
				plDataKecelakaanDto.setKasusKecelakaanDesc(a.getKasusKecelakaanDesc());
				plDataKecelakaanDto.setDeskripsiKecelakaan(a.getDeskripsiKecelakaan());
			}
			
			Map<String, Object> mapInput = new HashMap<>();
			mapInput.put("kodeLokasi", plDataKecelakaanDto.getKodeLokasi());
			RestResponse rest = callWs(WS_URI_LOV+ "/findOneLokasi",mapInput, HttpMethod.POST);
			try{
				fndLokasiDtos = JsonUtil.mapJsonToListObject(rest.getContents(), FndLokasiDto.class);
				BindUtils.postNotifyChange(null, null, this, "fndLokasiDtos");
			}catch(Exception e){
				e.printStackTrace();
			}
			
			for(FndLokasiDto lok : fndLokasiDtos){
				if(plDataKecelakaanDto.getKodeLokasi().equalsIgnoreCase(lok.getKodeLokasi())){
					plDataKecelakaanDto.setKodeLokasi(lok.getKodeLokasi()+" - "+lok.getDeskripsi());
				}
			}
			BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
		} catch (Exception e) {
			e.printStackTrace();
		}

		// ==========================GET DATA KORBAN=====================================
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("idKorban", selectedDto.getIdKorbanKecelakaan());
		RestResponse rest = callWs(WS_URI_LAKA + "/korbanLakaByIdKorban",mapInput, HttpMethod.POST);
			try {
			listKorban = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlKorbanKecelakaanDto.class);
				for (PlKorbanKecelakaanDto a : listKorban) {
					plKorbanKecelakaanDto.setNama(a.getNama());
					plKorbanKecelakaanDto.setUmur(a.getUmur());
					if(a.getJenisKelamin().equalsIgnoreCase("P")){
						plKorbanKecelakaanDto.setJenisKelamin("Pria");
					}else if(a.getJenisKelamin().equalsIgnoreCase("W")){
						plKorbanKecelakaanDto.setJenisKelamin("Wanita");
					}
					plKorbanKecelakaanDto.setJenisIdentitasDesc(a.getJenisIdentitasDesc());
					plKorbanKecelakaanDto.setNoIdentitas(a.getNoIdentitas());
					plKorbanKecelakaanDto.setCideraDesc(a.getCideraDesc());
					plKorbanKecelakaanDto.setAlamat(a.getAlamat());					
				}
				BindUtils.postNotifyChange(null, null, this, "plKorbanKecelakaanDto");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			// =============================GET DATA PL TL RS=======================================
			Map<String, Object> mapPlTl = new HashMap<>();
			mapPlTl.put("idJaminan", selectedDto.getIdJaminan());
			RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
				HttpMethod.POST);
			try {
				plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
				for (PlTlRDto a : plTlRDtos) {
					plTlRDto.setNoSuratJaminan(a.getNoSuratJaminan());
					plTlRDto.setTglSuratJaminan(a.getTglSuratJaminan());
					plTlRDto.setKodeRumahSakit(a.getKodeRumahSakit());
					plTlRDto.setJumlahPengajuan1(a.getJumlahPengajuan1());
					plTlRDto.setJmlAmbl(a.getJmlAmbl());
					plTlRDto.setJmlP3k(a.getJmlP3k());
					plTlRDto.setTglMasukRs(a.getTglMasukRs());
					plTlRDto.setOtorisasiAwal(a.getOtorisasiAwal());
					plTlRDto.setOtorisasiAjuSmt(a.getOtorisasiAjuSmt());
					plTlRDto.setCreatedDate(a.getCreatedDate());
					plTlRDto.setFlagBayar(a.getFlagBayar());
					plTlRDto.setJmlPengajuanSementara(a.getJmlPengajuanSementara()==null||a.getJmlPengajuanSementara().intValue()==0?a.getJumlahPengajuan1():a.getJmlPengajuanSementara());
					plTlRDto.setJmlAmblSementara(a.getJmlAmblSementara()==null||a.getJmlAmblSementara().intValue()==0?a.getJmlAmbl():a.getJmlAmblSementara());
					plTlRDto.setJmlP3kSementara(a.getJmlP3kSementara()==null||a.getJmlP3kSementara().intValue()==0?a.getJmlP3k():a.getJmlP3kSementara());
					plTlRDto.setJenisTagihan(a.getJenisTagihan());
					if(plTlRDto.getJenisTagihan().equalsIgnoreCase("T")){
						setStatusKorban("Terbit Tagihan Dari RS");
					}else if(plTlRDto.getJenisTagihan().equalsIgnoreCase("S")){
						setStatusKorban("Korban Ttd Surat Pernyataan");
					}
					rumahSakitDto.setKodeRumahsakit(plTlRDto.getKodeRumahSakit());
				}
				loadRumahSakit(plTlRDto.getKodeRumahSakit());
				for(PlRumahSakitDto rs : listRS){
					if(rumahSakitDto.getKodeRumahsakit().equalsIgnoreCase(rs.getKodeRumahsakit())){
						rumahSakitDto = new PlRumahSakitDto();
						rumahSakitDto = rs;
					}
				}
				cekPengajuanSementara();
				BindUtils.postNotifyChange(null, null, this, "plTlRDto");
				BindUtils.postNotifyChange(null, null, this, "statusKorban");
				BindUtils.postNotifyChange(null, null, this, "rumahSakitDto");
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
	
	@Command
	public void closeJaminanLanjutan(){
		setFlagBuatJaminanLanjutan(false);
		BindUtils.postNotifyChange(null, null, this, "flagBuatJaminanLanjutan");
	}
	
//	===================================================OTORISASI JAMINAN======================================
	private Date tglJaminan = new Date();

	@Command
	@NotifyChange("plTlRDto")
	public void otorisasiJaminan(@BindingParam("item")PlTlRDto selectedDto){
		setFlagOtorisasiJaminan(true);
		listJenisOtorisasi();
		listOtorisasi();
		BindUtils.postNotifyChange(null, null, this, "flagOtorisasiJaminan");
		plTlRDto = selectedDto;
		
		// =============================GET DATA PL TL RS=======================================
		Map<String, Object> mapPlTl = new HashMap<>();
		mapPlTl.put("idJaminan", selectedDto.getIdJaminan());
		RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
			HttpMethod.POST);
		try {
			plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
			for (PlTlRDto a : plTlRDtos) {
				plTlRDto.setJumlahPengajuan1(a.getJumlahPengajuan1());
				plTlRDto.setJmlAmbl(a.getJmlAmbl());
				plTlRDto.setJmlP3k(a.getJmlP3k());
				plTlRDto.setTglMasukRs(a.getTglMasukRs());
				plTlRDto.setTglSuratJaminan(a.getTglSuratJaminan());
				plTlRDto.setOtorisasiAwal(a.getOtorisasiAwal());
				plTlRDto.setOtorisasiAjuSmt(a.getOtorisasiAjuSmt());
				plTlRDto.setCreatedDate(a.getCreatedDate());
				plTlRDto.setFlagBayar(a.getFlagBayar());
				plTlRDto.setKeteranganOtorisasi(a.getKeteranganOtorisasi());
			}
			BindUtils.postNotifyChange(null, null, this, "plTlRDto");
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		// ==========================GET DATA KORBAN=====================================
		Map<String, Object> mapInput = new HashMap<>();
		mapInput.put("idKorban", selectedDto.getIdKorbanKecelakaan());
		RestResponse rest = callWs(WS_URI_LAKA + "/korbanLakaByIdKorban",mapInput, HttpMethod.POST);
			try {
			listKorban = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlKorbanKecelakaanDto.class);
				for (PlKorbanKecelakaanDto a : listKorban) {
					plKorbanKecelakaanDto.setNama(a.getNama());
				}
				BindUtils.postNotifyChange(null, null, this, "plKorbanKecelakaanDto");
			} catch (Exception e) {
				e.printStackTrace();
			}

			// =============================GET DATA LAKA=======================================
			Map<String, Object> mapLaka = new HashMap<>();
			mapLaka.put("idKecelakaan", selectedDto.getIdLaka());
			RestResponse restLaka = callWs(WS_URI_LAKA + "/dataLakaById", mapLaka,
				HttpMethod.POST);
			try {
				listPlDataKecelakaanDtos = JsonUtil.mapJsonToListObject(restLaka.getContents(), PlDataKecelakaanDto.class);
				for (PlDataKecelakaanDto a : listPlDataKecelakaanDtos) {
					plDataKecelakaanDto.setNoLaporanPolisi(a.getNoLaporanPolisi());
					plDataKecelakaanDto.setTglKejadian(a.getTglKejadian());
				}
				BindUtils.postNotifyChange(null, null, this, "plDataKecelakaanDto");
			} catch (Exception e) {
				e.printStackTrace();
			}
		
//		===================CEK STATUS================
		cekStatusOtorisasi();
		cekJenisOtorisasi();
	}
	
	@Command
	public void simpanOtorisasi(){
		// =============================GET DATA PL TL RS=======================================
		Map<String, Object> mapPlTl = new HashMap<>();
		mapPlTl.put("idJaminan", plTlRDto.getIdJaminan());
		RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
		HttpMethod.POST);
		try {
			plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
			for (PlTlRDto a : plTlRDtos) {
				a.setKeteranganOtorisasi(plTlRDto.getKeteranganOtorisasi());
				if(a.getFlagBayar().equalsIgnoreCase("0")){
					a.setOtorisasiAwal(otorisasi.equalsIgnoreCase("disetujui")?"1":"2");
				}else if(a.getFlagBayar().equalsIgnoreCase("4")){
					a.setOtorisasiAjuSmt(otorisasi.equalsIgnoreCase("disetujui")?"1":"2");
				}
				a.setLastUpdatedBy(userSession.getLoginID());
				a.setLastUpdatedDate(new Date());
				plTlRDto = a;
			}
			
			RestResponse savePlTlRsResponse = callWs(WS_URI+"/savePlTlRs", plTlRDto, HttpMethod.POST);
			if(savePlTlRsResponse.getStatus() == CommonConstants.OK_REST_STATUS){
				showInfoMsgBox(savePlTlRsResponse.getMessage());
				listJenisOtorisasi();
				cekStatusOtorisasi();
				cekJenisOtorisasi();
			}else{
				showErrorMsgBox(savePlTlRsResponse.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}			
	
	@Command
	public void batalOtorisasi(){
		// =============================GET DATA PL TL RS=======================================
		Map<String, Object> mapPlTl = new HashMap<>();
		mapPlTl.put("idJaminan", plTlRDto.getIdJaminan());
		RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
		HttpMethod.POST);
		try {
			plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
			for (PlTlRDto a : plTlRDtos) {
				a.setKeteranganOtorisasi(plTlRDto.getKeteranganOtorisasi());
				if(!a.getOtorisasiAwal().equalsIgnoreCase("0")){
					a.setOtorisasiAwal("0");
				}
				a.setLastUpdatedBy(userSession.getLoginID());
				a.setLastUpdatedDate(new Date());
				plTlRDto = a;
			}
			
			RestResponse savePlTlRsResponse = callWs(WS_URI+"/savePlTlRs", plTlRDto, HttpMethod.POST);
			if(savePlTlRsResponse.getStatus() == CommonConstants.OK_REST_STATUS){
				showInfoMsgBox(savePlTlRsResponse.getMessage());
				listJenisOtorisasi();
				cekStatusOtorisasi();
				cekJenisOtorisasi();
			}else{
				showErrorMsgBox(savePlTlRsResponse.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}	
	}
	
	@Command
	public void simpanPengajuan(){
		if(cekNominal()){
			plTlRDto.setJmlPengajuanSementara(plTlRDto.getJumlahPengajuan1());
			plTlRDto.setJmlAmblSementara(plTlRDto.getJmlAmbl());
			plTlRDto.setJmlP3kSementara(plTlRDto.getJmlP3k());
			BindUtils.postNotifyChange(null, null, this, "plTlRDto");
			return;
		}
		else if(statusKorban.trim().equalsIgnoreCase("-")||statusKorban==null||statusKorban.equalsIgnoreCase("")){
			showInfoMsgBox("Jenis Status Harus Dipilih!", "");
		}else{
			// =============================GET DATA PL TL RS=======================================
			Map<String, Object> mapPlTl = new HashMap<>();
			mapPlTl.put("idJaminan", plTlRDto.getIdJaminan());
			RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
			HttpMethod.POST);
			try {
				plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
				for (PlTlRDto a : plTlRDtos) {
					a.setJmlPengajuanSementara(plTlRDto.getJmlPengajuanSementara());
					a.setJmlAmblSementara(plTlRDto.getJmlAmblSementara());
					a.setJmlP3kSementara(plTlRDto.getJmlP3kSementara());
					a.setJumlahPengajuanSmt(plTlRDto.getJmlPengajuanSementara());
					a.setJmlAmblSmt(plTlRDto.getJmlAmblSementara());
					a.setJmlP3kSmt(plTlRDto.getJmlP3kSementara());
					a.setFlagBayar("4");
					if(statusKorban.equalsIgnoreCase("Terbit Tagihan Dari RS")){
						a.setJenisTagihan("T");
					}else if(statusKorban.equalsIgnoreCase("Korban Ttd Surat Pernyataan")){
						a.setJenisTagihan("S");
					}
					a.setKodeRsSementara(rumahSakitDto.getKodeRumahsakit());
					a.setLastUpdatedBy(userSession.getLoginID());
					a.setLastUpdatedDate(new Date());
					plTlRDto = a;
				}
				
				RestResponse savePlTlRsResponse = callWs(WS_URI+"/savePlTlRs", plTlRDto, HttpMethod.POST);
				if(savePlTlRsResponse.getStatus() == CommonConstants.OK_REST_STATUS){
					showInfoMsgBox(savePlTlRsResponse.getMessage());
					cekPengajuanSementara();
				}else{
					showErrorMsgBox(savePlTlRsResponse.getMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
	}
	
	@Command
	public void batalPengajuan(){
		if(plTlRDto.getFlagBayar().equalsIgnoreCase("0")){
			showInfoMsgBox("Status tahapan tidak sesuai, tidak dapat dibatalkan !", "");
		}else if(plTlRDto.getOtorisasiAjuSmt().equalsIgnoreCase("1")){
			showInfoMsgBox("Pengajuan telah diotorisasi, tidak dapat dibatalkan !", "");			
		}else{
			// =============================GET DATA PL TL RS=======================================
			Map<String, Object> mapPlTl = new HashMap<>();
			mapPlTl.put("idJaminan", plTlRDto.getIdJaminan());
			RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
			HttpMethod.POST);
			try {
				plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
				for (PlTlRDto a : plTlRDtos) {
					a.setJmlPengajuanSementara(new BigDecimal(0));
					a.setJmlAmblSementara(new BigDecimal(0));
					a.setJmlP3kSementara(new BigDecimal(0));
					a.setJumlahPengajuanSmt(null);
					a.setJmlAmblSmt(null);
					a.setJmlP3kSmt(null);
					a.setFlagBayar("0");
					a.setJenisTagihan("0");
					a.setLastUpdatedBy(userSession.getLoginID());
					a.setLastUpdatedDate(new Date());
					plTlRDto = a;
				}
				RestResponse savePlTlRsResponse = callWs(WS_URI+"/savePlTlRs", plTlRDto, HttpMethod.POST);
				if(savePlTlRsResponse.getStatus() == CommonConstants.OK_REST_STATUS){
					showInfoMsgBox("Pengajuan Sementara berhasil dibatalkan!");
					cekPengajuanSementara();
				}else{
					showErrorMsgBox(savePlTlRsResponse.getMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
	}
	
	@Command
	public void simpanJaminanLanjutan(){
		if(jenisTlDto.getRvLowValue().equalsIgnoreCase("3")){
			// =============================GET DATA PL TL RS=======================================
			Map<String, Object> mapPlTl = new HashMap<>();
			mapPlTl.put("idJaminan", plTlRDto.getIdJaminan());
			RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
			HttpMethod.POST);
			try {
				plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
				for (PlTlRDto a : plTlRDtos) {
					a.setJmlPengajuanSementara(new BigDecimal(0));
					a.setJmlAmblSementara(new BigDecimal(0));
					a.setJmlP3kSementara(new BigDecimal(0));
					a.setJumlahPengajuanSmt(new BigDecimal(0));
					a.setJmlAmblSmt(new BigDecimal(0));
					a.setJmlP3kSmt(new BigDecimal(0));
					a.setFlagBayar("2");
					a.setLastUpdatedBy(userSession.getLoginID());
					a.setLastUpdatedDate(new Date());
					plTlRDto = a;
				}
				RestResponse savePlTlRsResponse = callWs(WS_URI+"/savePlTlRs", plTlRDto, HttpMethod.POST);
				if(savePlTlRsResponse.getStatus() == CommonConstants.OK_REST_STATUS){
					showInfoMsgBox("Jaminan Berhasil Dibatalkan!");
				}else{
					showErrorMsgBox(savePlTlRsResponse.getMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}else if(jenisTlDto.getRvLowValue().equalsIgnoreCase("1")){
			// =============================GET DATA PL TL RS=======================================
			PlTlRDto plTlRDtoBaru = new PlTlRDto();
			Map<String, Object> mapPlTl = new HashMap<>();
			mapPlTl.put("idJaminan", plTlRDto.getIdJaminan());
			RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
			HttpMethod.POST);
			try {
				plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
				for (PlTlRDto a : plTlRDtos) {
					String idJaminanBaru = null;
					String incVal = a.getIdJaminan().substring(21,23);
					int countVal = Integer.valueOf(incVal)+1;
					idJaminanBaru = a.getIdJaminan().substring(0,21)+String.format("%02d", countVal);
					plTlRDtoBaru.setIdJaminan(idJaminanBaru);
					plTlRDtoBaru.setNoRegister(a.getNoRegister());
					plTlRDtoBaru.setKodeKantorJr(loketKantorDto.getKodeKantorJr());
					plTlRDtoBaru.setKodeRumahSakit(rumahSakitDto.getKodeRumahsakit());
					plTlRDtoBaru.setJumlahPengajuan1(plTlRDto.getJmlPengajuanSementara());
					plTlRDtoBaru.setJmlAmbl(plTlRDto.getJmlAmblSementara());
					plTlRDtoBaru.setJmlP3k(plTlRDto.getJmlP3kSementara());
					plTlRDtoBaru.setFlagBayar("3");
					plTlRDtoBaru.setFlagBayarAwal("0");
					plTlRDtoBaru.setJenisTagihan("0");
					plTlRDtoBaru.setOtorisasiAwal("0");
					plTlRDtoBaru.setOtorisasiAjuSmt("0");
					plTlRDtoBaru.setFlagLanjutan("0");
					plTlRDtoBaru.setFlagLimpahan("1");
					plTlRDtoBaru.setKodeKantorAsalLimp(a.getKodeKantorJr());
					plTlRDtoBaru.setCreatedBy(userSession.getLoginID());
					plTlRDtoBaru.setCreatedDate(new Date());
					plTlRDto = a;
					plTlRDto.setFlagLanjutan("2");
					plTlRDto.setLastUpdatedBy(userSession.getLoginID());
					plTlRDto.setLastUpdatedDate(new Date());
				}
				RestResponse savePlTlRsResponse = callWs(WS_URI+"/savePlTlRs", plTlRDto, HttpMethod.POST);
				if(savePlTlRsResponse.getStatus() == CommonConstants.OK_REST_STATUS){
					showInfoMsgBox("Jaminan Berhasil Dibatalkan!");
				}else{
					showErrorMsgBox(savePlTlRsResponse.getMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}else if(jenisTlDto.getRvLowValue().equalsIgnoreCase("2")){
			// =============================GET DATA PL TL RS=======================================
			PlTlRDto plTlRDtoBaru = new PlTlRDto();
			Map<String, Object> mapPlTl = new HashMap<>();
			mapPlTl.put("idJaminan", plTlRDto.getIdJaminan());
			RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
			HttpMethod.POST);
			try {
				plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
				for (PlTlRDto a : plTlRDtos) {
					String idJaminanBaru = null;
					String incVal = a.getIdJaminan().substring(21,23);
					int countVal = Integer.valueOf(incVal)+1;
					idJaminanBaru = a.getIdJaminan().substring(0,21)+String.format("%02d", countVal);
					plTlRDtoBaru.setIdJaminan(idJaminanBaru);
					plTlRDtoBaru.setNoRegister(a.getNoRegister());
					plTlRDtoBaru.setKodeKantorJr(loketKantorDto.getKodeKantorJr());
					plTlRDtoBaru.setKodeRumahSakit(rumahSakitDto.getKodeRumahsakit());
					plTlRDtoBaru.setJumlahPengajuan1(plTlRDto.getJmlPengajuanSementara());
					plTlRDtoBaru.setJmlAmbl(plTlRDto.getJmlAmblSementara());
					plTlRDtoBaru.setJmlP3k(plTlRDto.getJmlP3kSementara());
					plTlRDtoBaru.setFlagBayar("3");
					plTlRDtoBaru.setFlagBayarAwal("0");
					plTlRDtoBaru.setJenisTagihan("0");
					plTlRDtoBaru.setOtorisasiAwal("0");
					plTlRDtoBaru.setOtorisasiAjuSmt("0");
					plTlRDtoBaru.setFlagLanjutan("0");
					plTlRDtoBaru.setFlagLimpahan("1");
					plTlRDtoBaru.setKodeKantorAsalLimp(a.getKodeKantorJr());
					plTlRDtoBaru.setCreatedBy(userSession.getLoginID());
					plTlRDtoBaru.setCreatedDate(new Date());
					plTlRDto = a;
					plTlRDto.setFlagLanjutan("2");
					plTlRDto.setLastUpdatedBy(userSession.getLoginID());
					plTlRDto.setLastUpdatedDate(new Date());
				}
				RestResponse savePlTlRsResponse = callWs(WS_URI+"/savePlTlRs", plTlRDto, HttpMethod.POST);
				RestResponse savePlTlRsResponseBaru = callWs(WS_URI+"/savePlTlRs", plTlRDtoBaru, HttpMethod.POST);
				if(savePlTlRsResponse.getStatus() == CommonConstants.OK_REST_STATUS&&savePlTlRsResponseBaru.getStatus()== CommonConstants.OK_REST_STATUS){
					showInfoMsgBox("Jaminan Berhasil Dilimpahkan!");
				}else{
					showErrorMsgBox(savePlTlRsResponse.getMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}else if(jenisTlDto.getRvLowValue().equalsIgnoreCase("4")){
			// =============================GET DATA PL TL RS=======================================
			PlTlRDto plTlRDtoBaru = new PlTlRDto();
			Map<String, Object> mapPlTl = new HashMap<>();
			mapPlTl.put("idJaminan", plTlRDto.getIdJaminan());
			RestResponse restJaminan = callWs(WS_URI + "/plTlByIdJaminan", mapPlTl,
			HttpMethod.POST);
			try {
				plTlRDtos = JsonUtil.mapJsonToListObject(restJaminan.getContents(), PlTlRDto.class);
				for (PlTlRDto a : plTlRDtos) {
					plTlRDto = a;
//					plTlRDto.setNoSuratJaminan(noSuratJaminan);
					plTlRDto.setFlagBayar("0");
					plTlRDto.setLastUpdatedBy(userSession.getLoginID());
					plTlRDto.setLastUpdatedDate(new Date());
				}
				RestResponse savePlTlRsResponse = callWs(WS_URI+"/savePlTlRs", plTlRDto, HttpMethod.POST);
				RestResponse savePlTlRsResponseBaru = callWs(WS_URI+"/savePlTlRs", plTlRDtoBaru, HttpMethod.POST);
				if(savePlTlRsResponse.getStatus() == CommonConstants.OK_REST_STATUS&&savePlTlRsResponseBaru.getStatus()== CommonConstants.OK_REST_STATUS){
					showInfoMsgBox("Jaminan Berhasil Dilimpahkan!");
				}else{
					showErrorMsgBox(savePlTlRsResponse.getMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}	
		}
	}
	
	@Command
	public void batalJaminanLanjutan(){
		setFlagCatatanJaminanLanjutan(false);
		setFlagLoketKantorJaminanLanjutan(false);
		setFlagRsJaminanLanjutan(false);
		setFlagNominalJaminanLanjutan(false);
		setFlagTglMasukRsJaminanLanjutan(false);
		setFlagSimpanJaminanLanjutan(false);
		BindUtils.postNotifyChange(null, null, this, "flagCatatanJaminanLanjutan");
		BindUtils.postNotifyChange(null, null, this, "flagLoketKantorJaminanLanjutan");
		BindUtils.postNotifyChange(null, null, this, "flagRsJaminanLanjutan");
		BindUtils.postNotifyChange(null, null, this, "flagTglMasukRsJaminanLanjutan");
		BindUtils.postNotifyChange(null, null, this, "flagNominalJaminanLanjutan");
		BindUtils.postNotifyChange(null, null, this, "flagSimpanJaminanLanjutan");
	}

	
	@Command
	public void closeOtorisasiJaminan(){
		setFlagOtorisasiJaminan(false);
		BindUtils.postNotifyChange(null, null, this, "flagOtorisasiJaminan");
	}
	
	public boolean cekNominal(){
		if(plTlRDto.getJmlPengajuanSementara().intValue()>plTlRDto.getJumlahPengajuan1().intValue()){
			showInfoMsgBox("Nominal Pengajuan luka2 tidak boleh lebih besar dari Nilai Jaminan! Rp "+plTlRDto.getJumlahPengajuan1(), "");
			return true;
		}else if(plTlRDto.getJmlAmblSementara().intValue()>plTlRDto.getJmlAmbl().intValue()){
			showInfoMsgBox("Nominal Pengajuan Ambulans tidak boleh lebih besar dari Nilai Jaminan! Rp "+plTlRDto.getJmlAmbl(), "");
			return true;
		}else if(plTlRDto.getJmlP3kSementara().intValue()>plTlRDto.getJmlP3k().intValue()){
			showInfoMsgBox("Nominal Pengajuan P3K tidak boleh lebih besar dari Nilai Jaminan! Rp "+plTlRDto.getJmlP3k(), "");
			return true;
		}
		return false;
	}
	
	@NotifyChange({"listJenisOtorisasi","jenisOtorisasi"})
	public void listJenisOtorisasi(){
		listJenisOtorisasi.clear();
		listJenisOtorisasi.add("--Pilih Jenis Otorisasi--");
		listJenisOtorisasi.add("Otorisasi Jaminan Baru");
		listJenisOtorisasi.add("Otorisasi Pengajuan Sementara");
		setJenisOtorisasi("--Pilih Jenis Otorisasi--");
		BindUtils.postNotifyChange(null, null, this, "listJenisOtorisasi");
		BindUtils.postNotifyChange(null, null, this, "jenisOtorisasi");
	}
	
	public void listOtorisasi(){
		listOtorisasi.clear();
		listOtorisasi.add("Disetujui");
		listOtorisasi.add("Ditolak");
		setOtorisasi("Disetujui");
		BindUtils.postNotifyChange(null, null, this, "listOtorisasi");
		BindUtils.postNotifyChange(null, null, this, "otorisasi");
	}
	
	@Command
	public void cekJenisOtorisasi(){
		if(plTlRDto.getFlagBayar().equalsIgnoreCase("0")){
			if(jenisOtorisasi.contains("Pilih")){
				setFlagOtorisasiView(false);
				setFlagOtorisasiWarning(false);
			}else if(jenisOtorisasi.contains("Jaminan")&&!getStatusOtorisasi().equalsIgnoreCase("DITOLAK")){
				setFlagOtorisasiView(true);
				setFlagOtorisasiWarning(false);
			}else if(jenisOtorisasi.contains("Sementara")){
				setFlagOtorisasiWarning(true);
				setFlagOtorisasiView(false);
			}
			
			BindUtils.postNotifyChange(null, null, this, "flagOtorisasiView");
			BindUtils.postNotifyChange(null, null, this, "jenisOtorisasi");
			BindUtils.postNotifyChange(null, null, this, "flagOtorisasiWarning");			
		}else if(plTlRDto.getFlagBayar().equalsIgnoreCase("4")){
			if(jenisOtorisasi.contains("Pilih")){
				setFlagOtorisasiView(false);
				setFlagOtorisasiWarning(false);
			}else if(jenisOtorisasi.contains("Jaminan")&&!getStatusOtorisasi().equalsIgnoreCase("DITOLAK")){
				setFlagOtorisasiView(false);
				setFlagOtorisasiWarning(true);
			}else if(jenisOtorisasi.contains("Sementara")){
				setFlagOtorisasiWarning(false);
				setFlagOtorisasiView(true);
			}
			BindUtils.postNotifyChange(null, null, this, "flagOtorisasiView");
			BindUtils.postNotifyChange(null, null, this, "jenisOtorisasi");
			BindUtils.postNotifyChange(null, null, this, "flagOtorisasiWarning");

		}
	}
	
	public void cekStatusOtorisasi(){
		if(plTlRDto.getFlagBayar().equalsIgnoreCase("0")){
			if(plTlRDto.getOtorisasiAwal().equalsIgnoreCase("0")){
				setStatusOtorisasi("BELUM DIOTORISASI");
				setFlagButtonSimpanOtorisasi(true);
				setFlagButtonBatalOtorisasi(false);
			}else if(plTlRDto.getOtorisasiAwal().equalsIgnoreCase("1")){
				setStatusOtorisasi("DISETUJUI");
				setFlagButtonBatalOtorisasi(true);
				setFlagButtonSimpanOtorisasi(false);
			}else if(plTlRDto.getOtorisasiAwal().equalsIgnoreCase("2")){
				setStatusOtorisasi("DITOLAK");
				setFlagButtonBatalOtorisasi(true);
				setFlagButtonSimpanOtorisasi(false);
			}
			BindUtils.postNotifyChange(null, null, this, "statusOtorisasi");
			BindUtils.postNotifyChange(null, null, this, "flagButtonSimpanOtorisasi");
			BindUtils.postNotifyChange(null, null, this, "flagButtonBatalOtorisasi");			
		}else if(plTlRDto.getFlagBayar().equalsIgnoreCase("4")){
			if(plTlRDto.getOtorisasiAjuSmt().equalsIgnoreCase("0")){
				setStatusOtorisasi("BELUM DIOTORISASI");
				setFlagButtonSimpanOtorisasi(true);
				setFlagButtonBatalOtorisasi(false);
			}else if(plTlRDto.getOtorisasiAjuSmt().equalsIgnoreCase("1")){
				setStatusOtorisasi("DISETUJUI");
				setFlagButtonBatalOtorisasi(true);
				setFlagButtonSimpanOtorisasi(false);
			}else if(plTlRDto.getOtorisasiAjuSmt().equalsIgnoreCase("2")){
				setStatusOtorisasi("DITOLAK");
				setFlagButtonBatalOtorisasi(true);
				setFlagButtonSimpanOtorisasi(false);
			}
			BindUtils.postNotifyChange(null, null, this, "statusOtorisasi");
			BindUtils.postNotifyChange(null, null, this, "flagButtonSimpanOtorisasi");
			BindUtils.postNotifyChange(null, null, this, "flagButtonBatalOtorisasi");			
		}
	}
	
	@Command
	public void editJaminan(){
		setFlagEditJaminan(false);
		setFlagSimpanJaminan(true);
		BindUtils.postNotifyChange(null, null, this, "flagEditJaminan");
		BindUtils.postNotifyChange(null, null, this, "flagSimpanJaminan");
	}
	
	@Command
	public void batalEditJaminan(){
		setFlagEditJaminan(true);
		setFlagSimpanJaminan(false);
		BindUtils.postNotifyChange(null, null, this, "flagEditJaminan");
		BindUtils.postNotifyChange(null, null, this, "flagSimpanJaminan");
	}
	
	@Command
	public void simpanJaminan(){
		if(!plTlRDto.getOtorisasiAwal().equalsIgnoreCase("0")){
			showInfoMsgBox("Jaminan telah diotorisasi, Tidak dapat melakukan perubahan jaminan!", "");
			setFlagEditJaminan(true);
			setFlagSimpanJaminan(false);
			BindUtils.postNotifyChange(null, null, this, "flagEditJaminan");
			BindUtils.postNotifyChange(null, null, this, "flagSimpanJaminan");
		}
	}
	
	public void listJenisTl(){
		RestResponse restJenisTl = callWs(WS_URI_LOV+"/getListJenisTl", new HashMap<>(), HttpMethod.POST);
		List<DasiJrRefCodeDto> list = new ArrayList<>();
		listJenisTlDto = new ArrayList<>();
		try{
			list = JsonUtil.mapJsonToListObject(restJenisTl.getContents(), DasiJrRefCodeDto.class);
			DasiJrRefCodeDto kosong = new DasiJrRefCodeDto();
			kosong.setRvLowValue("");
			kosong.setRvMeaning("-");
			listJenisTlDto.add(kosong);
			listJenisTlDto.addAll(list);
			System.err.println("luthfi20 "+listJenisTlDto.size());
			BindUtils.postNotifyChange(null, null, this, "listJenisTlDto");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void listStatusKorban() {
		listStatusKorban.clear();
		setStatusKorban("-");
		listStatusKorban.add("-");
		listStatusKorban.add("Terbit Tagihan Dari RS");
		listStatusKorban.add("Korban Ttd Surat Pernyataan");
		BindUtils.postNotifyChange(null, null, this, "statusKorban");
		BindUtils.postNotifyChange(null, null, this, "listStatusKorban");
	}
	
	@Command
	@NotifyChange({"flagJenisTlWarning","flagCatatanJaminanLanjutan","flagJenisTlWarningMsg","flagNominalJaminanLanjutan", 
		"flagTglMasukRsJaminanLanjutan","flagRsJaminanLanjutan","flagSimpanJaminanLanjutan","flagLoketKantorJaminanLanjutan"})
	public void cekJenisTl(){
		if(jenisTlDto.getRvLowValue().equalsIgnoreCase("1")){
			if(plTlRDto.getFlagBayar().equalsIgnoreCase("0")){
				setFlagJenisTlWarning(true);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(false);
				setFlagSimpanJaminanLanjutan(false);
				setFlagJenisTlWarningMsg("Lakukan mapping penyelesaian berkas atau pengajuan sementara terlebih dahulu untuk mendapatkan saldo jaminan!");				
			}else if(plTlRDto.getFlagBayar().equalsIgnoreCase("4")&&plTlRDto.getOtorisasiAjuSmt().equalsIgnoreCase("0")){
				setFlagJenisTlWarning(true);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(false);
				setFlagSimpanJaminanLanjutan(false);
				setFlagJenisTlWarningMsg("Pengajuan Sementara belum diotorisasi !");								
			}else if(plTlRDto.getFlagBayar().equalsIgnoreCase("4")&&plTlRDto.getOtorisasiAjuSmt().equalsIgnoreCase("1")){
				setFlagJenisTlWarning(false);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(true);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(true);
				setFlagRsJaminanLanjutan(true);
				setFlagSimpanJaminanLanjutan(true);				
			}else if(plTlRDto.getFlagBayar().equalsIgnoreCase("3")){
				setFlagJenisTlWarning(true);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(false);
				setFlagSimpanJaminanLanjutan(false);
				setFlagJenisTlWarningMsg("Berkas Limpahan Masuk, silahkan Terima Limpahan terlebih dahulu !");												
			}
		}else if(jenisTlDto.getRvLowValue().equalsIgnoreCase("2")){
			if(plTlRDto.getFlagBayar().equalsIgnoreCase("0")){
				setFlagJenisTlWarning(true);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(false);
				setFlagSimpanJaminanLanjutan(false);
				setFlagJenisTlWarningMsg("Lakukan mapping penyelesaian berkas atau pengajuan sementara terlebih dahulu untuk mendapatkan saldo jaminan!");				
			}else if(plTlRDto.getFlagBayar().equalsIgnoreCase("4")&&plTlRDto.getOtorisasiAjuSmt().equalsIgnoreCase("0")){
				setFlagJenisTlWarning(true);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(false);
				setFlagSimpanJaminanLanjutan(false);
				setFlagJenisTlWarningMsg("Pengajuan Sementara belum diotorisasi !");								
			}else if(plTlRDto.getFlagBayar().equalsIgnoreCase("4")&&plTlRDto.getOtorisasiAjuSmt().equalsIgnoreCase("1")){
				setFlagJenisTlWarning(false);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(true);
				setFlagLoketKantorJaminanLanjutan(true);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(true);
				setFlagSimpanJaminanLanjutan(true);								
			}
		}else if(jenisTlDto.getRvLowValue().equalsIgnoreCase("3")){
			if(plTlRDto.getFlagBayar().equalsIgnoreCase("4")){
				setFlagJenisTlWarning(true);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(false);
				setFlagSimpanJaminanLanjutan(false);								
				setFlagJenisTlWarningMsg("Telah ada pengajuan sementara, Batalkan terlebih dahulu!");
			}else if(plTlRDto.getFlagBayar().equalsIgnoreCase("3")){
				setFlagJenisTlWarning(true);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(false);
				setFlagSimpanJaminanLanjutan(false);								
				setFlagJenisTlWarningMsg("Berkas telah diterima loket lain, tidak bisa dibatalkan!");				
			}else{
				setFlagJenisTlWarning(false);
				setFlagCatatanJaminanLanjutan(true);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(true);
				setFlagSimpanJaminanLanjutan(false);								
			}
		}else if(jenisTlDto.getRvLowValue().equalsIgnoreCase("4")){
			if(plTlRDto.getFlagBayar().equalsIgnoreCase("3")){
				setFlagJenisTlWarning(false);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(true);
				setFlagSimpanJaminanLanjutan(false);								
			}else{
				setFlagJenisTlWarning(true);
				setFlagCatatanJaminanLanjutan(false);
				setFlagNominalJaminanLanjutan(false);
				setFlagLoketKantorJaminanLanjutan(false);
				setFlagTglMasukRsJaminanLanjutan(false);
				setFlagRsJaminanLanjutan(false);
				setFlagSimpanJaminanLanjutan(false);								
				setFlagJenisTlWarningMsg("Bukan merupakan berkas limpahan Masuk!");										
			}
		}
	}
	
	public void cekPengajuanSementara(){
		if(plTlRDto.getJenisTagihan().equalsIgnoreCase("0")){
			setFlagDisableSimpanPengajuan(false);
			BindUtils.postNotifyChange(null, null, this, "flagDisableSimpanPengajuan");
		}else{
			setFlagDisableSimpanPengajuan(true);
			BindUtils.postNotifyChange(null, null, this, "flagDisableSimpanPengajuan");
		}
	}
		
	public String getSearchRS() {
		return searchRS;
	}

	public void setSearchRS(String searchRS) {
		this.searchRS = searchRS;
	}

	public List<PlRumahSakitDto> getListRS() {
		return listRS;
	}

	public void setListRS(List<PlRumahSakitDto> listRS) {
		this.listRS = listRS;
	}


	public Date getDateAwal() {
		return dateAwal;
	}

	public void setDateAwal(Date dateAwal) {
		this.dateAwal = dateAwal;
	}

	public Date getDateAkhir() {
		return dateAkhir;
	}

	public void setDateAkhir(Date dateAkhir) {
		this.dateAkhir = dateAkhir;
	}

	public String getStatusJaminan() {
		return statusJaminan;
	}

	public void setStatusJaminan(String statusJaminan) {
		this.statusJaminan = statusJaminan;
	}

	public String getJenisTgl() {
		return jenisTgl;
	}

	public void setJenisTgl(String jenisTgl) {
		this.jenisTgl = jenisTgl;
	}


	public boolean isListIndex() {
		return listIndex;
	}

	public void setListIndex(boolean listIndex) {
		this.listIndex = listIndex;
	}

	public int getPagesize() {
		return pagesize;
	}

	public void setPagesize(int pagesize) {
		this.pagesize = pagesize;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public List<DasiJrRefCodeDto> getListKodeJenisPembayaran() {
		return listKodeJenisPembayaran;
	}

	public void setListKodeJenisPembayaran(
			List<DasiJrRefCodeDto> listKodeJenisPembayaran) {
		this.listKodeJenisPembayaran = listKodeJenisPembayaran;
	}

	public DasiJrRefCodeDto getKodeJenisPembayaran() {
		return kodeJenisPembayaran;
	}

	public void setKodeJenisPembayaran(DasiJrRefCodeDto kodeJenisPembayaran) {
		this.kodeJenisPembayaran = kodeJenisPembayaran;
	}

	public PlTlRDto getJaminanAll() {
		return jaminanAll;
	}

	public void setJaminanAll(PlTlRDto jaminanAll) {
		this.jaminanAll = jaminanAll;
	}

	public PlTlRDto getJaminanBayar() {
		return jaminanBayar;
	}

	public void setJaminanBayar(PlTlRDto jaminanBayar) {
		this.jaminanBayar = jaminanBayar;
	}

	public PlTlRDto getJaminanHutang() {
		return jaminanHutang;
	}

	public void setJaminanHutang(PlTlRDto jaminanHutang) {
		this.jaminanHutang = jaminanHutang;
	}

	public PlTlRDto getJaminanBatal() {
		return jaminanBatal;
	}

	public void setJaminanBatal(PlTlRDto jaminanBatal) {
		this.jaminanBatal = jaminanBatal;
	}

	public List<PlTlRDto> getListJaminanAll() {
		return listJaminanAll;
	}

	public void setListJaminanAll(List<PlTlRDto> listJaminanAll) {
		this.listJaminanAll = listJaminanAll;
	}

	public List<PlTlRDto> getListJaminanBayar() {
		return listJaminanBayar;
	}

	public void setListJaminanBayar(List<PlTlRDto> listJaminanBayar) {
		this.listJaminanBayar = listJaminanBayar;
	}

	public List<PlTlRDto> getListJaminanHutang() {
		return listJaminanHutang;
	}

	public void setListJaminanHutang(List<PlTlRDto> listJaminanHutang) {
		this.listJaminanHutang = listJaminanHutang;
	}

	public List<PlTlRDto> getListJaminanBatal() {
		return listJaminanBatal;
	}

	public void setListJaminanBatal(List<PlTlRDto> listJaminanBatal) {
		this.listJaminanBatal = listJaminanBatal;
	}

	public List<PlTlRDto> getListIndexDtoCopy() {
		return listIndexDtoCopy;
	}

	public void setListIndexDtoCopy(List<PlTlRDto> listIndexDtoCopy) {
		this.listIndexDtoCopy = listIndexDtoCopy;
	}

	public void setListIndexDto(List<PlTlRDto> listIndexDto) {
		this.listIndexDto = listIndexDto;
	}

	public PlRumahSakitDto getRumahSakitDto() {
		return rumahSakitDto;
	}

	public void setRumahSakitDto(PlRumahSakitDto rumahSakitDto) {
		this.rumahSakitDto = rumahSakitDto;
	}

	public boolean isFlagBuatJaminanLanjutan() {
		return flagBuatJaminanLanjutan;
	}

	public void setFlagBuatJaminanLanjutan(boolean flagBuatJaminanLanjutan) {
		this.flagBuatJaminanLanjutan = flagBuatJaminanLanjutan;
	}

	public List<PlDataKecelakaanDto> getListPlDataKecelakaanDtos() {
		return listPlDataKecelakaanDtos;
	}

	public void setListPlDataKecelakaanDtos(
			List<PlDataKecelakaanDto> listPlDataKecelakaanDtos) {
		this.listPlDataKecelakaanDtos = listPlDataKecelakaanDtos;
	}

	public PlDataKecelakaanDto getPlDataKecelakaanDto() {
		return plDataKecelakaanDto;
	}

	public void setPlDataKecelakaanDto(PlDataKecelakaanDto plDataKecelakaanDto) {
		this.plDataKecelakaanDto = plDataKecelakaanDto;
	}

	public List<PlKorbanKecelakaanDto> getListKorban() {
		return listKorban;
	}

	public void setListKorban(List<PlKorbanKecelakaanDto> listKorban) {
		this.listKorban = listKorban;
	}

	public PlKorbanKecelakaanDto getPlKorbanKecelakaanDto() {
		return plKorbanKecelakaanDto;
	}

	public void setPlKorbanKecelakaanDto(PlKorbanKecelakaanDto plKorbanKecelakaanDto) {
		this.plKorbanKecelakaanDto = plKorbanKecelakaanDto;
	}

	public List<PlTlRDto> getListIndexDto() {
		return listIndexDto;
	}
	public List<FndLokasiDto> getFndLokasiDtos() {
		return fndLokasiDtos;
	}

	public void setFndLokasiDtos(List<FndLokasiDto> fndLokasiDtos) {
		this.fndLokasiDtos = fndLokasiDtos;
	}

	public PlTlRDto getPlTlRDto() {
		return plTlRDto;
	}

	public void setPlTlRDto(PlTlRDto plTlRDto) {
		this.plTlRDto = plTlRDto;
	}

	public List<PlTlRDto> getPlTlRDtos() {
		return plTlRDtos;
	}

	public void setPlTlRDtos(List<PlTlRDto> plTlRDtos) {
		this.plTlRDtos = plTlRDtos;
	}

	public boolean isFlagOtorisasiJaminan() {
		return flagOtorisasiJaminan;
	}

	public void setFlagOtorisasiJaminan(boolean flagOtorisasiJaminan) {
		this.flagOtorisasiJaminan = flagOtorisasiJaminan;
	}

	public Date getTglJaminan() {
		return tglJaminan;
	}

	public void setTglJaminan(Date tglJaminan) {
		this.tglJaminan = tglJaminan;
	}

	public List<String> getListJenisOtorisasi() {
		return listJenisOtorisasi;
	}

	public void setListJenisOtorisasi(List<String> listJenisOtorisasi) {
		this.listJenisOtorisasi = listJenisOtorisasi;
	}

	public List<String> getListOtorisasi() {
		return listOtorisasi;
	}

	public void setListOtorisasi(List<String> listOtorisasi) {
		this.listOtorisasi = listOtorisasi;
	}

	public String getJenisOtorisasi() {
		return jenisOtorisasi;
	}

	public void setJenisOtorisasi(String jenisOtorisasi) {
		this.jenisOtorisasi = jenisOtorisasi;
	}

	public String getOtorisasi() {
		return otorisasi;
	}

	public void setOtorisasi(String otorisasi) {
		this.otorisasi = otorisasi;
	}

	public boolean isFlagOtorisasiWarning() {
		return flagOtorisasiWarning;
	}

	public void setFlagOtorisasiWarning(boolean flagOtorisasiWarning) {
		this.flagOtorisasiWarning = flagOtorisasiWarning;
	}

	public String getStatusOtorisasi() {
		return statusOtorisasi;
	}

	public void setStatusOtorisasi(String statusOtorisasi) {
		this.statusOtorisasi = statusOtorisasi;
	}

	public boolean isFlagOtorisasiView() {
		return flagOtorisasiView;
	}

	public void setFlagOtorisasiView(boolean flagOtorisasiView) {
		this.flagOtorisasiView = flagOtorisasiView;
	}

	public boolean isFlagButtonSimpanOtorisasi() {
		return flagButtonSimpanOtorisasi;
	}

	public void setFlagButtonSimpanOtorisasi(boolean flagButtonSimpanOtorisasi) {
		this.flagButtonSimpanOtorisasi = flagButtonSimpanOtorisasi;
	}

	public boolean isFlagButtonBatalOtorisasi() {
		return flagButtonBatalOtorisasi;
	}

	public void setFlagButtonBatalOtorisasi(boolean flagButtonBatalOtorisasi) {
		this.flagButtonBatalOtorisasi = flagButtonBatalOtorisasi;
	}

	public boolean isFlagEditJaminan() {
		return flagEditJaminan;
	}

	public void setFlagEditJaminan(boolean flagEditJaminan) {
		this.flagEditJaminan = flagEditJaminan;
	}

	public boolean isFlagSimpanJaminan() {
		return flagSimpanJaminan;
	}

	public void setFlagSimpanJaminan(boolean flagSimpanJaminan) {
		this.flagSimpanJaminan = flagSimpanJaminan;
	}

	public List<String> getListStatusKorban() {
		return listStatusKorban;
	}

	public void setListStatusKorban(List<String> listStatusKorban) {
		this.listStatusKorban = listStatusKorban;
	}

	public String getStatusKorban() {
		return statusKorban;
	}

	public void setStatusKorban(String statusKorban) {
		this.statusKorban = statusKorban;
	}

	public List<DasiJrRefCodeDto> getListJenisTlDto() {
		return listJenisTlDto;
	}

	public void setListJenisTlDto(List<DasiJrRefCodeDto> listJenisTlDto) {
		this.listJenisTlDto = listJenisTlDto;
	}

	public DasiJrRefCodeDto getJenisTlDto() {
		return jenisTlDto;
	}

	public void setJenisTlDto(DasiJrRefCodeDto jenisTlDto) {
		this.jenisTlDto = jenisTlDto;
	}

	public boolean isFlagJenisTlWarning() {
		return flagJenisTlWarning;
	}

	public void setFlagJenisTlWarning(boolean flagJenisTlWarning) {
		this.flagJenisTlWarning = flagJenisTlWarning;
	}

	public String getFlagJenisTlWarningMsg() {
		return flagJenisTlWarningMsg;
	}

	public void setFlagJenisTlWarningMsg(String flagJenisTlWarningMsg) {
		this.flagJenisTlWarningMsg = flagJenisTlWarningMsg;
	}

	public boolean isFlagDisableSimpanPengajuan() {
		return flagDisableSimpanPengajuan;
	}

	public void setFlagDisableSimpanPengajuan(boolean flagDisableSimpanPengajuan) {
		this.flagDisableSimpanPengajuan = flagDisableSimpanPengajuan;
	}

	public Date getTanggalTl() {
		return tanggalTl;
	}

	public void setTanggalTl(Date tanggalTl) {
		this.tanggalTl = tanggalTl;
	}

	public List<FndKantorJasaraharjaDto> getListLoketKantor() {
		return listLoketKantor;
	}

	public void setListLoketKantor(List<FndKantorJasaraharjaDto> listLoketKantor) {
		this.listLoketKantor = listLoketKantor;
	}

	public String getSearchLoketKantor() {
		return searchLoketKantor;
	}

	public void setSearchLoketKantor(String searchLoketKantor) {
		this.searchLoketKantor = searchLoketKantor;
	}

	public FndKantorJasaraharjaDto getLoketKantorDto() {
		return loketKantorDto;
	}

	public void setLoketKantorDto(FndKantorJasaraharjaDto loketKantorDto) {
		this.loketKantorDto = loketKantorDto;
	}

	public boolean isFlagRsJaminanLanjutan() {
		return flagRsJaminanLanjutan;
	}

	public void setFlagRsJaminanLanjutan(boolean flagRsJaminanLanjutan) {
		this.flagRsJaminanLanjutan = flagRsJaminanLanjutan;
	}

	public boolean isFlagNominalJaminanLanjutan() {
		return flagNominalJaminanLanjutan;
	}

	public void setFlagNominalJaminanLanjutan(boolean flagNominalJaminanLanjutan) {
		this.flagNominalJaminanLanjutan = flagNominalJaminanLanjutan;
	}

	public boolean isFlagLoketKantorJaminanLanjutan() {
		return flagLoketKantorJaminanLanjutan;
	}

	public void setFlagLoketKantorJaminanLanjutan(
			boolean flagLoketKantorJaminanLanjutan) {
		this.flagLoketKantorJaminanLanjutan = flagLoketKantorJaminanLanjutan;
	}

	public boolean isFlagTglMasukRsJaminanLanjutan() {
		return flagTglMasukRsJaminanLanjutan;
	}

	public void setFlagTglMasukRsJaminanLanjutan(
			boolean flagTglMasukRsJaminanLanjutan) {
		this.flagTglMasukRsJaminanLanjutan = flagTglMasukRsJaminanLanjutan;
	}

	public boolean isFlagCatatanJaminanLanjutan() {
		return flagCatatanJaminanLanjutan;
	}

	public void setFlagCatatanJaminanLanjutan(boolean flagCatatanJaminanLanjutan) {
		this.flagCatatanJaminanLanjutan = flagCatatanJaminanLanjutan;
	}

	public boolean isFlagSimpanJaminanLanjutan() {
		return flagSimpanJaminanLanjutan;
	}

	public void setFlagSimpanJaminanLanjutan(boolean flagSimpanJaminanLanjutan) {
		this.flagSimpanJaminanLanjutan = flagSimpanJaminanLanjutan;
	}

	public int getTotalJmlPengajuan() {
		return totalJmlPengajuan;
	}

	public void setTotalJmlPengajuan(int totalJmlPengajuan) {
		this.totalJmlPengajuan = totalJmlPengajuan;
	}

	public boolean isOtorisasiVisible() {
		return otorisasiVisible;
	}

	public void setOtorisasiVisible(boolean otorisasiVisible) {
		this.otorisasiVisible = otorisasiVisible;
	}

	public String getJaminanOption() {
		return jaminanOption;
	}

	public void setJaminanOption(String jaminanOption) {
		this.jaminanOption = jaminanOption;
	}

	public boolean isJaminanVisible() {
		return jaminanVisible;
	}

	public void setJaminanVisible(boolean jaminanVisible) {
		this.jaminanVisible = jaminanVisible;
	}
	

}
