package ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import common.model.UserSessionJR;

public class IniPopUpVmd {
	@Wire("#iniIdPopup")
	private Window winLov;
	
	private String popUpHandler;
	private UserSessionJR ujr = new UserSessionJR();
	private String id;
	private Date hari;
	private Date bulan;
	private Date tahun;
	private Date tanggal;
	
	@NotifyChange({"ujr","id"})
	@Init
	public void baseInit(
			@ExecutionArgParam("popUpHandler") String popUpHandler,
			@ExecutionArgParam("ujr") UserSessionJR ujrs,
			@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		this.popUpHandler = popUpHandler;
		ujr = ujrs;
		
		System.out.println("Base popup init called");
		
	}
	
//	public void init(
//			@ExecutionArgParam("id") String ids,
//		@ExecutionArgParam("vendorType") String vendorType,
//		@ExecutionArgParam("vendorCode") String vendorCode){
//	}
	
	@Command
	public void close(@BindingParam("window") Window win){
		if(win!=null)
		win.detach();
	}
	
	private void close(){
		if (winLov == null)
			throw new RuntimeException(
					"id popUp tidak sama dengan viewModel");
		winLov.detach();
	}
	
	@Command
	public void batal(){
		close();
	}
	
	@Command
	public void lanjut(){
		Map<String, Object> args = new HashMap<>();
		args.put("ujr", ujr);
		returnValue(args);
		}
	@NotifyChange("tanggal")
	@Command
	public void checkTanggal() throws ParseException{
		if(hari!=null && bulan != null && tahun != null){
			String hari, bulan, tahun = "";
			SimpleDateFormat sdf = new SimpleDateFormat("dd");
			hari = sdf.format(getHari());
			sdf = new SimpleDateFormat("MM");
			bulan = sdf.format(getBulan());
			sdf = new SimpleDateFormat("yyyy");
			tahun = sdf.format(getTahun());
			sdf = new SimpleDateFormat("ddMMyyyy");
			tanggal = sdf.parse(hari+bulan+tahun);
//			Calendar cal = new GregorianCalendar();
//			cal.set(tahun.getYear(), bulan.getMinutes(), hari.getDay());
//			System.out.println(tahun.getYear()+"_"+ bulan.getMinutes()+" "+ hari.getDay());
//		tanggal = cal.getTime();	
		}
		
	}
	
	protected void returnValue(Map<String, Object> args) {
		//invoke global method in parent view model and also send return value
		BindUtils.postGlobalCommand(null, null, getPopUpHandler(), args);
		close();
	}
	
	private String getPopUpHandler(){
		return popUpHandler;
	}

	public UserSessionJR getUjr() {
		return ujr;
	}

	public void setUjr(UserSessionJR ujr) {
		this.ujr = ujr;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getHari() {
		return hari;
	}

	public void setHari(Date hari) {
		this.hari = hari;
	}

	public Date getBulan() {
		return bulan;
	}

	public void setBulan(Date bulan) {
		this.bulan = bulan;
	}

	public Date getTahun() {
		return tahun;
	}

	public void setTahun(Date tahun) {
		this.tahun = tahun;
	}

	public Date getTanggal() {
		return tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

}
