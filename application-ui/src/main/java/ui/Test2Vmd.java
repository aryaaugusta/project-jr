package ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.zkoss.bind.annotation.Command;
import org.zkoss.util.media.AMedia;
import org.zkoss.zul.Filedownload;

public class Test2Vmd implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CellStyle csAllBorder = null;
	private CellStyle csBold = null;
	private CellStyle csTop = null;
	private CellStyle csVertical = null;
	private CellStyle csHorizontalCenter = null;
	Font bold = null;
	
	private String dateToString(Date in){
		SimpleDateFormat sdf = new SimpleDateFormat("DD/MM/YYYY");
		return sdf.format(in);
	}
	
	@Command("download")
	public void createFileExcel() {
		Workbook workbook = new XSSFWorkbook();
		setCellStyles(workbook);
		Sheet testExcel = workbook.createSheet("Lembar1");
		int rowIndex = 0;
		CellRangeAddress cellRange1 = new CellRangeAddress(0, 0, 0, 10);
		CellRangeAddress cellRange2 = new CellRangeAddress(1, 1, 0, 2);
		CellRangeAddress cellRange3 = new CellRangeAddress(1, 1, 3, 12);
		CellRangeAddress cellRange4 = new CellRangeAddress(2, 2, 0, 2);
		CellRangeAddress cellRange5 = new CellRangeAddress(2, 2, 3, 12);
		CellRangeAddress cellRange6 = new CellRangeAddress(3, 3, 0, 2);
		CellRangeAddress cellRange7 = new CellRangeAddress(3, 3, 3, 12);
		CellRangeAddress cellRange8 = new CellRangeAddress(5, 6, 0, 0);
		CellRangeAddress cellRange9 = new CellRangeAddress(5, 5, 1, 5);
		CellRangeAddress cellRange10 = new CellRangeAddress(5, 5, 6, 10);
		CellRangeAddress cellRange11 = new CellRangeAddress(5, 6, 11, 12);

		Row row = testExcel.createRow(rowIndex++); //1
		Cell c = null;
		c = row.createCell(0);
		c.setCellValue("Laporan Coklit Data Kecelakaan IRSMS");
		c.setCellStyle(csBold);
		testExcel.addMergedRegion(cellRange1);
		c.getCellStyle().setAlignment(HorizontalAlignment.CENTER);
		
		c = row.createCell(11);
		c.setCellValue("Tgl Cetak :");
		c = row.createCell(12);
		c.getCellStyle().setAlignment(HorizontalAlignment.RIGHT);
		c.setCellValue(dateToString(new Date()));

		row = testExcel.createRow(rowIndex++); //2
		c = row.createCell(0);
		c.setCellValue("POLDA");
		c.setCellStyle(csBold);
		c.getCellStyle().setAlignment(HorizontalAlignment.RIGHT);
		testExcel.addMergedRegion(cellRange2);
		
		c = row.createCell(3);
		c.setCellValue("Jabar");
		testExcel.addMergedRegion(cellRange3);
		
		row = testExcel.createRow(rowIndex++); //3
		c = row.createCell(0);
		c.setCellValue("POLRES");
		c.setCellStyle(csBold);
		c.getCellStyle().setAlignment(HorizontalAlignment.RIGHT);
		testExcel.addMergedRegion(cellRange4);
		
		c = row.createCell(3);
		c.setCellValue("Bandung");
		testExcel.addMergedRegion(cellRange5);
		
		row = testExcel.createRow(rowIndex++); //4
		c = row.createCell(0);
		c.setCellValue("Periode Tgl Kejadian");
		c.setCellStyle(csBold);
		c.getCellStyle().setAlignment(HorizontalAlignment.RIGHT);
		testExcel.addMergedRegion(cellRange6);
		
		c = row.createCell(3);
		c.setCellValue("27/01/2009 s.d. 13/03/2019");
		testExcel.addMergedRegion(cellRange7);

		row = testExcel.createRow(rowIndex++); //5
		c = row.createCell(0);
		
		row = testExcel.createRow(rowIndex++); //6
		c = row.createCell(0);
		c.setCellValue("No.");
		c.getCellStyle().setAlignment(HorizontalAlignment.CENTER);
		c.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
		testExcel.addMergedRegion(cellRange8);
		System.out.println(0+" : "+rowIndex);
		mergedBorder(row.getSheet(), 7);
		
		c = row.createCell(1);
		c.setCellValue("DASI-JR");
		c.getCellStyle().setAlignment(HorizontalAlignment.CENTER);
		testExcel.addMergedRegion(cellRange9);
		System.out.println(1+" : "+rowIndex);
		mergedBorder(row.getSheet(), 8);
		
		c = row.createCell(6);
		c.setCellValue("IR-SMS");
		c.getCellStyle().setAlignment(HorizontalAlignment.CENTER);
		testExcel.addMergedRegion(cellRange10);
		System.out.println(6+" : "+rowIndex);
		mergedBorder(row.getSheet(), 9);
		
		c = row.createCell(11);
		c.setCellValue("KETERANGAN");
		c.getCellStyle().setAlignment(HorizontalAlignment.CENTER);
		c.getCellStyle().setVerticalAlignment(VerticalAlignment.CENTER);
		testExcel.addMergedRegion(cellRange11);
		System.out.println(11+" : "+rowIndex);
		mergedBorder(row.getSheet(), 10);
		
		int cellIndex = 0;
		
		row = testExcel.createRow(rowIndex++); //7
		c = row.createCell(cellIndex++);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Tgl Kejadian");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("No Laporan");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Tgl Laporan");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Nama Korban");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Cidera");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Tgl Kejadian");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("No Laporan");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Tgl Laporan");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Nama Korban");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellValue("Cidera");
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellStyle(csAllBorder);
		c = row.createCell(cellIndex++);
		c.setCellStyle(csAllBorder);
		
		// DATA BELOW 
		// Start Looping Data 
		//========================================================================================================
		cellIndex = 0;
		row = testExcel.createRow(rowIndex++); //8
		c = row.createCell(cellIndex++);
		c.setCellValue("1");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue(dateToString(new Date()));
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue("LP/01/I/2016/LANTAS");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue("05/01/2016");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue("ASEP KUSAERI");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue("MD");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue(" ");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue("LP/01/I/2016/LL");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue("-");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue("-");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue("-");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellValue("Keterangan");
		c.setCellStyle(csVertical);
		c = row.createCell(cellIndex++);
		c.setCellStyle(csVertical);
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,cellIndex-2,cellIndex-1)); //12
		
		//========================================================================================================
		//end looping

		//Closing Border 
		row = testExcel.createRow(rowIndex++);
		for(int i=0; i<=cellIndex-1; i++){
			c=row.createCell(i);
			c.setCellStyle(csTop);
		}

		row = testExcel.createRow(rowIndex++);
		
		
		row = testExcel.createRow(rowIndex++);
		c=row.createCell(0);
		c.setCellValue("PT. Jasa Raharja (Persero)");
		c.setCellStyle(csHorizontalCenter);
		c.getCellStyle().setFont(bold);
		c=row.createCell(6);
		c.setCellValue("KORLANTAS POLRI");
		c.setCellStyle(csHorizontalCenter);
		c.getCellStyle().setFont(bold);
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,0,5)); //13
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,6,12)); //14
		row = testExcel.createRow(rowIndex++);
		c=row.createCell(0);
		c.setCellValue("Kantor");
		c.setCellStyle(csHorizontalCenter);
		c=row.createCell(6);
		c.setCellValue("0220000");
		c.setCellStyle(csHorizontalCenter);
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,0,5)); //15
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,6,12)); //16
		row = testExcel.createRow(rowIndex++);
		c=row.createCell(0);
		c.setCellValue("Jabatan JR");
		c.setCellStyle(csHorizontalCenter);
		c=row.createCell(6);
		c.setCellValue("Mentri Peruntukan Perindustrian Tanah");
		c.setCellStyle(csHorizontalCenter);
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,0,5)); //17
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,6,12)); //18
		
		for(int j=0;j<=3;j++){
			row = testExcel.createRow(rowIndex++);
		}
		
		row = testExcel.createRow(rowIndex++);
		c=row.createCell(0);
		c.setCellValue("Nama Pejabat JR");
		c.setCellStyle(csHorizontalCenter);
		c=row.createCell(6);
		c.setCellValue("Kyuko");
		c.setCellStyle(csHorizontalCenter);
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,0,5)); //19
		testExcel.addMergedRegion(getRange(rowIndex-1,rowIndex-1,6,12)); //20
		
		
		//Auto size column
		for (int i = 0; i <= cellIndex; i++) {
			testExcel.autoSizeColumn(i);
		}
		try {
			// Write the workbook in file system
			File temp = File.createTempFile("testExcel", ".xlsx");
			FileOutputStream out = new FileOutputStream(temp);
			workbook.write(out);
			out.close();
			InputStream fis = new FileInputStream(temp);
			Filedownload.save(new AMedia("testExcel", "xlsx", "application/file", fis));
			temp.delete();
			
			
		} catch (Exception e) {
			
		}
	}
	
	private void mergedBorder(Sheet sheet, int ...reg) {
	    int numMerged = sheet.getNumMergedRegions();
	    if(reg.length==0){
	    	for (int i = 0; i < numMerged; i++) {
	    		CellRangeAddress mergedRegions = sheet.getMergedRegion(i);
	    		RegionUtil.setBorderLeft(BorderStyle.THIN, mergedRegions, sheet);
	    		RegionUtil.setBorderRight(BorderStyle.THIN, mergedRegions, sheet);
	    		RegionUtil.setBorderTop(BorderStyle.THIN, mergedRegions, sheet);
	    		RegionUtil.setBorderBottom(BorderStyle.THIN, mergedRegions, sheet);
	    	}
	    }else if(reg.length==1){
	    	CellRangeAddress mergedRegions = sheet.getMergedRegion(reg[0]);
	    	RegionUtil.setBorderLeft(BorderStyle.THIN, mergedRegions, sheet);
	    	RegionUtil.setBorderRight(BorderStyle.THIN, mergedRegions, sheet);
	    	RegionUtil.setBorderTop(BorderStyle.THIN, mergedRegions, sheet);
	    	RegionUtil.setBorderBottom(BorderStyle.THIN, mergedRegions, sheet);
	    }
	}
	
	private CellRangeAddress getRange(int row1, int row2, int col1, int col2){
		return new CellRangeAddress(row1, row2, col1, col2);
	}
	
	private void setCellStyles(Workbook wb) {

		// Bold Font
		bold = wb.createFont();
		bold.setBold(true);
		bold.setFontHeightInPoints((short) 11);

		// AllBorder
		csAllBorder = wb.createCellStyle();
		csAllBorder.setBorderTop(BorderStyle.THIN);
		csAllBorder.setBorderLeft(BorderStyle.THIN);
		csAllBorder.setBorderBottom(BorderStyle.THIN);
		csAllBorder.setBorderRight(BorderStyle.THIN);
		csAllBorder.setFont(bold);
		// Bold style
		csBold = wb.createCellStyle();
		csBold.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		csBold.setFont(bold);
		//Vertical Border Left and Right
		csVertical = wb.createCellStyle();
		csVertical.setBorderLeft(BorderStyle.THIN);
		csVertical.setBorderRight(BorderStyle.THIN);
		//Top Border
		csTop = wb.createCellStyle();
		csTop.setBorderTop(BorderStyle.THIN);
		//Normal
		csHorizontalCenter = wb.createCellStyle();
		csHorizontalCenter.setAlignment(HorizontalAlignment.CENTER);
		

	}
	
}
