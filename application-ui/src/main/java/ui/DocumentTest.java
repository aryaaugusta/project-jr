package ui;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.*;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTP;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTabStop;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STTabJc;

import common.util.JsonUtil;

import java.math.BigInteger;

public class DocumentTest {

	public static void main(String[] args) throws Exception {

		XWPFDocument doc = new XWPFDocument();

		// the body content
		XWPFParagraph paragraph = doc.createParagraph();
		XWPFRun run = paragraph.createRun();
		run.setText("The Body:");

		paragraph = doc.createParagraph();
		run = paragraph.createRun();
		run.setText("Lorem ipsum....");

		// create header start
		CTSectPr sectPr = doc.getDocument().getBody().addNewSectPr();
		XWPFHeaderFooterPolicy headerFooterPolicy = new XWPFHeaderFooterPolicy(
				doc, sectPr);

		XWPFHeader header = headerFooterPolicy
				.createHeader(XWPFHeaderFooterPolicy.DEFAULT);
		System.out.println(header.getParagraphArray(0));
		System.out.println(paragraph.toString());
//		System.out.println(JsonUtil.getJson(paragraph));
		XWPFParagraph paragraph2 = header.getParagraphArray(0);
		System.out.println(paragraph2.toString());
		try {
			paragraph2.getAlignment();
			paragraph2.setAlignment(ParagraphAlignment.LEFT);
		} catch (Exception e) {
			e.printStackTrace();
		}
		CTPPr ctppr = null;
		try{
			ctppr = paragraph2.getCTP().getPPr();
		}catch(Exception s){
			ctppr = paragraph.getCTP().addNewPPr();
			s.printStackTrace();
		}
		CTTabStop tabStop = ctppr.addNewTabs().addNewTab();
		tabStop.setVal(STTabJc.RIGHT);
		int twipsPerInch = 1440;
		tabStop.setPos(BigInteger.valueOf(6 * twipsPerInch));

		run = paragraph2.createRun();
		run.setText("The Header:");
		run.addTab();

		run = paragraph2.createRun();
		String imgFile = "/images/LOGO-POLRI-s.png";
		// String imgFile="Koala.png";
		run.addPicture(new FileInputStream(imgFile),
				XWPFDocument.PICTURE_TYPE_PNG, imgFile, Units.toEMU(50),
				Units.toEMU(50));

		// create footer start
		XWPFFooter footer = headerFooterPolicy
				.createFooter(XWPFHeaderFooterPolicy.DEFAULT);

		paragraph2 = footer.getParagraphArray(0);
		paragraph2.setAlignment(ParagraphAlignment.CENTER);

		run = paragraph2.createRun();
		run.setText("The Footer:");

		doc.write(new FileOutputStream("test.docx"));

	}
}