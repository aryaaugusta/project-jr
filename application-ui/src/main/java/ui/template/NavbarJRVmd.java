package ui.template;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.image.AImage;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Executions;


import org.zkoss.zk.ui.UiException;
import org.zkoss.zul.Window;

import Bpm.ListTaskByUser.UserTaskService;
import Bpm.ListTaskByUser.UserTask;
import share.FndBankDto;
import share.PlDataKecelakaanDto;
import share.common.LoginData;
import share.common.UserMenuDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.PageInfo;
import common.ui.UIConstants;
import common.util.JsonUtil;

/**
 * @author Bayu Adi
 *
 */
public class NavbarJRVmd extends BaseVmd implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private AImage companyImage;
	private String companyName;
	private Date currentDate = new Date();
	private String userName;
	private UserSessionJR userSession;
	private UserMenuDto menu = new UserMenuDto();
	private List<UserMenuDto> listMenu = new ArrayList<>();
	private String namaUserKantor;
	
	//For List Task
	private List<UserTask> taskList = new ArrayList<>();
	private UserTask task = new UserTask();
	private int jumlahTask;
	
	
	
	public int getJumlahTask() {
		return jumlahTask;
	}

	public void setJumlahTask(int jumlahTask) {
		this.jumlahTask = jumlahTask;
	}

	public UserTask getTask() {
		return task;
	}

	public void setTask(UserTask task) {
		this.task = task;
	}

	public List<UserTask> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<UserTask> taskList) {
		this.taskList = taskList;
	}

	//For Use in menu
	private	boolean	pel	=	false;
	private	boolean	pel_absah_daftar	=	false;
	private	boolean	pel_hapus_pengajuan	=	false;
	private	boolean	pel_hapus_register	=	false;
	private	boolean	pel_insidentil	=	false;
	private	boolean	pel_kabag_daftar	=	false;
	private	boolean	pel_kasubag_daftar	=	false;
	private	boolean	pel_laka_daftar	=	false;
	private	boolean	pel_nik_cetak	=	false;
	private	boolean	pel_operasional	=	false;
	private	boolean	pel_otorisasi_daftar	=	false;
	private	boolean	pel_penyelesaian_daftar	=	false;
	private	boolean	pel_register_daftar	=	false;
	private	boolean	pel_request_perubahan	=	false;
	private	boolean	pel_santunan_daftar	=	false;
	private	boolean	pel_selesai_pengajuan	=	false;
	private	boolean	pel_tutup_periode	=	false;
	private	boolean	pl_mst	=	false;
	private	boolean	pl_mst_bank	=	false;
	private	boolean	pl_mst_instansi	=	false;
	private	boolean	pl_mst_lokasi	=	false;
	private	boolean	pl_mst_picrs	=	false;
	private	boolean	pl_mst_rs	=	false;
	private	boolean	sw_ews	=	false;
	private	boolean	pel_monitoring_limpahan	=	false;
	private	boolean	pel_mon_rs	=	false;
	private	boolean	pel_mon_irsms	=	false;
	private	boolean	pel_monitoring	=	false;
	private	boolean	laporan	=	false;
	
	private	boolean	authenticate	=	false;
	

	@Init
	@NotifyChange({"authenticate","pel_absah_daftar","pel_hapus_pengajuan","pel_hapus_register",
		"pel_insidentil","pel_kabag_daftar","pel_kabag_daftar","pel_kasubag_daftar",
		"pel_laka_daftar","pel_nik_cetak","pel_operasional","pel_otorisasi_daftar",
		"pel_penyelesaian_daftar","pel_register_daftar","pel_request_perubahan","pel_request_perubahan",
		"pel_santunan_daftar","pel_selesai_pengajuan","pel_tutup_periode","pl_mst_bank",
		"pl_mst_instansi", "pl_mst_lokasi","pl_mst_picrs","pl_mst_rs","pel_monitoring_limpahan",
		"pel_mon_rs","pel_mon_irsms","namaUserKantor","taskList"})
	public void initialize(){
		try{
			userSession = getCurrentUserSessionJR();
			String login = userSession.getLoginID();
			LoginData loginD = new LoginData();
			loginD.setLogin(login);
			RestResponse rest = callCustomWs("/auth/getMenuByLogin/"+login, loginD, HttpMethod.POST);
			listMenu = JsonUtil.mapJsonToListObject(rest.getContents(), UserMenuDto.class);
			namaUserKantor = userSession.getLoginDesc()+"-"+userSession.getNamaKantor();
			
			//untuk get task berdasarkan user login
			System.out.println("USER LOGIN : " + userSession.getUserLdap());
			try{
			taskList = UserTaskService.getUserTasksByUser(Labels.getLabel("userBPM"), Labels.getLabel("passBPM"), userSession.getUserLdap());
			
			jumlahTask = taskList.size();
			} catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("LIST MENU");
			System.out.println(JsonUtil.getJson(listMenu));
			if(listMenu != null && listMenu.size()>0){
				authenticate = true;
			}
			compareListMenu();
			
		}catch (Exception s){
			s.printStackTrace();
		}
		Executions.getCurrent().getDesktop() .setAttribute("pageInfo", new PageInfo());
		BindUtils.postNotifyChange(null, null, this, "pel" );
		BindUtils.postNotifyChange(null, null, this, "pel_absah_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_hapus_pengajuan" );
		BindUtils.postNotifyChange(null, null, this, "pel_hapus_register" );
		BindUtils.postNotifyChange(null, null, this, "pel_insidentil" );
		BindUtils.postNotifyChange(null, null, this, "pel_kabag_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_kasubag_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_laka_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_nik_cetak" );
		BindUtils.postNotifyChange(null, null, this, "pel_operasional" );
		BindUtils.postNotifyChange(null, null, this, "pel_otorisasi_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_penyelesaian_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_register_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_request_perubahan" );
		BindUtils.postNotifyChange(null, null, this, "pel_santunan_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_selesai_pengajuan" );
		BindUtils.postNotifyChange(null, null, this, "pel_tutup_periode" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_bank" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_instansi" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_lokasi" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_picrs" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_rs" );
		BindUtils.postNotifyChange(null, null, this, "sw_ews" );
		BindUtils.postNotifyChange(null, null, this, "pel_monitoring_limpahan" );
		BindUtils.postNotifyChange(null, null, this, "pel_mon_rs" );
		BindUtils.postNotifyChange(null, null, this, "pel_mon_irsms" );
		BindUtils.postNotifyChange(null, null, this, "authenticate" );
	}
	
	@Command
	public void startTask(@BindingParam("item") UserTask task){
		System.out.println("ID Kecelakaan Task : " + task.getNomorPermohonan());
		System.out.println("User Task Number : " + task.getTaskNumber().toString());
		System.out.println("Task title : " + task.getOtherInfo1());
		List<PlDataKecelakaanDto> dataLaka = new ArrayList<PlDataKecelakaanDto>();
		//dataLaka.get(0).setIdKecelakaan(task.getNomorPermohonan());
		
//		try {
//			InsertTaskBpmSvcImpl.insert(task.getNomorPermohonan(), task.getKodeCabang(), task.getCreatedBy(), task.getCreationDate(), task.getLastUpdatedBy(), task.getLastUpdateDate(), task.getIdGUID(), task.getPengajuanType(), task.getKodeKantor(), task.getTaskTitle(), task.getTaskNumber().toString(), "", "", "");
//		} catch (RemoteException e) {
//			// TODO Auto-generated catch block
//			System.out.println("Data tidak berhasil di insert");
//			e.printStackTrace();
//		}		
		
		
		if(task.getTaskTitle().equalsIgnoreCase("Entry Data Pengajuan")){
//			HashMap<String, Object> map = new HashMap<>();
//			System.out.println("Nomor Permohonan ID Kecelakaan : " + task.getNomorPermohonan());
//			map.put("kejadianStartDate",null);
//			map.put("kejadianEndDate",null);
//			map.put("namaKantorInstansi",null);
//			map.put("laporStartDate",null);
//			map.put("laporEndDate",null);
//			map.put("noLaporPolisi",null);
//			map.put("namaKorbanLaka",null);
//			map.put("kodeJaminan",null);
//			map.put("statusLP",null);
//			map.put("search",null);
//			map.put("idKorban",null);
//			map.put("tipe","Y");
//			map.put("idKecelakaan", task.getNomorPermohonan());
//			RestResponse dataKecelakaan = callWs("/OperasionalRegisterSementara/dataLaka", map,
//					HttpMethod.POST);
//			try {
//				dataLaka = JsonUtil.mapJsonToListObject(dataKecelakaan.getContents(), PlDataKecelakaanDto.class);
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			System.out.println("ID Kecelakaan : " + dataLaka.get(0).getIdKecelakaan());
//			Executions.getCurrent().setAttribute("obj2", dataLaka.get(0));
//			getPageInfo().setAddDetailMode(true);
//			//pageInfo.setTitle("Operasional Pengajuan Santunan");
//			
//			navigate(UIConstants.BASE_PAGE_PATH + "/operasionalDetail/DataPengajuanSantunan.zul");
			System.out.println("Nomer Berkas: " + task.getOtherInfo3());
			Executions.getCurrent().setAttribute("obj2", task);
			PageInfo pageInfo = getPageInfo();
			pageInfo.setTitle(task.getOtherInfo1());
			navigate("");
			getPageInfo().setListMode(true);
			navigate(UIConstants.BASE_PAGE_PATH + "/operasional/OpPengajuanSantunan/_index.zul");
		}else if(task.getTaskTitle().equalsIgnoreCase("Identifikasi Kelengkapan dan Keabsahan Berkas")){
			System.out.println("Nomer Berkas: " + task.getOtherInfo3());
			Executions.getCurrent().setAttribute("obj2", task);
			PageInfo pageInfo = getPageInfo();
			pageInfo.setTitle(task.getOtherInfo1());
			navigate("");
			getPageInfo().setListMode(true);
			navigate(UIConstants.BASE_PAGE_PATH + "/operasional/OpIndentifikasiKeabsahanBerkas/_index.zul");
		}else if(task.getTaskTitle().equalsIgnoreCase("Entry Otorisasi Pengajuan Berkas")){
			System.out.println("Nomer Berkas: " + task.getOtherInfo3());
			Executions.getCurrent().setAttribute("obj2", task);
			PageInfo pageInfo = getPageInfo();
			pageInfo.setTitle(task.getOtherInfo1());
			navigate("");
			getPageInfo().setListMode(true);
			navigate(UIConstants.BASE_PAGE_PATH + "/operasional/OpOtorisasi/_index.zul");
		}else if(task.getTaskTitle().equalsIgnoreCase("Entry Penyelesaian Berkas")){
			System.out.println("Nomer Berkas: " + task.getOtherInfo3());
			Executions.getCurrent().setAttribute("obj2", task);
			PageInfo pageInfo = getPageInfo();
			pageInfo.setTitle(task.getOtherInfo1());
			navigate("");
			getPageInfo().setListMode(true);
			navigate(UIConstants.BASE_PAGE_PATH + "/operasional/OpPenyelesaianPengajuan/_index.zul");
		}else if(task.getTaskTitle().equalsIgnoreCase("Entry Verifikasi Berkas")){
			System.out.println("Nomer Berkas: " + task.getOtherInfo3());
			Executions.getCurrent().setAttribute("obj2", task);
			PageInfo pageInfo = getPageInfo();
			pageInfo.setTitle(task.getOtherInfo1());
			navigate("");
			getPageInfo().setListMode(true);
			navigate(UIConstants.BASE_PAGE_PATH + "/operasional/OpVerifikasi/_index.zul");
		}else if(task.getTaskTitle().equalsIgnoreCase("Entry Pengesahan Berkas")){
			System.out.println("Nomer Berkas: " + task.getOtherInfo3());
			Executions.getCurrent().setAttribute("obj2", task);
			PageInfo pageInfo = getPageInfo();
			pageInfo.setTitle(task.getOtherInfo1());
			navigate("");
			getPageInfo().setListMode(true);
			navigate(UIConstants.BASE_PAGE_PATH + "/operasional/OpPengesahan/_index.zul");
		}
		
	}
	
	
	
	@Command
	public void navi(@BindingParam("uri") String locationUri,
			@BindingParam("title") String title,
			@BindingParam("menuCode") String menuCode) {
		
//		Map<String, Object> mapInput = new HashMap<String, Object>();
//		mapInput.put("menuCode", menuCode);
//		mapInput.put("roleCode", getCurrentUserSessionJR().getKantor());
//		
//		RestResponse rest = callCustomWs("/template/getUserButton", mapInput, HttpMethod.POST);
//		List<String> dtos = new ArrayList<String>();
//		try {
//			dtos = JsonUtil.mapJsonToListObject(rest.getContents(), String.class);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		PageInfo pageInfo = getPageInfo();
//		pageInfo.getUserButton().clear();
//		pageInfo.getMenuButton().clear();
		
//		for (String s : dtos) {
//			pageInfo.getMenuButton().put(s.toLowerCase(), true);
//			pageInfo.getUserButton().put(s.toLowerCase(), true);
//		}
		pageInfo.setTitle(title);
		getPageInfo().setListMode(true);
		navigate(UIConstants.BASE_PAGE_PATH + locationUri);
	}
	
	/**
	 * @param ListUserMenu 
	 * @param search
	 * @return boolean
	 */
	private boolean findInList(final List<UserMenuDto> list, final String search) {
//	    return list.stream().filter(p -> p.getMenuID().equals(search)).findAny();
	    
	    for(UserMenuDto u : list){
	    	if(u.getMenuID().equalsIgnoreCase(search)){
	    		return true;
	    	}
	    }
	    
	    return false;
	    
	}
	
	@Command
	public void openCekKK(@BindingParam("uri") String popup){
		Map<String, Object> args = new HashMap<>();
		getPageInfo().setListMode(true);
		
		try {
			System.out.println(popup);
			((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
			BindUtils.postGlobalCommand(null, null, "getDocsAbsah", args);
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	
	private void compareListMenu(){
		
		if(findInList(listMenu, "PL.MST")){
			pl_mst = true;
		}

		if(findInList(listMenu, "PL.MST.BANK")){
			pl_mst_bank = true;
		}

		if(findInList(listMenu, "PL.MST.INSTANSI")){
			pl_mst_instansi = true;
		}

		if(findInList(listMenu, "PL.MST.LOKASI")){
			pl_mst_lokasi = true;
		}

		if(findInList(listMenu, "PL.MST.PICRS")){
			pl_mst_picrs = true;
		}

		if(findInList(listMenu, "PL.MST.RS")){
			pl_mst_rs = true;
		}

		if(findInList(listMenu, "SW.EWS")){
			sw_ews = true;
		}
		
		if (findInList(listMenu, "PEL.PENYELESAIAN.DAFTAR")){
			pel_penyelesaian_daftar = true;
		}
		
		if(findInList(listMenu, "PEL.ABSAH.DAFTAR")){
			pel_absah_daftar = true;
		}

		if(findInList(listMenu, "PEL.HAPUS.PENGAJUAN")){
			pel_hapus_pengajuan = true;
		}

		if(findInList(listMenu, "PEL.HAPUS.REGISTER")){
			pel_hapus_register = true;
		}

		if(findInList(listMenu, "PEL.INSIDENTIL")){
			pel_insidentil = true;
		}

		if(findInList(listMenu, "PEL.KABAG.DAFTAR")){
			pel_kabag_daftar = true;
		}

		if(findInList(listMenu, "PEL.KASUBAG.DAFTAR")){
			pel_kasubag_daftar = true;
		}

		if(findInList(listMenu, "PEL.LAKA.DAFTAR")){
			pel_laka_daftar = true;
		}

		if(findInList(listMenu, "PEL.NIK.CETAK")){
			pel_nik_cetak = true;
		}

		if(findInList(listMenu, "PEL.OPERASIONAL")){
			pel_operasional = true;
		}

		if(findInList(listMenu, "PEL.OTORISASI.DAFTAR")){
			pel_otorisasi_daftar = true;
		}

		if(findInList(listMenu, "PEL.REGISTER.DAFTAR")){
			pel_register_daftar = true;
		}

		if(findInList(listMenu, "PEL.REQUEST.PERUBAHAN")){
			pel_request_perubahan = true;
		}

		if(findInList(listMenu, "PEL.SANTUNAN.DAFTAR")){
			pel_santunan_daftar = true;
		}

		if(findInList(listMenu, "PEL.SELESAI.PENGAJUAN")){
			pel_selesai_pengajuan = true;
		}

		if(findInList(listMenu, "PEL.TUTUP.PERIODE")){
			pel_tutup_periode = true;
		}

		if(findInList(listMenu, "PEL.MONITORING.LIMPAHAN")){
			pel_monitoring_limpahan = true;
			pel_monitoring = true;
		}

		if(findInList(listMenu, "PEL.MON.RS")){
			pel_mon_rs = true;
			pel_monitoring = true;
		}

		if(findInList(listMenu, "PEL.MON.IRSMS")){
			pel_mon_irsms = true;
			pel_monitoring = true;
		}
		
		laporan = true;
		
		BindUtils.postNotifyChange(null, null, this, "pel" );
		BindUtils.postNotifyChange(null, null, this, "pel_absah_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_hapus_pengajuan" );
		BindUtils.postNotifyChange(null, null, this, "pel_hapus_register" );
		BindUtils.postNotifyChange(null, null, this, "pel_insidentil" );
		BindUtils.postNotifyChange(null, null, this, "pel_kabag_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_kasubag_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_laka_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_nik_cetak" );
		BindUtils.postNotifyChange(null, null, this, "pel_operasional" );
		BindUtils.postNotifyChange(null, null, this, "pel_otorisasi_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_penyelesaian_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_register_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_request_perubahan" );
		BindUtils.postNotifyChange(null, null, this, "pel_santunan_daftar" );
		BindUtils.postNotifyChange(null, null, this, "pel_selesai_pengajuan" );
		BindUtils.postNotifyChange(null, null, this, "pel_tutup_periode" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_bank" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_instansi" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_lokasi" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_picrs" );
		BindUtils.postNotifyChange(null, null, this, "pl_mst_rs" );
		BindUtils.postNotifyChange(null, null, this, "sw_ews" );
		BindUtils.postNotifyChange(null, null, this, "pel_monitoring_limpahan" );
		BindUtils.postNotifyChange(null, null, this, "pel_mon_rs" );
		BindUtils.postNotifyChange(null, null, this, "pel_mon_irsms" );
		BindUtils.postNotifyChange(null, null, this, "authenticate" );
		BindUtils.postNotifyChange(null, null, this, "laporan" );
	}
	
	public String getCurrentDate() {
		Date now = new Date();
		Locale locale = new Locale("in", "ID");
		String result = new SimpleDateFormat("d MMMM yyyy", locale)
				.format(now);
		return result;
	}
	
	@Command
	public void goHome(){
		Executions.sendRedirect("/index.zul");
	}
	
	@Command
	public void logout(){
		getCurrentSession().invalidate();
		Executions.sendRedirect("/login.zul");
	}

	public AImage getCompanyImage() {
		return companyImage;
	}

	public void setCompanyImage(AImage companyImage) {
		this.companyImage = companyImage;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getUserName() {
		String name = "";
		if (userName==null){
		name = getCurrentUserSessionJR().getUserName();
		}else{
			name = userName;
		}
		return name;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public UserSessionJR getUserSession() {
		return userSession;
	}

	public void setUserSession(UserSessionJR userSession) {
		this.userSession = userSession;
	}

	public UserMenuDto getMenu() {
		return menu;
	}

	public void setMenu(UserMenuDto menu) {
		this.menu = menu;
	}

	public List<UserMenuDto> getListMenu() {
		return listMenu;
	}

	public void setListMenu(List<UserMenuDto> listMenu) {
		this.listMenu = listMenu;
	}


	public boolean isAuthenticate() {
		return authenticate;
	}

	public void setAuthenticate(boolean authenticate) {
		this.authenticate = authenticate;
	}

	public boolean getPel() {
		return pel;
	}

	public void setPel(boolean pel) {
		this.pel = pel;
	}

	public boolean getPel_absah_daftar() {
		return pel_absah_daftar;
	}

	public void setPel_absah_daftar(boolean pel_absah_daftar) {
		this.pel_absah_daftar = pel_absah_daftar;
	}

	public boolean getPel_hapus_pengajuan() {
		return pel_hapus_pengajuan;
	}

	public void setPel_hapus_pengajuan(boolean pel_hapus_pengajuan) {
		this.pel_hapus_pengajuan = pel_hapus_pengajuan;
	}

	public boolean getPel_hapus_register() {
		return pel_hapus_register;
	}

	public void setPel_hapus_register(boolean pel_hapus_register) {
		this.pel_hapus_register = pel_hapus_register;
	}

	public boolean getPel_insidentil() {
		return pel_insidentil;
	}

	public void setPel_insidentil(boolean pel_insidentil) {
		this.pel_insidentil = pel_insidentil;
	}

	public boolean getPel_kabag_daftar() {
		return pel_kabag_daftar;
	}

	public void setPel_kabag_daftar(boolean pel_kabag_daftar) {
		this.pel_kabag_daftar = pel_kabag_daftar;
	}

	public boolean getPel_kasubag_daftar() {
		return pel_kasubag_daftar;
	}

	public void setPel_kasubag_daftar(boolean pel_kasubag_daftar) {
		this.pel_kasubag_daftar = pel_kasubag_daftar;
	}

	public boolean getPel_laka_daftar() {
		return pel_laka_daftar;
	}

	public void setPel_laka_daftar(boolean pel_laka_daftar) {
		this.pel_laka_daftar = pel_laka_daftar;
	}

	public boolean getPel_nik_cetak() {
		return pel_nik_cetak;
	}

	public void setPel_nik_cetak(boolean pel_nik_cetak) {
		this.pel_nik_cetak = pel_nik_cetak;
	}

	public boolean getPel_operasional() {
		return pel_operasional;
	}

	public void setPel_operasional(boolean pel_operasional) {
		this.pel_operasional = pel_operasional;
	}

	public boolean getPel_otorisasi_daftar() {
		return pel_otorisasi_daftar;
	}

	public void setPel_otorisasi_daftar(boolean pel_otorisasi_daftar) {
		this.pel_otorisasi_daftar = pel_otorisasi_daftar;
	}

	public boolean getPel_penyelesaian_daftar() {
		return pel_penyelesaian_daftar;
	}

	public void setPel_penyelesaian_daftar(boolean pel_penyelesaian_daftar) {
		this.pel_penyelesaian_daftar = pel_penyelesaian_daftar;
	}

	public boolean getPel_register_daftar() {
		return pel_register_daftar;
	}

	public void setPel_register_daftar(boolean pel_register_daftar) {
		this.pel_register_daftar = pel_register_daftar;
	}

	public boolean getPel_request_perubahan() {
		return pel_request_perubahan;
	}

	public void setPel_request_perubahan(boolean pel_request_perubahan) {
		this.pel_request_perubahan = pel_request_perubahan;
	}

	public boolean getPel_santunan_daftar() {
		return pel_santunan_daftar;
	}

	public void setPel_santunan_daftar(boolean pel_santunan_daftar) {
		this.pel_santunan_daftar = pel_santunan_daftar;
	}

	public boolean getPel_selesai_pengajuan() {
		return pel_selesai_pengajuan;
	}

	public void setPel_selesai_pengajuan(boolean pel_selesai_pengajuan) {
		this.pel_selesai_pengajuan = pel_selesai_pengajuan;
	}

	public boolean getPel_tutup_periode() {
		return pel_tutup_periode;
	}

	public void setPel_tutup_periode(boolean pel_tutup_periode) {
		this.pel_tutup_periode = pel_tutup_periode;
	}

	public boolean getPl_mst() {
		return pl_mst;
	}

	public void setPl_mst(boolean pl_mst) {
		this.pl_mst = pl_mst;
	}

	public boolean getPl_mst_bank() {
		return pl_mst_bank;
	}

	public void setPl_mst_bank(boolean pl_mst_bank) {
		this.pl_mst_bank = pl_mst_bank;
	}

	public boolean getPl_mst_instansi() {
		return pl_mst_instansi;
	}

	public void setPl_mst_instansi(boolean pl_mst_instansi) {
		this.pl_mst_instansi = pl_mst_instansi;
	}

	public boolean getPl_mst_lokasi() {
		return pl_mst_lokasi;
	}

	public void setPl_mst_lokasi(boolean pl_mst_lokasi) {
		this.pl_mst_lokasi = pl_mst_lokasi;
	}

	public boolean getPl_mst_picrs() {
		return pl_mst_picrs;
	}

	public void setPl_mst_picrs(boolean pl_mst_picrs) {
		this.pl_mst_picrs = pl_mst_picrs;
	}

	public boolean getPl_mst_rs() {
		return pl_mst_rs;
	}

	public void setPl_mst_rs(boolean pl_mst_rs) {
		this.pl_mst_rs = pl_mst_rs;
	}

	public boolean getSw_ews() {
		return sw_ews;
	}

	public void setSw_ews(boolean sw_ews) {
		this.sw_ews = sw_ews;
	}

	public boolean getPel_monitoring_limpahan() {
		return pel_monitoring_limpahan;
	}

	public void setPel_monitoring_limpahan(boolean pel_monitoring_limpahan) {
		this.pel_monitoring_limpahan = pel_monitoring_limpahan;
	}

	public boolean getPel_mon_rs() {
		return pel_mon_rs;
	}

	public void setPel_mon_rs(boolean pel_mon_rs) {
		this.pel_mon_rs = pel_mon_rs;
	}

	public boolean getPel_mon_irsms() {
		return pel_mon_irsms;
	}

	public void setPel_mon_irsms(boolean pel_mon_irsms) {
		this.pel_mon_irsms = pel_mon_irsms;
	}

	public boolean isPel_monitoring() {
		return pel_monitoring;
	}

	public void setPel_monitoring(boolean pel_monitoring) {
		this.pel_monitoring = pel_monitoring;
	}

	public String getNamaUserKantor() {
		return namaUserKantor;
	}

	public void setNamaUserKantor(String namaUserKantor) {
		this.namaUserKantor = namaUserKantor;
	}

	public boolean isLaporan() {
		return laporan;
	}

	public void setLaporan(boolean laporan) {
		this.laporan = laporan;
	}
	

}
