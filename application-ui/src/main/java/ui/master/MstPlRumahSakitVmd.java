package ui.master;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import share.FndBankDto;
import share.FndKantorJasaraharjaDto;
import share.PlPksRDto;
import share.PlRekeningRDto;
import share.PlRsBpjDto;
import share.PlRsMapBpjDto;
import share.PlRumahSakitDto;
import common.model.RestResponse;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass = true)
public class MstPlRumahSakitVmd extends BaseVmd implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterRumahSakit/_detail.zul";
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterRumahSakit/_index.zul";

	private final String WS_URI = "/MasterRumahSakit";
	private final String WS_URI_LOV = "/Lov";

	private Form formMaster = new SimpleForm();
	private Form formDetail = new SimpleForm();

	private List<PlRumahSakitDto> listDto = new ArrayList<>();
	private List<PlRumahSakitDto> listDtoCopy = new ArrayList<>();

	private List<FndBankDto> listBankDto = new ArrayList<>();
	private List<PlPksRDto> listPksDto = new ArrayList<>();
	private List<PlRekeningRDto> listRekDto = new ArrayList<>();
	
	private List<PlRsMapBpjDto> listLoketPjDto = new ArrayList<>();
	private List<PlRsMapBpjDto> listLoketPjDtoCopy = new ArrayList<>();
	
	private List<PlRsMapBpjDto> listMapBpjDto = new ArrayList<>();
	private List<FndKantorJasaraharjaDto> listKanWil = new ArrayList<>();
	private List<PlRumahSakitDto>listKodeNamaRsDto = new ArrayList<>();

	private PlRumahSakitDto mstPlRumahSakitDto = new PlRumahSakitDto();
	private PlRumahSakitDto selected = new PlRumahSakitDto();
	
	private PlRsMapBpjDto loketPjDto = new PlRsMapBpjDto();
	private PlRsMapBpjDto mstPlRsMapBpjDto = new PlRsMapBpjDto();
	private PlPksRDto mstPlPksRDto = new PlPksRDto();
	private PlPksRDto selectedPks = new PlPksRDto();
	
	private PlRekeningRDto mstPlRekeningRDto = new PlRekeningRDto();
	private PlRekeningRDto selectedRek = new PlRekeningRDto();
	
	private PlRsBpjDto mstPlRsBpjDto = new PlRsBpjDto();
	private FndBankDto mstFndBankDto = new FndBankDto();

	private boolean listIndex = false;

	private boolean daftarPksGb = true;
	private boolean daftarRekGb = true;

	private boolean formPksGb = false;
	private boolean formRekGb = false;

	private boolean bpjsField = false;
	private boolean formBpjsPopup = false;
	boolean editPKss = false;

	private String jenisRs = "Pemerintah";
	private String flagActive = "Ya";
	private String flagActiveRek = "Ya";
	private String flagActivePks = "Ya";
	private String kanWil = null;
	private String kodeNama = null;
	private String kodeBank = null;
	
	private boolean radioBpjs1 = false;
	private boolean radioBpjs2 = false;
	private boolean radioBpjs3 = false;	
	
	private String searchLoketPj;
	private String searchKanwil;
	private String searchKodeNamaRs;
	private String searchIndex = null;
	
	private int pageSize = 5;
	
	private List<String> listJenisRs = new ArrayList<>();
	private List<String> listStatusRs = new ArrayList<>();

	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent().getAttribute(
			UIConstants.SESS_LOGIN_ID);

	@Command
	public void close(@BindingParam("item") Window win){
		win.detach();	
	}
	
	public void listJenisRs(){
		setJenisRs("Pemerintah");
		listJenisRs.add("Pemerintah");
		listJenisRs.add("Swasta");		
		BindUtils.postNotifyChange(null, null, this, "listJenisRs");
		BindUtils.postNotifyChange(null, null, this, "jenisRs");
	}
	
	public void listStatusRs(){
		setFlagActive("Ya");
		listStatusRs.add("Ya");
		listStatusRs.add("Tidak");		
		BindUtils.postNotifyChange(null, null, this, "listStatusRs");
		BindUtils.postNotifyChange(null, null, this, "flagActive");
	}
	
	@Command
	public void openDetail(@BindingParam("popup") String popup, @BindingParam("item") PlRumahSakitDto dto,
			@Default("popUpHandler") @BindingParam("mode") String mode){
		Map<String, Object> args = new HashMap<>();
		if(dto!=null){
			this.selected = dto;
		}
		args.put("dto", dto);
		if(mode!= null && mode.equalsIgnoreCase("add")){
			add();
		}else{
			edit();
		}
		if(dto!=null){
			Executions.getCurrent().setAttribute("obj", dto);
		}
		
		try {
			System.out.println(popup);
			((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	protected void loadList() {
		RestResponse restKantor =  new RestResponse();
		try {
			restKantor = callWs(WS_URI_LOV + "/wilayahKantor/"+userSession.getKantor().substring(0,2)+"%",
					new HashMap<String, Object>(), HttpMethod.POST);
			listKanWil = JsonUtil.mapJsonToListObject(restKantor.getContents(),
					FndKantorJasaraharjaDto.class);
			for(FndKantorJasaraharjaDto a : listKanWil){
				setSearchKanwil(a.getKodeNama());
			}
//			Messagebox.show("test "+getSearchKanwil());
			setTotalSize(restKantor.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listKanWil");
			BindUtils.postNotifyChange(null, null, this, "searchKanwil");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void searchLoketPj() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", "");
		map.put("kodeKantorJr", userSession.getKantor().substring(0,2)+"%");
		RestResponse rest = callWs(WS_URI_LOV + "/getListLoketPj", map, HttpMethod.POST);
		try {
			listLoketPjDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRsMapBpjDto.class);
			listLoketPjDtoCopy = new ArrayList<>();
			listLoketPjDtoCopy.addAll(listLoketPjDto);
			BindUtils.postNotifyChange(null, null, this, "listLoketPjDtoCopy");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@NotifyChange("listLoketPjDtoCopy")
	@Command
	public void cariFilterAjaLoketPj(@BindingParam("item") String cari) {
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listLoketPjDtoCopy));
		if (listLoketPjDtoCopy != null || listLoketPjDtoCopy.size() > 0) {
			listLoketPjDtoCopy.clear();
		}
		if (listLoketPjDto != null && listLoketPjDto.size() > 0) {
			for (PlRsMapBpjDto dto : listLoketPjDto) {
				System.out.println("+++");
				if (dto.getKodeNama().toUpperCase()
						.contains(cari.toUpperCase())) {
					listLoketPjDtoCopy.add(dto);
				}
			}
		}
	}
	
	
	
	
	
	@Command
	public void searchKanwil(){
		Map<String, Object> map = new HashMap<>();
		map.put("input", getCurrentUserSessionJR());
		map.put("search", searchKanwil);
		RestResponse restKantor = callWs(WS_URI_LOV + "/wilayahKantor",
				map, HttpMethod.POST);
		try {			
			listKanWil = JsonUtil.mapJsonToListObject(restKantor.getContents(),
					FndKantorJasaraharjaDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKanWil");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void searchKodeNamaRs(){
		if(getSearchKanwil()==null||getSearchKanwil().equalsIgnoreCase("")){
			setSearchKanwil("%%");
		}
		if(getSearchKanwil().contains("-")){
			setSearchKanwil(getSearchKanwil().substring(0, 2));
		}
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchKodeNamaRs);
		RestResponse rest = callWs(WS_URI_LOV + "/kodeNamaRs/"+searchKanwil+"%",
				map, HttpMethod.POST);
		try {			
			listKodeNamaRsDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRumahSakitDto.class);
			BindUtils.postNotifyChange(null, null, this, "listKodeNamaRsDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@NotifyChange("listDtoCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari){
		if(listDtoCopy != null || listDtoCopy.size() >0 ){
			listDtoCopy.clear();
		}
		if(listDto != null && listDto.size() > 0){
			for(PlRumahSakitDto dto : listDto){
				if(
						dto.getKodeRumahsakit().toUpperCase().contains(cari)||
						dto.getDeskripsi().toUpperCase().contains(cari)||
						dto.getJenisRs().toUpperCase().contains(cari)||
						dto.getFlagEnable().toUpperCase().contains(cari))
				{
							listDtoCopy.add(dto);
				}
			}
		}
	}

	
	@Command
	public void searchIndex(){
		
		if(getSearchKanwil()==null||getSearchKanwil().equalsIgnoreCase("")){
			setSearchKanwil("%%");
		}else if(getSearchKanwil().contains("-")){
			setSearchKanwil(getSearchKanwil().substring(0, 2));
		}
		if(getSearchKodeNamaRs()==null||getSearchKodeNamaRs().equalsIgnoreCase("")){
			setSearchKodeNamaRs("");
		}else if(getSearchKodeNamaRs().contains("-")){
			setSearchKodeNamaRs(getSearchKodeNamaRs().substring(0, 7));
		}
		if(getSearchIndex()==null||getSearchIndex().equalsIgnoreCase("")){
			setSearchIndex("%%");
		}
		Map<String, Object> map = new HashMap<>();
		map.put("kodeNamaRs", searchKodeNamaRs);
		map.put("kanWil", searchKanwil);
		RestResponse rest = callWs(WS_URI+"/all",map, HttpMethod.POST);
		try{
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(), PlRumahSakitDto.class);
			setListIndex(true);
			listDtoCopy = new ArrayList<>();
			listDtoCopy.addAll(listDto);
			BindUtils.postNotifyChange(null, null, this, "listIndex");
			BindUtils.postNotifyChange(null, null, this, "listDto");
			BindUtils.postNotifyChange(null, null, this, "listDtoCopy");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void loadListRs(){
		if(getKanWil()==null || getKanWil().equalsIgnoreCase("")){
			setKanWil("%");
		}else if( getKodeNama()==null || getKodeNama().equalsIgnoreCase("")){
			setKodeNama("%");
		}else if (getKanWil().contains("-")){
			String a=getKanWil().substring(0,2)+"%";
			setKanWil(a);
		}else if(getKodeNama().contains("-")){
			String b=getKodeNama().substring(0, 7);
			setKodeNama(b);			
		}else if(getSearchIndex()==null || getSearchIndex().equalsIgnoreCase("")){
			setSearchIndex("%");
		}

		RestResponse rest = callWs(WS_URI + "/all/"+getKanWil()+"/"+getKodeNama()+"/"+getSearchIndex(),
				new HashMap<String, Object>(), HttpMethod.POST);
		
		try{
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRumahSakitDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listDto");			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	public void listRs(){
		loadListRs();
	}

//	@NotifyChange("listMapBpjDto")
//	@Command("add")
	private void add() {
		getPageInfo().setAddMode(true);
//		navigate(DETAIL_PAGE_PATH);
	}
	
	private void edit(){
		getPageInfo().setEditMode(true);
	}

	public void onAdd() {
//		findRsMapBpj();
		searchLoketPj();
		listJenisRs();
		listStatusRs();
	}

	public void findRsMapBpj() {
		RestResponse rest = callWs(WS_URI + "/findPj",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listMapBpjDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRsMapBpjDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listMapBpjDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange("loketPjDto")
	@Command
	public void findBpjsByLoket() {
		Map<String, Object> map = new HashMap<>();
		if(loketPjDto==null||loketPjDto.getLoketPenanggungjawab()==null){
			map.put("loketPenanggungjawab", "");
		}else{
			map.put("loketPenanggungjawab", loketPjDto.getLoketPenanggungjawab());			
		}
		RestResponse rest = callWs(WS_URI + "/findBpjsByLoket", map, HttpMethod.POST);
		try {
			listMapBpjDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRsMapBpjDto.class);
			BindUtils.postNotifyChange(null, null, this, "listMapBpjDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command("edit")
	public void edit(@BindingParam("item") PlRumahSakitDto selected) {
		if (selected == null || selected.getKodeRumahsakit() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Executions.getCurrent().setAttribute("obj", selected);
		getPageInfo().setEditMode(true);
		navigate(DETAIL_PAGE_PATH);
	}

	public void findPjByKodeRumahSakit() {
		
	}

	public void onEdit() {
		mstPlRumahSakitDto = (PlRumahSakitDto) Executions.getCurrent().getAttribute("obj");		
		searchLoketPj();
		listJenisRs();
		listStatusRs();
		
		mstPlRumahSakitDto.setNo(mstPlRumahSakitDto.getNo());
		mstPlRumahSakitDto.setKodeRumahsakit(mstPlRumahSakitDto.getKodeRumahsakit());
		mstPlRumahSakitDto.setDeskripsi(mstPlRumahSakitDto.getDeskripsi());
		mstPlRumahSakitDto.setAlamat(mstPlRumahSakitDto.getAlamat());
		mstPlRumahSakitDto.setPic(mstPlRumahSakitDto.getPic());
		if(mstPlRumahSakitDto.getFlagEnable().equalsIgnoreCase("Y")){
			setFlagActive("Ya");
		}else if(mstPlRumahSakitDto.getFlagEnable().equalsIgnoreCase("N")){
			setFlagActive("Tidak");
		}
		
		if(mstPlRumahSakitDto.getJenisRs().equalsIgnoreCase("P")){
			setJenisRs("Pemerintah");
		}else if(mstPlRumahSakitDto.getJenisRs().equalsIgnoreCase("S")){
			setJenisRs("Swasta");
		}

		RestResponse rest = callWs(
				WS_URI + "/findPj/" + mstPlRumahSakitDto.getKodeRumahsakit(),
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listLoketPjDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRsMapBpjDto.class);
			listLoketPjDtoCopy.addAll(listLoketPjDto);
			for(PlRsMapBpjDto a : listLoketPjDto){
				loketPjDto.setKodeNama(a.getKodeNama());
				loketPjDto.setKodeBpjs(a.getKodeBpjs());
				loketPjDto.setNamaRsBpjs(a.getNamaRsBpjs());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		for(PlRsMapBpjDto loket : listLoketPjDto){
			if(loketPjDto.getKodeBpjs().equalsIgnoreCase(loket.getKodeBpjs())){
				loketPjDto = new PlRsMapBpjDto();
				loketPjDto = loket;
			}
		}
		if(listLoketPjDto.size()>0){
			setRadioBpjs2(true);
		}
		setBpjsField(true);
		findRsMapBpj();
		
		Map<String, Object>mapBpjs = new HashMap<>();
		mapBpjs.put("kodeRumahsakit", mstPlRumahSakitDto.getKodeRumahsakit());
		RestResponse restBpjs = callWs(WS_URI_LOV + "/getBpjsByKodeRs",
				mapBpjs, HttpMethod.POST);
		try {
			listMapBpjDto = JsonUtil.mapJsonToListObject(restBpjs.getContents(),
					PlRsMapBpjDto.class);
			for(PlRsMapBpjDto a : listMapBpjDto){
				mstPlRsMapBpjDto.setKodeNama(a.getKodeNama());
				mstPlRsMapBpjDto.setKodeBpjs(a.getKodeBpjs());
				mstPlRsMapBpjDto.setNamaRsBpjs(a.getNamaRsBpjs());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		for(PlRsMapBpjDto bpjs: listMapBpjDto){
			if(mstPlRsMapBpjDto.getKodeBpjs().equalsIgnoreCase(bpjs.getKodeBpjs())){
				mstPlRsMapBpjDto = new PlRsMapBpjDto();
				mstPlRsMapBpjDto = bpjs;
			}
		}
		
//		showFieldBpjs();
//		if(listMapBpjDto.size()==0){
//			findRsMapBpj();
//			hideFieldBpjs();
//		}
		BindUtils.postNotifyChange(null, null, this, "bpjsField");
		BindUtils.postNotifyChange(null, null, this, "listLoketPjDtoCopy");
		BindUtils.postNotifyChange(null, null, this, "loketPjDto");
		BindUtils.postNotifyChange(null, null, this, "mstPlRsMapBpjDto");
		BindUtils.postNotifyChange(null, null, this, "listMapBpjDto");
		BindUtils.postNotifyChange(null, null, this, "mstPlRumahSakitDto");
		BindUtils.postNotifyChange(null, null, this, "jenisRs");
		BindUtils.postNotifyChange(null, null, this, "flagActive");

	}
	
	@Command
	public void changePageSize(){
		setPageSize(getPageSize());
		setListIndex(true);
		loadList();
		BindUtils.postNotifyChange(null, null, this, "pageSize");	
	}

	@Command("back")
	public void back() {
		getPageInfo().setListMode(true);
		setListIndex(true);
		navigate(INDEX_PAGE_PATH);
		BindUtils.postNotifyChange(null, null, this, "listIndex");
	}

	@Command("search")
	public void search() {
		setListIndex(true);
		loadList();		
		listRs();
		BindUtils.postNotifyChange(null, null, this, "sortAndSearch");
		BindUtils.postNotifyChange(null, null, this, "listIndex");
	}

	@NotifyChange({ "jenisRs", "flagActive" })
	@Command("saveDataRs")
	public void saveDataRs() {
		RestResponse restResponse = null;
		if (getPageInfo().isAddMode()) {
//			Messagebox.show("test "+mstPlRumahSakitDto.getAlamat());
			mstPlRumahSakitDto.setKodeRumahsakit(mstPlRumahSakitDto.getKodeRumahsakit());
			mstPlRumahSakitDto.setDeskripsi(mstPlRumahSakitDto.getDeskripsi());
			mstPlRumahSakitDto.setAlamat(mstPlRumahSakitDto.getAlamat());
			mstPlRumahSakitDto.setPic(mstPlRumahSakitDto.getPic());
			mstPlRumahSakitDto.setCreationDate(new Date());
			if(flagActive.equalsIgnoreCase("Ya")){
				mstPlRumahSakitDto.setFlagEnable("Y");
			}else if(flagActive.equalsIgnoreCase("Tidak")){
				mstPlRumahSakitDto.setFlagEnable("N");
			}
			if(jenisRs.equalsIgnoreCase("Pemerintah")){
				mstPlRumahSakitDto.setJenisRs("P");
			}else if(jenisRs.equalsIgnoreCase("Swasta")){
				mstPlRumahSakitDto.setJenisRs("S");
			}

			restResponse = callWs(WS_URI, mstPlRumahSakitDto, HttpMethod.POST);

			if(mstPlRsMapBpjDto.getKodeNama()!=null){
				mstPlRsMapBpjDto.setAlamatRsBpjs(mstPlRsMapBpjDto.getAlamatRsBpjs());
				mstPlRsMapBpjDto.setCreatedBy(userSession.getLoginID());
				mstPlRsMapBpjDto.setCreationDate(new Date());
				mstPlRsMapBpjDto.setKabKotaRsBpjs(mstPlRsMapBpjDto.getKabKotaRsBpjs());
				mstPlRsMapBpjDto.setKodeBpjs(mstPlRsMapBpjDto.getKodeBpjs());
				mstPlRsMapBpjDto.setKodeRumahsakit(mstPlRumahSakitDto.getKodeRumahsakit());
				mstPlRsMapBpjDto.setLoketPenanggungjawab(loketPjDto.getLoketPenanggungjawab());
				mstPlRsMapBpjDto.setNamaRsBpjs(mstPlRsMapBpjDto.getNamaRsBpjs());
				mstPlRsMapBpjDto.setPropinsiRsBpjs(mstPlRsMapBpjDto.getPropinsiRsBpjs());
				mstPlRsMapBpjDto.setKodeInstansi(mstPlRsMapBpjDto.getKodeInstansi());
				restResponse = callWs(WS_URI + "/saveRsMapBpj", mstPlRsMapBpjDto,
						HttpMethod.POST);
				
			}
		}else{
//			Messagebox.show("test "+mstPlRumahSakitDto.getAlamat());
			mstPlRumahSakitDto.setDeskripsi(mstPlRumahSakitDto.getDeskripsi());
			mstPlRumahSakitDto.setJenisRs(getJenisRs());
			if(flagActive.equalsIgnoreCase("Ya")){
				mstPlRumahSakitDto.setFlagEnable("Y");
			}else if(flagActive.equalsIgnoreCase("Tidak")){
				mstPlRumahSakitDto.setFlagEnable("N");
			}
			if(jenisRs.equalsIgnoreCase("Pemerintah")){
				mstPlRumahSakitDto.setJenisRs("P");
			}else if(jenisRs.equalsIgnoreCase("Swasta")){
				mstPlRumahSakitDto.setJenisRs("S");
			}

			mstPlRumahSakitDto.setLastUpdatedDate(new Date());
			mstPlRumahSakitDto.setAlamat(mstPlRumahSakitDto.getAlamat());
			restResponse = callWs(WS_URI, mstPlRumahSakitDto, HttpMethod.POST);


			if(mstPlRsMapBpjDto.getKodeNama()!=null){
			mstPlRsMapBpjDto
					.setAlamatRsBpjs(mstPlRsMapBpjDto.getAlamatRsBpjs());
			mstPlRsMapBpjDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
			mstPlRsMapBpjDto.setLastUpdatedDate(new Date());
			mstPlRsMapBpjDto.setKabKotaRsBpjs(mstPlRsMapBpjDto
					.getKabKotaRsBpjs());
			mstPlRsMapBpjDto.setKodeBpjs(mstPlRsMapBpjDto.getKodeBpjs());
			mstPlRsMapBpjDto.setKodeRumahsakit(mstPlRumahSakitDto
					.getKodeRumahsakit());
			mstPlRsMapBpjDto.setLoketPenanggungjawab(mstPlRsMapBpjDto
					.getLoketPenanggungjawab());
			mstPlRsMapBpjDto.setNamaRsBpjs(mstPlRsMapBpjDto.getNamaRsBpjs());
			mstPlRsMapBpjDto.setPropinsiRsBpjs(mstPlRsMapBpjDto
					.getPropinsiRsBpjs());
			mstPlRsMapBpjDto
					.setKodeInstansi(mstPlRsMapBpjDto.getKodeInstansi());
			restResponse = callWs(WS_URI + "/saveRsMapBpj", mstPlRsMapBpjDto,
					HttpMethod.POST);
			}
		}
		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
		} else if (restResponse.getStatus() == 2) {
			Messagebox.show("Area Code Already Exists");
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}

	@Command
	public void addBpjs() {
		setFormBpjsPopup(true);
		getPageInfo().setCustomAttribute("addBpjs");
		BindUtils.postNotifyChange(null, null, this, "formBpjsPopup");
	}

	@Command
	public void showFieldBpjs() {
		if(isRadioBpjs3()){
			if(loketPjDto == null ||loketPjDto.getKodeNama()==null){
				if(getPageInfo().isAddMode()==true){
					showInfoMsgBox("Pilih loket kantor PJ", "");
					setRadioBpjs3(false);
					BindUtils.postNotifyChange(null, null, this, "radioBpjs3");				
				}
			}else{
				addBpjs();
//				setRadioBpjs3(true);
//				setBpjsField(true);
//				BindUtils.postNotifyChange(null, null, this, "bpjsField");			
//				BindUtils.postNotifyChange(null, null, this, "radioBpjs3");
			}
		}else{
			if(loketPjDto == null ||loketPjDto.getKodeNama()==null){
				if(getPageInfo().isAddMode()==true){
					showInfoMsgBox("Pilih loket kantor PJ", "");
					setRadioBpjs2(false);
					BindUtils.postNotifyChange(null, null, this, "radioBpjs2");				
				}
			}else{
				setRadioBpjs2(true);
				setBpjsField(true);
				BindUtils.postNotifyChange(null, null, this, "bpjsField");			
				BindUtils.postNotifyChange(null, null, this, "radioBpjs2");
			}
		}
	}

	@Command
	public void hideFieldBpjs() {
		setBpjsField(false);
		BindUtils.postNotifyChange(null, null, this, "bpjsField");
	}

	@NotifyChange({"bpjsField"})
	@Command
	public void saveBpjs() {
		RestResponse restResponse = null;
		if (getPageInfo().getCustomAttribute().equalsIgnoreCase("addBpjs")) {
//			mstPlRsBpjDto.setKodeRsBpjs(mstPlRsBpjDto.getKodeRsBpjs());
//			mstPlRsBpjDto.setNamaRs(mstPlRsBpjDto.getNamaRs());
//			mstPlRsBpjDto.setProvinsi(mstPlRsBpjDto.getProvinsi());
//			mstPlRsBpjDto.setKabKota(mstPlRsBpjDto.getKabKota());
			
			mstPlRsMapBpjDto.setPropinsiRsBpjs(mstPlRsMapBpjDto.getPropinsiRsBpjs());
			mstPlRsMapBpjDto.setKabKotaRsBpjs(mstPlRsMapBpjDto.getKabKotaRsBpjs());
			mstPlRsMapBpjDto.setKodeBpjs(mstPlRsMapBpjDto.getKodeBpjs());
			mstPlRsMapBpjDto.setNamaRsBpjs(mstPlRsMapBpjDto.getNamaRsBpjs());
			mstPlRsMapBpjDto.setKodeNama(mstPlRsMapBpjDto.getKodeBpjs()+" - "+mstPlRsMapBpjDto.getNamaRsBpjs());
			listMapBpjDto.add(mstPlRsMapBpjDto);
			for(PlRsMapBpjDto a : listMapBpjDto){
				if(mstPlRsMapBpjDto.getKodeBpjs().equalsIgnoreCase(a.getKodeBpjs())){
					mstPlRsMapBpjDto = new PlRsMapBpjDto();
					mstPlRsMapBpjDto = a;
				}
			}
//			mstPlRsMapBpjDto.setKodeBpjs(mstPlRsBpjDto.getKodeRsBpjs());
			BindUtils.postNotifyChange(null, null, this, "mstPlRsBpjDto");
			BindUtils.postNotifyChange(null, null, this, "mstPlRsMapBpjDto");
			BindUtils.postNotifyChange(null, null, this, "listMapBpjDto");
			setBpjsField(true);
			BindUtils.postNotifyChange(null, null, this, "bpjsField");
			tutup();
//			restResponse = callWs(WS_URI+"/saveRsBpj", mstPlRsBpjDto, HttpMethod.POST);		
		}
//		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
//			showInfoMsgBox(restResponse.getMessage());
//			showFieldBpjs();
//			tutup();
//		} else {
//			showErrorMsgBox(restResponse.getMessage());
//		}
	}

	@Command
	public void tutup() {
		setFormBpjsPopup(false);
		BindUtils.postNotifyChange(null, null, this, "formBpjsPopup");
	}

	/* TAB PKS */
	@NotifyChange({ "formPksGb", "daftarPksGb", "mstPlPksRDto" })
	@Command("addPks")
	public void addPks() {
		getPageInfo().setCustomAttribute("addPks");
//		mstPlPksRDto = new PlPksRDto();
		if(!editPKss){
			mstPlPksRDto = new PlPksRDto();
		}
		setFormPksGb(true);
		setDaftarPksGb(false);
		editPKss = false;
	}

	public void loadListPks() {
		RestResponse rest = new RestResponse();
		try {
			rest = callWs(WS_URI + "/findPks/"
					+ mstPlRumahSakitDto.getKodeRumahsakit(),
					new HashMap<String, Object>(), HttpMethod.POST);
			listPksDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlPksRDto.class);
//			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listPksDto");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@NotifyChange({ "formPksGb", "daftarPksGb", "mstPlRumahSakitDto" })
	@Command("backPks")
	public void backPks() {
		loadListPks();
		setFormPksGb(false);
		setDaftarPksGb(true);
	}

	@NotifyChange({ "formPksGb", "daftarPksGb" })	
	@Command("saveDataPks")
	public void saveDataPks() {
		RestResponse restResponse = null;
		if (getPageInfo().getCustomAttribute().equalsIgnoreCase("addPks")) {
			if (mstPlRumahSakitDto.getKodeRumahsakit() == null) {
				Messagebox.show("Kode Rumah Sakit tidak Ditemukan");
			} else {				
				mstPlPksRDto.setNoPksJr(mstPlPksRDto.getNoPksJr());
				mstPlPksRDto.setNoPksRs(mstPlPksRDto.getNoPksRs());
				mstPlPksRDto.setTglPks(mstPlPksRDto.getTglPks());
				mstPlPksRDto.setMasaLaku(mstPlPksRDto.getMasaLaku());
				mstPlPksRDto.setKeterangan(mstPlPksRDto.getKeterangan());
				mstPlPksRDto.setKodeRumahsakit(mstPlRumahSakitDto
						.getKodeRumahsakit());
				mstPlPksRDto.setKantor(getCurrentUserSessionJR().getKantor());
				mstPlPksRDto.setCreationDate(new Date());
				mstPlPksRDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Date date = new Date();
				String tglStr[] = sdf.format(mstPlPksRDto.getTglPks()).split("/");
				String dd = tglStr[0];
				String mm = tglStr[1];
				String yyyy = tglStr[2];
				
				int tahunTotal = Integer.parseInt(yyyy)+Integer.parseInt(mstPlPksRDto.getMasaLaku());
				
				String tglBaruStr = dd+"/"+mm+"/"+tahunTotal;
				try{
					Date tglBaru = sdf.parse(tglBaruStr);
					if(tglBaru.before(date)){
						mstPlPksRDto.setFlagAktif("N");
					}else if(tglBaru.after(date)){
						mstPlPksRDto.setFlagAktif("Y");
					}					
				}catch(Exception e){
					e.printStackTrace();
				}
				mstPlPksRDto.setFlagAktif("Y");
				restResponse = callWs(WS_URI + "/savePksRs", mstPlPksRDto,
						HttpMethod.POST);
			}
		}else{
			mstPlPksRDto.setFlagAktif("Y");
			mstPlPksRDto.setNoPksJr(mstPlPksRDto.getNoPksJr());
			mstPlPksRDto.setNoPksRs(mstPlPksRDto.getNoPksRs());
			mstPlPksRDto.setTglPks(mstPlPksRDto.getTglPks());
			mstPlPksRDto.setMasaLaku(mstPlPksRDto.getMasaLaku());
			mstPlPksRDto.setKeterangan(mstPlPksRDto.getKeterangan());
			mstPlPksRDto.setKantor(getCurrentUserSessionJR().getKantor());
			mstPlPksRDto.setLastUpdatedBy(getCurrentUserSessionJR().getLoginID());
			mstPlPksRDto.setLastUpdatedDate(new Date());
			restResponse = callWs(WS_URI + "/savePksRs", mstPlPksRDto,
					HttpMethod.POST);
		}
		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
			
			RestResponse o = new RestResponse();
			try{
				o = callWs(WS_URI+"/updatePlPksRs/"+mstPlPksRDto.getKodeRumahsakit(), new HashMap<>(), HttpMethod.POST);
				
			}catch(Exception p){
				p.printStackTrace();
			}
			
			backPks();
		} else if (restResponse.getStatus() == 2) {
			Messagebox.show("Area Code Already Exists");
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}
	
	@NotifyChange({ "formPksGb", "daftarPksGb","mstPlPksRDto" })	
	@Command
	public void editPks(@BindingParam("item") final PlPksRDto selectedPks){
		if (selectedPks == null || selectedPks.getIdRsPks() == null) {
			showSmartMsgBox("W001");
			return;
		}else{
			mstPlPksRDto.setNoPksJr(selectedPks.getNoPksJr());
			mstPlPksRDto.setNoPksRs(selectedPks.getNoPksRs());
			mstPlPksRDto.setTglPks(selectedPks.getTglPks());
			mstPlPksRDto.setMasaLaku(selectedPks.getMasaLaku());
			mstPlPksRDto.setKeterangan(selectedPks.getKeterangan());
			mstPlPksRDto.setFlagAktif(selectedPks.getFlagAktif());
			mstPlPksRDto.setCreatedBy(selectedPks.getCreatedBy());
			mstPlPksRDto.setCreationDate(selectedPks.getCreationDate());
			mstPlPksRDto.setIdRsPks(selectedPks.getIdRsPks());
			mstPlPksRDto.setKodeKantorJr(selectedPks.getKodeKantorJr());
			mstPlPksRDto.setKodeRumahsakit(selectedPks.getKodeRumahsakit());
			mstPlPksRDto.setTglLakuAkhir(selectedPks.getTglLakuAkhir());
			if(mstPlPksRDto.getFlagAktif().equalsIgnoreCase("Y")){
				setFlagActivePks("Ya");
			}else if(mstPlPksRDto.getFlagAktif().equalsIgnoreCase("N")){
				setFlagActivePks("Tidak");
			}
			BindUtils.postNotifyChange(null, null, this, "mstPlPksRDto");
			BindUtils.postNotifyChange(null, null, this, "flagActivePks");
			editPKss = true;
			addPks();			
		}
	}

	@NotifyChange({ "formPksGb", "daftarPksGb" })	
	@Command
	public void deletePks(@BindingParam("item") final PlPksRDto selectedPks){
		if (selectedPks == null || selectedPks.getIdRsPks() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							selectedPks.setFlagAktif("N");
							restRespone = callWs(WS_URI + "/deletePksRs", selectedPks,
									HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data successfully deleted");
								backPks();
							} else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});		
	}


	/* TAB REKENING */
	@NotifyChange({ "formRekGb", "daftarRekGb", "mstPlRekeningRDto"})
	@Command("addRek")
	public void addRek() {
		getPageInfo().setCustomAttribute("addRek");
		mstPlRekeningRDto = new PlRekeningRDto();
		RestResponse restFndBank = callWs(WS_URI + "/findAllBank",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listBankDto = JsonUtil.mapJsonToListObject(
					restFndBank.getContents(), FndBankDto.class);
			setTotalSize(restFndBank.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listBankDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
		setFormRekGb(true);
		setDaftarRekGb(false);
	}

	public void loadListRek() {
		RestResponse rest = callWs(WS_URI + "/findRek/"
				+ mstPlRumahSakitDto.getKodeRumahsakit(),
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listRekDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRekeningRDto.class);
//			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listRekDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange({ "formRekGb", "daftarRekGb", "mstPlRumahSakitDto" })
	@Command("backRek")
	public void backRek() {
		loadListRek();
		setFormRekGb(false);
		setDaftarRekGb(true);
	}

	@NotifyChange({ "flagActiveRek", "kodeBank", "formRekGb", "daftarRekGb" })
	@Command("saveDataRek")
	public void saveDataRek() {
		RestResponse restResponse = null;
		if (getPageInfo().getCustomAttribute().equalsIgnoreCase("addRek")) {
			if (mstPlRumahSakitDto.getKodeRumahsakit() == null) {
				Messagebox.show("Kode Rumah Sakit tidak Ditemukan");
			} else {
				mstPlRekeningRDto.setMode("add");
				mstPlRekeningRDto.setIdRsRek(mstPlRekeningRDto.getIdRsRek());
				mstPlRekeningRDto.setKodeRumahsakit(mstPlRumahSakitDto
						.getKodeRumahsakit());
				mstPlRekeningRDto.setKodeKantorJr(getCurrentUserSessionJR().getKantor());
				mstPlRekeningRDto.setKodeBank(mstFndBankDto.getKodeBank());
				mstPlRekeningRDto.setCreationDate(new Date());
				mstPlRekeningRDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
				mstPlRekeningRDto.setNoRekening(mstPlRekeningRDto
						.getNoRekening());
				mstPlRekeningRDto.setNamaPemilik(mstPlRekeningRDto
						.getNamaPemilik());
				if(getFlagActiveRek().equalsIgnoreCase("Ya")){
					mstPlRekeningRDto.setFlagEnable("Y");					
				}else if(getFlagActiveRek().equalsIgnoreCase("Tidak")){
					mstPlRekeningRDto.setFlagEnable("N");
				}
				mstPlRekeningRDto.setCreationDate(new Date());
				restResponse = callWs(WS_URI + "/saveRekRs", mstPlRekeningRDto,
						HttpMethod.POST);
			}
		}else{
			mstPlRekeningRDto.setMode("edit");
			mstPlRekeningRDto.setIdRsRek(mstPlRekeningRDto.getIdRsRek());
			mstPlRekeningRDto.setKodeRumahsakit(mstPlRumahSakitDto
					.getKodeRumahsakit());
			mstPlRekeningRDto.setKodeKantorJr(getCurrentUserSessionJR().getKantor());
			mstPlRekeningRDto.setKodeBank(mstFndBankDto.getKodeBank());
//			mstPlRekeningRDto.setCreationDate(new Date());
			mstPlRekeningRDto.setNoRekening(mstPlRekeningRDto
					.getNoRekening());
			mstPlRekeningRDto.setNamaPemilik(mstPlRekeningRDto
					.getNamaPemilik());
			if(getFlagActiveRek().equalsIgnoreCase("Ya")){
				mstPlRekeningRDto.setFlagEnable("Y");					
			}else if(getFlagActiveRek().equalsIgnoreCase("Tidak")){
				mstPlRekeningRDto.setFlagEnable("N");
			}
			mstPlRekeningRDto.setLastUpdatedBy(getCurrentUserSessionJR().getKantor());
			mstPlRekeningRDto.setLastUpdatedDate(new Date());			
			restResponse = callWs(WS_URI + "/saveRekRs", mstPlRekeningRDto,
					HttpMethod.POST);
		}
		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
			backRek();
		} else if (restResponse.getStatus() == 2) {
			Messagebox.show("Nomor rekening sudah digunakan di Rumah Sakit lain");
		} else if (restResponse.getStatus() == 3) {
			Messagebox.show("Nomor rekening sudah digunakan transaksi");
		} else if (restResponse.getStatus() == 4) {
			Messagebox.show("Nomor rekening sudah digunakan pada bank ");
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}
	@NotifyChange({ "formRekGb", "daftarRekGb", "mstFndBankDto"})	
	@Command
	public void editRek(@BindingParam("item") final PlRekeningRDto selectedRek){
		if (selectedRek == null || selectedRek.getIdRsRek() == null) {
			showSmartMsgBox("W001");
			return;
		}
		mstFndBankDto.setNamaBank(selectedRek.getNamaBank());
		mstPlRekeningRDto.setCreatedBy(selectedRek.getCreatedBy());
		mstPlRekeningRDto.setCreationDate(selectedRek.getCreationDate());
		mstPlRekeningRDto.setIdRsRek(selectedRek.getIdRsRek());
		mstPlRekeningRDto.setKodeBank(selectedRek.getKodeBank());
		mstPlRekeningRDto.setKodeKantorJr(selectedRek.getKodeKantorJr());
		mstPlRekeningRDto.setKodeRumahsakit(selectedRek.getKodeRumahsakit());
		mstPlRekeningRDto.setLastUpdatedBy(selectedRek.getLastUpdatedBy());
		mstPlRekeningRDto.setLastUpdatedDate(selectedRek.getLastUpdatedDate());
		mstPlRekeningRDto.setNamaBank(selectedRek.getNamaBank());
		mstPlRekeningRDto.setNamaPemilik(selectedRek.getNamaPemilik());
		mstPlRekeningRDto.setNoRekening(selectedRek.getNoRekening());
		if(selectedRek.getFlagEnable().equalsIgnoreCase("Y")){
			setFlagActiveRek("Ya");
		}else if(selectedRek.getFlagEnable().equalsIgnoreCase("N")){
			setFlagActive("Tidak");
		}
		BindUtils.postNotifyChange(null, null, this, "mstPlRekeningRDto");
		BindUtils.postNotifyChange(null, null, this, "mstFndBankDto");
		BindUtils.postNotifyChange(null, null, this, "flagActiveRek");
		getPageInfo().setCustomAttribute("editRek");
//		addsRek();
		RestResponse restFndBank = callWs(WS_URI + "/findAllBank",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listBankDto = JsonUtil.mapJsonToListObject(
					restFndBank.getContents(), FndBankDto.class);
			setTotalSize(restFndBank.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listBankDto");
			
			for(FndBankDto a : listBankDto){
				if(selectedRek.getNamaBank().equalsIgnoreCase(a.getNamaBank())
						|| selectedRek.getKodeBank().equalsIgnoreCase(a.getKodeBank())){
					mstFndBankDto = a;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		setFormRekGb(true);
		setDaftarRekGb(false);
	}
	
	@NotifyChange({ })
	void addsRek(){
		
	}
	
	@NotifyChange({ "formRekGb", "daftarRekGb" })	
	@Command
	public void deleteRek(@BindingParam("item") final PlRekeningRDto selectedRek){
		if (selectedRek == null || selectedRek.getIdRsRek() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							selectedRek.setFlagEnable("N");
							restRespone = callWs(WS_URI + "/deleteRekRs", selectedRek,
									HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data successfully deleted");
								backRek();
							} else if(restRespone.getStatus()==2){
								showErrorMsgBox("Nomor rekening sudah ada di transaksi, tidak dapat menghapus rekening.");
							}else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});		
	}

	@Command
	public void refresh() {
		navigate("");
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}

	@Command
	public void delete(@BindingParam("item") final PlRumahSakitDto selected) {
		if (selected == null || selected.getKodeRumahsakit() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							selected.setFlagEnable("N");
							restRespone = callWs(WS_URI + "/delete", selected,
									HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data successfully deleted");
								refresh();
							} else if(restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showErrorMsgBox(restRespone.getMessage());
							} else if(restRespone.getStatus() == 2){
								showInfoMsgBox("Rumah sakit ini sudah digunakan pada tabel lain, tidak dapat dihapus");
							}
						}
					}
				});
	}

	public void deleteData() {
	}

	public Form getFormMaster() {
		return formMaster;
	}

	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}


	public boolean isDaftarPksGb() {
		return daftarPksGb;
	}

	public void setDaftarPksGb(boolean daftarPksGb) {
		this.daftarPksGb = daftarPksGb;
	}

	public boolean isDaftarRekGb() {
		return daftarRekGb;
	}

	public void setDaftarRekGb(boolean daftarRekGb) {
		this.daftarRekGb = daftarRekGb;
	}

	public boolean isFormPksGb() {
		return formPksGb;
	}

	public void setFormPksGb(boolean formPksGb) {
		this.formPksGb = formPksGb;
	}

	public boolean isFormRekGb() {
		return formRekGb;
	}

	public void setFormRekGb(boolean formRekGb) {
		this.formRekGb = formRekGb;
	}

	public boolean isListIndex() {
		return listIndex;
	}

	public void setListIndex(boolean listIndex) {
		this.listIndex = listIndex;
	}

	public String getFlagActive() {
		return flagActive;
	}

	public void setFlagActive(String flagActive) {
		this.flagActive = flagActive;
	}

	public String getJenisRs() {
		return jenisRs;
	}

	public void setJenisRs(String jenisRs) {
		this.jenisRs = jenisRs;
	}

	public boolean isFormBpjsPopup() {
		return formBpjsPopup;
	}

	public void setFormBpjsPopup(boolean formBpjsPopup) {
		this.formBpjsPopup = formBpjsPopup;
	}

	public Form getFormDetail() {
		return formDetail;
	}

	public void setFormDetail(Form formDetail) {
		this.formDetail = formDetail;
	}

	

	public String getFlagActiveRek() {
		return flagActiveRek;
	}

	public void setFlagActiveRek(String flagActiveRek) {
		this.flagActiveRek = flagActiveRek;
	}

	public boolean isBpjsField() {
		return bpjsField;
	}

	public void setBpjsField(boolean bpjsField) {
		this.bpjsField = bpjsField;
	}

	public String getKodeBank() {
		return kodeBank;
	}

	public void setKodeBank(String kodeBank) {
		this.kodeBank = kodeBank;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getKodeNama() {
		return kodeNama;
	}

	public void setKodeNama(String kodeNama) {
		this.kodeNama = kodeNama;
	}

	public String getKanWil() {
		return kanWil;
	}

	public void setKanWil(String kanWil) {
		this.kanWil = kanWil;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public boolean isRadioBpjs1() {
		return radioBpjs1;
	}

	public void setRadioBpjs1(boolean radioBpjs1) {
		this.radioBpjs1 = radioBpjs1;
	}

	public boolean isRadioBpjs2() {
		return radioBpjs2;
	}

	public void setRadioBpjs2(boolean radioBpjs2) {
		this.radioBpjs2 = radioBpjs2;
	}

	public boolean isRadioBpjs3() {
		return radioBpjs3;
	}

	public void setRadioBpjs3(boolean radioBpjs3) {
		this.radioBpjs3 = radioBpjs3;
	}

	public List<PlRumahSakitDto> getListDto() {
		return listDto;
	}

	public void setListDto(List<PlRumahSakitDto> listDto) {
		this.listDto = listDto;
	}

	public List<FndBankDto> getListBankDto() {
		return listBankDto;
	}

	public void setListBankDto(List<FndBankDto> listBankDto) {
		this.listBankDto = listBankDto;
	}

	public List<PlPksRDto> getListPksDto() {
		return listPksDto;
	}

	public void setListPksDto(List<PlPksRDto> listPksDto) {
		this.listPksDto = listPksDto;
	}

	public List<PlRekeningRDto> getListRekDto() {
		return listRekDto;
	}

	public void setListRekDto(List<PlRekeningRDto> listRekDto) {
		this.listRekDto = listRekDto;
	}

	public List<PlRsMapBpjDto> getListMapBpjDto() {
		return listMapBpjDto;
	}

	public void setListMapBpjDto(List<PlRsMapBpjDto> listMapBpjDto) {
		this.listMapBpjDto = listMapBpjDto;
	}

	public List<FndKantorJasaraharjaDto> getListKanWil() {
		return listKanWil;
	}

	public void setListKanWil(List<FndKantorJasaraharjaDto> listKanWil) {
		this.listKanWil = listKanWil;
	}

	public PlRumahSakitDto getMstPlRumahSakitDto() {
		return mstPlRumahSakitDto;
	}

	public void setMstPlRumahSakitDto(PlRumahSakitDto mstPlRumahSakitDto) {
		this.mstPlRumahSakitDto = mstPlRumahSakitDto;
	}

	public PlRumahSakitDto getSelected() {
		return selected;
	}

	public void setSelected(PlRumahSakitDto selected) {
		this.selected = selected;
	}

	public PlRsMapBpjDto getMstPlRsMapBpjDto() {
		return mstPlRsMapBpjDto;
	}

	public void setMstPlRsMapBpjDto(PlRsMapBpjDto mstPlRsMapBpjDto) {
		this.mstPlRsMapBpjDto = mstPlRsMapBpjDto;
	}

	public PlPksRDto getMstPlPksRDto() {
		return mstPlPksRDto;
	}

	public void setMstPlPksRDto(PlPksRDto mstPlPksRDto) {
		this.mstPlPksRDto = mstPlPksRDto;
	}

	public PlPksRDto getSelectedPks() {
		return selectedPks;
	}

	public void setSelectedPks(PlPksRDto selectedPks) {
		this.selectedPks = selectedPks;
	}

	public PlRekeningRDto getMstPlRekeningRDto() {
		return mstPlRekeningRDto;
	}

	public void setMstPlRekeningRDto(PlRekeningRDto mstPlRekeningRDto) {
		this.mstPlRekeningRDto = mstPlRekeningRDto;
	}

	public PlRekeningRDto getSelectedRek() {
		return selectedRek;
	}

	public void setSelectedRek(PlRekeningRDto selectedRek) {
		this.selectedRek = selectedRek;
	}

	public PlRsBpjDto getMstPlRsBpjDto() {
		return mstPlRsBpjDto;
	}

	public void setMstPlRsBpjDto(PlRsBpjDto mstPlRsBpjDto) {
		this.mstPlRsBpjDto = mstPlRsBpjDto;
	}

	public FndBankDto getMstFndBankDto() {
		return mstFndBankDto;
	}

	public void setMstFndBankDto(FndBankDto mstFndBankDto) {
		this.mstFndBankDto = mstFndBankDto;
	}

	public String getSearchKanwil() {
		return searchKanwil;
	}

	public void setSearchKanwil(String searchKanwil) {
		this.searchKanwil = searchKanwil;
	}

	public List<PlRumahSakitDto> getListKodeNamaRsDto() {
		return listKodeNamaRsDto;
	}

	public void setListKodeNamaRsDto(List<PlRumahSakitDto> listKodeNamaRsDto) {
		this.listKodeNamaRsDto = listKodeNamaRsDto;
	}

	public String getSearchKodeNamaRs() {
		return searchKodeNamaRs;
	}

	public void setSearchKodeNamaRs(String searchKodeNamaRs) {
		this.searchKodeNamaRs = searchKodeNamaRs;
	}



	public String getSearchLoketPj() {
		return searchLoketPj;
	}



	public void setSearchLoketPj(String searchLoketPj) {
		this.searchLoketPj = searchLoketPj;
	}

	public List<PlRsMapBpjDto> getListLoketPjDto() {
		return listLoketPjDto;
	}

	public void setListLoketPjDto(List<PlRsMapBpjDto> listLoketPjDto) {
		this.listLoketPjDto = listLoketPjDto;
	}

	public PlRsMapBpjDto getLoketPjDto() {
		return loketPjDto;
	}

	public void setLoketPjDto(PlRsMapBpjDto loketPjDto) {
		this.loketPjDto = loketPjDto;
	}

	public List<PlRumahSakitDto> getListDtoCopy() {
		return listDtoCopy;
	}

	public void setListDtoCopy(List<PlRumahSakitDto> listDtoCopy) {
		this.listDtoCopy = listDtoCopy;
	}

	public List<PlRsMapBpjDto> getListLoketPjDtoCopy() {
		return listLoketPjDtoCopy;
	}

	public void setListLoketPjDtoCopy(List<PlRsMapBpjDto> listLoketPjDtoCopy) {
		this.listLoketPjDtoCopy = listLoketPjDtoCopy;
	}

	public List<String> getListJenisRs() {
		return listJenisRs;
	}

	public void setListJenisRs(List<String> listJenisRs) {
		this.listJenisRs = listJenisRs;
	}

	public List<String> getListStatusRs() {
		return listStatusRs;
	}

	public void setListStatusRs(List<String> listStatusRs) {
		this.listStatusRs = listStatusRs;
	}

	public String getFlagActivePks() {
		return flagActivePks;
	}

	public void setFlagActivePks(String flagActivePks) {
		this.flagActivePks = flagActivePks;
	}

}
