package ui.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Window;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

import share.FndBankDto;
import share.FndCamatDto;
import share.FndKantorJasaraharjaDto;
import share.PlInstansiDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass=true)
public class MstFndCamatVmd extends BaseVmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterLokasi/_index.zul";
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterLokasi/_detail.zul";
	private final String DETAIL_PAGE_PATH2 = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterLokasi/detail2.zul";
	
	private final String WS_URI = "/MstLokasi";
	private final String WS_URI_LOV = "/Lov";

	private Form formMaster = new SimpleForm();
	private List<FndCamatDto> listDto = new ArrayList<>();
	private FndCamatDto mstLokasiDto = new FndCamatDto();
	private FndCamatDto selected = new FndCamatDto();
	
	private boolean listIndex = false;
	private String flagEnableDesc;
	private boolean flagEnable=true;
	private List<FndCamatDto> listProvinsiDto = new ArrayList<>();
	private List<FndCamatDto> listProv = new ArrayList<>();
	private List<FndCamatDto> listKota = new ArrayList<>();
	private List<FndCamatDto> listCamat = new ArrayList<>();
	private String kodeCamat;
	private String namaCamat;
	private String kodeProvinsi;
	private boolean firstLoad = true;
	private List<FndKantorJasaraharjaDto> listKantor = new ArrayList<>();
	private FndKantorJasaraharjaDto kantorJR = new FndKantorJasaraharjaDto();
	
	private String searchProvinsi;
	private String searchIndex;
	private String search;
	private String kodeProvinsiForSearch;
	
	private int pageSize = 5;
	
	private int kantorSize = 0;
	private int kantorSeq = 0;
	private String kantorOrder = "kodeKantorJr";
	private String kantorDirection = "ASC";
	
	private String kantorCari;
	private String namaKantor;
	private String kodeKantor;
	
	protected void loadList(){
		searchProvinsi();
	}
	
	@Command
	public void close(@BindingParam("item") Window win){
		/*if (winLov == null)
			throw new RuntimeException(
					"id popUp tidak sama dengan viewModel");
		winLov.detach();*/
		win.detach();	
			
	}
	
	@Command
	public void openDetail(@BindingParam("popup") String popup, @BindingParam("item") FndCamatDto dto,
			@Default("popUpHandler") @BindingParam("mode") String mode){
		Map<String, Object> args = new HashMap<>();
		if(dto!=null){
			this.selected = dto;
			System.out.println("selected : "+JsonUtil.getJson(dto));
		}
		args.put("dto", dto);
//		args.put("detailHandler", globalHandleMethodName);
//		args.put("PageInfo", getPageInfo());
		if(mode!= null && mode.equalsIgnoreCase("add")){
			add();
		}else{
			edit();
		}
//		getPageInfo().setEditMode(true);
		if(dto!=null){
			Executions.getCurrent().setAttribute("obj", dto);
		}
		
		try {
			System.out.println(popup);
			((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	@Command
	public void searchProvinsi(){
		if(getSearchProvinsi()==null||getSearchProvinsi().equalsIgnoreCase("")){
			setSearchProvinsi("%%");
		}else if(getSearchProvinsi().contains("-")){
			setSearchProvinsi(getSearchProvinsi().substring(0,2));
		}
		String searchs = getSearchProvinsi();
		Map<String, Object> input = new HashMap<>();
		input.put("search", searchs);
		RestResponse rest = new RestResponse();
		try {			
//			rest = callWs(WS_URI_LOV + "/getListProvinsi/"+getSearchProvinsi(),
//					input, HttpMethod.POST);
			rest = callWs(WS_URI_LOV + "/getListProvinsi/",
					input, HttpMethod.POST);
			listProvinsiDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndCamatDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listProvinsiDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command
	public void getKantorLIst(){
		getListKantr();
	}
	
	//TODO: listKAntor
	@NotifyChange({"listKantor","kantorSize"})
	private void getListKantr() {
		Map<String, Object> input = new HashMap<>();
		List<FndKantorJasaraharjaDto> listKan = new ArrayList<>();
		RestResponse response = new RestResponse();
		listKantor = new ArrayList<>();
		
		input.put("option", "listKantor");
		input.put("seq", getKantorSeq());
		input.put("size", 10);
		input.put("direction", getKantorDirection());
		input.put("orderBy", getKantorOrder());
		try{
			response = callWs(WS_URI + "/general", input, HttpMethod.POST);
			listKan = JsonUtil.mapJsonToListObject(response.getContents(),
					FndKantorJasaraharjaDto.class);
			setTotalSize(response.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listProv");
			System.out.println("======================");
			System.out.println(JsonUtil.getJson(listKan));
			listKantor.addAll(listKan);
			System.out.println(JsonUtil.getJson(listKantor));
			kantorSize = Integer.parseInt(String.valueOf(response.getTotalRecords()));
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		BindUtils.postNotifyChange(null, null, MstFndCamatVmd.this, "kantorSize");
		BindUtils.postNotifyChange(null, null, MstFndCamatVmd.this, "listKantor");
		
	}
	
	@Command
	public void searchKantor(@BindingParam("val")String value){
		if(value == null){
			showWarningMsgBox("pencarian kosong");
			return;
		}
		
		Map<String, Object> input = new HashMap<>();
		List<FndKantorJasaraharjaDto> listKan = new ArrayList<>();
		RestResponse response = new RestResponse();
		listKantor = new ArrayList<>();
		
		input.put("search", value);
		input.put("option", "searchKantor");
		input.put("seq", getKantorSeq());
		input.put("size", 10);
		input.put("direction", getKantorDirection());
		input.put("orderBy", getKantorOrder());
		try{
			response = callWs(WS_URI + "/general", input, HttpMethod.POST);
			listKan = JsonUtil.mapJsonToListObject(response.getContents(),
					FndKantorJasaraharjaDto.class);
			setTotalSize(response.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listProv");
			System.out.println("======================");
			System.out.println(JsonUtil.getJson(listKan));
			listKantor.addAll(listKan);
			System.out.println(JsonUtil.getJson(listKantor));
			kantorSize = Integer.parseInt(String.valueOf(response.getTotalRecords()));
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		BindUtils.postNotifyChange(null, null, MstFndCamatVmd.this, "kantorSize");
		BindUtils.postNotifyChange(null, null, MstFndCamatVmd.this, "listKantor");
	}
	
	public void listProv() {
		RestResponse restProv = callWs(WS_URI + "/findProv",
				new HashMap<String, Object>(), HttpMethod.POST);
			try{
				listProv = JsonUtil.mapJsonToListObject(restProv.getContents(),
						FndCamatDto.class);
				
				setTotalSize(restProv.getTotalRecords());
				BindUtils.postNotifyChange(null, null, this, "listProv");
				}catch(Exception e){
					e.printStackTrace();
				}
	}
	
	@Command
	public void changePageSize(){
		setPageSize(getPageSize());
	
		setListIndex(true);
		loadList();
		
		BindUtils.postNotifyChange(null, null, this, "pageSize");
		
	}
	
	@NotifyChange({"kodeProvinsi" , "listKota" })
	@Command()
	public void findKota() {
		RestResponse restKota = callWs(WS_URI + "/findKota/" + mstLokasiDto.getKodeProvinsi(),
				new HashMap<String, Object>(), HttpMethod.POST);
			try{
				listKota = JsonUtil.mapJsonToListObject(restKota.getContents(),
						FndCamatDto.class);
				setTotalSize(restKota.getTotalRecords());
				BindUtils.postNotifyChange(null, null, this, "listKota");
				}catch(Exception e){
					e.printStackTrace();
				}
	}
	
	public void listCamat() {
		listCamat = (List<FndCamatDto>) Executions.getCurrent().getAttribute("listCamat");
		if(listCamat==null || listCamat.size()==0){
			RestResponse restCamat = callWs(WS_URI + "/findLokasi",
					new HashMap<String, Object>(), HttpMethod.POST);
				try{
					listCamat = JsonUtil.mapJsonToListObject(restCamat.getContents(),
							FndCamatDto.class);
					
					setTotalSize(restCamat.getTotalRecords());
					BindUtils.postNotifyChange(null, null, this, "listCamat");
					}catch(Exception e){
						e.printStackTrace();
					}
			
		}
		
	}
	
	
//	@Command("addLokasi")
	private void addLokasi() {
		getPageInfo().setAddMode(true);
//		navigate(DETAIL_PAGE_PATH);
		
	}
	
//	@Command("add")
	private void add() {
		
		getPageInfo().setAddMode(true);
//		navigate(DETAIL_PAGE_PATH2);
	}
	
	
	@Override
	public void onAdd(){
		getListKantr();
		mstLokasiDto = new FndCamatDto(); 
		listProv();
		findKota();
	}
	
	@Command("back")
	public void back(){
		Executions.getCurrent().setAttribute("listCamat", listCamat);
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}
	
	@Command("searchIndex")
	public void searchIndex(){
		setListIndex(true);
		if(getSearchProvinsi()==null||getSearchProvinsi().equalsIgnoreCase("")||getSearchProvinsi().equalsIgnoreCase("%%")){
			setKodeProvinsiForSearch("%%");
		}else if(getSearchProvinsi().contains("-")){
			setKodeProvinsiForSearch(getSearchProvinsi().substring(0,2));
		}else{
			try {			
				Map<String, Object> in = new HashMap<>();
				in.put("search", getSearchProvinsi());
				RestResponse rest = callWs(WS_URI_LOV + "/getListProvinsi/",
						in, HttpMethod.POST);
				List<FndCamatDto> dto = JsonUtil.mapJsonToListObject(rest.getContents(),
						FndCamatDto.class);		
				for(FndCamatDto a : dto){
					setKodeProvinsiForSearch(a.getKodeProvinsi());				
				}
				BindUtils.postNotifyChange(null, null, this, "kodeProvinsiForSearch");	
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(getSearchIndex()==null||getSearchIndex().equalsIgnoreCase("")){
			setSearchIndex("%%");
		}
		
		RestResponse rest = callWs(WS_URI + "/all/"+getKodeProvinsiForSearch()+"/"+getSearchIndex(),
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					FndCamatDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listDto");
			BindUtils.postNotifyChange(null, null, this, "listIndex");
			
		} catch (Exception e) {
			e.printStackTrace();

		}
		
//		firstLoad = false;
//		if (getSearch().contains("-"))
//			{
//			String a=getSearch().substring(0,2)+"%";
//			setSearch(a);
//			}
//		
//		setListIndex(true);
//		loadList();
//		
//		BindUtils.postNotifyChange(null, null, this, "sortAndSearch");
//		BindUtils.postNotifyChange(null, null, this, "listIndex");

	}
	
	@NotifyChange({"listKota","listProv","kodeCamat","namaCamat"})
	@Command
	public void save() {
		RestResponse restResponse = new RestResponse();
		if(getKodeCamat()==null){
			Messagebox.show("Kode Camat Tidak Boleh Kosong");
			return;
		}
		if(getNamaCamat()==null){
			Messagebox.show("Nama Camat Tidak Boleh Kosong");
			return;
		}
		if(mstLokasiDto.getKodeProvinsi()==null){
			Messagebox.show("Kode Provinsi Tidak Boleh Kosong");
			return;
		}
			if (getPageInfo().isAddMode()) {
				
				if (isFlagEnable() == true) {
					mstLokasiDto.setFlagEnable("Y");
				} else {
					mstLokasiDto.setFlagEnable("N");
				}
				mstLokasiDto.setKodeCamat(getKodeCamat());
				mstLokasiDto.setNamaCamat(getNamaCamat());
				mstLokasiDto.setKodeProvinsi(mstLokasiDto.getKodeProvinsi());
				mstLokasiDto.setNamaProvinsi(mstLokasiDto.getNamaProvinsi());
				mstLokasiDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
				mstLokasiDto.setCreatedDate(new Date());
				mstLokasiDto.setMode("add");
				try {
					restResponse = callWs(WS_URI, mstLokasiDto, HttpMethod.POST);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					restResponse.setStatus(1);
					restResponse.setMessage("E003");
				}
			} else {
				if (isFlagEnable() == true) {
					mstLokasiDto.setFlagEnable("Y");
				} else {
					mstLokasiDto.setFlagEnable("N");
				}
				mstLokasiDto.setKodeCamat(getKodeCamat());
				mstLokasiDto.setNamaCamat(getNamaCamat());
				mstLokasiDto.setUpdatedBy(getCurrentUserSessionJR().getLoginID());
				mstLokasiDto.setUpdatedDate(new Date());
				mstLokasiDto.setMode("edit");
				try {
					restResponse = callWs(WS_URI + "/update", mstLokasiDto,
							HttpMethod.POST);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					restResponse.setStatus(1);
					restResponse.setMessage("E003");
				}
				
			}
			if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(restResponse.getMessage());
				navigate(INDEX_PAGE_PATH);
			} else {
				showErrorMsgBox(restResponse.getMessage());
			}			
		
	}
	
//	@Command("edit")
//	public void edit(@BindingParam("item")FndCamatDto selected) {
//		if(selected == null || selected.getKodeCamat() == null){
//			showSmartMsgBox("W001");
//			return;
//		}
//
//		Executions.getCurrent().setAttribute("obj", selected);
//		Executions.getCurrent().setAttribute("listCamat", listCamat);
//		getPageInfo().setEditMode(true);
//		navigate(DETAIL_PAGE_PATH);
//	}
	
	private void edit(){
		getPageInfo().setEditMode(true);
		
	}


	@Override
	protected void onEdit() {
		getListKantr();;
		mstLokasiDto = new FndCamatDto();
		mstLokasiDto = (FndCamatDto) Executions.getCurrent()
				.getAttribute("obj");
		
		System.out.println("on EDITT");
		System.out.println(JsonUtil.getJson(mstLokasiDto));
		
		listCamat = (List<FndCamatDto>) Executions.getCurrent().getAttribute("listCamat");

		if (mstLokasiDto!= null && mstLokasiDto.getFlagEnable() != null &&  mstLokasiDto.getFlagEnable().equalsIgnoreCase("Y")) {
			setFlagEnable(true);
			setKodeCamat(mstLokasiDto.getKodeCamat());
			setNamaCamat(mstLokasiDto.getNamaCamat());
			formMaster.setField("namaCamat", mstLokasiDto.getNamaCamat());
			formMaster.setField("kodeProvinsi", mstLokasiDto.getKodeProvinsi());
			formMaster.setField("namaProvinsi", mstLokasiDto.getNamaProvinsi());
			formMaster.setField("kodeKabKota", mstLokasiDto.getKodeKabkota());
			formMaster.setField("namaKabKota", mstLokasiDto.getNamaKabkota());
		} else {
			setFlagEnable(false);
		}

		

		BindUtils.postNotifyChange(null, null, this, "kodeCamat");
		BindUtils.postNotifyChange(null, null, this, "namaCamat");
		BindUtils.postNotifyChange(null, null, formMaster, "kodeProvinsi");
		BindUtils.postNotifyChange(null, null, formMaster, "namaProvinsi");
		BindUtils.postNotifyChange(null, null, formMaster, "kodeKabkota");
		BindUtils.postNotifyChange(null, null, formMaster, "namaKabkota");
		BindUtils.postNotifyChange(null, null, this, "flagEnable");

	}
	@Command
	public void refresh() {
		navigate("");
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}
	
	@Command("delete")
	public void delete(@BindingParam("item") final FndCamatDto selected) {
//		Messagebox.show("test "+selected.getKodeCamat()+selected.getKodeNamaCamat()+
//				selected.getKodeKabKota()+selected.getKodeProvinsi()+selected.getNamaProvinsi());
		if (selected == null || selected.getKodeCamat() == null) {
			showSmartMsgBox("W001");
			return;
		}
		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							
							
							selected.setFlagEnable("N");
							restRespone = callWs(WS_URI + "/delete", selected,
									HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								// showInfoMsgBox(restRespone.getMessage());
								refresh();
							} else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});
	}

	
	
	public void deleteData() {
		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							
							selected.setFlagEnable("N");
							selected.setKodeCamat(getKodeCamat());
							restRespone = callWs(WS_URI + "/delete", selected,
									HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								// showInfoMsgBox(restRespone.getMessage());
								refresh();
							} else {
								showErrorMsgBox(restRespone.getMessage());
							}
						}
					}
				});
	}


	
	public Form getFormMaster() {
		return formMaster;
	}
	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}
	
	public boolean isListIndex() {
		return listIndex;
	}
	public void setListIndex(boolean listIndex) {
		this.listIndex = listIndex;
	}
	
	public String getDETAIL_PAGE_PATH() {
		return DETAIL_PAGE_PATH;
	}
	public String getINDEX_PAGE_PATH() {
		return INDEX_PAGE_PATH;
	}

	public String getFlagEnableDesc() {
		return flagEnableDesc;
	}

	public void setFlagEnableDesc(String flagEnableDesc) {
		this.flagEnableDesc = flagEnableDesc;
	}

	public void setFlagEnable(boolean flagEnable) {
		this.flagEnable = flagEnable;
	}

	public boolean isFlagEnable() {
		return flagEnable;
	}

	public String getKodeCamat() {
		return kodeCamat;
	}

	public void setKodeCamat(String kodeCamat) {
		this.kodeCamat = kodeCamat;
	}

	public String getNamaCamat() {
		return namaCamat;
	}

	public void setNamaCamat(String namaCamat) {
		this.namaCamat = namaCamat;
	}

	public String getKodeProvinsi() {
		return kodeProvinsi;
	}

	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public List<FndCamatDto> getListDto() {
		return listDto;
	}

	public void setListDto(List<FndCamatDto> listDto) {
		this.listDto = listDto;
	}

	public FndCamatDto getMstLokasiDto() {
		return mstLokasiDto;
	}

	public void setMstLokasiDto(FndCamatDto mstLokasiDto) {
		this.mstLokasiDto = mstLokasiDto;
	}

	public FndCamatDto getSelected() {
		return selected;
	}

	public void setSelected(FndCamatDto selected) {
		this.selected = selected;
	}

	public List<FndCamatDto> getListProv() {
		return listProv;
	}

	public void setListProv(List<FndCamatDto> listProv) {
		this.listProv = listProv;
	}

	public List<FndCamatDto> getListKota() {
		return listKota;
	}

	public void setListKota(List<FndCamatDto> listKota) {
		this.listKota = listKota;
	}

	public List<FndCamatDto> getListCamat() {
		return listCamat;
	}

	public void setListCamat(List<FndCamatDto> listCamat) {
		this.listCamat = listCamat;
	}

	public String getSearchProvinsi() {
		return searchProvinsi;
	}

	public void setSearchProvinsi(String searchProvinsi) {
		this.searchProvinsi = searchProvinsi;
	}

	public List<FndCamatDto> getListProvinsiDto() {
		return listProvinsiDto;
	}

	public void setListProvinsiDto(List<FndCamatDto> listProvinsiDto) {
		this.listProvinsiDto = listProvinsiDto;
	}

	public String getKodeProvinsiForSearch() {
		return kodeProvinsiForSearch;
	}

	public void setKodeProvinsiForSearch(String kodeProvinsiForSearch) {
		this.kodeProvinsiForSearch = kodeProvinsiForSearch;
	}

	public List<FndKantorJasaraharjaDto> getListKantor() {
		return listKantor;
	}

	public void setListKantor(List<FndKantorJasaraharjaDto> listKantor) {
		this.listKantor = listKantor;
	}

	public int getKantorSize() {
		return kantorSize;
	}

	public void setKantorSize(int kantorSize) {
		this.kantorSize = kantorSize;
	}

	public int getKantorSeq() {
		return kantorSeq;
	}

	public void setKantorSeq(int kantorSeq) {
		this.kantorSeq = kantorSeq;
		getListKantr();
	}

	public String getKantorOrder() {
		return kantorOrder;
	}

	public void setKantorOrder(String kantorOrder) {
		this.kantorOrder = kantorOrder;
	}

	public String getKantorDirection() {
		return kantorDirection;
	}

	public void setKantorDirection(String kantorDirection) {
		this.kantorDirection = kantorDirection;
	}

	public FndKantorJasaraharjaDto getKantorJR() {
		return kantorJR;
	}

	public void setKantorJR(FndKantorJasaraharjaDto kantorJR) {
		this.kantorJR = kantorJR;
	}

	public String getKantorCari() {
		return kantorCari;
	}

	public void setKantorCari(String kantorCari) {
		this.kantorCari = kantorCari;
	}

	public String getNamaKantor() {
		return namaKantor;
	}

	public void setNamaKantor(String namaKantor) {
		this.namaKantor = namaKantor;
	}

	public String getKodeKantor() {
		return kodeKantor;
	}

	public void setKodeKantor(String kodeKantor) {
		this.kodeKantor = kodeKantor;
	}
}
