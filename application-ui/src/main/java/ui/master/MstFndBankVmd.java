package ui.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import share.FndBankDto;
import share.PlPengajuanSantunanDto;
import share.PlRekeningRDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.ui.PageInfo;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass=true)
public class MstFndBankVmd extends BaseVmd implements Serializable{
private static final long serialVersionUID = 1L;
	
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterBank/_index.zul";
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterBank/_detail.zul";
	
	private final String WS_URI = "/fndBank";

	private Form formMaster = new SimpleForm();
	private List<FndBankDto> listDto = new ArrayList<>();
	private FndBankDto mstFndBankDto = new FndBankDto();
	private FndBankDto selected = new FndBankDto();
	
	private List<PlRekeningRDto> listRekDto = new ArrayList<>();
	
	private boolean status=true;
	private String statusFlag="Ya";
	private boolean daftarRekGb = true;
	private boolean addText = true;
	
	private String searchRekening;
	private String searchIndex;
	private int pageSize= 5;
	

	@Wire("#winBank")
	private Window winLov;
	
	@Command
	public void close(@BindingParam("item") Window win){
		/*if (winLov == null)
			throw new RuntimeException(
					"id popUp tidak sama dengan viewModel");
		winLov.detach();*/
		win.detach();	
			
	}
	
	protected void loadList(){
		
		if(getSearchIndex()==null || getSearchIndex().equalsIgnoreCase("")){
			setSearchIndex("%%");
		}
		RestResponse rest = callWs(WS_URI+"/all" +"/"+getSearchIndex(),
				new HashMap<String, Object>(), HttpMethod.POST);
		
		try{
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(), FndBankDto.class);
			setTotalSize(rest.getTotalRecords());
			
			BindUtils.postNotifyChange(null, null, this, "listDto");
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void loadListRek() {
		if(getSearchRekening()==null || getSearchRekening().equalsIgnoreCase("")){
			setSearchRekening("%%");
		}
		RestResponse rest = callWs(WS_URI + "/findRek/"
				+ mstFndBankDto.getKodeBank()+"/"+getSearchRekening(),
				new HashMap<String, Object>(), HttpMethod.POST);
		//Messagebox.show("INI SUDAH MUNCUL YA DATA BANK ::" + mstFndBankDto.getKodeBank());
		try {
			listRekDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRekeningRDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listRekDto");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Command("add")
	public void add() {
		getPageInfo().setAddMode(true);
//		navigate(DETAIL_PAGE_PATH);
	}
	
	@NotifyChange({"daftarRekGb" , "addtext" })
	@Override
	public void onAdd(){
		mstFndBankDto = new FndBankDto(); 
		setDaftarRekGb(false);
		setAddText(true);
	}
	
	
	@Command("saveDataBank")
	public void save(){
		RestResponse rest = new RestResponse();
		if(mstFndBankDto==null || mstFndBankDto.getKodeBank() == null || mstFndBankDto.getKodeBank2()==null ||
				mstFndBankDto.getKodeBank().isEmpty() || mstFndBankDto.getKodeBank2().isEmpty()){
			showWarningMsgBox("Silahkan isi ID dan Kode Bank");
			return;
		}
		if(mstFndBankDto.getKodeBank().length()<8){
			showWarningMsgBox("Kode Bank Min. 8 Digit");
		}else{
			if (getPageInfo().isAddMode()) {
				mstFndBankDto.setMode("add");
					mstFndBankDto.setKodeBank(mstFndBankDto.getKodeBank());
					mstFndBankDto.setNamaBank(mstFndBankDto.getNamaBank());
					mstFndBankDto.setKodeBank2(mstFndBankDto.getKodeBank2());
					if(getStatusFlag().equalsIgnoreCase("Ya")){
						mstFndBankDto.setStatus("Y");				
					}else if(getStatusFlag().equalsIgnoreCase("Tidak")){
						mstFndBankDto.setStatus("N");
					}
					mstFndBankDto.setLastUpdatedBy(getCurrentUserSessionJR().getLoginID());
					mstFndBankDto.setLastUpdatedDate(new Date());				
					rest = callWs(WS_URI, mstFndBankDto, HttpMethod.POST);
					
			}else {
				mstFndBankDto.setMode("edit");
				if(getStatusFlag().equalsIgnoreCase("Ya")){
					mstFndBankDto.setStatus("Y");				
				}else if(getStatusFlag().equalsIgnoreCase("Tidak")){
					mstFndBankDto.setStatus("N");
				}
				mstFndBankDto.setKodeBank(mstFndBankDto.getKodeBank());
				mstFndBankDto.setNamaBank(mstFndBankDto.getNamaBank());
				mstFndBankDto.setKodeBank2(mstFndBankDto.getKodeBank2());
				mstFndBankDto.setLastUpdatedBy(getCurrentUserSessionJR().getLoginID());
				mstFndBankDto.setLastUpdatedDate(new Date());
				
				rest = callWs(WS_URI, mstFndBankDto, HttpMethod.POST);
			}
			if (rest.getStatus() == CommonConstants.OK_REST_STATUS) {
				showInfoMsgBox(rest.getMessage());
				back();
			}else if(rest.getStatus() == 2){
				showWarningMsgBox("ID Bank sudah dipakai, silahkan pilih kode lain");
				return;
			}else if(rest.getStatus() == 3){
				showWarningMsgBox("Kode Bank sudah dipakai, silahkan pilih kode lain");
			} else {
				showErrorMsgBox(rest.getMessage());
			}
		}
	}
	
	
	@Command("back")
	public void back(){
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}
	
//	@Command("edit")
//	public void edit(@BindingParam("item")FndBankDto selected) {
//		if(selected == null || selected.getKodeBank() == null){
//			showSmartMsgBox("W001");
//			return;
//		}
//
//		Executions.getCurrent().setAttribute("obj", selected);
//
//		getPageInfo().setEditMode(true);
//		navigate(DETAIL_PAGE_PATH);
//	}
	
	private void edit(){
		getPageInfo().setEditMode(true);
	}
	
	@Command
	public void openDetail(@BindingParam("popup") String popup, @BindingParam("item") FndBankDto dto,
			@Default("popUpHandler") @BindingParam("mode") String mode){
		Map<String, Object> args = new HashMap<>();
		if(dto!=null){
			this.selected = dto;
			System.out.println("selected : "+JsonUtil.getJson(dto));
		}
		args.put("dto", dto);
//		args.put("detailHandler", globalHandleMethodName);
		args.put("PageInfo", getPageInfo());
		if(mode!= null && mode.equalsIgnoreCase("add")){
			add();
		}else{
			edit();
		}
//		getPageInfo().setEditMode(true);
		if(dto!=null){
			Executions.getCurrent().setAttribute("obj", dto);
		}
		
		try {
			System.out.println(popup);
			((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
			BindUtils.postGlobalCommand(null, null, "getDocsAbsah", args);
		} catch (UiException u) {
			u.printStackTrace();
		}

		
	}
	
	@NotifyChange( {"daftarRekGb" , "addtext"} )
	@Override
	protected void onEdit() {
		mstFndBankDto = (FndBankDto) Executions.getCurrent().getAttribute(
				"obj");
		setAddText(false);
		setDaftarRekGb(true);
		loadListRek();
		if(mstFndBankDto.getStatus().equalsIgnoreCase("Y")){
			setStatusFlag("Aktif");
		}else{
			setStatusFlag("Tidak Aktif");
		}
		formMaster.setField("kodeBank", mstFndBankDto.getKodeBank());
		formMaster.setField("namaBank", mstFndBankDto.getNamaBank());
		formMaster.setField("kodeBank2", mstFndBankDto.getKodeBank2());
		formMaster.setField("jumlahRekening", mstFndBankDto.getJumlahRekening());
		//formMaster.setField("companyCode", mstMobileRoleDetailDto.getCompanyCode());
		

		BindUtils.postNotifyChange(null, null, formMaster, "kodeBank");
		BindUtils.postNotifyChange(null, null, formMaster, "namaBank");
		BindUtils.postNotifyChange(null, null, formMaster, "kodeBank2");
		BindUtils.postNotifyChange(null, null, formMaster, "jumlahRekening");
		BindUtils.postNotifyChange(null, null, this, "statusFlag");
	//	BindUtils.postNotifyChange(null, null, formMaster, "status");
	}
	
	@Command("searchIndex")
	public void searchIndex() {
	//	Messagebox.show("Mau Search apa ?" +getSearch());
	//	setSearch(getSearch());
		loadList();
	//	BindUtils.postNotifyChange(null, null, this, "sortAndSearch");
	}
	
	@Command
	public void searchRekening(){
		loadListRek();	
	}	
	//ini untuk paging
	
	@Command
	public void changePageSize(){
		setPageSize(getPageSize());
		
		loadList();
		loadListRek();
		BindUtils.postNotifyChange(null, null, this, "pageSize");
	}
	
	public Form getFormMaster() {
		return formMaster;
	}
	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}

	public boolean isStatus() {
		return status;
	}

	
	public String getStatusFlag() {
		return statusFlag;
	}

	public void setStatusFlag(String statusFlag) {
		this.statusFlag = statusFlag;
	}

	public boolean isDaftarRekGb() {
		return daftarRekGb;
	}

	public void setDaftarRekGb(boolean daftarRekGb) {
		this.daftarRekGb = daftarRekGb;
	}
	
	public boolean isAddText() {
		return addText;
	}

	public void setAddText(boolean addText) {
		this.addText = addText;
	}

	public String getSearchRekening() {
		return searchRekening;
	}

	public void setSearchRekening(String searchRekening) {
		this.searchRekening = searchRekening;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public List<FndBankDto> getListDto() {
		return listDto;
	}

	public void setListDto(List<FndBankDto> listDto) {
		this.listDto = listDto;
	}

	public FndBankDto getMstFndBankDto() {
		return mstFndBankDto;
	}

	public void setMstFndBankDto(FndBankDto mstFndBankDto) {
		this.mstFndBankDto = mstFndBankDto;
	}

	public FndBankDto getSelected() {
		return selected;
	}

	public void setSelected(FndBankDto selected) {
		this.selected = selected;
	}

	public List<PlRekeningRDto> getListRekDto() {
		return listRekDto;
	}

	public void setListRekDto(List<PlRekeningRDto> listRekDto) {
		this.listRekDto = listRekDto;
	}

	
}
