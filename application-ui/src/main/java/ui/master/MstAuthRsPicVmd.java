package ui.master;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zul.Window;

import share.AuthRsPicDto;
import share.AuthUserDto;
import share.AuthUserGadgetDto;
import share.DasiJrRefCodeDto;
import share.FndKantorJasaraharjaDto;
import share.PlDataKecelakaanDto;
import share.PlRumahSakitDto;
import common.model.RestResponse;
import common.model.UserSession;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass = true)
public class MstAuthRsPicVmd extends BaseVmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterPicRs/_detail.zul";
	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterPicRs/_index.zul";

	private final String WS_URI = "/MasterRsPic";
	private final String WS_URI_LOV = "/Lov";

	private List<FndKantorJasaraharjaDto> listKantorWilayah = new ArrayList<>();
	private List<FndKantorJasaraharjaDto> listKantorWilayahCopy = new ArrayList<>();
	private FndKantorJasaraharjaDto kantorWilayah = new FndKantorJasaraharjaDto();

	private List<AuthUserDto> listAuthUserDto = new ArrayList<>();
	private List<AuthUserDto> listAuthUserDtoCopy = new ArrayList<>();
	private List<PlRumahSakitDto> listRsDto = new ArrayList<>();
	private List<PlRumahSakitDto> listRsDtoCopy = new ArrayList<>();
	
	private List<AuthUserGadgetDto> listUserGadgetDto = new ArrayList<>();

	private AuthUserDto mstAuthUserDto = new AuthUserDto();
	private AuthUserGadgetDto mstAuthUserGadgetDto = new AuthUserGadgetDto();
	private AuthRsPicDto mstAuthRsPicDto = new AuthRsPicDto();
	private PlRumahSakitDto mstPlRumahSakitDto = new PlRumahSakitDto();

	private Form formMaster = new SimpleForm();

	private boolean daftarRsGb = true;
	private boolean tambahRsGb = false;

	private int pageSize = 5;
	private int pageSizeRs = 5;

	private List<String> listGadgetStatus = new ArrayList<>();
	private String gadgetStatus;
	private List<DasiJrRefCodeDto> listJenisGadget = new ArrayList<>();
	private DasiJrRefCodeDto jenisGadget = new DasiJrRefCodeDto();

	private String searchRs;
	private String searchIndex;

	UserSessionJR userSession = (UserSessionJR) Sessions.getCurrent()
			.getAttribute(UIConstants.SESS_LOGIN_ID);

	@Command
	public void close(@BindingParam("item") Window win) {
		win.detach();
	}

	@Command
	public void openDetail(@BindingParam("popup") String popup,
			@BindingParam("item") AuthUserDto dto,
			@Default("popUpHandler") @BindingParam("mode") String mode) {
		Map<String, Object> args = new HashMap<>();
		if (dto != null) {
			this.mstAuthUserDto = dto;
		}
		args.put("dto", dto);
		edit();
		if (dto != null) {
			Executions.getCurrent().setAttribute("obj", dto);
		}

		try {
			System.out.println(popup);
			((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH
					+ popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}

	private void edit() {
		getPageInfo().setEditMode(true);
	}

	protected void loadList() {
		searchWilayahKantor();
		search();
	}

	@Command("edit")
	public void edit(@BindingParam("item") AuthUserDto selected) {
		if (selected == null || selected.getLogin() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Executions.getCurrent().setAttribute("obj", selected);
		getPageInfo().setEditMode(true);
		navigate(DETAIL_PAGE_PATH);
	}

	public void onEdit() {
		mstAuthUserDto = (AuthUserDto) Executions.getCurrent().getAttribute(
				"obj");

		RestResponse rest = callWs(
				WS_URI + "/getUserGadget/" + mstAuthUserDto.getLogin(),
				new HashMap<>(), HttpMethod.POST);
		try {
			listUserGadgetDto = JsonUtil.mapJsonToListObject(
					rest.getContents(), AuthUserGadgetDto.class);
			listGadgetStatus();
			for (AuthUserGadgetDto a : listUserGadgetDto) {
				jenisGadget.setRvLowValue(a.getgSeries());
				mstAuthUserGadgetDto.setgNumber(a.getgNumber());
				if(a.getgStatus().equalsIgnoreCase("N")){
					setGadgetStatus("Tidak Aktif");
				}else if(a.getgStatus().equalsIgnoreCase("Y")){
					setGadgetStatus("Aktif");
				}
				mstAuthUserGadgetDto.setKeterangan(a.getKeterangan());
			}
			listGadget();
			for (DasiJrRefCodeDto dasi : listJenisGadget) {
				if (jenisGadget.getRvLowValue().equalsIgnoreCase(dasi.getRvLowValue())) {
					jenisGadget = new DasiJrRefCodeDto();
					jenisGadget = dasi;
				}
			}
			BindUtils.postNotifyChange(null, null, this, "mstAuthUserGadgetDto");
			BindUtils.postNotifyChange(null, null, this, "listUserGadgetDto");
			BindUtils.postNotifyChange(null, null, this, "jenisGadget");
			BindUtils.postNotifyChange(null, null, this, "gadgetStatus");
		} catch (Exception e) {
			e.printStackTrace();
		}

		mstAuthUserDto.setLogin(mstAuthUserDto.getLogin());
		mstAuthUserDto.setAttribute4(mstAuthUserDto.getAttribute4());
		mstAuthUserDto.setUserName(mstAuthUserDto.getUserName());
		BindUtils.postNotifyChange(null, null, this, "mstAuthUserDto");
	}

	@Command
	public void searchWilayahKantor() {
		Map<String, Object> map = new HashMap<>();
		map.put("search", searchIndex);
		map.put("userSession", getCurrentUserSessionJR());
		RestResponse rest = new RestResponse();
		if(isAdmin()){
			rest = callWs(WS_URI_LOV + "/findAllKantor", map, HttpMethod.POST);
		}else{
			rest = callWs(WS_URI_LOV + "/findAllKantorByLogin", map, HttpMethod.POST);
//			search = sess.getKantor().substring(0, 2)+"%";
		}
		try {
			listKantorWilayah = JsonUtil.mapJsonToListObject(
					rest.getContents(), FndKantorJasaraharjaDto.class);
			listKantorWilayahCopy = new ArrayList<>();
			listKantorWilayahCopy.addAll(listKantorWilayah);
			BindUtils.postNotifyChange(null, null, this,
					"listKantorWilayah");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange("listKantorWilayahCopy")
	@Command
	public void cariFilterAjaKantor(@BindingParam("item") String cari) {
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listKantorWilayahCopy));
		if (listKantorWilayahCopy != null || listKantorWilayahCopy.size() > 0) {
			listKantorWilayahCopy.clear();
		}
		if (listKantorWilayah != null && listKantorWilayah.size() > 0) {
			for (FndKantorJasaraharjaDto dto : listKantorWilayah) {
				System.out.println("+++");
				if (dto.getKodeNama().toUpperCase()
						.contains(cari.toUpperCase())) {
					listKantorWilayahCopy.add(dto);
				}
			}
		}
	}

	@Command
	public void search() {
		Map<String, Object> map = new HashMap<String, Object>();
		if (kantorWilayah ==null || kantorWilayah.getKodeKantorJr() == null) {
			map.put("search", "");
		}else{
			map.put("search", kantorWilayah.getKodeKantorJr());			
		}
		RestResponse rest = callWs(WS_URI + "/all", map, HttpMethod.POST);
		try {
			listAuthUserDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					AuthUserDto.class);
			listAuthUserDtoCopy = new ArrayList<>();
			listAuthUserDtoCopy.addAll(listAuthUserDto);
			BindUtils.postNotifyChange(null, null, this, "listAuthUserDtoCopy");
			System.out.println(JsonUtil.getJson(listAuthUserDtoCopy));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@NotifyChange("listAuthUserDtoCopy")
	@Command
	public void cariFilterAja(@BindingParam("item") String cari){
		System.out.println(cari);
		System.out.println(JsonUtil.getJson(listAuthUserDtoCopy));
		if(listAuthUserDtoCopy != null || listAuthUserDtoCopy.size() >0 ){
			listAuthUserDtoCopy.clear();
		}
		if(listAuthUserDto != null && listAuthUserDto.size() > 0){
			for(AuthUserDto dto : listAuthUserDto){
				if(dto.getRsDtos().size()!=0){
					for(PlRumahSakitDto dtoRs : dto.getRsDtos()){
						System.out.println("+++");
						if(dto.getAttribute4()==null){
							if(
									dto.getLogin().toUpperCase().contains(cari)||
									dto.getUserName().toUpperCase().contains(cari)||
									dto.getDescription().toUpperCase().contains(cari)||
									dtoRs.getKodeNamaRs().toUpperCase().contains(cari)
									)
							{
								listAuthUserDtoCopy.add(dto);
							}
						}else if(dto.getAttribute4()!=null){
							if(
									dto.getLogin().toUpperCase().contains(cari)||
									dto.getUserName().toUpperCase().contains(cari)||
									dto.getDescription().toUpperCase().contains(cari)||
									dto.getAttribute4().toUpperCase().contains(cari)||
									dtoRs.getKodeNamaRs().toUpperCase().contains(cari)
									)
							{
								listAuthUserDtoCopy.add(dto);
							}
						}
						break;
					}	
				}else if(dto.getRsDtos()==null||dto.getRsDtos().size()==0){
					if(dto.getAttribute4()==null){
						if(
								dto.getLogin().toUpperCase().contains(cari)||
								dto.getUserName().toUpperCase().contains(cari)||
								dto.getDescription().toUpperCase().contains(cari)
								)
						{
							listAuthUserDtoCopy.add(dto);
						}
					}else if(dto.getAttribute4()!=null){
						if(
								dto.getLogin().toUpperCase().contains(cari)||
								dto.getUserName().toUpperCase().contains(cari)||
								dto.getDescription().toUpperCase().contains(cari)||
								dto.getAttribute4().toUpperCase().contains(cari)
								)
						{
							listAuthUserDtoCopy.add(dto);
						}
					}
				}
			}
		}
	}

	@Command
	public void changePageSize() {
		setPageSize(getPageSize());
		loadList();
		BindUtils.postNotifyChange(null, null, this, "pageSize");
	}

	@Command
	public void save() {
		RestResponse restResponse = null;
		mstAuthUserDto.setLogin(mstAuthUserDto.getLogin());
		mstAuthUserDto.setAttribute4(mstAuthUserDto.getAttribute4());
		mstAuthUserDto.setAttribute2(mstAuthUserDto.getAttribute2());
		restResponse = callWs(WS_URI + "/updateAuthUser", mstAuthUserDto,
				HttpMethod.POST);

		mstAuthUserGadgetDto.setgSeries(jenisGadget.getRvLowValue());
		mstAuthUserGadgetDto.setgNumber(mstAuthUserGadgetDto.getgNumber());
		if(getGadgetStatus().equalsIgnoreCase("aktif")){
			mstAuthUserGadgetDto.setgStatus("Y");			
		}else if(getGadgetStatus().equalsIgnoreCase("tidak aktif")){
			mstAuthUserGadgetDto.setgStatus("N");						
		}
		mstAuthUserGadgetDto
				.setKeterangan(mstAuthUserGadgetDto.getKeterangan());
		mstAuthUserGadgetDto.setUpdatedBy("luthfi");
		mstAuthUserGadgetDto.setUpdatedDate(new Date());
		mstAuthUserGadgetDto.setUserLogin(mstAuthUserDto.getLogin());
		mstAuthUserGadgetDto.setUsrId(mstAuthUserDto.getUserId());
		restResponse = callWs(WS_URI + "/saveUserGadget", mstAuthUserGadgetDto,
				HttpMethod.POST);

		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
		} else if (restResponse.getStatus() == 2) {
			Messagebox.show("Area Code Already Exists");
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}

	@NotifyChange("listRsDto")
	@Command
	public void tabRs() {
		showDaftarRs();
		if (getSearchRs() == null || getSearchRs().equalsIgnoreCase("")) {
			setSearchRs("%%");
		}

		RestResponse rest = callWs(WS_URI + "/getListRsByUser/"
				+ mstAuthUserDto.getLogin() + "/" + getSearchRs(),
				new HashMap<String, Object>(), HttpMethod.POST);
 
		try {
			listRsDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlRumahSakitDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listRsDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Command
	public void tambahRs() {
		showTambahRs();
		listRsNotIn();
	}
	
	public void listRsNotIn(){
		RestResponse rest = new RestResponse();
		try {
			Map<String, Object> map = new HashMap<>();
			map.put("userSession", getCurrentUserSessionJR());
			rest = callWs(WS_URI + "/getRsNotIn",
					map, HttpMethod.POST);
			listRsDto = JsonUtil.mapJsonToListObject(rest.getContents(),PlRumahSakitDto.class);
			listRsDtoCopy=new ArrayList<>();
			listRsDtoCopy.addAll(listRsDto);
			BindUtils.postNotifyChange(null, null, this, "listRsDtoCopy");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listGadget(){
		RestResponse rest = callWs(WS_URI_LOV + "/getListGadget",
				new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listJenisGadget = JsonUtil.mapJsonToListObject(rest.getContents(),DasiJrRefCodeDto.class);
			BindUtils.postNotifyChange(null, null, this, "listJenisGadget");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listGadgetStatus(){
		setGadgetStatus("Aktif");
		listGadgetStatus.add("Aktif");
		listGadgetStatus.add("Tidak Aktif");
		BindUtils.postNotifyChange(null, null, this, "listGadgetStatus");
		BindUtils.postNotifyChange(null, null, this, "gadgetStatus");
	}
	
	@NotifyChange("listRsDtoCopy")
	@Command
	public void cariRs(@BindingParam("item") String cari) {
		if (listRsDtoCopy != null || listRsDtoCopy.size() > 0) {
			listRsDtoCopy.clear();
		}
		if (listRsDto != null && listRsDto.size() > 0) {
			for (PlRumahSakitDto dto : listRsDto) {
				if (dto.getKodeNamaRs().toUpperCase().contains(cari.toUpperCase())) {
					listRsDtoCopy.add(dto);
				}
			}
		}
	}


	@Command("saveListRs")
	public void saveListRs() {
		RestResponse restResponse = null;
		mstAuthRsPicDto.setUserLogin(mstAuthUserDto.getLogin());
		mstAuthRsPicDto.setKodeRumahsakit(mstPlRumahSakitDto
				.getKodeRumahsakit());
		restResponse = callWs(WS_URI + "/savePicRs", mstAuthRsPicDto,
				HttpMethod.POST);

		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
			tabRs();
		} else if (restResponse.getStatus() == 2) {
			Messagebox.show("Area Code Already Exists");
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}

	@Command
	public void deleteRs(
			@BindingParam("item") PlRumahSakitDto mstPlRumahSakitDto) {
		RestResponse restResponse = null;
		mstAuthRsPicDto.setUserLogin(mstAuthUserDto.getLogin());
		mstAuthRsPicDto.setKodeRumahsakit(mstPlRumahSakitDto
				.getKodeRumahsakit());
		restResponse = callWs(WS_URI + "/deletePicRs", mstAuthRsPicDto,
				HttpMethod.POST);

		if (restResponse.getStatus() == CommonConstants.OK_REST_STATUS) {
			showInfoMsgBox(restResponse.getMessage());
			tabRs();
		} else if (restResponse.getStatus() == 2) {
			Messagebox.show("Area Code Already Exists");
		} else {
			showErrorMsgBox(restResponse.getMessage());
		}
	}

	@Command("back")
	public void back() {
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}

	private void showTambahRs() {
		setTambahRsGb(true);
		setDaftarRsGb(false);
		BindUtils.postNotifyChange(null, null, this, "tambahRsGb");
		BindUtils.postNotifyChange(null, null, this, "daftarRsGb");
	}

	private void showDaftarRs() {
		setTambahRsGb(false);
		setDaftarRsGb(true);
		BindUtils.postNotifyChange(null, null, this, "tambahRsGb");
		BindUtils.postNotifyChange(null, null, this, "daftarRsGb");
	}

	public Form getFormMaster() {
		return formMaster;
	}

	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}

	public boolean isDaftarRsGb() {
		return daftarRsGb;
	}

	public void setDaftarRsGb(boolean daftarRsGb) {
		this.daftarRsGb = daftarRsGb;
	}

	public boolean isTambahRsGb() {
		return tambahRsGb;
	}

	public void setTambahRsGb(boolean tambahRsGb) {
		this.tambahRsGb = tambahRsGb;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getGadgetStatus() {
		return gadgetStatus;
	}

	public void setGadgetStatus(String gadgetStatus) {
		this.gadgetStatus = gadgetStatus;
	}

	public int getPageSizeRs() {
		return pageSizeRs;
	}

	public void setPageSizeRs(int pageSizeRs) {
		this.pageSizeRs = pageSizeRs;
	}

	public String getSearchRs() {
		return searchRs;
	}

	public void setSearchRs(String searchRs) {
		this.searchRs = searchRs;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public List<AuthUserDto> getListAuthUserDto() {
		return listAuthUserDto;
	}

	public void setListAuthUserDto(List<AuthUserDto> listAuthUserDto) {
		this.listAuthUserDto = listAuthUserDto;
	}

	public List<PlRumahSakitDto> getListRsDto() {
		return listRsDto;
	}

	public void setListRsDto(List<PlRumahSakitDto> listRsDto) {
		this.listRsDto = listRsDto;
	}

	public List<AuthUserGadgetDto> getListUserGadgetDto() {
		return listUserGadgetDto;
	}

	public void setListUserGadgetDto(List<AuthUserGadgetDto> listUserGadgetDto) {
		this.listUserGadgetDto = listUserGadgetDto;
	}

	public AuthUserDto getMstAuthUserDto() {
		return mstAuthUserDto;
	}

	public void setMstAuthUserDto(AuthUserDto mstAuthUserDto) {
		this.mstAuthUserDto = mstAuthUserDto;
	}

	public AuthUserGadgetDto getMstAuthUserGadgetDto() {
		return mstAuthUserGadgetDto;
	}

	public void setMstAuthUserGadgetDto(AuthUserGadgetDto mstAuthUserGadgetDto) {
		this.mstAuthUserGadgetDto = mstAuthUserGadgetDto;
	}

	public AuthRsPicDto getMstAuthRsPicDto() {
		return mstAuthRsPicDto;
	}

	public void setMstAuthRsPicDto(AuthRsPicDto mstAuthRsPicDto) {
		this.mstAuthRsPicDto = mstAuthRsPicDto;
	}

	public PlRumahSakitDto getMstPlRumahSakitDto() {
		return mstPlRumahSakitDto;
	}

	public void setMstPlRumahSakitDto(PlRumahSakitDto mstPlRumahSakitDto) {
		this.mstPlRumahSakitDto = mstPlRumahSakitDto;
	}

	public List<FndKantorJasaraharjaDto> getListKantorWilayah() {
		return listKantorWilayah;
	}

	public void setListKantorWilayah(
			List<FndKantorJasaraharjaDto> listKantorWilayah) {
		this.listKantorWilayah = listKantorWilayah;
	}

	public FndKantorJasaraharjaDto getKantorWilayah() {
		return kantorWilayah;
	}

	public void setKantorWilayah(FndKantorJasaraharjaDto kantorWilayah) {
		this.kantorWilayah = kantorWilayah;
	}

	public List<FndKantorJasaraharjaDto> getListKantorWilayahCopy() {
		return listKantorWilayahCopy;
	}

	public void setListKantorWilayahCopy(
			List<FndKantorJasaraharjaDto> listKantorWilayahCopy) {
		this.listKantorWilayahCopy = listKantorWilayahCopy;
	}

	public List<AuthUserDto> getListAuthUserDtoCopy() {
		return listAuthUserDtoCopy;
	}

	public void setListAuthUserDtoCopy(List<AuthUserDto> listAuthUserDtoCopy) {
		this.listAuthUserDtoCopy = listAuthUserDtoCopy;
	}

	public List<PlRumahSakitDto> getListRsDtoCopy() {
		return listRsDtoCopy;
	}

	public void setListRsDtoCopy(List<PlRumahSakitDto> listRsDtoCopy) {
		this.listRsDtoCopy = listRsDtoCopy;
	}

	public List<DasiJrRefCodeDto> getListJenisGadget() {
		return listJenisGadget;
	}

	public void setListJenisGadget(List<DasiJrRefCodeDto> listJenisGadget) {
		this.listJenisGadget = listJenisGadget;
	}

	public void setJenisGadget(DasiJrRefCodeDto jenisGadget) {
		this.jenisGadget = jenisGadget;
	}

	public DasiJrRefCodeDto getJenisGadget() {
		return jenisGadget;
	}

	public List<String> getListGadgetStatus() {
		return listGadgetStatus;
	}

	public void setListGadgetStatus(List<String> listGadgetStatus) {
		this.listGadgetStatus = listGadgetStatus;
	}

}
