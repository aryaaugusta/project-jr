package ui.master;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Form;
import org.zkoss.bind.SimpleForm;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

import share.FndKantorJasaraharjaDto;
import share.KorlantasDistrictDto;
import share.KorlantasProvinceDto;
import share.PlInstansiDto;
import share.PlMappingPoldaDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.CommonConstants;
import common.util.JsonUtil;

@Init(superclass = true)
public class MstPlInstansiVmd extends BaseVmd implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final String INDEX_PAGE_PATH = UIConstants.BASE_PAGE_PATH
			+ "/master/MasterInstansi/_index.zul";
	private final String DETAIL_PAGE_PATH = UIConstants.BASE_PAGE_PATH	
			+ "/master/MasterInstansi/_detail.zul";
	
	private final String WS_URI = "/MasterInstansi";
	private final String WS_URI_LOV = "/Lov";
	
	private Form formMaster = new SimpleForm();
	private Form formDetail = new SimpleForm();
	
	private List<PlInstansiDto> listDto = new ArrayList<>();
	private List<FndKantorJasaraharjaDto> listKanWil = new ArrayList<>();
	private PlInstansiDto mstInstansiDto = new PlInstansiDto();
	private KorlantasDistrictDto mstKorlantasDistrictDto = new KorlantasDistrictDto();
	private KorlantasProvinceDto mstKorlantasProvinceDto = new KorlantasProvinceDto();
	private List<KorlantasDistrictDto> listMstKorlantasDistrictDto = new ArrayList<>();
	private List<KorlantasProvinceDto> listMstKorlantasProvinceDto = new ArrayList<>();
	private FndKantorJasaraharjaDto kanwilDto = new FndKantorJasaraharjaDto();
	private PlMappingPoldaDto mappingPoldaDto = new PlMappingPoldaDto();
		
	private boolean listIndex = false;
	private String flagStatus;
	private String namaInstansi;
	private String kodeInstansi;
	
	private String searchIndex;
	private String searchKanwil;
	private String kode;
	
	private int pageSize = 5;
	
	private String groupCode = getCurrentUserSessionJR().getGroupCode();
	
	protected void loadList() {
		searchKanwil();
		searchByKanwil();
	}
	
	
	@Command
	public void close(@BindingParam("item") Window win){
		win.detach();	
	}
	
	@Command
	public void openDetail(@BindingParam("popup") String popup, @BindingParam("item") PlInstansiDto dto,
			@Default("popUpHandler") @BindingParam("mode") String mode){
		Map<String, Object> args = new HashMap<>();
		if(dto!=null){
			this.mstInstansiDto = dto;
		}
		args.put("dto", dto);
		if(mode!= null && mode.equalsIgnoreCase("add")){
			add();
		}else{
			edit();
		}
		if(dto!=null){
			Executions.getCurrent().setAttribute("obj", dto);
		}
		
		try {
			System.out.println(popup);
			((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	@Command
	public void searchKanwil(){
		if(getSearchKanwil()==null||getSearchKanwil().equalsIgnoreCase("")){
			setSearchKanwil("%%");
		}else if(getSearchKanwil().contains("-")){
			setSearchKanwil(getSearchKanwil().substring(0, 2));
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("kantor", getCurrentUserSessionJR().getKantor());
		map.put("loginId", getCurrentUserSessionJR().getLoginID());
		map.put("search", getSearchKanwil());
		
		RestResponse restKantor = callWs(WS_URI_LOV + "/wilayahKantor/",
				map, HttpMethod.POST);
		try {			
			listKanWil = JsonUtil.mapJsonToListObject(restKantor.getContents(),
					FndKantorJasaraharjaDto.class);
			setTotalSize(restKantor.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listKanWil");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	@Command("searchByKanwil")
	@NotifyChange({"searchKanwil","listIndex",})
	public void searchByKanwil(){
		setListIndex(true);
		if(getSearchKanwil()==null||getSearchKanwil().equalsIgnoreCase("")||getSearchKanwil().equalsIgnoreCase("%%")){
			setKode(getCurrentUserSessionJR().getKantor());
		}else if(getSearchKanwil().contains("-")){
			setKode(getSearchKanwil().substring(0,2));
		}else{
			
			Map<String, Object> map = new HashMap<>();
			map.put("kantor", getCurrentUserSessionJR().getKantor());
			map.put("loginId", getCurrentUserSessionJR().getLoginID());
			map.put("search", getSearchKanwil());
			
			RestResponse restKantor = new RestResponse();

			try {			
				restKantor = callWs(WS_URI_LOV + "/wilayahKantor/",
						map, HttpMethod.POST);
				List<FndKantorJasaraharjaDto> dto = JsonUtil.mapJsonToListObject(restKantor.getContents(),
						FndKantorJasaraharjaDto.class);		
				for(FndKantorJasaraharjaDto a : dto){
					setKode(a.getKodeKantorJr().substring(0,2));
					BindUtils.postNotifyChange(null, null, this, "kode");					
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if(getSearchIndex()==null||getSearchIndex().equalsIgnoreCase("")){
			setSearchIndex("%%");
		}
		if(getKode()==null){
			setKode("%%");
		}
		
		Map<String, Object> maps = new HashMap<>();
		maps.put("kode", getKode());
		maps.put("searchIndex", getSearchIndex());
		
		RestResponse rest = new RestResponse();

		try {
			rest = callWs(WS_URI + "/all/", maps, HttpMethod.POST);
			listDto = JsonUtil.mapJsonToListObject(rest.getContents(),
					PlInstansiDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listDto");
			BindUtils.postNotifyChange(null, null, this, "listIndex");
			
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
//	@Command("add")
	private void add() {
		getPageInfo().setAddMode(true);
//		navigate(DETAIL_PAGE_PATH);
	}
	
	private void edit(){
		getPageInfo().setEditMode(true);
		
	}
	
	public void onAdd(){
		refreshDetail();
		RestResponse rest = callWs(WS_URI + "/korlantasProvince",
			new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listMstKorlantasProvinceDto = JsonUtil.mapJsonToListObject(rest.getContents(), 
					KorlantasProvinceDto.class);
			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listMstKorlantasProvinceDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@Command("loadPolres")
	@NotifyChange({"mstKorlantasProvinceDto","mstKorlantasDistrictDto"})
	public void findPolres(){
		mstKorlantasDistrictDto.setName("");
		RestResponse rest = callWs(WS_URI + "/korlantasDistrict/" + mstKorlantasProvinceDto.getProvinceNumber(),
			new HashMap<String, Object>(), HttpMethod.POST);
		try {
			listMstKorlantasDistrictDto = JsonUtil.mapJsonToListObject(rest.getContents(), 
					KorlantasDistrictDto.class);
//			setTotalSize(rest.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "listMstKorlantasDistrictDto");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private List<KorlantasDistrictDto> getListKorlantasDistrict(String noProvinsi){
		RestResponse rest = new RestResponse();
		List<KorlantasDistrictDto> listDistrictDto = new ArrayList<>();
		try {
			rest = callWs(WS_URI + "/korlantasDistrict/" + mstKorlantasProvinceDto.getProvinceNumber(),
					new HashMap<String, Object>(), HttpMethod.POST);
			listDistrictDto = JsonUtil.mapJsonToListObject(rest.getContents(), 
					KorlantasDistrictDto.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listDistrictDto;
	}
	
	private void getListKorlantasDistrictVoid(String noProvinsi){
		RestResponse rest = new RestResponse();
		try {
			rest = callWs(WS_URI + "/korlantasDistrict/" + noProvinsi,
					new HashMap<String, Object>(), HttpMethod.POST);
			listMstKorlantasDistrictDto = JsonUtil.mapJsonToListObject(rest.getContents(), 
					KorlantasDistrictDto.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Command("save")
	public void save(){
//		showInfoMsgBox("sadasdas");
		RestResponse rest = new RestResponse();
		if(getPageInfo().isAddMode()){
			if (kodeInstansi == null) {
				Messagebox.show("Kode Instansi Tidak Boleh Kosong");
				return;
			}else if(getKodeInstansi().length()<7){
				Messagebox.show("Kode harus 7 digit");
				return;
//			}else if(!getCurrentUserSessionJR().getKantor().substring(0, 2).equalsIgnoreCase(kodeInstansi.substring(0, 2))){
//				if(!isAdmin()){
//					showWarningMsgBox("Kode instansi harus sesuai dengan lokasi kantor anda");
//					return;
//				}
			}
			
			else{
				mstInstansiDto.setKodeInstansi(kodeInstansi);
				mstInstansiDto.setDeskripsi(namaInstansi);
				mstInstansiDto.setCreatedBy(getCurrentUserSessionJR().getLoginID());
				mstInstansiDto.setCreationDate(new Date());
				mstInstansiDto.setLastUpdatedBy(null);
				mstInstansiDto.setLastUpdatedDate(null);
				if(getFlagStatus().equalsIgnoreCase("YES")){
					mstInstansiDto.setFlagEnable("Y");					
				}else if(getFlagStatus().equalsIgnoreCase("NO")){
					mstInstansiDto.setFlagEnable("N");
				}
				mstInstansiDto.setNamaPolda(mstKorlantasProvinceDto.getName());
				mstInstansiDto.setKodePolda(mstKorlantasProvinceDto.getProvinceNumber());
				mstInstansiDto.setIdDistrict(mstKorlantasDistrictDto.getDistrictNumber());
				rest = callWs(WS_URI, mstInstansiDto, HttpMethod.POST);				
			}			
		}else {
			if(getFlagStatus().equalsIgnoreCase("YES")){
				mstInstansiDto.setFlagEnable("Y");	
//				return;
			}else if(getFlagStatus().equalsIgnoreCase("NO")){
				mstInstansiDto.setFlagEnable("N");
			}
			mstInstansiDto.setDeskripsi(namaInstansi);
			mstInstansiDto.setLastUpdatedBy(getCurrentUserSessionJR().getLoginID());
			mstInstansiDto.setLastUpdatedDate(new Date());
			mstInstansiDto.setNamaPolda(mstKorlantasProvinceDto.getName());
			mstInstansiDto.setKodePolda(mstKorlantasProvinceDto.getProvinceNumber());
			mstInstansiDto.setIdDistrict(String.valueOf(mstKorlantasDistrictDto.getId()));
			rest = callWs(WS_URI+"/update", mstInstansiDto, HttpMethod.POST);			
		}
		if (rest.getStatus() == CommonConstants.OK_REST_STATUS && getPageInfo().isAddMode()) {
			refreshDetail();
			showInfoMsgBox(rest.getMessage());			
		} else if(rest.getStatus() == CommonConstants.OK_REST_STATUS && getPageInfo().isEditMode()){
			showInfoMsgBox(rest.getMessage());
		}else if(rest.getStatus()==2){
			Messagebox.show("Data sudah ada");
		}else{
			showErrorMsgBox(rest.getMessage());
		}
		
		refresh();
		
	}
	
	@NotifyChange({"kodeInstansi","namaInstansi","flagStatus","mstKorlantasProvinceDto","mstKorlantasDistrictDto"})
	public void refreshDetail(){
		setKodeInstansi("");
		setNamaInstansi("");
		setFlagStatus("");
		mstKorlantasProvinceDto.setName("");
		mstKorlantasDistrictDto.setName("");	
		
		BindUtils.postNotifyChange(null, null, this, "kodeInstansi");
		BindUtils.postNotifyChange(null, null, this, "namaInstansi");
		BindUtils.postNotifyChange(null, null, this, "flagStatus");
		BindUtils.postNotifyChange(null, null, this, "mstKorlantasProvinceDto");
		BindUtils.postNotifyChange(null, null, this, "mstKorlantasDistrictDto");
	}

	@Command("edit")
	public void edit(@BindingParam("item") PlInstansiDto selected) {
		if (selected == null || selected.getKodeInstansi() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Executions.getCurrent().setAttribute("obj", selected);
		getPageInfo().setEditMode(true);
		navigate(DETAIL_PAGE_PATH);
	}
	
	public void onEdit(){
		mstInstansiDto = (PlInstansiDto) Executions.getCurrent()
				.getAttribute("obj");
		System.out.println("MASUK ONEDIT");
		RestResponse rest2 = new RestResponse();
		RestResponse restDistrict = new RestResponse();
		KorlantasDistrictDto distrikDto = new KorlantasDistrictDto();
		try {
			System.err.println("MASUK TRY");
			rest2 = callWs(WS_URI + "/korlantasProvince/" + mstInstansiDto.getKodeInstansi(), new HashMap<String, Object>(), HttpMethod.POST);
			
			
			mappingPoldaDto = JsonUtil.mapJsonToSingleObject(rest2.getContents(), 
					PlMappingPoldaDto.class);
			restDistrict = callWs(WS_URI + "/korlantasDistrictId/" + 
					mappingPoldaDto.getIdDistricts(), new HashMap<String, Object>(), HttpMethod.POST);
			List<KorlantasDistrictDto> listKorl =JsonUtil.mapJsonToListObject(restDistrict.getContents(), KorlantasDistrictDto.class); 
			distrikDto = listKorl.get(0);
//			System.err.println("Mapping polda============");
//			System.out.println(JsonUtil.getJson(mappingPoldaDto));
//			setTotalSize(rest2.getTotalRecords());
			BindUtils.postNotifyChange(null, null, this, "mappingPoldaDto");
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		setKodeInstansi(mstInstansiDto.getKodeInstansi());
		setNamaInstansi(mstInstansiDto.getDeskripsi());
		if(mstInstansiDto.getFlagEnable().equalsIgnoreCase("Y")){
			setFlagStatus("YES");
		}else if(mstInstansiDto.getFlagEnable().equalsIgnoreCase("N")){
			setFlagStatus("NO");
		}
		mstInstansiDto.setFlagEnable(mstInstansiDto.getFlagEnable());
		mstKorlantasProvinceDto.setName(mstInstansiDto.getNamaPolda());
		mstKorlantasProvinceDto.setProvinceNumber(mappingPoldaDto.getKodePolda()!=null? 
				(mappingPoldaDto.getKodePolda().length() > 3 ? mappingPoldaDto.getKodePolda().substring(1,3) : mappingPoldaDto.getKodePolda())
				:mappingPoldaDto.getKodePolda());
		mstKorlantasDistrictDto.setDistrictNumber(mappingPoldaDto.getIdDistricts());
		mstKorlantasDistrictDto.setName(mstInstansiDto.getNamaPolres());
		BindUtils.postNotifyChange(null, null, this, "mstInstansiDto");
		BindUtils.postNotifyChange(null, null, this, "kodeInstansi");
		BindUtils.postNotifyChange(null, null, this, "namaInstansi");
		BindUtils.postNotifyChange(null, null, this, "mstKorlantasProvinceDto");
		BindUtils.postNotifyChange(null, null, this, "mstKorlantasDistrictDto");

		RestResponse rest = new RestResponse();
		try {
			
			rest = callWs(WS_URI + "/korlantasProvince", new HashMap<String, Object>(), HttpMethod.POST);
			if(isAdmin()){
			}else{
				
			}
//			System.out.println("============================\nmasuk province");
			
			listMstKorlantasProvinceDto = JsonUtil.mapJsonToListObject(rest.getContents(), 
					KorlantasProvinceDto.class);
//			System.out.println("data province \n"+JsonUtil.getJson(listMstKorlantasProvinceDto));
//			System.out.println("polda\tprov");
			for(KorlantasProvinceDto a : listMstKorlantasProvinceDto){
				if(a.getProvinceNumber()!=null){
//					System.out.println(mappingPoldaDto.getKodePolda()+"\t"+a.getProvinceNumber());
//					System.out.println(mappingPoldaDto.getKodePolda()+"\t"+a.getProvinceNumber()+" : "+
//							(distrikDto.getProvinceId().toString().equalsIgnoreCase(a.getProvinceNumber()) || 
//							distrikDto.getProvinceId().toString() == a.getProvinceNumber() ||
//							a.getProvinceNumber().equalsIgnoreCase(distrikDto.getProvinceId().toString()) ||
//							Integer.parseInt(distrikDto.getProvinceId().toString()) == Integer.parseInt(a.getProvinceNumber())  ));
//					System.out.println(distrikDto.getProvinceId().intValue() +"\t"+ Integer.parseInt(a.getProvinceNumber()) +
//							" : "+(distrikDto.getProvinceId().intValue() == Integer.parseInt(a.getProvinceNumber())) );
					if(distrikDto.getProvinceId().toString().equalsIgnoreCase(a.getProvinceNumber()) || 
							(distrikDto.getProvinceId().intValue() == Integer.parseInt(a.getProvinceNumber())) ||
							a.getProvinceNumber().equalsIgnoreCase(distrikDto.getProvinceId().toString())){
						System.out.println("dapat kode polda yang sama dgn proc : "+mappingPoldaDto.getKodePolda());
						setMstKorlantasProvinceDto(a);
						try {
//							System.out.println("============================\nmasuk district");
							listMstKorlantasDistrictDto = getListKorlantasDistrict(distrikDto.getProvinceId().toString());
							System.out.println("data districts \n"+JsonUtil.getJson(listMstKorlantasDistrictDto));
							for(KorlantasDistrictDto  b : listMstKorlantasDistrictDto){
								if( (b.getId().toString().equalsIgnoreCase(mappingPoldaDto.getIdDistricts())) ||
									(b.getId().intValue() == Integer.parseInt(mappingPoldaDto.getIdDistricts()))
									){
//									System.out.println("dapat distrik denga ID : "+b.getId()+"\nNama Distrik : "+b.getName());
									mstKorlantasDistrictDto = b;
								}
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
//			setTotalSize(rest.getTotalRecords());

		} catch (Exception e) {
			e.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "listMstKorlantasProvinceDto");
		BindUtils.postNotifyChange(null, null, this, "mstKorlantasProvinceDto");
		BindUtils.postNotifyChange(null, null, this, "listMstKorlantasDistrictDto");
		BindUtils.postNotifyChange(null, null, this, "mstKorlantasDistrictDto");
		System.out.println("EDIT DONE");
		
//		refresh();
	}
	
	@Command("back")
	public void back() {
		getPageInfo().setListMode(true);
		navigate(INDEX_PAGE_PATH);
	}
	
	@Command
	public void delete(@BindingParam("item") final PlInstansiDto selected) {
		if (selected == null || selected.getKodeInstansi() == null) {
			showSmartMsgBox("W001");
			return;
		}

		Messagebox.show(Labels.getLabel("C001"),
				Labels.getLabel("confirmation"), new Button[] { Button.YES,
						Button.NO }, Messagebox.QUESTION, Button.NO,
				new EventListener<ClickEvent>() {
					@Override
					public void onEvent(ClickEvent evt) throws Exception {
						if (Messagebox.ON_YES.equals(evt.getName())) {
							RestResponse restRespone;
							selected.setFlagEnable("N");
							restRespone = callWs(WS_URI + "/delete", selected,
									HttpMethod.POST);
							if (restRespone.getStatus() == CommonConstants.OK_REST_STATUS) {
								showInfoMsgBox("Data successfully deleted");
								refresh();
							} else if (restRespone.getStatus() == 3) {
								showInfoMsgBox("Instansi yang dipilih sudah dipakai di Pengajuan, silahkan pilih Instansi lain");
//								refresh();
							}else {
								showErrorMsgBox("Instansi gagal dihapus");
							}
						}
					}
				});
	}

	@Command
	public void refresh() {
		getPageInfo().setListMode(true);
//		setTotalSize(totalSize);
//		setPageSize(pageSize);
		navigate("");
		navigate(INDEX_PAGE_PATH);
	}
	
	public List<PlInstansiDto> getListDto() {
		return listDto;
	}

	public void setListDto(List<PlInstansiDto> listDto) {
		this.listDto = listDto;
	}

	public PlInstansiDto getMstInstansiDto() {
		return mstInstansiDto;
	}

	public void setMstInstansiDto(PlInstansiDto mstInstansiDto) {
		this.mstInstansiDto = mstInstansiDto;
	}

	public List<FndKantorJasaraharjaDto> getListKanWil() {
		return listKanWil;
	}

	public void setListKanWil(List<FndKantorJasaraharjaDto> listKanWil) {
		this.listKanWil = listKanWil;
	}

	public KorlantasDistrictDto getMstKorlantasDistrictDto() {
		return mstKorlantasDistrictDto;
	}

	public void setMstKorlantasDistrictDto(
			KorlantasDistrictDto mstKorlantasDistrictDto) {
		this.mstKorlantasDistrictDto = mstKorlantasDistrictDto;
	}

	public KorlantasProvinceDto getMstKorlantasProvinceDto() {
		return mstKorlantasProvinceDto;
	}

	public void setMstKorlantasProvinceDto(
			KorlantasProvinceDto mstKorlantasProvinceDto) {
		this.mstKorlantasProvinceDto = mstKorlantasProvinceDto;
	}

	public List<KorlantasDistrictDto> getListMstKorlantasDistrictDto() {
		return listMstKorlantasDistrictDto;
	}

	public void setListMstKorlantasDistrictDto(
			List<KorlantasDistrictDto> listMstKorlantasDistrictDto) {
		this.listMstKorlantasDistrictDto = listMstKorlantasDistrictDto;
	}

	public List<KorlantasProvinceDto> getListMstKorlantasProvinceDto() {
		return listMstKorlantasProvinceDto;
	}

	public void setListMstKorlantasProvinceDto(
			List<KorlantasProvinceDto> listMstKorlantasProvinceDto) {
		this.listMstKorlantasProvinceDto = listMstKorlantasProvinceDto;
	}

	public Form getFormMaster() {
		return formMaster;
	}

	public void setFormMaster(Form formMaster) {
		this.formMaster = formMaster;
	}

	public Form getFormDetail() {
		return formDetail;
	}

	public void setFormDetail(Form formDetail) {
		this.formDetail = formDetail;
	}

	public String getFlagStatus() {
		return flagStatus;
	}

	public void setFlagStatus(String flagStatus) {
		this.flagStatus = flagStatus;
	}

	public String getNamaInstansi() {
		return namaInstansi;
	}

	public void setNamaInstansi(String namaInstansi) {
		this.namaInstansi = namaInstansi;
	}

	public String getKodeInstansi() {
		return kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public FndKantorJasaraharjaDto getKanwilDto() {
		return kanwilDto;
	}

	public void setKanwilDto(FndKantorJasaraharjaDto kanwilDto) {
		this.kanwilDto = kanwilDto;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	

	public boolean isListIndex() {
		return listIndex;
	}

	public void setListIndex(boolean listIndex) {
		this.listIndex = listIndex;
	}

	public String getSearchIndex() {
		return searchIndex;
	}

	public void setSearchIndex(String searchIndex) {
		this.searchIndex = searchIndex;
	}

	public PlMappingPoldaDto getMappingPoldaDto() {
		return mappingPoldaDto;
	}

	public void setMappingPoldaDto(PlMappingPoldaDto mappingPoldaDto) {
		this.mappingPoldaDto = mappingPoldaDto;
	}

	public String getSearchKanwil() {
		return searchKanwil;
	}

	public void setSearchKanwil(String searchKanwil) {
		this.searchKanwil = searchKanwil;
	}

	public String getKode() {
		return kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

}
