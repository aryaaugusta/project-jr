package ui;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBody;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTDocument1;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageSz;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblBorders;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblPr;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTblWidth;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STBorder;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STJc;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STPageOrientation;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import share.PlPeriodeLockDto;
import common.model.UserSessionJR;
import common.ui.BaseVmd;
import common.ui.UIConstants;


public class TestVmd{
	private String search;
	private List<String> listData = new ArrayList<>();
	private List<String> listSelected = new ArrayList<>();
	private String selected = "";
	private UserSessionJR ujr;
	private Boolean ol = false;
	private Boolean nw = true;
	private Boolean tambahOK = true;
	private Date time = new Date();
	private PlPeriodeLockDto ss;
	private List<PlPeriodeLockDto> listLock = new ArrayList<>();	
	private List<PlPeriodeLockDto> listLockCopy = new ArrayList<>();	
	
	private void cekSama(String cari){
		if(listLockCopy != null || listLockCopy.size() >0 ){
			listLockCopy.clear();
		}
		if(listLock != null && listLock.size() > 0){
			for(PlPeriodeLockDto dto : listLock){
				if(dto.getKodeKantorJr().contains(cari)||dto.getPeriode().contains(cari)||dto.getStatus().contains(cari)||dto.getCreatedBy().equals(cari)){
					listLockCopy.add(dto);
				}
			}
		}
		
	}
	
	@Init
	public void init(){
		
		Clients.showNotification("Test Notification ");
		for (int i = 1; i < 10 ; i++){
			listData.add(i+"0000 "+"data ke "+i);
			System.out.println("masuk ke : "+i);
		}
		listSelected = listData;
		BindUtils.postNotifyChange(null, null, this, "listSelected");
	}
	
	@Command
	public void checkTime(){
		System.out.println(time);
	}
	
	@Command
	@NotifyChange({"listSelected","search"})
	public void search(@BindingParam("val") String val){
		System.out.println("masuk search");
		System.out.println("isi seach "+val);
		System.out.println("variable search : "+search);
		listSelected = new ArrayList<>();
		for (String x : listData){
			if(x.matches("(.*)"+val+"(.*)")){
				System.out.println("dapet data : "+x);
				listSelected.add(x);
			}
		}
	}
	
	@Command
	@NotifyChange({"listSelected","search"})
	public void select(@BindingParam("val")String val){
		System.out.println("Masuk onselect");
		System.out.println("isi val :"+val);
		listSelected = new ArrayList<>();
		for (String x : listData){
			if(x.matches("(.*)"+val+"(.*)")){
				System.out.println("dapet data : "+x);
				listSelected.add(x);
			}
		}
	}
	
	@Command
	@NotifyChange({"listSelected","search"})
	public void search2(@BindingParam("val") String val){
		System.out.println("masuk search 2");
		System.out.println("isi val "+val);
		System.out.println("variable search : "+search);
		listSelected = new ArrayList<>();
		for (String x : listData){
			if(x.matches("(.*)"+val+"(.*)")){
				System.out.println("dapet data : "+x);
				listSelected.add(x);
			}
		}
	}

	@NotifyChange({"ol","nw"})
	@Command("switchs")
	public void switchs(){
		ol = !ol;
		nw = !nw;
	}
	
	
	@Command
	@NotifyChange("selected")
	public void pilih(@BindingParam("data") String data){
		if(data!= null){
			selected = data;
		}
	}
	
	@Command("showPopup")
	public void showPopup(
			@BindingParam("popup") String popup,
			@Default("popUpHandler") @BindingParam("popUpHandler") String globalHandleMethodName) {
		Map<String, Object> args = new HashMap<>();
		
		args.put("popUpHandler", globalHandleMethodName);
		

		if (!beforePopup(args, popup))
			return;
		try {
			((Window) Executions.createComponents(popup, null, args)).doModal();
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	protected boolean beforePopup(Map<String, Object> args, String popup) {
//		args.put("id", popup);
		args.put("ujr", ujr);
		return true;
	}
	
	//TODO:s
	
	@NotifyChange({"ujr","tambahOK"})
	@Command
	public void tambah(){
		ujr = new UserSessionJR();
		tambahOK = false;
	}
	
	@GlobalCommand("iniHandler")
	public void titleHandler(@BindingParam("ujr") UserSessionJR ujrs) {
		if(ujr != null){
			this.ujr = ujrs;
		}
	}

	//TODO: cetak word dokument
	/*
	 *Untuk buat file docx  
	 */
	private String desktop = "C:\\Users\\Superman\\Desktop\\";
	private String companyName = "PT. JASA RAHARJA (PERSERO)";
	private String noBerkas = "2-001-00-02-00-11-2018";
	private String namaCabang = "CABANG DKI JAKARTA";
	private String namaPengaju = "Jajat Sudrajat";
	private String kecPengaju = "Kec. Cikarang Utara";
	private String kotaPengaju = "Bekasi";
	private String telpPengaju = "0853516689577";
	private String alamatPengaju = 	"Kp. Cabang Lio, RT.003/004, Kel. Karang Asih"+
									", "+kecPengaju+", "+kotaPengaju+" / "+telpPengaju;
	private String hubunganPengajuDgKorban = "Korban Sendiri";
	private String identitasKorban = "3216091601790011";
	private String namaKorban = "Jajat Sudrajat";
	private String umurKorban = "37";
	private String kecKorban = "Kec. Cikarang Utara";
	private String kotaKorban = "Bekasi";
	private String telpKorban = "0853516689577";
	private String alamatKorban = 	"Kp. Cabang Lio, RT.003/004, Kel. Karang Asih"+
									", "+kecKorban+", "+kotaKorban+" / "+telpKorban;
	private String kecKecelakaan = "Kec. Pulogadung";
	private String kotaKecelakaan = "Jakarta Timur";
	private String tempatKecelakaan = kecKecelakaan+", "+kotaKecelakaan;
	private Date tglKecelakaan = new Date();
	private String sifatCidera = "Luka - Luka";
	private List<String> listBerkasDiterima = new ArrayList<>();
	private List<String> listBerkasKurang = new ArrayList<>();
	private String namaLogin = "Petugas Testing 03";
	private String lokasi = "Jakarta";
	private Date date = new Date();
	private SimpleDateFormat sdf = new SimpleDateFormat("E yyyy/MM/dd HH:mm:ss");
	public Date getDate() {
//     	Clients.alert(sdf.format(date));
		return date;
	}
	
	public void setDate(Date time) {
//		Clients.alert(sdf.format(time));
		this.date = time;
	}
	
	public static void main(String[] args) throws Exception {
	
		TestVmd a = new TestVmd();
//		System.out.println("");
//		a.createDoc();
//		a.loaddong();
		a.cekHari();
	}
	
	private void cekHari(){
		SimpleDateFormat sdf = new SimpleDateFormat("E", Locale.ENGLISH);
		SimpleDateFormat sdf2 = new SimpleDateFormat("DD/MM/YYYY", Locale.ENGLISH);
		Date date = new Date();
		Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		for (int i = 0 ; i<7; i++){
			cal.add(Calendar.DATE, 1);
			System.out.println(i + " : "+sdf.format(cal.getTime()));
			System.out.println(i + " : "+sdf2.format(cal.getTime()));
			/**
			0 : Fri
			1 : Sat
			2 : Sun
			3 : Mon
			4 : Tue
			5 : Wed
			6 : Thu
			 */
		}
	}
	
	private String getHari(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("E", Locale.ENGLISH);
		String ha = sdf.format(tgl);
		return 	ha.equalsIgnoreCase("Mon")?"Senin":
				ha.equalsIgnoreCase("Tue")?"Selasa":
				ha.equalsIgnoreCase("Wed")?"Rabu":
				ha.equalsIgnoreCase("Thu")?"Kamis":
				ha.equalsIgnoreCase("Fri")?"Jum'at":
				ha.equalsIgnoreCase("Sat")?"Sabtu":
				ha.equalsIgnoreCase("Sun")?"Minggu":"";
	}
	
	
	private void createBlank() throws Exception{
		XWPFDocument doc = new XWPFDocument();
		
		FileOutputStream out = new FileOutputStream( new File(desktop+"blank.docx"));
		doc.write(out);
		out.close();
		System.out.println("blank.docx written successully");
	}
	
	private void createParagraph() throws Exception{
		 //Blank Document
	      XWPFDocument document = new XWPFDocument(); 
	      
	      //Write the Document in file system
	      FileOutputStream out = new FileOutputStream(new File(desktop+"createparagraph.docx"));
	        
	      //create Paragraph
	      XWPFParagraph paragraph = document.createParagraph();
	      XWPFRun run = paragraph.createRun();
	      run.setText("At tutorialspoint.com, we strive hard to " +
	         "provide quality tutorials for self-learning " +
	         "purpose in the domains of Academics, Information " +
	         "Technology, Management and Computer Programming Languages.");
				
	      document.write(out);
	      out.close();
	      System.out.println("createparagraph.docx written successfully");
	}
	
	/* nambah berkas yg diterima
	 */
	private void addListBerkas(){
		listBerkasDiterima.add("01-Formulir K");
		listBerkasDiterima.add("10-Akte Lahir");
		listBerkasDiterima.add("21-Formulir Pengajuan Santunan");
		listBerkasDiterima.add("30-Berita Acara Restitusi Klaim");
	}
	
	private void addDokumenKurang(){
		/*
		 * Semua dokumen yang belum masuk list diterima, dimasukkan disini
		 */
	}
	
	private String getBulan(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("MM");
		String month = sdf.format(tgl);
		return month.equalsIgnoreCase("01")?"Januari": 
			   month.equalsIgnoreCase("02")?"Februari":
			   month.equalsIgnoreCase("03")?"Maret":
			   month.equalsIgnoreCase("04")?"April":
			   month.equalsIgnoreCase("05")?"Mei":
			   month.equalsIgnoreCase("06")?"Juni":
			   month.equalsIgnoreCase("07")?"Juli":
			   month.equalsIgnoreCase("08")?"Agustus":
			   month.equalsIgnoreCase("09")?"September":
			   month.equalsIgnoreCase("10")?"Oktober":
			   month.equalsIgnoreCase("11")?"Nopember":
			   month.equalsIgnoreCase("12")?"Desember":"";
	}
	
	private String checkJam(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(tgl);
	}
	
	private String getTanggal(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("DD", Locale.ENGLISH);
		return sdf.format(tgl);
	}
	
	private String getTahun(Date tgl){
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
		return sdf.format(tgl);
	}
	
	private String formatTanggal(Date tgl){
		return getHari(tgl)+" "+getBulan(tgl)+" "+getTahun(tgl);
	}
	
	private void setTableBorderNone(XWPFTable table){
		CTTblPr tblpro = table.getCTTbl().getTblPr();
		
		CTTblWidth tblW = tblpro.addNewTblW();
//		tblW.setW(arg0);
		
		CTTblBorders border = tblpro.addNewTblBorders();

		border.addNewBottom().setVal(STBorder.NONE);
		border.addNewLeft().setVal(STBorder.NONE);
		border.addNewRight().setVal(STBorder.NONE);
		border.addNewTop().setVal(STBorder.NONE);
		border.addNewInsideH().setVal(STBorder.NONE);
		border.addNewInsideV().setVal(STBorder.NONE);
	}
	
	private void addRow(XWPFTable table, String...a){
		if (a.length>0){
			XWPFTableRow row = table.createRow();
			for(int x = 0; x<a.length; x++){
				if(x>0 && (row.getCell(x)==null)){
					row.addNewTableCell().setText(a[x]);
				}else{
					row.getCell(x).setText(a[x]);
				}
			}
		}
	}
	private void addFirstRow(XWPFTable table, String...a){
		if (a.length>0){
			XWPFTableRow row = table.createRow();
			row.getCell(0).setText(a[0]);
			for(int x = 1; x<a.length; x++){
				row.addNewTableCell().setText(a[x]);
			}
		}
	}
	
	private void setCellWidth(XWPFTable tabel, int column, long value){
		tabel.getRow(0).getCell(column).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(value));
	}
	
	private void setCellWidth(XWPFTableRow baris, int column, long value){
		baris.getCell(column).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(value));
	}
	
	public void setTableAlign(XWPFTable table,ParagraphAlignment align) {
	    CTTblPr tblPr = table.getCTTbl().getTblPr();
	    CTJc jc = (tblPr.isSetJc() ? tblPr.getJc() : tblPr.addNewJc());
	    STJc.Enum en = STJc.Enum.forInt(align.getValue());
	    jc.setVal(en);
	}
	
	/**
	 *  set whether alignment for each paragraph using {@code Apache Poi}.
	 * <pre>
	 *    setAlignment(paragraph, 1);
	 * </pre>
	 * will make align Center
	 * 
	 * @param paragraph
	 * @param align 
	 *         a value less than {@code 0} if {@code x < y}; and
	 *         a value greater than {@code 0} if {@code x > y}
	 * 
	 * 1 for align Center, 
	 * 2 for align Left, 
	 * 3 for align Right,
	 * 4 for justify
	 */
	public void setAlignment(XWPFParagraph paragraph, int align){
		if(align == 1)
			paragraph.setAlignment(ParagraphAlignment.CENTER);
		else if(align == 2)
			paragraph.setAlignment(ParagraphAlignment.LEFT);
		else if(align == 3)
			paragraph.setAlignment(ParagraphAlignment.RIGHT);
		else if(align == 4)
			paragraph.setAlignment(ParagraphAlignment.BOTH);
	}
	
	@Command
	public void loaddong() throws Exception{
		SimpleDateFormat tglKec = new SimpleDateFormat();
		String tagglKec = tglKec.format(tglKecelakaan);
		//Blank Document
		XWPFDocument document = new XWPFDocument();
		CTDocument1 doc = document.getDocument();
		CTBody body = doc.getBody();

		if (!body.isSetSectPr()) {
		     body.addNewSectPr();
		}
		
		CTPageSz pageSize;
		CTSectPr section = body.getSectPr();

		if(section.isSetPgSz()) {
		    pageSize = section.getPgSz();
		}else{
			pageSize = section.addNewPgSz();
		}

		pageSize.setOrient(STPageOrientation.LANDSCAPE);
		pageSize.setW(BigInteger.valueOf(842 * 20));
	    pageSize.setH(BigInteger.valueOf(595 * 20));
//		document.getDocument().getBody().getSectPr().addNewPgSz().setOrient(STPageOrientation.LANDSCAPE);
		
		//nama file
//		String name = "document.docx";
		
		//tempat dokument di simpan
//		FileOutputStream out = new FileOutputStream(new File(desktop+name));
		
		//bagian 1
		XWPFParagraph par = document.createParagraph();
//		par.getCTP().
		
		//run pertama
		XWPFRun run1 = par.createRun();
		run1.setFontFamily("calibri");
		run1.setText(companyName);
		int x = 0;
		while( x < 5){
			run1.addTab();
			x++;
		}
		run1.setText("No Berkas: "+noBerkas);
		run1.addCarriageReturn();
		run1.setText(namaCabang);
		run1.addCarriageReturn();
		
		//bagian 2
		XWPFParagraph par2 = document.createParagraph();
		setAlignment(par2, 1);;
//		par2.setAlignment(ParagraphAlignment.CENTER);
		//set align CENTER untuk tulisan
		
		//run kedua
		XWPFRun run2 = par2.createRun();
		run2.setText("TANDA TERIMA");
		
		//bagian 3
		XWPFParagraph par3 = document.createParagraph();
		XWPFRun run3 = par3.createRun();
		run3.setText("Telah terima berkas pengajuan klaim dari : ");
		
		//bagian 4
		XWPFTable tabelDataDiri = document.createTable();
		
		//set tabel border = NONE
		setTableBorderNone(tabelDataDiri);
		
		XWPFTableRow row1Data = tabelDataDiri.getRow(0);
		row1Data.getCell(0).setText("Nama");
		row1Data.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(2000));
		row1Data.addNewTableCell().setText(":");
		row1Data.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(100));
		row1Data.addNewTableCell().setText(namaPengaju);
		XWPFTableRow row2Data = tabelDataDiri.createRow();
		row2Data.getCell(0).setText("Alamat / Telp");
		row2Data.getCell(1).setText(":");
		row2Data.getCell(2).setText(alamatPengaju);
		XWPFTableRow row3Data = tabelDataDiri.createRow();
		row3Data.getCell(0).setText("Hub dgn korban");
		row3Data.getCell(1).setText(":");
		row3Data.getCell(2).setText(hubunganPengajuDgKorban);
		XWPFTableRow row4Data = tabelDataDiri.createRow();
		row4Data.getCell(0).setText("Identitas Korban");
		row4Data.getCell(1).setText(":");
		row4Data.getCell(2).setText(identitasKorban);
		XWPFTableRow row5Data = tabelDataDiri.createRow();
		row5Data.getCell(0).setText("Nama / Umur");
		row5Data.getCell(1).setText(":");
		row5Data.getCell(2).setText(namaKorban+" / "+telpKorban);
		XWPFTableRow row6Data = tabelDataDiri.createRow();
		row6Data.getCell(0).setText("Alamat / Telp");
		row6Data.getCell(1).setText(":");
		row6Data.getCell(2).setText(alamatKorban);
		XWPFTableRow row7Data = tabelDataDiri.createRow();
		row7Data.getCell(0).setText("Tempat Kecelakaan");
		row7Data.getCell(1).setText(":");
		row7Data.getCell(2).setText(tempatKecelakaan);
		XWPFTableRow row8Data = tabelDataDiri.createRow();
		row8Data.getCell(0).setText("Tanggal Kecelakaan");
		row8Data.getCell(1).setText(":");
		row8Data.getCell(2).setText(formatTanggal(tglKecelakaan));
		XWPFTableRow row9Data = tabelDataDiri.createRow();
		row9Data.getCell(0).setText("Sifat Cidera");
		row9Data.getCell(1).setText(":");
		row9Data.getCell(2).setText(sifatCidera);
		
		//Bagian 5
		XWPFParagraph par5 = document.createParagraph();
		//isi list dokumen
		addListBerkas();
		
		XWPFTable tabelBerkas = document.createTable();
		setTableBorderNone(tabelBerkas);
		
		
		XWPFTableRow rowData = tabelBerkas.getRow(0);
		rowData.getCell(0).setText("Berkas terdiri dari : ");
		rowData.getCell(0).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(10000));
		rowData.addNewTableCell().setText("Dokumen yang harus dilengkapi");
		rowData.getCell(1).getCTTc().addNewTcPr().addNewTcW().setW(BigInteger.valueOf(10000));
		
		if(listBerkasDiterima.size()<listBerkasKurang.size()){
			for(int i=0; i<listBerkasKurang.size();i++){
				String kurang = listBerkasKurang.get(i);
				String diterima = "";
				try{
					diterima = listBerkasDiterima.get(i);
				}catch(Exception p){
					
				}
				addRow(tabelBerkas, diterima, kurang);
			}
		}else if(listBerkasDiterima.size()>listBerkasKurang.size()){
			for(int i=0; i<listBerkasDiterima.size();i++){
				String diterima = listBerkasDiterima.get(i);
				String kurang = "";
				try{
					kurang = listBerkasKurang.get(i);
				}catch(Exception p){
				}
				addRow(tabelBerkas, diterima, kurang);
			}
		}
		
		//Bagian 6
		XWPFParagraph par6 = document.createParagraph();
		par6.setAlignment(ParagraphAlignment.RIGHT);
		XWPFRun run6 = par6.createRun();
		run6.setText(lokasi+", "+formatTanggal(new Date()));
		run6.addCarriageReturn();
		run6.setText("Jam Proses : "+checkJam(new Date()));
		
		//bagian 7
		XWPFTable tabelTTD = document.createTable();
		setTableBorderNone(tabelTTD);
		XWPFTableRow row = tabelTTD.getRow(0);
		row.getCell(0).setText("Yang Menyerahkan");
		setCellWidth(row, 0, 10000);
		row.addNewTableCell().setText("Yang Menerima");
		row.getCell(1).getParagraphs().get(0).setAlignment(ParagraphAlignment.RIGHT);
		setCellWidth(row, 1, 10000);
		row.setHeight(100);
		
		XWPFTableRow rowx=tabelTTD.createRow();
		addRow(tabelTTD, namaPengaju,namaLogin);
		rowx.getCell(1).getParagraphs().get(0).setAlignment(ParagraphAlignment.RIGHT);
		
		par = document.createParagraph();
		par.setBorderBottom(Borders.SINGLE);
		
		//download dokumen
		File temp = File.createTempFile("Document JR", ".docx");
		FileOutputStream outs = new FileOutputStream(temp);
		document.write(outs);
		document.close();
//		out.close();
		InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Document JR", "docx", "application/file", fis));
		temp.delete();
		
		
		//save document
//		document.write(out);
//		out.close();
		System.out.println("document created successfuly");
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * getter setter
	 * @return
	 */
	
	
	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	public List<String> getListData() {
		return listData;
	}

	public void setListData(List<String> listData) {
		this.listData = listData;
	}

	public List<String> getListSelected() {
		return listSelected;
	}

	public void setListSelected(List<String> listSelected) {
		this.listSelected = listSelected;
	}

	public String getSelected() {
		return selected;
	}

	public void setSelected(String selected) {
		this.selected = selected;
	}

	public UserSessionJR getUjr() {
		return ujr;
	}

	public void setUjr(UserSessionJR ujr) {
		this.ujr = ujr;
	}

	public Boolean getOl() {
		return ol;
	}

	public void setOl(Boolean ol) {
		this.ol = ol;
	}

	public Boolean getNw() {
		return nw;
	}

	public void setNw(Boolean nw) {
		this.nw = nw;
	}

	public Boolean getTambahOK() {
		return tambahOK;
	}

	public void setTambahOK(Boolean tambahOK) {
		this.tambahOK = tambahOK;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
}
