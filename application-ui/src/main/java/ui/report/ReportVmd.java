package ui.report;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.sql.Connection;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.ExporterOutput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

import org.apache.commons.io.IOUtils;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.io.Files;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import common.ui.BaseVmd;
import common.util.JasperConnectionUtil;

public class ReportVmd extends BaseVmd implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String WS_REPORT = "/report";
	private String filePath;
    private boolean fileuploaded = false;
   AMedia fileContent;
 
 @Wire("#test")
  private Window win;
 
 public AMedia getFileContent() {
        return fileContent;
 }
 
   public void setFileContent(AMedia fileContent) {
        this.fileContent = fileContent;
 }
 
   public boolean isFileuploaded() {
       return fileuploaded;
    }
 
   public void setFileuploaded(boolean fileuploaded) {
     this.fileuploaded = fileuploaded;
   }
 
   @AfterCompose
   public void initSetup(@ContextParam(ContextType.VIEW) Component view)
             {
     Selectors.wireComponents(view, this, false);
      
   }
 
   @Command
    @NotifyChange("fileuploaded")
   public void onUploadPDF(
            @ContextParam(ContextType.BIND_CONTEXT) BindContext ctx)
            throws IOException {
 
        UploadEvent upEvent = null;
     Object objUploadEvent = ctx.getTriggerEvent();
      if (objUploadEvent != null && (objUploadEvent instanceof UploadEvent)) {
            upEvent = (UploadEvent) objUploadEvent;
     }
       if (upEvent != null) {
          Media media = upEvent.getMedia();
           Calendar now = Calendar.getInstance();
          int year = now.get(Calendar.YEAR);
          int month = now.get(Calendar.MONTH); // Note: zero based!
           int day = now.get(Calendar.DAY_OF_MONTH);
           filePath = Executions.getCurrent().getDesktop().getWebApp()
                 .getRealPath("/");
          String yearPath = "\\" + "PDFs" + "\\" + year + "\\" + month + "\\"
                 + day + "\\";
           filePath = filePath + yearPath;
         File baseDir = new File(filePath);
          if (!baseDir.exists()) {
                baseDir.mkdirs();
           }
 
           Files.copy(new File(filePath + media.getName()),
                    media.getStreamData());
         Messagebox.show("File Sucessfully uploaded in the path [ ."
                 + filePath + " ]");
         fileuploaded = true;
            filePath = filePath + media.getName();
      }
   }
   
   private String getLoc(){
	   String port = ( Executions.getCurrent().getServerPort() == 80 ) ? "" : (":" + Executions.getCurrent().getServerPort());
	  String url = Executions.getCurrent().getScheme() + "://" + Executions.getCurrent().getServerName() + port + Executions.getCurrent().getContextPath() +  Executions.getCurrent().getDesktop().getRequestPath();
	  String x = Executions.getCurrent().getScheme() + "://" + Executions.getCurrent().getServerName() + port + Executions.getCurrent().getContextPath() ;
	  
//	  System.out.println(url);
//	  System.out.println(x);
	  return x;
   }
   
   @Command
   public void showMe(){
	   getLoc();
   }
   
   private ByteArrayOutputStream getByteArrayOutputStream(String...param) throws IOException, FileNotFoundException{
//	   String reportSrc = "/WEB-INF/report/report1.jasper";
	   URL url = new URL(getLoc()+"/report/report1.jrxml");
	   ByteArrayOutputStream baos = new ByteArrayOutputStream();
	   InputStream is = null;
	   try {
	     is = url.openStream ();
	     byte[] byteChunk = new byte[4096]; // Or whatever size you want to read in at a time.
	     int n;

	     while ( (n = is.read(byteChunk)) > 0 ) {
	       baos.write(byteChunk, 0, n);
	     }
	   }
	   catch (IOException e) {
	     System.err.printf ("Failed while reading bytes from %s: %s", url.toExternalForm(), e.getMessage());
	     e.printStackTrace ();
	     // Perform any other exception handling that's appropriate.
	   }
	   finally {
	     if (is != null) { is.close(); }
	   }
	   
	   return baos;
   }
   
   @Command
    @NotifyChange("fileContent")
    public void showPDF() throws IOException, Exception {
	 Connection conn = JasperConnectionUtil.getOracleConnection();
	   
//	 String reportSrc = "/report/report1.jasper";
	 
	 
	 
	 File temp = File.createTempFile("ReportJR", ".pdf");
	 System.out.println("===================================");
	 System.out.println(getLoc());
	 FileOutputStream outs = new FileOutputStream(temp);
	 File tempJsp = File.createTempFile("Jasper", ".jrxml");
	 ByteArrayOutputStream baos = getByteArrayOutputStream();
	 FileOutputStream fos = new FileOutputStream(tempJsp);
	 ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
	 try(OutputStream outputStream = new FileOutputStream(tempJsp)) {
		    baos.writeTo(outputStream);
		    outputStream.close();
		}catch(Exception x){
			x.printStackTrace();
		}
	 System.out.println("++++++++++++++++++++++++");
	 System.out.println(tempJsp.getAbsolutePath());
	 JasperReport jasper = JasperCompileManager.compileReport(tempJsp.getAbsolutePath());
	 Map<String, Object> param = new HashMap<>();
	 param.put("RV_DOMAIN", "KODE STATUS PROSES");
	 param.put("RV_HIGH_VALUE", "");
	 JasperPrint jasperReport = JasperFillManager.fillReport(jasper, param, conn);
	 
	 //PDF-Export
	 JRPdfExporter exportPdf = new JRPdfExporter();
	 System.out.println(temp.getAbsolutePath());
//	 ExporterInput exporterInput = new SimpleExporterInput(temp);
//	 ExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(temp);
//	 exportPdf.setExporterInput(exporterInput);
//	 OutputStreamExporterOutput expOut = new SimpleOutputStreamExporterOutput(temp);
	 exportPdf.setParameter(JRExporterParameter.JASPER_PRINT, jasperReport);
	 exportPdf.setParameter(JRExporterParameter.OUTPUT_STREAM , outs);
	 
	 exportPdf.exportReport();
//	 outs.close();
//	 IOutils.copy
	 
	 
	 //Before edited
      File f = new File(temp.getPath());
//      File f = new File(filePath);
//        Messagebox.show(" dfdfdfdsfdsf" + filePath);
        Messagebox.show(" Path to temp file" + temp.getPath());
        byte[] buffer = new byte[(int) f.length()];
     FileInputStream fs = new FileInputStream(f);
        fs.read(buffer);
        fs.close();
     ByteArrayInputStream is = new ByteArrayInputStream(buffer);
     fileContent = new AMedia("report", "pdf", "application/pdf", is);
         
         
 
   }
}
