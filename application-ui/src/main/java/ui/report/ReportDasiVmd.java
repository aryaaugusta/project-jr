package ui.report;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Default;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zul.Window;

import share.DasiJrRefCodeDto;
import share.GeneralDto;
import share.PlInstansiDto;
import share.PlJaminanDto;
import share.PlPengajuanSantunanDto;
import common.model.RestResponse;
import common.ui.BaseVmd;
import common.ui.UIConstants;
import common.util.JasperConnectionUtil;
import common.util.JsonUtil;
import core.model.DasiJrRefCode;

@Init(superclass=true)
public class ReportDasiVmd extends BaseVmd implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String WS_REPORT = "/report";
	private String filePath;
	private boolean fileuploaded = false;
	private String reportName = "";
	private Integer reportMode;
	private String kodeKantorJr;
	
	private GeneralDto selected;
	private List<GeneralDto> listDto; //listUtama
	private List<String> listThnPengajuan; //dari plPengajuanSantunan diambil dari tahun pengajuan
	private String selectedThnPengajuan;
	private List<String> listThnPenerimaan; //dari plPengajuanSantunan diambil dari tahun Penerimaan
	private String selectedThnPenerimaan;
	private List<String> listThnPenyelesaian; //dari plPengajuanSantunan diambil dari tahun penyelesaian
	private String selectedThnPenyelesaian;
	private String noBerkas;
	private String diajukanDi; 
	private String idKecelakaan; 
	private String noPengajuan;
	private String noLaporanPolisi;
	private Date tglLaporanPolisi;
	private List<DasiJrRefCodeDto> listSifatKecelakaan;  
	private DasiJrRefCodeDto selectedSifatLaka;
	private List<PlInstansiDto> listAsalBerkas;
	private PlInstansiDto selectedAsalBerkas;
	private List<DasiJrRefCodeDto> listKasusKecelakaan;
	private DasiJrRefCodeDto selectedKasusKecelakaan;
	private List<PlJaminanDto> listJaminan;
	private PlJaminanDto selectedJaminan;
	private List<DasiJrRefCodeDto> listStatusProses;
	private DasiJrRefCodeDto selectedStatusProses;
	private String noBpk;
	private Map<String, Object> parameter = new HashMap<>();
	private List<Integer> listSize = new ArrayList<>();
	
	public void loadList(){
		getFirstLoad();
		
	}
	
	@Command
	public void reset(){
		loadList();
		getDataReport();
		
	}
	
	
	
	private void getDataReport(){
		listDto = new ArrayList<>();
		for(int i = 1; i <= 40; i++){
			GeneralDto gen = new GeneralDto();
			gen.setKode("01");
			gen.setName("Laporan "+(i<10?"0"+i:i));
			gen.setCodeName("report"+(i<10?"0"+i:i));
//			gen.setCodeName("report"+i);
			listDto.add(gen);
		}
		
		listSize.add(5);
		listSize.add(10);
		listSize.add(15);
		listSize.add(20);
		listSize.add(listDto.size());
		
		
		BindUtils.postNotifyChange(null, null, this, "listSize");
		BindUtils.postNotifyChange(null, null, this, "listDto");
	}
	
	private void getFirstLoad(){
		System.out.println("=======================================================");
		System.out.println("Starting report view model");
		getDataReport();
		Map<String, Object> input = new HashMap<>();
		RestResponse rest = new RestResponse();
		input.put("option","firstLoad");
		input.put("userSession", getCurrentUserSessionJR());
		try{
			rest = callWs(WS_REPORT+"/general", input, HttpMethod.POST);
			Map<String, Object> restOut = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			listSifatKecelakaan = JsonUtil.mapJsonToListObject(restOut.get("sifatLaka"), DasiJrRefCodeDto.class);
			listKasusKecelakaan = JsonUtil.mapJsonToListObject(restOut.get("kasusLaka"), DasiJrRefCodeDto.class);
			listStatusProses = JsonUtil.mapJsonToListObject(restOut.get("statusProses"), DasiJrRefCodeDto.class);
			listJaminan = JsonUtil.mapJsonToListObject(restOut.get("jaminan"), PlJaminanDto.class);
			listAsalBerkas = JsonUtil.mapJsonToListObject(restOut.get("instansi"), PlInstansiDto.class);
			listThnPenerimaan = JsonUtil.mapJsonToListObject(restOut.get("thnPenerimaan"), String.class);
			listThnPengajuan = JsonUtil.mapJsonToListObject(restOut.get("thnPengajuan"), String.class);
			listThnPenyelesaian = JsonUtil.mapJsonToListObject(restOut.get("thnPenyelesaian"), String.class);
			
			BindUtils.postNotifyChange(null, null, this, "listSifatKecelakaan");
			BindUtils.postNotifyChange(null, null, this, "listKasusKecelakaan");
			BindUtils.postNotifyChange(null, null, this, "listStatusProses");
			BindUtils.postNotifyChange(null, null, this, "listJaminan");
			BindUtils.postNotifyChange(null, null, this, "listAsalBerkas");
			BindUtils.postNotifyChange(null, null, this, "listThnPenerimaan");
			BindUtils.postNotifyChange(null, null, this, "listThnPengajuan");
			BindUtils.postNotifyChange(null, null, this, "listThnPenyelesaian");
			
		}catch(Exception s){
			s.printStackTrace();
		}
	}

	@Command
	public void cetakLaporan(@BindingParam("popup") String popup, @BindingParam("item") GeneralDto dto,
			@Default("popUpHandler") @BindingParam("detailHandler") String globalHandleMethodName){
		Map<String, Object> args = new HashMap<>();
		if(dto!=null){
			this.selected = dto;
		}
		args.put("dto", dto);
		args.put("detailHandler", globalHandleMethodName);
		args.put("RV_DOMAIN", "RV_DOMAIN");
		if (!beforeDetail(args, popup))
			return;
		try {
			//navigate(UIConstants.BASE_PAGE_PATH + "/operasionalDetail/DataPengajuanSantunan.zul");navigate(UIConstants.BASE_PAGE_PATH + "/operasionalDetail/DataPengajuanSantunan.zul");
			///operasional/OpRegisterSementara/_index.zul
			System.out.println(popup);
			((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
			BindUtils.postGlobalCommand(null, null, "print", args);
		} catch (UiException u) {
			u.printStackTrace();
		}
	}
	
	protected boolean beforeDetail(Map<String, Object> args, String popup){
		args.put("param",parameter );
		return true;
	}
	
	@GlobalCommand
	public void reportHandler(@BindingParam("dto")GeneralDto dto){
		//TODO: execute when page report close
	}
	
	@Command
	public void cetakLaporans(@BindingParam("popup") String popup, @BindingParam("item") GeneralDto dto,
			@Default("popUpHandler") @BindingParam("detailHandler") String globalHandleMethodName){
		Map<String, Object> param = new HashMap<>();
		param.put("RV_DOMAIN", "KODE STATUS PROSES");
		param.put("RV_HIGH_VALUE", "");
		this.parameter = new HashMap<>();
		parameter.putAll(param);
	}
	
	@NotifyChange("fileContent")
	@Command
	private void print(){
		
	}
	
	@Command
	public void printDoc() throws IOException, Exception {
//		ss

	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public boolean isFileuploaded() {
		return fileuploaded;
	}

	public void setFileuploaded(boolean fileuploaded) {
		this.fileuploaded = fileuploaded;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public Integer getReportMode() {
		return reportMode;
	}

	public void setReportMode(Integer reportMode) {
		this.reportMode = reportMode;
	}

	public String getKodeKantorJr() {
		return kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}


	public GeneralDto getSelected() {
		return selected;
	}

	public void setSelected(GeneralDto selected) {
		this.selected = selected;
	}

	public List<GeneralDto> getListDto() {
		return listDto;
	}

	public void setListDto(List<GeneralDto> listDto) {
		this.listDto = listDto;
	}

	public List<String> getListThnPengajuan() {
		return listThnPengajuan;
	}

	public void setListThnPengajuan(List<String> listThnPengajuan) {
		this.listThnPengajuan = listThnPengajuan;
	}

	public String getSelectedThnPengajuan() {
		return selectedThnPengajuan;
	}

	public void setSelectedThnPengajuan(String selectedThnPengajuan) {
		this.selectedThnPengajuan = selectedThnPengajuan;
	}

	public List<String> getListThnPenerimaan() {
		return listThnPenerimaan;
	}

	public void setListThnPenerimaan(List<String> listThnPenerimaan) {
		this.listThnPenerimaan = listThnPenerimaan;
	}

	public String getSelectedThnPenerimaan() {
		return selectedThnPenerimaan;
	}

	public void setSelectedThnPenerimaan(String selectedThnPenerimaan) {
		this.selectedThnPenerimaan = selectedThnPenerimaan;
	}

	public List<String> getListThnPenyelesaian() {
		return listThnPenyelesaian;
	}

	public void setListThnPenyelesaian(List<String> listThnPenyelesaian) {
		this.listThnPenyelesaian = listThnPenyelesaian;
	}

	public String getSelectedThnPenyelesaian() {
		return selectedThnPenyelesaian;
	}

	public void setSelectedThnPenyelesaian(String selectedThnPenyelesaian) {
		this.selectedThnPenyelesaian = selectedThnPenyelesaian;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getDiajukanDi() {
		return diajukanDi;
	}

	public void setDiajukanDi(String diajukanDi) {
		this.diajukanDi = diajukanDi;
	}

	public String getIdKecelakaan() {
		return idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getNoPengajuan() {
		return noPengajuan;
	}

	public void setNoPengajuan(String noPengajuan) {
		this.noPengajuan = noPengajuan;
	}

	public String getNoLaporanPolisi() {
		return noLaporanPolisi;
	}

	public void setNoLaporanPolisi(String noLaporanPolisi) {
		this.noLaporanPolisi = noLaporanPolisi;
	}

	public Date getTglLaporanPolisi() {
		return tglLaporanPolisi;
	}

	public void setTglLaporanPolisi(Date tglLaporanPolisi) {
		this.tglLaporanPolisi = tglLaporanPolisi;
	}

	public List<DasiJrRefCodeDto> getListSifatKecelakaan() {
		return listSifatKecelakaan;
	}

	public void setListSifatKecelakaan(List<DasiJrRefCodeDto> listSifatKecelakaan) {
		this.listSifatKecelakaan = listSifatKecelakaan;
	}

	public DasiJrRefCodeDto getSelectedSifatLaka() {
		return selectedSifatLaka;
	}

	public void setSelectedSifatLaka(DasiJrRefCodeDto selectedSifatLaka) {
		this.selectedSifatLaka = selectedSifatLaka;
	}

	public List<PlInstansiDto> getListAsalBerkas() {
		return listAsalBerkas;
	}

	public void setListAsalBerkas(List<PlInstansiDto> listAsalBerkas) {
		this.listAsalBerkas = listAsalBerkas;
	}

	public PlInstansiDto getSelectedAsalBerkas() {
		return selectedAsalBerkas;
	}

	public void setSelectedAsalBerkas(PlInstansiDto selectedAsalBerkas) {
		this.selectedAsalBerkas = selectedAsalBerkas;
	}

	public List<DasiJrRefCodeDto> getListKasusKecelakaan() {
		return listKasusKecelakaan;
	}

	public void setListKasusKecelakaan(List<DasiJrRefCodeDto> listKasusKecelakaan) {
		this.listKasusKecelakaan = listKasusKecelakaan;
	}

	public DasiJrRefCodeDto getSelectedKasusKecelakaan() {
		return selectedKasusKecelakaan;
	}

	public void setSelectedKasusKecelakaan(DasiJrRefCodeDto selectedKasusKecelakaan) {
		this.selectedKasusKecelakaan = selectedKasusKecelakaan;
	}

	public List<PlJaminanDto> getListJaminan() {
		return listJaminan;
	}

	public void setListJaminan(List<PlJaminanDto> listJaminan) {
		this.listJaminan = listJaminan;
	}

	public PlJaminanDto getSelectedJaminan() {
		return selectedJaminan;
	}

	public void setSelectedJaminan(PlJaminanDto selectedJaminan) {
		this.selectedJaminan = selectedJaminan;
	}

	public List<DasiJrRefCodeDto> getListStatusProses() {
		return listStatusProses;
	}

	public void setListStatusProses(List<DasiJrRefCodeDto> listStatusProses) {
		this.listStatusProses = listStatusProses;
	}

	public DasiJrRefCodeDto getSelectedStatusProses() {
		return selectedStatusProses;
	}

	public void setSelectedStatusProses(DasiJrRefCodeDto selectedStatusProses) {
		this.selectedStatusProses = selectedStatusProses;
	}

	public String getNoBpk() {
		return noBpk;
	}

	public void setNoBpk(String noBpk) {
		this.noBpk = noBpk;
	}

	public Map<String, Object> getParameter() {
		return parameter;
	}

	public void setParameter(Map<String, Object> parameter) {
		this.parameter = parameter;
	}

	public List<Integer> getListSize() {
		return listSize;
	}

	public void setListSize(List<Integer> listSize) {
		this.listSize = listSize;
	}


}
