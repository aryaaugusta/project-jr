package ui.report.bulanan;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.query.JRQueryExecuterFactory;
//import net.sf.jasperreports.engine.util.JRProperties;








import org.springframework.http.HttpMethod;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.io.Files;
import org.zkoss.util.media.AMedia;
import org.zkoss.util.resource.Labels;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import share.FndKantorJasaraharjaDto;
import share.GeneralDto;
import share.LaporanDto;
import share.PlInstansiDto;
import common.model.RestResponse;
import common.ui.BaseReportVmd;
import common.ui.UIConstants;
import common.util.JasperConnectionUtil;
import common.util.JsonUtil;

@Init(superclass=true)
public class ReportBulananVmd extends BaseReportVmd implements Serializable{

	/**
	 * 
	 */
	
	private static final long serialVersionUID = 1L;
	private final String WS_REPORT = "/report";
	private final String INDEX_PATH=UIConstants.BASE_PAGE_PATH+"/report/laporanBulanan/_index.zul";
	
	private AMedia fileContent;
	private List<LaporanDto> listLaporan = new ArrayList<>();
	private LaporanDto laporan ;
	private String userLogin;
	private String namaLaporan;
	private Date tglAwal;
	private Date tglAkhir;
	private List<PlInstansiDto> listKantor;
	private PlInstansiDto kantor;
	private String namaPejabat1;
	private String namaPejabat2;
	private String posisiJabatan1;
	private String posisiJabatan2;
	private String laporanStr;
	private boolean viewTipe = false;
	private boolean viewKantor = false;
	private boolean viewPilihan = false;
	private boolean viewCidera = false;
	private boolean viewEG = false;
	private boolean viewTipeTgl = false;
	private boolean viewSimpul = false;
	private boolean viewKodeTgl = false;
	private boolean viewPejabat = false;
	private boolean viewPeriode = false;
	private boolean viewBulan = false;
	private boolean viewStatusLP = false;
	private String tahun;
	private GeneralDto bulan;
	private List<GeneralDto> bulans;
	private List<GeneralDto> listSifatCidera;
	private GeneralDto sifatCiedera;
	private GeneralDto jenisLaporan;
	private List<GeneralDto> listJenisLaporan;
	private String laporanCode;
	private GeneralDto statusLp;
	private List<GeneralDto> listStatusLp;
	private List<GeneralDto> listPilihan;
	private GeneralDto pilihan;
	private GeneralDto tipeTanggal;
	private List<GeneralDto> listTipeTanggal;
	private List<GeneralDto> listEG;
	private GeneralDto eg;
	private GeneralDto useTgl;
	private List<GeneralDto> listUseTgl;
	private String searchKantor;
	private String ibukota;
	private String provinsi;
	
	@Override
	protected void loadList() {
		loadAwal();
		loadKantor("%%");
		listLaporan = new ArrayList<>();
		loadListLaporan();
		if(laporan!=null && laporan.getKodeLaporan()!=null){
			setMenu();
		}
		setBulanx();
		loadJenisLaporan();
		loatTanngal();
		loadStatusLP();
		loadPilihan();
		loadKantorAwal();
	}
	
	
	
//	@Override
//	private String dateToString(Date input){
//		SimpleDateFormat sdf = new SimpleDateFormat(Labels.getLabel("dateFormat"));
//		return sdf.format(input);
//	}
	
	private void loadKantorAwal(){
		kantor = new PlInstansiDto();
		Map<String, Object> input = new HashMap<>();
			RestResponse rest = new RestResponse();
			input.put("option","instansi");
		input.put("search", getCurrentUserSessionJR().getKantor());
		try{
			rest = callWs(WS_REPORT+"/general", input, HttpMethod.POST);
		Map<String, Object> restOut = JsonUtil.mapJsonToHashMapObject(rest.getContents());
		List<PlInstansiDto> listDto = JsonUtil.mapJsonToListObject(restOut.get("instansi"), PlInstansiDto.class);
				kantor = listDto.get(0);
			}catch(Exception s){
				s.printStackTrace();
			}
		 BindUtils.postNotifyChange(null, null, this, "kantor");
	}
	
	@Command
	public void reload(){
		getPageInfo().setListMode(true);
		
		navigate(INDEX_PATH);
	}
	
	@Command
	public void searchKantor(@BindingParam("val")String search){
		loadKantor(search);
	}
	
	private void loadKantor(String search){
		System.out.println("============");
		System.out.println("LoadKantor");
		Map<String, Object> input = new HashMap<>();
		RestResponse rest = new RestResponse();
		input.put("option","instansi");
		input.put("search",search);
		input.put("kodeKantor", getCurrentUserSessionJR().getKantor());
		try{
			rest = callWs(WS_REPORT+"/general", input, HttpMethod.POST);
			Map<String, Object> restOut = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			listKantor = JsonUtil.mapJsonToListObject(restOut.get("instansi"), PlInstansiDto.class);
		}catch(Exception s){
			s.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "listKantor");
	}
	
	@Command
	public void pilihKantor(@BindingParam("item")PlInstansiDto kantor){
		this.kantor = kantor;
		BindUtils.postNotifyChange(null, null, this, "kantor");
		
	}
	
	private void loadAwal(){
		Map<String, Object> input = new HashMap<>();
		RestResponse rest = new RestResponse();
		input.put("option","loadBulanan");
		input.put("kodeKantor", getCurrentUserSessionJR().getKantor());
		try{
			rest = callWs(WS_REPORT+"/general", input, HttpMethod.POST);
			Map<String, Object> restOut = JsonUtil.mapJsonToHashMapObject(rest.getContents());
			Map<String,Object> genPar = JsonUtil.mapJsonToHashMapObject(restOut.get("pejabat"));
			namaPejabat1 = (String) genPar.get("PL.RPT.PEJABAT1");
			namaPejabat2 = (String) genPar.get("PL.RPT.PEJABAT2");
			posisiJabatan1 = (String) genPar.get("PL.RPT.NAMAJABATAN1");
			posisiJabatan2 = (String) genPar.get("PL.RPT.NAMAJABATAN2");
			provinsi = (String) genPar.get("provinsi");
			ibukota = (String) genPar.get("ibukota");
			System.out.println("============================");
			System.out.println(JsonUtil.getJson(genPar));
		}catch(Exception s){
			System.out.println("LOGGER "+new Date()+" could not connect to service host: ");
		}
		BindUtils.postNotifyChange(null, null, this, "namaPejabat1");
		BindUtils.postNotifyChange(null, null, this, "namaPejabat2");
		BindUtils.postNotifyChange(null, null, this, "posisiJabatan1");
		BindUtils.postNotifyChange(null, null, this, "posisiJabatan2");
		BindUtils.postNotifyChange(null, null, this, "provinsi");
		BindUtils.postNotifyChange(null, null, this, "ibukota");
	}
	
	private void loadUseTgl(){
		listUseTgl = new ArrayList<>();
		listUseTgl.add(new GeneralDto("ENTRY", "Gunakan Periode Tanggal Entry"));
		listUseTgl.add(new GeneralDto("KEJADIAN", "Gunakan Periode Tanggal Kecelakaan"));
		useTgl = listUseTgl.get(0);
		BindUtils.postNotifyChange(null, null, this, "listUseTgl");
		BindUtils.postNotifyChange(null, null, this, "useTgl");
	}
	
	private void loadEG(){
		listEG = new ArrayList<>();
		listEG.add(new GeneralDto("ALL","SEMUA ExGratia"));
		listEG.add(new GeneralDto("3","ExGratia 2 Kendaraan / Lebih"));
		listEG.add(new GeneralDto("4","ExGratia Murni Laka Tunggal"));
		listEG.add(new GeneralDto("5","ExGratia Awak Angkutan Umum"));
		listEG.add(new GeneralDto("6","ExGratia Kedaluarsa"));
		listEG.add(new GeneralDto("7","ExGratia Murni Lainnya"));
		eg = listEG.get(0);
		BindUtils.postNotifyChange(null, null, this, "listEG");
		BindUtils.postNotifyChange(null, null, this, "eg");
		
	}
	
	private void loadTipeTanggal(Object no){
		listTipeTanggal = new ArrayList<>();
		if(no.equals("1")){
			listTipeTanggal.add(new GeneralDto("LAKA","Berdasarkan Tanggal Kecelakaan"));
			listTipeTanggal.add(new GeneralDto("AJU","Berdasarkan Tanggal Pengajuan"));
		}else{
			listTipeTanggal.add(new GeneralDto("tgl_kejadian","Berdasarkan Tanggal Kejadian"));
			listTipeTanggal.add(new GeneralDto("tgl_pengajuan","Berdasarkan Tanggal Pengajuan"));
			listTipeTanggal.add(new GeneralDto("tgl_penyelesaian","Berdasarkan Tanggal Penyelesaian"));
		}
		tipeTanggal = listTipeTanggal.get(0);
		BindUtils.postNotifyChange(null, null, this, "listTipeTanggal");
		BindUtils.postNotifyChange(null, null, this, "tipeTanggal");
	}
	
	private void loadPilihan(){
		listPilihan = new ArrayList<>();
		listPilihan.add(new GeneralDto("1","Terjamin"));
		listPilihan.add(new GeneralDto("2","Penelitian LL"));
		listPilihan.add(new GeneralDto("3","Terjamin & Penelitian LL"));
		pilihan = listPilihan.get(0);
		BindUtils.postNotifyChange(null, null, this, "listPilihan");
		BindUtils.postNotifyChange(null, null, this, "pilihan");
		
	}
	
	private void loadStatusLP(){
		listStatusLp = new ArrayList<>();
		listStatusLp.add(new GeneralDto("%", "-"));
		listStatusLp.add(new GeneralDto("Y", "Laporan Resmi"));
		listStatusLp.add(new GeneralDto("N", "Mutasi Laka"));
		statusLp = listStatusLp.get(0);
		BindUtils.postNotifyChange(null, null, this, "statusLp");
		BindUtils.postNotifyChange(null, null, this, "listStatusLp");
	}
	
	private void loadJenisLaporan(){
		listJenisLaporan = new ArrayList<>();
		listJenisLaporan.add(new GeneralDto("N", "Nasional"));
		listJenisLaporan.add(new GeneralDto("C", "Cabang"));
		listJenisLaporan.add(new GeneralDto("P", "Perwakilan"));
		listJenisLaporan.add(new GeneralDto("L", "Loket"));
		jenisLaporan=listJenisLaporan.get(1);
		BindUtils.postNotifyChange(null, null, this, "listJenisLaporan");
		BindUtils.postNotifyChange(null, null, this, "jenisLaporan");
	}
	
	private void loatTanngal(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdm = new SimpleDateFormat("MM/yyyy");
		String blnThn = "01/"+sdm.format(new Date());
		System.out.println(blnThn);
		try {
			tglAwal = sdf.parse(blnThn);
			System.out.println(blnThn+"/t"+tglAwal);
			tglAkhir = new Date();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		BindUtils.postNotifyChange(null, null, this, "tglAwal");
		BindUtils.postNotifyChange(null, null, this, "tglAkhir");
	}
	
	@Command
	public void cekTahun(@BindingParam("item") Textbox text){
		try{
			Long.parseLong(tahun);
		}catch(Exception s){
			tahun = "";
			BindUtils.postNotifyChange(null, null, this, "tahun");
//			Clients.showNotification("Mohon masukkan tahun dengan benar");
			Clients.alert("Mohon masukkan tahun dengan benar");
			text.setFocus(true);
		}
	}
	
	@Command
	public void setMenu(){
		showParam(laporan.getNo());
		setSifatCiderax(Integer.parseInt(laporan.getNo()));
		setLaporanCode(laporan.getReportCode());
		loadTipeTanggal(laporan.getNo());
		if(laporan.getNo().equals("7")){
			loadEG();
		}
		if(laporan.getNo().equals("28")){
			loadUseTgl();
		}
		BindUtils.postNotifyChange(null, null, this, "laporanCode");
	}
	
	private void setSifatCiderax(int option){
		listSifatCidera = new ArrayList<>();
		if(option == 28){
			listSifatCidera = new ArrayList<>();
			listSifatCidera.add(new GeneralDto("%", "SEMUA" ));
			listSifatCidera.add(new GeneralDto("MD", "01. MENINGGAL DUNIA" ));
			listSifatCidera.add(new GeneralDto("LL", "02. LUKA-LUKA" ));
			listSifatCidera.add(new GeneralDto("CT", "03. CACAT TETAP" ));
			listSifatCidera.add(new GeneralDto("MD-LL", "04. MENINGGAL & LUKA-LUKA" ));
			listSifatCidera.add(new GeneralDto("LL-CT", "05. LUKA-LUKA & CACAT TETAP" ));
			listSifatCidera.add(new GeneralDto("PG", "06. PENGUBURAN" ));
			listSifatCidera.add(new GeneralDto("LL-PG", "07. LUKA-LUKA & PENGUBURAN" ));
			sifatCiedera = listSifatCidera.get(0);
		}
		if(option != 28){
			listSifatCidera = new ArrayList<>();
			listSifatCidera.add(new GeneralDto("1", "MD (Meninggal Dunia)" ));
			listSifatCidera.add(new GeneralDto("2", "MDLL (Meninggal dan Luka-luka)" ));
			listSifatCidera.add(new GeneralDto("3", "MD+LL (Meninggal Dunia plus Luka-luka" ));
			sifatCiedera = listSifatCidera.get(0);
		}
		
		BindUtils.postNotifyChange(null, null, this, "listSifatCidera");
		BindUtils.postNotifyChange(null, null, this, "sifatCiedera");
	}
	
	private String getThn(){
		SimpleDateFormat sdf = new SimpleDateFormat("YYYY");
		return sdf.format(new Date());
		
	}
	
	private void setBulanx(){
		bulans = new ArrayList<>();
		bulans.add(new GeneralDto("1","Januari"));
		bulans.add(new GeneralDto("2","Februari"));
		bulans.add(new GeneralDto("3","Maret"));
		bulans.add(new GeneralDto("4","April"));
		bulans.add(new GeneralDto("5","Mei"));
		bulans.add(new GeneralDto("6","Juni"));
		bulans.add(new GeneralDto("7","Juli"));
		bulans.add(new GeneralDto("8","Agustus"));
		bulans.add(new GeneralDto("9","September"));
		bulans.add(new GeneralDto("10","Oktober"));
		bulans.add(new GeneralDto("11","Nopember"));
		bulans.add(new GeneralDto("12","Desember"));
/*		bulans.add("Januari"); bulans.add("Februari"); bulans.add("Maret");
		bulans.add("April"); bulans.add("Mei"); bulans.add("Juni");
		bulans.add("Juli"); bulans.add("Agustus"); bulans.add("September");
		bulans.add("Oktober"); bulans.add("Nopember"); bulans.add("Desember"); */
		bulan=bulans.get(0);
		tahun = getThn();
		BindUtils.postNotifyChange(null, null, this, "tahun");
		BindUtils.postNotifyChange(null, null, this, "bulans");
		BindUtils.postNotifyChange(null, null, this, "bulan");
	}
	
	private void setView(int tipe, int kantor, int pilihan, 
			int cidera, int eg, int tgl, int simpul, 
			int kodeTgl, int pejabat, int periode, int bulan){
//		1,1,1, 1,0,0,0, 0,0,1,0
		setViewTipe(getTrue(tipe)); setViewKantor(getTrue(kantor)); setViewPilihan(getTrue(pilihan));
		setViewCidera(getTrue(cidera)); setViewEG(getTrue(eg)); setViewTipeTgl(getTrue(tgl));
		setViewSimpul(getTrue(simpul)); setViewKodeTgl(getTrue(kodeTgl)); setViewPejabat(getTrue(pejabat));
		setViewPeriode(getTrue(periode)); setViewBulan(getTrue(bulan));
		BindUtils.postNotifyChange(null, null, this, "viewTipe");
		BindUtils.postNotifyChange(null, null, this, "viewKantor");
		BindUtils.postNotifyChange(null, null, this, "viewPilihan");
		BindUtils.postNotifyChange(null, null, this, "viewCidera");
		BindUtils.postNotifyChange(null, null, this, "viewEG");
		BindUtils.postNotifyChange(null, null, this, "viewTipeTgl");
		BindUtils.postNotifyChange(null, null, this, "viewSimpul");
		BindUtils.postNotifyChange(null, null, this, "viewKodeTgl");
		BindUtils.postNotifyChange(null, null, this, "viewPejabat");
		BindUtils.postNotifyChange(null, null, this, "viewPeriode");
		BindUtils.postNotifyChange(null, null, this, "viewBulan");
	}
	
	private void showParam(String no){
		//tipe, kantor, pilihan, //cidera, eg,tipetgl, simpul,
		//kodetgl, pejabat periode bulan
		if(no.equals("1")||no.equals("2")||no.equals("4")||no.equals("5")
			|| no.equals("9")){
//			setView(1,1,1, 0,0,1,0, 1,1,1,0);
			setView(1,1,0, 0,0,0,0, 1,1,1,0);
			/*
			 * viewKantor, viewPeriode, viewPilihan, viewPejabat
			 */
		} 
		if(no.equals("3")||no.equals("6")||no.equals("8")){
			setView(0,1,0, 0,0,0,0, 0,1,1,0);
//			setView(1,1,0, 0,0,0,0, 0,1,1,0);
		}
		if(no.equals("7")){
			setView(0,1,0, 0,1,0,0, 0,1,1,0);
		}
		if(no.equals("10")|| no.equals("11")||no.equals("12")||
			no.equals("13")||no.equals("14")||no.equals("15")||
			no.equals("16")||no.equals("17")){
			setView(0,1,0, 0,0,0,0, 0,0,1,0);
		}
		if(no.equals("18")||no.equals("19")){
			setView(0,1,0, 0,0,0,0, 0,1,1,0);
		}
		if(no.equals("20")||
				no.equals("21")){
			setView(0,1,0, 0,0,0,0, 0,1,0,1);
		}
		//tipe, kantor, pilihan, //cidera, eg,tipetgl, simpul,
		//kodetgl, pejabat periode bulan
		if(no.equals("22")){
			setView(0,1,1, 0,0,0,0, 0,1,1,0);
		}
		if(no.equals("23")){
			setView(0,1,1, 1,0,0,0, 0,0,1,0);
		}
		if(no.equals("24")){
//			setView(1,1,0, 0,0,0,0, 0,1,1,0);
			setView(0,1,0, 0,0,0,0, 0,1,0,1);
		}
		if(no.equals("25")||no.equals("26")){
//			setView(1,1,0, 0,0,0,0, 0,1,1,0);
			setView(0,1,0, 0,0,0,0, 0,1,1,0);
		}
		if(no.equals("27")||no.equals("29")||no.equals("30")){
			setView(0,1,0, 0,0,0,0, 0,0,1,0);
		}
		if(no.equals("28")){
			setView(0,1,0, 1,0,1,0, 1,1,1,0);
			setViewStatusLP(true);
		}else{
			setViewStatusLP(false);
		}
		BindUtils.postNotifyChange(null, null, this, "viewTipe");
		BindUtils.postNotifyChange(null, null, this, "viewKantor");
		BindUtils.postNotifyChange(null, null, this, "viewPilihan");
		BindUtils.postNotifyChange(null, null, this, "viewCidera");
		BindUtils.postNotifyChange(null, null, this, "viewEG");
		BindUtils.postNotifyChange(null, null, this, "viewTipeTgl");
		BindUtils.postNotifyChange(null, null, this, "viewSimpul");
		BindUtils.postNotifyChange(null, null, this, "viewKodeTgl");
		BindUtils.postNotifyChange(null, null, this, "viewStatusLP");
		BindUtils.postNotifyChange(null, null, this, "viewPejabat");
	}
	
	private boolean getTrue(int input){
		return input==0?false:true;
	}
	
	private void loadListLaporan(){
		listLaporan = new ArrayList<>();
		listLaporan.add(new LaporanDto("Rekapitulasi Pengajuan","Rekapitulasi_Pengajuan_Klaim.jrxml","RPT.PL.C.14-0","1"));
		listLaporan.add(new LaporanDto("Rekapitulasi Penyelesaian","Rekapitulasi_Penyelesaian_Klaim.jrxml","RPT.PL.C.15-0","2"));
		listLaporan.add(new LaporanDto("Rekapitulasi Outstanding","Rekapitulasi_Outstanding_Klaim.jrxml","RPT.PL.C.16-0","3"));
		listLaporan.add(new LaporanDto("Rekapitulasi Pembayaran","Rekapitulasi_Pembayaran_Klaim.jrxml","RPT.PL.C.17-0","4"));
		listLaporan.add(new LaporanDto("Perbandingan Rekapitulasi Pembayaran Dasi Jr dan LHU","Rekapitulasi_Pembayaran_Klaim_DASI_dan_LHU.jrxml","RPT.PL.C.LHU-0","5"));
		listLaporan.add(new LaporanDto("Rekapitulasi Pembayaran Limpahan","Rekapitulasi_Pembayaran_Limpahan.jrxml","RPT.PL.C.LIMPAHAN-0","6"));
		listLaporan.add(new LaporanDto("Rekapitulasi Pembayaran Ex Gratia","Rekapitulasi_Pembayaran_Ex_Gratia.jrxml","RPT.PL.C.18-0","7"));
		listLaporan.add(new LaporanDto("Rekapitulasi Penolakan Klaim","Rekapitulasi_Penolakan_Klaim.jrxml","RPT.PL.C.20-0","8"));
		listLaporan.add(new LaporanDto("Rekapitulasi Pelimpahan Klaim","Rekapitulasi_Pelimpahan_Klaim.jrxml","RPT.PL.C.21-0","9"));
		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim sejak Tgl Laka MD (Terjamin)","Rekapitulasi_Penyelesaian_Klaim_sejak_Tanggal_Laka_MD_(Terjamin).jrxml","RPT.PL.C.26-2","10"));
//		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim sejak Tgl Laka MD (Terjamin)","Rekap_Penyelesaian_Klaim_sejak_Tgl_Laka_MD_(Terjamin).jrxml","RPT.PL.C.26-2","10"));
		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim Sejak Tgl Meninggal MD & MD-LL (Terjamin)","Rekap_Penyelesaian_Klaim_Sejak_Tgl_Meninggal_MD_&_MD-LL_(Terjamin).jrxml","RPT.PL.C.27-2","11"));
		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim Sejak Tgl Laka/Meninggal MD & MD-LL (Terjamin)","Rekap_Penyelesaian_Klaim_Sejak_Tgl_Laka_Meninggal_MD_&_MD-LL_(Terjamin).jrxml","RPT.PL.C.28-2","12"));
		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim Sejak Tgl Laka (MD) (Penelitian LL)","Rekap_Penyelesaian_Klaim_Sejak_Tgl_Laka_(MD)_(Penelitian_LL).jrxml","RPT.PL.C.29-2","13"));
		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim Sejak Tgl Meninggal MD-LL(Penelitian LL)","Rekap_Penyelesaian_Klaim_Sejak_Tgl_Meninggal_MD-LL(Penelitian_LL).jrxml","RPT.PL.C.30-2","14"));
		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim Sejak Tgl Laka/Meninggal MD & MD-LL (Penelitian LL)","Rekap_Penyelesaian_Klaim_Sejak_Tgl_Laka_Meninggal_MD_&_MD-LL_(Penelitian_LL).jrxml","RPT.PL.C.31-2","15"));
		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim Sejak Tgl Laka MD (Terjamin & Penelitian LL)","Rekap_Penyelesaian_Klaim_Sejak_Tgl_Laka_Meninggal_MD_&_MD-LL_(Penelitian_LL).jrxml","RPT.PL.C.32-2","16"));
//		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim Sejak Tgl Laka MD (Terjamin & Penelitian LL)","Rekap_Penyelesaian_Klaim_Sejak_Tgl_Laka_MD_(Terjamin_&_Penelitian_LL).jrxml","RPT.PL.C.32-2","16"));
		listLaporan.add(new LaporanDto("Rekap Penyelesaian Klaim Sejak Tgl Meninggal MD-LL(Terjamin & Penelitian LL)","Rekap_Penyelesaian_Klaim_Sejak_Tgl_Meninggal_MD_LL(Terjamin_&_Penelitian_LL).jrxml","RPT.PL.C.33-2","17"));
		listLaporan.add(new LaporanDto("Rekapitulasi Kecepatan Klaim Sejak Tgl Laka/Meninggal (MD & MD-LL) Per Instansi","Rekapitulasi_Kecepatan_Klaim_sejak_tgl_Laka_Meninggal(MD_&MD_LL)PER_INSTANSI.jrxml","NAC01-0","18"));
		listLaporan.add(new LaporanDto("Rekapitulasi Kecepatan Klaim Sejak Tgl Laka/Meninggal (MD & MD-LL) Per Samsat","Rekapitulasi_Kecepatan_Klaim_sejak_tgl_Laka_Meninggal(MD_&MD_LL)PER_SAMSAT.jrxml","NAC02-0","19"));
		listLaporan.add(new LaporanDto("Laporan Ratio Klaim","Laporan_Ratio_Klaim.jrxml","RPT.PL.C.34-1","20"));
		listLaporan.add(new LaporanDto("Realisasi Biaya Survey Materai","Laporan_Realisasi_Biaya_Survey_dan_Materai.jrxml","RPT.PL.C.35-1","21"));
		listLaporan.add(new LaporanDto("Rekapitulasi Pembayaran Klaim Per Kasus Kecelakaan","Rekapitulasi_Pembayaran_Klaim_Per_Kasus_Kecelakaan.jrxml","RPT.PL.C.36-0","22"));
		listLaporan.add(new LaporanDto("Daftar Kecepatan Sejak Tgl Kecelakaan/Meninggal per Berkas","Daftar_Kecepatan_Klaim_Per_Berkas_Sejak_Tgl_Kecelakaan_Meninggal.jrxml","NAC08-6","23"));
		listLaporan.add(new LaporanDto("Rekapitulasi Penyerapan Penyelesaian Santunan","Rekapitulasi_Penyerapan_Penyelesaian_Santunan.jrxml","NAC10-1","24"));
		listLaporan.add(new LaporanDto("Laporan Jumlah Pengajuan Luka-luka per Rumah Sakit","Laporan_Jumlah_Pengajuan_Luka-luka_per_Rumah_Sakit.jrxml","NAC13-0","25"));
		//listLaporan.add(new LaporanDto("Rekapitulasi Pembayaran Overbooking Per Rumah Sakit","Rekapitulasi_Pembayaran_Overbooking_Per_Rumah_Sakit.jrxml","NAC16-0","26"));
		listLaporan.add(new LaporanDto("Rekapitulasi Pembayaran Overbooking Per Rumah Sakit","test.jrxml","NAC16-0","26"));
		listLaporan.add(new LaporanDto("Daftar Perhitungan Kecepatan","Daftar_Perhitungan_Kecepatan.jrxml","NAC11-2","27"));
		listLaporan.add(new LaporanDto("Laporan Data Kecelakaan","Laporan_Data_Kecelakaan.jrxml","NAC12-3","28"));
		listLaporan.add(new LaporanDto("Rekapitulasi Penyelesaian Klaim dihitung menurut Jam Penyelesaian","Rekapitulasi_Penyelesaian_Klaim_dihitung_menurut_Jam_Penyelesaian.jrxml","NAC03-2","29"));
		listLaporan.add(new LaporanDto("Rekapitulasi Penyelesaian Klaim dihitung menurut jam penyelesaian (1.1)","Rekapitulasi_Penyelesaian_Klaim_dihitung_menurut_jam_penyelesaian_(1.1).jrxml","NAC04-2","30"));
		laporan = listLaporan.get(0);
		BindUtils.postNotifyChange(null, null, this, "listLaporan");
		BindUtils.postNotifyChange(null, null, this, "laporan");
	}
	
	//TODO: Print Section
	
	
	public void print() throws ClassNotFoundException, SQLException, IOException, JRException, Exception {
		Map<String, Object> param = getParam();
//		System.out.println("PARAMETEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEER");
//		System.out.println(JsonUtil.getJson(getParam()));
		cetak(getParam());
		System.out.println(JsonUtil.getJson(param));
		System.out.println("NO Laporan : "+laporan.getNo());
//		System.out.println(dateToString(tglAwal));
		
	}
	
	@Command("cetak")
	public void callPrint(@BindingParam("popup")String popup) throws ClassNotFoundException, SQLException, IOException, JRException, Exception{
		print();
		Map<String, Object> args = new HashMap<>();
		args.put("media", fileContent);
		((Window) Executions.createComponents(UIConstants.BASE_PAGE_PATH+popup, null, args)).doModal();
	}
	
	private Map<String, Object> getParam(){
		Map<String, Object> param = new HashMap<>();
		param.putAll(getKodeKantor(kantor.getKodeInstansi()));
		param.putAll(getTipeTanggal(tipeTanggal.getKode()));
		param.putAll(getTglAwal(dateToString(tglAwal)));
		param.putAll(getTglAkhir(dateToString(tglAkhir)));
		param.putAll(getSifatCidera(sifatCiedera.getKode()));
		try{
			param.put("JENIS_EG", eg.getKode() );
		}catch(Exception s){
			System.out.println("JENIS EG Null");
		}
		String kesimpulan = "";
		if(laporan.getNo().equals("6") || laporan.getNo().equals("24")){
			String tipe = "";
			if(jenisLaporan.getKode().equalsIgnoreCase("C")){
				param.putAll(getTipeLaporan("CABANG"));
			}else if(jenisLaporan.getKode().equalsIgnoreCase("N")){
				param.putAll(getTipeLaporan("NASIONAL"));
			}else if(jenisLaporan.getKode().equalsIgnoreCase("P")){
				param.putAll(getTipeLaporan("PERWAKILAN"));
			}else if(jenisLaporan.getKode().equalsIgnoreCase("L")){
				param.putAll(getTipeLaporan("LOKET"));
			}else{
				param.putAll(getTipeLaporan("CABANG"));
			}
		}else if(laporan.getNo().equals("18") ||
				laporan.getNo().equals("19")|| 
				laporan.getNo().equals("25")|| 
				laporan.getNo().equals("26")|| 
				laporan.getNo().equals("27")|| 
				laporan.getNo().equals("29")){
			String tipe = "";
			if(jenisLaporan.getKode().equalsIgnoreCase("C")){
				param.putAll(getTipeLaporan("Cabang"));
			}else if(jenisLaporan.getKode().equalsIgnoreCase("N")){
				param.putAll(getTipeLaporan("Nasional"));
			}else if(jenisLaporan.getKode().equalsIgnoreCase("P")){
				param.putAll(getTipeLaporan("Perwakilan"));
			}else if(jenisLaporan.getKode().equalsIgnoreCase("L")){
				param.putAll(getTipeLaporan("Loket"));
			}else{
				param.putAll(getTipeLaporan("Cabang"));
			}
		}else{
			param.putAll(getTipeLaporan(jenisLaporan.getKode()));
		}
		if(laporan.getNo().equals("10") || 
				laporan.getNo().equals("11") || 
				laporan.getNo().equals("12") || 
				laporan.getNo().equals("12") ){
			kesimpulan = "1";
		}
		if(laporan.getNo().equals("13") || 
				laporan.getNo().equals("14") || 
				laporan.getNo().equals("15")){
			kesimpulan = "3";
		}
		if(laporan.getNo().equals("16") || 
				laporan.getNo().equals("17")){
			kesimpulan = "13";
		}
		param.putAll(getKesimpulan(kesimpulan));
		param.putAll(getPilihan(pilihan.getKode()));
		String kdCidera = "";
		if(laporan.getNo().equals("10") || 
				laporan.getNo().equals("13") || 
				laporan.getNo().equals("16")){
			kdCidera = "01";
		}
		if(laporan.getNo().equals("11") || 
				laporan.getNo().equals("14") || 
				laporan.getNo().equals("17")){
			kdCidera = "05";
		}
		if(laporan.getNo().equals("12") || 
				laporan.getNo().equals("15")){
			kdCidera = "15";
		}
		param.putAll(getKdCidera(kdCidera));
		if(laporan.getNo().equals("20") || laporan.getNo().equals("21")){
			param.put("Jenis_Periode", "PTD");
			param.put("jenisPeriode", "PTD");
		}
		if(laporan.getNo().equals("29")){
			kdCidera="AA";
		}
		if (laporan.getNo().equals("30")){
			kdCidera="01";
		}
		param.putAll(getKodeCidera(kdCidera));
		param.putAll(getCidera(sifatCiedera.getKode()));
		//TODO: set simpul sementara
		if(laporan.getNo().equals("29")){
			param.putAll(getSimpulSementara("AA"));
		}
		
		if(laporan.getNo().equals("30")){
			param.putAll(getSimpulSementara("1"));
		}
		param.put("Periode_Tahun", tahun);
		param.put("Periode_Bulan", bulan.getKode());
		param.put("namaPejabat1", getNamaPejabat1());
		param.put("namaPetugas1", getNamaPejabat1());
		param.put("petugas_1", getNamaPejabat1());
		param.put("PETUGAS1", getNamaPejabat1());
		param.put("namaPetugas2", getNamaPejabat2());
		param.put("namaPejabat2", getNamaPejabat2());
		param.put("petugas_2", getNamaPejabat2());
		param.put("PETUGAS2", getNamaPejabat2());
		param.put("jabatanPetugas1", getPosisiJabatan1());
		param.put("jabatan_1", getPosisiJabatan1());
		param.put("JABATAN1", getPosisiJabatan1());
		param.put("JABATAN2", getPosisiJabatan2());
		param.put("jabatan_2", getPosisiJabatan2());
		param.put("jabatanPetugas2", getPosisiJabatan2());
		param.put("userLogin", getCurrentUserSessionJR().getLoginID());
		param.put("USER_LOGIN", getCurrentUserSessionJR().getLoginID());
		param.put("cabang", getCurrentUserSessionJR().getNamaKantor());
		param.put("CABANG", getCurrentUserSessionJR().getNamaKantor());
		param.put("lokasiKantor", getIbukota());
		param.put("lokasi_kantor", getIbukota());
		param.put("LOKASI_KANTOR", getIbukota());
		param.put("provinsi", getProvinsi());
		param.put("PROVINSI", getProvinsi());
		param.put("kota", getProvinsi());
		try{
			param.put("Mode_Tgl", getUseTgl().getKode());
		}catch(Exception s){
			System.out.println("mode tgl is null");
		}
		param.put("statLP",statusLp.getKode());
		return param;
	}
	
//	@Command("cetak")
//	public void printDoc(){
//		Map<String, Object> param = new HashMap<>();
//		param = 
//	}
	
	/*private String dateToString(Date input){
		String s = "";
		try{
			
		}catch (Exception l){
			l.printStackTrace();
		}
		return s;
	}*/
	private JasperReport readReportFromUrl(String kodeLaporan) throws IOException, JRException {
		File tempJsp = File.createTempFile(kodeLaporan, ".jrxml");
		ByteArrayOutputStream baos = getByteArrayOutputStream(kodeLaporan);
		try (OutputStream outputStream = new FileOutputStream(tempJsp)) {
			baos.writeTo(outputStream);
			outputStream.close();
		} catch (Exception x) {
			x.printStackTrace();
		}
		return JasperCompileManager.compileReport(tempJsp
				.getAbsolutePath());
	}
	
	@SuppressWarnings("deprecation")
	private void cetak(Map<String, Object> parameter) throws ClassNotFoundException, SQLException, 
		IOException, JRException, Exception{
		Connection conn = JasperConnectionUtil.getOracleConnection();
	
		File temp = File.createTempFile("ReportJR", ".pdf");
		FileOutputStream outs = new FileOutputStream(temp);
		File tempJsp = File.createTempFile("Jasper", ".jrxml");
		JasperReport jasper = null;
		try{
			ByteArrayOutputStream baos = getByteArrayOutputStream(laporan.getKodeLaporan());
			try (OutputStream outputStream = new FileOutputStream(tempJsp)) {
				baos.writeTo(outputStream);
				outputStream.close();
			} catch (Exception x) {
				x.printStackTrace();
			}
			jasper = JasperCompileManager.compileReport(tempJsp
					.getAbsolutePath());
		}catch(Exception x){
			x.printStackTrace();
			jasper = JasperCompileManager.compileReport(UIConstants.SUBREPORT_DIR +laporan.getKodeLaporan());
		}

		if(laporan.getNo().equals("28")){
			JasperReport subReport1 = readReportFromUrl("SubReportLaporanDataLaka_Detail1.jrxml");
			JasperReport subReport2 = readReportFromUrl("SubReportLaporanDataLaka_Detail2.jrxml");
			parameter.put("subReport1", subReport1);
			parameter.put("subReport2", subReport2);
		}
		
//		s
		JasperPrint jasperReport = JasperFillManager.fillReport(jasper, parameter,
				conn);
		jasperReport.setProperty("net.sf.jasperreports.query.executer.factory.plsql"
                ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
//		new com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory();
//		JRProperties.setProperty( JRQueryExecuterFactory.QUERY_EXECUTER_FACTORY_PREFIX+"plsql"
//                ,"com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
	
		// PDF-Export
		JRPdfExporter exportPdf = new JRPdfExporter();
		System.out.println(temp.getAbsolutePath());
		exportPdf.setParameter(JRExporterParameter.JASPER_PRINT, jasperReport);
		exportPdf.setParameter(JRExporterParameter.OUTPUT_STREAM, outs);
		exportPdf.exportReport();
	
		// show exported pdf 
		File f = new File(temp.getPath());
		byte[] buffer = new byte[(int) f.length()];
		FileInputStream fs = new FileInputStream(f);
		fs.read(buffer);
		fs.close();
		ByteArrayInputStream is = new ByteArrayInputStream(buffer);
		fileContent = new AMedia("report", "pdf", "application/pdf", is);
//		File temp2 = File.createTempFile("ReportJR", ".pdf");
		
//		Executions.getCurrent().sendRedirect(temp2.getPath(), "_blank");
		
		
		
//		Filedownload.save(new AMedia(laporan.getNamaLaporan(),"pdf","application/file", is));
		/*
		 * InputStream fis = new FileInputStream(temp);
		Filedownload.save(new AMedia("Data Kecelakaan", "docx", "application/file", fis));
		temp.delete();
		 */
		
		
		
	}
	
	
	private ByteArrayOutputStream getByteArrayOutputStream(String... param)
			throws IOException, FileNotFoundException {
		URL url;
		String reportName = "";
		if(param.length>0){
			url = new URL(getLoc() + "/" + param[0]);
		}else{
			url = new URL(getLoc() + "/" + param[0]);
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			is = url.openStream();
			byte[] byteChunk = new byte[4096];
			int n;
			while ((n = is.read(byteChunk)) > 0) {
				baos.write(byteChunk, 0, n);
			}
		} catch (IOException e) {
			System.err.printf("Failed while reading bytes from %s: %s",
					url.toExternalForm(), e.getMessage());
			e.printStackTrace();
		} finally {
			if (is != null) {
				is.close();
			}
		}
	
		return baos;
	}
	
	private String getLoc() {
		String port = (Executions.getCurrent().getServerPort() == 80) ? ""
				: (":" + Executions.getCurrent().getServerPort());
		/*
		 * String url = Executions.getCurrent().getScheme() + "://" +
		 * Executions.getCurrent().getServerName() + port +
		 * Executions.getCurrent().getContextPath() +
		 * Executions.getCurrent().getDesktop().getRequestPath();
		 */
		String x = "";
		if(System.getProperty("location")!=null){
			if(System.getProperty("location").equalsIgnoreCase("linux")){
				x = "/home/glassfish/report/bulanan";
			} else{
				x = Executions.getCurrent().getScheme() + "://"
						+ Executions.getCurrent().getServerName() + port
						+ Executions.getCurrent().getContextPath()+"/report/bulanan";
			}
		}else{
			x = Executions.getCurrent().getScheme() + "://"
					+ Executions.getCurrent().getServerName() + port
					+ Executions.getCurrent().getContextPath()+"/report/bulanan";
		}
		return x;
	}
	
	public List<LaporanDto> getListLaporan() {
		return listLaporan;
	}
	public void setListLaporan(List<LaporanDto> listLaporan) {
		this.listLaporan = listLaporan;
	}
	public LaporanDto getLaporan() {
		return laporan;
	}
	public void setLaporan(LaporanDto laporan) {
		this.laporan = laporan;
	}
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
	public String getNamaLaporan() {
		return namaLaporan;
	}
	public void setNamaLaporan(String namaLaporan) {
		this.namaLaporan = namaLaporan;
	}
	public Date getTglAwal() {
		return tglAwal;
	}
	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}
	public Date getTglAkhir() {
		return tglAkhir;
	}
	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}
	public String getNamaPejabat1() {
		return namaPejabat1;
	}
	public void setNamaPejabat1(String namaPejabat1) {
		this.namaPejabat1 = namaPejabat1;
	}
	public String getNamaPejabat2() {
		return namaPejabat2;
	}
	public void setNamaPejabat2(String namaPejabat2) {
		this.namaPejabat2 = namaPejabat2;
	}
	public String getPosisiJabatan1() {
		return posisiJabatan1;
	}
	public void setPosisiJabatan1(String posisiJabatan1) {
		this.posisiJabatan1 = posisiJabatan1;
	}
	public String getPosisiJabatan2() {
		return posisiJabatan2;
	}
	public void setPosisiJabatan2(String posisiJabatan2) {
		this.posisiJabatan2 = posisiJabatan2;
	}

	public String getLaporanStr() {
		return laporanStr;
	}

	public void setLaporanStr(String laporanStr) {
		this.laporanStr = laporanStr;
	}
	public boolean isViewTipe() {
		return viewTipe;
	}
	public void setViewTipe(boolean viewTipe) {
		this.viewTipe = viewTipe;
	}
	public boolean isViewKantor() {
		return viewKantor;
	}
	public void setViewKantor(boolean viewKantor) {
		this.viewKantor = viewKantor;
	}
	public boolean isViewPilihan() {
		return viewPilihan;
	}
	public void setViewPilihan(boolean viewPilihan) {
		this.viewPilihan = viewPilihan;
	}
	public boolean isViewCidera() {
		return viewCidera;
	}
	public void setViewCidera(boolean viewCidera) {
		this.viewCidera = viewCidera;
	}
	public boolean isViewEG() {
		return viewEG;
	}
	public void setViewEG(boolean viewEG) {
		this.viewEG = viewEG;
	}
	public boolean isViewTipeTgl() {
		return viewTipeTgl;
	}
	public void setViewTipeTgl(boolean viewTipeTgl) {
		this.viewTipeTgl = viewTipeTgl;
	}
	public boolean isViewSimpul() {
		return viewSimpul;
	}
	public void setViewSimpul(boolean viewSimpul) {
		this.viewSimpul = viewSimpul;
	}
	public boolean isViewKodeTgl() {
		return viewKodeTgl;
	}
	public void setViewKodeTgl(boolean viewKodeTgl) {
		this.viewKodeTgl = viewKodeTgl;
	}
	public boolean isViewPejabat() {
		return viewPejabat;
	}
	public void setViewPejabat(boolean viewPejabat) {
		this.viewPejabat = viewPejabat;
	}

	public String getTahun() {
		return tahun;
	}

	public void setTahun(String tahun) {
		this.tahun = tahun;
	}

	public boolean isViewPeriode() {
		return viewPeriode;
	}

	public void setViewPeriode(boolean viewPeriode) {
		this.viewPeriode = viewPeriode;
	}

	public boolean isViewBulan() {
		return viewBulan;
	}

	public void setViewBulan(boolean viewBulan) {
		this.viewBulan = viewBulan;
	}

	public List<GeneralDto> getListSifatCidera() {
		return listSifatCidera;
	}

	public void setListSifatCidera(List<GeneralDto> listSifatCidera) {
		this.listSifatCidera = listSifatCidera;
	}

	public GeneralDto getSifatCiedera() {
		return sifatCiedera;
	}

	public void setSifatCiedera(GeneralDto sifatCiedera) {
		this.sifatCiedera = sifatCiedera;
	}

	public String getLaporanCode() {
		return laporanCode;
	}

	public void setLaporanCode(String laporanCode) {
		this.laporanCode = laporanCode;
	}

	public GeneralDto getJenisLaporan() {
		return jenisLaporan;
	}

	public void setJenisLaporan(GeneralDto jenisLaporan) {
		this.jenisLaporan = jenisLaporan;
	}

	public List<GeneralDto> getListJenisLaporan() {
		return listJenisLaporan;
	}

	public void setListJenisLaporan(List<GeneralDto> listJenisLaporan) {
		this.listJenisLaporan = listJenisLaporan;
	}

	public GeneralDto getBulan() {
		return bulan;
	}

	public void setBulan(GeneralDto bulan) {
		this.bulan = bulan;
	}

	public List<GeneralDto> getBulans() {
		return bulans;
	}

	public void setBulans(List<GeneralDto> bulans) {
		this.bulans = bulans;
	}

	public GeneralDto getStatusLp() {
		return statusLp;
	}

	public void setStatusLp(GeneralDto statusLp) {
		this.statusLp = statusLp;
	}

	public List<GeneralDto> getListStatusLp() {
		return listStatusLp;
	}

	public void setListStatusLp(List<GeneralDto> listStatusLp) {
		this.listStatusLp = listStatusLp;
	}

	public List<GeneralDto> getListPilihan() {
		return listPilihan;
	}

	public void setListPilihan(List<GeneralDto> listPilihan) {
		this.listPilihan = listPilihan;
	}

	public GeneralDto getPilihan() {
		return pilihan;
	}

	public void setPilihan(GeneralDto pilihan) {
		this.pilihan = pilihan;
	}

	public GeneralDto getTipeTanggal() {
		return tipeTanggal;
	}

	public void setTipeTanggal(GeneralDto tipeTanggal) {
		this.tipeTanggal = tipeTanggal;
	}

	public List<GeneralDto> getListTipeTanggal() {
		return listTipeTanggal;
	}

	public void setListTipeTanggal(List<GeneralDto> listTipeTanggal) {
		this.listTipeTanggal = listTipeTanggal;
	}

	public List<GeneralDto> getListEG() {
		return listEG;
	}

	public void setListEG(List<GeneralDto> listEG) {
		this.listEG = listEG;
	}

	public GeneralDto getEg() {
		return eg;
	}

	public void setEg(GeneralDto eg) {
		this.eg = eg;
	}

	public GeneralDto getUseTgl() {
		return useTgl;
	}

	public void setUseTgl(GeneralDto useTgl) {
		this.useTgl = useTgl;
	}

	public List<GeneralDto> getListUseTgl() {
		return listUseTgl;
	}

	public void setListUseTgl(List<GeneralDto> listUseTgl) {
		this.listUseTgl = listUseTgl;
	}

	public List<PlInstansiDto> getListKantor() {
		return listKantor;
	}

	public void setListKantor(List<PlInstansiDto> listKantor) {
		this.listKantor = listKantor;
	}

	public PlInstansiDto getKantor() {
		return kantor;
	}

	public void setKantor(PlInstansiDto kantor) {
		this.kantor = kantor;
	}

	public String getSearchKantor() {
		return searchKantor;
	}

	public void setSearchKantor(String searchKantor) {
		this.searchKantor = searchKantor;
	}

	public String getIbukota() {
		return ibukota;
	}

	public void setIbukota(String ibukota) {
		this.ibukota = ibukota;
	}

	public String getProvinsi() {
		return provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

	public boolean isViewStatusLP() {
		return viewStatusLP;
	}

	public void setViewStatusLP(boolean viewStatusLP) {
		this.viewStatusLP = viewStatusLP;
	}
	

	
}
