package ui.report;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;

import org.hibernate.jpa.criteria.predicate.IsEmptyPredicate;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import share.GeneralDto;
import share.PlPengajuanSantunanDto;
import common.util.JasperConnectionUtil;

public class PopUpReportVmd {

	private AMedia fileContent;
	private String detailHandler;
	
	@Wire("#opAbsahDet")
	private Window winLov;
	private GeneralDto dto;
	private Map<String, Object> parameter = new HashMap<>();
	
	@Command
	public void close(){
		if (winLov == null)
			throw new RuntimeException(
					"id popUp tidak sama dengan viewModel");
		winLov.detach();
	}
	
	@Init
	public void baseInit(
			@ExecutionArgParam("detailHandler") String detailHandler,
			@ExecutionArgParam("param") Map<String,Object> param,
			@ExecutionArgParam("dto") GeneralDto dto,
			@ContextParam(ContextType.VIEW) Component view) throws ClassNotFoundException, SQLException, IOException, JRException, Exception {
		Selectors.wireComponents(view, this, false);
		this.detailHandler = detailHandler;
		this.parameter = param;
		this.dto = dto;
//		BindUtils.postNotifyChange(null, null, this, "fileContent");
		System.out.println("Base init called");
		print();
	}
	
	private String getLoc() {
		String port = (Executions.getCurrent().getServerPort() == 80) ? ""
				: (":" + Executions.getCurrent().getServerPort());
		/*
		 * String url = Executions.getCurrent().getScheme() + "://" +
		 * Executions.getCurrent().getServerName() + port +
		 * Executions.getCurrent().getContextPath() +
		 * Executions.getCurrent().getDesktop().getRequestPath();
		 */
		String x = "";
		if(System.getProperty("location")!=null){
			if(System.getProperty("location").equalsIgnoreCase("linux")){
				x = "/home/glassfish/report/bulanan/";
			} else{
				x = Executions.getCurrent().getScheme() + "://"
						+ Executions.getCurrent().getServerName() + port
						+ Executions.getCurrent().getContextPath();
			}
		}else{
			x = Executions.getCurrent().getScheme() + "://"
					+ Executions.getCurrent().getServerName() + port
					+ Executions.getCurrent().getContextPath();
		}
		return x;
	}
	
	@SuppressWarnings("deprecation")
	@NotifyChange("fileContent")
	private void print() throws ClassNotFoundException, SQLException, 
		IOException, JRException, Exception{
		Connection conn = JasperConnectionUtil.getOracleConnection();

		File temp = File.createTempFile("ReportJR", ".pdf");
		FileOutputStream outs = new FileOutputStream(temp);
		File tempJsp = File.createTempFile("Jasper", ".jrxml");
		ByteArrayOutputStream baos = getByteArrayOutputStream(dto.getCodeName());
		try (OutputStream outputStream = new FileOutputStream(tempJsp)) {
			baos.writeTo(outputStream);
			outputStream.close();
		} catch (Exception x) {
			x.printStackTrace();
		}
		JasperReport jasper = JasperCompileManager.compileReport(tempJsp
				.getAbsolutePath());
		
		JasperPrint jasperReport = JasperFillManager.fillReport(jasper, parameter,
				conn);

		// PDF-Export
		JRPdfExporter exportPdf = new JRPdfExporter();
		System.out.println(temp.getAbsolutePath());
		exportPdf.setParameter(JRExporterParameter.JASPER_PRINT, jasperReport);
		exportPdf.setParameter(JRExporterParameter.OUTPUT_STREAM, outs);
		exportPdf.exportReport();

		// show exported pdf 
		File f = new File(temp.getPath());
		byte[] buffer = new byte[(int) f.length()];
		FileInputStream fs = new FileInputStream(f);
		fs.read(buffer);
		fs.close();
		ByteArrayInputStream is = new ByteArrayInputStream(buffer);
		fileContent = new AMedia("report", "pdf", "application/pdf", is);
	}
	
	private ByteArrayOutputStream getByteArrayOutputStream(String... param)
			throws IOException, FileNotFoundException {
		URL url;
		String reportName = "";
		if(param.length>0){
			url = new URL(getLoc() + "/report/"+param[0]+".jrxml");
		}else{
			url = new URL(getLoc() + "/report/report1.jrxml");
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		InputStream is = null;
		try {
			is = url.openStream();
			byte[] byteChunk = new byte[4096];
			int n;
			while ((n = is.read(byteChunk)) > 0) {
				baos.write(byteChunk, 0, n);
			}
		} catch (IOException e) {
			System.err.printf("Failed while reading bytes from %s: %s",
					url.toExternalForm(), e.getMessage());
			e.printStackTrace();
		} finally {
			if (is != null) {
				is.close();
			}
		}

		return baos;
	}

	public AMedia getFileContent() {
		return fileContent;
	}

	public void setFileContent(AMedia fileContent) {
		this.fileContent = fileContent;
	}

	public String getDetailHandler() {
		return detailHandler;
	}

	public void setDetailHandler(String detailHandler) {
		this.detailHandler = detailHandler;
	}

	public GeneralDto getDto() {
		return dto;
	}

	public void setDto(GeneralDto dto) {
		this.dto = dto;
	}

	public Map<String, Object> getParameter() {
		return parameter;
	}

	public void setParameter(Map<String, Object> parameter) {
		this.parameter = parameter;
	}
	
}
