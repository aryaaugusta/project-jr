package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FND_KANTOR_JASARAHARJA_SAMSAT database table.
 * 
 */
@Entity
@Table(name="FND_KANTOR_JASARAHARJA_SAMSAT")
@NamedQuery(name="FndKantorJasaraharjaSamsat.findAll", query="SELECT f FROM FndKantorJasaraharjaSamsat f")
public class FndKantorJasaraharjaSamsat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="SAMSAT_INDUK")
	private String samsatInduk;

	@Column(name="STAT_ENTRY")
	private String statEntry;

	@Column(name="STAT_HASIL")
	private String statHasil;

	@Column(name="STAT_JENIS")
	private String statJenis;

	@Column(name="STAT_PETUGAS")
	private String statPetugas;

	public FndKantorJasaraharjaSamsat() {
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getSamsatInduk() {
		return this.samsatInduk;
	}

	public void setSamsatInduk(String samsatInduk) {
		this.samsatInduk = samsatInduk;
	}

	public String getStatEntry() {
		return this.statEntry;
	}

	public void setStatEntry(String statEntry) {
		this.statEntry = statEntry;
	}

	public String getStatHasil() {
		return this.statHasil;
	}

	public void setStatHasil(String statHasil) {
		this.statHasil = statHasil;
	}

	public String getStatJenis() {
		return this.statJenis;
	}

	public void setStatJenis(String statJenis) {
		this.statJenis = statJenis;
	}

	public String getStatPetugas() {
		return this.statPetugas;
	}

	public void setStatPetugas(String statPetugas) {
		this.statPetugas = statPetugas;
	}

}