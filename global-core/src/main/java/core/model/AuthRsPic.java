package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the AUTH_RS_PIC database table.
 * 
 */
@Entity
@Table(name="AUTH_RS_PIC")
@NamedQuery(name="AuthRsPic.findAll", query="SELECT a FROM AuthRsPic a")
@IdClass(AuthRsPicPK.class)
public class AuthRsPic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KODE_RUMAHSAKIT")
	private String kodeRumahsakit;

	@Id
	@Column(name="USER_LOGIN")
	private String userLogin;

	public AuthRsPic() {
	}

	public String getKodeRumahsakit() {
		return this.kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

	public String getUserLogin() {
		return this.userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

}