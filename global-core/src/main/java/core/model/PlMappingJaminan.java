package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_MAPPING_JAMINAN database table.
 * 
 */
@Entity
@Table(name="PL_MAPPING_JAMINAN")
@NamedQuery(name="PlMappingJaminan.findAll", query="SELECT p FROM PlMappingJaminan p")
public class PlMappingJaminan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_MAP")
	private String idMap;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="ID_JAMINAN")
	private String idJaminan;

	@Column(name="JML_BYR_AMBL")
	private BigDecimal jmlByrAmbl;

	@Column(name="JML_BYR_LL")
	private BigDecimal jmlByrLl;

	@Column(name="JML_BYR_P3K")
	private BigDecimal jmlByrP3k;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="NO_BPK")
	private String noBpk;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_PEMBUATAN_BPK")
	private Date tglPembuatanBpk;

	public PlMappingJaminan() {
	}

	public String getIdMap() {
		return this.idMap;
	}

	public void setIdMap(String idMap) {
		this.idMap = idMap;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdJaminan() {
		return this.idJaminan;
	}

	public void setIdJaminan(String idJaminan) {
		this.idJaminan = idJaminan;
	}

	public BigDecimal getJmlByrAmbl() {
		return this.jmlByrAmbl;
	}

	public void setJmlByrAmbl(BigDecimal jmlByrAmbl) {
		this.jmlByrAmbl = jmlByrAmbl;
	}

	public BigDecimal getJmlByrLl() {
		return this.jmlByrLl;
	}

	public void setJmlByrLl(BigDecimal jmlByrLl) {
		this.jmlByrLl = jmlByrLl;
	}

	public BigDecimal getJmlByrP3k() {
		return this.jmlByrP3k;
	}

	public void setJmlByrP3k(BigDecimal jmlByrP3k) {
		this.jmlByrP3k = jmlByrP3k;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getNoBpk() {
		return this.noBpk;
	}

	public void setNoBpk(String noBpk) {
		this.noBpk = noBpk;
	}

	public Date getTglPembuatanBpk() {
		return this.tglPembuatanBpk;
	}

	public void setTglPembuatanBpk(Date tglPembuatanBpk) {
		this.tglPembuatanBpk = tglPembuatanBpk;
	}

}