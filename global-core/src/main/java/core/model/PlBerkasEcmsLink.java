package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_BERKAS_ECMS_LINK database table.
 * 
 */
@Entity
@Table(name="PL_BERKAS_ECMS_LINK")
@NamedQuery(name="PlBerkasEcmsLink.findAll", query="SELECT p FROM PlBerkasEcmsLink p")
public class PlBerkasEcmsLink implements Serializable {
	private static final long serialVersionUID = 1L;

	private String ddocname;

	@Id
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="NO_REG_ECMS")
	private String noRegEcms;

	public PlBerkasEcmsLink() {
	}

	public String getDdocname() {
		return this.ddocname;
	}

	public void setDdocname(String ddocname) {
		this.ddocname = ddocname;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getNoRegEcms() {
		return this.noRegEcms;
	}

	public void setNoRegEcms(String noRegEcms) {
		this.noRegEcms = noRegEcms;
	}

}