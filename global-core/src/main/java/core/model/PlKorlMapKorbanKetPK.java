package core.model;

import java.io.Serializable;


public class PlKorlMapKorbanKetPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idJr;
	private String idKorlantas;
	public String getIdJr() {
		return idJr;
	}
	public void setIdJr(String idJr) {
		this.idJr = idJr;
	}
	public String getIdKorlantas() {
		return idKorlantas;
	}
	public void setIdKorlantas(String idKorlantas) {
		this.idKorlantas = idKorlantas;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idJr == null) ? 0 : idJr.hashCode());
		result = prime * result
				+ ((idKorlantas == null) ? 0 : idKorlantas.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlKorlMapKorbanKetPK other = (PlKorlMapKorbanKetPK) obj;
		if (idJr == null) {
			if (other.idJr != null)
				return false;
		} else if (!idJr.equals(other.idJr))
			return false;
		if (idKorlantas == null) {
			if (other.idKorlantas != null)
				return false;
		} else if (!idKorlantas.equals(other.idKorlantas))
			return false;
		return true;
	}
	
}
