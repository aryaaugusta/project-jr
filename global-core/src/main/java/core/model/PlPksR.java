package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_PKS_RS database table.
 * 
 */
@Entity
@Table(name="PL_PKS_RS")
@IdClass(PlPksRPK.class)
@NamedQuery(name="PlPksR.findAll", query="SELECT p FROM PlPksR p")
public class PlPksR implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_RS_PKS")
	private String idRsPks;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="FLAG_AKTIF")
	private String flagAktif;

	private String keterangan;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="KODE_RUMAHSAKIT")
	private String kodeRumahsakit;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="MASA_LAKU")
	private String masaLaku;

	@Column(name="NO_PKS_JR")
	private String noPksJr;

	@Column(name="NO_PKS_RS")
	private String noPksRs;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_LAKU_AKHIR")
	private Date tglLakuAkhir;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_PKS")
	private Date tglPks;

	public PlPksR() {
	}

	public String getIdRsPks() {
		return this.idRsPks;
	}

	public void setIdRsPks(String idRsPks) {
		this.idRsPks = idRsPks;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFlagAktif() {
		return this.flagAktif;
	}

	public void setFlagAktif(String flagAktif) {
		this.flagAktif = flagAktif;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeRumahsakit() {
		return this.kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getMasaLaku() {
		return this.masaLaku;
	}

	public void setMasaLaku(String masaLaku) {
		this.masaLaku = masaLaku;
	}

	public String getNoPksJr() {
		return this.noPksJr;
	}

	public void setNoPksJr(String noPksJr) {
		this.noPksJr = noPksJr;
	}

	public String getNoPksRs() {
		return this.noPksRs;
	}

	public void setNoPksRs(String noPksRs) {
		this.noPksRs = noPksRs;
	}

	public Date getTglLakuAkhir() {
		return this.tglLakuAkhir;
	}

	public void setTglLakuAkhir(Date tglLakuAkhir) {
		this.tglLakuAkhir = tglLakuAkhir;
	}

	public Date getTglPks() {
		return this.tglPks;
	}

	public void setTglPks(Date tglPks) {
		this.tglPks = tglPks;
	}

}