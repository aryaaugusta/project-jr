package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_VEHICLE_TYPES database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_VEHICLE_TYPES")
@NamedQuery(name="KorlantasVehicleType.findAll", query="SELECT k FROM KorlantasVehicleType k")
public class KorlantasVehicleType implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal archived;

	@Id
	private BigDecimal id;

	private BigDecimal mc;

	private String name;

	@Column(name="SORT_ORDER")
	private BigDecimal sortOrder;

	@Column(name="VT_TYPE")
	private BigDecimal vtType;

	public KorlantasVehicleType() {
	}

	public BigDecimal getArchived() {
		return this.archived;
	}

	public void setArchived(BigDecimal archived) {
		this.archived = archived;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getMc() {
		return this.mc;
	}

	public void setMc(BigDecimal mc) {
		this.mc = mc;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

	public BigDecimal getVtType() {
		return this.vtType;
	}

	public void setVtType(BigDecimal vtType) {
		this.vtType = vtType;
	}

}