package core.model;

import java.io.Serializable;


public class FndKantorVpnPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String hostClient;
	private String kodeKantorJr;
	public String getHostClient() {
		return hostClient;
	}
	public void setHostClient(String hostClient) {
		this.hostClient = hostClient;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((hostClient == null) ? 0 : hostClient.hashCode());
		result = prime * result
				+ ((kodeKantorJr == null) ? 0 : kodeKantorJr.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FndKantorVpnPK other = (FndKantorVpnPK) obj;
		if (hostClient == null) {
			if (other.hostClient != null)
				return false;
		} else if (!hostClient.equals(other.hostClient))
			return false;
		if (kodeKantorJr == null) {
			if (other.kodeKantorJr != null)
				return false;
		} else if (!kodeKantorJr.equals(other.kodeKantorJr))
			return false;
		return true;
	}
}
