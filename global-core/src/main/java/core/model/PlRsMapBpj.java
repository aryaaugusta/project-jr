package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the PL_RS_MAP_BPJS database table.
 * 
 */
@Entity
@Table(name="PL_RS_MAP_BPJS")
@IdClass(PlRsMapBpjPK.class)
@NamedQuery(name="PlRsMapBpj.findAll", query="SELECT p FROM PlRsMapBpj p")
public class PlRsMapBpj implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ALAMAT_RS_BPJS")
	private String alamatRsBpjs;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="FLAG_KODE_BPJS_DOBEL")
	private String flagKodeBpjsDobel;

	@Column(name="KAB_KOTA_RS_BPJS")
	private String kabKotaRsBpjs;

	@Id
	@Column(name="KODE_BPJS")
	private String kodeBpjs;

	@Column(name="KODE_INSTANSI")
	private String kodeInstansi;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="LOKET_PENANGGUNGJAWAB")
	private String loketPenanggungjawab;

	@Column(name="NAMA_RS_BPJS")
	private String namaRsBpjs;

	@Column(name="PROPINSI_RS_BPJS")
	private String propinsiRsBpjs;
	
	@Column(name="KODE_RUMAHSAKIT")
	private String kodeRumahsakit;

	//bi-directional many-to-one association to PlRumahSakit
//	@ManyToOne
//	@JoinColumn(name="KODE_RUMAHSAKIT")
//	private PlRumahSakit plRumahSakit;

	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

	public PlRsMapBpj() {
	}

	public String getAlamatRsBpjs() {
		return this.alamatRsBpjs;
	}

	public void setAlamatRsBpjs(String alamatRsBpjs) {
		this.alamatRsBpjs = alamatRsBpjs;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFlagKodeBpjsDobel() {
		return this.flagKodeBpjsDobel;
	}

	public void setFlagKodeBpjsDobel(String flagKodeBpjsDobel) {
		this.flagKodeBpjsDobel = flagKodeBpjsDobel;
	}

	public String getKabKotaRsBpjs() {
		return this.kabKotaRsBpjs;
	}

	public void setKabKotaRsBpjs(String kabKotaRsBpjs) {
		this.kabKotaRsBpjs = kabKotaRsBpjs;
	}

	public String getKodeBpjs() {
		return this.kodeBpjs;
	}

	public void setKodeBpjs(String kodeBpjs) {
		this.kodeBpjs = kodeBpjs;
	}

	public String getKodeInstansi() {
		return this.kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLoketPenanggungjawab() {
		return this.loketPenanggungjawab;
	}

	public void setLoketPenanggungjawab(String loketPenanggungjawab) {
		this.loketPenanggungjawab = loketPenanggungjawab;
	}

	public String getNamaRsBpjs() {
		return this.namaRsBpjs;
	}

	public void setNamaRsBpjs(String namaRsBpjs) {
		this.namaRsBpjs = namaRsBpjs;
	}

	public String getPropinsiRsBpjs() {
		return this.propinsiRsBpjs;
	}

	public void setPropinsiRsBpjs(String propinsiRsBpjs) {
		this.propinsiRsBpjs = propinsiRsBpjs;
	}

//	public PlRumahSakit getPlRumahSakit() {
//		return this.plRumahSakit;
//	}
//
//	public void setPlRumahSakit(PlRumahSakit plRumahSakit) {
//		this.plRumahSakit = plRumahSakit;
//	}

}