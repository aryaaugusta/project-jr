package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_DISPOSISI database table.
 * 
 */
@Entity
@Table(name="PL_DISPOSISI")
@NamedQuery(name="PlDisposisi.findAll", query="SELECT p FROM PlDisposisi p")
public class PlDisposisi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_DISPOSISI")
	private String idDisposisi;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private String dari;

	private String disposisi;

	@Column(name="ID_GUID")
	private String idGuid;

	@Column(name="JUMLAH_DISETUJUI")
	private BigDecimal jumlahDisetujui;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="LEVEL_CABANG_DISP")
	private String levelCabangDisp;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_DISPOSISI")
	private Date tglDisposisi;

	public PlDisposisi() {
	}

	public String getIdDisposisi() {
		return this.idDisposisi;
	}

	public void setIdDisposisi(String idDisposisi) {
		this.idDisposisi = idDisposisi;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDari() {
		return this.dari;
	}

	public void setDari(String dari) {
		this.dari = dari;
	}

	public String getDisposisi() {
		return this.disposisi;
	}

	public void setDisposisi(String disposisi) {
		this.disposisi = disposisi;
	}

	public String getIdGuid() {
		return this.idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public BigDecimal getJumlahDisetujui() {
		return this.jumlahDisetujui;
	}

	public void setJumlahDisetujui(BigDecimal jumlahDisetujui) {
		this.jumlahDisetujui = jumlahDisetujui;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLevelCabangDisp() {
		return this.levelCabangDisp;
	}

	public void setLevelCabangDisp(String levelCabangDisp) {
		this.levelCabangDisp = levelCabangDisp;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public Date getTglDisposisi() {
		return this.tglDisposisi;
	}

	public void setTglDisposisi(Date tglDisposisi) {
		this.tglDisposisi = tglDisposisi;
	}

}