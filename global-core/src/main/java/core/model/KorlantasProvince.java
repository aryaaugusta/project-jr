package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_PROVINCES database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_PROVINCES")
@NamedQuery(name="KorlantasProvince.findAll", query="SELECT k FROM KorlantasProvince k")
public class KorlantasProvince implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal archived;

	@Column(name="DASI_ID")
	private String dasiId;

	@Column(name="GPS_LATITUDE")
	private BigDecimal gpsLatitude;

	@Column(name="GPS_LONGITUDE")
	private BigDecimal gpsLongitude;

	@Id
	private BigDecimal id;

	private String name;

	@Column(name="PROVINCE_NUMBER")
	private String provinceNumber;

	@Column(name="SORT_ORDER")
	private BigDecimal sortOrder;

	public KorlantasProvince() {
	}

	public BigDecimal getArchived() {
		return this.archived;
	}

	public void setArchived(BigDecimal archived) {
		this.archived = archived;
	}

	public String getDasiId() {
		return this.dasiId;
	}

	public void setDasiId(String dasiId) {
		this.dasiId = dasiId;
	}

	public BigDecimal getGpsLatitude() {
		return this.gpsLatitude;
	}

	public void setGpsLatitude(BigDecimal gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}

	public BigDecimal getGpsLongitude() {
		return this.gpsLongitude;
	}

	public void setGpsLongitude(BigDecimal gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProvinceNumber() {
		return this.provinceNumber;
	}

	public void setProvinceNumber(String provinceNumber) {
		this.provinceNumber = provinceNumber;
	}

	public BigDecimal getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}