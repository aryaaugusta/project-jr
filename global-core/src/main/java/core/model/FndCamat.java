package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FND_CAMAT database table.
 * 
 */
@Entity
@Table(name="FND_CAMAT")
@NamedQuery(name="FndCamat.findAll", query="SELECT f FROM FndCamat f")
@IdClass(FndCamatPK.class)
public class FndCamat implements Serializable {
	private static final long serialVersionUID = 1L;

	private String catatan;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	@Id
	@Column(name="KODE_CAMAT")
	private String kodeCamat;

	@Id
	@Column(name="KODE_KABKOTA")
	private String kodeKabkota;

	@Id
	@Column(name="KODE_PROVINSI")
	private String kodeProvinsi;

	@Column(name="NAMA_CAMAT")
	private String namaCamat;

	@Column(name="NAMA_KABKOTA")
	private String namaKabkota;

	@Column(name="NAMA_PROVINSI")
	private String namaProvinsi;

	public FndCamat() {
	}

	public String getCatatan() {
		return this.catatan;
	}

	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getKodeCamat() {
		return this.kodeCamat;
	}

	public void setKodeCamat(String kodeCamat) {
		this.kodeCamat = kodeCamat;
	}

	public String getKodeKabkota() {
		return this.kodeKabkota;
	}

	public void setKodeKabkota(String kodeKabkota) {
		this.kodeKabkota = kodeKabkota;
	}

	public String getKodeProvinsi() {
		return this.kodeProvinsi;
	}

	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}

	public String getNamaCamat() {
		return this.namaCamat;
	}

	public void setNamaCamat(String namaCamat) {
		this.namaCamat = namaCamat;
	}

	public String getNamaKabkota() {
		return this.namaKabkota;
	}

	public void setNamaKabkota(String namaKabkota) {
		this.namaKabkota = namaKabkota;
	}

	public String getNamaProvinsi() {
		return this.namaProvinsi;
	}

	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}

}