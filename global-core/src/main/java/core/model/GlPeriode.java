package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the GL_PERIODE database table.
 * 
 */
@Entity
@Table(name="GL_PERIODE")
@NamedQuery(name="GlPeriode.findAll", query="SELECT g FROM GlPeriode g")
public class GlPeriode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NAMA_PERIODE")
	private String namaPeriode;

	private BigDecimal bulan;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="ID_PERIODE")
	private String idPeriode;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NO_URUT")
	private BigDecimal noUrut;

	@Column(name="STATUS_PERIODE")
	private String statusPeriode;

	@Column(name="STATUS_PERIODE_PENYESUAIAN")
	private String statusPeriodePenyesuaian;

	private BigDecimal tahun;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_BUKA")
	private Date tglBuka;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_TUTUP")
	private Date tglTutup;

	public GlPeriode() {
	}

	public String getNamaPeriode() {
		return this.namaPeriode;
	}

	public void setNamaPeriode(String namaPeriode) {
		this.namaPeriode = namaPeriode;
	}

	public BigDecimal getBulan() {
		return this.bulan;
	}

	public void setBulan(BigDecimal bulan) {
		this.bulan = bulan;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdPeriode() {
		return this.idPeriode;
	}

	public void setIdPeriode(String idPeriode) {
		this.idPeriode = idPeriode;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public BigDecimal getNoUrut() {
		return this.noUrut;
	}

	public void setNoUrut(BigDecimal noUrut) {
		this.noUrut = noUrut;
	}

	public String getStatusPeriode() {
		return this.statusPeriode;
	}

	public void setStatusPeriode(String statusPeriode) {
		this.statusPeriode = statusPeriode;
	}

	public String getStatusPeriodePenyesuaian() {
		return this.statusPeriodePenyesuaian;
	}

	public void setStatusPeriodePenyesuaian(String statusPeriodePenyesuaian) {
		this.statusPeriodePenyesuaian = statusPeriodePenyesuaian;
	}

	public BigDecimal getTahun() {
		return this.tahun;
	}

	public void setTahun(BigDecimal tahun) {
		this.tahun = tahun;
	}

	public Date getTglBuka() {
		return this.tglBuka;
	}

	public void setTglBuka(Date tglBuka) {
		this.tglBuka = tglBuka;
	}

	public Date getTglTutup() {
		return this.tglTutup;
	}

	public void setTglTutup(Date tglTutup) {
		this.tglTutup = tglTutup;
	}

}