package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the PL_REGISTER_BERKAS database table.
 * 
 */
@Entity
@Table(name="PL_REGISTER_BERKAS")
@NamedQuery(name="PlRegisterBerka.findAll", query="SELECT p FROM PlRegisterBerka p")
@IdClass(PlRegisterBerkaPK.class)
public class PlRegisterBerka implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@Column(name="NO_REGISTER")
	private String noRegister;

	@Id
	@Column(name="KODE_BERKAS")
	private String kodeBerkas;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private String filepath;

	@Lob
	@Column(name="IMAGE_BLOB")
	private byte[] imageBlob;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_TERIMA_BERKAS")
	private Date tglTerimaBerkas;

	public PlRegisterBerka() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFilepath() {
		return this.filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public byte[] getImageBlob() {
		return this.imageBlob;
	}

	public void setImageBlob(byte[] imageBlob) {
		this.imageBlob = imageBlob;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Date getTglTerimaBerkas() {
		return this.tglTerimaBerkas;
	}

	public void setTglTerimaBerkas(Date tglTerimaBerkas) {
		this.tglTerimaBerkas = tglTerimaBerkas;
	}

	public String getNoRegister() {
		return noRegister;
	}

	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}

	public String getKodeBerkas() {
		return kodeBerkas;
	}

	public void setKodeBerkas(String kodeBerkas) {
		this.kodeBerkas = kodeBerkas;
	}

}