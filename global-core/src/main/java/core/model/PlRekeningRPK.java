package core.model;

import java.io.Serializable;

public class PlRekeningRPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idRsRek;

	public String getIdRsRek() {
		return idRsRek;
	}

	public void setIdRsRek(String idRsRek) {
		this.idRsRek = idRsRek;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idRsRek == null) ? 0 : idRsRek.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlRekeningRPK other = (PlRekeningRPK) obj;
		if (idRsRek == null) {
			if (other.idRsRek != null)
				return false;
		} else if (!idRsRek.equals(other.idRsRek))
			return false;
		return true;
	}

}
