package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the AUTH_SESSION_ALERT database table.
 * 
 */
@Entity
@Table(name="AUTH_SESSION_ALERT")
@NamedQuery(name="AuthSessionAlert.findAll", query="SELECT a FROM AuthSessionAlert a")
@IdClass(AuthSessionAlertPK.class)
public class AuthSessionAlert implements Serializable {
	private static final long serialVersionUID = 1L;

	private String deskripsi;

	private String kode;

	@Column(name="URL_LINK")
	private String urlLink;

	@Column(name="USR_ALERT")
	private String usrAlert;

	@Id
	@Column(name="USR_LOGIN")
	private String usrLogin;

	@Id
	@Column(name="USR_SESSION")
	private String usrSession;

	public AuthSessionAlert() {
	}

	public String getDeskripsi() {
		return this.deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getKode() {
		return this.kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getUrlLink() {
		return this.urlLink;
	}

	public void setUrlLink(String urlLink) {
		this.urlLink = urlLink;
	}

	public String getUsrAlert() {
		return this.usrAlert;
	}

	public void setUsrAlert(String usrAlert) {
		this.usrAlert = usrAlert;
	}

	public String getUsrLogin() {
		return this.usrLogin;
	}

	public void setUsrLogin(String usrLogin) {
		this.usrLogin = usrLogin;
	}

	public String getUsrSession() {
		return this.usrSession;
	}

	public void setUsrSession(String usrSession) {
		this.usrSession = usrSession;
	}

}