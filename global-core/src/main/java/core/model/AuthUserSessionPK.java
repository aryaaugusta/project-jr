package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the AUTH_USER_SESSION database table.
 * 
 */
//@Embeddable
public class AuthUserSessionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String login;

//	@Column(name="SESSION_ID")
	private String sessionId;

//	@Temporal(TemporalType.DATE)
//	@Column(name="ONLINE_SINCE")
	private java.util.Date onlineSince;

	public AuthUserSessionPK() {
	}
	public String getLogin() {
		return this.login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSessionId() {
		return this.sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public java.util.Date getOnlineSince() {
		return this.onlineSince;
	}
	public void setOnlineSince(java.util.Date onlineSince) {
		this.onlineSince = onlineSince;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AuthUserSessionPK)) {
			return false;
		}
		AuthUserSessionPK castOther = (AuthUserSessionPK)other;
		return 
			this.login.equals(castOther.login)
			&& this.sessionId.equals(castOther.sessionId)
			&& this.onlineSince.equals(castOther.onlineSince);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.login.hashCode();
		hash = hash * prime + this.sessionId.hashCode();
		hash = hash * prime + this.onlineSince.hashCode();
		
		return hash;
	}
}