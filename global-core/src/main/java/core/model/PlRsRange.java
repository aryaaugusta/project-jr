package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_RS_RANGE database table.
 * 
 */
@Entity
@Table(name="PL_RS_RANGE")
@NamedQuery(name="PlRsRange.findAll", query="SELECT p FROM PlRsRange p")
public class PlRsRange implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Id
	@Column(name="KODE_RUMAHSAKIT")
	private String kodeRumahsakit;

	public PlRsRange() {
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeRumahsakit() {
		return this.kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

}