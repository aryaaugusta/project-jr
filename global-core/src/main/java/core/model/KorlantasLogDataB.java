package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the KORLANTAS_LOG_DATA_B database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_LOG_DATA_B")
@NamedQuery(name="KorlantasLogDataB.findAll", query="SELECT k FROM KorlantasLogDataB k")
public class KorlantasLogDataB implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="ID_DT")
	private String idDt;

	@Column(name="PARAMS_METHOD")
	private String paramsMethod;

	@Column(name="PARAMS_TABLE")
	private String paramsTable;

	@Column(name="PARAMS_VALUES")
	private String paramsValues;

	@Temporal(TemporalType.DATE)
	@Column(name="PROCESS_DATE")
	private Date processDate;

	public KorlantasLogDataB() {
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdDt() {
		return this.idDt;
	}

	public void setIdDt(String idDt) {
		this.idDt = idDt;
	}

	public String getParamsMethod() {
		return this.paramsMethod;
	}

	public void setParamsMethod(String paramsMethod) {
		this.paramsMethod = paramsMethod;
	}

	public String getParamsTable() {
		return this.paramsTable;
	}

	public void setParamsTable(String paramsTable) {
		this.paramsTable = paramsTable;
	}

	public String getParamsValues() {
		return this.paramsValues;
	}

	public void setParamsValues(String paramsValues) {
		this.paramsValues = paramsValues;
	}

	public Date getProcessDate() {
		return this.processDate;
	}

	public void setProcessDate(Date processDate) {
		this.processDate = processDate;
	}

}