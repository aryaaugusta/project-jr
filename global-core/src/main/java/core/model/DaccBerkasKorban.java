package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DACC_BERKAS_KORBAN database table.
 * 
 */
@Entity
@Table(name="DACC_BERKAS_KORBAN")
@NamedQuery(name="DaccBerkasKorban.findAll", query="SELECT d FROM DaccBerkasKorban d")
public class DaccBerkasKorban implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="ABSAH_FLAG")
	private String absahFlag;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="LAST_UDATED_BY")
	private String lastUdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NO_MANIFEST")
	private String noManifest;

	@Column(name="POS_ID")
	private String posId;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_TERIMA_BERKAS")
	private Date tglTerimaBerkas;

	public DaccBerkasKorban() {
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getAbsahFlag() {
		return this.absahFlag;
	}

	public void setAbsahFlag(String absahFlag) {
		this.absahFlag = absahFlag;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getLastUdatedBy() {
		return this.lastUdatedBy;
	}

	public void setLastUdatedBy(String lastUdatedBy) {
		this.lastUdatedBy = lastUdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNoManifest() {
		return this.noManifest;
	}

	public void setNoManifest(String noManifest) {
		this.noManifest = noManifest;
	}

	public String getPosId() {
		return this.posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	public Date getTglTerimaBerkas() {
		return this.tglTerimaBerkas;
	}

	public void setTglTerimaBerkas(Date tglTerimaBerkas) {
		this.tglTerimaBerkas = tglTerimaBerkas;
	}

}