package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_RANKS database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_RANKS")
@NamedQuery(name="KorlantasRank.findAll", query="SELECT k FROM KorlantasRank k")
public class KorlantasRank implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal archieved;

	@Id
	private BigDecimal id;

	private String name;

	@Column(name="SORT_ORDER")
	private BigDecimal sortOrder;

	public KorlantasRank() {
	}

	public BigDecimal getArchieved() {
		return this.archieved;
	}

	public void setArchieved(BigDecimal archieved) {
		this.archieved = archieved;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}