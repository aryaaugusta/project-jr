package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the DUKCAPIL_REF_CODES database table.
 * 
 */
@Entity
@Table(name="DUKCAPIL_REF_CODES")
@IdClass(DukcapilRefCodePK.class)
@NamedQuery(name="DukcapilRefCode.findAll", query="SELECT d FROM DukcapilRefCode d")
public class DukcapilRefCode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_SYNC_DATE")
	private Date lastSyncDate;

	@Id
	@Column(name="RF_DOMAIN")
	private String rfDomain;

	@Id
	@Column(name="RF_HIGH_VALUE")
	private String rfHighValue;

	@Id
	@Column(name="RF_LOW_VALUE")
	private String rfLowValue;

	@Id
	@Column(name="RV_MEANING")
	private String rvMeaning;

	public DukcapilRefCode() {
	}

	public Date getLastSyncDate() {
		return this.lastSyncDate;
	}

	public void setLastSyncDate(Date lastSyncDate) {
		this.lastSyncDate = lastSyncDate;
	}

	public String getRfDomain() {
		return this.rfDomain;
	}

	public void setRfDomain(String rfDomain) {
		this.rfDomain = rfDomain;
	}

	public String getRfHighValue() {
		return this.rfHighValue;
	}

	public void setRfHighValue(String rfHighValue) {
		this.rfHighValue = rfHighValue;
	}

	public String getRfLowValue() {
		return this.rfLowValue;
	}

	public void setRfLowValue(String rfLowValue) {
		this.rfLowValue = rfLowValue;
	}

	public String getRvMeaning() {
		return this.rvMeaning;
	}

	public void setRvMeaning(String rvMeaning) {
		this.rvMeaning = rvMeaning;
	}

}