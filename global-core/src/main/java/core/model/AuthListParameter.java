package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the AUTH_LIST_PARAMETER database table.
 * 
 */
@Entity
@Table(name="AUTH_LIST_PARAMETER")
@NamedQuery(name="AuthListParameter.findAll", query="SELECT a FROM AuthListParameter a")
public class AuthListParameter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PARAMETER")
	private String idParameter;

	@Column(name="ALIAS_PARAMETER")
	private String aliasParameter;

	@Column(name="COL_WHERE")
	private String colWhere;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	@Column(name="FLAG_WHERE")
	private String flagWhere;

	@Column(name="FORMAT_TGL")
	private String formatTgl;

	@Column(name="ISI_PARAMETER")
	private String isiParameter;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NAMA_PARAMETER")
	private String namaParameter;

	@Column(name="PL_STAT")
	private String plStat;

	@Column(name="STAT_WHERE")
	private String statWhere;

	@Column(name="SUMBER_PARAMETER")
	private String sumberParameter;

	@Column(name="TIPE_PARAMETER")
	private String tipeParameter;

	public AuthListParameter() {
	}

	public String getIdParameter() {
		return this.idParameter;
	}

	public void setIdParameter(String idParameter) {
		this.idParameter = idParameter;
	}

	public String getAliasParameter() {
		return this.aliasParameter;
	}

	public void setAliasParameter(String aliasParameter) {
		this.aliasParameter = aliasParameter;
	}

	public String getColWhere() {
		return this.colWhere;
	}

	public void setColWhere(String colWhere) {
		this.colWhere = colWhere;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getFlagWhere() {
		return this.flagWhere;
	}

	public void setFlagWhere(String flagWhere) {
		this.flagWhere = flagWhere;
	}

	public String getFormatTgl() {
		return this.formatTgl;
	}

	public void setFormatTgl(String formatTgl) {
		this.formatTgl = formatTgl;
	}

	public String getIsiParameter() {
		return this.isiParameter;
	}

	public void setIsiParameter(String isiParameter) {
		this.isiParameter = isiParameter;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamaParameter() {
		return this.namaParameter;
	}

	public void setNamaParameter(String namaParameter) {
		this.namaParameter = namaParameter;
	}

	public String getPlStat() {
		return this.plStat;
	}

	public void setPlStat(String plStat) {
		this.plStat = plStat;
	}

	public String getStatWhere() {
		return this.statWhere;
	}

	public void setStatWhere(String statWhere) {
		this.statWhere = statWhere;
	}

	public String getSumberParameter() {
		return this.sumberParameter;
	}

	public void setSumberParameter(String sumberParameter) {
		this.sumberParameter = sumberParameter;
	}

	public String getTipeParameter() {
		return this.tipeParameter;
	}

	public void setTipeParameter(String tipeParameter) {
		this.tipeParameter = tipeParameter;
	}

}