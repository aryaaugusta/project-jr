package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the KORLANTAS_RE_ACCIDENTS database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_RE_ACCIDENTS")
@NamedQuery(name="KorlantasReAccident.findAll", query="SELECT k FROM KorlantasReAccident k")
public class KorlantasReAccident implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="ACC_NO_N")
	private String accNoN;

	@Column(name="ACCIDENT_DATE")
	private Timestamp accidentDate;

	@Column(name="ACCIDENT_DIAGRAM_ID")
	private String accidentDiagramId;

	@Column(name="ACCIDENT_NO")
	private String accidentNo;

	@Column(name="ACCIDENT_SYNC_NUM")
	private String accidentSyncNum;

	@Column(name="CLOSED_ID")
	private BigDecimal closedId;

	@Column(name="CREATED_AT")
	private Timestamp createdAt;

	@Column(name="DEV_SERIAL")
	private BigDecimal devSerial;

	@Column(name="DIAGRAM_DIR")
	private BigDecimal diagramDir;

	@Column(name="DIRECTION_TO_ACCIDENT")
	private String directionToAccident;

	@Column(name="DISTANCE_TO_ACCIDENT")
	private String distanceToAccident;

	@Column(name="DISTRICT_ID")
	private BigDecimal districtId;

	@Column(name="EST_COST_MATERIAL_DAMAGE")
	private BigDecimal estCostMaterialDamage;

	@Column(name="EST_COST_VEHICLES_DAMAGE")
	private BigDecimal estCostVehiclesDamage;

	@Column(name="EXTRA_ORD")
	private BigDecimal extraOrd;

	@Column(name="FINISHED_DATE")
	private Timestamp finishedDate;

	@Column(name="GPS_LATITUDE")
	private BigDecimal gpsLatitude;

	@Column(name="GPS_LATITUDE_CORRECTED")
	private BigDecimal gpsLatitudeCorrected;

	@Column(name="GPS_LONGITUDE")
	private BigDecimal gpsLongitude;

	@Column(name="GPS_LONGITUDE_CORRECTED")
	private BigDecimal gpsLongitudeCorrected;

	@Column(name="HEAVY_INJURED_PERSONS_NUMBER")
	private BigDecimal heavyInjuredPersonsNumber;

	@Column(name="HIT_AND_RUN")
	private BigDecimal hitAndRun;

	@Column(name="INJURED_PERSONS_NUMBER")
	private BigDecimal injuredPersonsNumber;

	@Column(name="KILLED_PERSONS_NUMBER")
	private BigDecimal killedPersonsNumber;

	@Column(name="LIGHT_ID")
	private BigDecimal lightId;

	@Column(name="LP_DATE")
	private Timestamp lpDate;

	@Column(name="MOBILE_VERSION")
	private String mobileVersion;

	@Column(name="OFFICER_ID")
	private BigDecimal officerId;

	private BigDecimal patrol;

	@Column(name="POLICE_DETAILS")
	private String policeDetails;

	@Column(name="REFERENCE_DESC")
	private String referenceDesc;

	@Column(name="REFERENCE_TYPE_ID")
	private BigDecimal referenceTypeId;

	@Column(name="REPORTING_OFFICER_ID")
	private BigDecimal reportingOfficerId;

	@Column(name="ROAD_CHARACTER_ID")
	private BigDecimal roadCharacterId;

	@Column(name="ROAD_CLASS_ID")
	private BigDecimal roadClassId;

	@Column(name="ROAD_FUNCTION_ID")
	private BigDecimal roadFunctionId;

	@Column(name="ROAD_GRADIENT_ID")
	private BigDecimal roadGradientId;

	@Column(name="ROAD_KM")
	private BigDecimal roadKm;

	@Column(name="ROAD_NAME")
	private String roadName;

	@Column(name="ROAD_NUMBER")
	private String roadNumber;

	@Column(name="ROAD_STATE_ID")
	private BigDecimal roadStateId;

	@Column(name="ROAD_TYPE_ID")
	private BigDecimal roadTypeId;

	@Column(name="RTA_NO")
	private BigDecimal rtaNo;

	@Column(name="SEVERITY_ID")
	private BigDecimal severityId;

	@Column(name="SPEED_ID")
	private BigDecimal speedId;

	@Column(name="\"STATE\"")
	private BigDecimal state;

	@Column(name="SURFACE_CONDITION_ID")
	private BigDecimal surfaceConditionId;

	private BigDecimal valid;

	@Column(name="VALID_DATE")
	private Timestamp validDate;

	@Column(name="WEATHER_TYPE_ID")
	private BigDecimal weatherTypeId;

	public KorlantasReAccident() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAccNoN() {
		return this.accNoN;
	}

	public void setAccNoN(String accNoN) {
		this.accNoN = accNoN;
	}

	public Timestamp getAccidentDate() {
		return this.accidentDate;
	}

	public void setAccidentDate(Timestamp accidentDate) {
		this.accidentDate = accidentDate;
	}

	public String getAccidentDiagramId() {
		return this.accidentDiagramId;
	}

	public void setAccidentDiagramId(String accidentDiagramId) {
		this.accidentDiagramId = accidentDiagramId;
	}

	public String getAccidentNo() {
		return this.accidentNo;
	}

	public void setAccidentNo(String accidentNo) {
		this.accidentNo = accidentNo;
	}

	public String getAccidentSyncNum() {
		return this.accidentSyncNum;
	}

	public void setAccidentSyncNum(String accidentSyncNum) {
		this.accidentSyncNum = accidentSyncNum;
	}

	public BigDecimal getClosedId() {
		return this.closedId;
	}

	public void setClosedId(BigDecimal closedId) {
		this.closedId = closedId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getDevSerial() {
		return this.devSerial;
	}

	public void setDevSerial(BigDecimal devSerial) {
		this.devSerial = devSerial;
	}

	public BigDecimal getDiagramDir() {
		return this.diagramDir;
	}

	public void setDiagramDir(BigDecimal diagramDir) {
		this.diagramDir = diagramDir;
	}

	public String getDirectionToAccident() {
		return this.directionToAccident;
	}

	public void setDirectionToAccident(String directionToAccident) {
		this.directionToAccident = directionToAccident;
	}

	public String getDistanceToAccident() {
		return this.distanceToAccident;
	}

	public void setDistanceToAccident(String distanceToAccident) {
		this.distanceToAccident = distanceToAccident;
	}

	public BigDecimal getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(BigDecimal districtId) {
		this.districtId = districtId;
	}

	public BigDecimal getEstCostMaterialDamage() {
		return this.estCostMaterialDamage;
	}

	public void setEstCostMaterialDamage(BigDecimal estCostMaterialDamage) {
		this.estCostMaterialDamage = estCostMaterialDamage;
	}

	public BigDecimal getEstCostVehiclesDamage() {
		return this.estCostVehiclesDamage;
	}

	public void setEstCostVehiclesDamage(BigDecimal estCostVehiclesDamage) {
		this.estCostVehiclesDamage = estCostVehiclesDamage;
	}

	public BigDecimal getExtraOrd() {
		return this.extraOrd;
	}

	public void setExtraOrd(BigDecimal extraOrd) {
		this.extraOrd = extraOrd;
	}

	public Timestamp getFinishedDate() {
		return this.finishedDate;
	}

	public void setFinishedDate(Timestamp finishedDate) {
		this.finishedDate = finishedDate;
	}

	public BigDecimal getGpsLatitude() {
		return this.gpsLatitude;
	}

	public void setGpsLatitude(BigDecimal gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}

	public BigDecimal getGpsLatitudeCorrected() {
		return this.gpsLatitudeCorrected;
	}

	public void setGpsLatitudeCorrected(BigDecimal gpsLatitudeCorrected) {
		this.gpsLatitudeCorrected = gpsLatitudeCorrected;
	}

	public BigDecimal getGpsLongitude() {
		return this.gpsLongitude;
	}

	public void setGpsLongitude(BigDecimal gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}

	public BigDecimal getGpsLongitudeCorrected() {
		return this.gpsLongitudeCorrected;
	}

	public void setGpsLongitudeCorrected(BigDecimal gpsLongitudeCorrected) {
		this.gpsLongitudeCorrected = gpsLongitudeCorrected;
	}

	public BigDecimal getHeavyInjuredPersonsNumber() {
		return this.heavyInjuredPersonsNumber;
	}

	public void setHeavyInjuredPersonsNumber(BigDecimal heavyInjuredPersonsNumber) {
		this.heavyInjuredPersonsNumber = heavyInjuredPersonsNumber;
	}

	public BigDecimal getHitAndRun() {
		return this.hitAndRun;
	}

	public void setHitAndRun(BigDecimal hitAndRun) {
		this.hitAndRun = hitAndRun;
	}

	public BigDecimal getInjuredPersonsNumber() {
		return this.injuredPersonsNumber;
	}

	public void setInjuredPersonsNumber(BigDecimal injuredPersonsNumber) {
		this.injuredPersonsNumber = injuredPersonsNumber;
	}

	public BigDecimal getKilledPersonsNumber() {
		return this.killedPersonsNumber;
	}

	public void setKilledPersonsNumber(BigDecimal killedPersonsNumber) {
		this.killedPersonsNumber = killedPersonsNumber;
	}

	public BigDecimal getLightId() {
		return this.lightId;
	}

	public void setLightId(BigDecimal lightId) {
		this.lightId = lightId;
	}

	public Timestamp getLpDate() {
		return this.lpDate;
	}

	public void setLpDate(Timestamp lpDate) {
		this.lpDate = lpDate;
	}

	public String getMobileVersion() {
		return this.mobileVersion;
	}

	public void setMobileVersion(String mobileVersion) {
		this.mobileVersion = mobileVersion;
	}

	public BigDecimal getOfficerId() {
		return this.officerId;
	}

	public void setOfficerId(BigDecimal officerId) {
		this.officerId = officerId;
	}

	public BigDecimal getPatrol() {
		return this.patrol;
	}

	public void setPatrol(BigDecimal patrol) {
		this.patrol = patrol;
	}

	public String getPoliceDetails() {
		return this.policeDetails;
	}

	public void setPoliceDetails(String policeDetails) {
		this.policeDetails = policeDetails;
	}

	public String getReferenceDesc() {
		return this.referenceDesc;
	}

	public void setReferenceDesc(String referenceDesc) {
		this.referenceDesc = referenceDesc;
	}

	public BigDecimal getReferenceTypeId() {
		return this.referenceTypeId;
	}

	public void setReferenceTypeId(BigDecimal referenceTypeId) {
		this.referenceTypeId = referenceTypeId;
	}

	public BigDecimal getReportingOfficerId() {
		return this.reportingOfficerId;
	}

	public void setReportingOfficerId(BigDecimal reportingOfficerId) {
		this.reportingOfficerId = reportingOfficerId;
	}

	public BigDecimal getRoadCharacterId() {
		return this.roadCharacterId;
	}

	public void setRoadCharacterId(BigDecimal roadCharacterId) {
		this.roadCharacterId = roadCharacterId;
	}

	public BigDecimal getRoadClassId() {
		return this.roadClassId;
	}

	public void setRoadClassId(BigDecimal roadClassId) {
		this.roadClassId = roadClassId;
	}

	public BigDecimal getRoadFunctionId() {
		return this.roadFunctionId;
	}

	public void setRoadFunctionId(BigDecimal roadFunctionId) {
		this.roadFunctionId = roadFunctionId;
	}

	public BigDecimal getRoadGradientId() {
		return this.roadGradientId;
	}

	public void setRoadGradientId(BigDecimal roadGradientId) {
		this.roadGradientId = roadGradientId;
	}

	public BigDecimal getRoadKm() {
		return this.roadKm;
	}

	public void setRoadKm(BigDecimal roadKm) {
		this.roadKm = roadKm;
	}

	public String getRoadName() {
		return this.roadName;
	}

	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}

	public String getRoadNumber() {
		return this.roadNumber;
	}

	public void setRoadNumber(String roadNumber) {
		this.roadNumber = roadNumber;
	}

	public BigDecimal getRoadStateId() {
		return this.roadStateId;
	}

	public void setRoadStateId(BigDecimal roadStateId) {
		this.roadStateId = roadStateId;
	}

	public BigDecimal getRoadTypeId() {
		return this.roadTypeId;
	}

	public void setRoadTypeId(BigDecimal roadTypeId) {
		this.roadTypeId = roadTypeId;
	}

	public BigDecimal getRtaNo() {
		return this.rtaNo;
	}

	public void setRtaNo(BigDecimal rtaNo) {
		this.rtaNo = rtaNo;
	}

	public BigDecimal getSeverityId() {
		return this.severityId;
	}

	public void setSeverityId(BigDecimal severityId) {
		this.severityId = severityId;
	}

	public BigDecimal getSpeedId() {
		return this.speedId;
	}

	public void setSpeedId(BigDecimal speedId) {
		this.speedId = speedId;
	}

	public BigDecimal getState() {
		return this.state;
	}

	public void setState(BigDecimal state) {
		this.state = state;
	}

	public BigDecimal getSurfaceConditionId() {
		return this.surfaceConditionId;
	}

	public void setSurfaceConditionId(BigDecimal surfaceConditionId) {
		this.surfaceConditionId = surfaceConditionId;
	}

	public BigDecimal getValid() {
		return this.valid;
	}

	public void setValid(BigDecimal valid) {
		this.valid = valid;
	}

	public Timestamp getValidDate() {
		return this.validDate;
	}

	public void setValidDate(Timestamp validDate) {
		this.validDate = validDate;
	}

	public BigDecimal getWeatherTypeId() {
		return this.weatherTypeId;
	}

	public void setWeatherTypeId(BigDecimal weatherTypeId) {
		this.weatherTypeId = weatherTypeId;
	}

}