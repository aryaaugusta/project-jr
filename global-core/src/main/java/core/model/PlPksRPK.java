package core.model;

import java.io.Serializable;

public class PlPksRPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idRsPks;

	public String getIdRsPks() {
		return idRsPks;
	}

	public void setIdRsPks(String idRsPks) {
		this.idRsPks = idRsPks;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idRsPks == null) ? 0 : idRsPks.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlPksRPK other = (PlPksRPK) obj;
		if (idRsPks == null) {
			if (other.idRsPks != null)
				return false;
		} else if (!idRsPks.equals(other.idRsPks))
			return false;
		return true;
	}

}
