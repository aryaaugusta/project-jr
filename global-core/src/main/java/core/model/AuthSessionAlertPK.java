package core.model;

import java.io.Serializable;


public class AuthSessionAlertPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String usrLogin;
	private String usrSession;
	public String getUsrLogin() {
		return usrLogin;
	}
	public void setUsrLogin(String usrLogin) {
		this.usrLogin = usrLogin;
	}
	public String getUsrSession() {
		return usrSession;
	}
	public void setUsrSession(String usrSession) {
		this.usrSession = usrSession;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((usrLogin == null) ? 0 : usrLogin.hashCode());
		result = prime * result
				+ ((usrSession == null) ? 0 : usrSession.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthSessionAlertPK other = (AuthSessionAlertPK) obj;
		if (usrLogin == null) {
			if (other.usrLogin != null)
				return false;
		} else if (!usrLogin.equals(other.usrLogin))
			return false;
		if (usrSession == null) {
			if (other.usrSession != null)
				return false;
		} else if (!usrSession.equals(other.usrSession))
			return false;
		return true;
	}
}
