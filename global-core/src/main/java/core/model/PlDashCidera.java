package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_DASH_CIDERA database table.
 * 
 */
@Entity
@Table(name="PL_DASH_CIDERA")
@NamedQuery(name="PlDashCidera.findAll", query="SELECT p FROM PlDashCidera p")
public class PlDashCidera implements Serializable {
	private static final long serialVersionUID = 1L;

	private String cidera;

	@Column(name="JML_KRB")
	private BigDecimal jmlKrb;

	@Column(name="JML_SANTUNAN")
	private BigDecimal jmlSantunan;

	@Id
	@Column(name="KODE_KANTOR")
	private String kodeKantor;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_PENGAJUAN")
	private Date tglPengajuan;

	public PlDashCidera() {
	}

	public String getCidera() {
		return this.cidera;
	}

	public void setCidera(String cidera) {
		this.cidera = cidera;
	}

	public BigDecimal getJmlKrb() {
		return this.jmlKrb;
	}

	public void setJmlKrb(BigDecimal jmlKrb) {
		this.jmlKrb = jmlKrb;
	}

	public BigDecimal getJmlSantunan() {
		return this.jmlSantunan;
	}

	public void setJmlSantunan(BigDecimal jmlSantunan) {
		this.jmlSantunan = jmlSantunan;
	}

	public String getKodeKantor() {
		return this.kodeKantor;
	}

	public void setKodeKantor(String kodeKantor) {
		this.kodeKantor = kodeKantor;
	}

	public Date getTglPengajuan() {
		return this.tglPengajuan;
	}

	public void setTglPengajuan(Date tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}

}