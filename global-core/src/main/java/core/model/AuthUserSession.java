package core.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the AUTH_USER_SESSION database table.
 * 
 */
@Entity
@Table(name="AUTH_USER_SESSION")
@NamedQuery(name="AuthUserSession.findAll", query="SELECT a FROM AuthUserSession a")
@IdClass(AuthUserSessionPK.class)
public class AuthUserSession implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String login;
	
	@Id
	@Column(name="SESSION_ID")
	private String sessionId;
	
	@Id
	@Temporal(TemporalType.DATE)
	@Column(name="ONLINE_SINCE")
	private java.util.Date onlineSince;

	private String description;

	@Column(name="IP_ADDRESS")
	private String ipAddress;

	public AuthUserSession() {
	}

//	public AuthUserSessionPK getId() {
//		return this.id;
//	}
//
//	public void setId(AuthUserSessionPK id) {
//		this.id = id;
//	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public java.util.Date getOnlineSince() {
		return onlineSince;
	}

	public void setOnlineSince(java.util.Date onlineSince) {
		this.onlineSince = onlineSince;
	}

}