package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the PL_REKENING_RS database table.
 * 
 */
@Entity
@Table(name="PL_REKENING_RS")
@IdClass(PlRekeningRPK.class)
@NamedQuery(name="PlRekeningR.findAll", query="SELECT p FROM PlRekeningR p")
public class PlRekeningR implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_RS_REK")
	private String idRsRek;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	@Column(name="KODE_BANK")
	private String kodeBank;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NAMA_PEMILIK")
	private String namaPemilik;

	@Column(name="NO_REKENING")
	private String noRekening;
	
	@Column(name="KODE_RUMAHSAKIT")
	private String kodeRumahsakit;

	//bi-directional many-to-one association to PlRumahSakit
//	@ManyToOne
//	@JoinColumn(name="KODE_RUMAHSAKIT")
//	private PlRumahSakit plRumahSakit;

	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

	public PlRekeningR() {
	}

	public String getIdRsRek() {
		return this.idRsRek;
	}

	public void setIdRsRek(String idRsRek) {
		this.idRsRek = idRsRek;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getKodeBank() {
		return this.kodeBank;
	}

	public void setKodeBank(String kodeBank) {
		this.kodeBank = kodeBank;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamaPemilik() {
		return this.namaPemilik;
	}

	public void setNamaPemilik(String namaPemilik) {
		this.namaPemilik = namaPemilik;
	}

	public String getNoRekening() {
		return this.noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

//	public PlRumahSakit getPlRumahSakit() {
//		return this.plRumahSakit;
//	}
//
//	public void setPlRumahSakit(PlRumahSakit plRumahSakit) {
//		this.plRumahSakit = plRumahSakit;
//	}

}