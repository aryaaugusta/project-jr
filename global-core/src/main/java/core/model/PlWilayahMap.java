package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_WILAYAH_MAP database table.
 * 
 */
@Entity
@Table(name="PL_WILAYAH_MAP")
@NamedQuery(name="PlWilayahMap.findAll", query="SELECT p FROM PlWilayahMap p")
public class PlWilayahMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="KODE_LOKASI")
	private String kodeLokasi;

	@Column(name="MAPPING_ID")
	private String mappingId;

	public PlWilayahMap() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getKodeLokasi() {
		return this.kodeLokasi;
	}

	public void setKodeLokasi(String kodeLokasi) {
		this.kodeLokasi = kodeLokasi;
	}

	public String getMappingId() {
		return this.mappingId;
	}

	public void setMappingId(String mappingId) {
		this.mappingId = mappingId;
	}

}