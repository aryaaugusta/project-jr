package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_VEHICLE_PRIOR_DAM_MS database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_VEHICLE_PRIOR_DAM_MS")
@NamedQuery(name="KorlantasVehiclePriorDamM.findAll", query="SELECT k FROM KorlantasVehiclePriorDamM k")
public class KorlantasVehiclePriorDamM implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="PRIOR_DAMAGE_ID")
	private BigDecimal priorDamageId;

	@Column(name="VEHICLE_ID")
	private BigDecimal vehicleId;

	public KorlantasVehiclePriorDamM() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getPriorDamageId() {
		return this.priorDamageId;
	}

	public void setPriorDamageId(BigDecimal priorDamageId) {
		this.priorDamageId = priorDamageId;
	}

	public BigDecimal getVehicleId() {
		return this.vehicleId;
	}

	public void setVehicleId(BigDecimal vehicleId) {
		this.vehicleId = vehicleId;
	}

}