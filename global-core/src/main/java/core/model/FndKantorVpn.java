package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FND_KANTOR_VPN database table.
 * 
 */
@Entity
@Table(name="FND_KANTOR_VPN")
@NamedQuery(name="FndKantorVpn.findAll", query="SELECT f FROM FndKantorVpn f")
@IdClass(FndKantorVpnPK.class)
public class FndKantorVpn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="HOST_CLIENT")
	private String hostClient;

	@Id
	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	public FndKantorVpn() {
	}

	public String getHostClient() {
		return this.hostClient;
	}

	public void setHostClient(String hostClient) {
		this.hostClient = hostClient;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

}