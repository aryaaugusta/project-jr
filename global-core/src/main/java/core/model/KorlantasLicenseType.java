package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_LICENSE_TYPES database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_LICENSE_TYPES")
@NamedQuery(name="KorlantasLicenseType.findAll", query="SELECT k FROM KorlantasLicenseType k")
public class KorlantasLicenseType implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal archived;

	@Id
	private BigDecimal id;

	private String name;

	@Column(name="SORT_ORDER")
	private BigDecimal sortOrder;

	public KorlantasLicenseType() {
	}

	public BigDecimal getArchived() {
		return this.archived;
	}

	public void setArchived(BigDecimal archived) {
		this.archived = archived;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}