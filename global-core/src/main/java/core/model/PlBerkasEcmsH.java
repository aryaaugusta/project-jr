package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_BERKAS_ECMS_H database table.
 * 
 */
@Entity
@Table(name="PL_BERKAS_ECMS_H")
@NamedQuery(name="PlBerkasEcmsH.findAll", query="SELECT p FROM PlBerkasEcmsH p")
public class PlBerkasEcmsH implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_KORBAN_KECELAKAAN")
	private String idKorbanKecelakaan;

	@Column(name="NO_REG_ECMS")
	private String noRegEcms;

	public PlBerkasEcmsH() {
	}

	public String getIdKorbanKecelakaan() {
		return this.idKorbanKecelakaan;
	}

	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}

	public String getNoRegEcms() {
		return this.noRegEcms;
	}

	public void setNoRegEcms(String noRegEcms) {
		this.noRegEcms = noRegEcms;
	}

}