package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_DASH_OTORISASI database table.
 * 
 */
@Entity
@Table(name="PL_DASH_OTORISASI")
@NamedQuery(name="PlDashOtorisasi.findAll", query="SELECT p FROM PlDashOtorisasi p")
@IdClass(PlDashOtorisasiPK.class)
public class PlDashOtorisasi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="JML_BERKAS")
	private BigDecimal jmlBerkas;

	@Id
	@Column(name="KODE_KANTOR")
	private String kodeKantor;

	@Id
	private String otorisasi;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_PENGAJUAN")
	private Date tglPengajuan;

	public PlDashOtorisasi() {
	}

	public BigDecimal getJmlBerkas() {
		return this.jmlBerkas;
	}

	public void setJmlBerkas(BigDecimal jmlBerkas) {
		this.jmlBerkas = jmlBerkas;
	}

	public String getKodeKantor() {
		return this.kodeKantor;
	}

	public void setKodeKantor(String kodeKantor) {
		this.kodeKantor = kodeKantor;
	}

	public String getOtorisasi() {
		return this.otorisasi;
	}

	public void setOtorisasi(String otorisasi) {
		this.otorisasi = otorisasi;
	}

	public Date getTglPengajuan() {
		return this.tglPengajuan;
	}

	public void setTglPengajuan(Date tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}

}