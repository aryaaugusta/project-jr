package core.model;

import java.io.Serializable;


public class AuthElementKntrPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String elementCode;
	private String kodeKantorJr;
	private String levelRest;
	
	public AuthElementKntrPK(){}
	
	
	public AuthElementKntrPK(String elementCode, String kodeKantorJr,
			String levelRest) {
		super();
		this.elementCode = elementCode;
		this.kodeKantorJr = kodeKantorJr;
		this.levelRest = levelRest;
	}


	public String getElementCode() {
		return elementCode;
	}
	public void setElementCode(String elementCode) {
		this.elementCode = elementCode;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getLevelRest() {
		return levelRest;
	}

	public void setLevelRest(String levelRest) {
		this.levelRest = levelRest;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((elementCode == null) ? 0 : elementCode.hashCode());
		result = prime * result
				+ ((kodeKantorJr == null) ? 0 : kodeKantorJr.hashCode());
		result = prime * result
				+ ((levelRest == null) ? 0 : levelRest.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthElementKntrPK other = (AuthElementKntrPK) obj;
		if (elementCode == null) {
			if (other.elementCode != null)
				return false;
		} else if (!elementCode.equals(other.elementCode))
			return false;
		if (kodeKantorJr == null) {
			if (other.kodeKantorJr != null)
				return false;
		} else if (!kodeKantorJr.equals(other.kodeKantorJr))
			return false;
		if (levelRest == null) {
			if (other.levelRest != null)
				return false;
		} else if (!levelRest.equals(other.levelRest))
			return false;
		return true;
	}
}
