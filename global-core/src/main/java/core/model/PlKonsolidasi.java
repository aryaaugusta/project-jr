package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_KONSOLIDASI database table.
 * 
 */
@Entity
@Table(name="PL_KONSOLIDASI")
@NamedQuery(name="PlKonsolidasi.findAll", query="SELECT p FROM PlKonsolidasi p")
public class PlKonsolidasi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="ERR_TRACE")
	private String errTrace;

	@Id
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="STATUS_PROSES")
	private String statusProses;

	public PlKonsolidasi() {
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getErrTrace() {
		return this.errTrace;
	}

	public void setErrTrace(String errTrace) {
		this.errTrace = errTrace;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getStatusProses() {
		return this.statusProses;
	}

	public void setStatusProses(String statusProses) {
		this.statusProses = statusProses;
	}

}