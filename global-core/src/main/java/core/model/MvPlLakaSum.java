package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the MV_PL_LAKA_SUM database table.
 * 
 */
@Entity
@Table(name="MV_PL_LAKA_SUM")
@NamedQuery(name="MvPlLakaSum.findAll", query="SELECT m FROM MvPlLakaSum m")
public class MvPlLakaSum implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ASAL_BERKAS")
	private String asalBerkas;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="DESC_KODE_KANTOR_JR")
	private String descKodeKantorJr;

	@Column(name="DESC_SIFAT_KECELAKAAN")
	private String descSifatKecelakaan;

	@Column(name="DESKRIPSI_LOKASI")
	private String deskripsiLokasi;

	@Column(name="ID_KECELAKAAN")
	private String idKecelakaan;

	@Id
	@Column(name="ID_LAKA")
	private String idLaka;

	private String instansi;

	@Column(name="K_ROWID")
	private String kRowid;

	@Column(name="KODE_INSTANSI")
	private String kodeInstansi;

	@Column(name="KODE_JAMINAN")
	private String kodeJaminan;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="KODE_LOKASI")
	private String kodeLokasi;

	@Column(name="KODE_MASKAPAI")
	private String kodeMaskapai;

	@Column(name="KODE_PO")
	private String kodePo;

	@Column(name="KODE_SIFAT_CIDERA")
	private String kodeSifatCidera;

	@Column(name="LINGKUP_JAMINAN")
	private String lingkupJaminan;

	private String lokasi;

	@Column(name="N_ROWID")
	private String nRowid;

	@Column(name="NAMA_KORBAN")
	private String namaKorban;

	@Column(name="NAMA_PETUGAS")
	private String namaPetugas;

	@Column(name="NO_IDENTITAS")
	private String noIdentitas;

	@Column(name="NO_LAPORAN_POLISI")
	private String noLaporanPolisi;

	@Column(name="P_ROWID")
	private String pRowid;

	@Column(name="SIFAT_CIDERA")
	private String sifatCidera;

	@Column(name="SIFAT_KECELAKAAN")
	private String sifatKecelakaan;

	@Column(name="STATUS_LAPORAN_POLISI")
	private String statusLaporanPolisi;

	@Column(name="STATUS_TRANSISI")
	private String statusTransisi;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_KEJADIAN")
	private Date tglKejadian;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_LAPORAN_POLISI")
	private Date tglLaporanPolisi;

	public MvPlLakaSum() {
	}

	public String getAsalBerkas() {
		return this.asalBerkas;
	}

	public void setAsalBerkas(String asalBerkas) {
		this.asalBerkas = asalBerkas;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDescKodeKantorJr() {
		return this.descKodeKantorJr;
	}

	public void setDescKodeKantorJr(String descKodeKantorJr) {
		this.descKodeKantorJr = descKodeKantorJr;
	}

	public String getDescSifatKecelakaan() {
		return this.descSifatKecelakaan;
	}

	public void setDescSifatKecelakaan(String descSifatKecelakaan) {
		this.descSifatKecelakaan = descSifatKecelakaan;
	}

	public String getDeskripsiLokasi() {
		return this.deskripsiLokasi;
	}

	public void setDeskripsiLokasi(String deskripsiLokasi) {
		this.deskripsiLokasi = deskripsiLokasi;
	}

	public String getIdKecelakaan() {
		return this.idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getIdLaka() {
		return this.idLaka;
	}

	public void setIdLaka(String idLaka) {
		this.idLaka = idLaka;
	}

	public String getInstansi() {
		return this.instansi;
	}

	public void setInstansi(String instansi) {
		this.instansi = instansi;
	}

	public String getKRowid() {
		return this.kRowid;
	}

	public void setKRowid(String kRowid) {
		this.kRowid = kRowid;
	}

	public String getKodeInstansi() {
		return this.kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public String getKodeJaminan() {
		return this.kodeJaminan;
	}

	public void setKodeJaminan(String kodeJaminan) {
		this.kodeJaminan = kodeJaminan;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeLokasi() {
		return this.kodeLokasi;
	}

	public void setKodeLokasi(String kodeLokasi) {
		this.kodeLokasi = kodeLokasi;
	}

	public String getKodeMaskapai() {
		return this.kodeMaskapai;
	}

	public void setKodeMaskapai(String kodeMaskapai) {
		this.kodeMaskapai = kodeMaskapai;
	}

	public String getKodePo() {
		return this.kodePo;
	}

	public void setKodePo(String kodePo) {
		this.kodePo = kodePo;
	}

	public String getKodeSifatCidera() {
		return this.kodeSifatCidera;
	}

	public void setKodeSifatCidera(String kodeSifatCidera) {
		this.kodeSifatCidera = kodeSifatCidera;
	}

	public String getLingkupJaminan() {
		return this.lingkupJaminan;
	}

	public void setLingkupJaminan(String lingkupJaminan) {
		this.lingkupJaminan = lingkupJaminan;
	}

	public String getLokasi() {
		return this.lokasi;
	}

	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}

	public String getNRowid() {
		return this.nRowid;
	}

	public void setNRowid(String nRowid) {
		this.nRowid = nRowid;
	}

	public String getNamaKorban() {
		return this.namaKorban;
	}

	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}

	public String getNamaPetugas() {
		return this.namaPetugas;
	}

	public void setNamaPetugas(String namaPetugas) {
		this.namaPetugas = namaPetugas;
	}

	public String getNoIdentitas() {
		return this.noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getNoLaporanPolisi() {
		return this.noLaporanPolisi;
	}

	public void setNoLaporanPolisi(String noLaporanPolisi) {
		this.noLaporanPolisi = noLaporanPolisi;
	}

	public String getPRowid() {
		return this.pRowid;
	}

	public void setPRowid(String pRowid) {
		this.pRowid = pRowid;
	}

	public String getSifatCidera() {
		return this.sifatCidera;
	}

	public void setSifatCidera(String sifatCidera) {
		this.sifatCidera = sifatCidera;
	}

	public String getSifatKecelakaan() {
		return this.sifatKecelakaan;
	}

	public void setSifatKecelakaan(String sifatKecelakaan) {
		this.sifatKecelakaan = sifatKecelakaan;
	}

	public String getStatusLaporanPolisi() {
		return this.statusLaporanPolisi;
	}

	public void setStatusLaporanPolisi(String statusLaporanPolisi) {
		this.statusLaporanPolisi = statusLaporanPolisi;
	}

	public String getStatusTransisi() {
		return this.statusTransisi;
	}

	public void setStatusTransisi(String statusTransisi) {
		this.statusTransisi = statusTransisi;
	}

	public Date getTglKejadian() {
		return this.tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public Date getTglLaporanPolisi() {
		return this.tglLaporanPolisi;
	}

	public void setTglLaporanPolisi(Date tglLaporanPolisi) {
		this.tglLaporanPolisi = tglLaporanPolisi;
	}

}