package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the AUTH_DELEGATE database table.
 * 
 */
@Entity
@Table(name="AUTH_DELEGATE")
@NamedQuery(name="AuthDelegate.findAll", query="SELECT a FROM AuthDelegate a")
public class AuthDelegate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_DELEGATE")
	private String idDelegate;

	private String description;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATETIME")
	private Date endDatetime;

	@Column(name="GROUP_CODE_SOURCE")
	private String groupCodeSource;

	@Column(name="GROUP_CODE_TARGET")
	private String groupCodeTarget;

	@Column(name="LOGIN_SOURCE")
	private String loginSource;

	@Column(name="LOGIN_TARGET")
	private String loginTarget;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATETIME")
	private Date startDatetime;

	@Column(name="STATUS_DELEGATE")
	private String statusDelegate;

	@Column(name="USER_DELEGATE")
	private String userDelegate;

	public AuthDelegate() {
	}

	public String getIdDelegate() {
		return this.idDelegate;
	}

	public void setIdDelegate(String idDelegate) {
		this.idDelegate = idDelegate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getEndDatetime() {
		return this.endDatetime;
	}

	public void setEndDatetime(Date endDatetime) {
		this.endDatetime = endDatetime;
	}

	public String getGroupCodeSource() {
		return this.groupCodeSource;
	}

	public void setGroupCodeSource(String groupCodeSource) {
		this.groupCodeSource = groupCodeSource;
	}

	public String getGroupCodeTarget() {
		return this.groupCodeTarget;
	}

	public void setGroupCodeTarget(String groupCodeTarget) {
		this.groupCodeTarget = groupCodeTarget;
	}

	public String getLoginSource() {
		return this.loginSource;
	}

	public void setLoginSource(String loginSource) {
		this.loginSource = loginSource;
	}

	public String getLoginTarget() {
		return this.loginTarget;
	}

	public void setLoginTarget(String loginTarget) {
		this.loginTarget = loginTarget;
	}

	public Date getStartDatetime() {
		return this.startDatetime;
	}

	public void setStartDatetime(Date startDatetime) {
		this.startDatetime = startDatetime;
	}

	public String getStatusDelegate() {
		return this.statusDelegate;
	}

	public void setStatusDelegate(String statusDelegate) {
		this.statusDelegate = statusDelegate;
	}

	public String getUserDelegate() {
		return this.userDelegate;
	}

	public void setUserDelegate(String userDelegate) {
		this.userDelegate = userDelegate;
	}

}