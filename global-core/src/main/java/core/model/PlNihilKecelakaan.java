package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_NIHIL_KECELAKAAN database table.
 * 
 */
@Entity
@Table(name="PL_NIHIL_KECELAKAAN")
@NamedQuery(name="PlNihilKecelakaan.findAll", query="SELECT p FROM PlNihilKecelakaan p")
public class PlNihilKecelakaan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="ID_LAKA_NHL")
	private String idLakaNhl;

	@Column(name="KODE_INSTANSI")
	private String kodeInstansi;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="STATUS_NHL")
	private String statusNhl;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_KEJADIAN")
	private Date tglKejadian;

	public PlNihilKecelakaan() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdLakaNhl() {
		return this.idLakaNhl;
	}

	public void setIdLakaNhl(String idLakaNhl) {
		this.idLakaNhl = idLakaNhl;
	}

	public String getKodeInstansi() {
		return this.kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getStatusNhl() {
		return this.statusNhl;
	}

	public void setStatusNhl(String statusNhl) {
		this.statusNhl = statusNhl;
	}

	public Date getTglKejadian() {
		return this.tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

}