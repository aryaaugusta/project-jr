package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PL_BERKAS_PENGAJUAN database table.
 * 
 */
public class PlBerkasPengajuanPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String noBerkas;

	private String kodeBerkas;

	public PlBerkasPengajuanPK() {
	}
	public String getNoBerkas() {
		return this.noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getKodeBerkas() {
		return this.kodeBerkas;
	}
	public void setKodeBerkas(String kodeBerkas) {
		this.kodeBerkas = kodeBerkas;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PlBerkasPengajuanPK)) {
			return false;
		}
		PlBerkasPengajuanPK castOther = (PlBerkasPengajuanPK)other;
		return 
			this.noBerkas.equals(castOther.noBerkas)
			&& this.kodeBerkas.equals(castOther.kodeBerkas);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.noBerkas.hashCode();
		hash = hash * prime + this.kodeBerkas.hashCode();
		
		return hash;
	}
}