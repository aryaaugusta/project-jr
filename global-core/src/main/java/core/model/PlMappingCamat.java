package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_MAPPING_CAMAT database table.
 * 
 */
@Entity
@Table(name="PL_MAPPING_CAMAT")
@NamedQuery(name="PlMappingCamat.findAll", query="SELECT p FROM PlMappingCamat p")
public class PlMappingCamat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KODE_LOKASI")
	private String kodeLokasi;

	private String catatan;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	@Column(name="KODE_CAMAT")
	private String kodeCamat;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	public PlMappingCamat() {
	}

	public String getKodeLokasi() {
		return this.kodeLokasi;
	}

	public void setKodeLokasi(String kodeLokasi) {
		this.kodeLokasi = kodeLokasi;
	}

	public String getCatatan() {
		return this.catatan;
	}

	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getKodeCamat() {
		return this.kodeCamat;
	}

	public void setKodeCamat(String kodeCamat) {
		this.kodeCamat = kodeCamat;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

}