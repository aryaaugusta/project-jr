package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the CSV_STORE database table.
 * 
 */
@Entity
@Table(name="CSV_STORE")
@NamedQuery(name="CsvStore.findAll", query="SELECT c FROM CsvStore c")
public class CsvStore implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	@Column(name="\"DATA\"")
	private String data;

	@Id
	private BigDecimal id;

	public CsvStore() {
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

}