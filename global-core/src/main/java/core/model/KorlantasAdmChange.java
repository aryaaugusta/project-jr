package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the KORLANTAS_ADM_CHANGES database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_ADM_CHANGES")
@NamedQuery(name="KorlantasAdmChange.findAll", query="SELECT k FROM KorlantasAdmChange k")
public class KorlantasAdmChange implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	private Date dtime;

	@Id
	private BigDecimal id;

	public KorlantasAdmChange() {
	}

	public Date getDtime() {
		return this.dtime;
	}

	public void setDtime(Date dtime) {
		this.dtime = dtime;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

}