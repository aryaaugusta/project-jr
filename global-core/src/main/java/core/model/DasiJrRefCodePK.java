package core.model;

import java.io.Serializable;


public class DasiJrRefCodePK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String rvDomain;
	private String rvLowValue;
	public String getRvDomain() {
		return rvDomain;
	}
	public void setRvDomain(String rvDomain) {
		this.rvDomain = rvDomain;
	}
	public String getRvLowValue() {
		return rvLowValue;
	}
	public void setRvLowValue(String rvLowValue) {
		this.rvLowValue = rvLowValue;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((rvDomain == null) ? 0 : rvDomain.hashCode());
		result = prime * result
				+ ((rvLowValue == null) ? 0 : rvLowValue.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DasiJrRefCodePK other = (DasiJrRefCodePK) obj;
		if (rvDomain == null) {
			if (other.rvDomain != null)
				return false;
		} else if (!rvDomain.equals(other.rvDomain))
			return false;
		if (rvLowValue == null) {
			if (other.rvLowValue != null)
				return false;
		} else if (!rvLowValue.equals(other.rvLowValue))
			return false;
		return true;
	}
}
