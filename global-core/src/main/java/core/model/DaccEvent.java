package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DACC_EVENTS database table.
 * 
 */
@Entity
@Table(name="DACC_EVENTS")
@NamedQuery(name="DaccEvent.findAll", query="SELECT d FROM DaccEvent d")
public class DaccEvent implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="EVENT_ID")
	private String eventId;

	@Temporal(TemporalType.DATE)
	@Column(name="EVENT_DATE")
	private Date eventDate;

	@Column(name="EVENT_NAME")
	private String eventName;

	public DaccEvent() {
	}

	public String getEventId() {
		return this.eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public Date getEventDate() {
		return this.eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventName() {
		return this.eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

}