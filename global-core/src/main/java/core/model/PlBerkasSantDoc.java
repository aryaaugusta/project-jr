package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_BERKAS_SANT_DOC database table.
 * 
 */
@Entity
@Table(name="PL_BERKAS_SANT_DOC")
@NamedQuery(name="PlBerkasSantDoc.findAll", query="SELECT p FROM PlBerkasSantDoc p")
public class PlBerkasSantDoc implements Serializable {
	private static final long serialVersionUID = 1L;

	private String ddocname;

	@Column(name="KODE_BERKAS")
	private String kodeBerkas;

	@Id
	@Column(name="NO_BERKAS")
	private String noBerkas;

	public PlBerkasSantDoc() {
	}

	public String getDdocname() {
		return this.ddocname;
	}

	public void setDdocname(String ddocname) {
		this.ddocname = ddocname;
	}

	public String getKodeBerkas() {
		return this.kodeBerkas;
	}

	public void setKodeBerkas(String kodeBerkas) {
		this.kodeBerkas = kodeBerkas;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

}