package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the AUTH_ELEMENT_KNTR database table.
 * 
 */
@Entity
@Table(name="AUTH_ELEMENT_KNTR")
@NamedQuery(name="AuthElementKntr.findAll", query="SELECT a FROM AuthElementKntr a")
@IdClass(AuthElementKntrPK.class)
public class AuthElementKntr implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ELEMENT_CODE")
	private String elementCode;

	@Id
	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Id
	@Column(name="LEVEL_REST")
	private String levelRest;

	public AuthElementKntr() {
	}

	public String getElementCode() {
		return this.elementCode;
	}

	public void setElementCode(String elementCode) {
		this.elementCode = elementCode;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getLevelRest() {
		return this.levelRest;
	}

	public void setLevelRest(String levelRest) {
		this.levelRest = levelRest;
	}

}