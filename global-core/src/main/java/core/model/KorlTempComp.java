package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORL_TEMP_COMP database table.
 * 
 */
@Entity
@Table(name="KORL_TEMP_COMP")
@NamedQuery(name="KorlTempComp.findAll", query="SELECT k FROM KorlTempComp k")
public class KorlTempComp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private BigDecimal id;

	private BigDecimal stat;

	@Column(name="TBL_NAME")
	private String tblName;

	public KorlTempComp() {
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public BigDecimal getStat() {
		return this.stat;
	}

	public void setStat(BigDecimal stat) {
		this.stat = stat;
	}

	public String getTblName() {
		return this.tblName;
	}

	public void setTblName(String tblName) {
		this.tblName = tblName;
	}

}