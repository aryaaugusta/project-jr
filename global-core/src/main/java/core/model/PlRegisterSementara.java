package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_REGISTER_SEMENTARA database table.
 * 
 */
@Entity
@Table(name="PL_REGISTER_SEMENTARA")
@NamedQuery(name="PlRegisterSementara.findAll", query="SELECT p FROM PlRegisterSementara p")
public class PlRegisterSementara implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NO_REGISTER")
	private String noRegister;

	@Column(name="ALAMAT_PEMOHON")
	private String alamatPemohon;

	@Column(name="ASAL_BERKAS_FLAG")
	private String asalBerkasFlag;

	@Column(name="BPK_CREATOR")
	private String bpkCreator;

	@Column(name="CIDERA_KORBAN")
	private String cideraKorban;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="DILIMPAHKAN_KE")
	private String dilimpahkanKe;

	@Column(name="ID_KORBAN_KECELAKAAN")
	private String idKorbanKecelakaan;

	@Column(name="JENIS_IDENTITAS")
	private String jenisIdentitas;

	@Column(name="JML_AMBL")
	private BigDecimal jmlAmbl;

	@Column(name="JML_P3K")
	private BigDecimal jmlP3k;

	@Column(name="JUMLAH_PENGAJUAN_1")
	private BigDecimal jumlahPengajuan1;

	@Column(name="JUMLAH_PENGAJUAN_2")
	private BigDecimal jumlahPengajuan2;

	@Column(name="KODE_HUBUNGAN_KORBAN")
	private String kodeHubunganKorban;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="KODE_RUMAH_SAKIT")
	private String kodeRumahSakit;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NAMA_PEMOHON")
	private String namaPemohon;

	@Column(name="NO_BPK")
	private String noBpk;

	@Column(name="NO_IDENTITAS")
	private String noIdentitas;

	@Column(name="STATUS_KRBN_RS")
	private String statusKrbnRs;

	@Column(name="TELP_PEMOHON")
	private String telpPemohon;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_BPK")
	private Date tglBpk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_MASUK_RS")
	private Date tglMasukRs;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_MD_KORBAN")
	private Date tglMdKorban;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_REGISTER")
	private Date tglRegister;

	public PlRegisterSementara() {
	}

	public String getNoRegister() {
		return this.noRegister;
	}

	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}

	public String getAlamatPemohon() {
		return this.alamatPemohon;
	}

	public void setAlamatPemohon(String alamatPemohon) {
		this.alamatPemohon = alamatPemohon;
	}

	public String getAsalBerkasFlag() {
		return this.asalBerkasFlag;
	}

	public void setAsalBerkasFlag(String asalBerkasFlag) {
		this.asalBerkasFlag = asalBerkasFlag;
	}

	public String getBpkCreator() {
		return this.bpkCreator;
	}

	public void setBpkCreator(String bpkCreator) {
		this.bpkCreator = bpkCreator;
	}

	public String getCideraKorban() {
		return this.cideraKorban;
	}

	public void setCideraKorban(String cideraKorban) {
		this.cideraKorban = cideraKorban;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDilimpahkanKe() {
		return this.dilimpahkanKe;
	}

	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}

	public String getIdKorbanKecelakaan() {
		return this.idKorbanKecelakaan;
	}

	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}

	public String getJenisIdentitas() {
		return this.jenisIdentitas;
	}

	public void setJenisIdentitas(String jenisIdentitas) {
		this.jenisIdentitas = jenisIdentitas;
	}

	public BigDecimal getJmlAmbl() {
		return this.jmlAmbl;
	}

	public void setJmlAmbl(BigDecimal jmlAmbl) {
		this.jmlAmbl = jmlAmbl;
	}

	public BigDecimal getJmlP3k() {
		return this.jmlP3k;
	}

	public void setJmlP3k(BigDecimal jmlP3k) {
		this.jmlP3k = jmlP3k;
	}

	public BigDecimal getJumlahPengajuan1() {
		return this.jumlahPengajuan1;
	}

	public void setJumlahPengajuan1(BigDecimal jumlahPengajuan1) {
		this.jumlahPengajuan1 = jumlahPengajuan1;
	}

	public BigDecimal getJumlahPengajuan2() {
		return this.jumlahPengajuan2;
	}

	public void setJumlahPengajuan2(BigDecimal jumlahPengajuan2) {
		this.jumlahPengajuan2 = jumlahPengajuan2;
	}

	public String getKodeHubunganKorban() {
		return this.kodeHubunganKorban;
	}

	public void setKodeHubunganKorban(String kodeHubunganKorban) {
		this.kodeHubunganKorban = kodeHubunganKorban;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeRumahSakit() {
		return this.kodeRumahSakit;
	}

	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamaPemohon() {
		return this.namaPemohon;
	}

	public void setNamaPemohon(String namaPemohon) {
		this.namaPemohon = namaPemohon;
	}

	public String getNoBpk() {
		return this.noBpk;
	}

	public void setNoBpk(String noBpk) {
		this.noBpk = noBpk;
	}

	public String getNoIdentitas() {
		return this.noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getStatusKrbnRs() {
		return this.statusKrbnRs;
	}

	public void setStatusKrbnRs(String statusKrbnRs) {
		this.statusKrbnRs = statusKrbnRs;
	}

	public String getTelpPemohon() {
		return this.telpPemohon;
	}

	public void setTelpPemohon(String telpPemohon) {
		this.telpPemohon = telpPemohon;
	}

	public Date getTglBpk() {
		return this.tglBpk;
	}

	public void setTglBpk(Date tglBpk) {
		this.tglBpk = tglBpk;
	}

	public Date getTglMasukRs() {
		return this.tglMasukRs;
	}

	public void setTglMasukRs(Date tglMasukRs) {
		this.tglMasukRs = tglMasukRs;
	}

	public Date getTglMdKorban() {
		return this.tglMdKorban;
	}

	public void setTglMdKorban(Date tglMdKorban) {
		this.tglMdKorban = tglMdKorban;
	}

	public Date getTglRegister() {
		return this.tglRegister;
	}

	public void setTglRegister(Date tglRegister) {
		this.tglRegister = tglRegister;
	}

}