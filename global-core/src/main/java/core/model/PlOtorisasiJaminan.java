package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_OTORISASI_JAMINAN database table.
 * 
 */
@Entity
@Table(name="PL_OTORISASI_JAMINAN")
@NamedQuery(name="PlOtorisasiJaminan.findAll", query="SELECT p FROM PlOtorisasiJaminan p")
public class PlOtorisasiJaminan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_OTORISASI")
	private String idOtorisasi;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="ID_JAMINAN")
	private String idJaminan;

	@Column(name="JENIS_OTORISASI")
	private String jenisOtorisasi;

	@Column(name="KETERANGAN_OTORISASI")
	private String keteranganOtorisasi;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	private String otorisasi;

	@Column(name="STATUS_BATAL")
	private String statusBatal;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_OTORISASI")
	private Date tglOtorisasi;

	public PlOtorisasiJaminan() {
	}

	public String getIdOtorisasi() {
		return this.idOtorisasi;
	}

	public void setIdOtorisasi(String idOtorisasi) {
		this.idOtorisasi = idOtorisasi;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdJaminan() {
		return this.idJaminan;
	}

	public void setIdJaminan(String idJaminan) {
		this.idJaminan = idJaminan;
	}

	public String getJenisOtorisasi() {
		return this.jenisOtorisasi;
	}

	public void setJenisOtorisasi(String jenisOtorisasi) {
		this.jenisOtorisasi = jenisOtorisasi;
	}

	public String getKeteranganOtorisasi() {
		return this.keteranganOtorisasi;
	}

	public void setKeteranganOtorisasi(String keteranganOtorisasi) {
		this.keteranganOtorisasi = keteranganOtorisasi;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getOtorisasi() {
		return this.otorisasi;
	}

	public void setOtorisasi(String otorisasi) {
		this.otorisasi = otorisasi;
	}

	public String getStatusBatal() {
		return this.statusBatal;
	}

	public void setStatusBatal(String statusBatal) {
		this.statusBatal = statusBatal;
	}

	public Date getTglOtorisasi() {
		return this.tglOtorisasi;
	}

	public void setTglOtorisasi(Date tglOtorisasi) {
		this.tglOtorisasi = tglOtorisasi;
	}

}