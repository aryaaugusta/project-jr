package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_RS_RESPONSE database table.
 * 
 */
@Entity
@Table(name="PL_RS_RESPONSE")
@NamedQuery(name="PlRsResponse.findAll", query="SELECT p FROM PlRsResponse p")
public class PlRsResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="DILIMPAHKAN_KE")
	private String dilimpahkanKe;

	@Column(name="DIPROSES_DI")
	private String diprosesDi;

	@Id
	@Column(name="ID_RESPONSE")
	private String idResponse;

	@Column(name="KODE_KEJADIAN")
	private String kodeKejadian;

	@Column(name="KODE_KESIMPULAN")
	private String kodeKesimpulan;

	@Column(name="KODE_REKOMENDASI")
	private String kodeRekomendasi;

	@Column(name="KODE_RS")
	private String kodeRs;

	@Column(name="LAST_UPDATE_BY")
	private String lastUpdateBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATE_DATE")
	private Date lastUpdateDate;

	private String petugas;

	private String uraian;

	public PlRsResponse() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDilimpahkanKe() {
		return this.dilimpahkanKe;
	}

	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}

	public String getDiprosesDi() {
		return this.diprosesDi;
	}

	public void setDiprosesDi(String diprosesDi) {
		this.diprosesDi = diprosesDi;
	}

	public String getIdResponse() {
		return this.idResponse;
	}

	public void setIdResponse(String idResponse) {
		this.idResponse = idResponse;
	}

	public String getKodeKejadian() {
		return this.kodeKejadian;
	}

	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}

	public String getKodeKesimpulan() {
		return this.kodeKesimpulan;
	}

	public void setKodeKesimpulan(String kodeKesimpulan) {
		this.kodeKesimpulan = kodeKesimpulan;
	}

	public String getKodeRekomendasi() {
		return this.kodeRekomendasi;
	}

	public void setKodeRekomendasi(String kodeRekomendasi) {
		this.kodeRekomendasi = kodeRekomendasi;
	}

	public String getKodeRs() {
		return this.kodeRs;
	}

	public void setKodeRs(String kodeRs) {
		this.kodeRs = kodeRs;
	}

	public String getLastUpdateBy() {
		return this.lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public String getPetugas() {
		return this.petugas;
	}

	public void setPetugas(String petugas) {
		this.petugas = petugas;
	}

	public String getUraian() {
		return this.uraian;
	}

	public void setUraian(String uraian) {
		this.uraian = uraian;
	}

}