package core.model;

import java.io.Serializable;


public class PlOtorisasiBerkasPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kdKantorJr;
	private String noBerkas;
	private String otorisator;
	public String getKdKantorJr() {
		return kdKantorJr;
	}
	public void setKdKantorJr(String kdKantorJr) {
		this.kdKantorJr = kdKantorJr;
	}
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getOtorisator() {
		return otorisator;
	}
	public void setOtorisator(String otorisator) {
		this.otorisator = otorisator;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kdKantorJr == null) ? 0 : kdKantorJr.hashCode());
		result = prime * result
				+ ((noBerkas == null) ? 0 : noBerkas.hashCode());
		result = prime * result
				+ ((otorisator == null) ? 0 : otorisator.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlOtorisasiBerkasPK other = (PlOtorisasiBerkasPK) obj;
		if (kdKantorJr == null) {
			if (other.kdKantorJr != null)
				return false;
		} else if (!kdKantorJr.equals(other.kdKantorJr))
			return false;
		if (noBerkas == null) {
			if (other.noBerkas != null)
				return false;
		} else if (!noBerkas.equals(other.noBerkas))
			return false;
		if (otorisator == null) {
			if (other.otorisator != null)
				return false;
		} else if (!otorisator.equals(other.otorisator))
			return false;
		return true;
	}
}
