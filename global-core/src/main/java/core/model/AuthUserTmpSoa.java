package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the AUTH_USER_TMP_SOA database table.
 * 
 */
@Entity
@Table(name="AUTH_USER_TMP_SOA")
@NamedQuery(name="AuthUserTmpSoa.findAll", query="SELECT a FROM AuthUserTmpSoa a")
public class AuthUserTmpSoa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_PEGAWAI")
	private String idPegawai;

	@Column(name="KODE_KANTOR")
	private String kodeKantor;

	@Column(name="NAMA_PEGAWAI")
	private String namaPegawai;

	@Column(name="NO_TELP")
	private String noTelp;

	public AuthUserTmpSoa() {
	}

	public String getIdPegawai() {
		return this.idPegawai;
	}

	public void setIdPegawai(String idPegawai) {
		this.idPegawai = idPegawai;
	}

	public String getKodeKantor() {
		return this.kodeKantor;
	}

	public void setKodeKantor(String kodeKantor) {
		this.kodeKantor = kodeKantor;
	}

	public String getNamaPegawai() {
		return this.namaPegawai;
	}

	public void setNamaPegawai(String namaPegawai) {
		this.namaPegawai = namaPegawai;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

}