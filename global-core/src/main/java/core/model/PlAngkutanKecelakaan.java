package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_ANGKUTAN_KECELAKAAN database table.
 * 
 */
@Entity
@Table(name="PL_ANGKUTAN_KECELAKAAN")
@NamedQuery(name="PlAngkutanKecelakaan.findAll", query="SELECT p FROM PlAngkutanKecelakaan p")
public class PlAngkutanKecelakaan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_ANGKUTAN_KECELAKAAN")
	private String idAngkutanKecelakaan;

	@Column(name="ALAMAT_PEMILIK")
	private String alamatPemilik;

	@Column(name="ALAMAT_PENGEMUDI")
	private String alamatPengemudi;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="ID_GUID")
	private String idGuid;

	@Column(name="ID_KECELAKAAN")
	private String idKecelakaan;

	private String kode;

	@Column(name="KODE_GOLONGAN")
	private String kodeGolongan;

	@Column(name="KODE_JENIS")
	private String kodeJenis;

	@Column(name="KODE_JENIS_SIM")
	private String kodeJenisSim;

	@Column(name="KODE_MERK")
	private String kodeMerk;

	@Column(name="KODE_PO")
	private String kodePo;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Temporal(TemporalType.DATE)
	@Column(name="MASA_BERLAKU_SIM")
	private Date masaBerlakuSim;

	@Column(name="NAMA_PEMILIK")
	private String namaPemilik;

	@Column(name="NAMA_PENGEMUDI")
	private String namaPengemudi;

	@Column(name="NO_POLISI")
	private String noPolisi;

	@Column(name="NO_SIM_PENGEMUDI")
	private String noSimPengemudi;

	@Column(name="STATUS_KENDARAAN")
	private String statusKendaraan;

	@Column(name="TAHUN_PEMBUATAN")
	private BigDecimal tahunPembuatan;

	public PlAngkutanKecelakaan() {
	}

	public String getIdAngkutanKecelakaan() {
		return this.idAngkutanKecelakaan;
	}

	public void setIdAngkutanKecelakaan(String idAngkutanKecelakaan) {
		this.idAngkutanKecelakaan = idAngkutanKecelakaan;
	}

	public String getAlamatPemilik() {
		return this.alamatPemilik;
	}

	public void setAlamatPemilik(String alamatPemilik) {
		this.alamatPemilik = alamatPemilik;
	}

	public String getAlamatPengemudi() {
		return this.alamatPengemudi;
	}

	public void setAlamatPengemudi(String alamatPengemudi) {
		this.alamatPengemudi = alamatPengemudi;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdGuid() {
		return this.idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public String getIdKecelakaan() {
		return this.idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getKode() {
		return this.kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getKodeGolongan() {
		return this.kodeGolongan;
	}

	public void setKodeGolongan(String kodeGolongan) {
		this.kodeGolongan = kodeGolongan;
	}

	public String getKodeJenis() {
		return this.kodeJenis;
	}

	public void setKodeJenis(String kodeJenis) {
		this.kodeJenis = kodeJenis;
	}

	public String getKodeJenisSim() {
		return this.kodeJenisSim;
	}

	public void setKodeJenisSim(String kodeJenisSim) {
		this.kodeJenisSim = kodeJenisSim;
	}

	public String getKodeMerk() {
		return this.kodeMerk;
	}

	public void setKodeMerk(String kodeMerk) {
		this.kodeMerk = kodeMerk;
	}

	public String getKodePo() {
		return this.kodePo;
	}

	public void setKodePo(String kodePo) {
		this.kodePo = kodePo;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Date getMasaBerlakuSim() {
		return this.masaBerlakuSim;
	}

	public void setMasaBerlakuSim(Date masaBerlakuSim) {
		this.masaBerlakuSim = masaBerlakuSim;
	}

	public String getNamaPemilik() {
		return this.namaPemilik;
	}

	public void setNamaPemilik(String namaPemilik) {
		this.namaPemilik = namaPemilik;
	}

	public String getNamaPengemudi() {
		return this.namaPengemudi;
	}

	public void setNamaPengemudi(String namaPengemudi) {
		this.namaPengemudi = namaPengemudi;
	}

	public String getNoPolisi() {
		return this.noPolisi;
	}

	public void setNoPolisi(String noPolisi) {
		this.noPolisi = noPolisi;
	}

	public String getNoSimPengemudi() {
		return this.noSimPengemudi;
	}

	public void setNoSimPengemudi(String noSimPengemudi) {
		this.noSimPengemudi = noSimPengemudi;
	}

	public String getStatusKendaraan() {
		return this.statusKendaraan;
	}

	public void setStatusKendaraan(String statusKendaraan) {
		this.statusKendaraan = statusKendaraan;
	}

	public BigDecimal getTahunPembuatan() {
		return this.tahunPembuatan;
	}

	public void setTahunPembuatan(BigDecimal tahunPembuatan) {
		this.tahunPembuatan = tahunPembuatan;
	}

}