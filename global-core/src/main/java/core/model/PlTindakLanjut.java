package core.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_TINDAK_LANJUT database table.
 */
@Entity
@Table(name = "PL_TINDAK_LANJUT")
@IdClass(PlTindakLanjutPK.class)
@NamedQuery(name = "PlTindakLanjut.findAll", query = "SELECT p FROM PlTindakLanjut p")
public class PlTindakLanjut implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "ALASAN_NOCLAIM")
    private String alasanNoclaim;

    @Column(name = "CATATAN_TINDAK_LANJUT")
    private String catatanTindakLanjut;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "DILIMPAHKAN_KE")
    private String dilimpahkanKe;

    @Id
    @Column(name = "ID_TL_REGISTER")
    private String idTlRegister;

    @Column(name = "JML_KLAIM")
    private BigDecimal jmlKlaim;

    @Column(name = "LAST_UPDATED_BY")
    private String lastUpdatedBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Id
    @Column(name = "NO_REGISTER")
    private String noRegister;

    @Column(name = "NO_SURAT_JAMINAN")
    private String noSuratJaminan;

    @Column(name = "NO_SURAT_PANGGILAN")
    private String noSuratPanggilan;

    @Column(name = "PETUGAS_SURVEY")
    private String petugasSurvey;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "TGL_TINDAK_LANJUT")
    private Date tglTindakLanjut;

    @Column(name = "TINDAK_LANJUT_FLAG")
    private String tindakLanjutFlag;

    public PlTindakLanjut() {
    }

    public String getAlasanNoclaim() {
        return this.alasanNoclaim;
    }

    public void setAlasanNoclaim(String alasanNoclaim) {
        this.alasanNoclaim = alasanNoclaim;
    }

    public String getCatatanTindakLanjut() {
        return this.catatanTindakLanjut;
    }

    public void setCatatanTindakLanjut(String catatanTindakLanjut) {
        this.catatanTindakLanjut = catatanTindakLanjut;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return this.createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDilimpahkanKe() {
        return this.dilimpahkanKe;
    }

    public void setDilimpahkanKe(String dilimpahkanKe) {
        this.dilimpahkanKe = dilimpahkanKe;
    }

    public String getIdTlRegister() {
        return this.idTlRegister;
    }

    public void setIdTlRegister(String idTlRegister) {
        this.idTlRegister = idTlRegister;
    }

    public BigDecimal getJmlKlaim() {
        return this.jmlKlaim;
    }

    public void setJmlKlaim(BigDecimal jmlKlaim) {
        this.jmlKlaim = jmlKlaim;
    }

    public String getLastUpdatedBy() {
        return this.lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getLastUpdatedDate() {
        return this.lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getNoRegister() {
        return this.noRegister;
    }

    public void setNoRegister(String noRegister) {
        this.noRegister = noRegister;
    }

    public String getNoSuratJaminan() {
        return this.noSuratJaminan;
    }

    public void setNoSuratJaminan(String noSuratJaminan) {
        this.noSuratJaminan = noSuratJaminan;
    }

    public String getNoSuratPanggilan() {
        return this.noSuratPanggilan;
    }

    public void setNoSuratPanggilan(String noSuratPanggilan) {
        this.noSuratPanggilan = noSuratPanggilan;
    }

    public String getPetugasSurvey() {
        return this.petugasSurvey;
    }

    public void setPetugasSurvey(String petugasSurvey) {
        this.petugasSurvey = petugasSurvey;
    }

    public Date getTglTindakLanjut() {
        return this.tglTindakLanjut;
    }

    public void setTglTindakLanjut(Date tglTindakLanjut) {
        this.tglTindakLanjut = tglTindakLanjut;
    }

    public String getTindakLanjutFlag() {
        return this.tindakLanjutFlag;
    }

    public void setTindakLanjutFlag(String tindakLanjutFlag) {
        this.tindakLanjutFlag = tindakLanjutFlag;
    }

}