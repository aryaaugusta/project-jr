package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the KORLANTAS_LOG_FAILED database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_LOG_FAILED")
@NamedQuery(name="KorlantasLogFailed.findAll", query="SELECT k FROM KorlantasLogFailed k")
public class KorlantasLogFailed implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="ID_DT")
	private String idDt;

	@Column(name="PARAMS_ID_VAL")
	private String paramsIdVal;

	@Column(name="PARAMS_IDPARENT_VAL")
	private String paramsIdparentVal;

	@Column(name="PARAMS_TABLE")
	private String paramsTable;

	public KorlantasLogFailed() {
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdDt() {
		return this.idDt;
	}

	public void setIdDt(String idDt) {
		this.idDt = idDt;
	}

	public String getParamsIdVal() {
		return this.paramsIdVal;
	}

	public void setParamsIdVal(String paramsIdVal) {
		this.paramsIdVal = paramsIdVal;
	}

	public String getParamsIdparentVal() {
		return this.paramsIdparentVal;
	}

	public void setParamsIdparentVal(String paramsIdparentVal) {
		this.paramsIdparentVal = paramsIdparentVal;
	}

	public String getParamsTable() {
		return this.paramsTable;
	}

	public void setParamsTable(String paramsTable) {
		this.paramsTable = paramsTable;
	}

}