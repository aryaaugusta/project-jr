package core.model;

import java.io.Serializable;

public class AuthRsPicPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String kodeRumahsakit;
	private String userLogin;

	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeRumahsakit == null) ? 0 : kodeRumahsakit.hashCode());
		result = prime * result
				+ ((userLogin == null) ? 0 : userLogin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AuthRsPicPK other = (AuthRsPicPK) obj;
		if (kodeRumahsakit == null) {
			if (other.kodeRumahsakit != null)
				return false;
		} else if (!kodeRumahsakit.equals(other.kodeRumahsakit))
			return false;
		if (userLogin == null) {
			if (other.userLogin != null)
				return false;
		} else if (!userLogin.equals(other.userLogin))
			return false;
		return true;
	}
}
