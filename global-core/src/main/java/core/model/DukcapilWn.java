package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DUKCAPIL_WN database table.
 * 
 */
@Entity
@Table(name="DUKCAPIL_WN")
@NamedQuery(name="DukcapilWn.findAll", query="SELECT d FROM DukcapilWn d")
public class DukcapilWn implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private String nik;

	private String agama;

	private String alamat;

	@Column(name="ALAMAT_DUSUN")
	private String alamatDusun;

	@Column(name="ALAMAT_KODEPOS")
	private String alamatKodepos;

	@Column(name="ALAMAT_RT")
	private String alamatRt;

	@Column(name="ALAMAT_RW")
	private String alamatRw;

	@Temporal(TemporalType.DATE)
	@Column(name="EKTP_CREATED")
	private Date ektpCreated;

	@Column(name="EKTP_LOCALID")
	private String ektpLocalid;

	@Column(name="EKTP_STATUS")
	private String ektpStatus;

	@Column(name="GOL_DARAH")
	private String golDarah;

	@Column(name="JNS_KELAMIN")
	private String jnsKelamin;

	@Column(name="KODE_KABUPATEN")
	private String kodeKabupaten;

	@Column(name="KODE_KECAMATAN")
	private String kodeKecamatan;

	@Column(name="KODE_KELURAHAN")
	private String kodeKelurahan;

	@Column(name="KODE_PEKERJAAN")
	private String kodePekerjaan;

	@Column(name="KODE_PROPINSI")
	private String kodePropinsi;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_SYNC_DATE")
	private Date lastSyncDate;

	@Column(name="NAMA_AYAH")
	private String namaAyah;

	@Column(name="NAMA_IBU")
	private String namaIbu;

	@Column(name="NAMA_LENGKAP")
	private String namaLengkap;

	@Column(name="NO_KK")
	private String noKk;

	@Column(name="PEND_AKHIR")
	private String pendAkhir;

	@Column(name="STATUS_NIKAH")
	private String statusNikah;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_LAHIR")
	private Date tglLahir;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_NIKAH")
	private Date tglNikah;

	@Column(name="TMP_LAHIR")
	private String tmpLahir;

	public DukcapilWn() {
	}

	public String getNik() {
		return this.nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getAgama() {
		return this.agama;
	}

	public void setAgama(String agama) {
		this.agama = agama;
	}

	public String getAlamat() {
		return this.alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getAlamatDusun() {
		return this.alamatDusun;
	}

	public void setAlamatDusun(String alamatDusun) {
		this.alamatDusun = alamatDusun;
	}

	public String getAlamatKodepos() {
		return this.alamatKodepos;
	}

	public void setAlamatKodepos(String alamatKodepos) {
		this.alamatKodepos = alamatKodepos;
	}

	public String getAlamatRt() {
		return this.alamatRt;
	}

	public void setAlamatRt(String alamatRt) {
		this.alamatRt = alamatRt;
	}

	public String getAlamatRw() {
		return this.alamatRw;
	}

	public void setAlamatRw(String alamatRw) {
		this.alamatRw = alamatRw;
	}

	public Date getEktpCreated() {
		return this.ektpCreated;
	}

	public void setEktpCreated(Date ektpCreated) {
		this.ektpCreated = ektpCreated;
	}

	public String getEktpLocalid() {
		return this.ektpLocalid;
	}

	public void setEktpLocalid(String ektpLocalid) {
		this.ektpLocalid = ektpLocalid;
	}

	public String getEktpStatus() {
		return this.ektpStatus;
	}

	public void setEktpStatus(String ektpStatus) {
		this.ektpStatus = ektpStatus;
	}

	public String getGolDarah() {
		return this.golDarah;
	}

	public void setGolDarah(String golDarah) {
		this.golDarah = golDarah;
	}

	public String getJnsKelamin() {
		return this.jnsKelamin;
	}

	public void setJnsKelamin(String jnsKelamin) {
		this.jnsKelamin = jnsKelamin;
	}

	public String getKodeKabupaten() {
		return this.kodeKabupaten;
	}

	public void setKodeKabupaten(String kodeKabupaten) {
		this.kodeKabupaten = kodeKabupaten;
	}

	public String getKodeKecamatan() {
		return this.kodeKecamatan;
	}

	public void setKodeKecamatan(String kodeKecamatan) {
		this.kodeKecamatan = kodeKecamatan;
	}

	public String getKodeKelurahan() {
		return this.kodeKelurahan;
	}

	public void setKodeKelurahan(String kodeKelurahan) {
		this.kodeKelurahan = kodeKelurahan;
	}

	public String getKodePekerjaan() {
		return this.kodePekerjaan;
	}

	public void setKodePekerjaan(String kodePekerjaan) {
		this.kodePekerjaan = kodePekerjaan;
	}

	public String getKodePropinsi() {
		return this.kodePropinsi;
	}

	public void setKodePropinsi(String kodePropinsi) {
		this.kodePropinsi = kodePropinsi;
	}

	public Date getLastSyncDate() {
		return this.lastSyncDate;
	}

	public void setLastSyncDate(Date lastSyncDate) {
		this.lastSyncDate = lastSyncDate;
	}

	public String getNamaAyah() {
		return this.namaAyah;
	}

	public void setNamaAyah(String namaAyah) {
		this.namaAyah = namaAyah;
	}

	public String getNamaIbu() {
		return this.namaIbu;
	}

	public void setNamaIbu(String namaIbu) {
		this.namaIbu = namaIbu;
	}

	public String getNamaLengkap() {
		return this.namaLengkap;
	}

	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}

	public String getNoKk() {
		return this.noKk;
	}

	public void setNoKk(String noKk) {
		this.noKk = noKk;
	}

	public String getPendAkhir() {
		return this.pendAkhir;
	}

	public void setPendAkhir(String pendAkhir) {
		this.pendAkhir = pendAkhir;
	}

	public String getStatusNikah() {
		return this.statusNikah;
	}

	public void setStatusNikah(String statusNikah) {
		this.statusNikah = statusNikah;
	}

	public Date getTglLahir() {
		return this.tglLahir;
	}

	public void setTglLahir(Date tglLahir) {
		this.tglLahir = tglLahir;
	}

	public Date getTglNikah() {
		return this.tglNikah;
	}

	public void setTglNikah(Date tglNikah) {
		this.tglNikah = tglNikah;
	}

	public String getTmpLahir() {
		return this.tmpLahir;
	}

	public void setTmpLahir(String tmpLahir) {
		this.tmpLahir = tmpLahir;
	}

}