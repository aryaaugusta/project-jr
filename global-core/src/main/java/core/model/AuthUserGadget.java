package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the AUTH_USER_GADGET database table.
 * 
 */
@Entity
@Table(name="AUTH_USER_GADGET")
@NamedQuery(name="AuthUserGadget.findAll", query="SELECT a FROM AuthUserGadget a")
public class AuthUserGadget implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="G_NUMBER")
	private String gNumber;

	@Column(name="G_SERIES")
	private String gSeries;

	@Column(name="G_STATUS")
	private String gStatus;

	private String keterangan;

	@Column(name="UPDATED_BY")
	private String updatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@Column(name="USER_LOGIN")
	private String userLogin;

	@Column(name="USR_ID")
	private String usrId;

	public AuthUserGadget() {
	}

	public String getGNumber() {
		return this.gNumber;
	}

	public void setGNumber(String gNumber) {
		this.gNumber = gNumber;
	}

	public String getGSeries() {
		return this.gSeries;
	}

	public void setGSeries(String gSeries) {
		this.gSeries = gSeries;
	}

	public String getGStatus() {
		return this.gStatus;
	}

	public void setGStatus(String gStatus) {
		this.gStatus = gStatus;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getUserLogin() {
		return this.userLogin;
	}

	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	public String getUsrId() {
		return this.usrId;
	}

	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}

}