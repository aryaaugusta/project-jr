package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_JAMINAN database table.
 * 
 */
@Entity
@Table(name="PL_JAMINAN")
@NamedQuery(name="PlJaminan.findAll", query="SELECT p FROM PlJaminan p")
public class PlJaminan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KODE_JAMINAN")
	private String kodeJaminan;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private String deskripsi;

	@Column(name="KODE_LAPORAN")
	private String kodeLaporan;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="LINGKUP_JAMINAN")
	private String lingkupJaminan;

	public PlJaminan() {
	}

	public String getKodeJaminan() {
		return this.kodeJaminan;
	}

	public void setKodeJaminan(String kodeJaminan) {
		this.kodeJaminan = kodeJaminan;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDeskripsi() {
		return this.deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getKodeLaporan() {
		return this.kodeLaporan;
	}

	public void setKodeLaporan(String kodeLaporan) {
		this.kodeLaporan = kodeLaporan;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLingkupJaminan() {
		return this.lingkupJaminan;
	}

	public void setLingkupJaminan(String lingkupJaminan) {
		this.lingkupJaminan = lingkupJaminan;
	}

}