package core.model;

import java.io.Serializable;


public class PlTindakLanjutPK implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idTlRegister;
	private String noRegister;
	public String getIdTlRegister() {
		return idTlRegister;
	}
	public void setIdTlRegister(String idTlRegister) {
		this.idTlRegister = idTlRegister;
	}
	public String getNoRegister() {
		return noRegister;
	}
	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idTlRegister == null) ? 0 : idTlRegister.hashCode());
		result = prime * result
				+ ((noRegister == null) ? 0 : noRegister.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlTindakLanjutPK other = (PlTindakLanjutPK) obj;
		if (idTlRegister == null) {
			if (other.idTlRegister != null)
				return false;
		} else if (!idTlRegister.equals(other.idTlRegister))
			return false;
		if (noRegister == null) {
			if (other.noRegister != null)
				return false;
		} else if (!noRegister.equals(other.noRegister))
			return false;
		return true;
	}
}
