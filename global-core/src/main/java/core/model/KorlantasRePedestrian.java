package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_RE_PEDESTRIANS database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_RE_PEDESTRIANS")
@NamedQuery(name="KorlantasRePedestrian.findAll", query="SELECT k FROM KorlantasRePedestrian k")
public class KorlantasRePedestrian implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ACCIDENT_ID")
	private BigDecimal accidentId;

	private BigDecimal age;

	private BigDecimal arrested;

	@Column(name="EDUCATION_ID")
	private BigDecimal educationId;

	@Column(name="FIRST_NAME")
	private String firstName;

	private BigDecimal foreigner;

	@Column(name="HIT_BY_CAR_NO_SEQUENCE")
	private BigDecimal hitByCarNoSequence;

	@Id
	private BigDecimal id;

	@Column(name="\"IDENTITY\"")
	private String identity;

	@Column(name="IDENTITY_TYPE_ID")
	private BigDecimal identityTypeId;

	@Column(name="INJURY_ID")
	private BigDecimal injuryId;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="NATIONALITY_ID")
	private BigDecimal nationalityId;

	@Column(name="PEDESTRIAN_MOVEMENT_ID")
	private BigDecimal pedestrianMovementId;

	@Column(name="PROFESSION_ID")
	private BigDecimal professionId;

	@Column(name="RELIGION_ID")
	private BigDecimal religionId;

	@Column(name="SCHEMA_NO")
	private BigDecimal schemaNo;

	@Column(name="SCHOOL_KID")
	private BigDecimal schoolKid;

	private BigDecimal sex;

	@Column(name="\"STATE\"")
	private BigDecimal state;

	@Column(name="\"STATEMENT\"")
	private String statement;

	public KorlantasRePedestrian() {
	}

	public BigDecimal getAccidentId() {
		return this.accidentId;
	}

	public void setAccidentId(BigDecimal accidentId) {
		this.accidentId = accidentId;
	}

	public BigDecimal getAge() {
		return this.age;
	}

	public void setAge(BigDecimal age) {
		this.age = age;
	}

	public BigDecimal getArrested() {
		return this.arrested;
	}

	public void setArrested(BigDecimal arrested) {
		this.arrested = arrested;
	}

	public BigDecimal getEducationId() {
		return this.educationId;
	}

	public void setEducationId(BigDecimal educationId) {
		this.educationId = educationId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public BigDecimal getForeigner() {
		return this.foreigner;
	}

	public void setForeigner(BigDecimal foreigner) {
		this.foreigner = foreigner;
	}

	public BigDecimal getHitByCarNoSequence() {
		return this.hitByCarNoSequence;
	}

	public void setHitByCarNoSequence(BigDecimal hitByCarNoSequence) {
		this.hitByCarNoSequence = hitByCarNoSequence;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getIdentity() {
		return this.identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public BigDecimal getIdentityTypeId() {
		return this.identityTypeId;
	}

	public void setIdentityTypeId(BigDecimal identityTypeId) {
		this.identityTypeId = identityTypeId;
	}

	public BigDecimal getInjuryId() {
		return this.injuryId;
	}

	public void setInjuryId(BigDecimal injuryId) {
		this.injuryId = injuryId;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public BigDecimal getNationalityId() {
		return this.nationalityId;
	}

	public void setNationalityId(BigDecimal nationalityId) {
		this.nationalityId = nationalityId;
	}

	public BigDecimal getPedestrianMovementId() {
		return this.pedestrianMovementId;
	}

	public void setPedestrianMovementId(BigDecimal pedestrianMovementId) {
		this.pedestrianMovementId = pedestrianMovementId;
	}

	public BigDecimal getProfessionId() {
		return this.professionId;
	}

	public void setProfessionId(BigDecimal professionId) {
		this.professionId = professionId;
	}

	public BigDecimal getReligionId() {
		return this.religionId;
	}

	public void setReligionId(BigDecimal religionId) {
		this.religionId = religionId;
	}

	public BigDecimal getSchemaNo() {
		return this.schemaNo;
	}

	public void setSchemaNo(BigDecimal schemaNo) {
		this.schemaNo = schemaNo;
	}

	public BigDecimal getSchoolKid() {
		return this.schoolKid;
	}

	public void setSchoolKid(BigDecimal schoolKid) {
		this.schoolKid = schoolKid;
	}

	public BigDecimal getSex() {
		return this.sex;
	}

	public void setSex(BigDecimal sex) {
		this.sex = sex;
	}

	public BigDecimal getState() {
		return this.state;
	}

	public void setState(BigDecimal state) {
		this.state = state;
	}

	public String getStatement() {
		return this.statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

}