package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FND_JENIS_KENDARAAN database table.
 * 
 */
@Entity
@Table(name="FND_JENIS_KENDARAAN")
@NamedQuery(name="FndJenisKendaraan.findAll", query="SELECT f FROM FndJenisKendaraan f")
public class FndJenisKendaraan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KODE_JENIS")
	private String kodeJenis;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private String deskripsi;

	@Column(name="JNS_KEND")
	private String jnsKend;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	public FndJenisKendaraan() {
	}

	public String getKodeJenis() {
		return this.kodeJenis;
	}

	public void setKodeJenis(String kodeJenis) {
		this.kodeJenis = kodeJenis;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDeskripsi() {
		return this.deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getJnsKend() {
		return this.jnsKend;
	}

	public void setJnsKend(String jnsKend) {
		this.jnsKend = jnsKend;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

}