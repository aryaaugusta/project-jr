package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the GL_VOUCHER_LINE database table.
 * 
 */
@Entity
@Table(name="GL_VOUCHER_LINE")
@NamedQuery(name="GlVoucherLine.findAll", query="SELECT g FROM GlVoucherLine g")
@IdClass(GlVoucherLinePK.class)
public class GlVoucherLine implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_VOUCHER")
	private String idVoucher;

	@Id
	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Id
	@Column(name="KODE_AKUN", insertable=false, updatable=false)
	private String kodeAkun;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private BigDecimal debit;

	@Column(name="ID_VOUCHER_LINES")
	private String idVoucherLines;

	private String keterangan;

	private BigDecimal kredit;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NO_URUT")
	private BigDecimal noUrut;

	public GlVoucherLine() {
	}


	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public BigDecimal getDebit() {
		return this.debit;
	}

	public void setDebit(BigDecimal debit) {
		this.debit = debit;
	}

	public String getIdVoucherLines() {
		return this.idVoucherLines;
	}

	public void setIdVoucherLines(String idVoucherLines) {
		this.idVoucherLines = idVoucherLines;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public BigDecimal getKredit() {
		return this.kredit;
	}

	public void setKredit(BigDecimal kredit) {
		this.kredit = kredit;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public BigDecimal getNoUrut() {
		return this.noUrut;
	}

	public void setNoUrut(BigDecimal noUrut) {
		this.noUrut = noUrut;
	}


	public String getIdVoucher() {
		return idVoucher;
	}


	public void setIdVoucher(String idVoucher) {
		this.idVoucher = idVoucher;
	}


	public String getKodeKantorJr() {
		return kodeKantorJr;
	}


	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}


	public String getKodeAkun() {
		return kodeAkun;
	}


	public void setKodeAkun(String kodeAkun) {
		this.kodeAkun = kodeAkun;
	}

}