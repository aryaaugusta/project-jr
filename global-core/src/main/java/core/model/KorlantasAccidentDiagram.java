package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_ACCIDENT_DIAGRAM database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_ACCIDENT_DIAGRAM")
@NamedQuery(name="KorlantasAccidentDiagram.findAll", query="SELECT k FROM KorlantasAccidentDiagram k")
public class KorlantasAccidentDiagram implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal archived;

	@Column(name="DASI_ID")
	private String dasiId;

	@Column(name="FILE_NAME")
	private String fileName;

	@Id
	private String id;

	@Column(name="MAP_SCHEMA")
	private BigDecimal mapSchema;

	private String name;

	@Column(name="SORT_ORDER")
	private BigDecimal sortOrder;

	public KorlantasAccidentDiagram() {
	}

	public BigDecimal getArchived() {
		return this.archived;
	}

	public void setArchived(BigDecimal archived) {
		this.archived = archived;
	}

	public String getDasiId() {
		return this.dasiId;
	}

	public void setDasiId(String dasiId) {
		this.dasiId = dasiId;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public BigDecimal getMapSchema() {
		return this.mapSchema;
	}

	public void setMapSchema(BigDecimal mapSchema) {
		this.mapSchema = mapSchema;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}