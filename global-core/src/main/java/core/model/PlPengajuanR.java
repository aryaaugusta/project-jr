package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_PENGAJUAN_RS database table.
 * 
 */
@Entity
@Table(name="PL_PENGAJUAN_RS")
@NamedQuery(name="PlPengajuanR.findAll", query="SELECT p FROM PlPengajuanR p")
public class PlPengajuanR implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_PENGAJUAN_RS")
	private String idPengajuanRs;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="ID_GUID")
	private String idGuid;

	@Column(name="JUMLAH_PENGAJUAN_LUKALUKA")
	private BigDecimal jumlahPengajuanLukaluka;

	@Column(name="KODE_RUMAHSAKIT")
	private String kodeRumahsakit;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	public PlPengajuanR() {
	}

	public String getIdPengajuanRs() {
		return this.idPengajuanRs;
	}

	public void setIdPengajuanRs(String idPengajuanRs) {
		this.idPengajuanRs = idPengajuanRs;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdGuid() {
		return this.idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public BigDecimal getJumlahPengajuanLukaluka() {
		return this.jumlahPengajuanLukaluka;
	}

	public void setJumlahPengajuanLukaluka(BigDecimal jumlahPengajuanLukaluka) {
		this.jumlahPengajuanLukaluka = jumlahPengajuanLukaluka;
	}

	public String getKodeRumahsakit() {
		return this.kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

}