package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the AUTH_ELEMENT database table.
 * 
 */
@Entity
@Table(name="AUTH_ELEMENT")
@NamedQuery(name="AuthElement.findAll", query="SELECT a FROM AuthElement a")
public class AuthElement implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ELEMENT_CODE")
	private String elementCode;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	private String description;

	@Column(name="E_STATUS_TRF")
	private String eStatusTrf;

	@Column(name="ELEMENT_HEADER")
	private String elementHeader;

	@Column(name="ELEMENT_ID")
	private String elementId;

	@Column(name="ELEMENT_LINK")
	private String elementLink;

	@Column(name="ELEMENT_LINK_R")
	private String elementLinkR;

	@Column(name="ELEMENT_ORDER")
	private BigDecimal elementOrder;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	public AuthElement() {
	}

	public String getElementCode() {
		return this.elementCode;
	}

	public void setElementCode(String elementCode) {
		this.elementCode = elementCode;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEStatusTrf() {
		return this.eStatusTrf;
	}

	public void setEStatusTrf(String eStatusTrf) {
		this.eStatusTrf = eStatusTrf;
	}

	public String getElementHeader() {
		return this.elementHeader;
	}

	public void setElementHeader(String elementHeader) {
		this.elementHeader = elementHeader;
	}

	public String getElementId() {
		return this.elementId;
	}

	public void setElementId(String elementId) {
		this.elementId = elementId;
	}

	public String getElementLink() {
		return this.elementLink;
	}

	public void setElementLink(String elementLink) {
		this.elementLink = elementLink;
	}

	public String getElementLinkR() {
		return this.elementLinkR;
	}

	public void setElementLinkR(String elementLinkR) {
		this.elementLinkR = elementLinkR;
	}

	public BigDecimal getElementOrder() {
		return this.elementOrder;
	}

	public void setElementOrder(BigDecimal elementOrder) {
		this.elementOrder = elementOrder;
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

}