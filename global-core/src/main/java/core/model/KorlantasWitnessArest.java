package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_WITNESS_AREST database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_WITNESS_AREST")
@NamedQuery(name="KorlantasWitnessArest.findAll", query="SELECT k FROM KorlantasWitnessArest k")
public class KorlantasWitnessArest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="ACCIDENT_ID")
	private BigDecimal accidentId;

	private String address;

	private BigDecimal age;

	private BigDecimal arrested;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="\"IDENTITY\"")
	private String identity;

	@Column(name="IDENTITY_TYPE_ID")
	private BigDecimal identityTypeId;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="PROFESSION_ID")
	private BigDecimal professionId;

	@Column(name="RELIGION_ID")
	private BigDecimal religionId;

	private BigDecimal sex;

	@Column(name="\"STATE\"")
	private BigDecimal state;

	@Column(name="\"STATEMENT\"")
	private String statement;

	public KorlantasWitnessArest() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getAccidentId() {
		return this.accidentId;
	}

	public void setAccidentId(BigDecimal accidentId) {
		this.accidentId = accidentId;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BigDecimal getAge() {
		return this.age;
	}

	public void setAge(BigDecimal age) {
		this.age = age;
	}

	public BigDecimal getArrested() {
		return this.arrested;
	}

	public void setArrested(BigDecimal arrested) {
		this.arrested = arrested;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getIdentity() {
		return this.identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public BigDecimal getIdentityTypeId() {
		return this.identityTypeId;
	}

	public void setIdentityTypeId(BigDecimal identityTypeId) {
		this.identityTypeId = identityTypeId;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public BigDecimal getProfessionId() {
		return this.professionId;
	}

	public void setProfessionId(BigDecimal professionId) {
		this.professionId = professionId;
	}

	public BigDecimal getReligionId() {
		return this.religionId;
	}

	public void setReligionId(BigDecimal religionId) {
		this.religionId = religionId;
	}

	public BigDecimal getSex() {
		return this.sex;
	}

	public void setSex(BigDecimal sex) {
		this.sex = sex;
	}

	public BigDecimal getState() {
		return this.state;
	}

	public void setState(BigDecimal state) {
		this.state = state;
	}

	public String getStatement() {
		return this.statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

}