package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_LIGHTS database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_LIGHTS")
@NamedQuery(name="KorlantasLight.findAll", query="SELECT k FROM KorlantasLight k")
public class KorlantasLight implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal archived;

	@Id
	private BigDecimal id;

	private String name;

	@Column(name="SORT_ORDER")
	private BigDecimal sortOrder;

	public KorlantasLight() {
	}

	public BigDecimal getArchived() {
		return this.archived;
	}

	public void setArchived(BigDecimal archived) {
		this.archived = archived;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}