package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the GL_SALDO database table.
 * 
 */
public class GlSaldoPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="NAMA_PERIODE", insertable=false, updatable=false)
	private String namaPeriode;

	private String ccid;

	public GlSaldoPK() {
	}
	public String getNamaPeriode() {
		return this.namaPeriode;
	}
	public void setNamaPeriode(String namaPeriode) {
		this.namaPeriode = namaPeriode;
	}
	public String getCcid() {
		return this.ccid;
	}
	public void setCcid(String ccid) {
		this.ccid = ccid;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof GlSaldoPK)) {
			return false;
		}
		GlSaldoPK castOther = (GlSaldoPK)other;
		return 
			this.namaPeriode.equals(castOther.namaPeriode)
			&& this.ccid.equals(castOther.ccid);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.namaPeriode.hashCode();
		hash = hash * prime + this.ccid.hashCode();
		
		return hash;
	}
}