package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_KORBAN_RS_DBL database table.
 * 
 */
@Entity
@Table(name="PL_KORBAN_RS_DBL")
@NamedQuery(name="PlKorbanRsDbl.findAll", query="SELECT p FROM PlKorbanRsDbl p")
public class PlKorbanRsDbl implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KODE_KEJADIAN")
	private String kodeKejadian;

	@Column(name="KODE_KEJADIAN_02")
	private String kodeKejadian02;

	@Column(name="KODE_RS")
	private String kodeRs;

	@Column(name="KODE_RS_02")
	private String kodeRs02;

	public PlKorbanRsDbl() {
	}

	public String getKodeKejadian() {
		return this.kodeKejadian;
	}

	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}

	public String getKodeKejadian02() {
		return this.kodeKejadian02;
	}

	public void setKodeKejadian02(String kodeKejadian02) {
		this.kodeKejadian02 = kodeKejadian02;
	}

	public String getKodeRs() {
		return this.kodeRs;
	}

	public void setKodeRs(String kodeRs) {
		this.kodeRs = kodeRs;
	}

	public String getKodeRs02() {
		return this.kodeRs02;
	}

	public void setKodeRs02(String kodeRs02) {
		this.kodeRs02 = kodeRs02;
	}

}