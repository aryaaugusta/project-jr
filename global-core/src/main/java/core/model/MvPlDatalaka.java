package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the MV_PL_DATALAKA database table.
 * 
 */
@Entity
@Table(name="MV_PL_DATALAKA")
@NamedQuery(name="MvPlDatalaka.findAll", query="SELECT m FROM MvPlDatalaka m")
public class MvPlDatalaka implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ALAMAT_KORBAN")
	private String alamatKorban;

	@Column(name="DESC_TL")
	private String descTl;

	@Column(name="DESKRIPSI_LOKASI")
	private String deskripsiLokasi;

	@Column(name="INSTANSI_MENANGANI")
	private String instansiMenangani;

	@Column(name="JENIS_KELAMIN")
	private String jenisKelamin;

	@Column(name="KASUS_KECELAKAAN")
	private String kasusKecelakaan;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Id
	@Column(name="KODE_TL")
	private String kodeTl;

	@Column(name="LOKASI_LAKA")
	private String lokasiLaka;

	@Column(name="NAMA_KORBAN")
	private String namaKorban;

	@Column(name="NAMA_PETUGAS")
	private String namaPetugas;

	@Column(name="NO_LAPORAN_POLISI")
	private String noLaporanPolisi;

	@Column(name="SIFAT_CIDERA")
	private String sifatCidera;

	@Column(name="SIFAT_KECELAKAAN")
	private String sifatKecelakaan;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_KEJADIAN")
	private Date tglKejadian;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_LAPORAN_POLISI")
	private Date tglLaporanPolisi;

	private String ucode;

	@Column(name="UMUR_KORBAN")
	private String umurKorban;

	public MvPlDatalaka() {
	}

	public String getAlamatKorban() {
		return this.alamatKorban;
	}

	public void setAlamatKorban(String alamatKorban) {
		this.alamatKorban = alamatKorban;
	}

	public String getDescTl() {
		return this.descTl;
	}

	public void setDescTl(String descTl) {
		this.descTl = descTl;
	}

	public String getDeskripsiLokasi() {
		return this.deskripsiLokasi;
	}

	public void setDeskripsiLokasi(String deskripsiLokasi) {
		this.deskripsiLokasi = deskripsiLokasi;
	}

	public String getInstansiMenangani() {
		return this.instansiMenangani;
	}

	public void setInstansiMenangani(String instansiMenangani) {
		this.instansiMenangani = instansiMenangani;
	}

	public String getJenisKelamin() {
		return this.jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getKasusKecelakaan() {
		return this.kasusKecelakaan;
	}

	public void setKasusKecelakaan(String kasusKecelakaan) {
		this.kasusKecelakaan = kasusKecelakaan;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeTl() {
		return this.kodeTl;
	}

	public void setKodeTl(String kodeTl) {
		this.kodeTl = kodeTl;
	}

	public String getLokasiLaka() {
		return this.lokasiLaka;
	}

	public void setLokasiLaka(String lokasiLaka) {
		this.lokasiLaka = lokasiLaka;
	}

	public String getNamaKorban() {
		return this.namaKorban;
	}

	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}

	public String getNamaPetugas() {
		return this.namaPetugas;
	}

	public void setNamaPetugas(String namaPetugas) {
		this.namaPetugas = namaPetugas;
	}

	public String getNoLaporanPolisi() {
		return this.noLaporanPolisi;
	}

	public void setNoLaporanPolisi(String noLaporanPolisi) {
		this.noLaporanPolisi = noLaporanPolisi;
	}

	public String getSifatCidera() {
		return this.sifatCidera;
	}

	public void setSifatCidera(String sifatCidera) {
		this.sifatCidera = sifatCidera;
	}

	public String getSifatKecelakaan() {
		return this.sifatKecelakaan;
	}

	public void setSifatKecelakaan(String sifatKecelakaan) {
		this.sifatKecelakaan = sifatKecelakaan;
	}

	public Date getTglKejadian() {
		return this.tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public Date getTglLaporanPolisi() {
		return this.tglLaporanPolisi;
	}

	public void setTglLaporanPolisi(Date tglLaporanPolisi) {
		this.tglLaporanPolisi = tglLaporanPolisi;
	}

	public String getUcode() {
		return this.ucode;
	}

	public void setUcode(String ucode) {
		this.ucode = ucode;
	}

	public String getUmurKorban() {
		return this.umurKorban;
	}

	public void setUmurKorban(String umurKorban) {
		this.umurKorban = umurKorban;
	}

}