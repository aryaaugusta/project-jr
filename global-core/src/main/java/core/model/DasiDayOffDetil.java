package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DASI_DAY_OFF_DETIL database table.
 * 
 */
@Entity
@Table(name="DASI_DAY_OFF_DETIL")
@NamedQuery(name="DasiDayOffDetil.findAll", query="SELECT d FROM DasiDayOffDetil d")
public class DasiDayOffDetil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_OFF_DETIL")
	private String idOffDetil;

	@Column(name="ID_OFF")
	private String idOff;

	@Temporal(TemporalType.DATE)
	private Date tanggal;

	public DasiDayOffDetil() {
	}

	public String getIdOffDetil() {
		return this.idOffDetil;
	}

	public void setIdOffDetil(String idOffDetil) {
		this.idOffDetil = idOffDetil;
	}

	public String getIdOff() {
		return this.idOff;
	}

	public void setIdOff(String idOff) {
		this.idOff = idOff;
	}

	public Date getTanggal() {
		return this.tanggal;
	}

	public void setTanggal(Date tanggal) {
		this.tanggal = tanggal;
	}

}