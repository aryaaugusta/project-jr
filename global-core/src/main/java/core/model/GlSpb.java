package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the GL_SPB database table.
 * 
 */
@Entity
@Table(name="GL_SPB")
@NamedQuery(name="GlSpb.findAll", query="SELECT g FROM GlSpb g")
public class GlSpb implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NO_SPB")
	private String noSpb;

	@Column(name="ID_TELLER")
	private String idTeller;

	@Column(name="KODE_CABANG")
	private String kodeCabang;

	private String kontak;

	@Column(name="KOTA_BRI")
	private String kotaBri;

	private BigDecimal penarikan;

	private String status;

	@Column(name="STATUS_BRI")
	private String statusBri;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_SUBMIT")
	private Date tglSubmit;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_TRANSAKSI")
	private Date tglTransaksi;

	public GlSpb() {
	}

	public String getNoSpb() {
		return this.noSpb;
	}

	public void setNoSpb(String noSpb) {
		this.noSpb = noSpb;
	}

	public String getIdTeller() {
		return this.idTeller;
	}

	public void setIdTeller(String idTeller) {
		this.idTeller = idTeller;
	}

	public String getKodeCabang() {
		return this.kodeCabang;
	}

	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}

	public String getKontak() {
		return this.kontak;
	}

	public void setKontak(String kontak) {
		this.kontak = kontak;
	}

	public String getKotaBri() {
		return this.kotaBri;
	}

	public void setKotaBri(String kotaBri) {
		this.kotaBri = kotaBri;
	}

	public BigDecimal getPenarikan() {
		return this.penarikan;
	}

	public void setPenarikan(BigDecimal penarikan) {
		this.penarikan = penarikan;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusBri() {
		return this.statusBri;
	}

	public void setStatusBri(String statusBri) {
		this.statusBri = statusBri;
	}

	public Date getTglSubmit() {
		return this.tglSubmit;
	}

	public void setTglSubmit(Date tglSubmit) {
		this.tglSubmit = tglSubmit;
	}

	public Date getTglTransaksi() {
		return this.tglTransaksi;
	}

	public void setTglTransaksi(Date tglTransaksi) {
		this.tglTransaksi = tglTransaksi;
	}

}