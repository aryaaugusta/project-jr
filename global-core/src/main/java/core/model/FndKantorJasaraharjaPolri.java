package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FND_KANTOR_JASARAHARJA_POLRI database table.
 * 
 */
@Entity
@Table(name="FND_KANTOR_JASARAHARJA_POLRI")
@NamedQuery(name="FndKantorJasaraharjaPolri.findAll", query="SELECT f FROM FndKantorJasaraharjaPolri f")
public class FndKantorJasaraharjaPolri implements Serializable {
	private static final long serialVersionUID = 1L;

	private String alamat1;

	private String alamat2;

	@Column(name="BERINDUK_KE_KODE_KANTOR")
	private String berindukKeKodeKantor;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	@Column(name="GOLONGAN_KANTOR")
	private String golonganKantor;

	private String jenis;

	@Id
	@Column(name="KODE_KANTOR")
	private String kodeKantor;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="LEVEL_KANTOR")
	private String levelKantor;

	private String nama;

	public FndKantorJasaraharjaPolri() {
	}

	public String getAlamat1() {
		return this.alamat1;
	}

	public void setAlamat1(String alamat1) {
		this.alamat1 = alamat1;
	}

	public String getAlamat2() {
		return this.alamat2;
	}

	public void setAlamat2(String alamat2) {
		this.alamat2 = alamat2;
	}

	public String getBerindukKeKodeKantor() {
		return this.berindukKeKodeKantor;
	}

	public void setBerindukKeKodeKantor(String berindukKeKodeKantor) {
		this.berindukKeKodeKantor = berindukKeKodeKantor;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getGolonganKantor() {
		return this.golonganKantor;
	}

	public void setGolonganKantor(String golonganKantor) {
		this.golonganKantor = golonganKantor;
	}

	public String getJenis() {
		return this.jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public String getKodeKantor() {
		return this.kodeKantor;
	}

	public void setKodeKantor(String kodeKantor) {
		this.kodeKantor = kodeKantor;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLevelKantor() {
		return this.levelKantor;
	}

	public void setLevelKantor(String levelKantor) {
		this.levelKantor = levelKantor;
	}

	public String getNama() {
		return this.nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

}