package core.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class KorlantasProvincePK implements Serializable{

	private static final long serialVersionUID = 1L;
	private BigDecimal id;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	
}
