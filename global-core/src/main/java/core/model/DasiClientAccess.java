package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the DASI_CLIENT_ACCESS database table.
 * 
 */
@Entity
@Table(name="DASI_CLIENT_ACCESS")
@NamedQuery(name="DasiClientAccess.findAll", query="SELECT d FROM DasiClientAccess d")
public class DasiClientAccess implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KODE_CLIENT_ACCESS")
	private String kodeClientAccess;

	@Column(name="ACC_LIMIT")
	private BigDecimal accLimit;

	@Column(name="APICODE_CLIENT_ACCESS")
	private String apicodeClientAccess;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="INTO_LOG")
	private String intoLog;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="MODUL_CLIENT_ACCESS")
	private String modulClientAccess;

	@Column(name="NAMA_CLIENT_ACCESS")
	private String namaClientAccess;

	@Column(name="STATUS_CLIENT_ACCESS")
	private String statusClientAccess;

	public DasiClientAccess() {
	}

	public String getKodeClientAccess() {
		return this.kodeClientAccess;
	}

	public void setKodeClientAccess(String kodeClientAccess) {
		this.kodeClientAccess = kodeClientAccess;
	}

	public BigDecimal getAccLimit() {
		return this.accLimit;
	}

	public void setAccLimit(BigDecimal accLimit) {
		this.accLimit = accLimit;
	}

	public String getApicodeClientAccess() {
		return this.apicodeClientAccess;
	}

	public void setApicodeClientAccess(String apicodeClientAccess) {
		this.apicodeClientAccess = apicodeClientAccess;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getIntoLog() {
		return this.intoLog;
	}

	public void setIntoLog(String intoLog) {
		this.intoLog = intoLog;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getModulClientAccess() {
		return this.modulClientAccess;
	}

	public void setModulClientAccess(String modulClientAccess) {
		this.modulClientAccess = modulClientAccess;
	}

	public String getNamaClientAccess() {
		return this.namaClientAccess;
	}

	public void setNamaClientAccess(String namaClientAccess) {
		this.namaClientAccess = namaClientAccess;
	}

	public String getStatusClientAccess() {
		return this.statusClientAccess;
	}

	public void setStatusClientAccess(String statusClientAccess) {
		this.statusClientAccess = statusClientAccess;
	}

}