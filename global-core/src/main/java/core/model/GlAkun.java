package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the GL_AKUN database table.
 * 
 */
@Entity
@Table(name="GL_AKUN")
@NamedQuery(name="GlAkun.findAll", query="SELECT g FROM GlAkun g")
public class GlAkun implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KODE_AKUN")
	private String kodeAkun;

	@Column(name="ALLOW_POSTING")
	private String allowPosting;

	@Column(name="ASSET_FLAG")
	private String assetFlag;

	@Column(name="BUDGET_FLAG")
	private String budgetFlag;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private String deskripsi;

	@Column(name="ENABLE_FLAG")
	private String enableFlag;

	@Column(name="GRUP_AKUN")
	private String grupAkun;

	@Column(name="ID_AKUN")
	private String idAkun;

	private String kode;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="SALDO_NORMAL")
	private String saldoNormal;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATE")
	private Date startDate;

	@Column(name="SUMMARY_FLAG")
	private String summaryFlag;

	public GlAkun() {
	}

	public String getKodeAkun() {
		return this.kodeAkun;
	}

	public void setKodeAkun(String kodeAkun) {
		this.kodeAkun = kodeAkun;
	}

	public String getAllowPosting() {
		return this.allowPosting;
	}

	public void setAllowPosting(String allowPosting) {
		this.allowPosting = allowPosting;
	}

	public String getAssetFlag() {
		return this.assetFlag;
	}

	public void setAssetFlag(String assetFlag) {
		this.assetFlag = assetFlag;
	}

	public String getBudgetFlag() {
		return this.budgetFlag;
	}

	public void setBudgetFlag(String budgetFlag) {
		this.budgetFlag = budgetFlag;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDeskripsi() {
		return this.deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getEnableFlag() {
		return this.enableFlag;
	}

	public void setEnableFlag(String enableFlag) {
		this.enableFlag = enableFlag;
	}

	public String getGrupAkun() {
		return this.grupAkun;
	}

	public void setGrupAkun(String grupAkun) {
		this.grupAkun = grupAkun;
	}

	public String getIdAkun() {
		return this.idAkun;
	}

	public void setIdAkun(String idAkun) {
		this.idAkun = idAkun;
	}

	public String getKode() {
		return this.kode;
	}

	public void setKode(String kode) {
		this.kode = kode;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getSaldoNormal() {
		return this.saldoNormal;
	}

	public void setSaldoNormal(String saldoNormal) {
		this.saldoNormal = saldoNormal;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getSummaryFlag() {
		return this.summaryFlag;
	}

	public void setSummaryFlag(String summaryFlag) {
		this.summaryFlag = summaryFlag;
	}

}