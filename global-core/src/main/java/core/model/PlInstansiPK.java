package core.model;

import java.io.Serializable;

public class PlInstansiPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String kodeInstansi;

	public String getKodeInstansi() {
		return kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeInstansi == null) ? 0 : kodeInstansi.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlInstansiPK other = (PlInstansiPK) obj;
		if (kodeInstansi == null) {
			if (other.kodeInstansi != null)
				return false;
		} else if (!kodeInstansi.equals(other.kodeInstansi))
			return false;
		return true;
	}
	

}
