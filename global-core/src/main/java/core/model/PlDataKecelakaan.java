package core.model;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Size;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_DATA_KECELAKAAN database table.
 * 
 */
@Entity
@Table(name="PL_DATA_KECELAKAAN")
@NamedQuery(name="PlDataKecelakaan.findAll", query="SELECT p FROM PlDataKecelakaan p")
public class PlDataKecelakaan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_KECELAKAAN")
	private String idKecelakaan;

	@Column(name="ASAL_BERKAS")
	private String asalBerkas;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="DESKRIPSI_KECELAKAAN")
	private String deskripsiKecelakaan;

	@Column(name="DESKRIPSI_LOKASI")
	private String deskripsiLokasi;

	@Column(name="GPS_LS")
	private String gpsLs;

	@Column(name="GPS_LU")
	private String gpsLu;

	@Column(name="ID_GUID")
	private String idGuid;

	@Column(name="JUMLAH_LUKA")
	private BigDecimal jumlahLuka;

	@Column(name="JUMLAH_MENINGGAL")
	private BigDecimal jumlahMeninggal;

	@Column(name="KODE_INSTANSI")
	private String kodeInstansi;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="KODE_KASUS_KECELAKAAN")
	private String kodeKasusKecelakaan;

	@Column(name="KODE_LINTASAN")
	private String kodeLintasan;

	@Column(name="KODE_LOKASI")
	private String kodeLokasi;

	@Column(name="KODE_WILAYAH")
	private String kodeWilayah;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NAMA_PETUGAS")
	private String namaPetugas;

	@Column(name="NO_LAPORAN_POLISI")
	private String noLaporanPolisi;

	@Column(name="NO_URUT")
	private BigDecimal noUrut;

	@Column(name="SIFAT_KECELAKAAN")
	private String sifatKecelakaan;

	@Column(name="STATUS_LAPORAN_POLISI")
	private String statusLaporanPolisi;

	@Column(name="STATUS_TRANSISI")
	private String statusTransisi;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_KEJADIAN")
	private Date tglKejadian;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_LAPORAN_POLISI")
	private Date tglLaporanPolisi;

	public PlDataKecelakaan() {
	}

	public String getIdKecelakaan() {
		return this.idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getAsalBerkas() {
		return this.asalBerkas;
	}

	public void setAsalBerkas(String asalBerkas) {
		this.asalBerkas = asalBerkas;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDeskripsiKecelakaan() {
		return this.deskripsiKecelakaan;
	}

	public void setDeskripsiKecelakaan(String deskripsiKecelakaan) {
		this.deskripsiKecelakaan = deskripsiKecelakaan;
	}

	public String getDeskripsiLokasi() {
		return this.deskripsiLokasi;
	}

	public void setDeskripsiLokasi(String deskripsiLokasi) {
		this.deskripsiLokasi = deskripsiLokasi;
	}

	public String getGpsLs() {
		return this.gpsLs;
	}

	public void setGpsLs(String gpsLs) {
		this.gpsLs = gpsLs;
	}

	public String getGpsLu() {
		return this.gpsLu;
	}

	public void setGpsLu(String gpsLu) {
		this.gpsLu = gpsLu;
	}

	public String getIdGuid() {
		return this.idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public BigDecimal getJumlahLuka() {
		return this.jumlahLuka;
	}

	public void setJumlahLuka(BigDecimal jumlahLuka) {
		this.jumlahLuka = jumlahLuka;
	}

	public BigDecimal getJumlahMeninggal() {
		return this.jumlahMeninggal;
	}

	public void setJumlahMeninggal(BigDecimal jumlahMeninggal) {
		this.jumlahMeninggal = jumlahMeninggal;
	}

	public String getKodeInstansi() {
		return this.kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeKasusKecelakaan() {
		return this.kodeKasusKecelakaan;
	}

	public void setKodeKasusKecelakaan(String kodeKasusKecelakaan) {
		this.kodeKasusKecelakaan = kodeKasusKecelakaan;
	}

	public String getKodeLintasan() {
		return this.kodeLintasan;
	}

	public void setKodeLintasan(String kodeLintasan) {
		this.kodeLintasan = kodeLintasan;
	}

	public String getKodeLokasi() {
		return this.kodeLokasi;
	}

	public void setKodeLokasi(String kodeLokasi) {
		this.kodeLokasi = kodeLokasi;
	}

	public String getKodeWilayah() {
		return this.kodeWilayah;
	}

	public void setKodeWilayah(String kodeWilayah) {
		this.kodeWilayah = kodeWilayah;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamaPetugas() {
		return this.namaPetugas;
	}

	public void setNamaPetugas(String namaPetugas) {
		this.namaPetugas = namaPetugas;
	}

	public String getNoLaporanPolisi() {
		return this.noLaporanPolisi;
	}

	public void setNoLaporanPolisi(String noLaporanPolisi) {
		this.noLaporanPolisi = noLaporanPolisi;
	}

	public BigDecimal getNoUrut() {
		return this.noUrut;
	}

	public void setNoUrut(BigDecimal noUrut) {
		this.noUrut = noUrut;
	}

	public String getSifatKecelakaan() {
		return this.sifatKecelakaan;
	}

	public void setSifatKecelakaan(String sifatKecelakaan) {
		this.sifatKecelakaan = sifatKecelakaan;
	}

	public String getStatusLaporanPolisi() {
		return this.statusLaporanPolisi;
	}

	public void setStatusLaporanPolisi(String statusLaporanPolisi) {
		this.statusLaporanPolisi = statusLaporanPolisi;
	}

	public String getStatusTransisi() {
		return this.statusTransisi;
	}

	public void setStatusTransisi(String statusTransisi) {
		this.statusTransisi = statusTransisi;
	}

	public Date getTglKejadian() {
		return this.tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public Date getTglLaporanPolisi() {
		return this.tglLaporanPolisi;
	}

	public void setTglLaporanPolisi(Date tglLaporanPolisi) {
		this.tglLaporanPolisi = tglLaporanPolisi;
	}

}