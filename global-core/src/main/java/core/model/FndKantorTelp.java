package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FND_KANTOR_TELP database table.
 * 
 */
@Entity
@Table(name="FND_KANTOR_TELP")
@NamedQuery(name="FndKantorTelp.findAll", query="SELECT f FROM FndKantorTelp f")
public class FndKantorTelp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="MAP_KODE_KANTOR")
	private String mapKodeKantor;

	@Column(name="NAMA_KANTOR")
	private String namaKantor;

	@Column(name="NO_TELP")
	private String noTelp;

	public FndKantorTelp() {
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getMapKodeKantor() {
		return this.mapKodeKantor;
	}

	public void setMapKodeKantor(String mapKodeKantor) {
		this.mapKodeKantor = mapKodeKantor;
	}

	public String getNamaKantor() {
		return this.namaKantor;
	}

	public void setNamaKantor(String namaKantor) {
		this.namaKantor = namaKantor;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

}