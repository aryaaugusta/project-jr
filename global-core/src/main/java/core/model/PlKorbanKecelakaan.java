package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_KORBAN_KECELAKAAN database table.
 * 
 */
@Entity
@Table(name="PL_KORBAN_KECELAKAAN")
@NamedQuery(name="PlKorbanKecelakaan.findAll", query="SELECT p FROM PlKorbanKecelakaan p")
public class PlKorbanKecelakaan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_KORBAN_KECELAKAAN")
	private String idKorbanKecelakaan;

	private String alamat;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="ID_ANGKUTAN_KECELAKAAN")
	private String idAngkutanKecelakaan;

	@Column(name="ID_ANGKUTAN_PENANGGUNG")
	private String idAngkutanPenanggung;

	@Column(name="ID_GUID")
	private String idGuid;

	@Column(name="ID_KECELAKAAN")
	private String idKecelakaan;

	@Column(name="JENIS_IDENTITAS")
	private String jenisIdentitas;

	@Column(name="JENIS_KELAMIN")
	private String jenisKelamin;

	@Column(name="KODE_JAMINAN")
	private String kodeJaminan;

	@Column(name="KODE_PEKERJAAN")
	private String kodePekerjaan;

	@Column(name="KODE_SIFAT_CIDERA")
	private String kodeSifatCidera;

	@Column(name="KODE_STATUS_KORBAN")
	private String kodeStatusKorban;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	private String nama;

	@Column(name="NO_IDENTITAS")
	private String noIdentitas;

	@Column(name="NO_TELP")
	private String noTelp;

	@Column(name="STATUS_NIKAH")
	private String statusNikah;

	private BigDecimal umur;

	public PlKorbanKecelakaan() {
	}

	public String getIdKorbanKecelakaan() {
		return this.idKorbanKecelakaan;
	}

	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}

	public String getAlamat() {
		return this.alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdAngkutanKecelakaan() {
		return this.idAngkutanKecelakaan;
	}

	public void setIdAngkutanKecelakaan(String idAngkutanKecelakaan) {
		this.idAngkutanKecelakaan = idAngkutanKecelakaan;
	}

	public String getIdAngkutanPenanggung() {
		return this.idAngkutanPenanggung;
	}

	public void setIdAngkutanPenanggung(String idAngkutanPenanggung) {
		this.idAngkutanPenanggung = idAngkutanPenanggung;
	}

	public String getIdGuid() {
		return this.idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public String getIdKecelakaan() {
		return this.idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getJenisIdentitas() {
		return this.jenisIdentitas;
	}

	public void setJenisIdentitas(String jenisIdentitas) {
		this.jenisIdentitas = jenisIdentitas;
	}

	public String getJenisKelamin() {
		return this.jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getKodeJaminan() {
		return this.kodeJaminan;
	}

	public void setKodeJaminan(String kodeJaminan) {
		this.kodeJaminan = kodeJaminan;
	}

	public String getKodePekerjaan() {
		return this.kodePekerjaan;
	}

	public void setKodePekerjaan(String kodePekerjaan) {
		this.kodePekerjaan = kodePekerjaan;
	}

	public String getKodeSifatCidera() {
		return this.kodeSifatCidera;
	}

	public void setKodeSifatCidera(String kodeSifatCidera) {
		this.kodeSifatCidera = kodeSifatCidera;
	}

	public String getKodeStatusKorban() {
		return this.kodeStatusKorban;
	}

	public void setKodeStatusKorban(String kodeStatusKorban) {
		this.kodeStatusKorban = kodeStatusKorban;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNama() {
		return this.nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNoIdentitas() {
		return this.noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public String getStatusNikah() {
		return this.statusNikah;
	}

	public void setStatusNikah(String statusNikah) {
		this.statusNikah = statusNikah;
	}

	public BigDecimal getUmur() {
		return this.umur;
	}

	public void setUmur(BigDecimal umur) {
		this.umur = umur;
	}

}