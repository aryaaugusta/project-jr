package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the KORLANTAS_OFFICERS database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_OFFICERS")
@NamedQuery(name="KorlantasOfficer.findAll", query="SELECT k FROM KorlantasOfficer k")
public class KorlantasOfficer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	private BigDecimal active;

	@Column(name="CREATED_AT")
	private Timestamp createdAt;

	@Column(name="CREATOR_ID")
	private BigDecimal creatorId;

	@Column(name="DISTRICT_ID")
	private BigDecimal districtId;

	private Timestamp dtl;

	@Column(name="FIRST_NAME")
	private String firstName;

	@Column(name="IS_INVESTIGATOR")
	private BigDecimal isInvestigator;

	@Column(name="IS_PATROL")
	private BigDecimal isPatrol;

	@Column(name="IS_TRANSLATOR")
	private BigDecimal isTranslator;

	@Column(name="IS_VALIDATOR")
	private BigDecimal isValidator;

	@Column(name="\"LANGUAGE\"")
	private String language;

	@Column(name="LAST_ACTIVITY")
	private Timestamp lastActivity;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_LOGIN")
	private Date lastLogin;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="OFFICER_EMAIL")
	private String officerEmail;

	@Column(name="OFFICER_FORMATION")
	private String officerFormation;

	@Column(name="OFFICER_LOGIN")
	private String officerLogin;

	@Column(name="OFFICER_NUMBER")
	private BigDecimal officerNumber;

	@Column(name="OFFICER_PASSWORD")
	private String officerPassword;

	@Column(name="PERISHABLE_TOKEN")
	private String perishableToken;

	private String phone;

	@Column(name="PROVINCE_ID")
	private BigDecimal provinceId;

	@Column(name="RANK_ID")
	private BigDecimal rankId;

	@Column(name="\"ROLE\"")
	private String role;

	@Column(name="UPDATED_AT")
	private Timestamp updatedAt;

	public KorlantasOfficer() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getActive() {
		return this.active;
	}

	public void setActive(BigDecimal active) {
		this.active = active;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public BigDecimal getCreatorId() {
		return this.creatorId;
	}

	public void setCreatorId(BigDecimal creatorId) {
		this.creatorId = creatorId;
	}

	public BigDecimal getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(BigDecimal districtId) {
		this.districtId = districtId;
	}

	public Timestamp getDtl() {
		return this.dtl;
	}

	public void setDtl(Timestamp dtl) {
		this.dtl = dtl;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public BigDecimal getIsInvestigator() {
		return this.isInvestigator;
	}

	public void setIsInvestigator(BigDecimal isInvestigator) {
		this.isInvestigator = isInvestigator;
	}

	public BigDecimal getIsPatrol() {
		return this.isPatrol;
	}

	public void setIsPatrol(BigDecimal isPatrol) {
		this.isPatrol = isPatrol;
	}

	public BigDecimal getIsTranslator() {
		return this.isTranslator;
	}

	public void setIsTranslator(BigDecimal isTranslator) {
		this.isTranslator = isTranslator;
	}

	public BigDecimal getIsValidator() {
		return this.isValidator;
	}

	public void setIsValidator(BigDecimal isValidator) {
		this.isValidator = isValidator;
	}

	public String getLanguage() {
		return this.language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Timestamp getLastActivity() {
		return this.lastActivity;
	}

	public void setLastActivity(Timestamp lastActivity) {
		this.lastActivity = lastActivity;
	}

	public Date getLastLogin() {
		return this.lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getOfficerEmail() {
		return this.officerEmail;
	}

	public void setOfficerEmail(String officerEmail) {
		this.officerEmail = officerEmail;
	}

	public String getOfficerFormation() {
		return this.officerFormation;
	}

	public void setOfficerFormation(String officerFormation) {
		this.officerFormation = officerFormation;
	}

	public String getOfficerLogin() {
		return this.officerLogin;
	}

	public void setOfficerLogin(String officerLogin) {
		this.officerLogin = officerLogin;
	}

	public BigDecimal getOfficerNumber() {
		return this.officerNumber;
	}

	public void setOfficerNumber(BigDecimal officerNumber) {
		this.officerNumber = officerNumber;
	}

	public String getOfficerPassword() {
		return this.officerPassword;
	}

	public void setOfficerPassword(String officerPassword) {
		this.officerPassword = officerPassword;
	}

	public String getPerishableToken() {
		return this.perishableToken;
	}

	public void setPerishableToken(String perishableToken) {
		this.perishableToken = perishableToken;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BigDecimal getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(BigDecimal provinceId) {
		this.provinceId = provinceId;
	}

	public BigDecimal getRankId() {
		return this.rankId;
	}

	public void setRankId(BigDecimal rankId) {
		this.rankId = rankId;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}