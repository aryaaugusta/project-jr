package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the GL_VOUCHER database table.
 * 
 */
@Entity
@Table(name="GL_VOUCHER")
@NamedQuery(name="GlVoucher.findAll", query="SELECT g FROM GlVoucher g")
@IdClass(GlVoucherPK.class)
public class GlVoucher implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_VOUCHER")
	private String idVoucher;

	@Id
	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private String deskripsi;

	@Column(name="JENIS_VOUCHER")
	private String jenisVoucher;

	@Column(name="KODE_VOUCHER")
	private String kodeVoucher;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NAMA_JURNAL")
	private String namaJurnal;

	@Column(name="NAMA_PERIODE")
	private String namaPeriode;

	@Column(name="NO_REFERENSI")
	private String noReferensi;

	private String penerima;

	private String petugas;

	@Column(name="STATUS_PERSETUJUAN")
	private String statusPersetujuan;

	@Column(name="STATUS_POSTING")
	private String statusPosting;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_ENTRY")
	private Date tglEntry;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_TRANSAKSI")
	private Date tglTransaksi;

	public GlVoucher() {
	}


	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDeskripsi() {
		return this.deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getJenisVoucher() {
		return this.jenisVoucher;
	}

	public void setJenisVoucher(String jenisVoucher) {
		this.jenisVoucher = jenisVoucher;
	}

	public String getKodeVoucher() {
		return this.kodeVoucher;
	}

	public void setKodeVoucher(String kodeVoucher) {
		this.kodeVoucher = kodeVoucher;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamaJurnal() {
		return this.namaJurnal;
	}

	public void setNamaJurnal(String namaJurnal) {
		this.namaJurnal = namaJurnal;
	}

	public String getNamaPeriode() {
		return this.namaPeriode;
	}

	public void setNamaPeriode(String namaPeriode) {
		this.namaPeriode = namaPeriode;
	}

	public String getNoReferensi() {
		return this.noReferensi;
	}

	public void setNoReferensi(String noReferensi) {
		this.noReferensi = noReferensi;
	}

	public String getPenerima() {
		return this.penerima;
	}

	public void setPenerima(String penerima) {
		this.penerima = penerima;
	}

	public String getPetugas() {
		return this.petugas;
	}

	public void setPetugas(String petugas) {
		this.petugas = petugas;
	}

	public String getStatusPersetujuan() {
		return this.statusPersetujuan;
	}

	public void setStatusPersetujuan(String statusPersetujuan) {
		this.statusPersetujuan = statusPersetujuan;
	}

	public String getStatusPosting() {
		return this.statusPosting;
	}

	public void setStatusPosting(String statusPosting) {
		this.statusPosting = statusPosting;
	}

	public Date getTglEntry() {
		return this.tglEntry;
	}

	public void setTglEntry(Date tglEntry) {
		this.tglEntry = tglEntry;
	}

	public Date getTglTransaksi() {
		return this.tglTransaksi;
	}

	public void setTglTransaksi(Date tglTransaksi) {
		this.tglTransaksi = tglTransaksi;
	}

	public String getIdVoucher() {
		return idVoucher;
	}

	public void setIdVoucher(String idVoucher) {
		this.idVoucher = idVoucher;
	}

	public String getKodeKantorJr() {
		return kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

}