package core.model;

import java.io.Serializable;

public class PlMappingRsInstansiPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kodeInstansi;
	private String kodeRumahsakit;
	public String getKodeInstansi() {
		return kodeInstansi;
	}
	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}
	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}
	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeInstansi == null) ? 0 : kodeInstansi.hashCode());
		result = prime * result
				+ ((kodeRumahsakit == null) ? 0 : kodeRumahsakit.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlMappingRsInstansiPK other = (PlMappingRsInstansiPK) obj;
		if (kodeInstansi == null) {
			if (other.kodeInstansi != null)
				return false;
		} else if (!kodeInstansi.equals(other.kodeInstansi))
			return false;
		if (kodeRumahsakit == null) {
			if (other.kodeRumahsakit != null)
				return false;
		} else if (!kodeRumahsakit.equals(other.kodeRumahsakit))
			return false;
		return true;
	}
}
