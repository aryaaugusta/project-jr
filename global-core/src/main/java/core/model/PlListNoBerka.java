package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_LIST_NO_BERKAS database table.
 * 
 */
@Entity
@Table(name="PL_LIST_NO_BERKAS")
@NamedQuery(name="PlListNoBerka.findAll", query="SELECT p FROM PlListNoBerka p")
public class PlListNoBerka implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="KD_KANTOR_JR")
	private String kdKantorJr;

	@Id
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="NO_BPK")
	private String noBpk;

	public PlListNoBerka() {
	}

	public String getKdKantorJr() {
		return this.kdKantorJr;
	}

	public void setKdKantorJr(String kdKantorJr) {
		this.kdKantorJr = kdKantorJr;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getNoBpk() {
		return this.noBpk;
	}

	public void setNoBpk(String noBpk) {
		this.noBpk = noBpk;
	}

}