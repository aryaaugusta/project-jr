package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_OTORISASI_BERKAS database table.
 * 
 */
@Entity
@Table(name="PL_OTORISASI_BERKAS")
@NamedQuery(name="PlOtorisasiBerka.findAll", query="SELECT p FROM PlOtorisasiBerka p")
@IdClass(PlOtorisasiBerkasPK.class)
public class PlOtorisasiBerka implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="KD_KANTOR_JR")
	private String kdKantorJr;

	private String keterangan;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Id
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Id
	private String otorisator;

	private String status;

	public PlOtorisasiBerka() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getKdKantorJr() {
		return this.kdKantorJr;
	}

	public void setKdKantorJr(String kdKantorJr) {
		this.kdKantorJr = kdKantorJr;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getOtorisator() {
		return this.otorisator;
	}

	public void setOtorisator(String otorisator) {
		this.otorisator = otorisator;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}