package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DASI_DOWNLOAD_REQUEST database table.
 * 
 */
@Entity
@Table(name="DASI_DOWNLOAD_REQUEST")
@NamedQuery(name="DasiDownloadRequest.findAll", query="SELECT d FROM DasiDownloadRequest d")
public class DasiDownloadRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="DOWNLOAD_PATH")
	private String downloadPath;

	@Id
	@Column(name="ID_SYNC")
	private String idSync;

	@Column(name="JNS_KONSOL")
	private String jnsKonsol;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="LEVEL_KONSOL")
	private String levelKonsol;

	@Column(name="STATUS_REQUEST")
	private String statusRequest;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_END")
	private Date tglEnd;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_START")
	private Date tglStart;

	public DasiDownloadRequest() {
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDownloadPath() {
		return this.downloadPath;
	}

	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}

	public String getIdSync() {
		return this.idSync;
	}

	public void setIdSync(String idSync) {
		this.idSync = idSync;
	}

	public String getJnsKonsol() {
		return this.jnsKonsol;
	}

	public void setJnsKonsol(String jnsKonsol) {
		this.jnsKonsol = jnsKonsol;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getLevelKonsol() {
		return this.levelKonsol;
	}

	public void setLevelKonsol(String levelKonsol) {
		this.levelKonsol = levelKonsol;
	}

	public String getStatusRequest() {
		return this.statusRequest;
	}

	public void setStatusRequest(String statusRequest) {
		this.statusRequest = statusRequest;
	}

	public Date getTglEnd() {
		return this.tglEnd;
	}

	public void setTglEnd(Date tglEnd) {
		this.tglEnd = tglEnd;
	}

	public Date getTglStart() {
		return this.tglStart;
	}

	public void setTglStart(Date tglStart) {
		this.tglStart = tglStart;
	}

}