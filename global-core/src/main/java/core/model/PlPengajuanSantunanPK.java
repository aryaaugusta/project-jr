package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PL_PENGAJUAN_SANTUNAN database table.
 * 
 */
public class PlPengajuanSantunanPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String noBerkas;

	private String transisiFlag;

	public PlPengajuanSantunanPK() {
	}
	public String getNoBerkas() {
		return this.noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getTransisiFlag() {
		return this.transisiFlag;
	}
	public void setTransisiFlag(String transisiFlag) {
		this.transisiFlag = transisiFlag;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PlPengajuanSantunanPK)) {
			return false;
		}
		PlPengajuanSantunanPK castOther = (PlPengajuanSantunanPK)other;
		return 
			this.noBerkas.equals(castOther.noBerkas)
			&& this.transisiFlag.equals(castOther.transisiFlag);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.noBerkas.hashCode();
		hash = hash * prime + this.transisiFlag.hashCode();
		
		return hash;
	}
}