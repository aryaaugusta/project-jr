package core.model;


public class PlBerkasEcmsContainPK implements java.io.Serializable{
	private String ddocname;
	private String kodeBerkas;
	private String noRegEcms;
	


	public PlBerkasEcmsContainPK(){}
	
	public PlBerkasEcmsContainPK(String ddocname, String kodeBerkas) {
		super();
		this.ddocname = ddocname;
		this.kodeBerkas = kodeBerkas;
	}
	public String getDdocname() {
		return ddocname;
	}
	public void setDdocname(String ddocname) {
		this.ddocname = ddocname;
	}
	public String getKodeBerkas() {
		return kodeBerkas;
	}
	public void setKodeBerkas(String kodeBerkas) {
		this.kodeBerkas = kodeBerkas;
	}
	
	public String getNoRegEcms() {
		return noRegEcms;
	}

	public void setNoRegEcms(String noRegEcms) {
		this.noRegEcms = noRegEcms;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ddocname == null) ? 0 : ddocname.hashCode());
		result = prime * result
				+ ((kodeBerkas == null) ? 0 : kodeBerkas.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlBerkasEcmsContainPK other = (PlBerkasEcmsContainPK) obj;
		if (ddocname == null) {
			if (other.ddocname != null)
				return false;
		} else if (!ddocname.equals(other.ddocname))
			return false;
		if (kodeBerkas == null) {
			if (other.kodeBerkas != null)
				return false;
		} else if (!kodeBerkas.equals(other.kodeBerkas))
			return false;
		return true;
	}
}
