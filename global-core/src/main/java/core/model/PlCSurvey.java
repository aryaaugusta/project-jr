package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_C_SURVEY database table.
 * 
 */
@Entity
@Table(name="PL_C_SURVEY")
@NamedQuery(name="PlCSurvey.findAll", query="SELECT p FROM PlCSurvey p")
public class PlCSurvey implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="ID_SURVEY")
	private String idSurvey;

	@Column(name="JNS_SURVEY")
	private String jnsSurvey;

	private String kesimpulan;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	private String petugas;

	@Column(name="PILIHAN_KORBAN")
	private String pilihanKorban;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_SURVEY")
	private Date tglSurvey;

	public PlCSurvey() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdSurvey() {
		return this.idSurvey;
	}

	public void setIdSurvey(String idSurvey) {
		this.idSurvey = idSurvey;
	}

	public String getJnsSurvey() {
		return this.jnsSurvey;
	}

	public void setJnsSurvey(String jnsSurvey) {
		this.jnsSurvey = jnsSurvey;
	}

	public String getKesimpulan() {
		return this.kesimpulan;
	}

	public void setKesimpulan(String kesimpulan) {
		this.kesimpulan = kesimpulan;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getPetugas() {
		return this.petugas;
	}

	public void setPetugas(String petugas) {
		this.petugas = petugas;
	}

	public String getPilihanKorban() {
		return this.pilihanKorban;
	}

	public void setPilihanKorban(String pilihanKorban) {
		this.pilihanKorban = pilihanKorban;
	}

	public Date getTglSurvey() {
		return this.tglSurvey;
	}

	public void setTglSurvey(Date tglSurvey) {
		this.tglSurvey = tglSurvey;
	}

}