package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DASI_JR_KONSOL database table.
 * 
 */
@Entity
@Table(name="DASI_JR_KONSOL")
@NamedQuery(name="DasiJrKonsol.findAll", query="SELECT d FROM DasiJrKonsol d")
public class DasiJrKonsol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="ID_KONSOL")
	private String idKonsol;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="METHOD_DB")
	private String methodDb;

	@Column(name="METHOD_KONSOL")
	private String methodKonsol;

	@Column(name="METHOD_SYNC")
	private String methodSync;

	@Temporal(TemporalType.DATE)
	@Column(name="PERIODE_AKHIR")
	private Date periodeAkhir;

	@Temporal(TemporalType.DATE)
	@Column(name="PERIODE_AWAL")
	private Date periodeAwal;

	@Column(name="STATUS_PROSES")
	private String statusProses;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_MULAI")
	private Date tglMulai;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_SELESAI")
	private Date tglSelesai;

	public DasiJrKonsol() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdKonsol() {
		return this.idKonsol;
	}

	public void setIdKonsol(String idKonsol) {
		this.idKonsol = idKonsol;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getMethodDb() {
		return this.methodDb;
	}

	public void setMethodDb(String methodDb) {
		this.methodDb = methodDb;
	}

	public String getMethodKonsol() {
		return this.methodKonsol;
	}

	public void setMethodKonsol(String methodKonsol) {
		this.methodKonsol = methodKonsol;
	}

	public String getMethodSync() {
		return this.methodSync;
	}

	public void setMethodSync(String methodSync) {
		this.methodSync = methodSync;
	}

	public Date getPeriodeAkhir() {
		return this.periodeAkhir;
	}

	public void setPeriodeAkhir(Date periodeAkhir) {
		this.periodeAkhir = periodeAkhir;
	}

	public Date getPeriodeAwal() {
		return this.periodeAwal;
	}

	public void setPeriodeAwal(Date periodeAwal) {
		this.periodeAwal = periodeAwal;
	}

	public String getStatusProses() {
		return this.statusProses;
	}

	public void setStatusProses(String statusProses) {
		this.statusProses = statusProses;
	}

	public Date getTglMulai() {
		return this.tglMulai;
	}

	public void setTglMulai(Date tglMulai) {
		this.tglMulai = tglMulai;
	}

	public Date getTglSelesai() {
		return this.tglSelesai;
	}

	public void setTglSelesai(Date tglSelesai) {
		this.tglSelesai = tglSelesai;
	}

}