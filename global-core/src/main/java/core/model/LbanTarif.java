package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the LBAN_TARIF database table.
 * 
 */
@Entity
@Table(name="LBAN_TARIF")
@NamedQuery(name="LbanTarif.findAll", query="SELECT l FROM LbanTarif l")
public class LbanTarif implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KODE_TRF")
	private String kodeTrf;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="LAST_UPDATE")
	private String lastUpdate;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATE_DATE")
	private Date lastUpdateDate;

	@Column(name="MASA_LAKU")
	private BigDecimal masaLaku;

	@Column(name="NILAI_IW")
	private BigDecimal nilaiIw;

	@Column(name="NILAI_SW")
	private BigDecimal nilaiSw;

	private BigDecimal seat;

	@Column(name="STATUS_FLAG")
	private String statusFlag;

	public LbanTarif() {
	}

	public String getKodeTrf() {
		return this.kodeTrf;
	}

	public void setKodeTrf(String kodeTrf) {
		this.kodeTrf = kodeTrf;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(String lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public BigDecimal getMasaLaku() {
		return this.masaLaku;
	}

	public void setMasaLaku(BigDecimal masaLaku) {
		this.masaLaku = masaLaku;
	}

	public BigDecimal getNilaiIw() {
		return this.nilaiIw;
	}

	public void setNilaiIw(BigDecimal nilaiIw) {
		this.nilaiIw = nilaiIw;
	}

	public BigDecimal getNilaiSw() {
		return this.nilaiSw;
	}

	public void setNilaiSw(BigDecimal nilaiSw) {
		this.nilaiSw = nilaiSw;
	}

	public BigDecimal getSeat() {
		return this.seat;
	}

	public void setSeat(BigDecimal seat) {
		this.seat = seat;
	}

	public String getStatusFlag() {
		return this.statusFlag;
	}

	public void setStatusFlag(String statusFlag) {
		this.statusFlag = statusFlag;
	}

}