package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the DASI_CLIENT_IDS database table.
 * 
 */
@Entity
@Table(name="DASI_CLIENT_IDS")
@NamedQuery(name="DasiClientId.findAll", query="SELECT d FROM DasiClientId d")
public class DasiClientId implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CLIENT_DESC")
	private String clientDesc;

	@Id
	@Column(name="CLIENT_ID")
	private String clientId;

	@Column(name="CLIENT_MEANING")
	private String clientMeaning;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	@Column(name="KODE_CLIENT_ACCESS")
	private String kodeClientAccess;

	public DasiClientId() {
	}

	public String getClientDesc() {
		return this.clientDesc;
	}

	public void setClientDesc(String clientDesc) {
		this.clientDesc = clientDesc;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getClientMeaning() {
		return this.clientMeaning;
	}

	public void setClientMeaning(String clientMeaning) {
		this.clientMeaning = clientMeaning;
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getKodeClientAccess() {
		return this.kodeClientAccess;
	}

	public void setKodeClientAccess(String kodeClientAccess) {
		this.kodeClientAccess = kodeClientAccess;
	}

}