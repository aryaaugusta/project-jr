package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the KORLANTAS_RE_VEHICLES database table.
 * 
 */
public class KorlantasReVehiclePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private long id;

	private long accidentId;

	public KorlantasReVehiclePK() {
	}
	public long getId() {
		return this.id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getAccidentId() {
		return this.accidentId;
	}
	public void setAccidentId(long accidentId) {
		this.accidentId = accidentId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof KorlantasReVehiclePK)) {
			return false;
		}
		KorlantasReVehiclePK castOther = (KorlantasReVehiclePK)other;
		return 
			(this.id == castOther.id)
			&& (this.accidentId == castOther.accidentId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.id ^ (this.id >>> 32)));
		hash = hash * prime + ((int) (this.accidentId ^ (this.accidentId >>> 32)));
		
		return hash;
	}
}