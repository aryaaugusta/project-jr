package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_BERKAS_ECMS_CONTAINS database table.
 * 
 */
@Entity
@Table(name="PL_BERKAS_ECMS_CONTAINS")
@NamedQuery(name="PlBerkasEcmsContain.findAll", query="SELECT p FROM PlBerkasEcmsContain p")
@IdClass(value=PlBerkasEcmsContainPK.class)
public class PlBerkasEcmsContain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DDOCNAME")
	private String ddocname;

	@Id
	@Column(name="KODE_BERKAS")
	private String kodeBerkas;
	
	@Id
	@Column(name="NO_REG_ECMS")
	private String noRegEcms;

	public PlBerkasEcmsContain() {
	}

	public String getDdocname() {
		return this.ddocname;
	}

	public void setDdocname(String ddocname) {
		this.ddocname = ddocname;
	}

	public String getKodeBerkas() {
		return this.kodeBerkas;
	}

	public void setKodeBerkas(String kodeBerkas) {
		this.kodeBerkas = kodeBerkas;
	}

	public String getNoRegEcms() {
		return this.noRegEcms;
	}

	public void setNoRegEcms(String noRegEcms) {
		this.noRegEcms = noRegEcms;
	}

}