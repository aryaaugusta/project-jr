package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_LOG_CRUD database table.
 * 
 */
@Entity
@Table(name="PL_LOG_CRUD")
@NamedQuery(name="PlLogCrud.findAll", query="SELECT p FROM PlLogCrud p")
public class PlLogCrud implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="PARAMS_VAL")
	private String paramsVal;

	@Id
	@Column(name="SP_NAME")
	private String spName;

	@Column(name="STATUS_FIX")
	private String statusFix;

	public PlLogCrud() {
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getParamsVal() {
		return this.paramsVal;
	}

	public void setParamsVal(String paramsVal) {
		this.paramsVal = paramsVal;
	}

	public String getSpName() {
		return this.spName;
	}

	public void setSpName(String spName) {
		this.spName = spName;
	}

	public String getStatusFix() {
		return this.statusFix;
	}

	public void setStatusFix(String statusFix) {
		this.statusFix = statusFix;
	}

}