package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_PASSENGERS database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_PASSENGERS")
@NamedQuery(name="KorlantasPassenger.findAll", query="SELECT k FROM KorlantasPassenger k")
public class KorlantasPassenger implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	private BigDecimal age;

	private BigDecimal arrested;

	@Column(name="EDUCATION_ID")
	private BigDecimal educationId;

	@Column(name="FIRST_NAME")
	private String firstName;

	private BigDecimal foreigner;

	@Column(name="\"IDENTITY\"")
	private String identity;

	@Column(name="IDENTITY_TYPE_ID")
	private BigDecimal identityTypeId;

	@Column(name="INJURY_ID")
	private BigDecimal injuryId;

	@Column(name="LAST_NAME")
	private String lastName;

	@Column(name="NATIONALITY_ID")
	private BigDecimal nationalityId;

	@Column(name="PASSENGER_BEHAVIOUR_ID")
	private BigDecimal passengerBehaviourId;

	@Column(name="PROFESSION_ID")
	private BigDecimal professionId;

	@Column(name="RELIGION_ID")
	private BigDecimal religionId;

	@Column(name="SAFETY_EQUIPMENT_PASSENGER_ID")
	private BigDecimal safetyEquipmentPassengerId;

	@Column(name="SCHOOL_KID")
	private BigDecimal schoolKid;

	private BigDecimal sex;

	@Column(name="\"STATE\"")
	private BigDecimal state;

	@Column(name="VEHICLE_ID")
	private BigDecimal vehicleId;

	public KorlantasPassenger() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getAge() {
		return this.age;
	}

	public void setAge(BigDecimal age) {
		this.age = age;
	}

	public BigDecimal getArrested() {
		return this.arrested;
	}

	public void setArrested(BigDecimal arrested) {
		this.arrested = arrested;
	}

	public BigDecimal getEducationId() {
		return this.educationId;
	}

	public void setEducationId(BigDecimal educationId) {
		this.educationId = educationId;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public BigDecimal getForeigner() {
		return this.foreigner;
	}

	public void setForeigner(BigDecimal foreigner) {
		this.foreigner = foreigner;
	}

	public String getIdentity() {
		return this.identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public BigDecimal getIdentityTypeId() {
		return this.identityTypeId;
	}

	public void setIdentityTypeId(BigDecimal identityTypeId) {
		this.identityTypeId = identityTypeId;
	}

	public BigDecimal getInjuryId() {
		return this.injuryId;
	}

	public void setInjuryId(BigDecimal injuryId) {
		this.injuryId = injuryId;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public BigDecimal getNationalityId() {
		return this.nationalityId;
	}

	public void setNationalityId(BigDecimal nationalityId) {
		this.nationalityId = nationalityId;
	}

	public BigDecimal getPassengerBehaviourId() {
		return this.passengerBehaviourId;
	}

	public void setPassengerBehaviourId(BigDecimal passengerBehaviourId) {
		this.passengerBehaviourId = passengerBehaviourId;
	}

	public BigDecimal getProfessionId() {
		return this.professionId;
	}

	public void setProfessionId(BigDecimal professionId) {
		this.professionId = professionId;
	}

	public BigDecimal getReligionId() {
		return this.religionId;
	}

	public void setReligionId(BigDecimal religionId) {
		this.religionId = religionId;
	}

	public BigDecimal getSafetyEquipmentPassengerId() {
		return this.safetyEquipmentPassengerId;
	}

	public void setSafetyEquipmentPassengerId(BigDecimal safetyEquipmentPassengerId) {
		this.safetyEquipmentPassengerId = safetyEquipmentPassengerId;
	}

	public BigDecimal getSchoolKid() {
		return this.schoolKid;
	}

	public void setSchoolKid(BigDecimal schoolKid) {
		this.schoolKid = schoolKid;
	}

	public BigDecimal getSex() {
		return this.sex;
	}

	public void setSex(BigDecimal sex) {
		this.sex = sex;
	}

	public BigDecimal getState() {
		return this.state;
	}

	public void setState(BigDecimal state) {
		this.state = state;
	}

	public BigDecimal getVehicleId() {
		return this.vehicleId;
	}

	public void setVehicleId(BigDecimal vehicleId) {
		this.vehicleId = vehicleId;
	}

}