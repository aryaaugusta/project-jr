package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_KORL_MAP_LAKA database table.
 * 
 */
@Entity
@Table(name="PL_KORL_MAP_LAKA")
@NamedQuery(name="PlKorlMapLaka.findAll", query="SELECT p FROM PlKorlMapLaka p")
@IdClass(PlKorlMapLakaPK.class)
public class PlKorlMapLaka implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="CREATED_TYPE")
	private String createdType;

	@Id
	@Column(name="ID_JR")
	private String idJr;

	@Id
	@Column(name="ID_KORLANTAS")
	private String idKorlantas;

	public PlKorlMapLaka() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedType() {
		return this.createdType;
	}

	public void setCreatedType(String createdType) {
		this.createdType = createdType;
	}

	public String getIdJr() {
		return this.idJr;
	}

	public void setIdJr(String idJr) {
		this.idJr = idJr;
	}

	public String getIdKorlantas() {
		return this.idKorlantas;
	}

	public void setIdKorlantas(String idKorlantas) {
		this.idKorlantas = idKorlantas;
	}

}