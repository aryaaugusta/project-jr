package core.model;

import java.io.Serializable;

public class FndKantorJasaraharjaPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String kodeKantorJr;

	public String getKodeKantorJr() {
		return kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeKantorJr == null) ? 0 : kodeKantorJr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FndKantorJasaraharjaPK other = (FndKantorJasaraharjaPK) obj;
		if (kodeKantorJr == null) {
			if (other.kodeKantorJr != null)
				return false;
		} else if (!kodeKantorJr.equals(other.kodeKantorJr))
			return false;
		return true;
	}
	

}
