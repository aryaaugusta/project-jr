package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_KORL_MAP_KORBAN_KET database table.
 * 
 */
@Entity
@Table(name="PL_KORL_MAP_KORBAN_KET")
@NamedQuery(name="PlKorlMapKorbanKet.findAll", query="SELECT p FROM PlKorlMapKorbanKet p")
@IdClass(PlKorlMapKorbanKetPK.class)
public class PlKorlMapKorbanKet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Id
	@Column(name="ID_JR")
	private String idJr;

	@Id
	@Column(name="ID_KORLANTAS")
	private String idKorlantas;

	@Column(name="KODE_KET")
	private String kodeKet;

	public PlKorlMapKorbanKet() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getIdJr() {
		return this.idJr;
	}

	public void setIdJr(String idJr) {
		this.idJr = idJr;
	}

	public String getIdKorlantas() {
		return this.idKorlantas;
	}

	public void setIdKorlantas(String idKorlantas) {
		this.idKorlantas = idKorlantas;
	}

	public String getKodeKet() {
		return this.kodeKet;
	}

	public void setKodeKet(String kodeKet) {
		this.kodeKet = kodeKet;
	}

}