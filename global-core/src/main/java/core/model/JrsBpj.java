package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the JRS_BPJS database table.
 * 
 */
@Entity
@Table(name="JRS_BPJS")
@NamedQuery(name="JrsBpj.findAll", query="SELECT j FROM JrsBpj j")
public class JrsBpj implements Serializable {
	private static final long serialVersionUID = 1L;

	private String alamat;

	private String biaya;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Column(name="CREATED_BY_1")
	private String createdBy1;

	@Column(name="CREATED_DATE")
	private Timestamp createdDate;

	@Column(name="CREATED_DATE_1")
	private Timestamp createdDate1;

	private String diagnosa;

	@Column(name="DILIMPAHKAN_KE")
	private String dilimpahkanKe;

	@Column(name="DIPROSES_DI")
	private String diprosesDi;

	@Column(name="DOKTER_BERWENANG")
	private String dokterBerwenang;

	@Column(name="FLAG_DOBEL")
	private String flagDobel;

	@Column(name="FLAG_MULTI_JAMINAN")
	private String flagMultiJaminan;

	@Id
	@Column(name="ID_RESPONSE")
	private String idResponse;

	@Column(name="JENIS_KELAMIN")
	private String jenisKelamin;

	@Column(name="JENIS_PELAYANAN")
	private String jenisPelayanan;

	@Column(name="JENIS_TINDAKAN")
	private String jenisTindakan;

	@Column(name="JUMLAH_DIBAYARKAN")
	private String jumlahDibayarkan;

	private String keterangan;

	@Column(name="KODE_KEJADIAN")
	private String kodeKejadian;

	@Column(name="KODE_KEJADIAN_1")
	private String kodeKejadian1;

	@Column(name="KODE_KESIMPULAN")
	private String kodeKesimpulan;

	@Column(name="KODE_REKOMENDASI")
	private String kodeRekomendasi;

	@Column(name="KODE_RS")
	private String kodeRs;

	@Column(name="KODE_RUMAH_SAKIT")
	private String kodeRumahSakit;

	@Column(name="LAST_UPDATE_BY")
	private String lastUpdateBy;

	@Column(name="LAST_UPDATE_DATE")
	private Timestamp lastUpdateDate;

	@Column(name="LAST_UPDATE_DATE_1")
	private Timestamp lastUpdateDate1;

	@Column(name="LOKASI_KEJADIAN")
	private String lokasiKejadian;

	@Column(name="NAMA_KORBAN")
	private String namaKorban;

	private String nik;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="NO_KARTU")
	private String noKartu;

	@Column(name="NO_TELP")
	private String noTelp;

	private String petugas;

	@Column(name="REKAM_MEDIS")
	private String rekamMedis;

	private String ruangan;

	@Column(name="SIFAT_CIDERA")
	private String sifatCidera;

	@Column(name="STATUS_JAMINAN")
	private String statusJaminan;

	@Column(name="STATUS_KLAIM")
	private String statusKlaim;

	@Column(name="TGL_KEJADIAN")
	private Timestamp tglKejadian;

	@Column(name="TGL_MASUK_RS")
	private Timestamp tglMasukRs;

	@Column(name="TGL_PROSES")
	private String tglProses;

	@Column(name="TIPE_ID")
	private String tipeId;

	private String umur;

	private String uraian;

	public JrsBpj() {
	}

	public String getAlamat() {
		return this.alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getBiaya() {
		return this.biaya;
	}

	public void setBiaya(String biaya) {
		this.biaya = biaya;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy1() {
		return this.createdBy1;
	}

	public void setCreatedBy1(String createdBy1) {
		this.createdBy1 = createdBy1;
	}

	public Timestamp getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getCreatedDate1() {
		return this.createdDate1;
	}

	public void setCreatedDate1(Timestamp createdDate1) {
		this.createdDate1 = createdDate1;
	}

	public String getDiagnosa() {
		return this.diagnosa;
	}

	public void setDiagnosa(String diagnosa) {
		this.diagnosa = diagnosa;
	}

	public String getDilimpahkanKe() {
		return this.dilimpahkanKe;
	}

	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}

	public String getDiprosesDi() {
		return this.diprosesDi;
	}

	public void setDiprosesDi(String diprosesDi) {
		this.diprosesDi = diprosesDi;
	}

	public String getDokterBerwenang() {
		return this.dokterBerwenang;
	}

	public void setDokterBerwenang(String dokterBerwenang) {
		this.dokterBerwenang = dokterBerwenang;
	}

	public String getFlagDobel() {
		return this.flagDobel;
	}

	public void setFlagDobel(String flagDobel) {
		this.flagDobel = flagDobel;
	}

	public String getFlagMultiJaminan() {
		return this.flagMultiJaminan;
	}

	public void setFlagMultiJaminan(String flagMultiJaminan) {
		this.flagMultiJaminan = flagMultiJaminan;
	}

	public String getIdResponse() {
		return this.idResponse;
	}

	public void setIdResponse(String idResponse) {
		this.idResponse = idResponse;
	}

	public String getJenisKelamin() {
		return this.jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getJenisPelayanan() {
		return this.jenisPelayanan;
	}

	public void setJenisPelayanan(String jenisPelayanan) {
		this.jenisPelayanan = jenisPelayanan;
	}

	public String getJenisTindakan() {
		return this.jenisTindakan;
	}

	public void setJenisTindakan(String jenisTindakan) {
		this.jenisTindakan = jenisTindakan;
	}

	public String getJumlahDibayarkan() {
		return this.jumlahDibayarkan;
	}

	public void setJumlahDibayarkan(String jumlahDibayarkan) {
		this.jumlahDibayarkan = jumlahDibayarkan;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getKodeKejadian() {
		return this.kodeKejadian;
	}

	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}

	public String getKodeKejadian1() {
		return this.kodeKejadian1;
	}

	public void setKodeKejadian1(String kodeKejadian1) {
		this.kodeKejadian1 = kodeKejadian1;
	}

	public String getKodeKesimpulan() {
		return this.kodeKesimpulan;
	}

	public void setKodeKesimpulan(String kodeKesimpulan) {
		this.kodeKesimpulan = kodeKesimpulan;
	}

	public String getKodeRekomendasi() {
		return this.kodeRekomendasi;
	}

	public void setKodeRekomendasi(String kodeRekomendasi) {
		this.kodeRekomendasi = kodeRekomendasi;
	}

	public String getKodeRs() {
		return this.kodeRs;
	}

	public void setKodeRs(String kodeRs) {
		this.kodeRs = kodeRs;
	}

	public String getKodeRumahSakit() {
		return this.kodeRumahSakit;
	}

	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}

	public String getLastUpdateBy() {
		return this.lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}

	public Timestamp getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public Timestamp getLastUpdateDate1() {
		return this.lastUpdateDate1;
	}

	public void setLastUpdateDate1(Timestamp lastUpdateDate1) {
		this.lastUpdateDate1 = lastUpdateDate1;
	}

	public String getLokasiKejadian() {
		return this.lokasiKejadian;
	}

	public void setLokasiKejadian(String lokasiKejadian) {
		this.lokasiKejadian = lokasiKejadian;
	}

	public String getNamaKorban() {
		return this.namaKorban;
	}

	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}

	public String getNik() {
		return this.nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getNoKartu() {
		return this.noKartu;
	}

	public void setNoKartu(String noKartu) {
		this.noKartu = noKartu;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public String getPetugas() {
		return this.petugas;
	}

	public void setPetugas(String petugas) {
		this.petugas = petugas;
	}

	public String getRekamMedis() {
		return this.rekamMedis;
	}

	public void setRekamMedis(String rekamMedis) {
		this.rekamMedis = rekamMedis;
	}

	public String getRuangan() {
		return this.ruangan;
	}

	public void setRuangan(String ruangan) {
		this.ruangan = ruangan;
	}

	public String getSifatCidera() {
		return this.sifatCidera;
	}

	public void setSifatCidera(String sifatCidera) {
		this.sifatCidera = sifatCidera;
	}

	public String getStatusJaminan() {
		return this.statusJaminan;
	}

	public void setStatusJaminan(String statusJaminan) {
		this.statusJaminan = statusJaminan;
	}

	public String getStatusKlaim() {
		return this.statusKlaim;
	}

	public void setStatusKlaim(String statusKlaim) {
		this.statusKlaim = statusKlaim;
	}

	public Timestamp getTglKejadian() {
		return this.tglKejadian;
	}

	public void setTglKejadian(Timestamp tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public Timestamp getTglMasukRs() {
		return this.tglMasukRs;
	}

	public void setTglMasukRs(Timestamp tglMasukRs) {
		this.tglMasukRs = tglMasukRs;
	}

	public String getTglProses() {
		return this.tglProses;
	}

	public void setTglProses(String tglProses) {
		this.tglProses = tglProses;
	}

	public String getTipeId() {
		return this.tipeId;
	}

	public void setTipeId(String tipeId) {
		this.tipeId = tipeId;
	}

	public String getUmur() {
		return this.umur;
	}

	public void setUmur(String umur) {
		this.umur = umur;
	}

	public String getUraian() {
		return this.uraian;
	}

	public void setUraian(String uraian) {
		this.uraian = uraian;
	}

}