package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_RS_LOCATION database table.
 * 
 */
@Entity
@Table(name="PL_RS_LOCATION")
@NamedQuery(name="PlRsLocation.findAll", query="SELECT p FROM PlRsLocation p")
public class PlRsLocation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KODE_RS")
	private String kodeRs;

	private String latitude;

	private String longtitude;

	public PlRsLocation() {
	}

	public String getKodeRs() {
		return this.kodeRs;
	}

	public void setKodeRs(String kodeRs) {
		this.kodeRs = kodeRs;
	}

	public String getLatitude() {
		return this.latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongtitude() {
		return this.longtitude;
	}

	public void setLongtitude(String longtitude) {
		this.longtitude = longtitude;
	}

}