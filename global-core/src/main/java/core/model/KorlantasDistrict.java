package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_DISTRICTS database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_DISTRICTS")
@NamedQuery(name="KorlantasDistrict.findAll", query="SELECT k FROM KorlantasDistrict k")
public class KorlantasDistrict implements Serializable {
	private static final long serialVersionUID = 1L;

	private String address;

	private BigDecimal archived;

	@Column(name="DISTRICT_NUMBER")
	private String districtNumber;

	@Column(name="GEO_PROVINCE_ID")
	private BigDecimal geoProvinceId;

	private BigDecimal gmt;

	@Column(name="GPS_LATITUDE")
	private BigDecimal gpsLatitude;

	@Column(name="GPS_LONGITUDE")
	private BigDecimal gpsLongitude;

	@Id
	private BigDecimal id;

	private String name;

	@Column(name="PROVINCE_ID")
	private BigDecimal provinceId;

	@Column(name="SORT_ORDER")
	private BigDecimal sortOrder;

	public KorlantasDistrict() {
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public BigDecimal getArchived() {
		return this.archived;
	}

	public void setArchived(BigDecimal archived) {
		this.archived = archived;
	}

	public String getDistrictNumber() {
		return this.districtNumber;
	}

	public void setDistrictNumber(String districtNumber) {
		this.districtNumber = districtNumber;
	}

	public BigDecimal getGeoProvinceId() {
		return this.geoProvinceId;
	}

	public void setGeoProvinceId(BigDecimal geoProvinceId) {
		this.geoProvinceId = geoProvinceId;
	}

	public BigDecimal getGmt() {
		return this.gmt;
	}

	public void setGmt(BigDecimal gmt) {
		this.gmt = gmt;
	}

	public BigDecimal getGpsLatitude() {
		return this.gpsLatitude;
	}

	public void setGpsLatitude(BigDecimal gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}

	public BigDecimal getGpsLongitude() {
		return this.gpsLongitude;
	}

	public void setGpsLongitude(BigDecimal gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getProvinceId() {
		return this.provinceId;
	}

	public void setProvinceId(BigDecimal provinceId) {
		this.provinceId = provinceId;
	}

	public BigDecimal getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}