package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the AUTH_GROUP_PERMISSION database table.
 * 
 */
public class AuthGroupPermissionPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String groupCode;

	private String elementCode;

	public AuthGroupPermissionPK() {
	}
	public String getGroupCode() {
		return this.groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}
	public String getElementCode() {
		return this.elementCode;
	}
	public void setElementCode(String elementCode) {
		this.elementCode = elementCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AuthGroupPermissionPK)) {
			return false;
		}
		AuthGroupPermissionPK castOther = (AuthGroupPermissionPK)other;
		return 
			this.groupCode.equals(castOther.groupCode)
			&& this.elementCode.equals(castOther.elementCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.groupCode.hashCode();
		hash = hash * prime + this.elementCode.hashCode();
		
		return hash;
	}
}