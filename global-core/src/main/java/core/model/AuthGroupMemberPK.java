package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the AUTH_GROUP_MEMBER database table.
 * 
 */
public class AuthGroupMemberPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String login;

	private String groupCode;

	public AuthGroupMemberPK() {
	}
	public String getLogin() {
		return this.login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getGroupCode() {
		return this.groupCode;
	}
	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof AuthGroupMemberPK)) {
			return false;
		}
		AuthGroupMemberPK castOther = (AuthGroupMemberPK)other;
		return 
			this.login.equals(castOther.login)
			&& this.groupCode.equals(castOther.groupCode);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.login.hashCode();
		hash = hash * prime + this.groupCode.hashCode();
		
		return hash;
	}
}