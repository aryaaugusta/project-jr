package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DUKCAPIL_WN_FAM database table.
 * 
 */
@Entity
@Table(name="DUKCAPIL_WN_FAM")
@NamedQuery(name="DukcapilWnFam.findAll", query="SELECT d FROM DukcapilWnFam d")
public class DukcapilWnFam implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="KODE_HUB_KEL")
	private String kodeHubKel;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_SYNC_DATE")
	private Date lastSyncDate;

	@Id
	private String nik;

	@Column(name="NO_KK")
	private String noKk;

	public DukcapilWnFam() {
	}

	public String getKodeHubKel() {
		return this.kodeHubKel;
	}

	public void setKodeHubKel(String kodeHubKel) {
		this.kodeHubKel = kodeHubKel;
	}

	public Date getLastSyncDate() {
		return this.lastSyncDate;
	}

	public void setLastSyncDate(Date lastSyncDate) {
		this.lastSyncDate = lastSyncDate;
	}

	public String getNik() {
		return this.nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNoKk() {
		return this.noKk;
	}

	public void setNoKk(String noKk) {
		this.noKk = noKk;
	}

}