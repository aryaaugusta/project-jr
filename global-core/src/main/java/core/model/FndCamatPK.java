package core.model;

import java.io.Serializable;

public class FndCamatPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kodeCamat;
	private String kodeKabkota;
	private String kodeProvinsi;
	public String getKodeCamat() {
		return kodeCamat;
	}
	public void setKodeCamat(String kodeCamat) {
		this.kodeCamat = kodeCamat;
	}
	public String getKodeKabkota() {
		return kodeKabkota;
	}
	public void setKodeKabkota(String kodeKabkota) {
		this.kodeKabkota = kodeKabkota;
	}
	public String getKodeProvinsi() {
		return kodeProvinsi;
	}
	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeCamat == null) ? 0 : kodeCamat.hashCode());
		result = prime * result
				+ ((kodeKabkota == null) ? 0 : kodeKabkota.hashCode());
		result = prime * result
				+ ((kodeProvinsi == null) ? 0 : kodeProvinsi.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FndCamatPK other = (FndCamatPK) obj;
		if (kodeCamat == null) {
			if (other.kodeCamat != null)
				return false;
		} else if (!kodeCamat.equals(other.kodeCamat))
			return false;
		if (kodeKabkota == null) {
			if (other.kodeKabkota != null)
				return false;
		} else if (!kodeKabkota.equals(other.kodeKabkota))
			return false;
		if (kodeProvinsi == null) {
			if (other.kodeProvinsi != null)
				return false;
		} else if (!kodeProvinsi.equals(other.kodeProvinsi))
			return false;
		return true;
	}
}
