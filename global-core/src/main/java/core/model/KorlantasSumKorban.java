package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the KORLANTAS_SUM_KORBAN database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_SUM_KORBAN")
@NamedQuery(name="KorlantasSumKorban.findAll", query="SELECT k FROM KorlantasSumKorban k")
public class KorlantasSumKorban implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name="ACCIDENT_DATE")
	private Date accidentDate;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="DISTRICT_ID")
	private BigDecimal districtId;

	@Column(name="JML_LL")
	private BigDecimal jmlLl;

	@Column(name="JML_MD")
	private BigDecimal jmlMd;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public KorlantasSumKorban() {
	}

	public Date getAccidentDate() {
		return this.accidentDate;
	}

	public void setAccidentDate(Date accidentDate) {
		this.accidentDate = accidentDate;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public BigDecimal getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(BigDecimal districtId) {
		this.districtId = districtId;
	}

	public BigDecimal getJmlLl() {
		return this.jmlLl;
	}

	public void setJmlLl(BigDecimal jmlLl) {
		this.jmlLl = jmlLl;
	}

	public BigDecimal getJmlMd() {
		return this.jmlMd;
	}

	public void setJmlMd(BigDecimal jmlMd) {
		this.jmlMd = jmlMd;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}