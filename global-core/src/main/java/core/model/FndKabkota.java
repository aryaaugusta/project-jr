package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FND_KABKOTA database table.
 * 
 */
@Entity
@Table(name="FND_KABKOTA")
@NamedQuery(name="FndKabkota.findAll", query="SELECT f FROM FndKabkota f")
public class FndKabkota implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KODE_KABKOTA")
	private String kodeKabkota;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	@Column(name="KODE_PROVINSI")
	private String kodeProvinsi;

	@Column(name="NAMA_KABKOTA")
	private String namaKabkota;

	public FndKabkota() {
	}

	public String getKodeKabkota() {
		return this.kodeKabkota;
	}

	public void setKodeKabkota(String kodeKabkota) {
		this.kodeKabkota = kodeKabkota;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getKodeProvinsi() {
		return this.kodeProvinsi;
	}

	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}

	public String getNamaKabkota() {
		return this.namaKabkota;
	}

	public void setNamaKabkota(String namaKabkota) {
		this.namaKabkota = namaKabkota;
	}

}