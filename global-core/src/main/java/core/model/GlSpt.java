package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the GL_SPT database table.
 * 
 */
@Entity
@Table(name="GL_SPT")
@NamedQuery(name="GlSpt.findAll", query="SELECT g FROM GlSpt g")
public class GlSpt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NO_SPT")
	private String noSpt;

	@Column(name="BANK_TUJUAN")
	private String bankTujuan;

	@Column(name="BILYET_GIRO")
	private String bilyetGiro;

	@Column(name="KODE_CABANG")
	private String kodeCabang;

	@Column(name="KOTA_BANK")
	private String kotaBank;

	@Column(name="KOTA_BRI")
	private String kotaBri;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="NO_SPB")
	private String noSpb;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_TRANSAKSI")
	private Date tglTransaksi;

	public GlSpt() {
	}

	public String getNoSpt() {
		return this.noSpt;
	}

	public void setNoSpt(String noSpt) {
		this.noSpt = noSpt;
	}

	public String getBankTujuan() {
		return this.bankTujuan;
	}

	public void setBankTujuan(String bankTujuan) {
		this.bankTujuan = bankTujuan;
	}

	public String getBilyetGiro() {
		return this.bilyetGiro;
	}

	public void setBilyetGiro(String bilyetGiro) {
		this.bilyetGiro = bilyetGiro;
	}

	public String getKodeCabang() {
		return this.kodeCabang;
	}

	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}

	public String getKotaBank() {
		return this.kotaBank;
	}

	public void setKotaBank(String kotaBank) {
		this.kotaBank = kotaBank;
	}

	public String getKotaBri() {
		return this.kotaBri;
	}

	public void setKotaBri(String kotaBri) {
		this.kotaBri = kotaBri;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getNoSpb() {
		return this.noSpb;
	}

	public void setNoSpb(String noSpb) {
		this.noSpb = noSpb;
	}

	public Date getTglTransaksi() {
		return this.tglTransaksi;
	}

	public void setTglTransaksi(Date tglTransaksi) {
		this.tglTransaksi = tglTransaksi;
	}

}