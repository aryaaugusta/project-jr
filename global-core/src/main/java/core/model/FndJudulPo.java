package core.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the FND_JUDUL_POS database table.
 */
@Entity
@Table(name = "FND_JUDUL_POS")
@NamedQuery(name = "FndJudulPo.findAll", query = "SELECT f FROM FndJudulPo f")
public class FndJudulPo implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_JUDUL_POS")
    private String idJudulPos;

    private String atribute1;

    private String atribute2;

    private String atribute3;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "CREATION_DATE")
    private Date creationDate;

    @Column(name = "ENABLE_POSTING")
    private String enablePosting;

    @Column(name = "JENIS_ANGGARAN")
    private String jenisAnggaran;

    @Column(name = "JENIS_POS")
    private String jenisPos;

    @Column(name = "JENIS_REPORT")
    private String jenisReport;

    private String keterangan;

    @Column(name = "LAST_UPDATED_BY")
    private String lastUpdatedBy;

    @Temporal(TemporalType.DATE)
    @Column(name = "LAST_UPDATED_DATE")
    private Date lastUpdatedDate;

    @Column(name = "NO_BARIS")
    private BigDecimal noBaris;

    @Column(name = "NO_KODE")
    private String noKode;

    @Column(name = "TIPE_SALDO")
    private String tipeSaldo;

    public FndJudulPo() {
    }

    public String getIdJudulPos() {
        return this.idJudulPos;
    }

    public void setIdJudulPos(String idJudulPos) {
        this.idJudulPos = idJudulPos;
    }

    public String getAtribute1() {
        return this.atribute1;
    }

    public void setAtribute1(String atribute1) {
        this.atribute1 = atribute1;
    }

    public String getAtribute2() {
        return this.atribute2;
    }

    public void setAtribute2(String atribute2) {
        this.atribute2 = atribute2;
    }

    public String getAtribute3() {
        return this.atribute3;
    }

    public void setAtribute3(String atribute3) {
        this.atribute3 = atribute3;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getEnablePosting() {
        return this.enablePosting;
    }

    public void setEnablePosting(String enablePosting) {
        this.enablePosting = enablePosting;
    }

    public String getJenisAnggaran() {
        return this.jenisAnggaran;
    }

    public void setJenisAnggaran(String jenisAnggaran) {
        this.jenisAnggaran = jenisAnggaran;
    }

    public String getJenisPos() {
        return this.jenisPos;
    }

    public void setJenisPos(String jenisPos) {
        this.jenisPos = jenisPos;
    }

    public String getJenisReport() {
        return this.jenisReport;
    }

    public void setJenisReport(String jenisReport) {
        this.jenisReport = jenisReport;
    }

    public String getKeterangan() {
        return this.keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getLastUpdatedBy() {
        return this.lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public Date getLastUpdatedDate() {
        return this.lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public BigDecimal getNoBaris() {
        return this.noBaris;
    }

    public void setNoBaris(BigDecimal noBaris) {
        this.noBaris = noBaris;
    }

    public String getNoKode() {
        return this.noKode;
    }

    public void setNoKode(String noKode) {
        this.noKode = noKode;
    }

    public String getTipeSaldo() {
        return this.tipeSaldo;
    }

    public void setTipeSaldo(String tipeSaldo) {
        this.tipeSaldo = tipeSaldo;
    }

}