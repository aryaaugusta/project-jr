package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_RS_RESPONSE_POL database table.
 * 
 */
@Entity
@Table(name="PL_RS_RESPONSE_POL")
@NamedQuery(name="PlRsResponsePol.findAll", query="SELECT p FROM PlRsResponsePol p")
public class PlRsResponsePol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private String keterangan;

	@Column(name="KODE_INSTANSI")
	private String kodeInstansi;

	@Column(name="KODE_INSTANSI_LIMPAH")
	private String kodeInstansiLimpah;

	@Column(name="KODE_KEJADIAN")
	private String kodeKejadian;

	@Id
	@Column(name="KODE_RESPONSE_POL")
	private String kodeResponsePol;

	@Column(name="KODE_RUMAH_SAKIT")
	private String kodeRumahSakit;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NAMA_PETUGAS")
	private String namaPetugas;

	@Column(name="NO_LAPORAN_POLISI")
	private String noLaporanPolisi;

	@Column(name="STATUS_RESPONSE")
	private String statusResponse;

	@Column(name="TGL_LAPORAN_POLISI")
	private String tglLaporanPolisi;

	@Column(name="TKP_LIMPAH")
	private String tkpLimpah;

	public PlRsResponsePol() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getKodeInstansi() {
		return this.kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public String getKodeInstansiLimpah() {
		return this.kodeInstansiLimpah;
	}

	public void setKodeInstansiLimpah(String kodeInstansiLimpah) {
		this.kodeInstansiLimpah = kodeInstansiLimpah;
	}

	public String getKodeKejadian() {
		return this.kodeKejadian;
	}

	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}

	public String getKodeResponsePol() {
		return this.kodeResponsePol;
	}

	public void setKodeResponsePol(String kodeResponsePol) {
		this.kodeResponsePol = kodeResponsePol;
	}

	public String getKodeRumahSakit() {
		return this.kodeRumahSakit;
	}

	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamaPetugas() {
		return this.namaPetugas;
	}

	public void setNamaPetugas(String namaPetugas) {
		this.namaPetugas = namaPetugas;
	}

	public String getNoLaporanPolisi() {
		return this.noLaporanPolisi;
	}

	public void setNoLaporanPolisi(String noLaporanPolisi) {
		this.noLaporanPolisi = noLaporanPolisi;
	}

	public String getStatusResponse() {
		return this.statusResponse;
	}

	public void setStatusResponse(String statusResponse) {
		this.statusResponse = statusResponse;
	}

	public String getTglLaporanPolisi() {
		return this.tglLaporanPolisi;
	}

	public void setTglLaporanPolisi(String tglLaporanPolisi) {
		this.tglLaporanPolisi = tglLaporanPolisi;
	}

	public String getTkpLimpah() {
		return this.tkpLimpah;
	}

	public void setTkpLimpah(String tkpLimpah) {
		this.tkpLimpah = tkpLimpah;
	}

}