package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the DACC_POS database table.
 * 
 */
@Entity
@Table(name="DACC_POS")
@NamedQuery(name="DaccPo.findAll", query="SELECT d FROM DaccPo d")
public class DaccPo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="POS_ID")
	private String posId;

	@Column(name="EVENT_ID")
	private String eventId;

	@Column(name="POS_LOCATION")
	private String posLocation;

	@Column(name="POS_NAME")
	private String posName;

	public DaccPo() {
	}

	public String getPosId() {
		return this.posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	public String getEventId() {
		return this.eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getPosLocation() {
		return this.posLocation;
	}

	public void setPosLocation(String posLocation) {
		this.posLocation = posLocation;
	}

	public String getPosName() {
		return this.posName;
	}

	public void setPosName(String posName) {
		this.posName = posName;
	}

}