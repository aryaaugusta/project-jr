package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_PERIODE_LOCK database table.
 * 
 */
@Entity
@Table(name="PL_PERIODE_LOCK")
@NamedQuery(name="PlPeriodeLock.findAll", query="SELECT p FROM PlPeriodeLock p")
public class PlPeriodeLock implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="PERIODE_BULAN")
	private String periodeBulan;

	@Column(name="PERIODE_TAHUN")
	private String periodeTahun;

	private String status;

	@Column(name="UPDATED_BY")
	private String updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public PlPeriodeLock() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getPeriodeBulan() {
		return this.periodeBulan;
	}

	public void setPeriodeBulan(String periodeBulan) {
		this.periodeBulan = periodeBulan;
	}

	public String getPeriodeTahun() {
		return this.periodeTahun;
	}

	public void setPeriodeTahun(String periodeTahun) {
		this.periodeTahun = periodeTahun;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}