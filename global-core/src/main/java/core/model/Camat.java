package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the CAMAT database table.
 * 
 */
@Entity
@NamedQuery(name="Camat.findAll", query="SELECT c FROM Camat c")
public class Camat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="CAMAT_KODE")
	private String camatKode;

	@Column(name="CAMAT_KABKOTA")
	private String camatKabkota;

	@Column(name="CAMAT_NAMA")
	private String camatNama;

	public Camat() {
	}

	public String getCamatKode() {
		return this.camatKode;
	}

	public void setCamatKode(String camatKode) {
		this.camatKode = camatKode;
	}

	public String getCamatKabkota() {
		return this.camatKabkota;
	}

	public void setCamatKabkota(String camatKabkota) {
		this.camatKabkota = camatKabkota;
	}

	public String getCamatNama() {
		return this.camatNama;
	}

	public void setCamatNama(String camatNama) {
		this.camatNama = camatNama;
	}

}