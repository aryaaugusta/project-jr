package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_REQUEST_PERUBAHAN database table.
 * 
 */
@Entity
@Table(name="PL_REQUEST_PERUBAHAN")
@NamedQuery(name="PlRequestPerubahan.findAll", query="SELECT p FROM PlRequestPerubahan p")
public class PlRequestPerubahan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Id
	@Column(name="ID_REQUEST")
	private String idRequest;

	@Column(name="KET_STATUS")
	private String ketStatus;

	private String keterangan;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="STATUS_REQUEST")
	private String statusRequest;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_UPDATE")
	private Date tglUpdate;

	@Column(name="UPDATED_BY")
	private String updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public PlRequestPerubahan() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getIdRequest() {
		return this.idRequest;
	}

	public void setIdRequest(String idRequest) {
		this.idRequest = idRequest;
	}

	public String getKetStatus() {
		return this.ketStatus;
	}

	public void setKetStatus(String ketStatus) {
		this.ketStatus = ketStatus;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getStatusRequest() {
		return this.statusRequest;
	}

	public void setStatusRequest(String statusRequest) {
		this.statusRequest = statusRequest;
	}

	public Date getTglUpdate() {
		return this.tglUpdate;
	}

	public void setTglUpdate(Date tglUpdate) {
		this.tglUpdate = tglUpdate;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}