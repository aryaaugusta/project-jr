package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_DOKUMEN_KECELAKAAN database table.
 * 
 */
@Entity
@Table(name="PL_DOKUMEN_KECELAKAAN")
@NamedQuery(name="PlDokumenKecelakaan.findAll", query="SELECT p FROM PlDokumenKecelakaan p")
public class PlDokumenKecelakaan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_DOKUMEN")
	private String idDokumen;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Lob
	private byte[] dokumen;

	@Column(name="ID_KECELAKAAN")
	private String idKecelakaan;

	@Column(name="ID_KORBAN_KECELAKAAN")
	private String idKorbanKecelakaan;

	private String keterangan;

	@Column(name="KODE_JENIS_DOKUMEN")
	private String kodeJenisDokumen;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NAMA_DOKUMEN")
	private String namaDokumen;

	@Column(name="REF_BY")
	private String refBy;

	@Column(name="TIPE_DOKUMEN")
	private String tipeDokumen;

	public PlDokumenKecelakaan() {
	}

	public String getIdDokumen() {
		return this.idDokumen;
	}

	public void setIdDokumen(String idDokumen) {
		this.idDokumen = idDokumen;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public byte[] getDokumen() {
		return this.dokumen;
	}

	public void setDokumen(byte[] dokumen) {
		this.dokumen = dokumen;
	}

	public String getIdKecelakaan() {
		return this.idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getIdKorbanKecelakaan() {
		return this.idKorbanKecelakaan;
	}

	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getKodeJenisDokumen() {
		return this.kodeJenisDokumen;
	}

	public void setKodeJenisDokumen(String kodeJenisDokumen) {
		this.kodeJenisDokumen = kodeJenisDokumen;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamaDokumen() {
		return this.namaDokumen;
	}

	public void setNamaDokumen(String namaDokumen) {
		this.namaDokumen = namaDokumen;
	}

	public String getRefBy() {
		return this.refBy;
	}

	public void setRefBy(String refBy) {
		this.refBy = refBy;
	}

	public String getTipeDokumen() {
		return this.tipeDokumen;
	}

	public void setTipeDokumen(String tipeDokumen) {
		this.tipeDokumen = tipeDokumen;
	}

}