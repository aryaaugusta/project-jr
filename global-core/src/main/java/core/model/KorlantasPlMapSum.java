package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the KORLANTAS_PL_MAP_SUM database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_PL_MAP_SUM")
@NamedQuery(name="KorlantasPlMapSum.findAll", query="SELECT k FROM KorlantasPlMapSum k")
public class KorlantasPlMapSum implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name="ACCIDENT_DATE")
	private Date accidentDate;

	@Column(name="DISTRICT_ID")
	private BigDecimal districtId;

	@Column(name="JML_LAKA_DIF")
	private BigDecimal jmlLakaDif;

	public KorlantasPlMapSum() {
	}

	public Date getAccidentDate() {
		return this.accidentDate;
	}

	public void setAccidentDate(Date accidentDate) {
		this.accidentDate = accidentDate;
	}

	public BigDecimal getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(BigDecimal districtId) {
		this.districtId = districtId;
	}

	public BigDecimal getJmlLakaDif() {
		return this.jmlLakaDif;
	}

	public void setJmlLakaDif(BigDecimal jmlLakaDif) {
		this.jmlLakaDif = jmlLakaDif;
	}

}