package core.model;

import java.io.Serializable;

public class PlRsMapBpjPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String kodeBpjs;
	
	public String getKodeBpjs() {
		return kodeBpjs;
	}
	public void setKodeBpjs(String kodeBpjs) {
		this.kodeBpjs = kodeBpjs;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeBpjs == null) ? 0 : kodeBpjs.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlRsMapBpjPK other = (PlRsMapBpjPK) obj;
		if (kodeBpjs == null) {
			if (other.kodeBpjs != null)
				return false;
		} else if (!kodeBpjs.equals(other.kodeBpjs))
			return false;
		return true;
	}
	
	

}
