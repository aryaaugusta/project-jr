package core.model;

import java.io.Serializable;
import java.math.BigDecimal;


public class PlDashOtorisasiPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal jmlBerkas;
	private String kodeKantor;
	private String otorisasi;
	public BigDecimal getJmlBerkas() {
		return jmlBerkas;
	}
	public void setJmlBerkas(BigDecimal jmlBerkas) {
		this.jmlBerkas = jmlBerkas;
	}
	public String getKodeKantor() {
		return kodeKantor;
	}
	public void setKodeKantor(String kodeKantor) {
		this.kodeKantor = kodeKantor;
	}
	public String getOtorisasi() {
		return otorisasi;
	}
	public void setOtorisasi(String otorisasi) {
		this.otorisasi = otorisasi;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeKantor == null) ? 0 : kodeKantor.hashCode());
		result = prime * result
				+ ((otorisasi == null) ? 0 : otorisasi.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlDashOtorisasiPK other = (PlDashOtorisasiPK) obj;
		if (kodeKantor == null) {
			if (other.kodeKantor != null)
				return false;
		} else if (!kodeKantor.equals(other.kodeKantor))
			return false;
		if (otorisasi == null) {
			if (other.otorisasi != null)
				return false;
		} else if (!otorisasi.equals(other.otorisasi))
			return false;
		return true;
	}
}
