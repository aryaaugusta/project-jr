package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_PENYELESAIAN_SANTUNAN database table.
 * 
 */
@Entity
@Table(name="PL_PENYELESAIAN_SANTUNAN")
@NamedQuery(name="PlPenyelesaianSantunan.findAll", query="SELECT p FROM PlPenyelesaianSantunan p")
public class PlPenyelesaianSantunan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	//@Column(name="NO_BERKAS", insertable=true, updatable=true, unique=true, nullable=false)
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="ID_GUID")
	private String idGuid;

	@Column(name="JENIS_PEMBAYARAN")
	private String jenisPembayaran;

	@Column(name="JML_BYR_AMBL")
	private BigDecimal jmlByrAmbl;

	@Column(name="JML_BYR_P3K")
	private BigDecimal jmlByrP3k;

	@Column(name="JUMLAH_DIBAYAR_LUKALUKA")
	private BigDecimal jumlahDibayarLukaluka;

	@Column(name="JUMLAH_DIBAYAR_MENINGGAL")
	private BigDecimal jumlahDibayarMeninggal;

	@Column(name="JUMLAH_DIBAYAR_PENGUBURAN")
	private BigDecimal jumlahDibayarPenguburan;

	@Column(name="KELEBIHAN_BAYAR")
	private BigDecimal kelebihanBayar;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NO_BPK")
	private String noBpk;

	@Column(name="NO_REKENING")
	private String noRekening;

	@Column(name="NO_SURAT_PENYELESAIAN")
	private String noSuratPenyelesaian;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_KELEBIHAN_BAYAR")
	private Date tglKelebihanBayar;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_PEMBUATAN_BPK")
	private Date tglPembuatanBpk;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_PROSES")
	private Date tglProses;

	public PlPenyelesaianSantunan() {
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdGuid() {
		return this.idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public String getJenisPembayaran() {
		return this.jenisPembayaran;
	}

	public void setJenisPembayaran(String jenisPembayaran) {
		this.jenisPembayaran = jenisPembayaran;
	}

	public BigDecimal getJmlByrAmbl() {
		return this.jmlByrAmbl;
	}

	public void setJmlByrAmbl(BigDecimal jmlByrAmbl) {
		this.jmlByrAmbl = jmlByrAmbl;
	}

	public BigDecimal getJmlByrP3k() {
		return this.jmlByrP3k;
	}

	public void setJmlByrP3k(BigDecimal jmlByrP3k) {
		this.jmlByrP3k = jmlByrP3k;
	}

	public BigDecimal getJumlahDibayarLukaluka() {
		return this.jumlahDibayarLukaluka;
	}

	public void setJumlahDibayarLukaluka(BigDecimal jumlahDibayarLukaluka) {
		this.jumlahDibayarLukaluka = jumlahDibayarLukaluka;
	}

	public BigDecimal getJumlahDibayarMeninggal() {
		return this.jumlahDibayarMeninggal;
	}

	public void setJumlahDibayarMeninggal(BigDecimal jumlahDibayarMeninggal) {
		this.jumlahDibayarMeninggal = jumlahDibayarMeninggal;
	}

	public BigDecimal getJumlahDibayarPenguburan() {
		return this.jumlahDibayarPenguburan;
	}

	public void setJumlahDibayarPenguburan(BigDecimal jumlahDibayarPenguburan) {
		this.jumlahDibayarPenguburan = jumlahDibayarPenguburan;
	}

	public BigDecimal getKelebihanBayar() {
		return this.kelebihanBayar;
	}

	public void setKelebihanBayar(BigDecimal kelebihanBayar) {
		this.kelebihanBayar = kelebihanBayar;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNoBpk() {
		return this.noBpk;
	}

	public void setNoBpk(String noBpk) {
		this.noBpk = noBpk;
	}

	public String getNoRekening() {
		return this.noRekening;
	}

	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}

	public String getNoSuratPenyelesaian() {
		return this.noSuratPenyelesaian;
	}

	public void setNoSuratPenyelesaian(String noSuratPenyelesaian) {
		this.noSuratPenyelesaian = noSuratPenyelesaian;
	}

	public Date getTglKelebihanBayar() {
		return this.tglKelebihanBayar;
	}

	public void setTglKelebihanBayar(Date tglKelebihanBayar) {
		this.tglKelebihanBayar = tglKelebihanBayar;
	}

	public Date getTglPembuatanBpk() {
		return this.tglPembuatanBpk;
	}

	public void setTglPembuatanBpk(Date tglPembuatanBpk) {
		this.tglPembuatanBpk = tglPembuatanBpk;
	}

	public Date getTglProses() {
		return this.tglProses;
	}

	public void setTglProses(Date tglProses) {
		this.tglProses = tglProses;
	}

}