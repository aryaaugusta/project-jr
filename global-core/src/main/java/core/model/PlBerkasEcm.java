package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_BERKAS_ECMS database table.
 * 
 */
@Entity
@Table(name="PL_BERKAS_ECMS")
@NamedQuery(name="PlBerkasEcm.findAll", query="SELECT p FROM PlBerkasEcm p")
public class PlBerkasEcm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="DDOCNAME")
	private String ddocname;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="FILEPATH")
	private String filepath;

	@Column(name="NAMAFILE")
	private String namafile;

	@Column(name="NO_REG_ECMS")
	private String noRegEcms;

	public PlBerkasEcm() {
	}

	public String getDdocname() {
		return this.ddocname;
	}

	public void setDdocname(String ddocname) {
		this.ddocname = ddocname;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getFilepath() {
		return this.filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getNamafile() {
		return this.namafile;
	}

	public void setNamafile(String namafile) {
		this.namafile = namafile;
	}

	public String getNoRegEcms() {
		return this.noRegEcms;
	}

	public void setNoRegEcms(String noRegEcms) {
		this.noRegEcms = noRegEcms;
	}

}