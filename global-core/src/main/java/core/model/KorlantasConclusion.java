package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the KORLANTAS_CONCLUSION database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_CONCLUSION")
@NamedQuery(name="KorlantasConclusion.findAll", query="SELECT k FROM KorlantasConclusion k")
public class KorlantasConclusion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ACCIDENT_ID")
	private BigDecimal accidentId;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="OFFICER_ID")
	private BigDecimal officerId;

	private String src;

	public KorlantasConclusion() {
	}

	public BigDecimal getAccidentId() {
		return this.accidentId;
	}

	public void setAccidentId(BigDecimal accidentId) {
		this.accidentId = accidentId;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getOfficerId() {
		return this.officerId;
	}

	public void setOfficerId(BigDecimal officerId) {
		this.officerId = officerId;
	}

	public String getSrc() {
		return this.src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

}