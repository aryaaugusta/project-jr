package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PL_REGISTER_BERKAS database table.
 * 
 */
public class PlRegisterBerkaPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String noRegister;

	private String kodeBerkas;

	public PlRegisterBerkaPK() {
	}
	public String getNoRegister() {
		return this.noRegister;
	}
	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}
	public String getKodeBerkas() {
		return this.kodeBerkas;
	}
	public void setKodeBerkas(String kodeBerkas) {
		this.kodeBerkas = kodeBerkas;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PlRegisterBerkaPK)) {
			return false;
		}
		PlRegisterBerkaPK castOther = (PlRegisterBerkaPK)other;
		return 
			this.noRegister.equals(castOther.noRegister)
			&& this.kodeBerkas.equals(castOther.kodeBerkas);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.noRegister.hashCode();
		hash = hash * prime + this.kodeBerkas.hashCode();
		
		return hash;
	}
}