package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_PENGAJUAN_SANTUNAN database table.
 * 
 */
@Entity
@Table(name="PL_PENGAJUAN_SANTUNAN")
@NamedQuery(name="PlPengajuanSantunan.findAll", query="SELECT p FROM PlPengajuanSantunan p")
@IdClass(PlPengajuanSantunanPK.class)
public class PlPengajuanSantunan implements Serializable {
	private static final long serialVersionUID = 1L;

//	@EmbeddedId
//	private PlPengajuanSantunanPK id;

	@Id
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Id
	@Column(name="TRANSISI_FLAG")
	private String transisiFlag;
	
	@Column(name="ABSAH_FLAG")
	private String absahFlag;

	@Column(name="ALAMAT_PEMOHON")
	private String alamatPemohon;

	@Column(name="BEBAN_PUSAT_FLAG")
	private String bebanPusatFlag;

	@Column(name="CIDERA_KORBAN")
	private String cideraKorban;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="DIAJUKAN_DI")
	private String diajukanDi;

	@Column(name="DILIMPAHKAN_KE")
	private String dilimpahkanKe;

	@Column(name="FLAG_HASIL_SURVEY")
	private String flagHasilSurvey;

	@Column(name="ID_GUID")
	private String idGuid;

	@Column(name="ID_KECELAKAAN")
	private String idKecelakaan;

	@Column(name="ID_KORBAN_KECELAKAAN")
	private String idKorbanKecelakaan;

	@Column(name="JAMINAN_GANDA_FLAG")
	private String jaminanGandaFlag;

	@Column(name="JAMINAN_PEMBAYARAN")
	private String jaminanPembayaran;

	@Column(name="JENIS_IDENTITAS")
	private String jenisIdentitas;

	@Column(name="JML_PENGAJUAN_AMBL")
	private BigDecimal jmlPengajuanAmbl;

	@Column(name="JML_PENGAJUAN_P3K")
	private BigDecimal jmlPengajuanP3k;

	@Column(name="JML_REKOMEN_AMBL")
	private BigDecimal jmlRekomenAmbl;

	@Column(name="JML_REKOMEN_P3K")
	private BigDecimal jmlRekomenP3k;

	@Column(name="JUMLAH_PENGAJUAN_LUKALUKA")
	private BigDecimal jumlahPengajuanLukaluka;

	@Column(name="JUMLAH_PENGAJUAN_MENINGGAL")
	private BigDecimal jumlahPengajuanMeninggal;

	@Column(name="JUMLAH_PENGAJUAN_PENGUBURAN")
	private BigDecimal jumlahPengajuanPenguburan;

	@Column(name="KESIMPULAN_SEMENTARA")
	private String kesimpulanSementara;

	@Column(name="KODE_HUBUNGAN_KORBAN")
	private String kodeHubunganKorban;

	@Column(name="KODE_JAMINAN")
	private String kodeJaminan;

	@Column(name="KODE_JENIS_SURVEY")
	private String kodeJenisSurvey;

	@Column(name="KODE_PENGAJUAN")
	private String kodePengajuan;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="LENGKAP_FLAG")
	private String lengkapFlag;

	@Column(name="NAMA_PEMOHON")
	private String namaPemohon;

	@Column(name="NO_IDENTITAS")
	private String noIdentitas;

	@Column(name="NO_PENGAJUAN")
	private String noPengajuan;

	@Column(name="NO_SURAT_SURVEY")
	private String noSuratSurvey;

	@Column(name="OTORISASI_FLAG")
	private String otorisasiFlag;

	@Column(name="REKOMENDASI_FLAG")
	private String rekomendasiFlag;

	@Column(name="REKOMENDASI_JAMINAN")
	private String rekomendasiJaminan;

	@Column(name="REKOMENDASI_JML_LUKALUKA")
	private BigDecimal rekomendasiJmlLukaluka;

	@Column(name="REKOMENDASI_JML_MENINGGAL")
	private BigDecimal rekomendasiJmlMeninggal;

	@Column(name="REKOMENDASI_JML_PENGUBURAN")
	private BigDecimal rekomendasiJmlPenguburan;

	@Column(name="REKOMENDASI_SEMENTARA")
	private String rekomendasiSementara;

	@Column(name="SEQ_NO")
	private BigDecimal seqNo;

	@Column(name="STATUS_PROSES")
	private String statusProses;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_PENERIMAAN")
	private Date tglPenerimaan;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_PENGAJUAN")
	private Date tglPengajuan;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_PENYELESAIAN")
	private Date tglPenyelesaian;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_SURAT_SURVEY")
	private Date tglSuratSurvey;

	@Column(name="TIPE_PENGAJUAN")
	private String tipePengajuan;

	public PlPengajuanSantunan() {
	}

	public String getAbsahFlag() {
		return this.absahFlag;
	}

	public void setAbsahFlag(String absahFlag) {
		this.absahFlag = absahFlag;
	}

	public String getAlamatPemohon() {
		return this.alamatPemohon;
	}

	public void setAlamatPemohon(String alamatPemohon) {
		this.alamatPemohon = alamatPemohon;
	}

	public String getBebanPusatFlag() {
		return this.bebanPusatFlag;
	}

	public void setBebanPusatFlag(String bebanPusatFlag) {
		this.bebanPusatFlag = bebanPusatFlag;
	}

	public String getCideraKorban() {
		return this.cideraKorban;
	}

	public void setCideraKorban(String cideraKorban) {
		this.cideraKorban = cideraKorban;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDiajukanDi() {
		return this.diajukanDi;
	}

	public void setDiajukanDi(String diajukanDi) {
		this.diajukanDi = diajukanDi;
	}

	public String getDilimpahkanKe() {
		return this.dilimpahkanKe;
	}

	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}

	public String getFlagHasilSurvey() {
		return this.flagHasilSurvey;
	}

	public void setFlagHasilSurvey(String flagHasilSurvey) {
		this.flagHasilSurvey = flagHasilSurvey;
	}

	public String getIdGuid() {
		return this.idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public String getIdKecelakaan() {
		return this.idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getIdKorbanKecelakaan() {
		return this.idKorbanKecelakaan;
	}

	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}

	public String getJaminanGandaFlag() {
		return this.jaminanGandaFlag;
	}

	public void setJaminanGandaFlag(String jaminanGandaFlag) {
		this.jaminanGandaFlag = jaminanGandaFlag;
	}

	public String getJaminanPembayaran() {
		return this.jaminanPembayaran;
	}

	public void setJaminanPembayaran(String jaminanPembayaran) {
		this.jaminanPembayaran = jaminanPembayaran;
	}

	public String getJenisIdentitas() {
		return this.jenisIdentitas;
	}

	public void setJenisIdentitas(String jenisIdentitas) {
		this.jenisIdentitas = jenisIdentitas;
	}

	public BigDecimal getJmlPengajuanAmbl() {
		return this.jmlPengajuanAmbl;
	}

	public void setJmlPengajuanAmbl(BigDecimal jmlPengajuanAmbl) {
		this.jmlPengajuanAmbl = jmlPengajuanAmbl;
	}

	public BigDecimal getJmlPengajuanP3k() {
		return this.jmlPengajuanP3k;
	}

	public void setJmlPengajuanP3k(BigDecimal jmlPengajuanP3k) {
		this.jmlPengajuanP3k = jmlPengajuanP3k;
	}

	public BigDecimal getJmlRekomenAmbl() {
		return this.jmlRekomenAmbl;
	}

	public void setJmlRekomenAmbl(BigDecimal jmlRekomenAmbl) {
		this.jmlRekomenAmbl = jmlRekomenAmbl;
	}

	public BigDecimal getJmlRekomenP3k() {
		return this.jmlRekomenP3k;
	}

	public void setJmlRekomenP3k(BigDecimal jmlRekomenP3k) {
		this.jmlRekomenP3k = jmlRekomenP3k;
	}

	public BigDecimal getJumlahPengajuanLukaluka() {
		return this.jumlahPengajuanLukaluka;
	}

	public void setJumlahPengajuanLukaluka(BigDecimal jumlahPengajuanLukaluka) {
		this.jumlahPengajuanLukaluka = jumlahPengajuanLukaluka;
	}

	public BigDecimal getJumlahPengajuanMeninggal() {
		return this.jumlahPengajuanMeninggal;
	}

	public void setJumlahPengajuanMeninggal(BigDecimal jumlahPengajuanMeninggal) {
		this.jumlahPengajuanMeninggal = jumlahPengajuanMeninggal;
	}

	public BigDecimal getJumlahPengajuanPenguburan() {
		return this.jumlahPengajuanPenguburan;
	}

	public void setJumlahPengajuanPenguburan(BigDecimal jumlahPengajuanPenguburan) {
		this.jumlahPengajuanPenguburan = jumlahPengajuanPenguburan;
	}

	public String getKesimpulanSementara() {
		return this.kesimpulanSementara;
	}

	public void setKesimpulanSementara(String kesimpulanSementara) {
		this.kesimpulanSementara = kesimpulanSementara;
	}

	public String getKodeHubunganKorban() {
		return this.kodeHubunganKorban;
	}

	public void setKodeHubunganKorban(String kodeHubunganKorban) {
		this.kodeHubunganKorban = kodeHubunganKorban;
	}

	public String getKodeJaminan() {
		return this.kodeJaminan;
	}

	public void setKodeJaminan(String kodeJaminan) {
		this.kodeJaminan = kodeJaminan;
	}

	public String getKodeJenisSurvey() {
		return this.kodeJenisSurvey;
	}

	public void setKodeJenisSurvey(String kodeJenisSurvey) {
		this.kodeJenisSurvey = kodeJenisSurvey;
	}

	public String getKodePengajuan() {
		return this.kodePengajuan;
	}

	public void setKodePengajuan(String kodePengajuan) {
		this.kodePengajuan = kodePengajuan;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLengkapFlag() {
		return this.lengkapFlag;
	}

	public void setLengkapFlag(String lengkapFlag) {
		this.lengkapFlag = lengkapFlag;
	}

	public String getNamaPemohon() {
		return this.namaPemohon;
	}

	public void setNamaPemohon(String namaPemohon) {
		this.namaPemohon = namaPemohon;
	}

	public String getNoIdentitas() {
		return this.noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getNoPengajuan() {
		return this.noPengajuan;
	}

	public void setNoPengajuan(String noPengajuan) {
		this.noPengajuan = noPengajuan;
	}

	public String getNoSuratSurvey() {
		return this.noSuratSurvey;
	}

	public void setNoSuratSurvey(String noSuratSurvey) {
		this.noSuratSurvey = noSuratSurvey;
	}

	public String getOtorisasiFlag() {
		return this.otorisasiFlag;
	}

	public void setOtorisasiFlag(String otorisasiFlag) {
		this.otorisasiFlag = otorisasiFlag;
	}

	public String getRekomendasiFlag() {
		return this.rekomendasiFlag;
	}

	public void setRekomendasiFlag(String rekomendasiFlag) {
		this.rekomendasiFlag = rekomendasiFlag;
	}

	public String getRekomendasiJaminan() {
		return this.rekomendasiJaminan;
	}

	public void setRekomendasiJaminan(String rekomendasiJaminan) {
		this.rekomendasiJaminan = rekomendasiJaminan;
	}

	public BigDecimal getRekomendasiJmlLukaluka() {
		return this.rekomendasiJmlLukaluka;
	}

	public void setRekomendasiJmlLukaluka(BigDecimal rekomendasiJmlLukaluka) {
		this.rekomendasiJmlLukaluka = rekomendasiJmlLukaluka;
	}

	public BigDecimal getRekomendasiJmlMeninggal() {
		return this.rekomendasiJmlMeninggal;
	}

	public void setRekomendasiJmlMeninggal(BigDecimal rekomendasiJmlMeninggal) {
		this.rekomendasiJmlMeninggal = rekomendasiJmlMeninggal;
	}

	public BigDecimal getRekomendasiJmlPenguburan() {
		return this.rekomendasiJmlPenguburan;
	}

	public void setRekomendasiJmlPenguburan(BigDecimal rekomendasiJmlPenguburan) {
		this.rekomendasiJmlPenguburan = rekomendasiJmlPenguburan;
	}

	public String getRekomendasiSementara() {
		return this.rekomendasiSementara;
	}

	public void setRekomendasiSementara(String rekomendasiSementara) {
		this.rekomendasiSementara = rekomendasiSementara;
	}

	public BigDecimal getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public String getStatusProses() {
		return this.statusProses;
	}

	public void setStatusProses(String statusProses) {
		this.statusProses = statusProses;
	}

	public Date getTglPenerimaan() {
		return this.tglPenerimaan;
	}

	public void setTglPenerimaan(Date tglPenerimaan) {
		this.tglPenerimaan = tglPenerimaan;
	}

	public Date getTglPengajuan() {
		return this.tglPengajuan;
	}

	public void setTglPengajuan(Date tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}

	public Date getTglPenyelesaian() {
		return this.tglPenyelesaian;
	}

	public void setTglPenyelesaian(Date tglPenyelesaian) {
		this.tglPenyelesaian = tglPenyelesaian;
	}

	public Date getTglSuratSurvey() {
		return this.tglSuratSurvey;
	}

	public void setTglSuratSurvey(Date tglSuratSurvey) {
		this.tglSuratSurvey = tglSuratSurvey;
	}

	public String getTipePengajuan() {
		return this.tipePengajuan;
	}

	public void setTipePengajuan(String tipePengajuan) {
		this.tipePengajuan = tipePengajuan;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getTransisiFlag() {
		return transisiFlag;
	}

	public void setTransisiFlag(String transisiFlag) {
		this.transisiFlag = transisiFlag;
	}

}