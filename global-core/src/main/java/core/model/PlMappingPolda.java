package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_MAPPING_POLDA database table.
 * 
 */
@Entity
@Table(name="PL_MAPPING_POLDA")
@NamedQuery(name="PlMappingPolda.findAll", query="SELECT p FROM PlMappingPolda p")
public class PlMappingPolda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KODE_INSTANSI")
	private String kodeInstansi;

	private String deskripsi;

	@Column(name="ID_DISTRICTS")
	private String idDistricts;

	@Column(name="KODE_POLDA")
	private String kodePolda;

	@Column(name="NAMA_POLDA")
	private String namaPolda;

	@Column(name="WILAYAH_KPJR")
	private String wilayahKpjr;

	public PlMappingPolda() {
	}

	public String getKodeInstansi() {
		return this.kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public String getDeskripsi() {
		return this.deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getIdDistricts() {
		return this.idDistricts;
	}

	public void setIdDistricts(String idDistricts) {
		this.idDistricts = idDistricts;
	}

	public String getKodePolda() {
		return this.kodePolda;
	}

	public void setKodePolda(String kodePolda) {
		this.kodePolda = kodePolda;
	}

	public String getNamaPolda() {
		return this.namaPolda;
	}

	public void setNamaPolda(String namaPolda) {
		this.namaPolda = namaPolda;
	}

	public String getWilayahKpjr() {
		return this.wilayahKpjr;
	}

	public void setWilayahKpjr(String wilayahKpjr) {
		this.wilayahKpjr = wilayahKpjr;
	}

}