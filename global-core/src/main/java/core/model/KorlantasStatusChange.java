package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the KORLANTAS_STATUS_CHANGES database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_STATUS_CHANGES")
@NamedQuery(name="KorlantasStatusChange.findAll", query="SELECT k FROM KorlantasStatusChange k")
public class KorlantasStatusChange implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Id
	@Column(name="ID_TB")
	private BigDecimal idTb;

	@Column(name="STATUS_TB")
	private String statusTb;

	private String tablename;

	public KorlantasStatusChange() {
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getIdTb() {
		return this.idTb;
	}

	public void setIdTb(BigDecimal idTb) {
		this.idTb = idTb;
	}

	public String getStatusTb() {
		return this.statusTb;
	}

	public void setStatusTb(String statusTb) {
		this.statusTb = statusTb;
	}

	public String getTablename() {
		return this.tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

}