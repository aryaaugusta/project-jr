package core.model;

import java.io.Serializable;
import java.util.Date;


public class KorlantasSyncManualPK implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String accidentNo;
	private String createdBy;
	private Date createdDate;
	private Date lastUpdatedDate;
	private String statusReq;
	public String getAccidentNo() {
		return accidentNo;
	}
	public void setAccidentNo(String accidentNo) {
		this.accidentNo = accidentNo;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getStatusReq() {
		return statusReq;
	}
	public void setStatusReq(String statusReq) {
		this.statusReq = statusReq;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accidentNo == null) ? 0 : accidentNo.hashCode());
		result = prime * result
				+ ((statusReq == null) ? 0 : statusReq.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KorlantasSyncManualPK other = (KorlantasSyncManualPK) obj;
		if (accidentNo == null) {
			if (other.accidentNo != null)
				return false;
		} else if (!accidentNo.equals(other.accidentNo))
			return false;
		if (statusReq == null) {
			if (other.statusReq != null)
				return false;
		} else if (!statusReq.equals(other.statusReq))
			return false;
		return true;
	}
}
