package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FND_BANK database table.
 * 
 */
@Entity
@Table(name="FND_BANK")
@NamedQuery(name="FndBank.findAll", query="SELECT f FROM FndBank f")
public class FndBank implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KODE_BANK")
	private String kodeBank;

	@Column(name="KODE_BANK_2")
	private String kodeBank2;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NAMA_BANK")
	private String namaBank;

	private String status;

	public FndBank() {
	}

	public String getKodeBank() {
		return this.kodeBank;
	}

	public void setKodeBank(String kodeBank) {
		this.kodeBank = kodeBank;
	}

	public String getKodeBank2() {
		return this.kodeBank2;
	}

	public void setKodeBank2(String kodeBank2) {
		this.kodeBank2 = kodeBank2;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamaBank() {
		return this.namaBank;
	}

	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}