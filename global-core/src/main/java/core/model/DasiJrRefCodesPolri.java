package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the DASI_JR_REF_CODES_POLRI database table.
 * 
 */
@Entity
@Table(name="DASI_JR_REF_CODES_POLRI")
@NamedQuery(name="DasiJrRefCodesPolri.findAll", query="SELECT d FROM DasiJrRefCodesPolri d")
public class DasiJrRefCodesPolri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	@Column(name="RV_ABBREVIATION")
	private String rvAbbreviation;

	@Id
	@Column(name="RV_DOMAIN")
	private String rvDomain;

	@Column(name="RV_HIGH_VALUE")
	private String rvHighValue;

	@Column(name="RV_LOW_VALUE")
	private String rvLowValue;

	@Column(name="RV_MEANING")
	private String rvMeaning;

	public DasiJrRefCodesPolri() {
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getRvAbbreviation() {
		return this.rvAbbreviation;
	}

	public void setRvAbbreviation(String rvAbbreviation) {
		this.rvAbbreviation = rvAbbreviation;
	}

	public String getRvDomain() {
		return this.rvDomain;
	}

	public void setRvDomain(String rvDomain) {
		this.rvDomain = rvDomain;
	}

	public String getRvHighValue() {
		return this.rvHighValue;
	}

	public void setRvHighValue(String rvHighValue) {
		this.rvHighValue = rvHighValue;
	}

	public String getRvLowValue() {
		return this.rvLowValue;
	}

	public void setRvLowValue(String rvLowValue) {
		this.rvLowValue = rvLowValue;
	}

	public String getRvMeaning() {
		return this.rvMeaning;
	}

	public void setRvMeaning(String rvMeaning) {
		this.rvMeaning = rvMeaning;
	}

}