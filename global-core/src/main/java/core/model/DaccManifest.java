package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the DACC_MANIFEST database table.
 * 
 */
@Entity
@Table(name="DACC_MANIFEST")
@NamedQuery(name="DaccManifest.findAll", query="SELECT d FROM DaccManifest d")
public class DaccManifest implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NO_MANIFEST")
	private String noManifest;

	@Column(name="ALAMAT_KORBAN")
	private String alamatKorban;

	@Column(name="ALAMAT_PELAPOR")
	private String alamatPelapor;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="ID_KORBAN_KECELAKAAN")
	private String idKorbanKecelakaan;

	@Column(name="JENIS_KELAMIN")
	private String jenisKelamin;

	@Column(name="KODE_HUBUNGAN")
	private String kodeHubungan;

	@Column(name="NAMA_PELAPOR")
	private String namaPelapor;

	@Column(name="NAMA_PNP")
	private String namaPnp;

	@Column(name="NIK_KORBAN")
	private String nikKorban;

	@Column(name="NIK_PELAPOR")
	private String nikPelapor;

	@Column(name="NO_TELP")
	private String noTelp;

	@Column(name="POS_ID")
	private String posId;

	@Column(name="UMUR_KORBAN")
	private String umurKorban;

	public DaccManifest() {
	}

	public String getNoManifest() {
		return this.noManifest;
	}

	public void setNoManifest(String noManifest) {
		this.noManifest = noManifest;
	}

	public String getAlamatKorban() {
		return this.alamatKorban;
	}

	public void setAlamatKorban(String alamatKorban) {
		this.alamatKorban = alamatKorban;
	}

	public String getAlamatPelapor() {
		return this.alamatPelapor;
	}

	public void setAlamatPelapor(String alamatPelapor) {
		this.alamatPelapor = alamatPelapor;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getIdKorbanKecelakaan() {
		return this.idKorbanKecelakaan;
	}

	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}

	public String getJenisKelamin() {
		return this.jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getKodeHubungan() {
		return this.kodeHubungan;
	}

	public void setKodeHubungan(String kodeHubungan) {
		this.kodeHubungan = kodeHubungan;
	}

	public String getNamaPelapor() {
		return this.namaPelapor;
	}

	public void setNamaPelapor(String namaPelapor) {
		this.namaPelapor = namaPelapor;
	}

	public String getNamaPnp() {
		return this.namaPnp;
	}

	public void setNamaPnp(String namaPnp) {
		this.namaPnp = namaPnp;
	}

	public String getNikKorban() {
		return this.nikKorban;
	}

	public void setNikKorban(String nikKorban) {
		this.nikKorban = nikKorban;
	}

	public String getNikPelapor() {
		return this.nikPelapor;
	}

	public void setNikPelapor(String nikPelapor) {
		this.nikPelapor = nikPelapor;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public String getPosId() {
		return this.posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	public String getUmurKorban() {
		return this.umurKorban;
	}

	public void setUmurKorban(String umurKorban) {
		this.umurKorban = umurKorban;
	}

}