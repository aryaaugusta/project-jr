package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the FND_KANTOR_DAY_OFF database table.
 * 
 */
@Entity
@Table(name="FND_KANTOR_DAY_OFF")
@NamedQuery(name="FndKantorDayOff.findAll", query="SELECT f FROM FndKantorDayOff f")
public class FndKantorDayOff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_KD_OFF")
	private String idKdOff;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	private String ket;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="STATUS_OP")
	private String statusOp;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_AKHIR")
	private Date tglAkhir;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_AWAL")
	private Date tglAwal;

	public FndKantorDayOff() {
	}

	public String getIdKdOff() {
		return this.idKdOff;
	}

	public void setIdKdOff(String idKdOff) {
		this.idKdOff = idKdOff;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getKet() {
		return this.ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getStatusOp() {
		return this.statusOp;
	}

	public void setStatusOp(String statusOp) {
		this.statusOp = statusOp;
	}

	public Date getTglAkhir() {
		return this.tglAkhir;
	}

	public void setTglAkhir(Date tglAkhir) {
		this.tglAkhir = tglAkhir;
	}

	public Date getTglAwal() {
		return this.tglAwal;
	}

	public void setTglAwal(Date tglAwal) {
		this.tglAwal = tglAwal;
	}

}