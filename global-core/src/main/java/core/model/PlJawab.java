package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_JAWAB database table.
 * 
 */
@Entity
@Table(name="PL_JAWAB")
@NamedQuery(name="PlJawab.findAll", query="SELECT p FROM PlJawab p")
public class PlJawab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KODE_JAWAB")
	private String kodeJawab;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="KODE_SOAL")
	private String kodeSoal;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	private String saran;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_KUESIONER")
	private Date tglKuesioner;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_PENGAJUAN")
	private Date tglPengajuan;

	public PlJawab() {
	}

	public String getKodeJawab() {
		return this.kodeJawab;
	}

	public void setKodeJawab(String kodeJawab) {
		this.kodeJawab = kodeJawab;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeSoal() {
		return this.kodeSoal;
	}

	public void setKodeSoal(String kodeSoal) {
		this.kodeSoal = kodeSoal;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getSaran() {
		return this.saran;
	}

	public void setSaran(String saran) {
		this.saran = saran;
	}

	public Date getTglKuesioner() {
		return this.tglKuesioner;
	}

	public void setTglKuesioner(Date tglKuesioner) {
		this.tglKuesioner = tglKuesioner;
	}

	public Date getTglPengajuan() {
		return this.tglPengajuan;
	}

	public void setTglPengajuan(Date tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}

}