package core.model;

import java.io.Serializable;

public class PlRumahSakitPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String kodeRumahsakit;

	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeRumahsakit == null) ? 0 : kodeRumahsakit.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlRumahSakitPK other = (PlRumahSakitPK) obj;
		if (kodeRumahsakit == null) {
			if (other.kodeRumahsakit != null)
				return false;
		} else if (!kodeRumahsakit.equals(other.kodeRumahsakit))
			return false;
		return true;
	}

}
