package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_DRIVER_BEHAVIOURS database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_DRIVER_BEHAVIOURS")
@NamedQuery(name="KorlantasDriverBehaviour.findAll", query="SELECT k FROM KorlantasDriverBehaviour k")
public class KorlantasDriverBehaviour implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	private BigDecimal archived;

	private String name;

	@Column(name="SORT_ORDER")
	private BigDecimal sortOrder;

	public KorlantasDriverBehaviour() {
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public BigDecimal getArchived() {
		return this.archived;
	}

	public void setArchived(BigDecimal archived) {
		this.archived = archived;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}