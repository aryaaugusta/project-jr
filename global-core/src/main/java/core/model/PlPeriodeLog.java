package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_PERIODE_LOG database table.
 * 
 */
@Entity
@Table(name="PL_PERIODE_LOG")
@NamedQuery(name="PlPeriodeLog.findAll", query="SELECT p FROM PlPeriodeLog p")
public class PlPeriodeLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="PERIODE_BULAN")
	private String periodeBulan;

	@Column(name="PERIODE_TAHUN")
	private String periodeTahun;

	private String status;

	public PlPeriodeLog() {
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getPeriodeBulan() {
		return this.periodeBulan;
	}

	public void setPeriodeBulan(String periodeBulan) {
		this.periodeBulan = periodeBulan;
	}

	public String getPeriodeTahun() {
		return this.periodeTahun;
	}

	public void setPeriodeTahun(String periodeTahun) {
		this.periodeTahun = periodeTahun;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}