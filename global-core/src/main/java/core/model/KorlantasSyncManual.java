package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the KORLANTAS_SYNC_MANUAL database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_SYNC_MANUAL")
@NamedQuery(name="KorlantasSyncManual.findAll", query="SELECT k FROM KorlantasSyncManual k")
@IdClass(KorlantasSyncManualPK.class)
public class KorlantasSyncManual implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ACCIDENT_NO")
	private String accidentNo;

	@Id
	@Column(name="CREATED_BY")
	private String createdBy;

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Id
	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Id
	@Column(name="STATUS_REQ")
	private String statusReq;

	public KorlantasSyncManual() {
	}

	public String getAccidentNo() {
		return this.accidentNo;
	}

	public void setAccidentNo(String accidentNo) {
		this.accidentNo = accidentNo;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getStatusReq() {
		return this.statusReq;
	}

	public void setStatusReq(String statusReq) {
		this.statusReq = statusReq;
	}

}