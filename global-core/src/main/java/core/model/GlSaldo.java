package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the GL_SALDO database table.
 * 
 */
@Entity
@Table(name="GL_SALDO")
@NamedQuery(name="GlSaldo.findAll", query="SELECT g FROM GlSaldo g")
@IdClass(GlSaldoPK.class)
public class GlSaldo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NAMA_PERIODE", insertable=false, updatable=false)
	private String namaPeriode;

	@Id
	private String ccid;
	
	@Column(name="BULAN_PERIODE")
	private BigDecimal bulanPeriode;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="DEBIT_PERIODE")
	private BigDecimal debitPeriode;

	@Column(name="DEBIT_SALDO")
	private BigDecimal debitSaldo;

	@Column(name="DEBIT_SALDO_BEFORE")
	private BigDecimal debitSaldoBefore;

	@Column(name="ID_SALDO")
	private String idSaldo;

	@Column(name="KODE_AKUN")
	private String kodeAkun;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="KREDIT_PERIODE")
	private BigDecimal kreditPeriode;

	@Column(name="KREDIT_SALDO")
	private BigDecimal kreditSaldo;

	@Column(name="KREDIT_SALDO_BEFORE")
	private BigDecimal kreditSaldoBefore;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="TAHUN_PERIODE")
	private BigDecimal tahunPeriode;

	public GlSaldo() {
	}


	public BigDecimal getBulanPeriode() {
		return this.bulanPeriode;
	}

	public void setBulanPeriode(BigDecimal bulanPeriode) {
		this.bulanPeriode = bulanPeriode;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public BigDecimal getDebitPeriode() {
		return this.debitPeriode;
	}

	public void setDebitPeriode(BigDecimal debitPeriode) {
		this.debitPeriode = debitPeriode;
	}

	public BigDecimal getDebitSaldo() {
		return this.debitSaldo;
	}

	public void setDebitSaldo(BigDecimal debitSaldo) {
		this.debitSaldo = debitSaldo;
	}

	public BigDecimal getDebitSaldoBefore() {
		return this.debitSaldoBefore;
	}

	public void setDebitSaldoBefore(BigDecimal debitSaldoBefore) {
		this.debitSaldoBefore = debitSaldoBefore;
	}

	public String getIdSaldo() {
		return this.idSaldo;
	}

	public void setIdSaldo(String idSaldo) {
		this.idSaldo = idSaldo;
	}

	public String getKodeAkun() {
		return this.kodeAkun;
	}

	public void setKodeAkun(String kodeAkun) {
		this.kodeAkun = kodeAkun;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public BigDecimal getKreditPeriode() {
		return this.kreditPeriode;
	}

	public void setKreditPeriode(BigDecimal kreditPeriode) {
		this.kreditPeriode = kreditPeriode;
	}

	public BigDecimal getKreditSaldo() {
		return this.kreditSaldo;
	}

	public void setKreditSaldo(BigDecimal kreditSaldo) {
		this.kreditSaldo = kreditSaldo;
	}

	public BigDecimal getKreditSaldoBefore() {
		return this.kreditSaldoBefore;
	}

	public void setKreditSaldoBefore(BigDecimal kreditSaldoBefore) {
		this.kreditSaldoBefore = kreditSaldoBefore;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public BigDecimal getTahunPeriode() {
		return this.tahunPeriode;
	}

	public void setTahunPeriode(BigDecimal tahunPeriode) {
		this.tahunPeriode = tahunPeriode;
	}


	public String getNamaPeriode() {
		return namaPeriode;
	}


	public void setNamaPeriode(String namaPeriode) {
		this.namaPeriode = namaPeriode;
	}


	public String getCcid() {
		return ccid;
	}


	public void setCcid(String ccid) {
		this.ccid = ccid;
	}

}