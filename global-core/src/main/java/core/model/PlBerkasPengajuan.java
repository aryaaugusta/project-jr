package core.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.UpdateTimestamp;

import java.util.Date;


/**
 * The persistent class for the PL_BERKAS_PENGAJUAN database table.
 * 
 */
@Entity
@Table(name="PL_BERKAS_PENGAJUAN")
@NamedQuery(name="PlBerkasPengajuan.findAll", query="SELECT p FROM PlBerkasPengajuan p")
@IdClass(PlBerkasPengajuanPK.class)
public class PlBerkasPengajuan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Id
	@Column(name="KODE_BERKAS")
	private String kodeBerkas;
	
	@Column(name="ABSAH_FLAG")
	private String absahFlag;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="DITERIMA_FLAG")
	private String diterimaFlag;

	@Column(name="ID_GUID")
	private String idGuid;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_TERIMA_BERKAS")
	private Date tglTerimaBerkas;

	public PlBerkasPengajuan() {
	}


	public String getAbsahFlag() {
		return this.absahFlag;
	}

	public void setAbsahFlag(String absahFlag) {
		this.absahFlag = absahFlag;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDiterimaFlag() {
		return this.diterimaFlag;
	}

	public void setDiterimaFlag(String diterimaFlag) {
		this.diterimaFlag = diterimaFlag;
	}

	public String getIdGuid() {
		return this.idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public Date getTglTerimaBerkas() {
		return this.tglTerimaBerkas;
	}

	public void setTglTerimaBerkas(Date tglTerimaBerkas) {
		this.tglTerimaBerkas = tglTerimaBerkas;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getKodeBerkas() {
		return kodeBerkas;
	}

	public void setKodeBerkas(String kodeBerkas) {
		this.kodeBerkas = kodeBerkas;
	}

}