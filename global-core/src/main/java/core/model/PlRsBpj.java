package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_RS_BPJS database table.
 * 
 */
@Entity
@Table(name="PL_RS_BPJS")
@IdClass(PlRsBpjPK.class)
@NamedQuery(name="PlRsBpj.findAll", query="SELECT p FROM PlRsBpj p")
public class PlRsBpj implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="KAB_KOTA")
	private String kabKota;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Id
	@Column(name="KODE_RS_BPJS")
	private String kodeRsBpjs;

	@Column(name="NAMA_RS")
	private String namaRs;

	private String provinsi;

	public PlRsBpj() {
	}

	public String getKabKota() {
		return this.kabKota;
	}

	public void setKabKota(String kabKota) {
		this.kabKota = kabKota;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeRsBpjs() {
		return this.kodeRsBpjs;
	}

	public void setKodeRsBpjs(String kodeRsBpjs) {
		this.kodeRsBpjs = kodeRsBpjs;
	}

	public String getNamaRs() {
		return this.namaRs;
	}

	public void setNamaRs(String namaRs) {
		this.namaRs = namaRs;
	}

	public String getProvinsi() {
		return this.provinsi;
	}

	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}

}