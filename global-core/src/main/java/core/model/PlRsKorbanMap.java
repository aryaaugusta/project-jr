package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_RS_KORBAN_MAP database table.
 * 
 */
@Entity
@Table(name="PL_RS_KORBAN_MAP")
@NamedQuery(name="PlRsKorbanMap.findAll", query="SELECT p FROM PlRsKorbanMap p")
public class PlRsKorbanMap implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Id
	@Column(name="ID_KECELAKAAN")
	private String idKecelakaan;

	@Column(name="ID_KORBAN_KECELAKAAN")
	private String idKorbanKecelakaan;

	@Column(name="KODE_KEJADIAN")
	private String kodeKejadian;

	@Column(name="KODE_RS")
	private String kodeRs;

	@Column(name="MAP_COND")
	private String mapCond;

	public PlRsKorbanMap() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getIdKecelakaan() {
		return this.idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getIdKorbanKecelakaan() {
		return this.idKorbanKecelakaan;
	}

	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}

	public String getKodeKejadian() {
		return this.kodeKejadian;
	}

	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}

	public String getKodeRs() {
		return this.kodeRs;
	}

	public void setKodeRs(String kodeRs) {
		this.kodeRs = kodeRs;
	}

	public String getMapCond() {
		return this.mapCond;
	}

	public void setMapCond(String mapCond) {
		this.mapCond = mapCond;
	}

}