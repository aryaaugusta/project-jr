package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_SANTUNAN database table.
 * 
 */
@Entity
@Table(name="PL_SANTUNAN")
@NamedQuery(name="PlSantunan.findAll", query="SELECT p FROM PlSantunan p")
public class PlSantunan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_SANTUNAN")
	private String idSantunan;

	@Temporal(TemporalType.DATE)
	@Column(name="END_DATE")
	private Date endDate;

	@Column(name="KODE_JAMINAN")
	private String kodeJaminan;

	private BigDecimal santunan;

	@Column(name="SIFAT_CIDERA")
	private String sifatCidera;

	@Temporal(TemporalType.DATE)
	@Column(name="START_DATE")
	private Date startDate;

	public PlSantunan() {
	}

	public String getIdSantunan() {
		return this.idSantunan;
	}

	public void setIdSantunan(String idSantunan) {
		this.idSantunan = idSantunan;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getKodeJaminan() {
		return this.kodeJaminan;
	}

	public void setKodeJaminan(String kodeJaminan) {
		this.kodeJaminan = kodeJaminan;
	}

	public BigDecimal getSantunan() {
		return this.santunan;
	}

	public void setSantunan(BigDecimal santunan) {
		this.santunan = santunan;
	}

	public String getSifatCidera() {
		return this.sifatCidera;
	}

	public void setSifatCidera(String sifatCidera) {
		this.sifatCidera = sifatCidera;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

}