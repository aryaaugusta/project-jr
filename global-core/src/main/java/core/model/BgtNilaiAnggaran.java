package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the BGT_NILAI_ANGGARAN database table.
 * 
 */
@Entity
@Table(name="BGT_NILAI_ANGGARAN")
@NamedQuery(name="BgtNilaiAnggaran.findAll", query="SELECT b FROM BgtNilaiAnggaran b")
public class BgtNilaiAnggaran implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ID_NILAI_ANGGARAN")
	private String idNilaiAnggaran;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private String deskripsi;

	@Column(name="ID_GUID")
	private String idGuid;

	@Column(name="ID_JUDUL_POS")
	private String idJudulPos;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NILAI_ANGGARAN_TERPAKAI")
	private BigDecimal nilaiAnggaranTerpakai;

	@Column(name="NILAI_DISETUJUI")
	private BigDecimal nilaiDisetujui;

	@Column(name="NILAI_DIUSULKAN")
	private BigDecimal nilaiDiusulkan;

	@Column(name="NILAI_SISA_ANGGARAN")
	private BigDecimal nilaiSisaAnggaran;

	@Column(name="NO_KODE")
	private String noKode;

	private String penjelasan;

	private BigDecimal tahun;

	@Column(name="URUTAN_TAMPILAN")
	private BigDecimal urutanTampilan;

	public BgtNilaiAnggaran() {
	}

	public String getIdNilaiAnggaran() {
		return this.idNilaiAnggaran;
	}

	public void setIdNilaiAnggaran(String idNilaiAnggaran) {
		this.idNilaiAnggaran = idNilaiAnggaran;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDeskripsi() {
		return this.deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getIdGuid() {
		return this.idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public String getIdJudulPos() {
		return this.idJudulPos;
	}

	public void setIdJudulPos(String idJudulPos) {
		this.idJudulPos = idJudulPos;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public BigDecimal getNilaiAnggaranTerpakai() {
		return this.nilaiAnggaranTerpakai;
	}

	public void setNilaiAnggaranTerpakai(BigDecimal nilaiAnggaranTerpakai) {
		this.nilaiAnggaranTerpakai = nilaiAnggaranTerpakai;
	}

	public BigDecimal getNilaiDisetujui() {
		return this.nilaiDisetujui;
	}

	public void setNilaiDisetujui(BigDecimal nilaiDisetujui) {
		this.nilaiDisetujui = nilaiDisetujui;
	}

	public BigDecimal getNilaiDiusulkan() {
		return this.nilaiDiusulkan;
	}

	public void setNilaiDiusulkan(BigDecimal nilaiDiusulkan) {
		this.nilaiDiusulkan = nilaiDiusulkan;
	}

	public BigDecimal getNilaiSisaAnggaran() {
		return this.nilaiSisaAnggaran;
	}

	public void setNilaiSisaAnggaran(BigDecimal nilaiSisaAnggaran) {
		this.nilaiSisaAnggaran = nilaiSisaAnggaran;
	}

	public String getNoKode() {
		return this.noKode;
	}

	public void setNoKode(String noKode) {
		this.noKode = noKode;
	}

	public String getPenjelasan() {
		return this.penjelasan;
	}

	public void setPenjelasan(String penjelasan) {
		this.penjelasan = penjelasan;
	}

	public BigDecimal getTahun() {
		return this.tahun;
	}

	public void setTahun(BigDecimal tahun) {
		this.tahun = tahun;
	}

	public BigDecimal getUrutanTampilan() {
		return this.urutanTampilan;
	}

	public void setUrutanTampilan(BigDecimal urutanTampilan) {
		this.urutanTampilan = urutanTampilan;
	}

}