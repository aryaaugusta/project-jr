package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the FND_GENERAL_PARAM database table.
 * 
 */
@Entity
@Table(name="FND_GENERAL_PARAM")
@NamedQuery(name="FndGeneralParam.findAll", query="SELECT f FROM FndGeneralParam f")
public class FndGeneralParam implements Serializable {
	private static final long serialVersionUID = 1L;

	private String crc;

	@Id
	@Column(name="KODE_PARAM")
	private String kodeParam;

	@Column(name="NILAI_PARAM")
	private String nilaiParam;

	public FndGeneralParam() {
	}

	public String getCrc() {
		return this.crc;
	}

	public void setCrc(String crc) {
		this.crc = crc;
	}

	public String getKodeParam() {
		return this.kodeParam;
	}

	public void setKodeParam(String kodeParam) {
		this.kodeParam = kodeParam;
	}

	public String getNilaiParam() {
		return this.nilaiParam;
	}

	public void setNilaiParam(String nilaiParam) {
		this.nilaiParam = nilaiParam;
	}

}