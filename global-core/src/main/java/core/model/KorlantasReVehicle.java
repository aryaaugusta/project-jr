package core.model;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the KORLANTAS_RE_VEHICLES database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_RE_VEHICLES")
@NamedQuery(name="KorlantasReVehicle.findAll", query="SELECT k FROM KorlantasReVehicle k")
@IdClass(KorlantasVehiclePK.class)
public class KorlantasReVehicle implements Serializable {
	private static final long serialVersionUID = 1L;


	@Id
	@Column(name="ACCIDENT_ID")
	private long accidentId;
	
	@Id
	private long id;
	
	private BigDecimal age;

	private BigDecimal alcohol;

	@Column(name="CHASIS_NUMBER")
	private String chasisNumber;

	@Column(name="COLLISION_POINT_ID")
	private BigDecimal collisionPointId;

	private String column1;

	@Temporal(TemporalType.DATE)
	@Column(name="DATE_OF_BIRTH")
	private Date dateOfBirth;

	@Column(name="DRIVER_ARRESTED")
	private BigDecimal driverArrested;

	@Column(name="DRIVER_EXPIRIENCE")
	private BigDecimal driverExpirience;

	@Column(name="EDUCATION_ID")
	private BigDecimal educationId;

	@Column(name="ENGINE_CC")
	private BigDecimal engineCc;

	@Column(name="ENGINE_NUMBER")
	private String engineNumber;

	@Column(name="FIRST_NAME")
	private String firstName;

	private BigDecimal foreigner;

	private BigDecimal guilty;

	@Column(name="\"IDENTITY\"")
	private String identity;

	@Column(name="IDENTITY_TYPE_ID")
	private BigDecimal identityTypeId;

	@Column(name="INJURY_ID")
	private BigDecimal injuryId;

	@Column(name="LAST_NAME")
	private String lastName;

	@Temporal(TemporalType.DATE)
	@Column(name="LICENSE_ISSUE_DATE")
	private Date licenseIssueDate;

	@Column(name="LICENSE_NUMBER")
	private String licenseNumber;

	@Column(name="LICENSE_TYPE_ID")
	private BigDecimal licenseTypeId;

	@Column(name="NATIONALITY_ID")
	private BigDecimal nationalityId;

	@Column(name="ODOMETER_RECORD")
	private BigDecimal odometerRecord;

	@Column(name="PLATE_COLOR_ID")
	private BigDecimal plateColorId;

	@Column(name="PLATE_NUMBER")
	private String plateNumber;

	@Column(name="PROFESSION_ID")
	private BigDecimal professionId;

	@Column(name="RELIGION_ID")
	private BigDecimal religionId;

	@Column(name="SAFETY_EQUIPMENT_DRIVER_ID")
	private BigDecimal safetyEquipmentDriverId;

	@Column(name="SCHEMA_NO")
	private BigDecimal schemaNo;

	private BigDecimal sex;

	@Column(name="SKID_LENGHT")
	private String skidLenght;

	@Column(name="SPECIAL_DAMAGE_ID")
	private BigDecimal specialDamageId;

	@Column(name="\"STATE\"")
	private BigDecimal state;

	@Column(name="\"STATEMENT\"")
	private String statement;

	@Column(name="VEHICLE_DIRECTION_ID")
	private BigDecimal vehicleDirectionId;

	@Column(name="VEHICLE_MOVEMENTS_AFTER_ID")
	private BigDecimal vehicleMovementsAfterId;

	@Column(name="VEHICLE_MOVEMENTS_BEFORE_ID")
	private BigDecimal vehicleMovementsBeforeId;

	@Column(name="VEHICLE_MOVEMENTS_END_ID")
	private BigDecimal vehicleMovementsEndId;

	@Column(name="VEHICLE_REG_NO")
	private String vehicleRegNo;

	@Column(name="VEHICLE_SEIZED")
	private BigDecimal vehicleSeized;

	@Column(name="VEHICLE_SPECIAL_FUNCTION_ID")
	private BigDecimal vehicleSpecialFunctionId;

	@Column(name="VEHICLE_TYPE_ID")
	private BigDecimal vehicleTypeId;

	public KorlantasReVehicle() {
	}

	public BigDecimal getAge() {
		return this.age;
	}

	public void setAge(BigDecimal age) {
		this.age = age;
	}

	public BigDecimal getAlcohol() {
		return this.alcohol;
	}

	public void setAlcohol(BigDecimal alcohol) {
		this.alcohol = alcohol;
	}

	public String getChasisNumber() {
		return this.chasisNumber;
	}

	public void setChasisNumber(String chasisNumber) {
		this.chasisNumber = chasisNumber;
	}

	public BigDecimal getCollisionPointId() {
		return this.collisionPointId;
	}

	public void setCollisionPointId(BigDecimal collisionPointId) {
		this.collisionPointId = collisionPointId;
	}

	public String getColumn1() {
		return this.column1;
	}

	public void setColumn1(String column1) {
		this.column1 = column1;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public BigDecimal getDriverArrested() {
		return this.driverArrested;
	}

	public void setDriverArrested(BigDecimal driverArrested) {
		this.driverArrested = driverArrested;
	}

	public BigDecimal getDriverExpirience() {
		return this.driverExpirience;
	}

	public void setDriverExpirience(BigDecimal driverExpirience) {
		this.driverExpirience = driverExpirience;
	}

	public BigDecimal getEducationId() {
		return this.educationId;
	}

	public void setEducationId(BigDecimal educationId) {
		this.educationId = educationId;
	}

	public BigDecimal getEngineCc() {
		return this.engineCc;
	}

	public void setEngineCc(BigDecimal engineCc) {
		this.engineCc = engineCc;
	}

	public String getEngineNumber() {
		return this.engineNumber;
	}

	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public BigDecimal getForeigner() {
		return this.foreigner;
	}

	public void setForeigner(BigDecimal foreigner) {
		this.foreigner = foreigner;
	}

	public BigDecimal getGuilty() {
		return this.guilty;
	}

	public void setGuilty(BigDecimal guilty) {
		this.guilty = guilty;
	}

	public String getIdentity() {
		return this.identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public BigDecimal getIdentityTypeId() {
		return this.identityTypeId;
	}

	public void setIdentityTypeId(BigDecimal identityTypeId) {
		this.identityTypeId = identityTypeId;
	}

	public BigDecimal getInjuryId() {
		return this.injuryId;
	}

	public void setInjuryId(BigDecimal injuryId) {
		this.injuryId = injuryId;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getLicenseIssueDate() {
		return this.licenseIssueDate;
	}

	public void setLicenseIssueDate(Date licenseIssueDate) {
		this.licenseIssueDate = licenseIssueDate;
	}

	public String getLicenseNumber() {
		return this.licenseNumber;
	}

	public void setLicenseNumber(String licenseNumber) {
		this.licenseNumber = licenseNumber;
	}

	public BigDecimal getLicenseTypeId() {
		return this.licenseTypeId;
	}

	public void setLicenseTypeId(BigDecimal licenseTypeId) {
		this.licenseTypeId = licenseTypeId;
	}

	public BigDecimal getNationalityId() {
		return this.nationalityId;
	}

	public void setNationalityId(BigDecimal nationalityId) {
		this.nationalityId = nationalityId;
	}

	public BigDecimal getOdometerRecord() {
		return this.odometerRecord;
	}

	public void setOdometerRecord(BigDecimal odometerRecord) {
		this.odometerRecord = odometerRecord;
	}

	public BigDecimal getPlateColorId() {
		return this.plateColorId;
	}

	public void setPlateColorId(BigDecimal plateColorId) {
		this.plateColorId = plateColorId;
	}

	public String getPlateNumber() {
		return this.plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public BigDecimal getProfessionId() {
		return this.professionId;
	}

	public void setProfessionId(BigDecimal professionId) {
		this.professionId = professionId;
	}

	public BigDecimal getReligionId() {
		return this.religionId;
	}

	public void setReligionId(BigDecimal religionId) {
		this.religionId = religionId;
	}

	public BigDecimal getSafetyEquipmentDriverId() {
		return this.safetyEquipmentDriverId;
	}

	public void setSafetyEquipmentDriverId(BigDecimal safetyEquipmentDriverId) {
		this.safetyEquipmentDriverId = safetyEquipmentDriverId;
	}

	public BigDecimal getSchemaNo() {
		return this.schemaNo;
	}

	public void setSchemaNo(BigDecimal schemaNo) {
		this.schemaNo = schemaNo;
	}

	public BigDecimal getSex() {
		return this.sex;
	}

	public void setSex(BigDecimal sex) {
		this.sex = sex;
	}

	public String getSkidLenght() {
		return this.skidLenght;
	}

	public void setSkidLenght(String skidLenght) {
		this.skidLenght = skidLenght;
	}

	public BigDecimal getSpecialDamageId() {
		return this.specialDamageId;
	}

	public void setSpecialDamageId(BigDecimal specialDamageId) {
		this.specialDamageId = specialDamageId;
	}

	public BigDecimal getState() {
		return this.state;
	}

	public void setState(BigDecimal state) {
		this.state = state;
	}

	public String getStatement() {
		return this.statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	public BigDecimal getVehicleDirectionId() {
		return this.vehicleDirectionId;
	}

	public void setVehicleDirectionId(BigDecimal vehicleDirectionId) {
		this.vehicleDirectionId = vehicleDirectionId;
	}

	public BigDecimal getVehicleMovementsAfterId() {
		return this.vehicleMovementsAfterId;
	}

	public void setVehicleMovementsAfterId(BigDecimal vehicleMovementsAfterId) {
		this.vehicleMovementsAfterId = vehicleMovementsAfterId;
	}

	public BigDecimal getVehicleMovementsBeforeId() {
		return this.vehicleMovementsBeforeId;
	}

	public void setVehicleMovementsBeforeId(BigDecimal vehicleMovementsBeforeId) {
		this.vehicleMovementsBeforeId = vehicleMovementsBeforeId;
	}

	public BigDecimal getVehicleMovementsEndId() {
		return this.vehicleMovementsEndId;
	}

	public void setVehicleMovementsEndId(BigDecimal vehicleMovementsEndId) {
		this.vehicleMovementsEndId = vehicleMovementsEndId;
	}

	public String getVehicleRegNo() {
		return this.vehicleRegNo;
	}

	public void setVehicleRegNo(String vehicleRegNo) {
		this.vehicleRegNo = vehicleRegNo;
	}

	public BigDecimal getVehicleSeized() {
		return this.vehicleSeized;
	}

	public void setVehicleSeized(BigDecimal vehicleSeized) {
		this.vehicleSeized = vehicleSeized;
	}

	public BigDecimal getVehicleSpecialFunctionId() {
		return this.vehicleSpecialFunctionId;
	}

	public void setVehicleSpecialFunctionId(BigDecimal vehicleSpecialFunctionId) {
		this.vehicleSpecialFunctionId = vehicleSpecialFunctionId;
	}

	public BigDecimal getVehicleTypeId() {
		return this.vehicleTypeId;
	}

	public void setVehicleTypeId(BigDecimal vehicleTypeId) {
		this.vehicleTypeId = vehicleTypeId;
	}

	public long getAccidentId() {
		return accidentId;
	}

	public void setAccidentId(long accidentId) {
		this.accidentId = accidentId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}