package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PL_RUMAH_SAKIT database table.
 * 
 */
@Entity
@Table(name="PL_RUMAH_SAKIT")
@IdClass(PlRumahSakitPK.class)
@NamedQuery(name="PlRumahSakit.findAll", query="SELECT p FROM PlRumahSakit p")
public class PlRumahSakit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="KODE_RUMAHSAKIT")
	private String kodeRumahsakit;

	private String alamat;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	private String deskripsi;

	@Column(name="FLAG_ENABLE")
	private String flagEnable;

	@Column(name="JENIS_RS")
	private String jenisRs;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NO_TELP")
	private String noTelp;

	private String pic;

//	//bi-directional many-to-one association to PlRekeningR
//	@OneToMany(mappedBy="plRumahSakit")
//	private List<PlRekeningR> plRekeningRs;
//
//	//bi-directional many-to-one association to PlRsMapBpj
//	@OneToMany(mappedBy="plRumahSakit")
//	private List<PlRsMapBpj> plRsMapBpjs;

	public PlRumahSakit() {
	}

	public String getKodeRumahsakit() {
		return this.kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

	public String getAlamat() {
		return this.alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDeskripsi() {
		return this.deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public String getFlagEnable() {
		return this.flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getJenisRs() {
		return this.jenisRs;
	}

	public void setJenisRs(String jenisRs) {
		this.jenisRs = jenisRs;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public String getPic() {
		return this.pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

//	public List<PlRekeningR> getPlRekeningRs() {
//		return this.plRekeningRs;
//	}
//
//	public void setPlRekeningRs(List<PlRekeningR> plRekeningRs) {
//		this.plRekeningRs = plRekeningRs;
//	}

//	public PlRekeningR addPlRekeningR(PlRekeningR plRekeningR) {
//		getPlRekeningRs().add(plRekeningR);
//		plRekeningR.setPlRumahSakit(this);
//
//		return plRekeningR;
//	}
//
//	public PlRekeningR removePlRekeningR(PlRekeningR plRekeningR) {
//		getPlRekeningRs().remove(plRekeningR);
//		plRekeningR.setPlRumahSakit(null);
//
//		return plRekeningR;
//	}

//	public List<PlRsMapBpj> getPlRsMapBpjs() {
//		return this.plRsMapBpjs;
//	}
//
//	public void setPlRsMapBpjs(List<PlRsMapBpj> plRsMapBpjs) {
//		this.plRsMapBpjs = plRsMapBpjs;
//	}

//	public PlRsMapBpj addPlRsMapBpj(PlRsMapBpj plRsMapBpj) {
//		getPlRsMapBpjs().add(plRsMapBpj);
//		plRsMapBpj.setPlRumahSakit(this);
//
//		return plRsMapBpj;
//	}
//
//	public PlRsMapBpj removePlRsMapBpj(PlRsMapBpj plRsMapBpj) {
//		getPlRsMapBpjs().remove(plRsMapBpj);
//		plRsMapBpj.setPlRumahSakit(null);
//
//		return plRsMapBpj;
//	}

}