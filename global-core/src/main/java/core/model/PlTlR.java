package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_TL_RS database table.
 * 
 */
@Entity
@Table(name="PL_TL_RS")
@NamedQuery(name="PlTlR.findAll", query="SELECT p FROM PlTlR p")
public class PlTlR implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ID_JAMINAN")
	private String idJaminan;

	@Column(name="BPK_CREATOR")
	private String bpkCreator;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="FLAG_BAYAR")
	private String flagBayar;

	@Column(name="FLAG_BAYAR_AWAL")
	private String flagBayarAwal;

	@Column(name="FLAG_LANJUTAN")
	private String flagLanjutan;

	@Column(name="FLAG_LIMPAHAN")
	private String flagLimpahan;

	@Column(name="JENIS_TAGIHAN")
	private String jenisTagihan;

	@Column(name="JML_AMBL")
	private BigDecimal jmlAmbl;

	@Column(name="JML_AMBL_SEMENTARA")
	private BigDecimal jmlAmblSementara;

	@Column(name="JML_AMBL_SMT")
	private BigDecimal jmlAmblSmt;

	@Column(name="JML_P3K")
	private BigDecimal jmlP3k;

	@Column(name="JML_P3K_SEMENTARA")
	private BigDecimal jmlP3kSementara;

	@Column(name="JML_P3K_SMT")
	private BigDecimal jmlP3kSmt;

	@Column(name="JML_PENGAJUAN_SEMENTARA")
	private BigDecimal jmlPengajuanSementara;

	@Column(name="JUMLAH_PENGAJUAN_1")
	private BigDecimal jumlahPengajuan1;

	@Column(name="JUMLAH_PENGAJUAN_SMT")
	private BigDecimal jumlahPengajuanSmt;

	@Column(name="KETERANGAN_OTORISASI")
	private String keteranganOtorisasi;

	@Column(name="KODE_KANTOR_ASAL_LIMP")
	private String kodeKantorAsalLimp;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="KODE_RS_SEMENTARA")
	private String kodeRsSementara;

	@Column(name="KODE_RUMAH_SAKIT")
	private String kodeRumahSakit;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="NO_BPK")
	private String noBpk;

	@Column(name="NO_REGISTER")
	private String noRegister;

	@Column(name="NO_SURAT_JAMINAN")
	private String noSuratJaminan;

	@Column(name="OTORISASI_AJU_SMT")
	private String otorisasiAjuSmt;

	@Column(name="OTORISASI_AWAL")
	private String otorisasiAwal;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_BPK")
	private Date tglBpk;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_MASUK_RS")
	private Date tglMasukRs;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_SURAT_JAMINAN")
	private Date tglSuratJaminan;

	public PlTlR() {
	}

	public String getIdJaminan() {
		return this.idJaminan;
	}

	public void setIdJaminan(String idJaminan) {
		this.idJaminan = idJaminan;
	}

	public String getBpkCreator() {
		return this.bpkCreator;
	}

	public void setBpkCreator(String bpkCreator) {
		this.bpkCreator = bpkCreator;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getFlagBayar() {
		return this.flagBayar;
	}

	public void setFlagBayar(String flagBayar) {
		this.flagBayar = flagBayar;
	}

	public String getFlagBayarAwal() {
		return this.flagBayarAwal;
	}

	public void setFlagBayarAwal(String flagBayarAwal) {
		this.flagBayarAwal = flagBayarAwal;
	}

	public String getFlagLanjutan() {
		return this.flagLanjutan;
	}

	public void setFlagLanjutan(String flagLanjutan) {
		this.flagLanjutan = flagLanjutan;
	}

	public String getFlagLimpahan() {
		return this.flagLimpahan;
	}

	public void setFlagLimpahan(String flagLimpahan) {
		this.flagLimpahan = flagLimpahan;
	}

	public String getJenisTagihan() {
		return this.jenisTagihan;
	}

	public void setJenisTagihan(String jenisTagihan) {
		this.jenisTagihan = jenisTagihan;
	}

	public BigDecimal getJmlAmbl() {
		return this.jmlAmbl;
	}

	public void setJmlAmbl(BigDecimal jmlAmbl) {
		this.jmlAmbl = jmlAmbl;
	}

	public BigDecimal getJmlAmblSementara() {
		return this.jmlAmblSementara;
	}

	public void setJmlAmblSementara(BigDecimal jmlAmblSementara) {
		this.jmlAmblSementara = jmlAmblSementara;
	}

	public BigDecimal getJmlAmblSmt() {
		return this.jmlAmblSmt;
	}

	public void setJmlAmblSmt(BigDecimal jmlAmblSmt) {
		this.jmlAmblSmt = jmlAmblSmt;
	}

	public BigDecimal getJmlP3k() {
		return this.jmlP3k;
	}

	public void setJmlP3k(BigDecimal jmlP3k) {
		this.jmlP3k = jmlP3k;
	}

	public BigDecimal getJmlP3kSementara() {
		return this.jmlP3kSementara;
	}

	public void setJmlP3kSementara(BigDecimal jmlP3kSementara) {
		this.jmlP3kSementara = jmlP3kSementara;
	}

	public BigDecimal getJmlP3kSmt() {
		return this.jmlP3kSmt;
	}

	public void setJmlP3kSmt(BigDecimal jmlP3kSmt) {
		this.jmlP3kSmt = jmlP3kSmt;
	}

	public BigDecimal getJmlPengajuanSementara() {
		return this.jmlPengajuanSementara;
	}

	public void setJmlPengajuanSementara(BigDecimal jmlPengajuanSementara) {
		this.jmlPengajuanSementara = jmlPengajuanSementara;
	}

	public BigDecimal getJumlahPengajuan1() {
		return this.jumlahPengajuan1;
	}

	public void setJumlahPengajuan1(BigDecimal jumlahPengajuan1) {
		this.jumlahPengajuan1 = jumlahPengajuan1;
	}

	public BigDecimal getJumlahPengajuanSmt() {
		return this.jumlahPengajuanSmt;
	}

	public void setJumlahPengajuanSmt(BigDecimal jumlahPengajuanSmt) {
		this.jumlahPengajuanSmt = jumlahPengajuanSmt;
	}

	public String getKeteranganOtorisasi() {
		return this.keteranganOtorisasi;
	}

	public void setKeteranganOtorisasi(String keteranganOtorisasi) {
		this.keteranganOtorisasi = keteranganOtorisasi;
	}

	public String getKodeKantorAsalLimp() {
		return this.kodeKantorAsalLimp;
	}

	public void setKodeKantorAsalLimp(String kodeKantorAsalLimp) {
		this.kodeKantorAsalLimp = kodeKantorAsalLimp;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeRsSementara() {
		return this.kodeRsSementara;
	}

	public void setKodeRsSementara(String kodeRsSementara) {
		this.kodeRsSementara = kodeRsSementara;
	}

	public String getKodeRumahSakit() {
		return this.kodeRumahSakit;
	}

	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getNoBpk() {
		return this.noBpk;
	}

	public void setNoBpk(String noBpk) {
		this.noBpk = noBpk;
	}

	public String getNoRegister() {
		return this.noRegister;
	}

	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}

	public String getNoSuratJaminan() {
		return this.noSuratJaminan;
	}

	public void setNoSuratJaminan(String noSuratJaminan) {
		this.noSuratJaminan = noSuratJaminan;
	}

	public String getOtorisasiAjuSmt() {
		return this.otorisasiAjuSmt;
	}

	public void setOtorisasiAjuSmt(String otorisasiAjuSmt) {
		this.otorisasiAjuSmt = otorisasiAjuSmt;
	}

	public String getOtorisasiAwal() {
		return this.otorisasiAwal;
	}

	public void setOtorisasiAwal(String otorisasiAwal) {
		this.otorisasiAwal = otorisasiAwal;
	}

	public Date getTglBpk() {
		return this.tglBpk;
	}

	public void setTglBpk(Date tglBpk) {
		this.tglBpk = tglBpk;
	}

	public Date getTglMasukRs() {
		return this.tglMasukRs;
	}

	public void setTglMasukRs(Date tglMasukRs) {
		this.tglMasukRs = tglMasukRs;
	}

	public Date getTglSuratJaminan() {
		return this.tglSuratJaminan;
	}

	public void setTglSuratJaminan(Date tglSuratJaminan) {
		this.tglSuratJaminan = tglSuratJaminan;
	}

}