package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_ADDITIONAL_DESC database table.
 * 
 */
@Entity
@Table(name="PL_ADDITIONAL_DESC")
@NamedQuery(name="PlAdditionalDesc.findAll", query="SELECT p FROM PlAdditionalDesc p")
public class PlAdditionalDesc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	//@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	@Column(name="ID_REK_RS")
	private String idRekRs;

	@Column(name="JNS_REKENING")
	private String jnsRekening;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NAMA_REKENING")
	private String namaRekening;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_MD")
	private Date tglMd;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_RAWAT_AKHIR")
	private Date tglRawatAkhir;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_RAWAT_AWAL")
	private Date tglRawatAwal;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_TERIMA_LIMPAH")
	private Date tglTerimaLimpah;

	@Column(name="KODE_LOKASI_PEMOHON")
	private String kodeLokasiPemohon;

	public PlAdditionalDesc() {
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getIdRekRs() {
		return this.idRekRs;
	}

	public void setIdRekRs(String idRekRs) {
		this.idRekRs = idRekRs;
	}

	public String getJnsRekening() {
		return this.jnsRekening;
	}

	public void setJnsRekening(String jnsRekening) {
		this.jnsRekening = jnsRekening;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamaRekening() {
		return this.namaRekening;
	}

	public void setNamaRekening(String namaRekening) {
		this.namaRekening = namaRekening;
	}

	public Date getTglMd() {
		return this.tglMd;
	}

	public void setTglMd(Date tglMd) {
		this.tglMd = tglMd;
	}

	public Date getTglRawatAkhir() {
		return this.tglRawatAkhir;
	}

	public void setTglRawatAkhir(Date tglRawatAkhir) {
		this.tglRawatAkhir = tglRawatAkhir;
	}

	public Date getTglRawatAwal() {
		return this.tglRawatAwal;
	}

	public void setTglRawatAwal(Date tglRawatAwal) {
		this.tglRawatAwal = tglRawatAwal;
	}

	public Date getTglTerimaLimpah() {
		return this.tglTerimaLimpah;
	}

	public void setTglTerimaLimpah(Date tglTerimaLimpah) {
		this.tglTerimaLimpah = tglTerimaLimpah;
	}

	public String getKodeLokasiPemohon() {
		return kodeLokasiPemohon;
	}

	public void setKodeLokasiPemohon(String kodeLokasiPemohon) {
		this.kodeLokasiPemohon = kodeLokasiPemohon;
	}

}