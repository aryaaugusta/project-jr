package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the AUTH_USER_DUKCAPIL database table.
 * 
 */
@Entity
@Table(name="AUTH_USER_DUKCAPIL")
@NamedQuery(name="AuthUserDukcapil.findAll", query="SELECT a FROM AuthUserDukcapil a")
public class AuthUserDukcapil implements Serializable {
	private static final long serialVersionUID = 1L;

	private String login;

	@Column(name="USER_PASS")
	private String userPass;

	@Id
	@Column(name="USERID_LOKAL")
	private String useridLokal;

	@Column(name="USERID_WS")
	private String useridWs;

	public AuthUserDukcapil() {
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getUserPass() {
		return this.userPass;
	}

	public void setUserPass(String userPass) {
		this.userPass = userPass;
	}

	public String getUseridLokal() {
		return this.useridLokal;
	}

	public void setUseridLokal(String useridLokal) {
		this.useridLokal = useridLokal;
	}

	public String getUseridWs() {
		return this.useridWs;
	}

	public void setUseridWs(String useridWs) {
		this.useridWs = useridWs;
	}

}