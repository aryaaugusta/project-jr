package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the GL_VOUCHER database table.
 * 
 */
public class GlVoucherPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	private String idVoucher;

	private String kodeKantorJr;

	public GlVoucherPK() {
	}
	public String getIdVoucher() {
		return this.idVoucher;
	}
	public void setIdVoucher(String idVoucher) {
		this.idVoucher = idVoucher;
	}
	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof GlVoucherPK)) {
			return false;
		}
		GlVoucherPK castOther = (GlVoucherPK)other;
		return 
			this.idVoucher.equals(castOther.idVoucher)
			&& this.kodeKantorJr.equals(castOther.kodeKantorJr);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idVoucher.hashCode();
		hash = hash * prime + this.kodeKantorJr.hashCode();
		
		return hash;
	}
}