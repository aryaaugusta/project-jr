package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_KORL_MAP_KEND database table.
 * 
 */
@Entity
@Table(name="PL_KORL_MAP_KEND")
@NamedQuery(name="PlKorlMapKend.findAll", query="SELECT p FROM PlKorlMapKend p")
public class PlKorlMapKend implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Column(name="CREATED_TYPE")
	private String createdType;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="ID_JR")
	private String idJr;

	@Column(name="ID_KORLANTAS")
	private String idKorlantas;

	public PlKorlMapKend() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedType() {
		return this.createdType;
	}

	public void setCreatedType(String createdType) {
		this.createdType = createdType;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdJr() {
		return this.idJr;
	}

	public void setIdJr(String idJr) {
		this.idJr = idJr;
	}

	public String getIdKorlantas() {
		return this.idKorlantas;
	}

	public void setIdKorlantas(String idKorlantas) {
		this.idKorlantas = idKorlantas;
	}

}