package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the PL_KORBAN_RS_SUPLESI database table.
 * 
 */
@Entity
@Table(name="PL_KORBAN_RS_SUPLESI")
@NamedQuery(name="PlKorbanRsSuplesi.findAll", query="SELECT p FROM PlKorbanRsSuplesi p")
public class PlKorbanRsSuplesi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KODE_KEJADIAN_BARU")
	private String kodeKejadianBaru;

	@Column(name="CREATION_DATE")
	private Timestamp creationDate;

	private String diagnosa;

	@Column(name="FLAG_PAY")
	private String flagPay;

	@Column(name="JENIS_PELAYANAN")
	private String jenisPelayanan;

	private String keterangan;

	@Column(name="KODE_KEJADIAN_LAMA")
	private String kodeKejadianLama;

	@Column(name="KODE_RUMAH_SAKIT")
	private String kodeRumahSakit;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="NO_TELP")
	private String noTelp;

	@Column(name="REKAM_MEDIS")
	private String rekamMedis;

	private String ruangan;

	@Column(name="RUPIAH_PEMBAYARAN")
	private BigDecimal rupiahPembayaran;

	@Temporal(TemporalType.DATE)
	@Column(name="TANGGAL_MASUK_RS")
	private Date tanggalMasukRs;

	public PlKorbanRsSuplesi() {
	}

	public String getKodeKejadianBaru() {
		return this.kodeKejadianBaru;
	}

	public void setKodeKejadianBaru(String kodeKejadianBaru) {
		this.kodeKejadianBaru = kodeKejadianBaru;
	}

	public Timestamp getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public String getDiagnosa() {
		return this.diagnosa;
	}

	public void setDiagnosa(String diagnosa) {
		this.diagnosa = diagnosa;
	}

	public String getFlagPay() {
		return this.flagPay;
	}

	public void setFlagPay(String flagPay) {
		this.flagPay = flagPay;
	}

	public String getJenisPelayanan() {
		return this.jenisPelayanan;
	}

	public void setJenisPelayanan(String jenisPelayanan) {
		this.jenisPelayanan = jenisPelayanan;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getKodeKejadianLama() {
		return this.kodeKejadianLama;
	}

	public void setKodeKejadianLama(String kodeKejadianLama) {
		this.kodeKejadianLama = kodeKejadianLama;
	}

	public String getKodeRumahSakit() {
		return this.kodeRumahSakit;
	}

	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public String getRekamMedis() {
		return this.rekamMedis;
	}

	public void setRekamMedis(String rekamMedis) {
		this.rekamMedis = rekamMedis;
	}

	public String getRuangan() {
		return this.ruangan;
	}

	public void setRuangan(String ruangan) {
		this.ruangan = ruangan;
	}

	public BigDecimal getRupiahPembayaran() {
		return this.rupiahPembayaran;
	}

	public void setRupiahPembayaran(BigDecimal rupiahPembayaran) {
		this.rupiahPembayaran = rupiahPembayaran;
	}

	public Date getTanggalMasukRs() {
		return this.tanggalMasukRs;
	}

	public void setTanggalMasukRs(Date tanggalMasukRs) {
		this.tanggalMasukRs = tanggalMasukRs;
	}

}