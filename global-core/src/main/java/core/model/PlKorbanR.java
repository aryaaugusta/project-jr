package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_KORBAN_RS database table.
 * 
 */
@Entity
@Table(name="PL_KORBAN_RS")
@NamedQuery(name="PlKorbanR.findAll", query="SELECT p FROM PlKorbanR p")
public class PlKorbanR implements Serializable {
	private static final long serialVersionUID = 1L;

	private String alamat;

	private BigDecimal biaya;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CREATED_DATE")
	private Date createdDate;

	private String diagnosa;

	@Column(name="DOKTER_BERWENANG")
	private String dokterBerwenang;

	@Column(name="FLAG_DOBEL")
	private String flagDobel;

	@Column(name="FLAG_MULTI_JAMINAN")
	private String flagMultiJaminan;

	@Column(name="JENIS_KELAMIN")
	private String jenisKelamin;

	@Column(name="JENIS_PELAYANAN")
	private String jenisPelayanan;

	@Column(name="JENIS_TINDAKAN")
	private String jenisTindakan;

	@Column(name="JUMLAH_DIBAYARKAN")
	private BigDecimal jumlahDibayarkan;

	private String keterangan;

	@Column(name="KODE_INSTANSI")
	private String kodeInstansi;

	@Column(name="KODE_KEJADIAN")
	private String kodeKejadian;

	@Column(name="KODE_RUMAH_SAKIT")
	private String kodeRumahSakit;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	@Column(name="LOKASI_KEJADIAN")
	private String lokasiKejadian;

	@Column(name="NAMA_KORBAN")
	private String namaKorban;

	@Id
	private String nik;

	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="NO_KARTU")
	private String noKartu;

	@Column(name="NO_TELP")
	private String noTelp;

	@Column(name="REKAM_MEDIS")
	private String rekamMedis;

	private String ruangan;

	@Column(name="SIFAT_CIDERA")
	private String sifatCidera;

	@Column(name="STATUS_JAMINAN")
	private String statusJaminan;

	@Column(name="STATUS_KLAIM")
	private String statusKlaim;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_KEJADIAN")
	private Date tglKejadian;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_MASUK_RS")
	private Date tglMasukRs;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TGL_PROSES")
	private Date tglProses;

	@Column(name="TIPE_ID")
	private String tipeId;

	private String umur;

	public PlKorbanR() {
	}

	public String getAlamat() {
		return this.alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public BigDecimal getBiaya() {
		return this.biaya;
	}

	public void setBiaya(BigDecimal biaya) {
		this.biaya = biaya;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDiagnosa() {
		return this.diagnosa;
	}

	public void setDiagnosa(String diagnosa) {
		this.diagnosa = diagnosa;
	}

	public String getDokterBerwenang() {
		return this.dokterBerwenang;
	}

	public void setDokterBerwenang(String dokterBerwenang) {
		this.dokterBerwenang = dokterBerwenang;
	}

	public String getFlagDobel() {
		return this.flagDobel;
	}

	public void setFlagDobel(String flagDobel) {
		this.flagDobel = flagDobel;
	}

	public String getFlagMultiJaminan() {
		return this.flagMultiJaminan;
	}

	public void setFlagMultiJaminan(String flagMultiJaminan) {
		this.flagMultiJaminan = flagMultiJaminan;
	}

	public String getJenisKelamin() {
		return this.jenisKelamin;
	}

	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}

	public String getJenisPelayanan() {
		return this.jenisPelayanan;
	}

	public void setJenisPelayanan(String jenisPelayanan) {
		this.jenisPelayanan = jenisPelayanan;
	}

	public String getJenisTindakan() {
		return this.jenisTindakan;
	}

	public void setJenisTindakan(String jenisTindakan) {
		this.jenisTindakan = jenisTindakan;
	}

	public BigDecimal getJumlahDibayarkan() {
		return this.jumlahDibayarkan;
	}

	public void setJumlahDibayarkan(BigDecimal jumlahDibayarkan) {
		this.jumlahDibayarkan = jumlahDibayarkan;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getKodeInstansi() {
		return this.kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public String getKodeKejadian() {
		return this.kodeKejadian;
	}

	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}

	public String getKodeRumahSakit() {
		return this.kodeRumahSakit;
	}

	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLokasiKejadian() {
		return this.lokasiKejadian;
	}

	public void setLokasiKejadian(String lokasiKejadian) {
		this.lokasiKejadian = lokasiKejadian;
	}

	public String getNamaKorban() {
		return this.namaKorban;
	}

	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}

	public String getNik() {
		return this.nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getNoKartu() {
		return this.noKartu;
	}

	public void setNoKartu(String noKartu) {
		this.noKartu = noKartu;
	}

	public String getNoTelp() {
		return this.noTelp;
	}

	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}

	public String getRekamMedis() {
		return this.rekamMedis;
	}

	public void setRekamMedis(String rekamMedis) {
		this.rekamMedis = rekamMedis;
	}

	public String getRuangan() {
		return this.ruangan;
	}

	public void setRuangan(String ruangan) {
		this.ruangan = ruangan;
	}

	public String getSifatCidera() {
		return this.sifatCidera;
	}

	public void setSifatCidera(String sifatCidera) {
		this.sifatCidera = sifatCidera;
	}

	public String getStatusJaminan() {
		return this.statusJaminan;
	}

	public void setStatusJaminan(String statusJaminan) {
		this.statusJaminan = statusJaminan;
	}

	public String getStatusKlaim() {
		return this.statusKlaim;
	}

	public void setStatusKlaim(String statusKlaim) {
		this.statusKlaim = statusKlaim;
	}

	public Date getTglKejadian() {
		return this.tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public Date getTglMasukRs() {
		return this.tglMasukRs;
	}

	public void setTglMasukRs(Date tglMasukRs) {
		this.tglMasukRs = tglMasukRs;
	}

	public Date getTglProses() {
		return this.tglProses;
	}

	public void setTglProses(Date tglProses) {
		this.tglProses = tglProses;
	}

	public String getTipeId() {
		return this.tipeId;
	}

	public void setTipeId(String tipeId) {
		this.tipeId = tipeId;
	}

	public String getUmur() {
		return this.umur;
	}

	public void setUmur(String umur) {
		this.umur = umur;
	}

}