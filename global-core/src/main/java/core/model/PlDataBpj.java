package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_DATA_BPJS database table.
 * 
 */
@Entity
@Table(name="PL_DATA_BPJS")
@NamedQuery(name="PlDataBpj.findAll", query="SELECT p FROM PlDataBpj p")
public class PlDataBpj implements Serializable {
	private static final long serialVersionUID = 1L;

	private String jnspelsep;

	private String kdppk;

	private String keterangan;

	@Id
	private String nik;

	private String nmkc;

	private String nmppk;

	private String nmpst;

	private String nokapst;

	private String nosep;

	@Column(name="RETURN_JR")
	private String returnJr;

	private String statusdijamin;

	private String statuskirim;

	private String tglkejadian;

	private String tgllhrpst;

	private String tglsep;

	public PlDataBpj() {
	}

	public String getJnspelsep() {
		return this.jnspelsep;
	}

	public void setJnspelsep(String jnspelsep) {
		this.jnspelsep = jnspelsep;
	}

	public String getKdppk() {
		return this.kdppk;
	}

	public void setKdppk(String kdppk) {
		this.kdppk = kdppk;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getNik() {
		return this.nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getNmkc() {
		return this.nmkc;
	}

	public void setNmkc(String nmkc) {
		this.nmkc = nmkc;
	}

	public String getNmppk() {
		return this.nmppk;
	}

	public void setNmppk(String nmppk) {
		this.nmppk = nmppk;
	}

	public String getNmpst() {
		return this.nmpst;
	}

	public void setNmpst(String nmpst) {
		this.nmpst = nmpst;
	}

	public String getNokapst() {
		return this.nokapst;
	}

	public void setNokapst(String nokapst) {
		this.nokapst = nokapst;
	}

	public String getNosep() {
		return this.nosep;
	}

	public void setNosep(String nosep) {
		this.nosep = nosep;
	}

	public String getReturnJr() {
		return this.returnJr;
	}

	public void setReturnJr(String returnJr) {
		this.returnJr = returnJr;
	}

	public String getStatusdijamin() {
		return this.statusdijamin;
	}

	public void setStatusdijamin(String statusdijamin) {
		this.statusdijamin = statusdijamin;
	}

	public String getStatuskirim() {
		return this.statuskirim;
	}

	public void setStatuskirim(String statuskirim) {
		this.statuskirim = statuskirim;
	}

	public String getTglkejadian() {
		return this.tglkejadian;
	}

	public void setTglkejadian(String tglkejadian) {
		this.tglkejadian = tglkejadian;
	}

	public String getTgllhrpst() {
		return this.tgllhrpst;
	}

	public void setTgllhrpst(String tgllhrpst) {
		this.tgllhrpst = tgllhrpst;
	}

	public String getTglsep() {
		return this.tglsep;
	}

	public void setTglsep(String tglsep) {
		this.tglsep = tglsep;
	}

}