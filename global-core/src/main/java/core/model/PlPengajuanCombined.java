package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_PENGAJUAN_COMBINED database table.
 * 
 */
@Entity
@Table(name="PL_PENGAJUAN_COMBINED")
@NamedQuery(name="PlPengajuanCombined.findAll", query="SELECT p FROM PlPengajuanCombined p")
public class PlPengajuanCombined implements Serializable {
	private static final long serialVersionUID = 1L;

	private String ket;

	@Id
	@Column(name="NO_BERKAS_1")
	private String noBerkas1;

	@Column(name="NO_BERKAS_2")
	private String noBerkas2;

	@Column(name="NO_BERKAS_DISP")
	private String noBerkasDisp;

	@Column(name="SD_ID")
	private String sdId;

	public PlPengajuanCombined() {
	}

	public String getKet() {
		return this.ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getNoBerkas1() {
		return this.noBerkas1;
	}

	public void setNoBerkas1(String noBerkas1) {
		this.noBerkas1 = noBerkas1;
	}

	public String getNoBerkas2() {
		return this.noBerkas2;
	}

	public void setNoBerkas2(String noBerkas2) {
		this.noBerkas2 = noBerkas2;
	}

	public String getNoBerkasDisp() {
		return this.noBerkasDisp;
	}

	public void setNoBerkasDisp(String noBerkasDisp) {
		this.noBerkasDisp = noBerkasDisp;
	}

	public String getSdId() {
		return this.sdId;
	}

	public void setSdId(String sdId) {
		this.sdId = sdId;
	}

}