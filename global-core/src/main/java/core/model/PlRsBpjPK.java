package core.model;

import java.io.Serializable;

public class PlRsBpjPK implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String kodeRsBpjs;

	public String getKodeRsBpjs() {
		return kodeRsBpjs;
	}

	public void setKodeRsBpjs(String kodeRsBpjs) {
		this.kodeRsBpjs = kodeRsBpjs;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeRsBpjs == null) ? 0 : kodeRsBpjs.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlRsBpjPK other = (PlRsBpjPK) obj;
		if (kodeRsBpjs == null) {
			if (other.kodeRsBpjs != null)
				return false;
		} else if (!kodeRsBpjs.equals(other.kodeRsBpjs))
			return false;
		return true;
	}

}
