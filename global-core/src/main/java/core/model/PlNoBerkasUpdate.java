package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PL_NO_BERKAS_UPDATE database table.
 * 
 */
@Entity
@Table(name="PL_NO_BERKAS_UPDATE")
@NamedQuery(name="PlNoBerkasUpdate.findAll", query="SELECT p FROM PlNoBerkasUpdate p")
public class PlNoBerkasUpdate implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="NO_BERKAS")
	private String noBerkas;

	@Column(name="NO_BPK")
	private String noBpk;

	public PlNoBerkasUpdate() {
	}

	public String getNoBerkas() {
		return this.noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getNoBpk() {
		return this.noBpk;
	}

	public void setNoBpk(String noBpk) {
		this.noBpk = noBpk;
	}

}