package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_SUM_KORLT database table.
 * 
 */
@Entity
@Table(name="PL_SUM_KORLT")
@NamedQuery(name="PlSumKorlt.findAll", query="SELECT p FROM PlSumKorlt p")
public class PlSumKorlt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="ACCIDENT_DATE")
	private Date accidentDate;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="DISTRICT_ID")
	private BigDecimal districtId;

	@Column(name="JML_LL")
	private BigDecimal jmlLl;

	@Column(name="JML_MD")
	private BigDecimal jmlMd;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	public PlSumKorlt() {
	}

	public Date getAccidentDate() {
		return this.accidentDate;
	}

	public void setAccidentDate(Date accidentDate) {
		this.accidentDate = accidentDate;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public BigDecimal getDistrictId() {
		return this.districtId;
	}

	public void setDistrictId(BigDecimal districtId) {
		this.districtId = districtId;
	}

	public BigDecimal getJmlLl() {
		return this.jmlLl;
	}

	public void setJmlLl(BigDecimal jmlLl) {
		this.jmlLl = jmlLl;
	}

	public BigDecimal getJmlMd() {
		return this.jmlMd;
	}

	public void setJmlMd(BigDecimal jmlMd) {
		this.jmlMd = jmlMd;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

}