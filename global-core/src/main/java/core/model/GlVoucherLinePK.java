package core.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the GL_VOUCHER_LINE database table.
 * 
 */
@Embeddable
public class GlVoucherLinePK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="ID_VOUCHER")
	private String idVoucher;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="KODE_AKUN", insertable=false, updatable=false)
	private String kodeAkun;

	public GlVoucherLinePK() {
	}
	public String getIdVoucher() {
		return this.idVoucher;
	}
	public void setIdVoucher(String idVoucher) {
		this.idVoucher = idVoucher;
	}
	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getKodeAkun() {
		return this.kodeAkun;
	}
	public void setKodeAkun(String kodeAkun) {
		this.kodeAkun = kodeAkun;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof GlVoucherLinePK)) {
			return false;
		}
		GlVoucherLinePK castOther = (GlVoucherLinePK)other;
		return 
			this.idVoucher.equals(castOther.idVoucher)
			&& this.kodeKantorJr.equals(castOther.kodeKantorJr)
			&& this.kodeAkun.equals(castOther.kodeAkun);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.idVoucher.hashCode();
		hash = hash * prime + this.kodeKantorJr.hashCode();
		hash = hash * prime + this.kodeAkun.hashCode();
		
		return hash;
	}
}