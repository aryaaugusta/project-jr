package core.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the KABKOTA database table.
 * 
 */
@Entity
@NamedQuery(name="Kabkota.findAll", query="SELECT k FROM Kabkota k")
public class Kabkota implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="KABKOTA_KODE")
	private String kabkotaKode;

	@Column(name="KABKOTA_NAMA")
	private String kabkotaNama;

	@Column(name="KABKOTA_PROVINSI")
	private String kabkotaProvinsi;

	public Kabkota() {
	}

	public String getKabkotaKode() {
		return this.kabkotaKode;
	}

	public void setKabkotaKode(String kabkotaKode) {
		this.kabkotaKode = kabkotaKode;
	}

	public String getKabkotaNama() {
		return this.kabkotaNama;
	}

	public void setKabkotaNama(String kabkotaNama) {
		this.kabkotaNama = kabkotaNama;
	}

	public String getKabkotaProvinsi() {
		return this.kabkotaProvinsi;
	}

	public void setKabkotaProvinsi(String kabkotaProvinsi) {
		this.kabkotaProvinsi = kabkotaProvinsi;
	}

}