package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the KORLANTAS_INJURIES database table.
 * 
 */
@Entity
@Table(name="KORLANTAS_INJURIES")
@NamedQuery(name="KorlantasInjury.findAll", query="SELECT k FROM KorlantasInjury k")
public class KorlantasInjury implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="DASI_ID")
	private String dasiId;

	@Id
	private BigDecimal id;

	private String name;

	@Column(name="SORT_ORDER")
	private BigDecimal sortOrder;

	public KorlantasInjury() {
	}

	public String getDasiId() {
		return this.dasiId;
	}

	public void setDasiId(String dasiId) {
		this.dasiId = dasiId;
	}

	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getSortOrder() {
		return this.sortOrder;
	}

	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}