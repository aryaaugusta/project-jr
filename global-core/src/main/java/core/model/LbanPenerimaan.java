package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the LBAN_PENERIMAAN database table.
 * 
 */
@Entity
@Table(name="LBAN_PENERIMAAN")
@NamedQuery(name="LbanPenerimaan.findAll", query="SELECT l FROM LbanPenerimaan l")
public class LbanPenerimaan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Column(name="FLAG_CETAK")
	private String flagCetak;

	@Column(name="FLAG_DEL")
	private BigDecimal flagDel;

	@Id
	@Column(name="ID_KEND")
	private String idKend;

	private String ket;

	@Column(name="KODE_KANTOR_JR")
	private String kodeKantorJr;

	@Column(name="KODE_PENERIMAAN")
	private String kodePenerimaan;

	@Column(name="KODE_TRF")
	private String kodeTrf;

	@Temporal(TemporalType.DATE)
	@Column(name="MASA_LAKU_AKHIR")
	private Date masaLakuAkhir;

	@Column(name="NMR_RESI")
	private String nmrResi;

	@Temporal(TemporalType.DATE)
	@Column(name="TGL_PENERIMAAN")
	private Date tglPenerimaan;

	@Column(name="TOTAL_IW")
	private BigDecimal totalIw;

	@Column(name="TOTAL_SW")
	private BigDecimal totalSw;

	public LbanPenerimaan() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFlagCetak() {
		return this.flagCetak;
	}

	public void setFlagCetak(String flagCetak) {
		this.flagCetak = flagCetak;
	}

	public BigDecimal getFlagDel() {
		return this.flagDel;
	}

	public void setFlagDel(BigDecimal flagDel) {
		this.flagDel = flagDel;
	}

	public String getIdKend() {
		return this.idKend;
	}

	public void setIdKend(String idKend) {
		this.idKend = idKend;
	}

	public String getKet() {
		return this.ket;
	}

	public void setKet(String ket) {
		this.ket = ket;
	}

	public String getKodeKantorJr() {
		return this.kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodePenerimaan() {
		return this.kodePenerimaan;
	}

	public void setKodePenerimaan(String kodePenerimaan) {
		this.kodePenerimaan = kodePenerimaan;
	}

	public String getKodeTrf() {
		return this.kodeTrf;
	}

	public void setKodeTrf(String kodeTrf) {
		this.kodeTrf = kodeTrf;
	}

	public Date getMasaLakuAkhir() {
		return this.masaLakuAkhir;
	}

	public void setMasaLakuAkhir(Date masaLakuAkhir) {
		this.masaLakuAkhir = masaLakuAkhir;
	}

	public String getNmrResi() {
		return this.nmrResi;
	}

	public void setNmrResi(String nmrResi) {
		this.nmrResi = nmrResi;
	}

	public Date getTglPenerimaan() {
		return this.tglPenerimaan;
	}

	public void setTglPenerimaan(Date tglPenerimaan) {
		this.tglPenerimaan = tglPenerimaan;
	}

	public BigDecimal getTotalIw() {
		return this.totalIw;
	}

	public void setTotalIw(BigDecimal totalIw) {
		this.totalIw = totalIw;
	}

	public BigDecimal getTotalSw() {
		return this.totalSw;
	}

	public void setTotalSw(BigDecimal totalSw) {
		this.totalSw = totalSw;
	}

}