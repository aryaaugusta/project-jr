package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PL_MAPPING_RS_INSTANSI database table.
 * 
 */
@Entity
@Table(name="PL_MAPPING_RS_INSTANSI")
@NamedQuery(name="PlMappingRsInstansi.findAll", query="SELECT p FROM PlMappingRsInstansi p")
@IdClass(PlMappingRsInstansiPK.class)
public class PlMappingRsInstansi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="KODE_INSTANSI")
	private String kodeInstansi;

	@Id
	@Column(name="KODE_RUMAHSAKIT")
	private String kodeRumahsakit;

	public PlMappingRsInstansi() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getKodeInstansi() {
		return this.kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public String getKodeRumahsakit() {
		return this.kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

}