package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the LBAN_ARMADA database table.
 * 
 */
@Entity
@Table(name="LBAN_ARMADA")
@NamedQuery(name="LbanArmada.findAll", query="SELECT l FROM LbanArmada l")
public class LbanArmada implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="ALAMAT_PEMILIK")
	private String alamatPemilik;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Id
	@Column(name="ID_KEND")
	private String idKend;

	@Column(name="JENIS_BBM")
	private String jenisBbm;

	@Column(name="JML_CC")
	private BigDecimal jmlCc;

	@Column(name="KODE_GOL")
	private String kodeGol;

	@Column(name="KODE_JENIS")
	private String kodeJenis;

	@Column(name="KODE_PO")
	private String kodePo;

	private String merk;

	@Column(name="NAMA_PEMILIK")
	private String namaPemilik;

	@Column(name="NO_MESIN")
	private String noMesin;

	@Column(name="NO_RANGKA")
	private String noRangka;

	private String nopol;

	private BigDecimal seat;

	@Column(name="UPDATED_BY")
	private String updatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="UPDATED_DATE")
	private Date updatedDate;

	@Column(name="WARNA_PLAT")
	private String warnaPlat;

	public LbanArmada() {
	}

	public String getAlamatPemilik() {
		return this.alamatPemilik;
	}

	public void setAlamatPemilik(String alamatPemilik) {
		this.alamatPemilik = alamatPemilik;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getIdKend() {
		return this.idKend;
	}

	public void setIdKend(String idKend) {
		this.idKend = idKend;
	}

	public String getJenisBbm() {
		return this.jenisBbm;
	}

	public void setJenisBbm(String jenisBbm) {
		this.jenisBbm = jenisBbm;
	}

	public BigDecimal getJmlCc() {
		return this.jmlCc;
	}

	public void setJmlCc(BigDecimal jmlCc) {
		this.jmlCc = jmlCc;
	}

	public String getKodeGol() {
		return this.kodeGol;
	}

	public void setKodeGol(String kodeGol) {
		this.kodeGol = kodeGol;
	}

	public String getKodeJenis() {
		return this.kodeJenis;
	}

	public void setKodeJenis(String kodeJenis) {
		this.kodeJenis = kodeJenis;
	}

	public String getKodePo() {
		return this.kodePo;
	}

	public void setKodePo(String kodePo) {
		this.kodePo = kodePo;
	}

	public String getMerk() {
		return this.merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public String getNamaPemilik() {
		return this.namaPemilik;
	}

	public void setNamaPemilik(String namaPemilik) {
		this.namaPemilik = namaPemilik;
	}

	public String getNoMesin() {
		return this.noMesin;
	}

	public void setNoMesin(String noMesin) {
		this.noMesin = noMesin;
	}

	public String getNoRangka() {
		return this.noRangka;
	}

	public void setNoRangka(String noRangka) {
		this.noRangka = noRangka;
	}

	public String getNopol() {
		return this.nopol;
	}

	public void setNopol(String nopol) {
		this.nopol = nopol;
	}

	public BigDecimal getSeat() {
		return this.seat;
	}

	public void setSeat(BigDecimal seat) {
		this.seat = seat;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedDate() {
		return this.updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getWarnaPlat() {
		return this.warnaPlat;
	}

	public void setWarnaPlat(String warnaPlat) {
		this.warnaPlat = warnaPlat;
	}

}