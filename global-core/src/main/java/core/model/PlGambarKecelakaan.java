package core.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PL_GAMBAR_KECELAKAAN database table.
 * 
 */
@Entity
@Table(name="PL_GAMBAR_KECELAKAAN")
@NamedQuery(name="PlGambarKecelakaan.findAll", query="SELECT p FROM PlGambarKecelakaan p")
public class PlGambarKecelakaan implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="CREATED_BY")
	private String createdBy;

	@Temporal(TemporalType.DATE)
	@Column(name="CREATION_DATE")
	private Date creationDate;

	@Lob
	private byte[] gambar;

	@Column(name="ID_KECELAKAAN")
	private String idKecelakaan;

	private String judul;

	private String keterangan;

	@Column(name="LAST_UPDATED_BY")
	private String lastUpdatedBy;

	@Temporal(TemporalType.DATE)
	@Column(name="LAST_UPDATED_DATE")
	private Date lastUpdatedDate;

	private String namafile;

	@Id
	@Column(name="NO_URUT")
	private BigDecimal noUrut;

	public PlGambarKecelakaan() {
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return this.creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public byte[] getGambar() {
		return this.gambar;
	}

	public void setGambar(byte[] gambar) {
		this.gambar = gambar;
	}

	public String getIdKecelakaan() {
		return this.idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getJudul() {
		return this.judul;
	}

	public void setJudul(String judul) {
		this.judul = judul;
	}

	public String getKeterangan() {
		return this.keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return this.lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getNamafile() {
		return this.namafile;
	}

	public void setNamafile(String namafile) {
		this.namafile = namafile;
	}

	public BigDecimal getNoUrut() {
		return this.noUrut;
	}

	public void setNoUrut(BigDecimal noUrut) {
		this.noUrut = noUrut;
	}

}