package core.dao;

import java.util.List;

import core.model.PlRequestPerubahan;

public interface PlRequestPerubahanCustomDao {
	public List<Object[]> getListPPDataPengajuan(String noBerkas,String status, String search);
	public int deletePermintaan(PlRequestPerubahan plRequestPerubahan);

}
