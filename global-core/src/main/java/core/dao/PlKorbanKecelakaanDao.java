package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlKorbanKecelakaan;

public interface PlKorbanKecelakaanDao extends JpaRepository<PlKorbanKecelakaan, String>{

	@Query("select a from PlKorbanKecelakaan a where a.idKecelakaan = :idKec")
	public List<PlKorbanKecelakaan> getKorbanDariLaka(@Param("idKec")String idKec);
	
	@Query("select a from PlKorbanKecelakaan a where a.idKorbanKecelakaan = :idKorban")
	public List<PlKorbanKecelakaan> getKorbanDariID(@Param("idKorban")String idKorban);
}
