package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlPengajuanR;

public interface PlPengajuanRsDao extends JpaRepository<PlPengajuanR, String>{
	
	@Query("select a from PlPengajuanR a where a.noBerkas like :noBerkas")
	public List<PlPengajuanR> findPengajuanRsByNoBerkas(@Param("noBerkas")String noBerkas);

}
