package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlRsMapBpj;
import core.model.PlRsMapBpjPK;

public interface PlRsMapBpjDao extends JpaRepository<PlRsMapBpj, PlRsMapBpjPK> {
	
	
	@Query("select distinct a.loketPenanggungjawab, b.nama "
			+ "from PlRsMapBpj a, FndKantorJasaraharja b where "
			+ "a.loketPenanggungjawab = b.kodeKantorJr "
			+ "order by a.loketPenanggungjawab asc")
	public List<Object[]> getDistinctLoketPj();
	
	@Query("select distinct a.loketPenanggungjawab, b.nama, a.kodeBpjs, a.namaRsBpjs from PlRsMapBpj a, FndKantorJasaraharja b where "
			+ "a.loketPenanggungjawab = b.kodeKantorJr and "
			+ "a.kodeRumahsakit = :kodeRumahsakit "
			+ "order by a.loketPenanggungjawab asc")
	public List<Object[]> getDistinctLoketPjByKodeRumahSakit(@Param("kodeRumahsakit")String kodeRumahsakit);
	
	@Query("select a.loketPenanggungjawab, b.nama, "
			+ "a.alamatRsBpjs, a.createdBy, a.creationDate, "
			+ "a.flagKodeBpjsDobel, a.kabKotaRsBpjs, a.kodeBpjs, a.kodeInstansi, "
			+ "a.lastUpdatedBy, a.lastUpdatedDate, a.namaRsBpjs, a.propinsiRsBpjs, "
			+ "a.kodeRumahsakit "
			+ "from PlRsMapBpj a, FndKantorJasaraharja b where "
			+ "a.loketPenanggungjawab = b.kodeKantorJr and "
			+ "a.loketPenanggungjawab like :loketPenanggungjawab "
			+ "order by a.loketPenanggungjawab asc")
	public List<Object[]> getBpjsByLoket(@Param("loketPenanggungjawab")String loketPenanggungjawab);

}
