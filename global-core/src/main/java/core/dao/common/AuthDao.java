package core.dao.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.AuthElement;
import core.model.AuthGroupMember;
import core.model.AuthUser;
import core.model.FndKantorJasaraharja;

public interface AuthDao  extends JpaRepository<AuthUser, String>{

	@Query(value = "select a from AuthUser a where a.login = :login and "
			+ "((a.startDate <= sysdate and a.expiryDate > sysdate and a.passwordNeverExpire = 'N' ) "
			+ "or a.passwordNeverExpire = 'Y')")
	public AuthUser getUser(@Param("login")String userLogin);
	
	@Query(value = "select a from AuthGroupMember a where a.login = :login") 
	public List<AuthGroupMember> getGroupFromLogin (@Param("login")String userLogin);
	
	@Query(value ="select a,b from AuthGroupPermission a, AuthElement b where a.elementCode = b.elementCode "
			+ " and a.groupCode = :groupCode")
	public List<Object[]> getElementByGroup(@Param("groupCode")String groupCode);
	
	/*
	 * select distinct(a.element_code), a.element_header, a.description from 
auth_group_member c, auth_group_permission b, auth_element a where a.element_code = b.element_code
and b.group_code = c.group_code and c.login = 'userTest03'
	 */
	
	@Query(value = "select a from AuthGroupMember c, "
			+ " AuthGroupPermission b, AuthElement a where a.elementCode = b.elementCode and "
			+ " b.groupCode = c.groupCode and c.login = :login ")
	public List<AuthElement> getElemetsByLogin(@Param("login") String login);
	
	@Query(value="select a from FndKantorJasaraharja a where a.kodeKantorJr = :kodeKantor ")
	public List<FndKantorJasaraharja> getKantorFromLogin(@Param("kodeKantor") String kodeKantor);
	
}
