package core.dao;

import core.model.PlBerkasPengajuan;
import core.model.PlBerkasPengajuanPK;

public interface PlBerkasPengajuanCustomDao {
	
	public PlBerkasPengajuan findBerkas(PlBerkasPengajuanPK plBerkasPengajuanPK);
	public PlBerkasPengajuan findBerkas(String noBerkas, String kodeBerkas);
	public void saveBerkas(PlBerkasPengajuan berkas);
	public void updateBerkas(PlBerkasPengajuan berkas);
	
}
