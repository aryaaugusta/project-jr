package core.dao;

import java.util.List;

import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import core.model.DasiJrRefCode;
import core.model.PlBerkasPengajuan;
import core.model.PlKorbanKecelakaan;
import core.model.PlPengajuanR;
import core.model.PlPengajuanSantunan;
import core.model.PlPenyelesaianSantunan;


public interface PlPengajuanSantunanDao extends JpaRepository<PlPengajuanSantunan, String>, PlPengajuanSantunanCustomDao{
	
	@Query("select a from PlPengajuanSantunan a where a.idKecelakaan = :idKecelakaan and a.idKorbanKecelakaan = :idKorban")
	public List<PlPengajuanSantunan> findPengajuanByKorban(@Param("idKecelakaan") String idKecelakaan,@Param("idKorban") String idKorban);

	@Query("select a from PlPengajuanSantunan a where a.idKorbanKecelakaan like :idKorban")
	public List<PlPengajuanSantunan> findPengajuanByIdKorban(@Param("idKorban") String idKorban);
	
	@Query("select a from PlPengajuanSantunan a where a.noBerkas like :noBerkas")
	public List<PlPengajuanSantunan> findByNoBerkas(@Param("noBerkas") String noBerkas);

	@Query("select a from PlPengajuanR a where a.noBerkas like :noBerkas")
	public List<PlPengajuanR> findPengajuanRsByNoBerkas(@Param("noBerkas") String noBerkas);

	@Query("select a from PlPengajuanSantunan a where a.noBerkas like :noBerkas")
	public PlPengajuanSantunan findOneByNoBerkas(@Param("noBerkas") String noBerkas);

	@Query("select a from PlBerkasPengajuan a where a.noBerkas like :noBerkas")
	public List<PlBerkasPengajuan> findBerkasPengajuanByNoBerkas(@Param("noBerkas") String noBerkas);

	//Starting query used in absah
	@Query("select a, b, c from PlPengajuanSantunan a, PlKorbanKecelakaan b, DasiJrRefCode c "
			+ "where a.idKorbanKecelakaan = b.idKorbanKecelakaan and a.statusProses = c.rvLowValue "
			+ "and c.rvDomain = 'KODE STATUS PROSES' and a.noBerkas = :noBerkas ")
	public List<Object[]> getPengajuanByNoBerkas(@Param("noBerkas")String noBerkas);
	
	@Query("select a, b, c from PlPengajuanSantunan a, PlKorbanKecelakaan b, DasiJrRefCode c "
			+ "where a.idKorbanKecelakaan = b.idKorbanKecelakaan and a.statusProses = c.rvLowValue "
			+ "and c.rvDomain = 'KODE STATUS PROSES' and a.noBerkas in :noBerkas ")
	public List<Object[]> getListPengajuanByNoBerkas(@Param("noBerkas")List<String> noBerkas);
	
	@Query(value="select no_berkas from pl_pengajuan_santunan where id_kecelakaan = :idKec and seq_no >= "+
	"(select decode((select max(seq_no) from pl_pengajuan_santunan where id_kecelakaan = :idKec and dilimpahkan_ke is not null) " +
	", null "+
	", 0 "+
	", (select max(seq_no) from pl_pengajuan_santunan where id_kecelakaan = :idKec and dilimpahkan_ke is not null)) from dual)", nativeQuery = true)
	public List<String> getPengajuanByIdKec(@Param("idKec") String idKec);
	
	//added by luthfi
	//getSantunanByIdKorban
	@Query("select a from PlPengajuanSantunan a where a.idKorbanKecelakaan like :idKorban")
	public List<PlPengajuanSantunan> getPengajuanByIdKorban(@Param("idKorban")String idKorban);
	
	//maryo
	//akumulassi pembayaran
	@Query("select b from "
			+ "PlPengajuanSantunan a, "
			+ "PlPenyelesaianSantunan b "
			+ "where a.noBerkas = b.noBerkas "
			+ "and a.idKorbanKecelakaan = :idKorban")
	public List<PlPenyelesaianSantunan> getAkumulasiPembayaran(@Param("idKorban")String idKorban);
	
	
}
