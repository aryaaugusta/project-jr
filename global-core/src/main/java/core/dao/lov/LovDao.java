package core.dao.lov;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import share.FndKantorJasaraharjaDto;
import share.PlDisposisiDto;
import core.model.AuthUser;
import core.model.DasiJrRefCode;
import core.model.FndBank;
import core.model.FndCamat;
import core.model.FndJenisKendaraan;
import core.model.FndKantorJasaraharja;
import core.model.FndLokasi;
import core.model.PlDisposisi;
import core.model.PlInstansi;
import core.model.PlJaminan;
import core.model.PlMappingCamat;
import core.model.PlPenyelesaianSantunan;
import core.model.PlRekeningR;
import core.model.PlRsMapBpj;
import core.model.PlRumahSakit;

public interface LovDao extends JpaRepository<DasiJrRefCode, String>,
		LovCustomDao {

	// added by Maryo
	@Query("select a from FndKantorJasaraharja a where "
			+ "a.nama like 'CABANG%' and "
			+ "(a.kodeKantorJr like UPPER(:search) or "
			+ "a.nama like UPPER(:search)) " + "order by a.kodeKantorJr asc")
	public List<FndKantorJasaraharja> lovWilayahKantor(
			@Param("search") String search);

	@Query("select a from FndKantorJasaraharja a where "
			+ "a.kodeKantorJr like (SUBSTR(:kantor,1,2) || '0' || SUBSTR(:kantor,5,2) || '%') and "
			+ "(a.kodeKantorJr like UPPER(:search) or "
			+ "a.nama like UPPER(:search)) " + "order by a.kodeKantorJr asc")
	public List<FndKantorJasaraharja> lovWilayahKantor(
			@Param("search") String search, @Param("kantor") String kantor);

	// added by Luthfi
	// combobox list kode nama rumahsakit
	@Query("select a from PlRumahSakit a  where "
			+ "a.kodeRumahsakit like UPPER(:kodeWilayah) and "
			+ "(a.kodeRumahsakit like UPPER(:search) or "
			+ "a.deskripsi like UPPER(:search))")
	public List<PlRumahSakit> lovKodeNamaRs(
			@Param("kodeWilayah") String kodeWilayah,
			@Param("search") String search);

	// get kode lokasi by camat (pl mapping camat)
	@Query("select a from PlMappingCamat a where " + "a.kodeCamat = :kodeCamat")
	public PlMappingCamat getLokasiByCamat(@Param("kodeCamat") String kodeCamat);

	// get kode camat by lokasi (pl mapping camat)
	@Query("select a from PlMappingCamat a where " + "a.kodeLokasi = :kodeLokasi")
	public PlMappingCamat getCamatByLokasi(@Param("kodeLokasi") String kodeLokasi);
	
	// combobox gadget
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'GADGET' and a.flagEnable = 'Y'")
	public List<Object[]> getGadget();

	// combobox identitas
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'KODE JENIS ID DATARS' and "
			+ "a.rvHighValue = 'RS' and " + "a.flagEnable = 'Y'")
	public List<Object[]> getJenisId();

	// combobox sifat cidera
	@Query("select a.rvLowValue, a.rvMeaning ,a.rvHighValue from DasiJrRefCode a where "
			+ "a.rvDomain = 'KODE SIFAT CIDERA' and " + "a.flagEnable = 'Y'")
	public List<Object[]> getKodeSifatCidera();

	// combobox sifat kecelakaan
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'KODE SIFAT KECELAKAAN' and "
			+ "a.flagEnable = 'Y'")
	public List<Object[]> getKodeSifatKecelakaan();

	// combobox status nikah
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'KODE STATUS NIKAH' and " + "a.flagEnable = 'Y'")
	public List<Object[]> getKodeStatusNikah();

	// combobox status korban
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'KODE STATUS KORBAN' and " + "a.flagEnable = 'Y'")
	public List<Object[]> getKodeStatusKorban();

	// combobox jenis pekerjaan
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'KODE JENIS PEKERJAAN' and "
			+ "a.flagEnable = 'Y' order by a.rvMeaning asc")
	public List<Object[]> getKodeJenisPekerjaan();

	// combobox instansi pembuat
	@Query("select a.rvMeaning, a.rvHighValue, a.rvLowValue from DasiJrRefCode a where "
			+ "a.rvDomain = 'INSTANSI PEMBUAT LK' and "
			+ "a.flagEnable = 'Y' order by a.rvHighValue asc")
	public List<Object[]> getInstansiPembuat();

	// combobox kasus kecelakaan
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'PL KASUS KECELAKAAN' and "
			+ "a.flagEnable = 'Y' order by a.rvLowValue asc")
	public List<Object[]> getKasusKecelakaan();

	// combobox status kendaraan
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'KODE STATUS KENDARAAN' and "
			+ "a.flagEnable = 'Y' order by a.rvMeaning asc")
	public List<Object[]> getStatusKendaraan();

	// combobox jenis sim
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'KODE JENIS SIM' and "
			+ "a.flagEnable = 'Y' order by a.rvMeaning asc")
	public List<Object[]> getJenisSim();

	// combobox merk kendaraan
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'PL MERK KENDARAAN' and "
			+ "a.flagEnable = 'Y' order by a.rvMeaning asc")
	public List<Object[]> getMerkKendaraan();

	// combobox lingkup jaminan
	@Query("select distinct a.lingkupJaminan from PlJaminan a")
	public List<Object> getLingkupJaminan();

	// combobox asal berkas
	@Query("select a from FndKantorJasaraharja a where "
			+ "(a.levelKantor = 'CA' or " + "a.levelKantor = 'CB' or "
			+ "a.levelKantor = 'CC') and "
//			+ "ROWNUM <= 500 and "
//			+ "a.kodeKantorJr like '%01' and "
			+ "(a.kodeKantorJr like UPPER(:search) or "
			+ "a.nama like UPPER(:search) and a.flagEnable = 'Y') order by a.kodeKantorJr asc")
	public List<FndKantorJasaraharja> getKantorAsalBerkas(
			@Param("search") String search);

	// combobox kantor samsat
	@Query("select a from FndKantorJasaraharja a where "
			+ "a.levelKantor = 'SM' and "
			+ "(a.kodeKantorJr like UPPER(:search) or "
			+ "a.nama like UPPER(:search)) "
			+ "AND a.flagEnable = 'Y' "
//			+ "and ROWNUM <= 100 "
			+ "order by a.kodeKantorJr asc")
	public List<FndKantorJasaraharja> getKantorSamsat(
			@Param("search") String search);

	// combobox instansi(data kecelakaan)
	@Query("select a from PlInstansi a where "
			+ "(a.kodeInstansi like UPPER(:search) or "
			+ "a.deskripsi like UPPER(:search)) "
			+ "and a.flagEnable = 'Y' "
//			+ "and ROWNUM <= 100 "
			+ "order by a.kodeInstansi asc")
	public List<PlInstansi> getAllInstansi(@Param("search") String search);

	// combobox lokasi
	@Query("select a from FndLokasi a WHERE a.flagEnable = 'Y'")
	public List<FndLokasi> getAllLokasi();

	// combobox lokasi
	@Query("select a from FndLokasi a WHERE a.flagEnable = 'Y' and a.kodeLokasi like :kodeLokasi")
	public List<FndLokasi> findOneLokasi(@Param("kodeLokasi") String kodeLokasi);

	// combobox jenis pertanggungan
	@Query("select a from PlJaminan a")
	public List<PlJaminan> getJenisPertanggungan();

	// added by Meilona
	// combobox list provinsi
	@Query("select distinct a.kodeProvinsi, a.namaProvinsi from FndCamat a where "
			+ "a.kodeProvinsi like UPPER(:search) or a.namaProvinsi like UPPER (:search) order by a.kodeProvinsi asc")
	public List<Object[]> getProvinsi(@Param("search") String search);

	// combobox list kabupaten/kota
	@Query("select distinct a.kodeKabkota, a.namaKabkota from FndCamat a where "
			+ "a.kodeProvinsi = :kodeProvinsi and "
			+ "(a.kodeKabkota like UPPER (:search) or a.namaKabkota like UPPER (:search)) "
			+ "order by a.kodeKabkota")
	public List<Object[]> getKabkotaByProvinsi(
			@Param("kodeProvinsi") String kodeProvinsi,
			@Param("search") String search);

	// combobox list camat
	@Query("select a.kodeCamat, a.namaCamat from FndCamat a where "
			+ "a.kodeKabkota like :kodeKabkota and "
			+ "(a.kodeCamat like UPPER(:search) or "
			+ "a.namaCamat like UPPER(:search))")
	public List<Object[]> getCamatByKabkota(
			@Param("kodeKabkota") String kodeKabkota,
			@Param("search") String search);

	// maryo dasi jr ref codes by rvdomain
	@Query("select a.rvLowValue,a.rvHighValue,a.rvMeaning,a.flagEnable,a.rvAbbreviation,a.orderSeq "
			+ "from DasiJrRefCode a where a.rvDomain like :rvDomain and a.flagEnable like :flag "
			+ "and (a.rvMeaning like :search or a.rvLowValue like :search)")
	public List<Object[]> getJrRefCode(@Param("rvDomain") String rvDomain,
			@Param("flag") String flag, @Param("search") String search);

	// added by Luthfi
	// list kode berkas/jenis dokumen untuk tambah lampiran
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a where "
			+ "a.rvDomain = 'KODE BERKAS' and a.flagEnable = 'Y' "
			+ "order by a.rvMeaning asc")
	public List<Object[]> getJenisDokumen();

	// combobox for loket bpjs
	@Query("select distinct a.loketPenanggungjawab, b.nama "
			+ "from PlRsMapBpj a, FndKantorJasaraharja b where "
			+ "a.loketPenanggungjawab = b.kodeKantorJr and "
			+ "b.kodeKantorJr like :kodeKantorJr and "
			+ "(a.loketPenanggungjawab like UPPER(:search) or "
			+ "b.nama like UPPER(:search)) "
			+ "order by a.loketPenanggungjawab asc")
	public List<Object[]> getDistinctLoketPj(@Param("search") String search,
			@Param("kodeKantorJr")String kodeKantorJr);

	// combobox for jenis kendaraan
	@Query("select a from FndJenisKendaraan a where "
			+ "a.kodeJenis like 'A%' or " + "a.kodeJenis like 'B%' or "
			+ "a.kodeJenis like 'C%' or " + "a.kodeJenis like 'D%' or "
			+ "a.kodeJenis like 'E%' or " + "a.kodeJenis like 'F%' or "
			+ "a.kodeJenis like 'G%' or " + "a.kodeJenis like 'KL%' or "
			+ "a.kodeJenis like 'P%' order by a.kodeJenis asc ")
	public List<FndJenisKendaraan> getListJenisKendaraan();

	// Meilona
	// ListKantor
	@Query("select a from FndKantorJasaraharja a where "
			+ "a.kodeKantorJr like :userLogin")
	public List<FndKantorJasaraharja> findKantor(
			@Param("userLogin") String userLogin);

	// For combobox Status Proses Pengajuan Santunan
	@Query("select a.rvMeaning, a.rvLowValue from DasiJrRefCode a "
			+ "where a.rvDomain='KODE STATUS PROSES' and a.flagEnable ='Y' ")
	public List<Object[]> getDataProses();
	
	// For combobox Jenis TL 
	@Query("select a.rvMeaning, a.rvLowValue from DasiJrRefCode a "
			+ "where a.rvDomain='TINDAK LANJUT GL' and a.flagEnable ='Y' ")
	public List<Object[]> getJenisTl();

	// for combobox Data Penyelesaian
	@Query("select a.rvMeaning, a.rvLowValue from DasiJrRefCode a "
			+ "where a.rvDomain='KODE OTORISASI' and a.flagEnable ='Y'")
	public List<Object[]> getDataPenyelesaian();

	@Query("select a.rvMeaning, a.rvLowValue from DasiJrRefCode a "
			+ "where a.rvDomain ='KODE HUBUNGAN KORBAN' and "
			+ "a.flagEnable ='Y' order by a.rvLowValue asc")
	public List<Object[]> getDataHubungan();

	@Query("select a.rvMeaning, a.rvLowValue from DasiJrRefCode a "
			+ "where a.rvDomain= 'KODE JENIS IDENTITAS' "
			+ "and a.flagEnable ='Y' ")
	public List<Object[]> getIdentitas();

	// MARYO FIND ALL KANTOR
	@Query("select a from FndKantorJasaraharja a where "
			+ "(a.kodeKantorJr like UPPER(:search) or "
			+ "a.nama like UPPER(:search)) "
			+ "and ROWNUM <= 100 "
			+ "order by a.kodeKantorJr asc")
	public List<FndKantorJasaraharja> lovKantor(@Param("search") String search);
	
	@Query("select a from FndKantorJasaraharja a where "
			+ "(a.kodeKantorJr like UPPER(:search) or "
			+ "a.nama like UPPER(:search)) "
			+ "and a.kodeKantorJr like :kantor "
			+ "order by a.kodeKantorJr asc")
	public List<FndKantorJasaraharja> lovKantorbyLogin(@Param("search") String search, 
			@Param("kantor") String kantor);

	// maryo find all rs
	@Query("select a from PlRumahSakit a where "
			+ "a.kodeRumahsakit like UPPER(:search) or "
			+ "a.deskripsi like UPPER(:search)")
	public List<PlRumahSakit> getAllRS(@Param("search") String search);
	
	@Query("select a.kodeRumahsakit, a.deskripsi from PlRumahSakit a where a.kodeRumahsakit like :kodeRumahSakit")
	public List<Object[]> findOneRs(@Param("kodeRumahSakit") String kodeRumahSakit);


	// Meilona Jenis Pembayaran
	@Query("select a.rvLowValue, a.rvMeaning " + "from DasiJrRefCode a "
			+ "where a.rvDomain= 'KODE PENGAJUAN' " + "and a.flagEnable ='Y' "
			+ "order by rvLowValue")
	public List<Object[]> getJenisPembayaran();

	@Query("select a " + "from AuthUser a "
			+ "where a.login like UPPER(:search) "
			+ "or a.userName like UPPER(:search)")
	public List<AuthUser> getAllUSer(@Param("search") String search);

	// ADDED BY LUTFI
	// KEPALA CABANG
	@Query("select a from AuthUser a where a.attribute1 like :kantorUser and UPPER(a.description) like UPPER('kepala cabang%')")
	public List<AuthUser> getKepalaCabangByUser(
			@Param("kantorUser") String kantorUser);

	// combobox kesimpulan sementara
	@Query("select a.rvMeaning, a.rvLowValue, a.rvHighValue from DasiJrRefCode a "
			+ "where a.rvDomain= 'KODE KESIMPULAN SEMENTARA' "
			+ "and a.flagEnable ='Y' ")
	public List<Object[]> getKesimpulanSementara();

	// combobox kode otorisasi flag
	@Query("select a.rvMeaning, a.rvLowValue, a.rvHighValue from DasiJrRefCode a "
			+ "where a.rvDomain= 'KODE OTORISASI FLAG' "
			+ "and a.flagEnable ='Y' ")
	public List<Object[]> getOtorisasiFlag();

	// combobox jenis pembayaran
	@Query("select a.rvMeaning, a.rvLowValue, a.rvHighValue from DasiJrRefCode a "
			+ "where a.rvDomain= 'KODE JAMINAN PEMBAYARAN' "
			+ "and a.flagEnable ='Y' ")
	public List<Object[]> getKodeJenisPembayaran();
	
	//nurmaryo
	@Query("select a.nama,a.kodeKantorJr from FndKantorJasaraharja a "
			+ "where a.kodeKantorJr like :kodeKantorJr")
	public List<Object[]> getOneKantorByKode(@Param("kodeKantorJr")String kodeKantorJr);
	
	//ADDED BY LUTHFI
	//findOne Otorisasi dilimpahkan
	@Query("select a from FndKantorJasaraharja a where a.kodeKantorJr like :kodeKantor")
	public List<FndKantorJasaraharja> getKantorByKodeKantor(@Param("kodeKantor")String kodeKantor);
	
	//added by luthfi
	//find pl penyelesaian santunan by no berkas
	@Query("select a from PlPenyelesaianSantunan a where a.noBerkas like :noBerkas")
	public List<PlPenyelesaianSantunan> getPenyelesaianSantunanByNoBerkas(@Param("noBerkas")String noBerkas);
	
	//added by luthfi
	//find all rv domain kode disposisi 
	@Query("select a.rvLowValue, a.rvHighValue, a.rvAbbreviation, a.rvMeaning from DasiJrRefCode a where a.rvDomain = 'KODE DISPOSISI' and a.flagEnable = 'Y'")
	public List<Object[]> listKodeDisposisi();
	
	@Query("select a from PlDisposisi a where a.idDisposisi like :idDisposisi")
	public List<PlDisposisi> findDisposisiByIdDisposisi(@Param("idDisposisi")String idDisposisi);
	
	//find ref code
	@Query("select a from DasiJrRefCode a where a.rvLowValue = :lowValue "
			+ "and upper(a.rvDomain) = upper(:domain) "
			+ "and a.flagEnable = 'Y' ")
	public List<DasiJrRefCode> getRef(@Param("lowValue")String lowValue, @Param("domain")String domain);
	
	@Query("select a from DasiJrRefCode a where a.rvDomain = 'KODE STATUS PROSES' ")
	public List<DasiJrRefCode> getAllProses();
	
	@Query("select a from FndBank a where upper(a.namaBank) like upper(:search)")
	public List<FndBank> getAllBank(@Param("search")String search);
	
	@Query("select a from FndBank a where a.kodeBank like :kodeBank")
	public List<FndBank> findOneBank(@Param("kodeBank")String kodeBank);
	
	//find ref code
	@Query("select a from DasiJrRefCode a where a.rvDomain = :domain and a.rvHighValue = :rvHighValue "
			+ "and a.flagEnable = 'Y' order by a.orderSeq ")
	public List<DasiJrRefCode> getRefByRvDomainRvHigh(@Param("domain") String domain, @Param("rvHighValue") String rvHigh);
	
	//find ref code 2
		@Query("select a from DasiJrRefCode a where a.rvDomain = :domain "
				+ "and a.flagEnable = 'Y' order by a.orderSeq ")
		public List<DasiJrRefCode> getRefByRvDomain(@Param("domain") String domain);
		
	
	//added by luthfi
	//instansi menu monitoring data laka irsms
	@Query("select a.deskripsi, a.kodeInstansi from "
			+ "PlMappingPolda a, PlInstansi b "
			+ "where "
			+ "a.kodeInstansi = b.kodeInstansi "
			+ "and a.kodeInstansi like :kodeInstansi "
			+ "and a.idDistricts not like '%a' "
			+ "and b.flagEnable = 'Y' "
			+ "and (a.kodeInstansi like :search) "
			+ "order by a.kodeInstansi ")
	public List<Object[]> getInstansiMonitoringIrsms(@Param("kodeInstansi") String kodeInstansi, @Param("search")String search);	
	
	//KETERANGAN COKLIT
		@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a "
				+ "where a.rvDomain = 'PL KET COKLIT' and a.flagEnable = 'Y' order by a.rvLowValue asc")
		public List<Object[]> getKeteranganCoklit();
	
	//added by Meilona
	//jenis laporan for Monitoring Laka belum tuntas
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a "
			+ "where a.rvDomain = 'KODE JENIS LAPORAN'")
	public List<Object[]> getJenisLaporan();
	
	//Status/Response for Monitoring Data Laka RS
	@Query("select a.rvLowValue, a.rvMeaning from DasiJrRefCode a "
			+ "where a.rvDomain = 'FIRSTRESPONSE RS' OR a.rvDomain = 'FASTRESPONSE RS'")
	public List<Object[]> getResponse();
	
	//PIC RS monitoring data Laka RS
	@Query("select a.login, a.userName from AuthUser a")
	public List<Object[]> getPicLakaRs();
	
	// tambah response(Response List) monitoring data laka rs
	@Query("select a.rvLowValue,a.rvMeaning from DasiJrRefCode a "
			+ "where a.rvDomain = 'FIRSTRESPONSE RS' and a.flagEnable ='P'")
	public List<Object[]> getAddResponseList();
	
	//sublist tambah response monitoring data laka rs
	@Query("select a.rvLowValue, a.rvMeaning "
			+ "from DasiJrRefCode a where a.rvDomain ='FASTRESPONSE RS' "
			+ "order by a.rvLowValue desc")
	public List<Object[]> getKesimpulanSurvey();
	
	@Query("select a from FndCamat a where a.kodeCamat like :kodeCamat")
	public List<FndCamat> getAllByCamat(@Param("kodeCamat") String kodeCamat);
	
	@Query("select a from PlRsMapBpj a where a.kodeRumahsakit like :kodeRumahsakit")
	public List<PlRsMapBpj> getBpjsByKodeRs(@Param("kodeRumahsakit")String kodeRumahsakit);
	
	@Query("select a from AuthUser a where a.login like :login")
	public List<AuthUser> getUserByLogin(@Param("login")String login);
	
	@Query("select a from FndKantorJasaraharja a where a.kodeKantorJr like '%01' "
			+ "and (a.kodeKantorJr like UPPER(:search) OR "
			+ "a.nama like UPPER(:search)) and a.flagEnable = 'Y' "
			+ "order by a.kodeKantorJr asc ")
	public List<FndKantorJasaraharja> listLoketkantor(@Param("search")String search);
	
	@Query("select a from PlRekeningR a where "
			+ "UPPER(a.kodeBank) like UPPER(:kodeBank) and "
			+ "UPPER(a.kodeRumahsakit) like UPPER(:kodeRumahsakit)")
	public List<PlRekeningR>listRekening(@Param("kodeBank")String kodeBank, 
			@Param("kodeRumahsakit")String kodeRumahsakit);
		
}
