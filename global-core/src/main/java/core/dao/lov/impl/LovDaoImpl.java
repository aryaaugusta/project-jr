package core.dao.lov.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.lov.LovCustomDao;
import core.model.PlDisposisi;
import core.model.PlMappingCamat;
import core.model.PlRegisterSementara;

public class LovDaoImpl implements LovCustomDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Object[]> listLokasi(String search) {
		
		String query = "select a.KODE_LOKASI, b.NAMA_CAMAT, b.NAMA_KABKOTA "
				+ "from PL_MAPPING_CAMAT a, FND_CAMAT b where a.KODE_CAMAT = b.KODE_CAMAT and "
				+ "(a.KODE_LOKASI like UPPER('"+search+"') or "
				+ "b.NAMA_CAMAT like UPPER('"+search+"') or "
				+ "b.NAMA_KABKOTA like UPPER('"+search+"')) "
						+ "and ROWNUM <= 100";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	@Override
	public List<Object[]> getAllRSRegister(String search) {
		String query = "SELECT A.KODE_RUMAHSAKIT,A.DESKRIPSI FROM PL_RUMAH_SAKIT A "
				+ "WHERE (UPPER(A.DESKRIPSI) LIKE UPPER('"+search+"') "
				+ "OR UPPER(A.KODE_RUMAHSAKIT)  LIKE UPPER('"+search+"')) "
				+ "AND A.FLAG_ENABLE = 'Y' "
//				+ "AND ROWNUM <= 500 "
				+ "ORDER BY A.KODE_RUMAHSAKIT ASC";
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;	
	}

	@Override
	public List<Object[]> listDisposisi(String noBerkas) {
		
		String query="SELECT KET.RV_MEANING, A.TGL_DISPOSISI,  A.DISPOSISI, A.ID_DISPOSISI, A.DARI "
				+ "FROM PL_DISPOSISI A "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_HIGH_VALUE, RV_MEANING, RV_ABBREVIATION "
				+ "FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE DISPOSISI') KET "
				+ "ON A.LEVEL_CABANG_DISP = KET.RV_HIGH_VALUE "
				+ "WHERE A.NO_BERKAS LIKE '"+noBerkas+"' "
				+ "AND A.DARI = KET.RV_LOW_VALUE ORDER BY A.TGL_DISPOSISI ASC ";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	/* (non-Javadoc)
	 * @see core.dao.lov.LovCustomDao#getKodeDisposisi(java.lang.String, java.lang.String)
	 */
	/* (non-Javadoc)
	 * @see core.dao.lov.LovCustomDao#getKodeDisposisi(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Object[]> getKodeDisposisi(String statusProses,
			String levelKantor) {
		
		String query="SELECT DISTINCT A.RV_LOW_VALUE, "
				+ "A.RV_MEANING, "
				+ "RV_HIGH_VALUE, "
				+ "RV_ABBREVIATION "
				+ "FROM DASI_JR_REF_CODES A "
				+ "LEFT JOIN(SELECT KODE_KANTOR_JR,LEVEL_KANTOR "
				+ "FROM FND_KANTOR_JASARAHARJA ) KANTOR "
				+ "ON A.RV_HIGH_VALUE=KANTOR.LEVEL_KANTOR "
				+ "LEFT JOIN(SELECT STATUS_PROSES "
				+ "FROM PL_PENGAJUAN_SANTUNAN ) SANTUNAN "
				+ "ON A.RV_ABBREVIATION=SANTUNAN.STATUS_PROSES "
				+ "WHERE A.RV_ABBREVIATION LIKE '"+statusProses+"' "
				+ "AND A.RV_HIGH_VALUE LIKE '"+levelKantor+"' "
				+ "AND A.RV_DOMAIN = 'KODE DISPOSISI' ";
				
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	@Override
	public List<Object[]> getKantorDilimpahkan(String idKorbanKecelakaan) {

		String query = "SELECT A.ID_KORBAN_KECELAKAAN, "
				+ "CAMAT.LOKET_PNG_JWB, "
				+ "KANTOR.NAMA "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN (SELECT ID_KORBAN_KECELAKAAN, ID_GUID FROM PL_KORBAN_KECELAKAAN) KORBAN "
				+ "ON A.ID_KORBAN_KECELAKAAN = KORBAN.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN (SELECT KODE_LOKASI, KODE_CAMAT FROM PL_MAPPING_CAMAT) MAPPING_CAMAT "
				+ "ON KORBAN.ID_GUID = MAPPING_CAMAT.KODE_LOKASI "
				+ "LEFT JOIN (SELECT KODE_CAMAT, LOKET_PNG_JWB FROM FND_CAMAT) CAMAT "
				+ "ON MAPPING_CAMAT.KODE_CAMAT = CAMAT.KODE_CAMAT "
				+ "LEFT JOIN (SELECT KODE_KANTOR_JR, NAMA FROM FND_KANTOR_JASARAHARJA) KANTOR "
				+ "ON CAMAT.LOKET_PNG_JWB = KANTOR.KODE_KANTOR_JR "
				+ "WHERE A.ID_KORBAN_KECELAKAAN LIKE '"+idKorbanKecelakaan+"'";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;

	}

	@Override
	public List<Object[]> getValueLdbp(String noBerkas) {
		String query = "SELECT "
				+ "A.NO_BERKAS, "
				+ "A.TGL_PENERIMAAN, "
				+ "A.TGL_PENYELESAIAN, "
				+ "KORBAN.NAMA, "
				+ "DATA_LAKA.DESKRIPSI_KECELAKAAN, "
				+ "INSTANSI.DESKRIPSI, "
				+ "OTORISASI.RV_MEANING AS KESIMPULAN, "
				+ "KORBAN.KODE_JAMINAN, "
				+ "JAMINAN.KODE_LAPORAN "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN (SELECT * FROM PL_KORBAN_KECELAKAAN) KORBAN "
				+ "ON A.ID_KORBAN_KECELAKAAN = KORBAN.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN (SELECT ID_KECELAKAAN, DESKRIPSI_KECELAKAAN, KODE_INSTANSI FROM PL_DATA_KECELAKAAN) DATA_LAKA "
				+ "ON A.ID_KECELAKAAN = DATA_LAKA.ID_KECELAKAAN "
				+ "LEFT JOIN (SELECT KODE_INSTANSI, DESKRIPSI FROM PL_INSTANSI) INSTANSI "
				+ "ON DATA_LAKA.KODE_INSTANSI = INSTANSI.KODE_INSTANSI "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE OTORISASI FLAG') OTORISASI "
				+ "ON A.OTORISASI_FLAG = OTORISASI.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT * FROM PL_JAMINAN) JAMINAN "
				+ "ON KORBAN.KODE_JAMINAN = JAMINAN.KODE_JAMINAN "
				+ "WHERE A.NO_BERKAS LIKE '"+noBerkas+"'"; 
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();
		return list;

	}

	@Override
	public List<Object[]> getMetodePembayaran(String noBerkas) {
		String query="SELECT PEMBAYARAN.RV_MEANING AS METODE_PEMBAYARAN, "
				+ "BANK.NAMA_BANK, "
				+ "A.NO_REKENING, "
				+ "ADDITIONAL.NAMA_REKENING "
				+ "FROM PL_PENYELESAIAN_SANTUNAN A "
				+ "LEFT JOIN(SELECT KODE_RUMAHSAKIT,NO_BERKAS "
				+ "FROM PL_PENYELESAIAN_RS) PENYELESAIAN_RS "
				+ "ON A.NO_BERKAS=PENYELESAIAN_RS.NO_BERKAS "
				+ "LEFT JOIN(SELECT NO_BERKAS, NAMA_REKENING "
				+ "FROM PL_ADDITIONAL_DESC ) ADDITIONAL "
				+ "ON A.NO_BERKAS=ADDITIONAL.NO_BERKAS "
				+ "LEFT JOIN(SELECT KODE_BANK,NAMA_BANK "
				+ "FROM FND_BANK)BANK "
				+ "ON A.ID_GUID=BANK.KODE_BANK "
				+ "LEFT JOIN(SELECT RV_DOMAIN,RV_MEANING,RV_LOW_VALUE "
				+ "FROM DASI_JR_REF_CODES WHERE RV_DOMAIN='KODE JENIS PEMBAYARAN SANTUNAN') PEMBAYARAN "
				+ "ON A.JENIS_PEMBAYARAN=PEMBAYARAN.RV_LOW_VALUE "
				+ "WHERE A.NO_BERKAS LIKE '"+noBerkas+"'";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();
		return list;
	}

	@Override
	public int deleteDisposisi(PlDisposisi plDisposisi) {
		String query = "DELETE FROM PL_DISPOSISI WHERE ID_DISPOSISI = ? ";
		
		em.createNativeQuery(query)
			.setParameter(1, plDisposisi.getIdDisposisi()).executeUpdate();
			return 1;
	}

	@Override
	public List<String> getTahunPenerimaanFromPengajuanSantunan() {

		String query = "select distinct(to_char(tgl_penerimaan, 'YYYY')) from pl_pengajuan_santunan";
		@SuppressWarnings("unchecked")
		List<String> years = em.createNativeQuery(query).getResultList();
		
		return years;
	}
	
	@Override
	public List<String> getTahunPengajuanFromPengajuanSantunan() {
		
		String query = "select distinct(to_char(tgl_pengajuan, 'YYYY')) from pl_pengajuan_santunan";
		@SuppressWarnings("unchecked")
		List<String> years = em.createNativeQuery(query).getResultList();
		
		return years;
	}
	
	@Override
	public List<String> getTahunPenyelesaianFromPengajuanSantunan() {
		
		String query = "select distinct(to_char(tgl_Penyelesaian, 'YYYY')) from pl_pengajuan_santunan where tgl_Penyelesaian is not null";
		@SuppressWarnings("unchecked")
		List<String> years = em.createNativeQuery(query).getResultList();
		
		return years;
	}

	@Override
	public List<Object[]> getListCariDataLaka(String kejadianStartDate,
			String kejadianEndDate, String laporanStartDate,
			String laporanEndDate, String instansi, String noLaporan,
			String namaKorban, String jenisPertanggungan, String statusLp, String menu) {
		
		String tglKejadian="";
		if(kejadianStartDate!=null&&kejadianEndDate!=null){
			tglKejadian="(TRUNC (INSIDE.TGL_KEJADIAN) >= TRUNC (TO_DATE ('"+kejadianStartDate+"', 'DD/MM/YYYY')) AND TRUNC (INSIDE.TGL_KEJADIAN) <= TRUNC (TO_DATE ('"+kejadianEndDate+"', 'DD/MM/YYYY'))) AND ";
		}
		
		String tglLaporan="";
		if(laporanStartDate!=null&&laporanEndDate!=null){
			tglLaporan="INSIDE.TGL_LAPORAN_POLISI BETWEEN TO_DATE ('"+laporanStartDate+"', 'DD/MM/YYYY') AND TO_DATE ('"+laporanEndDate+"', 'DD/MM/YYYY') AND ";
		}
		
		String menuPengajuan = "";
		if(menu.equalsIgnoreCase("register")){
			menuPengajuan = "AND INSIDE.ID_KORBAN_KECELAKAAN NOT IN (SELECT ID_KORBAN_KECELAKAAN FROM PL_REGISTER_SEMENTARA WHERE ID_KORBAN_KECELAKAAN = INSIDE.ID_KORBAN_KECELAKAAN) ";
		}
		
		String query="SELECT * FROM (SELECT "
				+ "A.NO_LAPORAN_POLISI, "
				+ "A.TGL_KEJADIAN, "
				+ "NVL (NAMA.NAMA, '-') AS NAMA, "
				+ "NVL (JAMINAN.DESKRIPSI, '-') AS DESKRIP_JAMINAN, "
				+ "A.STATUS_LAPORAN_POLISI, "
				+ "A.ID_KECELAKAAN, "
				+ "NVL (NAMA.ID_KORBAN_KECELAKAAN, '-') AS ID_KORBAN_KECELAKAAN, "
				+ "A.TGL_LAPORAN_POLISI, "
				+ "NVL (NAMA.KODE_JAMINAN, '-') AS KODE_JAMINAN, "
				+ "A.KODE_INSTANSI, "
				+ "NVL (INSTANSI.DESKRIPSI, '-') AS INSTANSI "
				+ "FROM PL_KORBAN_KECELAKAAN NAMA "
				+ "LEFT JOIN (SELECT NO_LAPORAN_POLISI, KODE_INSTANSI, ID_KECELAKAAN, TGL_KEJADIAN, TGL_LAPORAN_POLISI, STATUS_LAPORAN_POLISI FROM PL_DATA_KECELAKAAN) A "
				+ "ON NAMA.ID_KECELAKAAN = A.ID_KECELAKAAN "
				+ "LEFT JOIN (SELECT KODE_INSTANSI, DESKRIPSI FROM PL_INSTANSI) INSTANSI "
				+ "ON A.KODE_INSTANSI = INSTANSI.KODE_INSTANSI "
				+ "LEFT JOIN (SELECT KODE_JAMINAN, DESKRIPSI, LINGKUP_JAMINAN FROM PL_JAMINAN) JAMINAN "
				+ "ON NAMA.KODE_JAMINAN = JAMINAN.KODE_JAMINAN) "
				+ "INSIDE "
				+ "WHERE "
				+ tglKejadian
				+ tglLaporan
				+ "(INSIDE.KODE_INSTANSI LIKE '"+instansi+"' OR INSIDE.INSTANSI LIKE '"+instansi+"') AND "
				+ "INSIDE.NO_LAPORAN_POLISI LIKE '"+noLaporan+"' AND "
				+ "INSIDE.NAMA LIKE '"+namaKorban+"' AND "
				+ "(INSIDE.KODE_JAMINAN LIKE '"+jenisPertanggungan+"' OR INSIDE.DESKRIP_JAMINAN LIKE '"+jenisPertanggungan+"') AND "
				+ "INSIDE.STATUS_LAPORAN_POLISI LIKE '"+statusLp+"' "
				+ menuPengajuan
				+ "AND INSIDE.ID_KORBAN_KECELAKAAN NOT IN (SELECT ID_KORBAN_KECELAKAAN FROM PL_PENGAJUAN_SANTUNAN WHERE ID_KORBAN_KECELAKAAN = INSIDE.ID_KORBAN_KECELAKAAN)"; 
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();
		return list;
	}

	@Override
	public List<Object[]> getValueLdbpListDisposisi(String levelKantor, String noBerkas) {
		String query = "SELECT * FROM "
				+ "(SELECT DISTINCT A.RV_LOW_VALUE, "
				+ "A.RV_MEANING, "
				+ "DISPOSISI.DISPOSISI, "
				+ "DISPOSISI.TGL_DISPOSISI, "
				+ "USR.USER_NAME, "
				+ "DISPOSISI.NO_BERKAS "
				+ "FROM DASI_JR_REF_CODES A "
				+ "LEFT JOIN(SELECT * FROM FND_KANTOR_JASARAHARJA ) KANTOR "
				+ "ON A.RV_HIGH_VALUE=KANTOR.LEVEL_KANTOR "
				+ "LEFT JOIN(SELECT * FROM PL_DISPOSISI) DISPOSISI "
				+ "ON A.RV_LOW_VALUE=DISPOSISI.DARI "
				+ "LEFT JOIN(SELECT * FROM AUTH_USER) USR "
				+ "ON USR.LOGIN = DISPOSISI.CREATED_BY "
				+ "WHERE A.RV_HIGH_VALUE LIKE '"+levelKantor+"' "
				+ "AND A.RV_DOMAIN = 'KODE DISPOSISI' )"
				+ "INSIDE "
				+ "WHERE INSIDE.NO_BERKAS like '"+noBerkas+"' "
				+ "ORDER BY INSIDE.TGL_DISPOSISI ASC ";
		
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();
		return list;

	}

	@Override
	public List<Object[]> getBankByKodeRs(String kodeRumahsakit, String search) {
		String sql="SELECT DISTINCT A.NAMA_BANK, A.KODE_BANK "
				+ "from FND_BANK A "
				+ "LEFT JOIN PL_REKENING_RS B "
				+ "ON A.KODE_BANK = B.KODE_BANK "
				+ "WHERE "
				+ "B.KODE_RUMAHSAKIT like '"+kodeRumahsakit+"' AND "
				+ "(UPPER(A.NAMA_BANK) LIKE UPPER('"+search+"') OR "
				+ "UPPER(A.KODE_BANK) LIKE UPPER('"+search+"'))";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(sql).getResultList();
		return list;

	}
}