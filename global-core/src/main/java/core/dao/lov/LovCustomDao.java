package core.dao.lov;

import java.util.List;

import core.model.PlDisposisi;

public interface LovCustomDao {
	
	List<Object[]> listLokasi(String search);
	public List<Object[]> getAllRSRegister(String search);
	
	//added by Meilona
	public List<Object[]> listDisposisi(String noBerkas);
	public List<Object[]> getKodeDisposisi(String statusProses, String levelKantor);
	public List<Object[]> getMetodePembayaran(String noBerkas);
	
	//added by luthfi
	// get kantor dilimpahkan 
	public List<Object[]> getKantorDilimpahkan(String idKorbanKecelakaan);
	
	//delete disposisi
	public int deleteDisposisi(PlDisposisi plDisposisi);
	
	//added by Luthfi
	//cetak ldbp
	public List<Object[]> getValueLdbp(String noBerkas);
	public List<Object[]> getValueLdbpListDisposisi(String levelKantor, String noBerkas);
	
	//cari data laka
	public List<Object[]> getListCariDataLaka(String kejadianStartDate,
			String kejadianEndDate,
			String laporanStartDate,
			String laporanEndDate,
			String instansi,
			String noLaporan,
			String namaKorban,
			String jenisPertanggungan,
			String statusLp, 
			String menu);
	
	//
	public List<String> getTahunPenerimaanFromPengajuanSantunan();
	public List<String> getTahunPengajuanFromPengajuanSantunan();
	public List<String> getTahunPenyelesaianFromPengajuanSantunan();
	
	public List<Object[]> getBankByKodeRs(String kodeRumahsakit, String search);
}
