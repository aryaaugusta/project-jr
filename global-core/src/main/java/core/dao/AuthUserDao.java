package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import core.model.AuthUser;

public interface AuthUserDao extends JpaRepository <AuthUser, String>{
	

}
