package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlRegisterSementara;
import core.model.PlTindakLanjut;

public interface PlRegisterSementaraDao extends JpaRepository<PlRegisterSementara, String>, PlRegisterSementaraCustomDao{

//	@Query("select a.kodeKantorJr,a.noRegister,e.tglKejadian,a.tglRegister,d.nama,"
//			+ "g.rvHighValue,a.namaPemohon,h.rvMeaning,b.tglTindakLanjut "
//			+ "from PlRegisterSementara a,PlTindakLanjut b,FndKantorJasaraharja c,PlKorbanKecelakaan d, "
//			+ "PlDataKecelakaan e,PlInstansi f,DasiJrRefCode g,DasiJrRefCode h "
//			+ "where a.noRegister = b.noRegister "
//			+ "and a.kodeKantorJr = c.kodeKantorJr "
//			+ "and a.idKorbanKecelakaan = d.idKorbanKecelakaan "
//			+ "and d.idKecelakaan = e.idKecelakaan "
//			+ "and e.kodeInstansi = f.kodeInstansi "
//			+ "and d.kodeSifatCidera = g.rvLowValue "
//			+ "and b.tindakLanjutFlag = h.rvLowValue "
//			+ "and a.kodeKantorJr like :kodeKantorJr "
//			+ "and (a.tglRegister between :regStartDate and :regEndDate) "
//			+ "and d.nama like :namaKorban "
//			+ "and (e.tglKejadian between :lakaStartDate and :lakaEndDate) "
//			+ "and e.noLaporanPolisi like :noLaporan "
//			+ "and a.noRegister like :noRegister "
//			+ "and a.asalBerkasFlag like :asalBerkasFlag "
//			+ "and b.tindakLanjutFlag like :tindakLanjutFlag "
//			+ "and a.cideraKorban like :cideraKorban "
//			+ "and f.deskripsi like :namaInstansi "
//			+ "and g.rvDomain like :rvDomainCidera "
//			+ "and h.rvDomain like :rvDomainTindakLanjut")
//	public List<Object[]> getListData(
//			@Param("kodeKantorJr")String kodeKantorJr,
//			@Param("regStartDate")Date regStartDate,
//			@Param("regEndDate")Date regEndDate,
//			@Param("namaKorban")String namaKorban,
//			@Param("lakaStartDate")Date lakaStartDate,
//			@Param("lakaEndDate")Date lakaEndDate,
//			@Param("noLaporan")String noLaporan,
//			@Param("noRegister")String noRegister,
//			@Param("asalBerkasFlag")String asalBerkasFlag,
//			@Param("tindakLanjutFlag")String tindakLanjutFlag,
//			@Param("cideraKorban")String cideraKorban,
//			@Param("namaInstansi")String namaInstansi,
//			@Param("rvDomainCidera")String rvDomainCidera,
//			@Param("rvDomainTindakLanjut")String rvDomainTindakLanjut);
	
	@Query("select a from PlRegisterSementara a where a.noRegister like :noReg")
	public List<PlRegisterSementara> findByKode(@Param("noReg")String noReg);
	
	@Query("select a from PlRegisterSementara a where a.idKorbanKecelakaan like :idKecelakaan")
	public List<PlRegisterSementara> findRegisterByIdKorban(@Param("idKecelakaan")String idKecelakaan);

	@Query("select a from PlTindakLanjut a where a.noRegister like :noReg")
	public List<PlTindakLanjut> findTlByNoRegister(@Param("noReg")String noReg);	
	
	@Query(value = "SELECT "
			+ "A.NO_REGISTER, "
			+ "A.TGL_REGISTER, "
			+ "B.NAMA, "
			+ "F.RV_HIGH_VALUE AS CIDERA, "
			+ "H.RV_MEANING AS ASAL_BERKAS, "
			+ "A.NAMA_PEMOHON, "
			+ "G.RV_MEANING AS HUB, "
			+ "C.TGL_KEJADIAN, "
			+ "C.TGL_LAPORAN_POLISI, "
			+ "C.NO_LAPORAN_POLISI, "
			+ "E.DESKRIPSI, "
			+ "I.RV_MEANING AS KASUS, "
			+ "J.RV_MEANING AS STATUS_KORBAN, "
			+ "D.NO_POLISI, "
			+ "C.DESKRIPSI_LOKASI "
			+ "FROM PL_REGISTER_SEMENTARA A "
			+ "LEFT JOIN PL_KORBAN_KECELAKAAN B ON A.ID_KORBAN_KECELAKAAN = B.ID_KORBAN_KECELAKAAN "
			+ "LEFT JOIN PL_DATA_KECELAKAAN C ON B.ID_KECELAKAAN = C.ID_KECELAKAAN "
			+ "LEFT JOIN PL_ANGKUTAN_KECELAKAAN D ON B.ID_ANGKUTAN_PENANGGUNG = D.ID_ANGKUTAN_KECELAKAAN "
			+ "LEFT JOIN PL_INSTANSI E ON C.KODE_INSTANSI = E.KODE_INSTANSI "
			+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA') F ON F.RV_LOW_VALUE = A.CIDERA_KORBAN "
			+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE HUBUNGAN KORBAN') G ON G.RV_LOW_VALUE = A.KODE_HUBUNGAN_KORBAN "
			+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'ASAL BERKAS REGISTER') H ON H.RV_LOW_VALUE = A.ASAL_BERKAS_FLAG "
			+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'PL KASUS KECELAKAAN') I ON I.RV_LOW_VALUE = C.KODE_KASUS_KECELAKAAN "
			+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS KORBAN') J ON J.RV_LOW_VALUE = B.KODE_STATUS_KORBAN "
			+ "WHERE A.NO_REGISTER LIKE :noRegister ",nativeQuery = true)
	public List<Object[]> dataCetakTl1(@Param("noRegister")String noRegister);
}
