package core.dao;

import java.util.List;

public interface PlPeriodeLockCustomDao {
	
	public List<Object[]> getDataList(String bulan, String tahun, String kodeKantor);

}
