package core.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlPenyelesaianSantunan;
import core.model.PlRekeningR;
import core.model.PlRekeningRPK;

public interface PlRekeningRDao extends JpaRepository<PlRekeningR, PlRekeningRPK> {

	@Query("select a, b.namaBank from PlRekeningR a, FndBank b where "
			+ "a.kodeRumahsakit like :kodeRumahsakit and "
			+ "a.kodeBank = b.kodeBank and a.flagEnable = 'Y'")
	public Page<Object[]> getRekeningByKodeRumahSakit(
			@Param("kodeRumahsakit")String kodeRumahsakit,
			Pageable page);
	
	@Query("select a, b.namaBank from PlRekeningR a, FndBank b where "
			+ "a.kodeRumahsakit like :kodeRumahsakit and "
			+ "a.kodeBank = b.kodeBank and a.flagEnable = 'Y'")
	public List<Object[]> getRekeningByKodeRumahSakit(
			@Param("kodeRumahsakit")String kodeRumahsakit);
	
	@Query("select a from PlRekeningR a where "
			+ "a.kodeRumahsakit like :kodeRumahsakit and "
			+ "a.kodeBank like :kodeBank")
	public List<PlRekeningR> getListRekeningByKodeRumahSakit(
			@Param("kodeRumahsakit")String kodeRumahsakit,
			@Param("kodeBank")String kodeBank);
	
	@Query("select a from PlRekeningR a where a.noRekening = :noRek")
	public List<PlRekeningR> getRekeningByNoRek(@Param("noRek") String noRek);
	
	@Query("select a from PlPenyelesaianSantunan a where a.noRekening = :norek")
	public List<PlPenyelesaianSantunan> getPenyFromRek(@Param("norek")String noRek);
	
	@Query("select a from PlPenyelesaianSantunan a where replace(a.noRekening,'-') = :norek and a.idGuid = :kodeBank")
	public List<PlPenyelesaianSantunan> getPenyFromRekDanBank(@Param("norek")String noRek, @Param("kodeBank") String kodeBank);
	
	@Query("select a , b.deskripsi , c.noRekening , c.namaPemilik "
			+ "from FndBank a, PlRumahSakit b, PlRekeningR c "
			+ "where a.kodeBank = c.kodeBank and "
			+ "b.kodeRumahsakit = c.kodeRumahsakit and "
			+ "a.kodeBank like :kodeBank and "
			+ "a.status ='Y' and "
			+ "(b.deskripsi like :search or "
			+ "c.noRekening like :search or "
			+ "c.namaPemilik like :search) ")
	public List<Object[]> getRekeningByKodeBank(
			@Param("kodeBank")String kodeBank, 
			@Param("search")String search);
	
	@Query("select a from PlRekeningR a where a.idRsRek = :idRsRek")
	public PlRekeningR getRekById(@Param("idRsRek")String idRsRek);

	
	@Modifying
	@Query("delete from PlRekeningR a where a.idRsRek like :idRsRek")
	public int deleteRek(@Param("idRsRek")String idRsRek);
	
}
