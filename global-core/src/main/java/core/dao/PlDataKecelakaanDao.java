package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.AuthUser;
import core.model.DasiJrRefCode;
import core.model.FndCamat;
import core.model.FndKantorJasaraharja;
import core.model.PlDataKecelakaan;

public interface PlDataKecelakaanDao extends JpaRepository<PlDataKecelakaan, String>, PlDataKecelakaanCustomDao {

	@Query("select a, "
			+ "b.nama as asal_berkas, "
			+ "c.deskripsi as instansi, "
			+ "d.deskripsi as lokasi, "
			+ "e.nama as samsat, "
			+ "f.rvMeaning as kasus_laka, "
			+ "g.rvMeaning as sifat_laka "
			+ "from PlDataKecelakaan a, "
			+ "FndKantorJasaraharja b, "
			+ "PlInstansi c, "
			+ "FndLokasi d, "
			+ "FndKantorJasaraharja e, "
			+ "DasiJrRefCode f, DasiJrRefCode g "
			+ "where "
			+ "a.asalBerkas = b.kodeKantorJr and "
			+ "a.kodeInstansi = c.kodeInstansi and "
			+ "a.kodeLokasi = d.kodeLokasi and "
			+ "a.kodeWilayah = e.kodeKantorJr and "
			+ "a.kodeKasusKecelakaan = f.rvLowValue and "
			+ "a.sifatKecelakaan = g.rvLowValue and "
			+ "a.idKecelakaan = :idKecelakaan")
	public List<Object[]> getDataKecelakaanById(@Param("idKecelakaan")String idKecelakaan);
	
	@Query("select a from PlDataKecelakaan a where "
			+ "a.idKecelakaan like :idLaka ")
	public List<PlDataKecelakaan> getDataLaka(@Param("idLaka")String idLaka);
	
	@Query("select a from DasiJrRefCode a where a.rvLowValue = :kode and a.flagEnable = 'Y' and a.rvDomain = :domain")
	public List<DasiJrRefCode> getRvMeaningByRvLowValue(@Param("kode")String code, @Param("domain")String domain);
	
	@Query("select a from FndCamat a, PlMappingCamat b where a.kodeCamat = b.kodeCamat and b.kodeLokasi = :kodeLokasi")
	public List<FndCamat> getCamatByLoc(@Param("kodeLokasi")String kodeLokasi);
	
	@Query("select a from FndKantorJasaraharja a where a.kodeKantorJr = :kodeKantor")
	public List<FndKantorJasaraharja> getKantorFromLokasi(@Param("kodeKantor")String kodeKantor);
	
	/*
	 * select * from auth_user where attribute1 like 
	 * '0200%' and upper(description) like upper('kepala cabang%')
	 */
	
	@Query("select a from AuthUser a where a.attribute1 like :kantor and upper(a.description) like upper('kepala cabang%')")
	public List<AuthUser> getKepalaCabang(@Param("kantor")String kantor);
	
	@Query("select a.idAngkutanKecelakaan, b.idAngkutanKecelakaan from PlAngkutanKecelakaan a, PlKorbanKecelakaan b "
			+ "where (a.idAngkutanKecelakaan = b.idAngkutanKecelakaan or "
			+ "a.idAngkutanKecelakaan = b.idAngkutanPenanggung) and "
			+ "a.idAngkutanKecelakaan like :idAngkutanKecelakaan ")
	public List<Object[]> getAngkutanByKorban(@Param("idAngkutanKecelakaan")String idAngkutanKecelakaan);
		
}
