package core.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.FndCamat;
import core.model.FndCamatPK;
import core.model.FndKantorJasaraharja;
import core.model.PlMappingCamat;

public interface FndCamatDao extends JpaRepository<FndCamat, FndCamatPK>, FndCamatCustomDao {

	@Query("select a from FndCamat a where "
			+ "(a.kodeProvinsi like UPPER(:kodeNama) or "
			+ "a.namaProvinsi like UPPER(:kodeNama)) and "
			+ "(a.kodeProvinsi like :search or "
			+ "a.namaProvinsi like :search or "
			+ "a.kodeCamat like :search or "
			+ "a.namaCamat like :search or "
			+ "a.kodeKabkota like :search or "
			+ "a.namaKabkota like :search) and a.flagEnable = 'Y' ")
	public List<FndCamat> getDataList(
			@Param("kodeNama") String kodeNama,
			@Param("search") String search);

	@Query("select a from FndCamat a where "
			+ "a.kodeProvinsi like :search or "
			+ "a.namaProvinsi like :search ")
	public List<FndCamat> getDataListByProvinsi(@Param("search") String search);
	
    @Query("select distinct a.kodeKabkota , a.namaKabkota, a.kodeProvinsi, a.namaProvinsi from FndCamat a where"
    		+ " a.kodeProvinsi= :kodeProvinsi and a.flagEnable='Y'")
    public List<Object[]> findKodeKabKotaByProv(@Param("kodeProvinsi")String kodeProvinsi);
	
    @Query("select distinct a.kodeProvinsi, a.namaProvinsi from FndCamat a where a.kodeProvinsi like :search "
    		+ "order by a.kodeProvinsi asc")
	public List<Object[]> getProvList(@Param("search") String search);
	
	@Query("select distinct a.kodeCamat, a.namaCamat from FndCamat a where a.kodeCamat like :search or "
	 		+ "a.namaCamat like :search "
	 		+ "order by a.kodeCamat asc")
	public List<Object[]> getLokasi(@Param("search") String search);
	
	@Modifying
	@Query("update FndCamat a set a.namaProvinsi = :namaProv where a.kodeProvinsi = :kodeProv")
	public int updateProvName(@Param("namaProv")String namaProv, @Param("kodeProv") String kodeProv);
	
	@Modifying
	@Query("update FndCamat a set a.namaKabkota = :namaKabkota where a.kodeKabkota = :kodeKabkota")
	public int updateKabKotaName(@Param("namaKabkota")String namaKabkota, @Param("kodeKabkota") String kodeKabkota);

	@Query("select a from PlMappingCamat a where a.kodeCamat = :kodeCamat")
	public List<PlMappingCamat> getMappingCamatByKodeCamat(@Param("kodeCamat")String kodeCamat);
	
	@Query("select a from FndCamat a where a.kodeCamat = :kodeCamat")
	public List<FndCamat> getCamatFromKode(@Param("kodeCamat")String kodeCamat);
	
//	@Modifying
//	@Query("insert into FndCamat a set a = :camat")
//	public int insertTOCamat(@Param("camat")FndCamat camat);
	
	@Query("select a from FndKantorJasaraharja a where a.flagEnable = 'Y'")
	public Page<FndKantorJasaraharja> getEnabledKantor(Pageable pageable);
	
	@Query("select a from FndKantorJasaraharja a where a.flagEnable = 'Y' and "
			+ "(upper( a.kodeKantorJr || ' - ' || a.nama) like upper(:search) )")
	public Page<FndKantorJasaraharja> searchEnabledKantor(@Param("search") String search, Pageable pageable);
	
	
}
