package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import core.model.PlBerkasEcmsContain;
import core.model.PlBerkasEcmsContainPK;



public interface PlBerkasEcmsContainsDao extends JpaRepository<PlBerkasEcmsContain, PlBerkasEcmsContainPK>{
	

}
