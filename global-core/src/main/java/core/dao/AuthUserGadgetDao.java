package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.AuthUserGadget;

public interface AuthUserGadgetDao extends JpaRepository<AuthUserGadget, String>{
	
	@Query("select a from AuthUserGadget a where a.userLogin = :userLogin")
	public List<AuthUserGadget> getListUserGadgetByLogin(@Param("userLogin")String userLogin);

}
