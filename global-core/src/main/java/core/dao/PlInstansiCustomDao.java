package core.dao;

import java.util.List;

public interface PlInstansiCustomDao {
	public List<Object[]> getList(String kodeInstansi, String search);
}
