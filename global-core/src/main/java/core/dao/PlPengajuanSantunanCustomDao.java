package core.dao;

import java.util.List;

import core.model.FndCamat;
import core.model.PlPengajuanSantunan;

public interface PlPengajuanSantunanCustomDao {
	public List<Object[]> getListIndex(
			String diajukanDi, 
			String dilimpahkanKe, 
			String namaKorban, 
			String pengajuanDay, 
			String pengajuanMonth, 
			String pengajuanYear,  
			String lakaDay, 
			String lakaMonth, 
			String lakaYear, 
			String kodeInstansi,
			String noLaporan, 
			String noBerkas, 
			String statusProses, 
			String penyelesaian, 
			String namaPemohon, 
			String kodeRs, 
			String search);
	
	//added by luthfi
	public int savePengajuanBerikutnya(PlPengajuanSantunan plPengajuanSantunan);	
	
	public List<Object[]> findById2(String idKecelakaan);
	
	public List<Object> findMaxId(String noBerkas);

	List<Object[]> findByIdKorban(String idKorbanKecelakaan);
	
	public List<Object[]> findByNoPengajuan(String noPengajuan);
	
	public List<Object[]> findForPenyelesaianPengajuan(String tglPengajuan,
			String noBerkas, String search, String jenisPengajuan, String dateFlag);
	
	public List<Object[]> findForPenyelesaianPengajuanDetail(String tglPengajuan,
			String noBerkas, String search, String jenisPengajuan, String dateFlag);
	
	//added by luthfi
	public List<Object[]> getIndexOtorisasi(String pilihPengajuan,String tglPenerimaan, String noBerkas, String search);
	public List<Object[]> getIndexPengesahan(String pilihPengajuan,String tglPenerimaan, String noBerkas, String search);
	public List<Object[]> getIndexIdentifikasi(String pilihPengajuan,String tglPenerimaan, String noBerkas, String search);
	
	public int updatePengajuanSantunanOtorisasi(PlPengajuanSantunan plPengajuanSantunan);
	public int updateProsesSelanjutnya(PlPengajuanSantunan plPengajuanSantunan);
	
	//10-05-2019
	public List<Object[]> getIndexPenyelesaian(String pilihPengajuan,String tglPenerimaan, String noBerkas, String search);

	public List<Object[]> getIndexHapusPengajuan(String noBerkas);
	public int deletePengajuan(PlPengajuanSantunan plPengajuanSantunan);
	
	//added by Meilona
	public List<Object[]> getListVerifikasi(String pilihPengajuan,String tglPengajuan, String noBerkas,String search);
	public List<Object[]> getDataPengajuanSantunanByNoBerkas(String noBerkas);

	//Absah
	public List<Object[]> getListAbsah(String noBerkas, String tglPenerimaan, String statusProses, String filter);
	
	
	//dokumen penyelesaian
	public List<Object[]> cetakKuitansi(String noBerkas);
	

}
