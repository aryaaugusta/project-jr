package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlPenyelesaianR;

public interface PlPenyelesaianRSDao  extends JpaRepository<PlPenyelesaianR, String>{
	
	@Query("select a from PlPenyelesaianR a where a.idPenyelesaianRs = :idPenyelesaianRs")
	public PlPenyelesaianR getOnebyId (@Param("idPenyelesaianRs")String idPenyelesaianRs);
}
