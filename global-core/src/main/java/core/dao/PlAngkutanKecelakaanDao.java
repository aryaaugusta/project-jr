package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlAngkutanKecelakaan;

public interface PlAngkutanKecelakaanDao extends JpaRepository<PlAngkutanKecelakaan, String>{
	
//	@Query("select a from PlAngkutanKecelakaan a, PlDataKecelakaan b where "
//			+ "a.idKecelakaan = b.idKecelakaan and a.idKecelakaan = :idKecelakaan")
//	public List<PlAngkutanKecelakaan> getAngkutanByIdLaka(@Param("idKecelakaan")String idKecelakaan);

	@Query("select a, b.rvMeaning from PlAngkutanKecelakaan a, DasiJrRefCode b where "
			+ "a.idKecelakaan = :idKec and a.statusKendaraan = b.rvLowValue and b.flagEnable = 'Y' and b.rvDomain = 'KODE STATUS KENDARAAN'")
	public List<Object[]> getKendaraanByLaka(@Param("idKec")String idKec);
}
