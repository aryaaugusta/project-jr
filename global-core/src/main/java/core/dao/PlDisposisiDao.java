package core.dao;

import java.util.List;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlDisposisi;

public interface PlDisposisiDao extends JpaRepository<PlDisposisi, String> {
	@Query("select a from PlDisposisi a where a.noBerkas like :noBerkas")
	public List<PlDisposisi> getListbyId(@Param("noBerkas") String noBerkas);

}
