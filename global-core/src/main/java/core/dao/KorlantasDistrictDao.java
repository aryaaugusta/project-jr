package core.dao;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.KorlantasDistrict;

public interface KorlantasDistrictDao extends JpaRepository<KorlantasDistrict, String>{

	@Query("select k from KorlantasDistrict k where k.provinceId = :provinceId")
	public List<KorlantasDistrict> findKorlantasDistrictByProvinceId(
			@Param("provinceId") BigDecimal provinceId);
	
	@Query("select k from KorlantasDistrict k where k.id = :districNumber")
	public List<KorlantasDistrict> findKorlantasDistrictByDistricNumber(
			@Param("districNumber") BigDecimal districtNumber);
}
