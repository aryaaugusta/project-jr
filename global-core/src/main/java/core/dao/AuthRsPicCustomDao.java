package core.dao;

import java.util.List;

import core.model.AuthRsPic;
import core.model.AuthUser;


public interface AuthRsPicCustomDao {

	public List<Object[]> getDataList(String search);
	
//	public List<Object[]> getRsByUserLogin(String userLogin);
	
	public List<Object[]> getRsNotIn();
	public int saveRsPic(AuthRsPic rsPic);
	public int updateAuthUser(AuthUser authUser);
	public int updateRsPic(AuthRsPic rsPic);
	public int deleteRsPic(AuthRsPic rsPic);
}
