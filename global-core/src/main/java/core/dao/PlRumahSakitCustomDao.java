package core.dao;

import java.util.List;


public interface PlRumahSakitCustomDao {

	public List<Object[]> getDataRS(
			String kanWil,
			String kodeNama,
			String search);
}
