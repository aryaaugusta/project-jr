package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import core.model.KorlantasAccident;

public interface KorlantasAccidentDao extends JpaRepository<KorlantasAccident, String> {

}
