	package core.dao;

import java.util.List;

import core.model.PlRegisterSementara;

public interface PlRegisterSementaraCustomDao {

	public List<Object[]> getListData(String kodeKantorJr,String regDay,String regMonth,
			String regYear,String namaKorban,String lakaDay, String lakaMonth, String lakaYear,
			String noLaporan,String noRegister,String asalBerkasFlag,String tindakLanjutFlag,
			String cideraKorban,String namaInstansi,String rvDomainCidera,String rvDomainTindakLanjut,String search);
	
	public List<Object[]> findOneByNoRegister(String noRegister);
	
	public int deleteRegister(PlRegisterSementara noRegister);
	
	public List<Object[]> findMonitoringGLRS(String kodeKantor,String jenisTgl,String tglAwal,String tglAkhir,String kodeRS,String flagBayar);
	public List<Object[]> getMonitoringGLRS(String kodeKantor,String jenisTgl,String tglAwal,String tglAkhir,String kodeRS,String flagBayar);
	
	public List<Object[]> getListIndex(String kodeKantorJr,String regDay,String regMonth,
			String regYear,String namaKorban,String lakaDay, String lakaMonth, String lakaYear,
			String noLaporan,String noRegister,String asalBerkasFlag,String tindakLanjutFlag,
			String cideraKorban,String kodeInstansi);

}
