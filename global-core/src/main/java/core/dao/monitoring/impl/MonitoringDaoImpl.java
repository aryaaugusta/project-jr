package core.dao.monitoring.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.monitoring.MonitoringCustomDao;

public class MonitoringDaoImpl implements MonitoringCustomDao {
	
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Object[]> getListMonitoring(String noBerkas, String namaKorban,
			String tglKejadian, String tglPenerimaan, String diajukanDi,
			String dilimpahkanKe, String search) {
		String tgl = "";
		if(tglPenerimaan!=null && !tglPenerimaan.equalsIgnoreCase("%")){
			tgl="AND INSIDE.TGL_PENGAJUAN LIKE '" + tglPenerimaan+" ";			
		}
		
		String tglKej ="";
		if(tglKejadian!=null && !tglKejadian.equalsIgnoreCase("%")){
			tglKej="AND INSIDE.TGL_KEJADIAN LIKE '" + tglKejadian+"'";			
		}
		
		String kantor = "AND ";
		if(diajukanDi.equalsIgnoreCase(dilimpahkanKe)){
			kantor = "OR ";
		}else{
			kantor = "AND ";
		}
		
		String query="SELECT * FROM "
				+ "( SELECT DISTINCT S.NO_BERKAS, "
				+ "NVL(PS.ID_KORBAN_KECELAKAAN, "
				+ "RG.ID_KORBAN_KECELAKAAN) AS ID_KORBAN, "
				+ "S.DIAJUKAN_DI, REPLACE(K.NAMA, 'LOKET ', '') AS KANTOR_DIAJUKAN, "
				+ "S.DILIMPAHKAN_KE, "
				+ "REPLACE(KD.NAMA, 'LOKET ', '') AS KANTOR_LIMPAHAN, "
				+ "working_days(S.TGL_PENGAJUAN, SYSDATE) AS JEDA, "
				+ "S.NAMA_KORBAN, "
				+ "TO_CHAR(S.TGL_PENGAJUAN, 'DD/MM/YYYY') AS TGL_PENGAJUAN, "
				+ "TO_CHAR(S.TGL_KEJADIAN,  'DD/MM/YYYY') AS TGL_KEJADIAN "
				+ "FROM PL_V_SANTUNAN_SUM S "
				+ "JOIN FND_KANTOR_JASARAHARJA K "
				+ "ON K.KODE_KANTOR_JR = S.DIAJUKAN_DI "
				+ "JOIN FND_KANTOR_JASARAHARJA KD "
				+ "ON KD.KODE_KANTOR_JR = S.DILIMPAHKAN_KE "
				+ "LEFT JOIN PL_PENGAJUAN_SANTUNAN PS "
				+ "ON PS.ID_KORBAN_KECELAKAAN = S.ID_KORBAN_KECELAKAAN AND "
				+ "PS.DIAJUKAN_DI = S.DILIMPAHKAN_KE "
				+ "LEFT JOIN PL_REGISTER_SEMENTARA RG "
				+ "ON RG.ID_KORBAN_KECELAKAAN = S.ID_KORBAN_KECELAKAAN AND "
				+ "RG.KODE_KANTOR_JR = S.DILIMPAHKAN_KE "
				+ "WHERE S.OTORISASI_FLAG = '3') INSIDE "
				+ "WHERE INSIDE.ID_KORBAN IS NULL AND " 
				+ "INSIDE.NO_BERKAS LIKE '"+noBerkas+"' AND " 
				+ "INSIDE.NAMA_KORBAN LIKE '"+namaKorban+"' AND "
				+ "INSIDE.DIAJUKAN_DI LIKE '"+diajukanDi+"' "
				+ kantor
				+ "INSIDE.DILIMPAHKAN_KE LIKE '"+dilimpahkanKe+"' "
				+ tglKej
				+ tgl
				+ "AND (INSIDE.NO_BERKAS LIKE '"+search+"' OR "
				+ "INSIDE.TGL_KEJADIAN LIKE  '"+search+"' OR "
				+ "INSIDE.TGL_PENGAJUAN LIKE '"+search+"' OR "
				+ "INSIDE.NAMA_KORBAN LIKE '"+search+"')";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	@Override
	public List<Object[]> getLakaBelumTuntas(String startDate, String endDate,
			String tipePeriode, String jenisLaporan, String search) {
		// TODO Auto-generated method stub
		String tgl="";
	
		if(tipePeriode.contains("Entry")){
			tgl="AND (TRUNC(A.CREATION_DATE) >= TRUNC(TO_DATE('"+startDate+"', 'DD/MM/YYYY')) AND TRUNC(A.CREATION_DATE) <= TRUNC(TO_DATE('"+endDate+"', 'DD/MM/YYYY')))";
		}else if(tipePeriode.contains("Kecelakaan")){
			tgl="AND (TRUNC(A.TGL_KEJADIAN) >= TRUNC(TO_DATE('"+startDate+"', 'DD/MM/YYYY')) AND TRUNC(A.TGL_KEJADIAN) <= TRUNC(TO_DATE('"+endDate+"', 'DD/MM/YYYY')))";
		}
		
		String query="SELECT "
				+ "A.TGL_KEJADIAN, "
				+ "K.NAMA, "
				+ "CC.RV_MEANING AS CIDERA, "
				+ "I.DESKRIPSI, "
				+ "C.RV_MEANING AS KASUS_KECELAKAAN, "
				+ "TL.TGL_TINDAK_LANJUT, "
				+ "CCC.RV_MEANING AS JENIS_TL, "
				+ "A.CREATION_DATE, "
				+ "A.KODE_KANTOR_JR "
				+ "FROM PL_KORBAN_KECELAKAAN K "
				+ "JOIN PL_DATA_KECELAKAAN A "
				+ "ON K.ID_KECELAKAAN = A.ID_KECELAKAAN "
				+ "JOIN FND_KANTOR_JASARAHARJA KJ "
				+ "ON KJ.KODE_KANTOR_JR = A.KODE_KANTOR_JR "
				+ "JOIN FND_LOKASI LOK "
				+ "ON LOK.KODE_LOKASI = A.KODE_LOKASI "
				+ "JOIN PL_INSTANSI I "
				+ "ON I.KODE_INSTANSI = A.KODE_INSTANSI "
				+ "LEFT JOIN PL_JAMINAN JM "
				+ "ON JM.KODE_JAMINAN = K.KODE_JAMINAN "
				+ "LEFT JOIN DASI_JR_REF_CODES C "
				+ "ON C.RV_LOW_VALUE = A.KODE_KASUS_KECELAKAAN AND "
				+ "C.RV_DOMAIN = 'PL KASUS KECELAKAAN' "
				+ "LEFT JOIN DASI_JR_REF_CODES CC "
				+ "ON CC.RV_LOW_VALUE = K.KODE_SIFAT_CIDERA AND "
				+ "CC.RV_DOMAIN = 'KODE SIFAT CIDERA' "
				+ "LEFT JOIN PL_REGISTER_SEMENTARA RG "
				+ "ON RG.ID_KORBAN_KECELAKAAN = K.ID_KORBAN_KECELAKAAN AND "
				+ "SUBSTR(RG.KODE_KANTOR_JR,1,5) = SUBSTR(A.KODE_KANTOR_JR,1,5) "
				+ "JOIN PL_TINDAK_LANJUT TL "
				+ "ON RG.NO_REGISTER = TL.NO_REGISTER AND "
				+ "TL.TINDAK_LANJUT_FLAG NOT IN ('11','13') AND "
				+ "TL.CREATED_DATE = (SELECT MAX(CREATED_DATE) "
				+ "FROM PL_TINDAK_LANJUT WHERE TL.NO_REGISTER = NO_REGISTER) "
				+ "LEFT JOIN PL_PENGAJUAN_SANTUNAN PS "
				+ "ON K.ID_KORBAN_KECELAKAAN = PS.ID_KORBAN_KECELAKAAN "
				+ "AND PS.OTORISASI_FLAG NOT IN ('1','2','3') "
				+ "LEFT JOIN DASI_JR_REF_CODES CCC "
				+ "ON CCC.RV_LOW_VALUE = TL.TINDAK_LANJUT_FLAG AND "
				+ "CCC.RV_DOMAIN = 'TINDAK LANJUT REGISTER' "
				+ "WHERE "
				+ "A.KODE_KANTOR_JR LIKE '"+jenisLaporan+"' "
				+ tgl;
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}
	
	@Override
	public List<Object[]> getListDataLakaIrsms(String instansi, String kejadianStartDate, String kejadianEndDate, 
			String laporanStartDate,String laporanEndDate, String namaKorban, String noLaporan, String jenisData) {
		
		String tglLaporan="";
		if(laporanStartDate!=null&&laporanEndDate!=null){
			tglLaporan="AND (TRUNC(INSIDE.TGL_LAPORAN_POLISI) >= TRUNC(TO_DATE('"+laporanStartDate+"', 'DD/MM/YYYY')) AND TRUNC(INSIDE.TGL_LAPORAN_POLISI) <= TRUNC(TO_DATE('"+laporanEndDate+"', 'DD/MM/YYYY'))) ";
		}
		
		String tglKejadian="";
		if(kejadianStartDate!=null&&laporanEndDate!=null){
			tglKejadian="AND (TRUNC(INSIDE.TGL_KEJADIAN) >= TRUNC(TO_DATE('"+kejadianEndDate+"', 'DD/MM/YYYY')) AND TRUNC(INSIDE.TGL_KEJADIAN) <= TRUNC(TO_DATE('"+kejadianEndDate+"', 'DD/MM/YYYY'))) ";
		}
		
		String map="";
		System.err.println("lona "+jenisData);
		if(jenisData.contains("Sudah")){
			map="AND INSIDE.ALREADY_TL = 'Y'";
		}else if(jenisData.contains("Belum")){
			map="AND INSIDE.ALREADY_TL = 'N'";
		}
		
		String query="SELECT * FROM "
				+ "(SELECT "
				+ "A.ID AS ID_KECELAKAAN, "
				+ "MP.DESKRIPSI, "
				+ "TO_DATE(TO_CHAR(A.ACCIDENT_DATE,'DD/MM/YYYY HH24:MI'),'DD/MM/YYYY HH24:MI') AS TGL_KEJADIAN, "
				+ "TO_DATE(TO_CHAR(A.LP_DATE, 'DD/MM/YYYY HH24:MI'), 'DD/MM/YYYY HH24:MI') AS TGL_LAPORAN_POLISI, "
				+ "A.ACCIDENT_NO AS NO_LAPORAN, "
				+ "(A.ROAD_NAME || ' ' || A.REFERENCE_DESC) AS DESKRIPSI_LOKASI, "
				+ "(V.FIRST_NAME || ' ' || V.LAST_NAME) AS KORBAN, "
				+ "DECODE(ML.ID_JR, NULL, 'N', 'Y') AS ALREADY_TL, "
				+ "A.POLICE_DETAILS AS URAIAN_SINGKAT, "
				+ "I.KODE_INSTANSI, "
				+ "NVL(ML.ID_JR, '') AS ID_LAKA_JR, "
				+ "NVL(MPK.ID_JR, '') AS ID_KORBAN_JR, "
				+ "V.ID AS ID_KORBAN_KECELAKAAN "
				+ "FROM KORLANTAS_ACCIDENTS A "
				+ "JOIN PL_MAPPING_POLDA MP "
				+ "ON MP.ID_DISTRICTS NOT LIKE '%a' AND NVL(MP.ID_DISTRICTS, 0) = TO_CHAR(A.DISTRICT_ID) "
				+ "JOIN PL_INSTANSI I "
				+ "ON I.KODE_INSTANSI = MP.KODE_INSTANSI AND I.FLAG_ENABLE = 'Y' "
				+ "JOIN KORLANTAS_VEHICLES V "
				+ "ON V.ACCIDENT_ID = NVL(TO_CHAR(A.ID), '') AND ((V.INJURY_ID IN (1, 2, 3))) AND (V.STATE IN (1, 2) OR (V.STATE IS NULL)) "
				+ "LEFT JOIN PL_KORL_MAP_LAKA ML "
				+ "ON ML.ID_KORLANTAS = NVL(TO_CHAR(A.ID),'')"
				+ "LEFT JOIN PL_KORL_MAP_KORBAN MPK ON MPK.POS_KORBAN = 'DRIVER' AND MPK.ID_KORLANTAS = NVL(TO_CHAR(V.ID),'') ) "
				+ "INSIDE WHERE INSIDE.KODE_INSTANSI LIKE '"+instansi+"' "
				+ "AND INSIDE.KORBAN LIKE '"+namaKorban+"' "
				+ "AND INSIDE.NO_LAPORAN LIKE '"+noLaporan+"' "
				+ tglKejadian
				+ tglLaporan
				+ map;
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
		
	}

	@Override
	public List<Object[]> getLakaRs(String namaKantor, String kodeRs,
			String startDate,String endDate, String pilihSumberData, String namaKorban, String statusResponse,
			String search) {
		// TODO Auto-generated method stub
		String tgl="";
		if(startDate!=null&& endDate!=null){
			tgl="AND (TRUNC(INSIDE.TGL_MASUK_RS) >= TRUNC(TO_DATE('"+startDate+"', 'DD/MM/YYYY')) AND TRUNC(INSIDE.TGL_MASUK_RS) <= TRUNC(TO_DATE('"+endDate+"', 'DD/MM/YYYY')))";
		}
		String query=" SELECT * FROM "
				+ "(SELECT RS.DESKRIPSI, "
				+ "A.TGL_KEJADIAN, "
				+ "A.TGL_MASUK_RS, "
				+ "A.CREATED_DATE AS TGL_MASUK_SISTEM, "
				+ "rs_kcpt_due_date(SUBSTR(TO_CHAR(A.CREATED_DATE, 'DD/MM/YYYY HH24:MI:SS'), 1, 16)) AS BATAS_KUNJUNGAN , "
				+ "RS_KCPT_DUE_DATE_DAY(SUBSTR(TO_CHAR(A.CREATED_DATE, 'DD/MM/YYYY HH24:MI:SS'), 1, 16)) AS BATAS_KESIMPULAN, "
				+ "A.KODE_KEJADIAN, "
				+ "A.NAMA_KORBAN, "
				+ "STATUS.RV_MEANING AS RESPONSE, "
				+ "A.KODE_RUMAH_SAKIT, "
				+ "RESPONSE.KODE_RS, "
				+ "KANTOR.KODE_KANTOR_JR "
				+ "FROM PL_KORBAN_RS A "
				+ "JOIN ( SELECT KODE_KEJADIAN, KODE_RS, KODE_KESIMPULAN FROM PL_RS_RESPONSE ) RESPONSE "
				+ "ON A.KODE_KEJADIAN = RESPONSE.KODE_KEJADIAN "
				+ "JOIN ( SELECT KODE_RUMAHSAKIT, DESKRIPSI FROM PL_RUMAH_SAKIT ) RS "
				+ "ON A.KODE_RUMAH_SAKIT = RS.KODE_RUMAHSAKIT "
				+ "LEFT JOIN ( SELECT KODE_KEJADIAN, KODE_RS FROM PL_RS_KORBAN_MAP ) KORBAN_MAP "
				+ "ON A.KODE_KEJADIAN = KORBAN_MAP.KODE_KEJADIAN AND A.KODE_RUMAH_SAKIT = KORBAN_MAP.KODE_RS "
				+ "LEFT JOIN ( SELECT RV_DOMAIN, RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES "
				+ "WHERE RV_DOMAIN = 'FIRSTRESPONSE RS' AND RV_LOW_VALUE IN ( 'F0', 'F1', 'F3' ) OR "
				+ "RV_DOMAIN = 'FASTRESPONSE RS' AND RV_LOW_VALUE IN ( '1', '2', '3' ) ) STATUS "
				+ "ON RESPONSE.KODE_KESIMPULAN =STATUS.RV_LOW_VALUE "
				+ "LEFT JOIN ( SELECT DISTINCT KODE_RUMAHSAKIT, LOKET_PENANGGUNGJAWAB, NAMA_RS_BPJS FROM PL_RS_MAP_BPJS ) RS_BPJS "
				+ "ON A.KODE_RUMAH_SAKIT = RS_BPJS.KODE_RUMAHSAKIT "
				+ "LEFT JOIN ( SELECT KODE_KANTOR_JR, NAMA FROM FND_KANTOR_JASARAHARJA ) KANTOR "
				+ "ON RS_BPJS.LOKET_PENANGGUNGJAWAB = KANTOR.KODE_KANTOR_JR "
				+ "WHERE A.KODE_RUMAH_SAKIT LIKE '02%' ) INSIDE "
				+ "WHERE INSIDE.KODE_KANTOR_JR LIKE '"+namaKantor+"' AND "
				+ "INSIDE.KODE_RS LIKE '"+kodeRs+"' AND "
				+ "INSIDE.NAMA_KORBAN LIKE '"+namaKorban+"' AND "
				+ "INSIDE.RESPONSE LIKE '"+statusResponse+"'"
				+ tgl;

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
		
	}

	@Override
	public List<Object[]> getLakaRsById(String kodeRumahSakit, String kodeKejadian) {
		// TODO Auto-generated method stub
		
		String query="SELECT A.KODE_KEJADIAN, "
				+ "RS_BPJS.NAMA_RS_BPJS, "
				+ "A.TGL_MASUK_RS, "
				+ "A.RUANGAN, "
				+ "A.TGL_KEJADIAN, "
				+ "A.NAMA_KORBAN, "
				+ "A.ALAMAT, "
				+ "RS.PIC, "
				+ "A.CREATED_DATE, "
				+ "INSTANSI.DESKRIPSI AS NAMA_INSTANSI, "
				+ "RESPONSE.CREATED_DATE AS TGL_RESPONSE, "
				+ "RESPONSE.PETUGAS, "
				+ "KR.RV_MEANING AS RESPONSEDES "
				+ "FROM PL_KORBAN_RS A "
				+ "LEFT JOIN (SELECT KODE_RUMAHSAKIT, DESKRIPSI, PIC FROM PL_RUMAH_SAKIT) RS "
				+ "ON A.KODE_RUMAH_SAKIT = RS.KODE_RUMAHSAKIT "
				+ "LEFT JOIN ( SELECT DISTINCT KODE_RUMAHSAKIT, LOKET_PENANGGUNGJAWAB, NAMA_RS_BPJS FROM PL_RS_MAP_BPJS ) RS_BPJS "
				+ "ON A.KODE_RUMAH_SAKIT = RS_BPJS.KODE_RUMAHSAKIT "
				+ "LEFT JOIN (SELECT KODE_INSTANSI, DESKRIPSI FROM PL_INSTANSI) INSTANSI "
				+ "ON A.KODE_INSTANSI = INSTANSI.KODE_INSTANSI "
				+ "LEFT JOIN(SELECT KODE_KEJADIAN, KODE_RS, PETUGAS, KODE_KESIMPULAN, CREATED_DATE FROM PL_RS_RESPONSE) RESPONSE "
				+ "ON A.KODE_KEJADIAN = RESPONSE.KODE_KEJADIAN "
				+ "LEFT JOIN (SELECT RV_DOMAIN, RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'FIRSTRESPONSE RS' OR RV_DOMAIN ='FASTRESPONSE RS') KR "
				+ "ON RESPONSE.KODE_KESIMPULAN = KR.RV_LOW_VALUE "
				+ "WHERE A.KODE_RUMAH_SAKIT LIKE '"+kodeRumahSakit+"' AND "
				+ "A.KODE_KEJADIAN LIKE '"+kodeKejadian+"'";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
		
	}

	@Override
	public List<Object[]> findOneDataLakaIrsms(String idKejadian, String instansi) {
		String query="SELECT * FROM "
				+ "(SELECT "
				+ "A.ID AS ID_KECELAKAAN, "
				+ "MP.DESKRIPSI, "
				+ "TO_DATE(TO_CHAR(A.ACCIDENT_DATE,'DD/MM/YYYY HH24:MI'),'DD/MM/YYYY HH24:MI') AS TGL_KEJADIAN, "
				+ "TO_DATE(TO_CHAR(A.LP_DATE, 'DD/MM/YYYY HH24:MI'), 'DD/MM/YYYY HH24:MI') AS TGL_LAPORAN_POLISI, "
				+ "A.ACCIDENT_NO AS NO_LAPORAN, "
				+ "(A.ROAD_NAME || ' ' || A.REFERENCE_DESC) AS DESKRIPSI_LOKASI, "
				+ "(V.FIRST_NAME || ' ' || V.LAST_NAME) AS KORBAN, "
				+ "DECODE(ML.ID_JR, NULL, 'N', 'Y') AS ALREADY_TL, "
				+ "A.POLICE_DETAILS AS URAIAN_SINGKAT, "
				+ "I.KODE_INSTANSI, "
				+ "NVL(ML.ID_JR, '') AS ID_LAKA_JR, "
				+ "NVL(MPK.ID_JR, '') AS ID_KORBAN_JR, "
				+ "V.ID AS ID_KORBAN_KECELAKAAN "
				+ "FROM KORLANTAS_ACCIDENTS A "
				+ "JOIN PL_MAPPING_POLDA MP "
				+ "ON MP.ID_DISTRICTS NOT LIKE '%a' AND NVL(MP.ID_DISTRICTS, 0) = TO_CHAR(A.DISTRICT_ID) "
				+ "JOIN PL_INSTANSI I "
				+ "ON I.KODE_INSTANSI = MP.KODE_INSTANSI AND I.FLAG_ENABLE = 'Y' "
				+ "JOIN KORLANTAS_VEHICLES V "
				+ "ON V.ACCIDENT_ID = NVL(TO_CHAR(A.ID), '') AND ((V.INJURY_ID IN (1, 2, 3))) AND (V.STATE IN (1, 2) OR (V.STATE IS NULL)) "
				+ "LEFT JOIN PL_KORL_MAP_LAKA ML "
				+ "ON ML.ID_KORLANTAS = NVL(TO_CHAR(A.ID),'')"
				+ "LEFT JOIN PL_KORL_MAP_KORBAN MPK ON MPK.POS_KORBAN = 'DRIVER' AND MPK.ID_KORLANTAS = NVL(TO_CHAR(V.ID),'') ) "
				+ "INSIDE WHERE INSIDE.ID_KECELAKAAN LIKE '"+idKejadian+"' "
				+ "AND INSIDE.KODE_INSTANSI LIKE '"+instansi+"'";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	@Override
	public List<Object[]> getIndexCoklitDasiIrsms(String kodeInstansi,
			String startDate, String endDate) {

		String tgl="";
		if(startDate!=null&&endDate!=null){
			tgl="AND (TRUNC(INSIDE.TGL_KEJADIAN_DASI) >= TRUNC(TO_DATE('01/01/2016', 'DD/MM/YYYY')) AND TRUNC(INSIDE.TGL_KEJADIAN_DASI) <= TRUNC(TO_DATE('31/03/2019', 'DD/MM/YYYY')))";
		}
		
		String query="SELECT * FROM (SELECT "
				+ "A.TGL_KEJADIAN AS TGL_KEJADIAN_DASI, "
				+ "A.NO_LAPORAN_POLISI AS NO_LAPORAN_POLISI_DASI, "
				+ "A.TGL_LAPORAN_POLISI AS TGL_LAPORAN_POLISI_DASI, "
				+ "KORBAN.NAMA, "
				+ "CODE_CIDERA.RV_HIGH_VALUE, "
				+ "(irsms.TGL_KEJADIAN) AS TGL_KEJADIAN_IRSMS, "
				+ "(irsms.TGL_LAPORAN_POLISI) AS TGL_LAPORAN_POLISI_IRSMS, "
				+ "NVL(irsms.KORBAN,'-') AS KORBAN_IRSMS, "
				+ "NVL(irsms.KODE_SIFAT_CIDERA,'-') AS CIDERA_KORBAN_IRSMS, "
				+ "A.ID_KECELAKAAN, "
				+ "A.KODE_INSTANSI "
				+ "FROM PL_DATA_KECELAKAAN A "
				+ "LEFT JOIN (SELECT ID_KECELAKAAN, NAMA, KODE_SIFAT_CIDERA FROM PL_KORBAN_KECELAKAAN) KORBAN "
				+ "ON A.ID_KECELAKAAN = KORBAN.ID_KECELAKAAN "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_HIGH_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA') CODE_CIDERA "
				+ "ON KORBAN.KODE_SIFAT_CIDERA = CODE_CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT "
							+ "A.ID AS ID_KECELAKAAN, "
							+ "PJL.ID AS ID_KORBAN_KECELAKAAN, "
							+ "NVL (ML.ID_JR, '') AS ID_LAKA_JR, "
							+ "NVL (MPK.ID_JR, '') AS ID_KORBAN_JR, "
							+ "(SUBSTR (MP.KODE_INSTANSI, 1, 2) || '0' || SUBSTR (MP.KODE_INSTANSI, 3, 2) || '01') AS KODE_KANTOR_JR, "
							+ "TO_DATE (TO_CHAR (A.ACCIDENT_DATE, 'DD/MM/YYYY HH24:MI'), 'DD/MM/YYYY HH24:MI') AS TGL_KEJADIAN, "
							+ "KP.ID AS ID_POLDA, "
							+ "NVL (MP.KODE_INSTANSI, I.KODE_INSTANSI) AS KODE_INSTANSI, "
							+ "NVL (MP.DESKRIPSI, I.DESKRIPSI) AS NAMA_INSTANSI, "
							+ "(A.ROAD_NAME || ' ' || A.REFERENCE_DESC) AS DESKRIPSI_LOKASI, "
							+ "TO_DATE (TO_CHAR (A.LP_DATE, 'DD/MM/YYYY HH24:MI'), 'DD/MM/YYYY HH24:MI') AS TGL_LAPORAN_POLISI, "
							+ "A.ACCIDENT_NO AS NO_LAPORAN_POLISI, "
							+ "(O.FIRST_NAME || ' ' || O.LAST_NAME) AS NAMA_PETUGAS, "
							+ "'Y' AS STATUS_LAPORAN_POLISI, "
							+ "A.POLICE_DETAILS AS DESKRIPSI_KECELAKAAN, "
							+ "(SUBSTR (MP.KODE_INSTANSI, 1, 3) || '0' || SUBSTR (MP.KODE_INSTANSI, 4, 1) || '01') AS ASAL_BERKAS, "
							+ "DECODE (A.HIT_AND_RUN, NULL, 'NR', 'TL') AS SIFAT_KECELAKAAN, "
							+ "(PJL.FIRST_NAME || ' ' || PJL.LAST_NAME) AS KORBAN, "
							+ "DECODE (ML.ID_JR, NULL, 'N', 'Y') AS ALREADY_TL, "
							+ "DECODE (MPK.ID_JR,  NULL, 'N',  '-', 'N',  'Y') AS ALREADY_TL_KRB, "
							+ "'PJK' AS STATUS_KORBAN, "
							+ "KORBAN_JR.KODE_SIFAT_CIDERA "
							+ "FROM KORLANTAS_ACCIDENTS A "
							+ "JOIN PL_MAPPING_POLDA MP ON MP.ID_DISTRICTS NOT LIKE '%a' AND NVL (MP.ID_DISTRICTS, 0) = TO_CHAR (A.DISTRICT_ID) "
							+ "JOIN PL_INSTANSI I ON I.KODE_INSTANSI = MP.KODE_INSTANSI AND I.FLAG_ENABLE = 'Y' "
							+ "JOIN KORLANTAS_PEDESTRIANS PJL ON PJL.ACCIDENT_ID = A.ID AND PJL.INJURY_ID IN (1, 2, 3) AND (PJL.STATE IN (1, 2) OR (PJL.STATE IS NULL)) "
							+ "JOIN KORLANTAS_DISTRICTS KRD ON KRD.ID = A.DISTRICT_ID "
							+ "JOIN KORLANTAS_PROVINCES KP ON KP.ID = KRD.PROVINCE_ID "
							+ "LEFT JOIN PL_KORL_MAP_LAKA ML ON ML.ID_KORLANTAS = NVL (TO_CHAR (A.ID), '') "
							+ "LEFT JOIN PL_KORL_MAP_KORBAN MPK ON MPK.POS_KORBAN = 'PJK' AND MPK.ID_KORLANTAS = TO_CHAR (PJL.ID) "
							+ "LEFT JOIN KORLANTAS_OFFICERS O ON O.ID = NVL (A.OFFICER_ID, 0) "
							+ "LEFT JOIN PL_KORBAN_KECELAKAAN KORBAN_JR ON MPK.ID_JR = KORBAN_JR.ID_KORBAN_KECELAKAAN "
							+ "UNION ALL "
							+ "SELECT "
							+ "A.ID AS ID_KECELAKAAN, "
							+ "V.ID AS ID_KORBAN_KECELAKAAN, NVL (ML.ID_JR, '') AS ID_LAKA_JR, "
							+ "NVL (MPK.ID_JR, '') AS ID_KORBAN_JR, "
							+ "(SUBSTR (MP.KODE_INSTANSI, 1, 2) || '0' || SUBSTR (MP.KODE_INSTANSI, 3, 2) || '01') AS KODE_KANTOR_JR, "
							+ "TO_DATE (TO_CHAR (A.ACCIDENT_DATE, 'DD/MM/YYYY HH24:MI'), 'DD/MM/YYYY HH24:MI') AS TGL_KEJADIAN, "
							+ "KP.ID AS ID_POLDA, "
							+ "NVL (MP.KODE_INSTANSI, I.KODE_INSTANSI) AS KODE_INSTANSI, "
							+ "NVL (MP.DESKRIPSI, I.DESKRIPSI) AS NAMA_INSTANSI, "
							+ "(A.ROAD_NAME || ' ' || A.REFERENCE_DESC) AS DESKRIPSI_LOKASI, "
							+ "TO_DATE (TO_CHAR (A.LP_DATE, 'DD/MM/YYYY HH24:MI'), 'DD/MM/YYYY HH24:MI') AS TGL_LAPORAN_POLISI, "
							+ "A.ACCIDENT_NO AS NO_LAPORAN_POLISI, "
							+ "(O.FIRST_NAME || ' ' || O.LAST_NAME) AS NAMA_PETUGAS, "
							+ "'Y' AS STATUS_LAPORAN_POLISI, "
							+ "A.POLICE_DETAILS AS DESKRIPSI_KECELAKAAN, "
							+ "(SUBSTR (MP.KODE_INSTANSI, 1, 3) || '0' || SUBSTR (MP.KODE_INSTANSI, 4, 1) || '01') AS ASAL_BERKAS, "
							+ "DECODE (A.HIT_AND_RUN, NULL, 'NR', 'TL') AS SIFAT_KECELAKAAN, "
							+ "(V.FIRST_NAME || ' ' || V.LAST_NAME) AS KORBAN, "
							+ "DECODE (ML.ID_JR, NULL, 'N', 'Y') AS ALREADY_TL, "
							+ "DECODE (MPK.ID_JR,  NULL, 'N',  '-', 'N',  'Y') AS ALREADY_TL_KRB, "
							+ "'DRIVER' AS STATUS_KORBAN, "
							+ "KORBAN_JR.KODE_SIFAT_CIDERA "
							+ "FROM "
							+ "KORLANTAS_ACCIDENTS A "
							+ "JOIN KORLANTAS_DISTRICTS KRD ON KRD.ID = A.DISTRICT_ID "
							+ "JOIN KORLANTAS_PROVINCES KP ON KP.ID = KRD.PROVINCE_ID "
							+ "JOIN PL_MAPPING_POLDA MP ON MP.ID_DISTRICTS NOT LIKE '%a' AND NVL (MP.ID_DISTRICTS, 0) = TO_CHAR (A.DISTRICT_ID) "
							+ "JOIN PL_INSTANSI I ON I.KODE_INSTANSI = MP.KODE_INSTANSI AND I.FLAG_ENABLE = 'Y' "
							+ "JOIN KORLANTAS_VEHICLES V ON V.ACCIDENT_ID = NVL (TO_CHAR (A.ID), '') AND ( (V.INJURY_ID IN (1, 2, 3)) ) AND (V.STATE IN (1, 2) OR (V.STATE IS NULL)) "
							+ "LEFT JOIN PL_KORL_MAP_LAKA ML ON ML.ID_KORLANTAS = NVL (TO_CHAR (A.ID), '') "
							+ "LEFT JOIN PL_KORL_MAP_KORBAN MPK ON MPK.POS_KORBAN = 'DRIVER' AND MPK.ID_KORLANTAS = NVL (TO_CHAR (V.ID), '') "
							+ "LEFT JOIN KORLANTAS_OFFICERS O ON O.ID = NVL (A.OFFICER_ID, 0) "
							+ "LEFT JOIN PL_KORBAN_KECELAKAAN KORBAN_JR ON MPK.ID_JR = KORBAN_JR.ID_KORBAN_KECELAKAAN "
							+ "UNION ALL "
							+ "SELECT "
							+ "A.ID AS ID_KECELAKAAN, "
							+ "P.ID AS ID_KORBAN_KECELAKAAN, "
							+ "NVL (ML.ID_JR, '') AS ID_LAKA_JR, "
							+ "NVL (MPK.ID_JR, '') AS ID_KORBAN_JR, "
							+ "(SUBSTR (MP.KODE_INSTANSI, 1, 2) || '0' || SUBSTR (MP.KODE_INSTANSI, 3, 2) || '01') AS KODE_KANTOR_JR, "
							+ "TO_DATE (TO_CHAR (A.ACCIDENT_DATE, 'DD/MM/YYYY HH24:MI'), 'DD/MM/YYYY HH24:MI') AS TGL_KEJADIAN, "
							+ "KP.ID AS ID_POLDA, "
							+ "NVL (MP.KODE_INSTANSI, I.KODE_INSTANSI) AS KODE_INSTANSI, "
							+ "NVL (MP.DESKRIPSI, I.DESKRIPSI) AS NAMA_INSTANSI, "
							+ "(A.ROAD_NAME || ' ' || A.REFERENCE_DESC) AS DESKRIPSI_LOKASI, "
							+ "TO_DATE (TO_CHAR (A.LP_DATE, 'DD/MM/YYYY HH24:MI'), 'DD/MM/YYYY HH24:MI') AS TGL_LAPORAN_POLISI, "
							+ "A.ACCIDENT_NO AS NO_LAPORAN_POLISI, "
							+ "(O.FIRST_NAME || ' ' || O.LAST_NAME) AS NAMA_PETUGAS, "
							+ "'Y' AS STATUS_LAPORAN_POLISI, "
							+ "A.POLICE_DETAILS AS DESKRIPSI_KECELAKAAN, "
							+ "(SUBSTR (MP.KODE_INSTANSI, 1, 3) || '0' || SUBSTR (MP.KODE_INSTANSI, 4, 1) || '01') AS ASAL_BERKAS, "
							+ "DECODE (A.HIT_AND_RUN, NULL, 'NR', 'TL') AS SIFAT_KECELAKAAN, "
							+ "(P.FIRST_NAME || ' ' || P.LAST_NAME) AS KORBAN, "
							+ "DECODE (ML.ID_JR, NULL, 'N', 'Y') AS ALREADY_TL, "
							+ "DECODE (MPK.ID_JR,  NULL, 'N',  '-', 'N',  'Y') AS ALREADY_TL_KRB, "
							+ "'PNP' AS STATUS_KORBAN, "
							+ "KORBAN_JR.KODE_SIFAT_CIDERA "
							+ "FROM KORLANTAS_ACCIDENTS A "
							+ "JOIN KORLANTAS_DISTRICTS KRD ON KRD.ID = A.DISTRICT_ID "
							+ "JOIN KORLANTAS_PROVINCES KP ON KP.ID = KRD.PROVINCE_ID "
							+ "JOIN PL_MAPPING_POLDA MP ON MP.ID_DISTRICTS NOT LIKE '%a' AND NVL (MP.ID_DISTRICTS, 0) = TO_CHAR (A.DISTRICT_ID) "
							+ "JOIN PL_INSTANSI I ON I.KODE_INSTANSI = MP.KODE_INSTANSI AND I.FLAG_ENABLE = 'Y' "
							+ "JOIN KORLANTAS_VEHICLES V ON V.ACCIDENT_ID = A.ID "
							+ "JOIN KORLANTAS_PASSENGERS P ON P.VEHICLE_ID = V.ID AND ( (P.INJURY_ID IN (1, 2, 3)) ) AND (P.STATE IN (1, 2) OR (P.STATE IS NULL)) "
							+ "LEFT JOIN PL_KORL_MAP_LAKA ML ON ML.ID_KORLANTAS = NVL (TO_CHAR (A.ID), '') "
							+ "LEFT JOIN PL_KORL_MAP_KORBAN MPK ON MPK.POS_KORBAN = 'PNP' AND MPK.ID_KORLANTAS = NVL (TO_CHAR (P.ID), '') "
							+ "LEFT JOIN PL_KORBAN_KECELAKAAN KORBAN_JR ON MPK.ID_JR = KORBAN_JR.ID_KORBAN_KECELAKAAN "
							+ "LEFT JOIN KORLANTAS_OFFICERS O ON O.ID = NVL (A.OFFICER_ID, 0))irsms on A.ID_KECELAKAAN = irsms.ID_LAKA_JR) INSIDE "
							+ "WHERE "
							+ "INSIDE.KODE_INSTANSI = '"+kodeInstansi+"' "
							+ tgl; 
		
		@SuppressWarnings("unchecked") 
		List<Object[]> list = em.createNativeQuery(query).getResultList(); 
		return list; }
	
	

}
