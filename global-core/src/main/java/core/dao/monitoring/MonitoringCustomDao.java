package core.dao.monitoring;

import java.util.List;

public interface MonitoringCustomDao {
	//added by Meilona
		// for Monitoring Pelimpahan
		public List<Object[]> getListMonitoring(
				String noBerkas, 
				String namaKorban, 
				String tglKejadian, 
				String tglPenerimaan, 
				String diajukanDi, 
				String dilimpahkanKe, 
				String search);
		
		//index monitoring data laka irsms
		public List<Object[]> getListDataLakaIrsms(String instansi, String kejadianStartDate, String kejadianEndDate, 
				String laporanStartDate,String laporanEndDate, String namaKorban, String noLaporan, String jenisData);

		public List<Object[]> findOneDataLakaIrsms(String idKejadian, String instansi);

		public List<Object[]> getLakaBelumTuntas(
				String startDate,
				String endDate,
				String tipePeriode,
				String jenisLaporan,
				String search);
		
		public List<Object[]> getLakaRs(
				String namaKantor,
				String kodeRs,
				String startDate,
				String endDate,
				String pilihSumberData,
				String namaKorban,
				String statusResponse,
				String search
				);
		public List<Object[]> getLakaRsById(String kodeRumahSakit, String kodeKejadian);
		
		//added by luthfi
		public List<Object[]> getIndexCoklitDasiIrsms(String kodeInstansi, String startDate, String endDate);
		
}
