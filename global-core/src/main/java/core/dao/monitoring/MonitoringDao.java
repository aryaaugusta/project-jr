package core.dao.monitoring;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.KorlantasDistrict;
import core.model.KorlantasProvince;
import core.model.PlMappingPolda;
import core.model.PlPengajuanSantunan;

public interface MonitoringDao extends JpaRepository<PlPengajuanSantunan, String>, MonitoringCustomDao {
	
	//added by luthfi
	//find all polda untuk monitoring coklit data irsms
	@Query("select a from KorlantasProvince a "
			+ "where a.name like upper(:search) order by a.name asc")
	public List<KorlantasProvince>getListProvince(@Param("search")String search);
	
	//added by luthfi
	//find all polda untuk monitoring coklit data irsms
	@Query("select a from KorlantasDistrict a "
			+ "where a.provinceId like upper(:provinceId) and a.name like upper(:search) order by a.name asc")
	public List<KorlantasDistrict>getListDistrictByProvinceId(
			@Param("provinceId")String provinceId,
			@Param("search")String search);
	
	//get kode instansi by mapping polda
	@Query("select a from PlMappingPolda a where a.idDistricts like :idDistricts")
	public List<PlMappingPolda>getPoldaByDistrict(@Param("idDistricts")String idDistricts);

}
