package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import core.model.KorlantasProvince;

public interface KorlantasProvinceDao extends JpaRepository<KorlantasProvince, String>{

	@Query("select a from KorlantasProvince a")
	public List<KorlantasProvince> getKorlantasProvinces();
	
}
