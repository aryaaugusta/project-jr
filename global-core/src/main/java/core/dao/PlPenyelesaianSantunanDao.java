package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlPenyelesaianSantunan;

public interface PlPenyelesaianSantunanDao extends JpaRepository<PlPenyelesaianSantunan, String> {

	@Query("select a from PlPenyelesaianSantunan a where a.noBerkas = :noBerkas")
	public List<PlPenyelesaianSantunan> findOneByNoBerkas(@Param("noBerkas") String noBerkas);

	@Modifying(clearAutomatically = true)
	@Query("UPDATE PlPenyelesaianSantunan pl SET " + "pl.createdBy = :#{#santunan.createdBy}, "
			+ "pl.creationDate = :#{#santunan.creationDate}, " + "pl.idGuid = :#{#santunan.idGuid}, "
			+ "pl.jenisPembayaran = :#{#santunan.jenisPembayaran}, " + "pl.jmlByrAmbl = :#{#santunan.jmlByrAmbl}, "
			+ "pl.jmlByrP3k = :#{#santunan.jmlByrP3k}, "
			+ "pl.jumlahDibayarLukaluka = :#{#santunan.jumlahDibayarLukaluka}, "
			+ "pl.jumlahDibayarMeninggal = :#{#santunan.jumlahDibayarMeninggal}, "
			+ "pl.jumlahDibayarPenguburan = :#{#santunan.jumlahDibayarPenguburan}, "
			+ "pl.kelebihanBayar = :#{#santunan.kelebihanBayar}, " + "pl.lastUpdatedBy = 'test789', "
			+ "pl.lastUpdatedDate = :#{#santunan.lastUpdatedDate}, " + "pl.noBpk = :#{#santunan.noBpk}, "
			+ "pl.noRekening = :#{#santunan.noRekening}, "
			+ "pl.noSuratPenyelesaian = :#{#santunan.noSuratPenyelesaian}, "
			+ "pl.tglKelebihanBayar = :#{#santunan.tglKelebihanBayar}, "
			+ "pl.tglPembuatanBpk = :#{#santunan.tglPembuatanBpk}, " + "pl.tglProses = :#{#santunan.tglProses} "
			+ "WHERE pl.noBerkas = :#{#santunan.noBerkas}")
	public void updatePenyelesaianSantunan(PlPenyelesaianSantunan santunan);

}
