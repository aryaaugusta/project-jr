package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlRequestPerubahan;

public interface PlRequestPerubahanDao extends JpaRepository<PlRequestPerubahan, String> ,PlRequestPerubahanCustomDao{

	@Query("select a.createdBy, b.description, a.kodeKantorJr, c.nama, "
			+ "a.noBerkas, a.createdDate, a.keterangan, a.updatedBy, a.tglUpdate "
			+ "from PlRequestPerubahan a, AuthUser b ,FndKantorJasaraharja c "
			+ "where a.createdBy = b.login and a.kodeKantorJr = c.kodeKantorJr and "
			+ "a.noBerkas like :noBerkas ")
	public List<Object[]> findByNoBerkas(@Param("noBerkas") String noBerkas);
	
//	@Query("select a, b.description, c.nama "
//			+ "from PlRequestPerubahan a, AuthUser b, FndKantorJasaraharja c "
//			+ "where a.createdBy = b.login and a.kodeKantorJr = c.kodeKantorJr and "
//			+ "a.noBerkas like :noBerkas ")
//	public List<Object[]> findByNoBerkas(@Param("noBerkas") String noBerkas);
	
}
