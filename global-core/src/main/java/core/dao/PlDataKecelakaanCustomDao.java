package core.dao;

import java.util.List;

import core.model.PlAngkutanKecelakaan;
import core.model.PlDataKecelakaan;
import core.model.PlKorbanKecelakaan;

public interface PlDataKecelakaanCustomDao {
	
	public List<Object[]> getDataList(
			String kejadianStartDate,
			String kejadianEndDate,
			String asalBerkas,
			String samsat, 
			String laporanStartDate,
			String laporanEndDate,
			String instansi,
			String instansiPembuat,
			String noLaporan,
			String lokasi,
			String namaKorban,
			String noIdentitas,
			String lingkupJaminan,
			String jenisPertanggungan,
			String sifatCidera,
			String sifatKecelakaan,
			String kecelakaanKatostrop,
			String perusahaanPenerbangan,
			String perusahaanOtobus, 
			String search);
	
	public List<Object[]> getDataById(String idKecelakaan);
	
	public List<Object[]> getAngkutanByIdLaka(String idKecelakaan, String search);
	public List<Object[]> getAngkutanByIdAngkutan(String idAngkutan);

	public List<Object[]> getKorbanByIdLaka(String idKecelakaan, String search);
	public List<Object[]> getKorbanByIdKorban(String idKorban);

	public int deleteAngkutanByIdAngkutan(PlAngkutanKecelakaan idAngkutan);
	public int deleteKorbanByIdKorban(PlKorbanKecelakaan idKorban);
	public int deleteKorbanByIdLaka(PlDataKecelakaan idLaka);

	public int saveDataLaka(PlDataKecelakaan plDataKecelakaan);
	public int updateDataLaka(PlDataKecelakaan plDataKecelakaan);
	
	public int saveKendaraanTerlibat(PlAngkutanKecelakaan plAngkutanKecelakaan);
	public int updateKendaraanTerlibat(PlAngkutanKecelakaan plAngkutanKecelakaan);
	
	public int saveDataKorban(PlKorbanKecelakaan plKorbanKecelakaan);
	public int updateDataKorban(PlKorbanKecelakaan plKorbanKecelakaan);
	
	public List<Object[]> getRegisterSementara(String kejadianStartDate,
			String kejadianEndDate,
			String namaKantorInstansi,
			String laporStartDate,
			String laporEndDate,
			String noLaporPolisi,
			String namaKorbanLaka,
			String kodeJaminan,
			String statusLP,
			String search,
			String idKorban,
			String...param);
	
	public List<String> getKodeNoLaporan(String kodeInstansi);
	
	public List<Object[]> getKorbanFromLaka(String idKec);
	public List<Object[]> cekData(String tglKejadian, String tglLaporan, String kodeInstansi);
	
	public List<Object[]> reportDataLaka1(String idKec);
	public List<Object[]> reportDataLaka2(String idKec);

}
