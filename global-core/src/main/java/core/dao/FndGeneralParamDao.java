package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.FndGeneralParam;

public interface FndGeneralParamDao extends JpaRepository<FndGeneralParam, String>{

	@Query("select a from FndGeneralParam a where a.kodeParam in :kodeParam and a.crc = :crc")
	public List<FndGeneralParam> getPejabat(
			@Param("kodeParam")List<String> param, 
			@Param("crc") String crc);
	
	@Query(value="select nama_provinsi from fnd_provinsi a, "
			+ "PL_MAPPING_CAMAT b where substr(b.kode_lokasi,0,2) "
			+ "= substr(:kodeKantor,0,2) and substr(b.kode_camat,0,2) = a.KODE_PROVINSI "
			+ "group by nama_provinsi", nativeQuery=true)
	public String getProvinsi(@Param("kodeKantor")String kodeKantor);
}
