package core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.PlRequestPerubahanCustomDao;
import core.model.PlRegisterSementara;
import core.model.PlRequestPerubahan;

public class PlRequestPerubahanDaoImpl implements PlRequestPerubahanCustomDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Object[]> getListPPDataPengajuan(String noBerkas, String status, String search) {
//		String statusPermintaan=null;
//		if (status.contains("Pending") || status.equalsIgnoreCase("PENDING")){
//			statusPermintaan=
//		}
				
		
		String query="SELECT * FROM "
				+ "( SELECT KANTOR.NAMA AS NAMA_KANTOR, "
				+ "A.NO_BERKAS, "
				+ "KORBAN.NAMA AS NAMA_KORBAN, "
				+ "A.CREATED_DATE, "
				+ "A.STATUS_REQUEST, "
				+ "A.ID_REQUEST, "
				+ "A.TGL_UPDATE "
				+ "FROM PL_REQUEST_PERUBAHAN "
				+ "A LEFT JOIN (SELECT KODE_KANTOR_JR, NAMA FROM FND_KANTOR_JASARAHARJA) KANTOR "
				+ "ON A.KODE_KANTOR_JR=KANTOR.KODE_KANTOR_JR "
				+ "LEFT JOIN (SELECT NO_BERKAS, DIAJUKAN_DI, ID_KORBAN_KECELAKAAN "
				+ "FROM PL_PENGAJUAN_SANTUNAN) SANTUNAN "
				+ "ON A.NO_BERKAS= SANTUNAN.NO_BERKAS "
				+ "LEFT JOIN (SELECT ID_KORBAN_KECELAKAAN, NAMA FROM PL_KORBAN_KECELAKAAN) KORBAN "
				+ "ON SANTUNAN.ID_KORBAN_KECELAKAAN=KORBAN.ID_KORBAN_KECELAKAAN "
				+ "WHERE A.NO_BERKAS=SANTUNAN.NO_BERKAS) INSIDE "
				+ "WHERE INSIDE.NO_BERKAS LIKE '"+noBerkas+"' AND "
						+ "INSIDE.STATUS_REQUEST LIKE UPPER('"+status+"') AND "
						+ "(INSIDE.NAMA_KANTOR LIKE '"+search+"' OR "
								+ "INSIDE.NAMA_KORBAN LIKE '"+search+"' OR "
										+ "INSIDE.NO_BERKAS LIKE '"+search+"' OR "
												+ "CREATED_DATE LIKE '"+search+"'  OR "
												+ "INSIDE.STATUS_REQUEST LIKE '"+search+"')";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
		} 
	
	@Override
	public int deletePermintaan(PlRequestPerubahan plRequestPerubahan) {
		String query = "DELETE FROM PL_REQUEST_PERUBAHAN WHERE ID_REQUEST = ? ";
		
		em.createNativeQuery(query)
			.setParameter(1, plRequestPerubahan.getIdRequest()).executeUpdate();

		return 1;
	}

	} 