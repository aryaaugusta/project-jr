package core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import core.dao.PlRumahSakitCustomDao;

public class PlRumahSakitDaoImpl implements PlRumahSakitCustomDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Object[]> getDataRS(String kanWil, String kodeNama,
			String search) {
		// TODO Auto-generated method stub
		String query = "select a.kode_rumahsakit, a.deskripsi, a.jenis_rs, a.created_by, "
				+ "a.creation_date, a.last_updated_by, a.last_updated_date, a.flag_enable, "
				+ "a.pic, a.no_telp, a.alamat, b.LOKET_PENANGGUNGJAWAB, c.NAMA from PL_RUMAH_SAKIT a "
				+ "left join PL_RS_MAP_BPJS b on a.KODE_RUMAHSAKIT = b.KODE_RUMAHSAKIT "
				+ "left join FND_KANTOR_JASARAHARJA c on b.LOKET_PENANGGUNGJAWAB = c.KODE_KANTOR_JR "
				+ "where a.kode_rumahsakit like '"+kanWil+"' and "
				+ "(a.kode_rumahsakit like '"+kodeNama+"' or a.deskripsi like '"+kodeNama+"') and "
				+ "(a.kode_rumahsakit like '"+search+"' or a.deskripsi like '"+search+"' or "
				+ "A.FLAg_enable like '"+search+"' or a.jenis_rs like '"+search+"') "
				+ "order by a.kode_rumahsakit asc";
		
		@SuppressWarnings("unchecked")
		List<Object[]> getListBank=em.createNativeQuery(query).getResultList();
		return getListBank;
		
	}

}
