package core.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.PlBerkasPengajuanCustomDao;
import core.model.PlBerkasPengajuan;
import core.model.PlBerkasPengajuanPK;

public class PlBerkasPengajuanDaoImpl implements PlBerkasPengajuanCustomDao {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public PlBerkasPengajuan findBerkas(String noBerkas, String kodeBerkas) {
		String query = "select" + 
				"    plberkaspe0_.KODE_BERKAS as KODE_BERKAS1_124_0_," + 
				"    plberkaspe0_.NO_BERKAS as NO_BERKAS2_124_0_," + 
				"    plberkaspe0_.ABSAH_FLAG as ABSAH_FLAG3_124_0_," + 
				"    plberkaspe0_.CREATED_BY as CREATED_BY4_124_0_," + 
				"    plberkaspe0_.CREATION_DATE as CREATION_DATE5_124_0_," + 
				"    plberkaspe0_.DITERIMA_FLAG as DITERIMA_FLAG6_124_0_," + 
				"    plberkaspe0_.ID_GUID as ID_GUID7_124_0_," + 
				"    plberkaspe0_.LAST_UPDATED_BY as LAST_UPDATED_BY8_124_0_," + 
				"    plberkaspe0_.LAST_UPDATED_DATE as LAST_UPDATED_DATE9_124_0_," + 
				"    plberkaspe0_.TGL_TERIMA_BERKAS as TGL_TERIMA_BERKAS10_124_0_" + 
				"from" + 
				"    PL_BERKAS_PENGAJUAN plberkaspe0_" + 
				"where" + 
				"    plberkaspe0_.KODE_BERKAS=?" + 
				"    and plberkaspe0_.NO_BERKAS=?;";
		
		return (PlBerkasPengajuan) em.createNativeQuery(query, PlBerkasPengajuan.class)
				.setParameter(1, kodeBerkas)
				.setParameter(2, noBerkas)
				.getSingleResult();
	}

	@Override
	public void saveBerkas(PlBerkasPengajuan berkas) {
		// TODO
	}

	@Override
	public PlBerkasPengajuan findBerkas(PlBerkasPengajuanPK plBerkasPengajuanPK) {
		return this.findBerkas(plBerkasPengajuanPK.getNoBerkas(), plBerkasPengajuanPK.getKodeBerkas());
	}

	@Override
	public void updateBerkas(PlBerkasPengajuan berkas) {
		// TODO
	}

}
