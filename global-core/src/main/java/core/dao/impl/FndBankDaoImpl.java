package core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.FndBankCustomDao;


public class FndBankDaoImpl implements FndBankCustomDao{
	
	@PersistenceContext
	private EntityManager em;


	@Override
	public List<Object[]> getListDataBank(String search) {
		String query = "SELECT a.KODE_BANK, "
				+ "a.NAMA_BANK, "
				+ "NVL (rek_bank.rekening, 0) AS JUMLAH_REKENING, "
				+ "a.KODE_BANK_2, "
				+ "a.status "
				+ "FROM FND_BANK a "
				+ "LEFT JOIN "
				+ "(SELECT KODE_BANK, COUNT(NO_REKENING) AS rekening FROM PL_REKENING_RS GROUP BY KODE_BANK) "
				+ "rek_bank ON a.KODE_BANK = rek_bank.KODE_BANK where "
				+ "a.KODE_BANK like '"+search+"' "
						+ "or a.NAMA_BANK like '"+search+"' "
								+ "or a.KODE_BANK_2 like '"+search+"' "
										+ "or a.status like '"+search+"'"; 
		
		@SuppressWarnings("unchecked")
		List<Object[]> getListBank=em.createNativeQuery(query).getResultList();
		return getListBank;
		
	}

}
