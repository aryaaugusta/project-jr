
package core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.PlDataKecelakaanCustomDao;
import core.model.PlAngkutanKecelakaan;
import core.model.PlDataKecelakaan;
import core.model.PlKorbanKecelakaan;

public class PlDataKecelakaanDaoImpl implements PlDataKecelakaanCustomDao{

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Object[]> getDataList(
			String kejadianStartDate,
			String kejadianEndDate,
			String asalBerkas,
			String samsat, 
			String laporanStartDate,
			String laporanEndDate,
			String instansi,
			String instansiPembuat,
			String noLaporan,
			String lokasi,
			String namaKorban,
			String noIdentitas,
			String lingkupJaminan,
			String jenisPertanggungan,
			String sifatCidera,
			String sifatKecelakaan,
			String kecelakaanKatostrop,
			String perusahaanPenerbangan,
			String perusahaanOtobus, 
			String search) {
			
			String katastropCondition = "(1 = 1)";
			if (kecelakaanKatostrop != null && "%biasa%".equals(kecelakaanKatostrop.trim().toLowerCase())) {
				katastropCondition = "(INSIDE.JML_KORBAN_MENINGGAL < 4)";
			} else if (kecelakaanKatostrop != null && "%katastrop%".equals(kecelakaanKatostrop.trim().toLowerCase())) {
				katastropCondition = "(INSIDE.JML_KORBAN_MENINGGAL >= 4)";
			} else {
				System.out.println(kecelakaanKatostrop);
				System.out.println(kecelakaanKatostrop.trim().toLowerCase());
			}
		
		String query="SELECT * FROM "
				+ "(SELECT "
				+ "A.TGL_KEJADIAN, "
				+ "NVL(LOKASI.DESKRIPSI, '-') AS LOKASI, "
				+ "NVL(INSTANSI.DESKRIPSI, '-') AS INSTANSI, "
				+ "A.NO_LAPORAN_POLISI, "
				+ "NVL(NAMA.NAMA, '-') AS NAMA, "
				+ "NVL(CIDERA.RV_MEANING, '-') AS CIDERA, "
				+ "NVL(SIFAT_LAKA.RV_MEANING, '-') AS SIFAT_LAKA, "
				+ "NVL(KORB.JML_KORBAN, '0'), "
//				+ "/* SELECT DIBAWAH UNTUK SEARCH */ "
				+ "A.STATUS_LAPORAN_POLISI, "
				+ "A.ID_KECELAKAAN, "
				+ "A.ASAL_BERKAS, "
				+ "A.TGL_LAPORAN_POLISI, "
				+ "A.KODE_INSTANSI, "
				+ "NVL(NAMA.NO_IDENTITAS, '-') AS NO_IDENTITAS, "
				+ "NVL(NAMA.KODE_SIFAT_CIDERA, '-') AS KODE_SIFAT_CIDERA, "
				+ "NVL(JAMINAN.LINGKUP_JAMINAN, '_') AS LINGKUP_JAMINAN, "
				+ "NVL(JAMINAN.DESKRIPSI, '-') AS DESKRIP_JAMINAN, "
				+ "NVL(CIDERA.RV_HIGH_VALUE, '-') AS KODE_CIDERA, "
				+ "NVL(NAMA.KODE_JAMINAN, '-') AS KODE_JAMINAN, "
				+ "SIFAT_LAKA.RV_MEANING AS SIFAT_KECELAKAAN, "
				+ "NVL(NAMA.ID_KORBAN_KECELAKAAN, '-') AS ID_KORBAN_KECELAKAAN, "
				+ "NVL(A.KODE_WILAYAH,'-') AS SAMSAT, "
				+ "A.CREATION_DATE, "
				// START Melihat jumlah korban meninggal
				+ "Nvl(KORB_MENINGGAL.jml_korban_meninggal, '0') AS JML_KORBAN_MENINGGAL "
				// END Melihat jumlah korban meninggal
				+ "FROM PL_DATA_KECELAKAAN A "
				+ "LEFT JOIN (SELECT KODE_LOKASI, DESKRIPSI FROM FND_LOKASI) LOKASI "
				+ "ON A.KODE_LOKASI = LOKASI.KODE_LOKASI "
				+ "LEFT JOIN (SELECT KODE_INSTANSI, DESKRIPSI FROM PL_INSTANSI) INSTANSI "
				+ "ON A.KODE_INSTANSI = INSTANSI.KODE_INSTANSI "
				+ "LEFT JOIN (SELECT NAMA, ID_KECELAKAAN, KODE_SIFAT_CIDERA, ID_KORBAN_KECELAKAAN, KODE_JAMINAN, NO_IDENTITAS FROM PL_KORBAN_KECELAKAAN) NAMA "
				+ "ON A.ID_KECELAKAAN = NAMA.ID_KECELAKAAN "
				// START Melihat jumlah korban meninggal
				+ "LEFT JOIN (SELECT KOL.id_kecelakaan, COUNT(ID_KORBAN_KECELAKAAN) AS JML_KORBAN_MENINGGAL FROM PL_KORBAN_KECELAKAAN KOL WHERE KODE_SIFAT_CIDERA IN ('01', '05', '07', '08') GROUP BY KOL.ID_KECELAKAAN) KORB_MENINGGAL "           
				+ "ON A.ID_KECELAKAAN = KORB_MENINGGAL.ID_KECELAKAAN "
				// END Melihat jumlah korban meninggal
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_HIGH_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA' ) CIDERA "
				+ "ON NAMA.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT KECELAKAAN' ) SIFAT_LAKA "
				+ "ON A.SIFAT_KECELAKAAN = SIFAT_LAKA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT KODE_JAMINAN, DESKRIPSI, LINGKUP_JAMINAN FROM PL_JAMINAN) JAMINAN "
				+ "ON NAMA.KODE_JAMINAN = JAMINAN.KODE_JAMINAN "
				+ "LEFT JOIN (SELECT KOL.ID_KECELAKAAN, COUNT (ID_KORBAN_KECELAKAAN) AS JML_KORBAN FROM PL_KORBAN_KECELAKAAN KOL GROUP BY KOL.ID_KECELAKAAN) KORB "
				+ "ON A.ID_KECELAKAAN = KORB.ID_KECELAKAAN) INSIDE "
				+ "WHERE "
				+ "(TRUNC(INSIDE.TGL_KEJADIAN) >= TRUNC(TO_DATE('"+kejadianStartDate+"', 'DD/MM/YYYY')) AND TRUNC(INSIDE.TGL_KEJADIAN) <= TRUNC(TO_DATE('"+kejadianEndDate+"', 'DD/MM/YYYY'))) AND "
				+ "INSIDE.ASAL_BERKAS LIKE UPPER ('"+asalBerkas+"') AND "
				+ "INSIDE.TGL_LAPORAN_POLISI BETWEEN TO_DATE('"+laporanStartDate+"', 'DD/MM/YYYY') AND TO_DATE('"+laporanEndDate+"', 'DD/MM/YYYY') AND "
				+ "(INSIDE.KODE_INSTANSI LIKE '"+instansi+"' OR INSIDE.INSTANSI LIKE '"+instansi+"') "
				+ "AND INSIDE.NO_LAPORAN_POLISI LIKE '"+noLaporan+"' AND "
				+ "INSIDE.NAMA LIKE '"+namaKorban+"' AND "
				+ "INSIDE.SAMSAT LIKE '"+samsat+"' AND "
				+ "INSIDE.NO_IDENTITAS LIKE '"+noIdentitas+"' AND "
				+ "INSIDE.KODE_SIFAT_CIDERA LIKE '"+sifatCidera+"' AND "
				+ "INSIDE.SIFAT_KECELAKAAN LIKE '"+sifatKecelakaan+"' AND "
				+ "INSIDE.LINGKUP_JAMINAN LIKE '"+lingkupJaminan+"' AND "
				+ "(INSIDE.KODE_JAMINAN LIKE '"+jenisPertanggungan+"' OR INSIDE.DESKRIP_JAMINAN LIKE '"+jenisPertanggungan+"') "
				+ "AND INSIDE.STATUS_LAPORAN_POLISI LIKE '"+instansiPembuat+"' AND "
				+ "(INSIDE.LOKASI LIKE '"+search+"' OR "
				+ "INSIDE.INSTANSI LIKE '"+search+"' OR "
				+ "INSIDE.NO_LAPORAN_POLISI LIKE '"+search+"' OR "
				+ "INSIDE.NAMA LIKE '"+search+"' OR "
				+ "INSIDE.KODE_CIDERA LIKE '"+search+"' OR "
				+ "INSIDE.SIFAT_LAKA LIKE '"+search+"' OR "
				+ "INSIDE.STATUS_LAPORAN_POLISI LIKE '"+search+"') "
				+ "AND " + katastropCondition
				+ " ORDER BY INSIDE.CREATION_DATE DESC"; 
		
		System.out.println(query);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}
	

	@Override
	public List<Object[]> getDataById(String idKecelakaan) {
		
		String query="SELECT A.TGL_LAPORAN_POLISI, "
				+ "A.NAMA_PETUGAS, "
				+ "A.TGL_KEJADIAN, "
				+ "A.DESKRIPSI_LOKASI, "
				+ "A.DESKRIPSI_KECELAKAAN, "
				+ "A.GPS_LS, "
				+ "A.GPS_LU, "
				+ "ASAL_BERKAS.NAMA AS ASAL_BERKAS, "
				+ "INSTANSI.DESKRIPSI, "
				+ "LOKASI.NAMA_PROVINSI, "
				+ "LOKASI.NAMA_KABKOTA, "
				+ "LOKASI.NAMA_CAMAT, "
				+ "SAMSAT.NAMA AS SAMSAT, "
				+ "KASUS_LAKA.RV_MEANING AS KASUS_LAKA, "
				+ "SIFAT_LAKA.RV_MEANING AS SIFAT_LAKA, "
				+ "A.ID_KECELAKAAN,"
				+ "A.NO_LAPORAN_POLISI, "
				+ "ASAL_BERKAS.KODE_KANTOR_JR AS KODE_KANTOR_ASAL, "
				+ "INSTANSI.KODE_INSTANSI AS KODE_INSTANSI, "
				+ "SAMSAT.KODE_KANTOR_JR AS KODE_SAMSAT, "
				+ "LOKASI.KODE_CAMAT, "
				+ "LOKASI.KODE_PROVINSI, "
				+ "LOKASI.KODE_KABKOTA, "
				+ "KASUS_LAKA.RV_LOW_VALUE AS KODE_KASUS_LAKA, "
				+ "SIFAT_LAKA.RV_LOW_VALUE AS KODE_SIFAT_LAKA, "
				+ "A.NO_URUT, "
				+ "A.STATUS_LAPORAN_POLISI, "
				+ "A.STATUS_TRANSISI, "
				+ "A.KODE_LOKASI "
				+ "FROM PL_DATA_KECELAKAAN A "
				+ "LEFT JOIN (SELECT KODE_KANTOR_JR, NAMA FROM FND_KANTOR_JASARAHARJA) ASAL_BERKAS "
				+ "ON A.ASAL_BERKAS = ASAL_BERKAS.KODE_KANTOR_JR "
				+ "LEFT JOIN (SELECT KODE_INSTANSI, DESKRIPSI FROM PL_INSTANSI) INSTANSI "
				+ "ON A.KODE_INSTANSI = INSTANSI.KODE_INSTANSI "
				+ "LEFT JOIN (SELECT KODE_KANTOR_JR, NAMA FROM FND_KANTOR_JASARAHARJA) SAMSAT "
				+ "ON A.KODE_WILAYAH = SAMSAT.KODE_KANTOR_JR "
				+ "LEFT JOIN (SELECT KODE_LOKASI, KODE_CAMAT FROM PL_MAPPING_CAMAT) MAPPING_CAMAT "
				+ "ON A.KODE_LOKASI = MAPPING_CAMAT.KODE_LOKASI "
				+ "LEFT JOIN (SELECT NAMA_CAMAT, NAMA_KABKOTA, NAMA_PROVINSI, KODE_CAMAT, KODE_PROVINSI, KODE_KABKOTA FROM FND_CAMAT) LOKASI "
				+ "ON MAPPING_CAMAT.KODE_CAMAT = LOKASI.KODE_CAMAT "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'PL KASUS KECELAKAAN' AND FLAG_ENABLE = 'Y') KASUS_LAKA "
				+ "ON A.KODE_KASUS_KECELAKAAN = KASUS_LAKA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT KECELAKAAN' AND FLAG_ENABLE = 'Y') SIFAT_LAKA "
				+ "ON A.SIFAT_KECELAKAAN = SIFAT_LAKA.RV_LOW_VALUE "
//				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'INSTANSI PEMBUAT LK' AND FLAG_ENABLE = 'Y') PEMBUAT_LAPORAN "
//				+ "ON "
				+ "WHERE ID_KECELAKAAN LIKE '"+idKecelakaan+"'";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}
	
	@Override
	public int saveDataLaka(PlDataKecelakaan plDataKecelakaan) {
		String sql = "INSERT INTO HSE_SYSTEM.PL_DATA_KECELAKAAN "
				+ "(ID_KECELAKAAN, "
				+ "KODE_INSTANSI, "
				+ "KODE_KANTOR_JR, "
				+ "KODE_KASUS_KECELAKAAN, "
				+ "KODE_LOKASI, "
				+ "NO_URUT, "
				+ "STATUS_TRANSISI, "
				+ "TGL_KEJADIAN, "
				+ "ASAL_BERKAS, "
				+ "CREATED_BY, "
				+ "CREATION_DATE, "
				+ "DESKRIPSI_KECELAKAAN, "
				+ "DESKRIPSI_LOKASI, "
				+ "ID_GUID, "
				+ "KODE_WILAYAH, "
				+ "NAMA_PETUGAS, "
				+ "NO_LAPORAN_POLISI, "
				+ "TGL_LAPORAN_POLISI) "
				+ "VALUES (?,?,?,?,?,?,?,TO_CHAR(?, 'dd/mm/yyyy hh24:mi'), ?,?,?,?,?,?,?,?,?,?)";
				
		em.createNativeQuery(sql)
				.setParameter(1, plDataKecelakaan.getIdKecelakaan())
				.setParameter(2, plDataKecelakaan.getKodeInstansi())
				.setParameter(3, plDataKecelakaan.getKodeKantorJr())
				.setParameter(4, plDataKecelakaan.getKodeKasusKecelakaan())
				.setParameter(5, plDataKecelakaan.getKodeLokasi())
				.setParameter(6, plDataKecelakaan.getNoUrut())
				.setParameter(7, plDataKecelakaan.getStatusTransisi())
				.setParameter(8, plDataKecelakaan.getTglKejadian())
				.setParameter(9, plDataKecelakaan.getAsalBerkas())
				.setParameter(10, plDataKecelakaan.getCreatedBy())
				.setParameter(11, plDataKecelakaan.getCreationDate())
				.setParameter(12, plDataKecelakaan.getDeskripsiKecelakaan())
				.setParameter(13, plDataKecelakaan.getDeskripsiLokasi())
				.setParameter(14, plDataKecelakaan.getIdGuid())
				.setParameter(15, plDataKecelakaan.getKodeWilayah())
				.setParameter(16, plDataKecelakaan.getNamaPetugas())
				.setParameter(17, plDataKecelakaan.getNoLaporanPolisi())
				.setParameter(18, plDataKecelakaan.getTglLaporanPolisi()).executeUpdate();
		
		return 1;
	}

	@Override
	public int updateDataLaka(PlDataKecelakaan plDataKecelakaan) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int saveKendaraanTerlibat(PlAngkutanKecelakaan plAngkutanKecelakaan) {
		String sql = "INSERT INTO HSE_SYSTEM.PL_ANGKUTAN_KECELAKAAN ("
				+ "ID_ANGKUTAN_KECELAKAAN, "
				+ "ID_KECELAKAAN, "
				+ "STATUS_KENDARAAN, "
				+ "ALAMAT_PENGEMUDI, "
				+ "CREATED_BY, "
				+ "CREATION_DATE, "
				+ "ID_GUID, KODE, "
				+ "KODE_GOLONGAN, "
				+ "KODE_JENIS, "
				+ "KODE_JENIS_SIM, "
				+ "KODE_MERK, "
				+ "KODE_PO, "
				+ "MASA_BERLAKU_SIM, "
				+ "NAMA_PEMILIK, "
				+ "NAMA_PENGEMUDI, "
				+ "NO_POLISI, "
				+ "NO_SIM_PENGEMUDI, "
				+ "TAHUN_PEMBUATAN"
				+ ") "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				
		em.createNativeQuery(sql)
				.setParameter(1, plAngkutanKecelakaan.getIdAngkutanKecelakaan())
				.setParameter(2, plAngkutanKecelakaan.getIdKecelakaan())
				.setParameter(3, plAngkutanKecelakaan.getStatusKendaraan())
				.setParameter(4, plAngkutanKecelakaan.getAlamatPengemudi())
				.setParameter(5, plAngkutanKecelakaan.getCreatedBy())
				.setParameter(6, plAngkutanKecelakaan.getCreationDate())
				.setParameter(7, plAngkutanKecelakaan.getIdGuid())
				.setParameter(8, plAngkutanKecelakaan.getKode())
				.setParameter(9, plAngkutanKecelakaan.getKodeGolongan())
				.setParameter(10, plAngkutanKecelakaan.getKodeJenis())
				.setParameter(11, plAngkutanKecelakaan.getKodeJenisSim())
				.setParameter(12, plAngkutanKecelakaan.getKodeMerk())
				.setParameter(13, plAngkutanKecelakaan.getKodePo())
				.setParameter(14, plAngkutanKecelakaan.getMasaBerlakuSim())
				.setParameter(15, plAngkutanKecelakaan.getNamaPemilik())
				.setParameter(16, plAngkutanKecelakaan.getNamaPengemudi())
				.setParameter(17, plAngkutanKecelakaan.getNoPolisi())
				.setParameter(18, plAngkutanKecelakaan.getNoSimPengemudi())
				.setParameter(19, plAngkutanKecelakaan.getTahunPembuatan()).executeUpdate();
		
		return 1;
	}

	@Override
	public int updateKendaraanTerlibat(PlAngkutanKecelakaan plAngkutanKecelakaan) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int saveDataKorban(PlKorbanKecelakaan plKorbanKecelakaan) {
		String sql = "INSERT INTO HSE_SYSTEM.PL_KORBAN_KECELAKAAN "
				+ "(ALAMAT, "
				+ "ID_KECELAKAAN, "
				+ "ID_KORBAN_KECELAKAAN, "
				+ "KODE_PEKERJAAN, "
				+ "KODE_SIFAT_CIDERA, "
				+ "KODE_STATUS_KORBAN, "
				+ "NAMA, "
				+ "CREATED_BY, "
				+ "CREATION_DATE, "
				+ "ID_ANGKUTAN_KECELAKAAN, "
				+ "ID_ANGKUTAN_PENANGGUNG, "
				+ "ID_GUID, "
				+ "JENIS_IDENTITAS, "
				+ "JENIS_KELAMIN, "
				+ "KODE_JAMINAN, "
				+ "NO_IDENTITAS, "
				+ "NO_TELP, "
				+ "STATUS_NIKAH, "
				+ "UMUR) "
				+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				
		em.createNativeQuery(sql)
				.setParameter(1, plKorbanKecelakaan.getAlamat())
				.setParameter(2, plKorbanKecelakaan.getIdKecelakaan())
				.setParameter(3, plKorbanKecelakaan.getIdKorbanKecelakaan())
				.setParameter(4, plKorbanKecelakaan.getKodePekerjaan())
				.setParameter(5, plKorbanKecelakaan.getKodeSifatCidera())
				.setParameter(6, plKorbanKecelakaan.getKodeStatusKorban())
				.setParameter(7, plKorbanKecelakaan.getNama())
				.setParameter(8, plKorbanKecelakaan.getCreatedBy())
				.setParameter(9, plKorbanKecelakaan.getCreationDate())
				.setParameter(10, plKorbanKecelakaan.getIdAngkutanKecelakaan())
				.setParameter(11, plKorbanKecelakaan.getIdAngkutanPenanggung())
				.setParameter(12, plKorbanKecelakaan.getIdGuid())
				.setParameter(13, plKorbanKecelakaan.getJenisIdentitas())
				.setParameter(14, plKorbanKecelakaan.getJenisKelamin())
				.setParameter(15, plKorbanKecelakaan.getKodeJaminan())
				.setParameter(16, plKorbanKecelakaan.getNoIdentitas())
				.setParameter(17, plKorbanKecelakaan.getNoTelp())
				.setParameter(18, plKorbanKecelakaan.getStatusNikah())
				.setParameter(19, plKorbanKecelakaan.getUmur()).executeUpdate();
		
		return 1;
	}

	@Override
	public int updateDataKorban(PlKorbanKecelakaan plKorbanKecelakaan) {
		// TODO Auto-generated method stub
		return 0;
	}


	
	public List<Object[]> getRegisterSementara(String kejadianStartDate,
			String kejadianEndDate,
			String namaKantorInstansi,
			String laporStartDate,
			String laporEndDate,
			String noLaporPolisi,
			String namaKorbanLaka,
			String kodeJaminan,
			String statusLP,
			String search,
			String idKorban,
			String...param ){
		String idKecelakan = "";
		String isRegister = "N";
		if(param.length>0){
			idKecelakan = param[0];
			if(param[1]!=null){
				isRegister = param[1];
			}
		}
		
		String forRegister = "";
		if(isRegister.equalsIgnoreCase("Y")){
			forRegister = "AND B.ID_KORBAN_KECELAKAAN NOT IN (SELECT DISTINCT(ID_KORBAN_KECELAKAAN) FROM PL_REGISTER_SEMENTARA)";
		}
		
		String query = "SELECT "
			+ "A.NO_LAPORAN_POLISI,"
			+ "A.TGL_KEJADIAN,"
			+ "B.NAMA AS NAMAKORBAN,"
			+ "D.DESKRIPSI,"
			+ "A.STATUS_LAPORAN_POLISI,"
			+ "B.UMUR,"
			+ "B.JENIS_KELAMIN,"
			+ "E.RV_MEANING AS CIDERA,"
			+ "G.NO_POLISI,"
			+ "G.NAMA_PENGEMUDI,"
			+ "H.RV_MEANING AS KASUS,"
			+ "A.DESKRIPSI_KECELAKAAN,"
			+ "F.RV_MEANING AS STATUS,"
			+ "C.NAMA AS NAMAINSTANSI, "
			+ "A.ID_KECELAKAAN, "
			+ "A.DESKRIPSI_LOKASI, "
			+ "B.NO_IDENTITAS, "
			+ "B.JENIS_IDENTITAS, "
			+ "B.ALAMAT, "
			+ "B.STATUS_NIKAH, "
			+ "B.NO_TELP, "
			+ "A.KODE_KANTOR_JR, "
			+ "B.ID_KORBAN_KECELAKAAN, "
			+ "B.KODE_SIFAT_CIDERA, "
			+ "B.KODE_JAMINAN, "
			+ "D.lINGKUP_JAMINAN, "
			//ADDED BY LUTHFI
			+ "J.NAMA_PROVINSI, "
			+ "J.NAMA_KABKOTA, "
			+ "J.NAMA_CAMAT, "
			+ "E.RV_LOW_VALUE, "
			
			+ "E.RV_HIGH_VALUE, "
			+ "A.TGL_LAPORAN_POLISI "
			+ "FROM "
			+ "PL_DATA_KECELAKAAN A, "
			+ "PL_KORBAN_KECELAKAAN B "
			//ADDED BY LUTHFI
			+ "LEFT JOIN "
			+ "(SELECT I.KODE_LOKASI, X.* FROM "
			+ "PL_MAPPING_CAMAT I, FND_CAMAT X WHERE I.KODE_CAMAT = X.KODE_CAMAT AND I.FLAG_ENABLE = 'Y' AND X.FLAG_ENABLE ='Y') J ON J.KODE_LOKASI = B.ID_GUID, "
			
			+ "FND_KANTOR_JASARAHARJA C,"
			+ "PL_JAMINAN D,"
			+ "DASI_JR_REF_CODES E,"
			+ "DASI_JR_REF_CODES F,"
			+ "PL_ANGKUTAN_KECELAKAAN G,"
			+ "DASI_JR_REF_CODES H "
//			+ "PL_MAPPING_CAMAT I, "
//			+ "FND_CAMAT J "
			+ "WHERE "
			+ "A.ID_KECELAKAAN = B.ID_KECELAKAAN "
			+ "AND A.KODE_KANTOR_JR = C.KODE_KANTOR_JR "
			+ "AND B.KODE_JAMINAN = D.KODE_JAMINAN "
			+ "AND B.KODE_SIFAT_CIDERA = E.RV_LOW_VALUE "
			+ "AND B.KODE_STATUS_KORBAN = F.RV_LOW_VALUE "
			+ "AND B.ID_ANGKUTAN_PENANGGUNG = G.ID_ANGKUTAN_KECELAKAAN "
			+ "AND A.KODE_KASUS_KECELAKAAN = H.RV_LOW_VALUE "
			+ "AND (TRUNC(A.TGL_KEJADIAN) >= TRUNC(TO_DATE('"+kejadianStartDate+"', 'DD/MM/YYYY')) AND TRUNC(A.TGL_KEJADIAN) <= TRUNC(TO_DATE('"+kejadianEndDate+"', 'DD/MM/YYYY'))) "
			+ "AND C.NAMA LIKE '"+namaKantorInstansi+"' "
			+ "AND (TRUNC(A.TGL_LAPORAN_POLISI) >= TRUNC(TO_DATE('"+laporStartDate+"', 'DD/MM/YYYY')) AND TRUNC(A.TGL_LAPORAN_POLISI) <= TRUNC(TO_DATE('"+laporEndDate+"', 'DD/MM/YYYY'))) "
			+ "AND A.NO_LAPORAN_POLISI LIKE '"+noLaporPolisi+"' "
			+ "AND B.NAMA LIKE '"+namaKorbanLaka+"' "
			+ "AND B.KODE_JAMINAN LIKE '"+kodeJaminan+"' "
			+ "AND A.STATUS_LAPORAN_POLISI LIKE '"+statusLP+"' "
			+ "AND E.RV_DOMAIN = 'KODE SIFAT CIDERA' "
			+ "AND F.RV_DOMAIN = 'KODE STATUS KORBAN' "
			+ "AND H.RV_DOMAIN = 'PL KASUS KECELAKAAN' "
			+ "AND A.ID_KECELAKAAN LIKE '"+idKecelakan+"' "
			+ "and B.ID_KORBAN_KECELAKAAN LIKE '"+idKorban+"'"
			//ADDED BY LUTHFI
//			+ "AND B.ID_GUID = I.KODE_LOKASI "
//			+ "AND I.KODE_CAMAT = J.KODE_CAMAT "
			//forRegsiter
			+ forRegister
			+ "AND (A.NO_LAPORAN_POLISI LIKE UPPER('"+search+"') "
			+ "OR TO_CHAR(A.TGL_KEJADIAN,'DD/MM/YYYY') LIKE '"+search+"' "
			+ "OR B.NAMA LIKE UPPER('"+search+"') "
			+ "OR D.DESKRIPSI LIKE UPPER('"+search+"') "
			+ "OR A.STATUS_LAPORAN_POLISI LIKE UPPER('"+search+"')) ";
		
		
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}


	@Override
	public List<Object[]> getAngkutanByIdLaka(String idKecelakaan, String search) {
		String sql = "SELECT A.NO_POLISI, "
				+ "A.KODE_JENIS, "
				+ "A.NAMA_PENGEMUDI, "
				+ "JENIS.DESKRIPSI, "
				+ "STATUS.RV_MEANING as status, "
				+ "SIM.RV_MEANING as sim, MERK.RV_MEANING as merk, "
				+ "A.ID_ANGKUTAN_KECELAKAAN "
				+ "FROM PL_ANGKUTAN_KECELAKAAN A "
				+ "LEFT JOIN (SELECT KODE_JENIS, DESKRIPSI FROM FND_JENIS_KENDARAAN) JENIS "
				+ "ON A.KODE_JENIS = JENIS.KODE_JENIS "
				+ "LEFT JOIN (SELECT RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS KENDARAAN' AND FLAG_ENABLE = 'Y') STATUS "
				+ "ON A.STATUS_KENDARAAN = STATUS.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE JENIS SIM' AND FLAG_ENABLE = 'Y') SIM "
				+ "ON A.KODE_JENIS_SIM = SIM.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'PL MERK KENDARAAN' AND FLAG_ENABLE = 'Y') "
				+ "MERK ON A.KODE_MERK = MERK.RV_LOW_VALUE "
				+ "WHERE A.ID_KECELAKAAN LIKE '"+idKecelakaan+"' AND "
				+ "(A.NO_POLISI LIKE '"+search+"' OR "
				+ "A.KODE_JENIS LIKE '"+search+"' OR "
				+ "STATUS.RV_MEANING LIKE '"+search+"' OR "
				+ "A.NAMA_PENGEMUDI LIKE '"+search+"') order by A.ID_ANGKUTAN_KECELAKAAN ASC";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(sql).getResultList();

		return list;
	}


	@Override
	public List<Object[]> getKorbanByIdLaka(String idKecelakaan, String search) {
		String sql = "SELECT A.NAMA, "
				+ "A.UMUR, "
				+ "CIDERA.RV_HIGH_VALUE, "
				+ "STATUS.RV_MEANING AS STATUS, "
				+ "PERTANGGUNGAN.DESKRIPSI, "
				+ "PEKERJAAN.RV_MEANING AS PEKERJAAN, "
				+ "CIDERA.RV_MEANING AS CIDERA,"
				+ "A.ID_KORBAN_KECELAKAAN "
				+ "FROM PL_KORBAN_KECELAKAAN A "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE JENIS PEKERJAAN' AND FLAG_ENABLE = 'Y') PEKERJAAN "
				+ "ON A.KODE_PEKERJAAN = PEKERJAAN.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_HIGH_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA' AND FLAG_ENABLE = 'Y') CIDERA "
				+ "ON A.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS KORBAN' AND FLAG_ENABLE = 'Y') STATUS "
				+ "ON A.KODE_STATUS_KORBAN = STATUS.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT KODE_JAMINAN, DESKRIPSI, LINGKUP_JAMINAN FROM PL_JAMINAN ) PERTANGGUNGAN "
				+ "ON A.KODE_JAMINAN = PERTANGGUNGAN.KODE_JAMINAN "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS NIKAH' AND FLAG_ENABLE = 'Y') NIKAH "
				+ "ON A.STATUS_NIKAH = NIKAH.RV_LOW_VALUE WHERE A.ID_KECELAKAAN LIKE '"+idKecelakaan+"' AND "
				+ "(A.NAMA LIKE '"+search+"' OR "
				+ "A.UMUR LIKE '"+search+"' OR "
				+ "CIDERA.RV_HIGH_VALUE LIKE '"+search+"' OR "
				+ "STATUS.RV_MEANING LIKE '"+search+"' OR "
				+ "PERTANGGUNGAN.DESKRIPSI LIKE '"+search+"') order by A.ID_KORBAN_KECELAKAAN ASC";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(sql).getResultList();

		return list;
	}


	@Override
	public List<Object[]> getAngkutanByIdAngkutan(String idAngkutan) {
		String sql = "SELECT A.NO_POLISI, "
				+ "A.NAMA_PENGEMUDI, "
				+ "A.NAMA_PEMILIK, "
				+ "A.ALAMAT_PENGEMUDI, "
				+ "A.ALAMAT_PEMILIK, "
				+ "A.TAHUN_PEMBUATAN, "
				+ "A.NO_SIM_PENGEMUDI, "
				+ "A.MASA_BERLAKU_SIM, "
				+ "A.ID_ANGKUTAN_KECELAKAAN, "
				+ "JENIS_KENDARAAN.KODE_JENIS AS KODE_JENIS_KENDARAAN, "
				+ "JENIS_KENDARAAN.DESKRIPSI AS DESKRIPSI_JENIS, "
				+ "STATUS_KENDARAAN.RV_MEANING AS STATUS_KENDARAAN, "
				+ "JENIS_SIM.RV_MEANING AS JENIS_SIM, "
				+ "MERK.RV_MEANING AS MERK "
				+ "FROM PL_ANGKUTAN_KECELAKAAN A "
				+ "LEFT JOIN (SELECT KODE_JENIS, DESKRIPSI FROM FND_JENIS_KENDARAAN) JENIS_KENDARAAN "
				+ "ON A.KODE_JENIS = JENIS_KENDARAAN.KODE_JENIS "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS KENDARAAN' AND FLAG_ENABLE = 'Y') STATUS_KENDARAAN "
				+ "ON A.STATUS_KENDARAAN = STATUS_KENDARAAN.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT NAMA_CAMAT, NAMA_KABKOTA, NAMA_PROVINSI, KODE_CAMAT FROM FND_CAMAT) CAMAT_PENGEMUDI "
				+ "ON A.ALAMAT_PENGEMUDI = CAMAT_PENGEMUDI.KODE_CAMAT "
				+ "LEFT JOIN (SELECT NAMA_CAMAT, NAMA_KABKOTA, NAMA_PROVINSI, KODE_CAMAT FROM FND_CAMAT) CAMAT_PEMILIK "
				+ "ON A.ALAMAT_PENGEMUDI = CAMAT_PEMILIK.KODE_CAMAT LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE JENIS SIM' AND FLAG_ENABLE = 'Y') JENIS_SIM "
				+ "ON A.KODE_JENIS_SIM = JENIS_SIM.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'PL MERK KENDARAAN' AND FLAG_ENABLE = 'Y') MERK "
				+ "ON A.KODE_MERK = MERK.RV_LOW_VALUE "
				+ "WHERE "
				+ "A.ID_ANGKUTAN_KECELAKAAN LIKE '"+idAngkutan+"' order by A.ID_ANGKUTAN_KECELAKAAN ASC "; 
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(sql).getResultList();

		return list;
	}


	@Override
	public List<Object[]> getKorbanByIdKorban(String idKorban) {
		String sql = "SELECT A.NO_IDENTITAS, "
				+ "A.NAMA, "
				+ "A.JENIS_KELAMIN, "
				+ "A.UMUR, "
				+ "A.ALAMAT, "
				+ "A.NO_TELP, "
				+ "IDENTITAS.RV_MEANING AS IDENTITAS, "
				+ "PEKERJAAN.RV_MEANING AS PEKERJAAN, "
				+ "CIDERA.RV_MEANING AS SIFAT_CIDERA, "
				+ "PENJAMIN.NO_POLISI AS NOPOL_PENJAMIN, " //8
				+ "PENJAMIN.NAMA_PENGEMUDI AS NAMA_PENJAMIN, "//9
				+ "POSISI.NO_POLISI AS NOPOL_POSISI, " //10
				+ "POSISI.NAMA_PENGEMUDI AS NAMA_POSISI, " //11
				+ "STATUS_KORBAN.RV_MEANING AS STATUS_KORBAN, "
				+ "STATUS_NIKAH.RV_MEANING AS STATUS_NIKAH, "
				+ "PERTANGGUNGAN.DESKRIPSI AS PERTANGGUNGAN," //12
				+ "PERTANGGUNGAN.KODE_JAMINAN AS KODE_JAMINAN,"
				+ "CAMAT.KODE_PROVINSI, "
				+ "CAMAT.NAMA_PROVINSI, "
				+ "CAMAT.KODE_KABKOTA, "
				+ "CAMAT.NAMA_KABKOTA, "
				+ "CAMAT.KODE_CAMAT, "
				+ "CAMAT.NAMA_CAMAT, "
				+ "A.ID_KECELAKAAN, "
				+ "CIDERA.RV_HIGH_VALUE, "
				+ "A.KODE_SIFAT_CIDERA, "
				+ "PENJAMIN.NAMA_PENGEMUDI, "
				+ "PENJAMIN.NO_POLISI, "
				+ "A.JENIS_IDENTITAS, "
				+ "A.ID_ANGKUTAN_KECELAKAAN, "
				+ "A.ID_ANGKUTAN_PENANGGUNG "
				+ "FROM PL_KORBAN_KECELAKAAN A "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE JENIS IDENTITAS' AND FLAG_ENABLE = 'Y') IDENTITAS "
				+ "ON A.JENIS_IDENTITAS = IDENTITAS.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT KODE_LOKASI, KODE_CAMAT FROM PL_MAPPING_CAMAT) MAPPING_CAMAT "
				+ "ON A.ID_GUID = MAPPING_CAMAT.KODE_LOKASI "
				+ "LEFT JOIN (SELECT NAMA_CAMAT, NAMA_KABKOTA,NAMA_PROVINSI, KODE_CAMAT, KODE_KABKOTA, KODE_PROVINSI FROM FND_CAMAT) CAMAT "
				+ "ON MAPPING_CAMAT.KODE_CAMAT = CAMAT.KODE_CAMAT "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE JENIS PEKERJAAN' AND FLAG_ENABLE = 'Y') PEKERJAAN "
				+ "ON A.KODE_PEKERJAAN = PEKERJAAN.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING, RV_HIGH_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA' AND FLAG_ENABLE = 'Y') CIDERA "
				+ "ON A.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT ID_ANGKUTAN_KECELAKAAN, NO_POLISI, NAMA_PENGEMUDI FROM PL_ANGKUTAN_KECELAKAAN) PENJAMIN "
				+ "ON A.ID_ANGKUTAN_PENANGGUNG = PENJAMIN.ID_ANGKUTAN_KECELAKAAN "
				+ "LEFT JOIN (SELECT ID_ANGKUTAN_KECELAKAAN, NO_POLISI, NAMA_PENGEMUDI FROM PL_ANGKUTAN_KECELAKAAN) POSISI "
				+ "ON A.ID_ANGKUTAN_KECELAKAAN = POSISI.ID_ANGKUTAN_KECELAKAAN "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS KORBAN' AND FLAG_ENABLE='Y') STATUS_KORBAN "
				+ "ON A.KODE_STATUS_KORBAN = STATUS_KORBAN.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS NIKAH' AND FLAG_ENABLE = 'Y') STATUS_NIKAH "
				+ "ON A.STATUS_NIKAH = STATUS_NIKAH.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT KODE_JAMINAN, DESKRIPSI, LINGKUP_JAMINAN FROM PL_JAMINAN) PERTANGGUNGAN "
				+ "ON A.KODE_JAMINAN = PERTANGGUNGAN.KODE_JAMINAN "
				+ "WHERE "
				+ "A.ID_KORBAN_KECELAKAAN LIKE '"+idKorban+"' order by A.ID_KORBAN_KECELAKAAN ASC";
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(sql).getResultList();

		return list;
	}


	@Override
	public int deleteAngkutanByIdAngkutan(PlAngkutanKecelakaan idAngkutan) {
		String query = "DELETE FROM PL_ANGKUTAN_KECELAKAAN WHERE ID_ANGKUTAN_KECELAKAAN = ? ";
		
		em.createNativeQuery(query)
			.setParameter(1, idAngkutan.getIdAngkutanKecelakaan()).executeUpdate();

		return 1;
	}


	@Override
	public int deleteKorbanByIdKorban(PlKorbanKecelakaan idKorban) {
		String query = "DELETE FROM PL_KORBAN_KECELAKAAN WHERE ID_KORBAN_KECELAKAAN = ? ";
		
		em.createNativeQuery(query)
			.setParameter(1, idKorban.getIdKorbanKecelakaan()).executeUpdate();

		return 1;
	}
	
	@Override
	public int deleteKorbanByIdLaka(PlDataKecelakaan idLaka) {
		String query = "DELETE FROM PL_DATA_KECELAKAAN WHERE ID_KECELAKAAN = ? ";
		
		em.createNativeQuery(query)
			.setParameter(1, idLaka.getIdKecelakaan()).executeUpdate();

		return 1;
	}


	@Override
	public List<String> getKodeNoLaporan(String kodeInstansi) {
		String query = "select decode(sign(to_number(b.province_id) - 10) ,-1, '0'||b.province_id, b.province_id) || B.DISTRICT_NUMBER as ANGKA_TENGAH  "
				+ "from pl_mapping_polda a, korlantas_districts b where a.id_districts = b.id and a.kode_instansi like '"+kodeInstansi+"'";
		
		@SuppressWarnings("unchecked")
		List<String> list = em.createNativeQuery(query).getResultList();

		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getKorbanFromLaka(String idKec){
		String idLaka = "";
		if (idKec != null)
			idLaka = idKec;
		
		String q  = " SELECT A.NAMA || ' / ' || DECODE(A.JENIS_KELAMIN,'W', 'Perempuan',  "
				 + " 'P', 'Laki-laki',  "
				 + " 'TIDAK DIKETAHUI') || ' / ' || A.UMUR || ' TAHUN' AS NAMA, A.ALAMAT, B.RV_MEANING AS CIDERA, C.RV_MEANING AS STATUS, "
				 + " D.NO_POLISI AS KEND_KORBAN, E.NO_POLISI AS KEND_PENJAMMIN, F.DESKRIPSI AS JAMINAN, A.ID_KORBAN_KECELAKAAN "
				 + " FROM PL_KORBAN_KECELAKAAN A "
				 + " LEFT JOIN DASI_JR_REF_CODES B ON "
				 + " A.KODE_SIFAT_CIDERA = B.RV_LOW_VALUE "
				 + " AND B.RV_DOMAIN = 'KODE SIFAT CIDERA'  "
				 + " AND B.FLAG_ENABLE = 'Y' "
				 + " LEFT JOIN DASI_JR_REF_CODES C ON "
				 + " A.KODE_STATUS_KORBAN = C.RV_LOW_VALUE "
				 + " AND C.RV_DOMAIN = 'KODE STATUS KORBAN' "
				 + " AND C.FLAG_ENABLE = 'Y' "
				 + " LEFT JOIN PL_ANGKUTAN_KECELAKAAN D ON "
				 + " A.ID_ANGKUTAN_KECELAKAAN = D.ID_ANGKUTAN_KECELAKAAN "
				 + " LEFT JOIN PL_ANGKUTAN_KECELAKAAN E ON  "
				 + " A.ID_ANGKUTAN_PENANGGUNG = E.ID_ANGKUTAN_KECELAKAAN "
				 + " LEFT JOIN PL_JAMINAN F ON "
				 + " A.KODE_JAMINAN = F.KODE_JAMINAN  "
				 + " WHERE A.ID_KECELAKAAN like '" + idLaka +"' ";
		List<Object[]> list = em.createNativeQuery(q).getResultList();
		return list;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> reportDataLaka1(String idKec) {
		String q  = "SELECT "
				+ "B.NAMA as NAMA_LOKET, "
				+ "A.NAMA_PETUGAS, "
				+ "A.TGL_LAPORAN_POLISI, "
				+ "A.TGL_KEJADIAN, "
				+ "A.DESKRIPSI_LOKASI, "
				+ "C.RV_MEANING AS KASUS, "
				+ "A.DESKRIPSI_KECELAKAAN, "
				+ "A.CREATED_BY, "
				+ "A.LAST_UPDATED_BY, "
				+ "A.CREATION_DATE, "
				+ "A.LAST_UPDATED_DATE "
				+ "FROM PL_DATA_KECELAKAAN A "
				+ "LEFT JOIN FND_KANTOR_JASARAHARJA B ON A.ASAL_BERKAS = B.KODE_KANTOR_JR "
				+ "LEFT JOIN DASI_JR_REF_CODES C ON A.KODE_KASUS_KECELAKAAN = C.RV_LOW_VALUE "
				+ "WHERE A.ID_KECELAKAAN like '"+idKec+"' "
				+ "AND C.RV_DOMAIN = 'PL KASUS KECELAKAAN' "; 

		List<Object[]> list = em.createNativeQuery(q).getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> reportDataLaka2(String idKec) {
		String q  = "SELECT "
				+ "A.NO_POLISI, "
				+ "A.KODE_JENIS, "
				+ "A.NAMA_PENGEMUDI, "
				+ "A.NAMA_PEMILIK, "
				+ "STS.RV_MEANING AS STATUS "
				+ "FROM PL_ANGKUTAN_KECELAKAAN A "
				+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES B WHERE RV_DOMAIN = 'KODE STATUS KENDARAAN') STS "
				+ "ON A.STATUS_KENDARAAN = STS.RV_LOW_VALUE "
				+ "WHERE ID_KECELAKAAN LIKE '"+idKec+"'"; 
		
		List<Object[]> list = em.createNativeQuery(q).getResultList();
		return list;

	}


	@Override
	public List<Object[]> cekData(String tglKejadian,
			String tglLaporan, String kodeInstansi) {
		
		String kejadian = "";
		if(tglKejadian==null||tglKejadian == ""){
			kejadian = "";
		}else{
			kejadian = "A.TGL_KEJADIAN LIKE TO_DATE('"+tglKejadian+"','DD/MM/YYYY') AND ";
		}
		
		String sql = "SELECT A.ID_KECELAKAAN, A.KODE_KANTOR_JR FROM PL_DATA_KECELAKAAN A "
				+ "WHERE "
				+ kejadian
				+ "A.TGL_LAPORAN_POLISI LIKE TO_DATE('"+tglLaporan+"','DD/MM/YYYY') "
				+ "AND A.KODE_INSTANSI LIKE '"+kodeInstansi+"'"; 
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(sql).getResultList();
		return list;
	}

}
