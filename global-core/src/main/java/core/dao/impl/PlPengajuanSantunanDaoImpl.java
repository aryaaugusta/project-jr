package core.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import common.util.StringUtil;
import core.dao.PlPengajuanSantunanCustomDao;
import core.model.PlPengajuanSantunan;

public class PlPengajuanSantunanDaoImpl implements PlPengajuanSantunanCustomDao {

	@PersistenceContext
	private EntityManager em;

	/*
	 * (non-Javadoc)
	 * 
	 * @see core.dao.PlPengajuanSantunanCustomDao#getListIndex(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see core.dao.PlPengajuanSantunanCustomDao#getListIndex(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see core.dao.PlPengajuanSantunanCustomDao#getListIndex(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see core.dao.PlPengajuanSantunanCustomDao#getListIndex(java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	
	@Override
	public List<Object[]> getListIndex(String diajukanDi, String dilimpahkanKe,
			String namaKorban, String pengajuanDay, String pengajuanMonth,
			String pengajuanYear, String lakaDay, String lakaMonth,
			String lakaYear, String kodeInstansi, String noLaporan,
			String noBerkas, String statusProses, String penyelesaian,
			String namaPemohon, String kodeRs, String search) {
		
		String pilih="";
		if(dilimpahkanKe.contains("Pelimpahan")){
			pilih="AND INSIDE.DILIMPAHKAN_KE IS NOT NULL ";
		}else if(dilimpahkanKe.contains("Berkas")){
			pilih="AND INSIDE.DILIMPAHKAN_KE IS NULL ";			
		}
		
		String tglPenerimaan="";
		if(!pengajuanDay.equalsIgnoreCase("%%%%") ||!pengajuanMonth.equalsIgnoreCase("%%%%")||!pengajuanYear.equalsIgnoreCase("%%%%")){
			tglPenerimaan="TO_CHAR(INSIDE.TGL_PENERIMAAN,'DD') LIKE '"+pengajuanDay+ "' AND "
					+ "TO_CHAR(INSIDE.TGL_PENERIMAAN,'MM') LIKE '"+pengajuanMonth+"' AND " 
					+ "TO_CHAR(INSIDE.TGL_PENERIMAAN,'YYYY') LIKE '"+pengajuanYear+ "' AND ";
		}
		

		String query = "SELECT * FROM "
				+ "(SELECT "
				// + "--LIST INDEX-- "
				+ "A.DIAJUKAN_DI, "
				+ "A.NO_BERKAS, "
				+ "A.TGL_PENERIMAAN, "
				+ "NVL(NAMA.NAMA,'-') AS NAMA, "
				+ "NVL(CIDERA.RV_HIGH_VALUE,'-') AS KODE_CIDERA, "
				+ "A.NAMA_PEMOHON, "
				+ "NVL(STATUS_PROSES.RV_MEANING, '-') AS STATUS_PROSES, "
				+ "NVL(PENYELESAIAN.RV_MEANING,'-') AS PENYELESAIAN, "
				+ "A.ID_KECELAKAAN, "
				+ "A.ID_KORBAN_KECELAKAAN, "
				// + "--FILTER-- "
				+ "DATA_LAKA.TGL_KEJADIAN, "
				+ "NVL(INSTANSI.KODE_INSTANSI,'-') AS KODE_INSTANSI, "
				+ "NVL(INSTANSI.DESKRIPSI,'-') AS INSTANSI, "
				+ "NVL(DATA_LAKA.NO_LAPORAN_POLISI,'-') AS NO_LAPORAN_POLISI, "
				+ "A.STATUS_PROSES AS STATUS_LOW_VALUE, "
				+ "NVL(A.OTORISASI_FLAG,'-') AS STATUS_PENYELESAIAN, "
				+ "A.DILIMPAHKAN_KE, "
				+ "A.STATUS_PROSES AS STS_PROSES, "
				+ "A.SEQ_NO, "
				+ "A.NO_PENGAJUAN "
				// + "--NVL(SIFAT_LAKA.RV_MEANING,'-') AS SIFAT_LAKA, "
				// + "--NVL(KORB.JML_KORBAN,'0'), "
				// + "--A.STATUS_LAPORAN_POLISI, "
				// + "--A.ID_KECELAKAAN, "
				// + "--A.ASAL_BERKAS, "
				// + "--A.TGL_LAPORAN_POLISI, "
				// + "--A.KODE_INSTANSI, "
				// + "--NVL(NAMA.NO_IDENTITAS,'-') AS NO_IDENTITAS, "
				// + "--NVL(NAMA.KODE_SIFAT_CIDERA,'-') AS KODE_SIFAT_CIDERA, "
				// + "--NVL(JAMINAN.LINGKUP_JAMINAN,'_') AS LINGKUP_JAMINAN, "
				// + "--NVL(JAMINAN.DESKRIPSI, '-') AS DESKRIP_JAMINAN, "
				// + "--NVL(NAMA.KODE_JAMINAN,'-') AS KODE_JAMINAN, "
				// + "--A.SIFAT_KECELAKAAN, "
				// +
				// "--NVL(NAMA.ID_KORBAN_KECELAKAAN,'-') AS ID_KORBAN_KECELAKAAN "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN ( SELECT KODE_INSTANSI, ID_KECELAKAAN, TGL_KEJADIAN, NO_LAPORAN_POLISI "
				+ "FROM PL_DATA_KECELAKAAN ) DATA_LAKA "
				+ "ON A.ID_KECELAKAAN = DATA_LAKA.ID_KECELAKAAN "
				+ "LEFT JOIN ( SELECT NAMA, ID_KORBAN_KECELAKAAN, KODE_SIFAT_CIDERA "
				+ "FROM PL_KORBAN_KECELAKAAN ) NAMA "
				+ "ON A.ID_KORBAN_KECELAKAAN = NAMA.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN ( SELECT KODE_INSTANSI, DESKRIPSI "
				+ "FROM PL_INSTANSI ) INSTANSI "
				+ "ON DATA_LAKA.KODE_INSTANSI = INSTANSI.KODE_INSTANSI "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_HIGH_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA' ) CIDERA "
				+ "ON NAMA.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES "
				+ "WHERE RV_DOMAIN = 'KODE STATUS PROSES' ) STATUS_PROSES "
				+ "ON A.STATUS_PROSES = STATUS_PROSES.RV_LOW_VALUE "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE OTORISASI FLAG' ) PENYELESAIAN "
				+ "ON A.OTORISASI_FLAG = PENYELESAIAN.RV_LOW_VALUE ) INSIDE "
				+ "WHERE INSIDE.DIAJUKAN_DI LIKE '"+diajukanDi+ "' AND "
				+ "INSIDE.NAMA LIKE '"+namaKorban+ "' AND "
				+tglPenerimaan
				+ "TO_CHAR(INSIDE.TGL_KEJADIAN,'DD') LIKE '"+lakaDay+ "' "
				+ "AND TO_CHAR(INSIDE.TGL_KEJADIAN,'MM') LIKE '"+ lakaMonth+ "' "
				+ "AND TO_CHAR(INSIDE.TGL_KEJADIAN,'YYYY') LIKE '"+ lakaYear+ "' "
				+ "AND INSIDE.KODE_INSTANSI LIKE '"+kodeInstansi+ "' "
				+ "AND INSIDE.NO_LAPORAN_POLISI LIKE '"+noLaporan+ "' "
				+ "AND INSIDE.NO_BERKAS LIKE '"+ noBerkas+ "' "
				+ "AND INSIDE.STATUS_LOW_VALUE LIKE '"+statusProses+ "' "
				+ "AND INSIDE.STATUS_PENYELESAIAN LIKE '"+ penyelesaian+ "' "
				+ "AND INSIDE.NAMA_PEMOHON LIKE '"+ namaPemohon+ "'"
				+ pilih 
				// + "--AND INSIDE.KODE_RUMAHSAKIT LIKE '%%%%' "
				+ "AND( INSIDE.DIAJUKAN_DI LIKE '"+ search+ "' "
				+ "OR INSIDE.NO_BERKAS LIKE '"+search+ "' OR "
				+ "INSIDE.NAMA LIKE '"+search+"' "
				+ "OR INSIDE.KODE_CIDERA LIKE '"+search+"' "
				+ "OR INSIDE.NAMA_PEMOHON LIKE '"+search+"' "
				+ "OR INSIDE.STATUS_PROSES LIKE '"+search+"' "
				+ "OR INSIDE.PENYELESAIAN LIKE '" + search + "' ) "
				+ "AND INSIDE.SEQ_NO = (SELECT MAX(SEQ_NO) FROM PL_PENGAJUAN_SANTUNAN WHERE NO_PENGAJUAN = INSIDE.NO_PENGAJUAN)";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	@Override
	public List<Object[]> findByIdKorban(String idKorbanKecelakaan) {
		String query = "SELECT A.ID_KORBAN_KECELAKAAN , NAMA_KORBAN.NAMA AS NAMA, DATA_LAKA.TGL_KEJADIAN AS TGL_KECELAKAAN, "
				+ "INSTANSI.DESKRIPSI AS INSTANSI, RS.DESKRIPSI, CIDERA2.CIDERA, "
				+ "STSPROSES.STATUS_PROSES AS STS_PRS, PENYELESAIAN.RV_MEANING, "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN (SELECT NAMA,ID_KORBAN_KECELAKAAN ,KODE_SIFAT_CIDERA "
				+ "FROM PL_KORBAN_KECELAKAAN) NAMA_KORBAN "
				+ "ON A.ID_KORBAN_KECELAKAAN = NAMA_KORBAN.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN (SELECT ID_KECELAKAAN, TGL_KEJADIAN, KODE_INSTANSI "
				+ "FROM PL_DATA_KECELAKAAN) DATA_LAKA "
				+ "ON A.ID_KECELAKAAN = DATA_LAKA.ID_KECELAKAAN "
				+ "LEFT JOIN (SELECT KODE_INSTANSI, DESKRIPSI "
				+ "FROM PL_INSTANSI) INSTANSI "
				+ "ON DATA_LAKA.KODE_INSTANSI = INSTANSI.KODE_INSTANSI "
				+ "LEFT JOIN (SELECT RV_HIGH_VALUE AS CIDERA, RV_LOW_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES "
				+ "WHERE RV_DOMAIN = 'KODE SIFAT CIDERA') CIDERA2 "
				+ "ON NAMA_KORBAN.KODE_SIFAT_CIDERA = CIDERA2.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING AS STATUS_PROSES "
				+ "FROM DASI_JR_REF_CODES "
				+ "WHERE RV_DOMAIN = 'KODE STATUS PROSES') STSPROSES "
				+ "ON A.STATUS_PROSES = STSPROSES.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES "
				+ "WHERE RV_DOMAIN = 'KODE OTORISASI FLAG') PENYELESAIAN "
				+ "ON A.OTORISASI_FLAG = PENYELESAIAN.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT NO_BERKAS, KODE_RUMAHSAKIT "
				+ "FROM PL_PENGAJUAN_RS) PENGAJUAN_RS "
				+ "ON A.NO_BERKAS = PENGAJUAN_RS.NO_BERKAS "
				+ "LEFT JOIN (SELECT KODE_RUMAHSAKIT, DESKRIPSI "
				+ "FROM PL_RUMAH_SAKIT) RS "
				+ "ON PENGAJUAN_RS.KODE_RUMAHSAKIT = RS.KODE_RUMAHSAKIT "
				+ "WHERE A.DIAJUKAN_DI = '0200001' AND "
				+ "A.ID_KORBAN_KECELAKAAN = '" + idKorbanKecelakaan + "'";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	public List<Object[]> findById2(String idKecelakaan) {

		String query = "SELECT "
				+ "A.NO_LAPORAN_POLISI,"
				+ "A.TGL_KEJADIAN,"
				+ "B.NAMA AS NAMAKORBAN,"
				+ "D.DESKRIPSI,"
				+ "A.STATUS_LAPORAN_POLISI,"
				+ "B.UMUR,"
				+ "B.JENIS_KELAMIN,"
				+ "E.RV_MEANING AS CIDERA,"
				+ "G.NO_POLISI,"
				+ "G.NAMA_PENGEMUDI,"
				+ "H.RV_MEANING AS KASUS,"
				+ "A.DESKRIPSI_KECELAKAAN,"
				+ "F.RV_MEANING AS STATUS,"
				+ "C.NAMA AS NAMAINSTANSI, "
				+ "A.ID_KECELAKAAN, "
				+ "A.DESKRIPSI_LOKASI, "
				+ "B.NO_IDENTITAS, "
				+ "B.JENIS_IDENTITAS, "
				+ "B.ALAMAT, "
				+ "B.STATUS_NIKAH, "
				+ "B.NO_TELP, "
				+ "A.KODE_KANTOR_JR, "
				+ "B.ID_KORBAN_KECELAKAAN, "
				+ "B.KODE_SIFAT_CIDERA, "
				+ "B.KODE_JAMINAN, "
				+ "D.lINGKUP_JAMINAN "
				+ "FROM "
				+ "PL_DATA_KECELAKAAN A, PL_KORBAN_KECELAKAAN B,FND_KANTOR_JASARAHARJA C,PL_JAMINAN D,"
				+ "DASI_JR_REF_CODES E,DASI_JR_REF_CODES F,PL_ANGKUTAN_KECELAKAAN G,DASI_JR_REF_CODES H "
				+ "WHERE " + "A.ID_KECELAKAAN = B.ID_KECELAKAAN "
				+ "AND A.KODE_KANTOR_JR = C.KODE_KANTOR_JR "
				+ "AND B.KODE_JAMINAN = D.KODE_JAMINAN "
				+ "AND B.KODE_SIFAT_CIDERA = E.RV_LOW_VALUE "
				+ "AND B.KODE_STATUS_KORBAN = F.RV_LOW_VALUE "
				+ "AND A.ID_KECELAKAAN = G.ID_KECELAKAAN "
				+ "AND A.KODE_KASUS_KECELAKAAN = H.RV_LOW_VALUE "
				+ "AND E.RV_DOMAIN = 'KODE SIFAT CIDERA' "
				+ "AND F.RV_DOMAIN = 'KODE STATUS KORBAN' "
				+ "AND H.RV_DOMAIN = 'PL KASUS KECELAKAAN' AND "
				+ "B.ID_KORBAN_KECELAKAAN LIKE '" + idKecelakaan + "'";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;

	}

	@Override
	public List<Object> findMaxId(String noBerkas) {
		String query = "SELECT MAX(A.NO_BERKAS) FROM PL_PENGAJUAN_SANTUNAN A WHERE A.NO_BERKAS LIKE '"
				+ noBerkas + "'";

		@SuppressWarnings("unchecked")
		List<Object> data = em.createNativeQuery(query).getResultList();
		return data;
	}

	@Override
	public List<Object[]> findByNoPengajuan(String noPengajuan) {
		String query = "SELECT  A.NO_BERKAS, "
				+ "A.DIAJUKAN_DI, "
				+ "A.TGL_PENGAJUAN, "
				+ "A.TGL_PENYELESAIAN, "
				+ "STS_PROSES.RV_MEANING AS STATUS_PROSES, "
				+ "PENYELESAIAN.RV_MEANING AS PENYELESAIAN, "
				+ "KANTOR.NAMA AS NAMA_KANTOR, "
				+ "A.DILIMPAHKAN_KE "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN(SELECT RV_LOW_VALUE,RV_MEANING "
				+ "FROM DASI_JR_REF_CODES "
				+ "WHERE RV_DOMAIN ='KODE STATUS PROSES') STS_PROSES "
				+ "ON A.STATUS_PROSES=STS_PROSES.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES "
				+ "WHERE RV_DOMAIN = 'KODE OTORISASI FLAG') PENYELESAIAN "
				+ "ON A.OTORISASI_FLAG = PENYELESAIAN.RV_LOW_VALUE "
				+ "LEFT JOIN(SELECT NAMA,KODE_KANTOR_JR "
				+ "FROM FND_KANTOR_JASARAHARJA) KANTOR "
				+ "ON KANTOR.KODE_KANTOR_JR = A.DIAJUKAN_DI "
				+ "WHERE A.NO_PENGAJUAN LIKE '"+noPengajuan+"'"; 
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	@Override
	public List<Object[]> findForPenyelesaianPengajuan(String tglPengajuan,
			String noBerkas, String search, String jenisPengajuan, String dateFlag) {
//		String query = "SELECT A.NO_BERKAS, "
//				+ "D.RV_MEANING AS STATUSPROSES, "
//				+ "A.TGL_PENGAJUAN, "
//				+ "C.NAMA, "
//				+ "C.ALAMAT "
//				+ "FROM PL_PENGAJUAN_SANTUNAN A ,"
//				+ "PL_DATA_KECELAKAAN B, "
//				+ "PL_KORBAN_KECELAKAAN C, "
//				+ "DASI_JR_REF_CODES D "
//				+ "WHERE A.ID_KECELAKAAN = B.ID_KECELAKAAN "
//				+ "AND A.ID_KORBAN_KECELAKAAN = C.ID_KORBAN_KECELAKAAN "
//				+ "AND A.STATUS_PROSES = D.RV_LOW_VALUE "
//				+ "AND D.RV_DOMAIN = 'KODE STATUS PROSES' "
//				+ "AND D.FLAG_ENABLE = 'Y' "
//				+ "AND TRUNC(A.TGL_PENGAJUAN) LIKE TRUNC(TO_DATE('"+tglPengajuan+"', 'DD/MM/YYYY')) "
//				+ "AND UPPER(A.NO_BERKAS) LIKE UPPER('"+noBerkas+"') ";
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
		Calendar c=new GregorianCalendar();
		c.add(Calendar.DATE, -180);
		Date d = c.getTime();
		String dateawal = sdf.format(d);
		String datenow = sdf.format(new Date());
		String query2 = " AND D.RV_LOW_VALUE IN "
		        + "(SELECT AA.RV_LOW_VALUE "
		        + "FROM JR_USER.DASI_JR_REF_CODES AA WHERE "
		        + "AA.RV_DOMAIN = 'KODE STATUS PROSES' "
		        + "AND AA.FLAG_ENABLE = 'Y' "
		        + "AND AA.RV_ABBREVIATION = 'O')";
		String query3 = "AND TRUNC(PLS.TGL_PENGAJUAN) LIKE TRUNC(TO_DATE('"+tglPengajuan+"', 'DD/MM/YYYY')) ";
		
		String query1 = "SELECT PLS.NO_BERKAS, "
				+ "PKK.NAMA AS NAMA_KORBAN, "
				+ "PLS.CIDERA_KORBAN, "
				+ "PLS.KODE_PENGAJUAN, "
			   + "TO_CHAR(NVL(PLS.TGL_PENYELESAIAN, SYSDATE), 'DD/MM/YYYY HH24:MI') AS TGL_PENYELESAIAN, "
			   + "DK.STATUS_LAPORAN_POLISI, "
               + "TO_CHAR(DK.TGL_KEJADIAN, 'DD/MM/YYYY HH24:MI') AS TGL_KEJADIAN, "
               + "PLS.KESIMPULAN_SEMENTARA, "
               + "NVL((SELECT RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN='KODE KESIMPULAN SEMENTARA' AND FLAG_ENABLE='Y' AND RV_LOW_VALUE = PLS.KESIMPULAN_SEMENTARA), 'N/A') AS KESIMPULAN_SEMENTARA_DESC, "   
               + "PLS.JAMINAN_PEMBAYARAN, "
               + "NVL((SELECT RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN='KODE JAMINAN PEMBAYARAN' AND FLAG_ENABLE='Y' AND RV_LOW_VALUE = PLS.JAMINAN_PEMBAYARAN), 'N/A') AS JAMINAN_PEMBAYARAN_DESC, "   
               + "PLS.OTORISASI_FLAG, "
               + "NVL((SELECT RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN='KODE OTORISASI FLAG' AND FLAG_ENABLE='Y' AND RV_LOW_VALUE = PLS.OTORISASI_FLAG), 'N/A') AS OTORISASI_DESC, "          
               + "PLS.STATUS_PROSES, "
               + "D.RV_HIGH_VALUE AS URUT_PROSES, "
               + "NVL(D.RV_MEANING, 'N/A') AS STATUS_PROSES_DESC, "
               + "K.KODE_KANTOR_JR, "
               + "K.NAMA, "
               + "DH.RV_HIGH_VALUE AS HUB_KORBAN, "
               + "JENIS_PEMBAYARAN, "
               + "TGL_PROSES, " 
               + "NVL(JUMLAH_DIBAYAR_LUKALUKA, 0) AS JUMLAH_DIBAYAR_LUKALUKA, " 
               + "NVL(JML_BYR_AMBL, 0) AS JML_BYR_AMBL, "
               + "NVL(JML_BYR_P3K, 0) AS JML_BYR_P3K, "  
               + "NO_BPK, "
               + "TGL_PEMBUATAN_BPK, "
               + "NO_SURAT_PENYELESAIAN, "
               + "NVL(DECODE(JENIS_PEMBAYARAN, 'T', DECODE(Y.ID_GUID, NULL, 'BRINIDJA', Y.ID_GUID)), 'BRINIDJA') AS KODE_BANK, "
               + "NO_REKENING, "
               + "NAMA_REKENING, "
               + "NVL(AD.JNS_REKENING, 'B') AS JNS_REKENING, "
               + "(MRS.DESKRIPSI || ' - ' || DH.RV_MEANING) AS PEMOHON, "
               + "PRS.KODE_RUMAHSAKIT AS KODE_RS, "
               + "AD.ID_REK_RS, "
               + "PKK.ALAMAT, "
               + "TGL_PENGAJUAN, "
               + "NVL(JUMLAH_DIBAYAR_MENINGGAL, 0) AS JUMLAH_DIBAYAR_MENINGGAL, "
               + "DK.DESKRIPSI_LOKASI, "
               + "PLS.DIAJUKAN_DI "


               + "FROM PL_PENGAJUAN_SANTUNAN PLS "
               + "JOIN PL_PENYELESAIAN_SANTUNAN Y ON Y.NO_BERKAS = PLS.NO_BERKAS "
               + "JOIN PL_DATA_KECELAKAAN DK ON DK.ID_KECELAKAAN = PLS.ID_KECELAKAAN "
               + "JOIN PL_KORBAN_KECELAKAAN PKK ON PKK.ID_KORBAN_KECELAKAAN = PLS.ID_KORBAN_KECELAKAAN "
               + "JOIN DASI_JR_REF_CODES D ON RV_DOMAIN='KODE STATUS PROSES' AND D.RV_LOW_VALUE = PLS.STATUS_PROSES " 
               + "JOIN DASI_JR_REF_CODES DH ON DH.RV_DOMAIN = 'KODE HUBUNGAN KORBAN' AND DH.RV_LOW_VALUE = PLS.KODE_HUBUNGAN_KORBAN " 
               + "LEFT JOIN PL_PENGAJUAN_RS PRS ON PRS.NO_BERKAS = PLS.NO_BERKAS "
               + "LEFT JOIN PL_RUMAH_SAKIT MRS ON MRS.KODE_RUMAHSAKIT = PRS.KODE_RUMAHSAKIT "
               + "LEFT JOIN FND_KANTOR_JASARAHARJA K ON PLS.DILIMPAHKAN_KE = K.KODE_KANTOR_JR "  
               + "LEFT JOIN PL_ADDITIONAL_DESC AD ON AD.NO_BERKAS = PLS.NO_BERKAS " 
               + "WHERE (PLS.TRANSISI_FLAG = 'N' OR PLS.TRANSISI_FLAG IS NULL) "
               + "AND PLS.NO_BERKAS LIKE '"+noBerkas+"' "
               + "AND (UPPER(PLS.NO_BERKAS) LIKE UPPER('"+search+"') "
				+ "OR UPPER(PKK.NAMA) LIKE UPPER('"+search+"') "
				+ "OR UPPER(D.RV_MEANING) LIKE UPPER('"+search+"') "
				+ "OR UPPER(PKK.ALAMAT) LIKE UPPER('"+search+"') "
				+ "OR TO_CHAR(PLS.TGL_PENGAJUAN,'DD/MM/YYYY') LIKE '"+search+"' ) ";
		
		String query = "";
		if (jenisPengajuan.equalsIgnoreCase("O") && dateFlag.equalsIgnoreCase("Y")) {
			query = query1+query3+query2;
		} else if (jenisPengajuan.equalsIgnoreCase("O") && dateFlag.equalsIgnoreCase("N")) {
			query = query1+query2;
		} else if (jenisPengajuan.equalsIgnoreCase("") && dateFlag.equalsIgnoreCase("Y")) {
			query = query1+query3;
		} else if (jenisPengajuan.equalsIgnoreCase("") && dateFlag.equalsIgnoreCase("N")) {
			query = query1;
		}
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
		
	}

	@Override
	public List<Object[]> findForPenyelesaianPengajuanDetail(String tglPengajuan,
			String noBerkas, String search, String jenisPengajuan, String dateFlag) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
		Calendar c=new GregorianCalendar();
		c.add(Calendar.DATE, -180);
		Date d = c.getTime();
		String dateawal = sdf.format(d);
		String datenow = sdf.format(new Date());
		String query2 = " AND D.RV_LOW_VALUE IN "
		        + "(SELECT AA.RV_LOW_VALUE "
		        + "FROM JR_USER.DASI_JR_REF_CODES AA WHERE "
		        + "AA.RV_DOMAIN = 'KODE STATUS PROSES' "
		        + "AND AA.FLAG_ENABLE = 'Y' "
		        + "AND AA.RV_ABBREVIATION = 'O')";
		String query3 = "AND TRUNC(A.TGL_PENGAJUAN) LIKE TRUNC(TO_DATE('"+tglPengajuan+"', 'DD/MM/YYYY')) ";

		String query1 = "SELECT A.NO_BERKAS, "
				+ "C.NAMA AS NAMAKORBAN, "
				+ "D.RV_MEANING AS STATUSPROSES, "
				+ "H.RV_MEANING AS PENYELESAIAN, "
				+ "F.RV_MEANING AS KESIMPULAN, "
				+ "G.RV_MEANING AS PEMBAYARAN, "
				+ "A.TGL_PENGAJUAN, "
				+ "A.JUMLAH_PENGAJUAN_MENINGGAL, "
				+ "A.JUMLAH_PENGAJUAN_LUKALUKA, "
				+ "A.JUMLAH_PENGAJUAN_PENGUBURAN, "
				+ "A.JML_PENGAJUAN_AMBL, "
				+ "A.JML_PENGAJUAN_P3K, "
				+ "C.ALAMAT AS ALAMATKORBAN, "
				+ "A.CIDERA_KORBAN, "
				+ "A.TGL_PENYELESAIAN, "
				+ "A.DILIMPAHKAN_KE, "
				+ "A.STATUS_PROSES, "
				+ "A.OTORISASI_FLAG, "
				+ "A.KESIMPULAN_SEMENTARA, "
				+ "A.JAMINAN_PEMBAYARAN, "
				+ "I.NAMA AS KANTOR_DILIMPAHKAN, "
				+ "J.DESKRIPSI AS JAMINANDESC, "
				+ "B.TGL_KEJADIAN, "
				+ "B.DESKRIPSI_LOKASI, "
				+ "K.NO_POLISI, "
				+ "L.DESKRIPSI AS JENIS_KENDARAAN, "
				+ "C.UMUR AS UMURKORBAN, "
				+ "A.NAMA_PEMOHON, "
				+ "A.ALAMAT_PEMOHON AS ALAMATPEMOHON, "
				+ "M.RV_MEANING AS HUBUNGANKORBAN, "
				+ "N.RV_MEANING AS CIDERADESC, "
				+ "O.DESKRIPSI AS NAMAINSTANSI, "
				+ "B.TGL_LAPORAN_POLISI, "
				+ "B.NO_LAPORAN_POLISI, "
				+ "P.RV_MEANING AS KASUSKECELAKAAN, "
				+ "Q.RV_MEANING AS STATUSKORBAN, "
				+ "R.RV_MEANING AS SIFATKECELAKAAN, "
				+ "S.RV_MEANING AS GOLONGANKENDARAAN, "
				+ "A.NO_IDENTITAS AS IDENTITASPEMOHON, "
				+ "A.ID_GUID AS TELEPON, "
				+ "C.NO_IDENTITAS AS IDENTITASKORBAN, "
				+ "C.NO_TELP AS NOTELPONKORBAN, "
				+ "T.KODE_RUMAHSAKIT, "
				+ "T.JUMLAH_PENGAJUAN_LUKALUKA AS LUKA2RS, "
				+ "U.DESKRIPSI AS NAMARS, "
				+ "C.ID_KORBAN_KECELAKAAN, "
				+ "U.ALAMAT "				
				+ "FROM "
				+ "PL_PENGAJUAN_SANTUNAN A , "
				+ "PL_DATA_KECELAKAAN B, "
				+ "PL_KORBAN_KECELAKAAN C, "
				+ "DASI_JR_REF_CODES D, "
				+ "DASI_JR_REF_CODES F, "
				+ "DASI_JR_REF_CODES G, "
				+ "DASI_JR_REF_CODES H, "
				+ "FND_KANTOR_JASARAHARJA I, "
				+ "PL_JAMINAN J, "
				+ "PL_ANGKUTAN_KECELAKAAN K, "
				+ "FND_JENIS_KENDARAAN L, "
				+ "DASI_JR_REF_CODES M, "
				+ "DASI_JR_REF_CODES N, "
				+ "PL_INSTANSI O, "
				+ "DASI_JR_REF_CODES P, "
				+ "DASI_JR_REF_CODES Q, "
				+ "DASI_JR_REF_CODES R, "
				+ "DASI_JR_REF_CODES S, "
				+ "PL_PENGAJUAN_RS T, "
				+ "PL_RUMAH_SAKIT U "
				+ "WHERE A.ID_KECELAKAAN = B.ID_KECELAKAAN "
				+ "AND A.ID_KORBAN_KECELAKAAN = C.ID_KORBAN_KECELAKAAN "
				+ "AND A.STATUS_PROSES = D.RV_LOW_VALUE "
				+ "AND D.RV_DOMAIN = 'KODE STATUS PROSES' "
				+ "AND D.FLAG_ENABLE = 'Y' "
				+ "AND A.KESIMPULAN_SEMENTARA = F.RV_LOW_VALUE "
				+ "AND F.RV_DOMAIN = 'KODE KESIMPULAN SEMENTARA' "
				+ "AND F.FLAG_ENABLE = 'Y' "
				+ "AND A.JAMINAN_PEMBAYARAN = G.RV_LOW_VALUE "
				+ "AND G.RV_DOMAIN = 'KODE JAMINAN PEMBAYARAN' "
				+ "AND G.FLAG_ENABLE = 'Y' "
				+ "AND A.OTORISASI_FLAG = H.RV_LOW_VALUE "
				+ "AND H.RV_DOMAIN = 'KODE OTORISASI FLAG' "
				+ "AND H.FLAG_ENABLE = 'Y' "
				+ "AND A.DIAJUKAN_DI = I.KODE_KANTOR_JR "
				+ "AND A.KODE_JAMINAN = J.KODE_JAMINAN "
				+ "AND C.ID_ANGKUTAN_KECELAKAAN = K.ID_ANGKUTAN_KECELAKAAN "
				+ "AND K.KODE_JENIS = L.KODE_JENIS "
				+ "AND A.KODE_HUBUNGAN_KORBAN = M.RV_LOW_VALUE "
				+ "AND M.RV_DOMAIN = 'KODE HUBUNGAN KORBAN' "
				+ "AND M.FLAG_ENABLE = 'Y' "
				+ "AND C.KODE_SIFAT_CIDERA = N.RV_LOW_VALUE "
				+ "AND N.RV_DOMAIN = 'KODE SIFAT CIDERA' "
				+ "AND N.FLAG_ENABLE = 'Y' "
				+ "AND B.KODE_INSTANSI = O.KODE_INSTANSI "
				+ "AND B.KODE_KASUS_KECELAKAAN = P.RV_LOW_VALUE "
				+ "AND P.RV_DOMAIN = 'PL KASUS KECELAKAAN' "
				+ "AND P.FLAG_ENABLE = 'Y' "
				+ "AND C.KODE_STATUS_KORBAN = Q.RV_LOW_VALUE "
				+ "AND Q.RV_DOMAIN = 'KODE STATUS KORBAN' "
				+ "AND Q.FLAG_ENABLE = 'Y' "
				+ "AND B.SIFAT_KECELAKAAN = R.RV_LOW_VALUE "
				+ "AND R.RV_DOMAIN = 'KODE SIFAT KECELAKAAN' "
				+ "AND R.FLAG_ENABLE = 'Y' "
				+ "AND K.KODE_GOLONGAN = S.RV_LOW_VALUE "
				+ "AND S.RV_DOMAIN = 'KODE GOLONGAN' "
				+ "AND S.FLAG_ENABLE = 'Y' "
				+ "AND A.NO_BERKAS = T.NO_BERKAS "
				+ "AND T.KODE_RUMAHSAKIT = U.KODE_RUMAHSAKIT "
				+ "AND UPPER(A.NO_BERKAS) LIKE UPPER('"+noBerkas+"') "
				+ "AND (TRUNC(A.TGL_PENGAJUAN) >= TRUNC(TO_DATE('"+dateawal+"', 'DD/MM/YYYY')) "
				+ "AND TRUNC(A.TGL_PENGAJUAN) <= TRUNC(TO_DATE('"+datenow+"', 'DD/MM/YYYY'))) "
				+ "AND (UPPER(A.NO_BERKAS) LIKE UPPER('"+search+"') "
				+ "OR UPPER(C.NAMA) LIKE UPPER('"+search+"') "
				+ "OR UPPER(D.RV_MEANING) LIKE UPPER('"+search+"') "
				+ "OR UPPER(C.ALAMAT) LIKE UPPER('"+search+"') "
				+ "OR TO_CHAR(A.TGL_PENGAJUAN,'DD/MM/YYYY') LIKE '"+search+"' ) ";
		
		String query = "";
		if (jenisPengajuan.equalsIgnoreCase("O") && dateFlag.equalsIgnoreCase("Y")) {
			query = query1+query3+query2;
		} else if (jenisPengajuan.equalsIgnoreCase("O") && dateFlag.equalsIgnoreCase("N")) {
			query = query1+query2;
		} else if (jenisPengajuan.equalsIgnoreCase("") && dateFlag.equalsIgnoreCase("Y")) {
			query = query1+query3;
		} else if (jenisPengajuan.equalsIgnoreCase("") && dateFlag.equalsIgnoreCase("N")) {
			query = query1;
		}
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}
	
	
	
	@Override
	public List<Object[]> getIndexOtorisasi(String pilihPengajuan, String tglPenerimaan,
			String noBerkas, String search) {

		String tgl = "";
		if(tglPenerimaan!=null && !tglPenerimaan.equalsIgnoreCase("%")){
			tgl="AND INSIDE.TGL_PENERIMAAN LIKE TO_DATE('" + tglPenerimaan+"', 'DD/MM/YYYY') ";			
		}
		
		String pilih="";
		if(pilihPengajuan!=null && pilihPengajuan.contains("BA")){
			pilih="AND INSIDE.STATUS_PROSES_LOW='BA' ";
		}
		
		
		String query = "SELECT * FROM "
				+ "(SELECT "
				+ "A.ID_KECELAKAAN, "
				+ "A.NO_BERKAS, "
				+ "NVL(STATUS_PROSES.RV_MEANING,'-') AS STATUS_PROSES, "
				+ "A.TGL_PENERIMAAN, "
				+ "NVL(NAMA.NAMA,'-') "
				+ "AS NAMA, NVL(NAMA.ALAMAT,'-')AS ALAMAT, "
				+ "STATUS_PROSES.RV_LOW_VALUE AS STATUS_PROSES_LOW, "
				+ "A.STATUS_PROSES AS STS_PROSES "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				// +
				// "LEFT JOIN ( SELECT KODE_INSTANSI, ID_KECELAKAAN, TGL_KEJADIAN, NO_LAPORAN_POLISI FROM PL_DATA_KECELAKAAN ) DATA_LAKA "
				// + "ON A.ID_KECELAKAAN = DATA_LAKA.ID_KECELAKAAN "
				+ "LEFT JOIN ( SELECT NAMA, ALAMAT, ID_KORBAN_KECELAKAAN, KODE_SIFAT_CIDERA FROM PL_KORBAN_KECELAKAAN ) NAMA "
				+ "ON A.ID_KORBAN_KECELAKAAN = NAMA.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_HIGH_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA' ) CIDERA "
				+ "ON NAMA.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS PROSES' ) STATUS_PROSES "
				+ "ON A.STATUS_PROSES = STATUS_PROSES.RV_LOW_VALUE ) INSIDE "
				+ "WHERE " + "INSIDE.NO_BERKAS LIKE '" + noBerkas + "' "
				+tgl
				+pilih
				+ "AND( " + "INSIDE.NO_BERKAS LIKE '"+ search + "' OR " + "INSIDE.STATUS_PROSES LIKE '" + search
				+ "' OR " + "INSIDE.TGL_PENERIMAAN LIKE '" + search + "' OR "
				+ "INSIDE.NAMA LIKE '" + search + "' OR "
				+ "INSIDE.ALAMAT LIKE '" + search + "' )";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	
		

	@Override
	public List<Object[]> getListVerifikasi(String pilihPengajuan,String tglPengajuan,
			String noBerkas, String search) {
		
		String tgl = "";
		if(tglPengajuan!=null && !tglPengajuan.equalsIgnoreCase("%")){
			tgl="AND INSIDE.TGL_PENGAJUAN LIKE TO_DATE('" + tglPengajuan+"', 'DD/MM/YYYY') ";			
		}
		
		String pilih="";
		if(pilihPengajuan!=null && pilihPengajuan.contains("BK")){
			pilih="AND INSIDE.STATUS_PROSES_LOW='BK' ";
		}
		
		String query = "SELECT * FROM "
				+ "(SELECT A.NO_BERKAS, NVL(STATUS_PROSES.RV_MEANING,'-') AS STATUS_PROSES, "
				+ "A.TGL_PENERIMAAN, "
				+ "A.TGL_PENGAJUAN, "
				+ "NVL(NAMA.NAMA,'-') AS NAMA, "
				+ "NVL(NAMA.ALAMAT,'-')AS ALAMAT, "
				+ "STATUS_PROSES.RV_LOW_VALUE AS STATUS_PROSES_LOW "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN ( SELECT KODE_INSTANSI, ID_KECELAKAAN, TGL_KEJADIAN, NO_LAPORAN_POLISI "
				+ "FROM PL_DATA_KECELAKAAN ) DATA_LAKA "
				+ "ON A.ID_KECELAKAAN = DATA_LAKA.ID_KECELAKAAN "
				+ "LEFT JOIN ( SELECT NAMA, ALAMAT, ID_KORBAN_KECELAKAAN, KODE_SIFAT_CIDERA "
				+ "FROM PL_KORBAN_KECELAKAAN ) NAMA "
				+ "ON A.ID_KORBAN_KECELAKAAN = NAMA.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_HIGH_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA' ) CIDERA "
				+ "ON NAMA.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS PROSES' ) STATUS_PROSES "
				+ "ON A.STATUS_PROSES = STATUS_PROSES.RV_LOW_VALUE ) INSIDE "
				+ "WHERE INSIDE.NO_BERKAS LIKE '" + noBerkas + "' "
				+ tgl 
				+ pilih
				+ "AND( INSIDE.NO_BERKAS LIKE '" + search + "' "
				+ "OR INSIDE.STATUS_PROSES LIKE '" + search + "' "
				+ "OR INSIDE.TGL_PENERIMAAN LIKE '" + search + "' "
				+ "OR INSIDE.NAMA LIKE '" + search + "' "
				+ "OR INSIDE.ALAMAT LIKE '" + search + "' )";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	@Override
	public List<Object[]> getDataPengajuanSantunanByNoBerkas(String noBerkas) {
		String query = "SELECT "
				+ "KANTOR.NAMA, "
				+ "HUB.RV_MEANING AS HUBUNGAN_KORBAN, "
				+ "PROSES.RV_MEANING AS STATUS_PROSES, "
				+ "OTORISASI.RV_MEANING AS PENYELESAIAN, "
				+ "A.DIAJUKAN_DI, "
				+ "A.DILIMPAHKAN_KE, "
				+ "NVL(KANTOR_DILIMPAHKAN.NAMA,'-') AS KANTOR_DILIMPAHKAN "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN (SELECT NAMA, KODE_KANTOR_JR FROM FND_KANTOR_JASARAHARJA) KANTOR "
				+ "ON A.DIAJUKAN_DI = KANTOR.KODE_KANTOR_JR "
				+ "LEFT JOIN (SELECT NAMA, KODE_KANTOR_JR FROM FND_KANTOR_JASARAHARJA) KANTOR_DILIMPAHKAN "
				+ "ON A.DILIMPAHKAN_KE = KANTOR_DILIMPAHKAN.KODE_KANTOR_JR "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE HUBUNGAN KORBAN') HUB "
				+ "ON A.KODE_HUBUNGAN_KORBAN = HUB.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE OTORISASI') OTORISASI "
				+ "ON A.OTORISASI_FLAG = OTORISASI.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS PROSES') PROSES "
				+ "ON A.STATUS_PROSES = PROSES.RV_LOW_VALUE "
				+ "WHERE A.NO_BERKAS LIKE '"
				+ noBerkas + "'";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	@Override
	public List<Object[]> getListAbsah(String noBerkas, String tglPenerimaan, String statusProses, String filter){

		String stat, tglTerima, noBerkass, filters;
		stat = "";	tglTerima = ""; noBerkass = ""; filters="";
		if(statusProses == "1"){
			stat=" A.STATUS_PROSES = 'BL' AND ";
		}
		
		
		if (filter!=null){
			if( !filter.equalsIgnoreCase("") ){
				if (!filter.isEmpty()){
					if (!filter.equalsIgnoreCase("ALL")){
						System.out.println(filter.equalsIgnoreCase(""));
						System.out.println(filter.isEmpty() );
						System.out.println(filter.equalsIgnoreCase("ALL"));
						System.out.println(filter!="ALL" );
						System.out.println(filter!="");
					filter = StringUtil.surroundString(StringUtil.nevl(filter, "%"),"%");
					filters = " and ( " +
							  " a.no_berkas like '"+filter +"' or" +
							  " upper(status.rv_meaning) like upper('"+filter +"') or" +
							  " to_char(a.tgl_penerimaan,'DD/MM/YYYY') like '"+filter +"' or" +
							  " upper(KORBAN.NAMA) like upper('"+filter +"') or" +
							  " upper(KORBAN.ALAMAT) like upper('"+filter +"') " +
							  " ) ";
					}
				}
			}
		}
		if (noBerkas != null){
			if (!noBerkas.equalsIgnoreCase("") || 
					!noBerkas.isEmpty() || 
					!noBerkas.equalsIgnoreCase("ALL") || 
					noBerkas!="ALL" || noBerkas!=""){
			noBerkass = "A.NO_BERKAS = '"+noBerkas+"' and ";
			}
			
			if(noBerkas.equalsIgnoreCase("ALL") || noBerkas == "ALL"){
				noBerkass = "";
			}
		}
		
		if (tglPenerimaan!=null && !tglPenerimaan.equalsIgnoreCase("") ){
			if(!tglPenerimaan.isEmpty() || !tglPenerimaan.equalsIgnoreCase("ALL")){
				tglTerima = "trunc(a.tgl_penerimaan) = trunc(to_date('"+tglPenerimaan+"','DD/MM/YYYY')) and ";
			}
		}
		
		String query = "select a.no_berkas, status.rv_meaning, "
				+ "a.tgl_penerimaan, KORBAN.NAMA, KORBAN.ALAMAT, a.id_korban_kecelakaan, "
				+ "a.status_proses from "+
		" pl_pengajuan_santunan a,  "+
		" dasi_jr_ref_codes status, "+
		" pl_korban_kecelakaan korban where upper(a.status_proses) = upper(STATUS.RV_LOW_VALUE) and  "+
		stat + tglTerima + noBerkass +
		" STATUS.RV_DOMAIN = 'KODE STATUS PROSES' and status.flag_enable = 'Y' and "+
		" A.ID_KORBAN_KECELAKAAN = KORBAN.ID_KORBAN_KECELAKAAN "+
		filters+
		" order by tgl_penerimaan desc"; 
		
		System.out.println("+++++++++++++++++++++++");
		System.out.println(query);
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();
		return list;
	}

	@Override
	public int savePengajuanBerikutnya(PlPengajuanSantunan plPengajuanSantunan) {
		
		return 0;
	}

	//added by luthfi
	@Override
	public int updatePengajuanSantunanOtorisasi(PlPengajuanSantunan plPengajuanSantunan) {
		String sql = "UPDATE PL_PENGAJUAN_SANTUNAN SET "
				+ "KESIMPULAN_SEMENTARA = ?, "
				+ "JAMINAN_PEMBAYARAN = ?, "
				+ "OTORISASI_FLAG = ?, "
				+ "TGL_PENGAJUAN = ?,"
				+ "DILIMPAHKAN_KE = ? "
				+ "WHERE NO_BERKAS = '"+plPengajuanSantunan.getNoBerkas()+"'";
		
		 em.createNativeQuery(sql)
				.setParameter(1, plPengajuanSantunan.getKesimpulanSementara())
				.setParameter(2, plPengajuanSantunan.getJaminanPembayaran())
				.setParameter(3, plPengajuanSantunan.getOtorisasiFlag())
				.setParameter(4, plPengajuanSantunan.getTglPengajuan())
				.setParameter(5, plPengajuanSantunan.getDilimpahkanKe()).executeUpdate();
		return 1;
	}

	//added by luthfi
	@Override
	public int updateProsesSelanjutnya(PlPengajuanSantunan plPengajuanSantunan) {
		String sql = "UPDATE PL_PENGAJUAN_SANTUNAN SET "
				+ "STATUS_PROSES = ? "
				+ "WHERE NO_BERKAS = '"+plPengajuanSantunan.getNoBerkas()+"'";
		 em.createNativeQuery(sql)
				.setParameter(1, plPengajuanSantunan.getStatusProses()).executeUpdate();
		return 1;
	}

	@Override
	public List<Object[]> getIndexPengesahan(String pilihPengajuan,
			String tglPenerimaan, String noBerkas, String search) {

		String tgl = "";
		if(tglPenerimaan!=null && !tglPenerimaan.equalsIgnoreCase("%")){
			tgl="AND INSIDE.TGL_PENERIMAAN LIKE TO_DATE('" + tglPenerimaan+"', 'DD/MM/YYYY') ";			
		}
		
		String pilih="";
		if(pilihPengajuan!=null && pilihPengajuan.contains("BV")){
			pilih="AND INSIDE.STATUS_PROSES_LOW='BV' ";
		}		
		
		String query = "SELECT * FROM "
				+ "(SELECT A.NO_BERKAS, NVL(STATUS_PROSES.RV_MEANING,'-') AS STATUS_PROSES, "
				+ "A.TGL_PENERIMAAN, "
				+ "A.TGL_PENGAJUAN, "
				+ "NVL(NAMA.NAMA,'-') AS NAMA, "
				+ "NVL(NAMA.ALAMAT,'-')AS ALAMAT, "
				+ "STATUS_PROSES.RV_LOW_VALUE AS STATUS_PROSES_LOW "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN ( SELECT KODE_INSTANSI, ID_KECELAKAAN, TGL_KEJADIAN, NO_LAPORAN_POLISI "
				+ "FROM PL_DATA_KECELAKAAN ) DATA_LAKA "
				+ "ON A.ID_KECELAKAAN = DATA_LAKA.ID_KECELAKAAN "
				+ "LEFT JOIN ( SELECT NAMA, ALAMAT, ID_KORBAN_KECELAKAAN, KODE_SIFAT_CIDERA "
				+ "FROM PL_KORBAN_KECELAKAAN ) NAMA "
				+ "ON A.ID_KORBAN_KECELAKAAN = NAMA.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_HIGH_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA' ) CIDERA "
				+ "ON NAMA.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_MEANING "
				+ "FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS PROSES' ) STATUS_PROSES "
				+ "ON A.STATUS_PROSES = STATUS_PROSES.RV_LOW_VALUE ) INSIDE "
				+ "WHERE INSIDE.NO_BERKAS LIKE '" + noBerkas + "' "
				+ tgl 
				+ pilih
				+ "AND( INSIDE.NO_BERKAS LIKE '" + search + "' "
				+ "OR INSIDE.STATUS_PROSES LIKE '" + search + "' "
				+ "OR INSIDE.TGL_PENERIMAAN LIKE '" + search + "' "
				+ "OR INSIDE.NAMA LIKE '" + search + "' "
				+ "OR INSIDE.ALAMAT LIKE '" + search + "' )";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	@Override
	public List<Object[]> cetakKuitansi(String noBerkas) {
		
		String query = "SELECT "
				+ "A.NO_BERKAS, "
				+ "SIFAT_LAKA.RV_MEANING AS SIFAT_LAKA, "
				+ "A.NAMA_PEMOHON, "
				+ "A.ALAMAT_PEMOHON, "
				+ "DATA_LAKA.TGL_KEJADIAN, "
				+ "DATA_LAKA.DESKRIPSI_LOKASI, "
				+ "ANGKUTAN.NO_POLISI, "
				+ "JENIS_KENDARAAN.DESKRIPSI, "
				+ "KORBAN.NAMA, "
				+ "KORBAN.UMUR, "
				+ "KORBAN.ALAMAT, "
				+ "SIFAT_CIDERA.RV_MEANING AS SIFAT_CIDERA, "
				+ "HUB.RV_MEANING AS HUB, "
				+ "PENYELESAIAN.JUMLAH_DIBAYAR_MENINGGAL, "
				+ "PENYELESAIAN.JUMLAH_DIBAYAR_LUKALUKA, "
				+ "PENYELESAIAN.JML_BYR_AMBL, "
				+ "PENYELESAIAN.JML_BYR_P3K, "
				+ "PENYELESAIAN.NO_BPK, "
				+ "JAMINAN.LINGKUP_JAMINAN "
				+ "FROM "
				+ "PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN(SELECT ID_KECELAKAAN, SIFAT_KECELAKAAN, TGL_KEJADIAN, DESKRIPSI_LOKASI FROM PL_DATA_KECELAKAAN) DATA_LAKA "
				+ "ON A.ID_KECELAKAAN = DATA_LAKA.ID_KECELAKAAN "
				+ "LEFT JOIN(SELECT NO_POLISI, ID_KECELAKAAN, KODE_JENIS FROM PL_ANGKUTAN_KECELAKAAN) ANGKUTAN "
				+ "ON DATA_LAKA.ID_KECELAKAAN = ANGKUTAN.ID_KECELAKAAN "
				+ "LEFT JOIN(SELECT KODE_JENIS, DESKRIPSI FROM FND_JENIS_KENDARAAN) JENIS_KENDARAAN "
				+ "ON ANGKUTAN.KODE_JENIS = JENIS_KENDARAAN.KODE_JENIS "
				+ "LEFT JOIN(SELECT ID_KORBAN_KECELAKAAN, NAMA, UMUR, ALAMAT, KODE_SIFAT_CIDERA FROM PL_KORBAN_KECELAKAAN) KORBAN "
				+ "ON A.ID_KORBAN_KECELAKAAN = KORBAN.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN(SELECT NO_BERKAS, JUMLAH_DIBAYAR_MENINGGAL, JUMLAH_DIBAYAR_LUKALUKA, NO_BPK, JML_BYR_AMBL, JML_BYR_P3K FROM PL_PENYELESAIAN_SANTUNAN) PENYELESAIAN "
				+ "ON A.NO_BERKAS = PENYELESAIAN.NO_BERKAS "
				+ "LEFT JOIN(SELECT KODE_JAMINAN, LINGKUP_JAMINAN FROM PL_JAMINAN) JAMINAN "
				+ "ON A.KODE_JAMINAN = JAMINAN.KODE_JAMINAN "
				+ "LEFT JOIN(SELECT RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT KECELAKAAN') SIFAT_LAKA "
				+ "ON DATA_LAKA.SIFAT_KECELAKAAN = SIFAT_LAKA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA') SIFAT_CIDERA "
				+ "ON KORBAN.KODE_SIFAT_CIDERA = SIFAT_CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE HUBUNGAN KORBAN') HUB "
				+ "ON A.KODE_HUBUNGAN_KORBAN = HUB.RV_LOW_VALUE "
				+ "WHERE "
				+ "A.NO_BERKAS = '"+noBerkas+"'";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;		
	}

	
	
	//added by Luthfi
	@Override
	public List<Object[]> getIndexHapusPengajuan(String noBerkas) {
		
		String query = "SELECT "
				+ "A.NO_BERKAS, "
				+ "A.TGL_PENERIMAAN, "
				+ "KORBAN.NAMA, "
				+ "A.NAMA_PEMOHON, "
				+ "STATUS_PROSES.RV_MEANING AS STATUS_PROSES, "
				+ "OTORISASI.RV_MEANING AS OTORISASI "
				+ "FROM "
				+ "PL_PENGAJUAN_SANTUNAN A "
				+ "LEFT JOIN(SELECT NAMA, ID_KORBAN_KECELAKAAN FROM PL_KORBAN_KECELAKAAN) KORBAN "
				+ "ON A.ID_KORBAN_KECELAKAAN = KORBAN.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN(SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS PROSES') STATUS_PROSES "
				+ "ON A.STATUS_PROSES = STATUS_PROSES.RV_LOW_VALUE "
				+ "LEFT JOIN(SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE OTORISASI FLAG') OTORISASI "
				+ "ON A.OTORISASI_FLAG = OTORISASI.RV_LOW_VALUE "
				+ "WHERE "
				+ "A.NO_BERKAS LIKE '"+noBerkas+"'"; 
		
		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;		

	}

	//added by Luthfi
	@Override
	public int deletePengajuan(PlPengajuanSantunan plPengajuanSantunan) {
		
		String query = "DELETE FROM PL_PENGAJUAN_SANTUNAN WHERE NO_BERKAS = ? ";
		
		em.createNativeQuery(query)
			.setParameter(1, plPengajuanSantunan.getNoBerkas()).executeUpdate();

		return 1;

	}

	@Override
	public List<Object[]> getIndexPenyelesaian(String pilihPengajuan,
			String tglPenerimaan, String noBerkas, String search) {
		
		String tgl = "";
		if(tglPenerimaan!=null && !tglPenerimaan.equalsIgnoreCase("%")){
			tgl="AND INSIDE.TGL_PENGAJUAN LIKE TO_DATE('" + tglPenerimaan+"', 'DD/MM/YYYY') ";			
		}
		
		String pilih="";
		if(pilihPengajuan!=null && pilihPengajuan.contains("O")){
			pilih="AND INSIDE.STATUS_PROSES_LOW like 'O%'";
		}
		
		
		String query = "SELECT * FROM "
				+ "(SELECT "
				+ "A.ID_KECELAKAAN, "
				+ "A.NO_BERKAS, "
				+ "NVL(STATUS_PROSES.RV_MEANING,'-') AS STATUS_PROSES, "
				+ "A.TGL_PENGAJUAN, "
				+ "NVL(NAMA.NAMA,'-') "
				+ "AS NAMA, NVL(NAMA.ALAMAT,'-')AS ALAMAT, "
				+ "STATUS_PROSES.RV_LOW_VALUE AS STATUS_PROSES_LOW, "
				+ "B.KODE_RUMAHSAKIT "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				// +
				// "LEFT JOIN ( SELECT KODE_INSTANSI, ID_KECELAKAAN, TGL_KEJADIAN, NO_LAPORAN_POLISI FROM PL_DATA_KECELAKAAN ) DATA_LAKA "
				// + "ON A.ID_KECELAKAAN = DATA_LAKA.ID_KECELAKAAN "
				+ "LEFT JOIN ( SELECT NAMA, ALAMAT, ID_KORBAN_KECELAKAAN, KODE_SIFAT_CIDERA FROM PL_KORBAN_KECELAKAAN ) NAMA "
				+ "ON A.ID_KORBAN_KECELAKAAN = NAMA.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_HIGH_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA' ) CIDERA "
				+ "ON NAMA.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS PROSES' ) STATUS_PROSES "
				+ "ON A.STATUS_PROSES = STATUS_PROSES.RV_LOW_VALUE "
				+ "LEFT JOIN PL_PENGAJUAN_RS B ON A.NO_BERKAS = B.NO_BERKAS ) INSIDE "
				+ "WHERE " + "INSIDE.NO_BERKAS LIKE '" + noBerkas + "' "
				+tgl
				+pilih
				+ "AND( " + "INSIDE.NO_BERKAS LIKE '"+ search + "' OR " + "INSIDE.STATUS_PROSES LIKE '" + search
				+ "' OR " + "INSIDE.TGL_PENGAJUAN LIKE '" + search + "' OR "
				+ "INSIDE.NAMA LIKE '" + search + "' OR "
				+ "INSIDE.ALAMAT LIKE '" + search + "' )";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}
	
	@Override
	public List<Object[]> getIndexIdentifikasi(String pilihPengajuan, String tglPenerimaan,
			String noBerkas, String search) {

		String tgl = "";
		if(tglPenerimaan!=null){
			tgl="AND INSIDE.TGL_PENERIMAAN LIKE TO_DATE('" + tglPenerimaan+"', 'DD/MM/YYYY') ";			
		}
		
		
		String pilih="";
		if(pilihPengajuan.contains("BL")){
			pilih="AND INSIDE.STATUS_PROSES_LOW='BL' ";
		}
		System.err.println("luthfi90 "+pilih+" *");
		
		
		String query = "SELECT * FROM "
				+ "(SELECT "
				+ "A.ID_KECELAKAAN, "
				+ "A.NO_BERKAS, "
				+ "NVL(STATUS_PROSES.RV_MEANING,'-') AS STATUS_PROSES, "
				+ "A.TGL_PENERIMAAN, "
				+ "NVL(NAMA.NAMA,'-') "
				+ "AS NAMA, NVL(NAMA.ALAMAT,'-')AS ALAMAT, "
				+ "STATUS_PROSES.RV_LOW_VALUE AS STATUS_PROSES_LOW "
				+ "FROM PL_PENGAJUAN_SANTUNAN A "
				// +
				// "LEFT JOIN ( SELECT KODE_INSTANSI, ID_KECELAKAAN, TGL_KEJADIAN, NO_LAPORAN_POLISI FROM PL_DATA_KECELAKAAN ) DATA_LAKA "
				// + "ON A.ID_KECELAKAAN = DATA_LAKA.ID_KECELAKAAN "
				+ "LEFT JOIN ( SELECT NAMA, ALAMAT, ID_KORBAN_KECELAKAAN, KODE_SIFAT_CIDERA FROM PL_KORBAN_KECELAKAAN ) NAMA "
				+ "ON A.ID_KORBAN_KECELAKAAN = NAMA.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_HIGH_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA' ) CIDERA "
				+ "ON NAMA.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN ( SELECT RV_LOW_VALUE, RV_MEANING FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE STATUS PROSES' ) STATUS_PROSES "
				+ "ON A.STATUS_PROSES = STATUS_PROSES.RV_LOW_VALUE ) INSIDE "
				+ "WHERE " + "INSIDE.NO_BERKAS LIKE '" + noBerkas + "' "
				+tgl
				+pilih
				+ "AND( " + "INSIDE.NO_BERKAS LIKE '"+ search + "' OR " + "INSIDE.STATUS_PROSES LIKE '" + search
				+ "' OR " + "INSIDE.TGL_PENERIMAAN LIKE '" + search + "' OR "
				+ "INSIDE.NAMA LIKE '" + search + "' OR "
				+ "INSIDE.ALAMAT LIKE '" + search + "' )";

		@SuppressWarnings("unchecked")
		List<Object[]> list = em.createNativeQuery(query).getResultList();

		return list;
	}

	
	
}
