package core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.PlRegisterSementaraCustomDao;
import core.model.PlRegisterSementara;


public class PlRegisterSementaraDaoImpl implements PlRegisterSementaraCustomDao{

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Object[]> getListData(String kodeKantorJr, String regDay,
			String regMonth, String regYear, String namaKorban, String lakaDay,
			String lakaMonth, String lakaYear, String noLaporan,
			String noRegister, String asalBerkasFlag, String tindakLanjutFlag,
			String cideraKorban, String namaInstansi, String rvDomainCidera,
			String rvDomainTindakLanjut,String search) {
		// TODO Auto-generated method stub
		String query = "SELECT * FROM(SELECT A.KODE_KANTOR_JR,"
				+ "A.NO_REGISTER,"
				+ "E.TGL_KEJADIAN,"
				+ "A.TGL_REGISTER,"
				+ "D.NAMA,"
				+ "G.RV_HIGH_VALUE,"
				+ "A.NAMA_PEMOHON,"
				+ "H.RV_MEANING,"
				+ "B.TGL_TINDAK_LANJUT,"
				+ "D.ID_KORBAN_KECELAKAAN,"
				+ "E.ID_KECELAKAAN,"
				+ "I.RV_MEANING AS ASALBERKAS,"
				+ "J.RV_MEANING AS HUBUNGANKORBAN "
				+ "FROM "
				+ "PL_REGISTER_SEMENTARA A,"
				+ "PL_TINDAK_LANJUT B,"
				+ "FND_KANTOR_JASARAHARJA C,"
				+ "PL_KORBAN_KECELAKAAN D, "
				+ "PL_DATA_KECELAKAAN E,"
				+ "PL_INSTANSI F,"
				+ "DASI_JR_REF_CODES G,"
				+ "DASI_JR_REF_CODES H,"
				+ "DASI_JR_REF_CODES I,"
				+ "DASI_JR_REF_CODES J "
				+ "WHERE A.NO_REGISTER = B.NO_REGISTER "
				+ "AND A.KODE_KANTOR_JR = C.KODE_KANTOR_JR "
				+ "AND A.ID_KORBAN_KECELAKAAN = D.ID_KORBAN_KECELAKAAN "
				+ "AND D.ID_KECELAKAAN = E.ID_KECELAKAAN "
				+ "AND E.KODE_INSTANSI = F.KODE_INSTANSI "
				+ "AND D.KODE_SIFAT_CIDERA = G.RV_LOW_VALUE "
				+ "AND B.TINDAK_LANJUT_FLAG = H.RV_LOW_VALUE "
				+ "AND A.ASAL_BERKAS_FLAG = I.RV_LOW_VALUE "
				+ "AND I.RV_DOMAIN = 'ASAL BERKAS REGISTER' "
				+ "AND A.KODE_HUBUNGAN_KORBAN = J.RV_LOW_VALUE "
				+ "AND A.KODE_KANTOR_JR LIKE '"+kodeKantorJr+"' "
				+ "AND TO_CHAR(A.TGL_REGISTER,'DD') LIKE '"+regDay+"' "
				+ "AND TO_CHAR(A.TGL_REGISTER,'MM') LIKE '"+regMonth+"' "
				+ "AND TO_CHAR(A.TGL_REGISTER,'YYYY') LIKE '"+regYear+"' "
				+ "AND D.NAMA LIKE '"+namaKorban+"' "
				+ "AND TO_CHAR(E.TGL_KEJADIAN,'DD') LIKE '"+lakaDay+"' "
				+ "AND TO_CHAR(E.TGL_KEJADIAN,'MM') LIKE '"+lakaMonth+"' "
				+ "AND TO_CHAR(E.TGL_KEJADIAN,'YYYY') LIKE '"+lakaYear+"' "
				+ "AND E.NO_LAPORAN_POLISI LIKE '"+noLaporan+"' "
				+ "AND A.NO_REGISTER LIKE '"+noRegister+"' "
				+ "AND A.ASAL_BERKAS_FLAG LIKE '"+asalBerkasFlag+"' "
				+ "AND B.TINDAK_LANJUT_FLAG LIKE '"+tindakLanjutFlag+"' "
				+ "AND A.CIDERA_KORBAN LIKE '"+cideraKorban+"' "
				+ "AND F.DESKRIPSI LIKE '"+namaInstansi+"' "
				+ "AND G.RV_DOMAIN LIKE '"+rvDomainCidera+"' "
				+ "AND H.RV_DOMAIN LIKE '"+rvDomainTindakLanjut+"' "
				+ "AND J.RV_DOMAIN = 'KODE HUBUNGAN KORBAN' "
				+ "AND (A.KODE_KANTOR_JR LIKE UPPER('"+search+"') "
				+ "OR A.NO_REGISTER LIKE UPPER('"+search+"') "
				+ "OR E.TGL_KEJADIAN LIKE UPPER('"+search+"') "
				+ "OR A.TGL_REGISTER LIKE UPPER('"+search+"') "
				+ "OR D.NAMA LIKE UPPER('"+search+"') "
				+ "OR G.RV_HIGH_VALUE LIKE UPPER('"+search+"') "
				+ "OR A.NAMA_PEMOHON LIKE UPPER('"+search+"') "
				+ "OR H.RV_MEANING LIKE UPPER('"+search+"') "
				+ "OR B.TGL_TINDAK_LANJUT LIKE UPPER('"+search+"')))"
				+ "INSIDE "
				+ "WHERE "
				+ "INSIDE.TGL_TINDAK_LANJUT = (SELECT MAX(TGL_TINDAK_LANJUT) FROM PL_TINDAK_LANJUT WHERE INSIDE.NO_REGISTER = NO_REGISTER)";
		
		@SuppressWarnings("unchecked")
		List<Object[]> listRegisterSementara = em.createNativeQuery(query).getResultList();
		return listRegisterSementara;
	}

	@Override
	public List<Object[]> findOneByNoRegister(String noRegister) {
		String query = "SELECT NVL(A.KODE_KANTOR_JR, '-'), "
				+ "NVL(A.NO_REGISTER, '-'), "
				+ "DATA_LAKA.TGL_KEJADIAN, "
				+ "A.TGL_REGISTER, "
				+ "NVL(CIDERA.RV_MEANING,'-')AS CIDERA, "
				+ "NVL(KORBAN.NAMA, '-') AS NAMA_KORBAN, "
				+ "NVL(A.NAMA_PEMOHON, '-'), "
				+ "TL.TGL_TINDAK_LANJUT, "
				+ "TINDAK_LANJUT.RV_MEANING AS TINDAK_LANJUT "
				+ "FROM PL_REGISTER_SEMENTARA A "
				+ "LEFT JOIN (SELECT NAMA, ID_KECELAKAAN, ID_KORBAN_KECELAKAAN, KODE_SIFAT_CIDERA FROM PL_KORBAN_KECELAKAAN) KORBAN "
				+ "ON A.ID_KORBAN_KECELAKAAN = KORBAN.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN (SELECT ID_KECELAKAAN, TGL_KEJADIAN FROM PL_DATA_KECELAKAAN) DATA_LAKA "
				+ "ON DATA_LAKA.ID_KECELAKAAN = KORBAN.ID_KECELAKAAN "
				+ "LEFT JOIN (SELECT RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA') CIDERA "
				+ "ON KORBAN.KODE_SIFAT_CIDERA = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT NO_REGISTER, TGL_TINDAK_LANJUT, TINDAK_LANJUT_FLAG FROM PL_TINDAK_LANJUT) TL "
				+ "ON A.NO_REGISTER = TL.NO_REGISTER "
				+ "LEFT JOIN (SELECT RV_MEANING, RV_LOW_VALUE FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'TINDAK LANJUT REGISTER') TINDAK_LANJUT "
				+ "ON TL.TINDAK_LANJUT_FLAG = TINDAK_LANJUT.RV_LOW_VALUE "
				+ "WHERE "
				+ "A.NO_REGISTER LIKE '"+noRegister+"' order by A.NO_REGISTER ASC"; 
		@SuppressWarnings("unchecked")
		List<Object[]> listRegisterSementara = em.createNativeQuery(query).getResultList();
		return listRegisterSementara;
	}

	@Override
	public int deleteRegister(PlRegisterSementara plRegisterSementara) {
		String query = "DELETE FROM PL_REGISTER_SEMENTARA WHERE NO_REGISTER = ? ";
		
		em.createNativeQuery(query)
			.setParameter(1, plRegisterSementara.getNoRegister()).executeUpdate();

		return 1;
	}

	@Override
	public List<Object[]> findMonitoringGLRS(String kodeKantor,String jenisTgl,String tglAwal,String tglAkhir,String kodeRS,String flagBayar) {
		// TODO Auto-generated method stub
		String query = "";
		String queryUtama = "SELECT * "
				+ "FROM ("
				+ "SELECT DISTINCT R.KODE_KANTOR_JR,"
				+ "R.NO_REGISTER,"
				+ "TO_CHAR(DK.TGL_KEJADIAN, 'DD/MM/YYYY' )AS TGL_KEJADIAN,"
				+ "TO_CHAR(R.TGL_REGISTER, 'DD/MM/YYYY') AS TGL_REGISTER,"
				+ "KC.NAMA AS NAMA_KORBAN,"
				+ "C.RV_HIGH_VALUE AS CIDERA_DESC, "
				+ "TR.NO_SURAT_JAMINAN AS NO_SURAT_JAMINAN,"
				+ "(NVL(TR.JUMLAH_PENGAJUAN_1,0) + NVL(TR.JML_AMBL,0) + NVL(TR.JML_P3K,0)) AS JML_PENGAJUAN,"
				+ "NVL(SB.NO_BERKAS, NVL(ST.NO_BERKAS, NVL(SL.NO_BERKAS, NULL))) AS BERKAS_SS,"
				+ "RSI.DESKRIPSI AS NAMA_RS, "
				+ "MB.ALAMAT_RS_BPJS AS ALAMAT_RS, "
				+ "R.ID_KORBAN_KECELAKAAN, "
				+ "TR.ID_JAMINAN, "
				+ "DC.RV_MEANING AS FLAG_BAYAR "
				+ "FROM PL_REGISTER_SEMENTARA R "
				+ "JOIN PL_TINDAK_LANJUT TL ON TL.NO_REGISTER = R.NO_REGISTER AND TL.TINDAK_LANJUT_FLAG IN ('5','10') "
				+ "JOIN PL_TL_RS TR ON TR.NO_REGISTER = R.NO_REGISTER "
				+ "JOIN PL_KORBAN_KECELAKAAN KC ON KC.ID_KORBAN_KECELAKAAN = R.ID_KORBAN_KECELAKAAN "
				+ "JOIN PL_DATA_KECELAKAAN DK ON DK.ID_KECELAKAAN = KC.ID_KECELAKAAN "
				+ "JOIN DASI_JR_REF_CODES C ON C.RV_LOW_VALUE = R.CIDERA_KORBAN AND C.RV_DOMAIN = 'KODE SIFAT CIDERA' "
				+ "JOIN PL_RUMAH_SAKIT RSI ON RSI.KODE_RUMAHSAKIT = TR.KODE_RUMAH_SAKIT "
				+ "JOIN PL_RS_MAP_BPJS MB ON MB.KODE_RUMAHSAKIT = RSI.KODE_RUMAHSAKIT "
				+ "JOIN DASI_JR_REF_CODES DC ON DC.RV_LOW_VALUE = TR.FLAG_BAYAR AND DC.RV_DOMAIN = 'STATUS JAMINAN GL' "
				+ "LEFT JOIN PL_PENGAJUAN_SANTUNAN SB ON SB.ID_KORBAN_KECELAKAAN = R.ID_KORBAN_KECELAKAAN AND SB.KODE_PENGAJUAN = '0' AND SB.OTORISASI_FLAG = '1' "
				+ "LEFT JOIN PL_PENGAJUAN_SANTUNAN ST ON ST.ID_KORBAN_KECELAKAAN = R.ID_KORBAN_KECELAKAAN AND ST.OTORISASI_FLAG = '2' "
				+ "LEFT JOIN PL_PENGAJUAN_SANTUNAN SL ON SL.ID_KORBAN_KECELAKAAN = R.ID_KORBAN_KECELAKAAN AND SL.OTORISASI_FLAG = '3' "
				+ "WHERE R.KODE_KANTOR_JR LIKE '"+kodeKantor+"' AND "
				+ "RSI.KODE_RUMAHSAKIT LIKE '"+kodeRS+"' AND "
				+ "TR.FLAG_BAYAR LIKE '"+flagBayar+"' ";
		
		String queryTglKejadian = "AND TRUNC(DK.TGL_KEJADIAN) BETWEEN TO_DATE('"+tglAwal+"', 'DD/MM/YYYY') AND TO_DATE(NVL('"+tglAkhir+"', SYSDATE), 'DD/MM/YYYY')) V "
				+ "WHERE V.BERKAS_SS IS NULL";
		String queryTglRegister = "AND TRUNC(R.TGL_REGISTER) BETWEEN TO_DATE('"+tglAwal+"', 'DD/MM/YYYY') AND TO_DATE(NVL('"+tglAkhir+"', SYSDATE), 'DD/MM/YYYY')) V "
				+ "WHERE V.BERKAS_SS IS NULL";
		
		
		if (jenisTgl.equalsIgnoreCase("Tgl Kecelakaan")) {
			query = query+queryUtama+queryTglKejadian;
		} else if (jenisTgl.equalsIgnoreCase("Tgl Register")) {
			query = query+queryUtama+queryTglRegister;
		}
		System.out.println("cegafgaga       "+jenisTgl);
		System.out.println("aaaaaaaaaaa      "+query);
		@SuppressWarnings("unchecked")
		List<Object[]> listGLRS = em.createNativeQuery(query).getResultList();
		return listGLRS;
			
		}

	@Override
	public List<Object[]> getListIndex(String kodeKantorJr, String regDay,
			String regMonth, String regYear, String namaKorban, String lakaDay,
			String lakaMonth, String lakaYear, String noLaporan,
			String noRegister, String asalBerkasFlag, String tindakLanjutFlag,
			String cideraKorban, String kodeInstansi) {
		
		String query = "SELECT * FROM(SELECT "
				+ "B.NAMA, "
				+ "A.NO_REGISTER, "
				+ "D.TGL_KEJADIAN, "
				+ "A.TGL_REGISTER, "
				+ "C.NAMA AS NAMA_KORBAN, "
				+ "CIDERA.RV_HIGH_VALUE, "
				+ "A.NAMA_PEMOHON, "
				+ "E.TGL_TINDAK_LANJUT, "
				+ "TL.RV_MEANING AS TL, "
				+ "B.KODE_KANTOR_JR, "
				+ "D.NO_LAPORAN_POLISI, "
				+ "A.ASAL_BERKAS_FLAG, "
				+ "E.TINDAK_LANJUT_FLAG, "
				+ "A.CIDERA_KORBAN, "
				+ "H.KODE_INSTANSI "
				+ "FROM PL_REGISTER_SEMENTARA A "
				+ "LEFT JOIN FND_KANTOR_JASARAHARJA B "
				+ "ON A.KODE_KANTOR_JR = B.KODE_KANTOR_JR "
				+ "LEFT JOIN PL_KORBAN_KECELAKAAN C "
				+ "ON A.ID_KORBAN_KECELAKAAN = C.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN PL_DATA_KECELAKAAN D "
				+ "ON C.ID_KECELAKAAN = D.ID_KECELAKAAN "
				+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'KODE SIFAT CIDERA') CIDERA "
				+ "ON A.CIDERA_KORBAN = CIDERA.RV_LOW_VALUE "
				+ "LEFT JOIN (SELECT * FROM PL_TINDAK_LANJUT) E "
				+ "ON E.NO_REGISTER = A.NO_REGISTER "
				+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'TINDAK LANJUT REGISTER') TL "
				+ "ON TL.RV_LOW_VALUE = E.TINDAK_LANJUT_FLAG "
				+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'ASAL BERKAS REGISTER') G "
				+ "ON A.ASAL_BERKAS_FLAG = G.RV_LOW_VALUE "
				+ "LEFT JOIN PL_INSTANSI H "
				+ "ON D.KODE_INSTANSI = H.KODE_INSTANSI "
				+ "WHERE E.TGL_TINDAK_LANJUT = (SELECT MAX(TGL_TINDAK_LANJUT) "
				+ "FROM PL_TINDAK_LANJUT WHERE E.NO_REGISTER = NO_REGISTER)) "
				+ "INSIDE "
				+ "WHERE "
				+ "INSIDE.KODE_KANTOR_JR LIKE '"+kodeKantorJr+"' "
				+ "AND TO_CHAR(INSIDE.TGL_REGISTER,'DD') LIKE '"+regDay+"' "
				+ "AND TO_CHAR(INSIDE.TGL_REGISTER,'MM') LIKE '"+regMonth+"' "
				+ "AND TO_CHAR(INSIDE.TGL_REGISTER,'YYYY') LIKE '"+regYear+"' "
				+ "AND INSIDE.NAMA_KORBAN LIKE '"+namaKorban+"' "
				+ "AND TO_CHAR(INSIDE.TGL_KEJADIAN,'DD') LIKE '"+lakaDay+"' "
				+ "AND TO_CHAR(INSIDE.TGL_KEJADIAN,'MM') LIKE '"+lakaMonth+"' "
				+ "AND TO_CHAR(INSIDE.TGL_KEJADIAN,'YYYY') LIKE '"+lakaYear+"' "
				+ "AND INSIDE.NO_LAPORAN_POLISI LIKE '"+noLaporan+"' "
				+ "AND INSIDE.NO_REGISTER LIKE '"+noRegister+"' "
				+ "AND INSIDE.ASAL_BERKAS_FLAG LIKE '"+asalBerkasFlag+"' "
				+ "AND INSIDE.TINDAK_LANJUT_FLAG LIKE '"+tindakLanjutFlag+"' "
				+ "AND INSIDE.CIDERA_KORBAN LIKE '"+cideraKorban+"' "
				+ "AND INSIDE.KODE_INSTANSI LIKE '"+kodeInstansi+"'"; 
		
		@SuppressWarnings("unchecked")
		List<Object[]> listRegisterSementara = em.createNativeQuery(query).getResultList();
		return listRegisterSementara;

	}

	@Override
	public List<Object[]> getMonitoringGLRS(String kodeKantor, String jenisTgl,
			String tglAwal, String tglAkhir, String kodeRS, String flagBayar) {
		
		String tglPeriode ="";
		if(jenisTgl.contains("Register")){
			tglPeriode = "(TRUNC(B.TGL_REGISTER) >= TRUNC(TO_DATE('"+tglAwal+"','DD/MM/YYYY')) AND TRUNC(B.TGL_REGISTER) <= TRUNC(TO_DATE('"+tglAkhir+"','DD/MM/YYYY'))) ";
		}else{
			tglPeriode = "(TRUNC(D.TGL_KEJADIAN) >= TRUNC(TO_DATE('"+tglAwal+"','DD/MM/YYYY')) AND TRUNC(D.TGL_KEJADIAN) <= TRUNC(TO_DATE('"+tglAkhir+"','DD/MM/YYYY'))) "; 
		}
		
		String query= "SELECT "
				+ "A.KODE_KANTOR_JR, "
				+ "J.DESKRIPSI, "
				+ "A.NO_REGISTER, "
				+ "D.TGL_KEJADIAN, "
				+ "C.NAMA, "
				+ "A.NO_SURAT_JAMINAN, "
				+ "A.TGL_SURAT_JAMINAN, "
				+ "(SUM(A.JUMLAH_PENGAJUAN_1)+SUM(A.JML_AMBL)+SUM(A.JML_P3K)) AS JML_PENGAJUAN, "
				+ "(SUM(A.JUMLAH_PENGAJUAN_SMT)+SUM(A.JML_AMBL_SMT)+SUM(A.JML_P3K_SMT)) AS JML_DIGUNAKAN, "
				+ "H.RV_MEANING, "
				+ "A.ID_JAMINAN, "
				+ "C.ID_KORBAN_KECELAKAAN, "
				+ "D.ID_KECELAKAAN, "
				+ "K.RV_MEANING AS ASAL_BERKAS, "
				+ "M.NAMA AS LOKASI_PENGAJUAN, "
				+ "B.TGL_REGISTER, "
				+ "A.FLAG_BAYAR, "
				+ "A.OTORISASI_AWAL, "
				+ "A.OTORISASI_AJU_SMT "
				+ "FROM "
				+ "PL_TL_RS A "
				+ "LEFT JOIN PL_REGISTER_SEMENTARA B "
				+ "ON A.NO_REGISTER = B.NO_REGISTER "
				+ "LEFT JOIN PL_KORBAN_KECELAKAAN C "
				+ "ON B.ID_KORBAN_KECELAKAAN = C.ID_KORBAN_KECELAKAAN "
				+ "LEFT JOIN PL_DATA_KECELAKAAN D "
				+ "ON C.ID_KECELAKAAN = D.ID_KECELAKAAN "
				+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'STATUS JAMINAN GL') H "
				+ "ON A.FLAG_BAYAR = H.RV_LOW_VALUE "
				+ "LEFT JOIN PL_RUMAH_SAKIT J "
				+ "ON A.KODE_RUMAH_SAKIT = J.KODE_RUMAHSAKIT "
				+ "LEFT JOIN (SELECT * FROM DASI_JR_REF_CODES WHERE RV_DOMAIN = 'ASAL BERKAS REGISTER') K "
				+ "ON B.ASAL_BERKAS_FLAG = K.RV_LOW_VALUE "
				+ "LEFT JOIN PL_TINDAK_LANJUT L "
				+ "ON B.NO_REGISTER = L.NO_REGISTER "
				+ "LEFT JOIN FND_KANTOR_JASARAHARJA M "
				+ "ON A.KODE_KANTOR_JR = M.KODE_KANTOR_JR "
				+ "WHERE A.KODE_KANTOR_JR LIKE '"+kodeKantor+"' "
				+ "AND A.KODE_RUMAH_SAKIT LIKE '"+kodeRS+"' "
				+ "AND ("
				+ tglPeriode
				+ "AND A.KODE_RUMAH_SAKIT LIKE '"+kodeRS+"' "
				+ "AND H.RV_LOW_VALUE LIKE '"+flagBayar+"') "
				+ "GROUP BY "
				+ "A.KODE_KANTOR_JR, "
				+ "J.DESKRIPSI, "
				+ "A.NO_REGISTER, "
				+ "D.TGL_KEJADIAN, "
				+ "C.NAMA, "
				+ "A.NO_SURAT_JAMINAN, "
				+ "A.TGL_SURAT_JAMINAN, "
				+ "H.RV_MEANING, "
				+ "A.ID_JAMINAN, "
				+ "C.ID_KORBAN_KECELAKAAN, "
				+ "D.ID_KECELAKAAN, "
				+ "K.RV_MEANING, "
				+ "M.NAMA, "
				+ "B.TGL_REGISTER, "
				+ "A.FLAG_BAYAR, "
				+ "A.OTORISASI_AWAL, "
				+ "A.OTORISASI_AJU_SMT ";
		
		@SuppressWarnings("unchecked")
		List<Object[]> listRegisterSementara = em.createNativeQuery(query).getResultList();
		return listRegisterSementara;
	}

}
