package core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import share.DukcapilRefCodeDto;
import common.util.JsonUtil;
import core.dao.DukcapilWinCustomDao;
import core.model.DukcapilRefCode;
import core.model.DukcapilWn;
import core.model.DukcapilWnFam;

public class DukcapilWinDaoImpl implements DukcapilWinCustomDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public int saveDukcapil(DukcapilWn dukcapilWin) {
		try{
			System.out.println("DAta tO be saved : \n"+JsonUtil.getJson(dukcapilWin));
			em.persist(dukcapilWin);
//			String sql = "insert into DUKCAPIL_WN ("
//					 + "NIK, " + "NO_KK, " + "NAMA_LENGKAP, " + "TMP_LAHIR, " + "TGL_LAHIR, "
//					 + "JNS_KELAMIN, " + "GOL_DARAH, " + "AGAMA, " + "ALAMAT, " + "ALAMAT_RT, "
//					 + "ALAMAT_RW, " + "ALAMAT_DUSUN, " + "ALAMAT_KODEPOS, " + "KODE_PEKERJAAN, "
//					 + "KODE_KELURAHAN, " + "KODE_KECAMATAN, " + "KODE_KABUPATEN, " + "KODE_PROPINSI, "
//					 + "PEND_AKHIR, " + "STATUS_NIKAH, " + "TGL_NIKAH, " + "EKTP_STATUS, "
//					 + "EKTP_LOCALID, " + "EKTP_CREATED, " + "NAMA_AYAH, " + "NAMA_IBU, "
//					 + "LAST_SYNC_DATE) "
//					 + "VALUES "
//					 + "(:nik, :nokk, :namaLkp, :tmpLahir, :tglLahir, "
//					 + ":jnsKel, :golDarah, :agama, :alamat, :alamatRt, "
//					 + ":alamatRw, :alamatDusun, :kodePos, :kodePek, "
//					 + ":kodeKel, :kodeKec, :kodeKab, :kodeProp, "
//					 + ":pendAkhir, :statusNikah, :tglNikah, :ektpSts, "
//					 + ":ektpLocId, :ektpCreated, :namaAyah, :namaIbu, :lastSynch)";
			try{
//				em.createNativeQuery(sql)
//				.setParameter("nik", dukcapilWin.getNik()) .setParameter("nokk", dukcapilWin.getNoKk()) .setParameter("tmpLahir", dukcapilWin.getTmpLahir())
//				.setParameter("jnsKel", dukcapilWin.getJnsKelamin()) .setParameter("golDarah", dukcapilWin.getGolDarah()) .setParameter("agama", dukcapilWin.getAgama())
//				.setParameter("alamat", dukcapilWin.getAlamat()) .setParameter("alamatRt", dukcapilWin.getAlamatRt()) .setParameter("alamatRw", dukcapilWin.getAlamatRw())
//				.setParameter("alamatDusun", dukcapilWin.getAlamatDusun()) .setParameter("kodePos", dukcapilWin.getAlamatKodepos()) .setParameter("kodePek", dukcapilWin.getKodePekerjaan())
//				.setParameter("kodeProp", dukcapilWin.getKodePropinsi()) .setParameter("pendAkhir", dukcapilWin.getPendAkhir()) .setParameter("statusNikah", dukcapilWin.getStatusNikah())
//				.setParameter("tglNikah", dukcapilWin.getTglNikah()) .setParameter("ektpSts", dukcapilWin.getEktpStatus()) .setParameter("ektpLocId", dukcapilWin.getEktpLocalid())
//				.setParameter("ektpCreated", dukcapilWin.getEktpCreated()) .setParameter("namaAyah", dukcapilWin.getNamaAyah()) .setParameter("namaIbu", dukcapilWin.getNamaIbu())
//				.setParameter("lastSynch", dukcapilWin.getLastSyncDate()) .setParameter("namaLkp", dukcapilWin.getNamaLengkap()) .setParameter("tglLahir", dukcapilWin.getTglLahir())
//				.setParameter("kodeKel", dukcapilWin.getKodeKelurahan()) .setParameter("kodeKec", dukcapilWin.getKodeKecamatan()) .setParameter("kodeKab", dukcapilWin.getKodeKabupaten())
//				.executeUpdate();
//				return 0;
			}catch(Exception a){
				a.printStackTrace();
				return 1;
			}
		}catch(Exception s){
			s.printStackTrace();
			return 1;
		}
		return 0;
	}
	
	public List<DukcapilRefCodeDto> getDukcapil(String meaning){
		String query = "select a.rf_domain, a.rf_low_value, a.rf_high_value, a.rv_meaning "
				+ "from DUKCAPIL_REF_CODES a where a.rf_domain = 'KODE HUB KELUARGA'";
		List<DukcapilRefCodeDto> listDto = new ArrayList<>();
		
		try {
			@SuppressWarnings("unchecked")
			List<Object[]> objL = em.createNamedQuery(query).getResultList();
			
			for(Object[] obj : objL){
				DukcapilRefCodeDto dto = new DukcapilRefCodeDto();
				dto.setRfDomain((String) obj[0] );
				dto.setRfLowValue((String) obj[1]);
				dto.setRfHighValue((String) obj[2]);
				dto.setRvMeaning((String) obj[3]);
				listDto.add(dto);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return listDto;
		
	}
	
	@Override
	public int saveDukcapilFam(DukcapilWnFam dukcapilWnFam){
		String sql = "Insert into DUKCAPIL_WN_FAM "
				+ "(NO_KK, "
				+ "NIK, "
				+ "KODE_HUB_KEL, "
				+ "LAST_SYNC_DATE "
				+ ") "
				+ "Values (:noKK,:nik,:kodeHub,:lastSync)";
		
		try{
			em.createNativeQuery(sql)
			.setParameter("noKK", dukcapilWnFam.getNoKk())
			.setParameter("nik", dukcapilWnFam.getNik())
			.setParameter("kodeHub", dukcapilWnFam.getKodeHubKel())
			.setParameter("lastSync", dukcapilWnFam.getLastSyncDate())
			.executeUpdate();
			return 0;
		}catch(Exception s){
			s.printStackTrace();
			return 1;
		}
	}

}
