package core.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.PlPenyelesaianSantunanCustomDao;
import core.model.PlPenyelesaianSantunan;

public class PlPenyelesaianSantunanDaoImpl implements PlPenyelesaianSantunanCustomDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public void save(PlPenyelesaianSantunan pl) {

		String query = "INSERT INTO PL_PENYELESAIAN_SANTUNAN("
				+ "NO_BERKAS, CREATED_BY, CREATION_DATE, ID_GUID, JENIS_PEMBAYARAN, "
				+ "JML_BYR_AMBL, JML_BYR_P3K, JUMLAH_DIBAYAR_LUKALUKA, JUMLAH_DIBAYAR_MENINGGAL, JUMLAH_DIBAYAR_PENGUBURAN, "
				+ "KELEBIHAN_BAYAR, LAST_UPDATED_BY, LAST_UPDATED_DATE, NO_BPK, NO_REKENING, "
				+ "NO_SURAT_PENYELESAIAN, TGL_KELEBIHAN_BAYAR, TGL_PEMBUATAN_BPK, TGL_PROSES) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		em.createNativeQuery(query).setParameter(1, pl.getNoBerkas())
				.setParameter(2, pl.getCreatedBy())
				.setParameter(3, pl.getIdGuid())
				.setParameter(4, pl.getJenisPembayaran())
				.setParameter(5, pl.getJenisPembayaran())
				.setParameter(6, pl.getJmlByrAmbl() != null ? pl.getJmlByrAmbl() : 0)
				.setParameter(7, pl.getJmlByrP3k() != null ? pl.getJmlByrP3k() : 0)
				.setParameter(8, pl.getJumlahDibayarLukaluka() != null ? pl.getJumlahDibayarLukaluka() : 0)
				.setParameter(9, pl.getJumlahDibayarMeninggal() != null ? pl.getJumlahDibayarMeninggal() : 0)
				.setParameter(10, pl.getJumlahDibayarPenguburan() != null ? pl.getJumlahDibayarPenguburan() : 0)
				.setParameter(11, pl.getKelebihanBayar() != null ? pl.getKelebihanBayar() : 0)
				.setParameter(12, pl.getLastUpdatedBy())
				.setParameter(13, pl.getLastUpdatedDate())
				.setParameter(14, pl.getNoBpk())
				.setParameter(15, pl.getNoRekening())
				.setParameter(16, pl.getNoSuratPenyelesaian())
				.setParameter(17, pl.getTglKelebihanBayar())
				.setParameter(18, pl.getTglPembuatanBpk())
				.setParameter(19, pl.getTglProses()).executeUpdate();
	}

}
