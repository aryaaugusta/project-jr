package core.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.FndCamatCustomDao;
import core.model.FndCamat;

public class FndCamatDaoImpl implements FndCamatCustomDao {
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public int saveCamat(FndCamat lok){
		em.persist(lok);
		return 1;
	}
	
	@Override
	public int saveLokasi(FndCamat lok) {
		String sql = "Insert into FND_CAMAT "
				+ "(KODE_CAMAT, "
				+ "NAMA_CAMAT, "
				+ "KODE_KABKOTA, "
				+ "NAMA_KABKOTA, "
				+ "KODE_PROVINSI, "
				+ "NAMA_PROVINSI, "
				+ "FLAG_ENABLE, CATATAN) "
				+ "Values (?,?,?,?,?,?,?,?)";
		
		em.createNativeQuery(sql)
				.setParameter(1, lok.getKodeCamat())
				.setParameter(2, lok.getNamaCamat())
				.setParameter(3, lok.getKodeKabkota())
				.setParameter(4, lok.getNamaKabkota())
				.setParameter(5, lok.getKodeProvinsi())
				.setParameter(6, lok.getNamaProvinsi())
				.setParameter(7, lok.getFlagEnable())
				.setParameter(8, lok.getCatatan()).executeUpdate();
		
		return 1;
	}

	@Override
	public int updateLokasi(FndCamat lok) {
		String sql = "update FND_CAMAT set "
				+ "NAMA_CAMAT = ?, "
				+ "KODE_KABKOTA = ?, "
				+ "NAMA_KABKOTA = ?, "
				+ "KODE_PROVINSI = ?, "
				+ "NAMA_PROVINSI = ?, "
				+ "FLAG_ENABLE = ?, "
				+ "CATATAN = ? where KODE_CAMAT = '"+lok.getKodeCamat()+"'";
		
		
		 em.createNativeQuery(sql)
				.setParameter(1, lok.getNamaCamat())
				.setParameter(2, lok.getKodeKabkota())
				.setParameter(3, lok.getNamaKabkota())
				.setParameter(4, lok.getKodeProvinsi())
				.setParameter(5, lok.getNamaProvinsi())
				.setParameter(6, lok.getFlagEnable())
				.setParameter(7, lok.getCatatan()).executeUpdate();
		return 1;
	}

}
