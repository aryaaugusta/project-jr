package core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import core.dao.PlInstansiCustomDao;

public class PlInstansiDaoImpl implements PlInstansiCustomDao{
	@PersistenceContext
	private EntityManager em;
	@Override
	public List<Object[]> getList(String kodeInstansi, String search) {
		// TODO Auto-generated method stub

		String value=" SELECT I.KODE_INSTANSI, I.DESKRIPSI, I.FLAG_ENABLE, " + 
				" MP.NAMA_POLDA, " +
//				" DECODE(VW.KODE_INSTANSI, NULL, 'inline', 'none') AS DIS_DEL, " + 
				" (I.KODE_INSTANSI || '|' || I.DESKRIPSI || '|' || I.FLAG_ENABLE || '|' || KD.PROVINCE_ID || '|' || MP.ID_DISTRICTS) AS INSTANSI_ARRAY " + 
				" FROM PL_INSTANSI I " + 
				" JOIN PL_MAPPING_POLDA MP ON MP.KODE_INSTANSI = I.KODE_INSTANSI " + 
				" LEFT JOIN KORLANTAS_DISTRICTS KD ON TO_CHAR(KD.ID) = MP.ID_DISTRICTS " + 
//				" LEFT JOIN " + 
//				" (SELECT DISTINCT(D.KODE_INSTANSI) AS KODE_INSTANSI " + 
//				" FROM PL_DATA_KECELAKAAN D " + 
//				" WHERE D.KODE_INSTANSI LIKE (SUBSTR('"+kodeInstansi+"',1,2) || '0' || SUBSTR('"+kodeInstansi+"',5,2) || '%')) VW ON VW.KODE_INSTANSI = I.KODE_INSTANSI " + 
				" WHERE I.DESKRIPSI NOT LIKE '%Migrasi%' " +
				" AND I.KODE_INSTANSI LIKE (SUBSTR('"+kodeInstansi+"',1,2) || '0' || SUBSTR('"+kodeInstansi+"',5,2) || '%') " +
				" AND (I.KODE_INSTANSI LIKE '"+search+"' or I.DESKRIPSI LIKE '"+search+"') "+
				" ORDER BY KODE_INSTANSI ";
		
//		Query q = em.createNativeQuery(value);
//		q.setParameter("params", kodeInstansi);
//		q.setParameter("search", search);
//		
//		return q.getResultList();
		return em.createNativeQuery(value).getResultList();
		
		
		
	}

}
