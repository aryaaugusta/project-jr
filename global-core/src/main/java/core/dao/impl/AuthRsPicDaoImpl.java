package core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.AuthRsPicCustomDao;
import core.model.AuthRsPic;
import core.model.AuthUser;

public class AuthRsPicDaoImpl implements AuthRsPicCustomDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Object[]> getDataList(String search) {

		String query = "SELECT a.LOGIN, "
				+ "NVL(RS.KODE_RUMAHSAKIT, '') AS RS, "
				+ "a.USER_NAME, a.description, a.attribute4, a.attribute2, a.USER_ID "
				+ "FROM AUTH_USER a "
				+ "LEFT JOIN (SELECT USER_LOGIN, KODE_RUMAHSAKIT FROM AUTH_RS_PIC ) RS_PIC "
				+ "ON a.LOGIN = RS_PIC.USER_LOGIN "
				+ "LEFT JOIN (SELECT * FROM PL_RUMAH_SAKIT) RS "
				+ "ON RS_PIC.KODE_RUMAHSAKIT = RS.KODE_RUMAHSAKIT where a.ATTRIBUTE1 like '"+search+"'";
		
		@SuppressWarnings("unchecked")
		List<Object[]> listRsPic = em.createNativeQuery(query).getResultList();
		
		return listRsPic;			
		
	}

//	@Override
//	public List<Object[]> getRsByUserLogin(String userLogin) {
//		
//		String query = "SELECT RS.DESKRIPSI, "
//				+ "NVL(RS.KODE_RUMAHSAKIT, '') AS RS "
//				+ "FROM AUTH_USER a "
//				+ "LEFT JOIN (SELECT USER_LOGIN, KODE_RUMAHSAKIT FROM AUTH_RS_PIC ) RS_PIC "
//				+ "ON a.LOGIN = RS_PIC.USER_LOGIN "
//				+ "LEFT JOIN (SELECT * FROM PL_RUMAH_SAKIT) RS "
//				+ "ON RS_PIC.KODE_RUMAHSAKIT = RS.KODE_RUMAHSAKIT where a.LOGIN like '"+userLogin+"'";
//		
//		@SuppressWarnings("unchecked")
//		List<Object[]> listRs = em.createNativeQuery(query).getResultList();
//		
//		return listRs;			
//	}

	@Override
	public int saveRsPic(AuthRsPic rsPic) {
		
		String query = "INSERT INTO AUTH_RS_PIC (USER_LOGIN, KODE_RUMAHSAKIT) VALUES (?,?)";

		em.createNativeQuery(query).
			setParameter(1, rsPic.getUserLogin()).
			setParameter(2, rsPic.getKodeRumahsakit()).executeUpdate();
		
		return 1;
	}

	@Override
	public int updateRsPic(AuthRsPic rsPic) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteRsPic(AuthRsPic rsPic) {
		
		String query = "DELETE FROM AUTH_RS_PIC WHERE USER_LOGIN = ? AND "
				+ "AUTH_RS_PIC.KODE_RUMAHSAKIT = ?";
		
		em.createNativeQuery(query)
			.setParameter(1, rsPic.getUserLogin())
			.setParameter(2, rsPic.getKodeRumahsakit()).executeUpdate();

		return 1;
	}

	@Override
	public List<Object[]> getRsNotIn() {
		
		String query ="SELECT  a.KODE_RUMAHSAKIT, a.DESKRIPSI FROM PL_RUMAH_SAKIT a WHERE NOT EXISTS "
				+ "(SELECT KODE_RUMAHSAKIT FROM AUTH_RS_PIC b WHERE "
				+ "a.KODE_RUMAHSAKIT = b.KODE_RUMAHSAKIT)";
		
		@SuppressWarnings("unchecked")
		List<Object[]> listRsNotIn = em.createNativeQuery(query).getResultList();
		
		return listRsNotIn;
	}

	@Override
	public int updateAuthUser(AuthUser authUser) {
		
		String query = "UPDATE AUTH_USER "
				+ "SET attribute4 = ?, "
				+ "attribute2 = ? WHERE LOGIN = ? ";

		em.createNativeQuery(query)
		.setParameter(1, authUser.getAttribute4())
		.setParameter(2, authUser.getAttribute2())
		.setParameter(3, authUser.getLogin()).executeUpdate();

		return 1;
	}
	
	
}
