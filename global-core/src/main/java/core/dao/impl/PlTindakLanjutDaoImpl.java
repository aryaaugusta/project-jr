package core.dao.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import core.dao.PlTindakLanjutCustomDao;

public class PlTindakLanjutDaoImpl implements PlTindakLanjutCustomDao{

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public int deleteByID(String idTLReg) {
		// TODO Auto-generated method stub
		String query = "DELETE FROM PL_TINDAK_LANJUT WHERE ID_TL_REGISTER = ?";
		
		em.createNativeQuery(query).setParameter(1, idTLReg).executeUpdate();
		return 1;
	}

}
