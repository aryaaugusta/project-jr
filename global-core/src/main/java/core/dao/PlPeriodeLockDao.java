package core.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlPeriodeLock;

public interface PlPeriodeLockDao extends JpaRepository<PlPeriodeLock, Date>, PlPeriodeLockCustomDao {
	
	@Query(value="select a from PlPeriodeLock a "
			+ "where "
			+ "a.kodeKantorJr like :kodeKantorJr and "
			+ "a.periodeBulan like :periodeBulan and "
			+ "a.periodeTahun like :periodeTahun")
	public List<PlPeriodeLock> getListIndex(@Param("kodeKantorJr") String kodeKantorJr, 
			@Param("periodeBulan")String periodeBulan, 
			@Param("periodeTahun")String periodeTahun);
	
//	@Query("select a from PlPeriodeLock a where ")
//	public List<PlPeriodeLock> getLastPeriode();
	
	@Modifying
	@Query("update PlPeriodeLock a set a.status = 'CLOSED' where a.kodeKantorJr = :kodeKantor and "
			+ " a.creationDate = " + "(select max(b.creationDate) from PlPeriodeLock b)")
	public int updateLock(@Param("kodeKantor")String kodeKantor);
	
//	@Modifying
//	@Query("update PlPeriodeLock a set a.status = :status, a.updatedBy :updatedBy, a.updatedDate :updatedDate where "
//			+ "a.kodeKantorJd = :kodeKantor and a.periodeBulan = :bulan and a.periodeTahun = :tahun")
//	public void updateByLock(
//			@Param("kodeKantor")String kodeKantor,
//			@Param("bulan")String bulan,
//			@Param("tahun")String tahun,
//			@Param("status")String status,
//			@Param("updatedBy")String updatedBy,
//			@Param("updatedDate")Date updatedDate)
//			;
	
	@Query("select a.kodeInstansi from PlInstansi a where a.flagEnable = 'Y'")
	public List<String> getAllInstansiCode();

}
