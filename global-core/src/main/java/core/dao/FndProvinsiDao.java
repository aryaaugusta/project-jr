package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.FndProvinsi;

public interface FndProvinsiDao extends JpaRepository<FndProvinsi, String>{

@Query("select a from FndProvinsi a where "
		+ "a.kodeProvinsi like :kdProvinsi and "
		+ "a.namaProvinsi like :nmProvinsi ")
public FndProvinsi getProvinsi(@Param("kdProvinsi") String kdProvinsi,
		@Param("nmProvinsi") String nmProvinsi);

@Query("select a from FndProvinsi a where a.kodeProvinsi = :kdProvinsi")
public FndProvinsi getProfPk(@Param("kdProvinsi") String kdProvinsi);

	
}
