package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlMappingPolda;

public interface PlMappingPoldaDao extends JpaRepository<PlMappingPolda, String>{

	@Query("select a from PlMappingPolda a where "
			+ "a.kodeInstansi = :kodeInstansi ")
	public PlMappingPolda getProvinceByKodeInstansi(@Param("kodeInstansi") String kodeInstansi);

}
