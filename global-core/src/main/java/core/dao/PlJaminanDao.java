package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import core.model.PlJaminan;

public interface PlJaminanDao extends JpaRepository<PlJaminan, String>{
	
	@Query("select distinct a.lingkupJaminan from PlJaminan a")
	public List<Object> getLingkupJaminan();

}
