package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.DukcapilRefCode;
import core.model.DukcapilWn;

public interface DukcapilWinDao extends JpaRepository<DukcapilWn, String>, DukcapilWinCustomDao {
	
	@Query("select a from DukcapilRefCode a where a.rfDomain = 'KODE HUB KELUARGA' and a.rvMeaning like '%'|| :meaning ||'%'")
	public List<DukcapilRefCode> getDukcapil(@Param("meaning")String meaning);
	
	@Query("select a from DukcapilRefCode a where a.rfDomain = 'KODE HUB KELUARGA'")
	public List<DukcapilRefCode> getDukcapil();

}
