package core.dao;

import java.util.List;

public interface FndBankCustomDao {
	
	public List<Object[]> getListDataBank(String search);

}
