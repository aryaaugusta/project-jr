package core.dao;

import core.model.FndCamat;

public interface FndCamatCustomDao {
	
	public int saveLokasi(FndCamat lok);	
	public int updateLokasi(FndCamat lok);
	public int saveCamat(FndCamat lok);
}
