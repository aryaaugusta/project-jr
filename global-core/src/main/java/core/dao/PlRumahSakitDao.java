package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlRekeningR;
import core.model.PlRumahSakit;
import core.model.PlRumahSakitPK;

public interface PlRumahSakitDao extends JpaRepository<PlRumahSakit, PlRumahSakitPK>, PlRumahSakitCustomDao{
	
	@Query("select a from PlRumahSakit a where "
			+ "a.kodeRumahsakit like :kanWil and "
			+ "(a.kodeRumahsakit like :kodeNama or "
			+ "a.deskripsi like :kodeNama) and "
			+ "(a.kodeRumahsakit like :search or "
			+ "a.deskripsi like :search or "
			+ "a.flagEnable like :search or "
			+ "a.jenisRs like :search) order by a.kodeRumahsakit asc ")
	public List<PlRumahSakit> getDataList(
			@Param("kanWil") String kanWil,
			@Param("kodeNama") String kodeNama,
			@Param("search") String search);
	
	@Query("select a from PlRekeningR a where a.kodeRumahsakit like :rsCode")
	public List<PlRekeningR> getRekeningByRsCode(@Param("rsCode")String rsCode);
	
//	@Modifying
//	@Query("update PlRumahSakit a set a.flagEnable = 'N' ")
}
