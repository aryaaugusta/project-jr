package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.FndKabkota;

public interface FndKabKotaDao extends JpaRepository<FndKabkota, String>{

	@Query("select a from FndKabkota a where a.kodeKabkota = :kodeKab")
	public FndKabkota getKabKota(@Param("kodeKab") String kodeKab);
}
