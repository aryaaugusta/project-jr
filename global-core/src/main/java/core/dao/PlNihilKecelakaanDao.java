package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import core.model.PlNihilKecelakaan;

public interface PlNihilKecelakaanDao extends JpaRepository<PlNihilKecelakaan, String>{

}
