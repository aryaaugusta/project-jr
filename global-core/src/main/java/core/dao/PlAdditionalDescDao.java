package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlAdditionalDesc;

public interface PlAdditionalDescDao extends JpaRepository<PlAdditionalDesc, String>{
	@Query("select a from PlAdditionalDesc a where a.noBerkas like :noBerkas")
	public List<PlAdditionalDesc> getOnebyNoberkas(@Param("noBerkas") String noBerkas);
}
