package core.dao;

import core.model.DukcapilWn;
import core.model.DukcapilWnFam;

public interface DukcapilWinCustomDao {
	public int saveDukcapil(DukcapilWn dukcapilWin);
	
	public int saveDukcapilFam(DukcapilWnFam dukcapilWnFam);
}
