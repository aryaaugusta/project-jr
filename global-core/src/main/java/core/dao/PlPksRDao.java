package core.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlPksR;
import core.model.PlPksRPK;

public interface PlPksRDao extends JpaRepository<PlPksR, PlPksRPK>, PlPksRCustomDao {
	
	
	@Query("select a from PlPksR a where "
			+ "a.kodeRumahsakit like :kodeRumahsakit")
	public Page<PlPksR> getPlPksRByKodeRumahSakit(
			@Param("kodeRumahsakit")String kodeRumahsakit, 
			Pageable page);
	
	@Query("select a from PlPksR a where "
			+ "a.kodeRumahsakit like :kodeRumahsakit order by a.tglLakuAkhir")
	public List<PlPksR> getPlPksRByKodeRumahSakitNoPage(
			@Param("kodeRumahsakit")String kodeRumahsakit);

	@Query("select a from PlPksR a where "
			+ "a.kodeRumahsakit like :kodeRumahsakit ")
	public List<PlPksR> getPlPksRByKodeRumahSakitAndKantorJr(
			@Param("kodeRumahsakit")String kodeRumahsakit);
	@Query("select max(a.idRsPks) from PlPksR a where a.kodeRumahsakit = :kodeRS")
	public String getMaxIdRSPKSByKoodeRS(@Param("kodeRS")String kodeRS);
//	
//	public PlPksR getLatest
	@Modifying
	@Query(value="update pl_pks_rs set FLAG_AKTIF = 'N' where kode_rumahsakit = :kodeRS and (tgl_laku_akhir < "+
		  "(select max(a.tgl_laku_akhir) from pl_pks_rs a where a.kode_rumahsakit = :kodeRS) or tgl_laku_akhir is null)", nativeQuery=true)	
	public int rebuildFlagAktif(@Param("kodeRS")String kodeRS);
	
	@Modifying
	@Query("update PlPksR a set a.flagAktif = 'N' where a.kodeRumahsakit = :kodeRS and a.tglLakuAkhir < :tglLakuAkhir")
	public int updateFlagAktif(@Param("kodeRS") String kodeRS, @Param("tglLakuAkhir") Date tglLakuAkhir);
	
	@Query("select max(a.tglLakuAkhir) from PlPksR a where a.kodeRumahsakit = :kodeRS ")
	public Date getTglLakuAkhirMax(@Param("kodeRS")String kodeRS);

	@Query("select a from PlPksR a where a.idRsPks = :idRsPks")
	public PlPksR getPksById(@Param("idRsPks")String idRsPks);

	@Modifying
	@Query("delete from PlPksR a where a.idRsPks =  :idRsPks")
	public void deletePks(@Param("idRsPks")String idRsPks);
}
