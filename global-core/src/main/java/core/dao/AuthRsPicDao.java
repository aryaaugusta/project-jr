package core.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.AuthRsPic;
import core.model.AuthRsPicPK;
import core.model.AuthUser;

public interface AuthRsPicDao extends JpaRepository<AuthRsPic, AuthRsPicPK>, AuthRsPicCustomDao {
	
	@Query(value = "select a from AuthUser a where "
//			+ "a.login like :search or "
//			+ "a.userName like :search or "
//			+ "a.description like :search or "
			+ "a.attribute1 like :search "
//			+ "a.attribute4 like :search "
			)
	public List<AuthUser> getListUser(@Param("search")String search);
	
	@Query(value = "select a, b.deskripsi, b.kodeRumahsakit from AuthRsPic a, PlRumahSakit b where a.kodeRumahsakit = b.kodeRumahsakit and "
			+ "a.userLogin = :userLogin ")
	public List<Object[]> getRsByUserLoginJpa(@Param("userLogin")String userLogin);
	
	@Query("select a.kodeRumahsakit, a.deskripsi from PlRumahSakit a where "
			+ "not exists (select b.kodeRumahsakit from AuthRsPic b where "
			+ "a.kodeRumahsakit = b.kodeRumahsakit) order by a.kodeRumahsakit asc")
	public List<Object[]> getListRs();
	
	
	@Query("select a.kodeRumahsakit, a.deskripsi from PlRumahSakit a where "
			+ "not exists (select b.kodeRumahsakit from AuthRsPic b where "
			+ "a.kodeRumahsakit = b.kodeRumahsakit) and a.kodeRumahsakit like :search "
			+ "order by a.kodeRumahsakit asc")
	public List<Object[]> getListRsByUserLogin(@Param("search") String login);
	
	
	
	@Query("select distinct a.deskripsi, a.kodeRumahsakit from PlRumahSakit a, AuthRsPic b, AuthUser c where "
			+ "a.kodeRumahsakit = b.kodeRumahsakit and "
			+ "b.userLogin like :userLogin and "
			+ "(a.deskripsi like :search or "
			+ "a.kodeRumahsakit like :search)")
	public List<Object[]> getRsByUserLogin(
			@Param("userLogin")String userLogin, 
			@Param("search")String search);
	
}
