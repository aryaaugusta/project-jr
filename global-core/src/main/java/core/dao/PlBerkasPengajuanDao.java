package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlBerkasPengajuan;
import core.model.PlBerkasPengajuanPK;

public interface PlBerkasPengajuanDao extends JpaRepository<PlBerkasPengajuan, PlBerkasPengajuanPK>{

	@Modifying(clearAutomatically = true)
	@Query(
			"UPDATE PlBerkasPengajuan pl "
			+ "SET pl.lastUpdatedBy = 't123' "
			+ "WHERE pl.noBerkas = :#{#berkas.noBerkas} "
			+ "AND pl.kodeBerkas = :#{#berkas.kodeBerkas}")
	public void updateBerkas(@Param("berkas") PlBerkasPengajuan berkas);
	
	@Query("SELECT pl FROM PlBerkasPengajuan pl WHERE pl.kodeBerkas = ?1 AND pl.noBerkas = ?2")
	public PlBerkasPengajuan findBerkas(String kodeBerkas, String noBerkas);
	
}
