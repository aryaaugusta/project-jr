package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import core.model.PlBerkasEcmsH;

public interface PlBerkasEcmsHDao extends JpaRepository<PlBerkasEcmsH, String>{

}
