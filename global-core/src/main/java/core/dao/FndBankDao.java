package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.FndBank;

public interface FndBankDao extends JpaRepository<FndBank, String>, FndBankCustomDao {
	
	
	@Query("select a from FndBank a where "
			+ "a.kodeBank like :search or "
			+ "a.kodeBank2 like :search or "
			+ "a.namaBank like :search")
	public List<FndBank> getDataList(@Param("search")String search);
	
	@Query("select a from FndBank a where a.kodeBank = :kodeBank")
	public List<FndBank> getBankFromKode(@Param("kodeBank")String kodeBank);
	
	@Query("select a from FndBank a where a.kodeBank2 = :kodeBank2")
	public List<FndBank> getBankFromKode2(@Param("kodeBank2")String kodeBank2);
	
	

}
