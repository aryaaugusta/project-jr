package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlInstansi;
import core.model.PlInstansiPK;
import core.model.PlPengajuanSantunan;

public interface PlInstansiDao extends JpaRepository<PlInstansi, PlInstansiPK>, PlInstansiCustomDao{
	
	@Query("select a.kodeInstansi,a.deskripsi,a.createdBy,a.creationDate,"
			+ "a.flagEnable,c.name,d.name "
			+ "from PlInstansi a, PlMappingPolda b, KorlantasProvince c, KorlantasDistrict d "
			+ "where a.kodeInstansi = b.kodeInstansi "
			+ "and b.idDistricts = d.id "
			+ "and d.provinceId = c.id "
			+ "and a.kodeInstansi like UPPER(:kodeInstansi) "
			+ "and (a.kodeInstansi like :search or "
			+ "a.deskripsi like :search) "
			+ "and a.flagEnable = 'Y'")
	public List<Object[]> getDataList(
			@Param("kodeInstansi")String kodeInstansi,
			@Param("search")String search);
	
	/*@Query(value=" SELECT I.KODE_INSTANSI, I.DESKRIPSI, I.FLAG_ENABLE, " + 
			" DECODE(VW.KODE_INSTANSI, NULL, 'inline', 'none') AS DIS_DEL, " + 
			" (I.KODE_INSTANSI || '|' || I.DESKRIPSI || '|' || I.FLAG_ENABLE || '|' || KD.PROVINCE_ID || '|' || MP.ID_DISTRICTS) AS INSTANSI_ARRAY " + 
			" FROM PL_INSTANSI I " + 
			" JOIN PL_MAPPING_POLDA MP ON MP.KODE_INSTANSI = I.KODE_INSTANSI " + 
			" LEFT JOIN KORLANTAS_DISTRICTS KD ON TO_CHAR(KD.ID) = MP.ID_DISTRICTS " + 
			" LEFT JOIN " + 
			" (SELECT DISTINCT(D.KODE_INSTANSI) AS KODE_INSTANSI " + 
			" FROM PL_DATA_KECELAKAAN D " + 
			" WHERE D.KODE_INSTANSI LIKE (SUBSTR(:params,1,2) || '0' || SUBSTR(:params,5,2) || '%')) VW ON VW.KODE_INSTANSI = I.KODE_INSTANSI " + 
			" WHERE I.DESKRIPSI NOT LIKE '%Migrasi%' " +
			" AND I.KODE_INSTANSI LIKE (SUBSTR(:params,1,2) || '0' || SUBSTR(:params,5,2) || '%') " +
			" AND (I.KODE_INSTANSI LIKE :search or I.DESKRIPSI LIKE :search) "+
			" ORDER BY KODE_INSTANSI; ")
	public List<Object[]> getList(@Param("params") String kodeInstansi,
			@Param("search") String search);*/
	
	//report
	@Query("select a, b from PlInstansi a, PlMappingPolda b where upper(a.deskripsi) not like upper('%migrasi%') "
			+ "and a.kodeInstansi = b.kodeInstansi and a.kodeInstansi like :instansi")
	public List<Object[]> getInstansi(@Param("instansi") String instansi);

	@Query("select a from PlInstansi a where a.kodeInstansi = :insCode")
	public List<PlInstansi> getInstansiByCode(@Param("insCode")String insCode);
	
	@Query("select a from PlPengajuanSantunan a where a.diajukanDi = :insCode")
	public List<PlPengajuanSantunan> getPengajuanFromInstansi(@Param("insCode")String insCode);
	
	@Query("select a from PlInstansi a where a.kodeInstansi = :instanseCode")
	public PlInstansi getInstansiById(@Param("instanseCode")String instanseCode);
}
