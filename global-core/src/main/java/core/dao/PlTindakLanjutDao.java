package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.PlTindakLanjut;

public interface PlTindakLanjutDao extends JpaRepository<PlTindakLanjut, String>,PlTindakLanjutCustomDao{

	@Query("select a from PlTindakLanjut a where a.idTlRegister like :idTl")
	public List<PlTindakLanjut> findById2(@Param("idTl")String idTl);
	
	@Query("select a,b.rvMeaning from PlTindakLanjut a,DasiJrRefCode b "
			+ "where a.tindakLanjutFlag = b.rvLowValue "
			+ "and a.noRegister = :noRegister "
			+ "and b.rvDomain = 'TINDAK LANJUT REGISTER' "
			+ "and b.flagEnable = 'Y' order by a.createdDate desc")
	public List<Object[]> findByNoRegister(@Param("noRegister") String noRegister);
	
	@Query("select a.idTlRegister, a.tglTindakLanjut from PlTindakLanjut a where a.noRegister like :noRegister "
			+ "and a.tglTindakLanjut = (select max(b.tglTindakLanjut) from PlTindakLanjut b "
			+ "where b.tglTindakLanjut = a.tglTindakLanjut)")
	public List<Object[]> findLastTlByNoRegister(@Param("noRegister") String noRegister);
	
	
}
