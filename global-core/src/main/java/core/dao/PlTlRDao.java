package core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import core.model.DasiJrRefCode;
import core.model.PlTlR;

public interface PlTlRDao extends JpaRepository<PlTlR, String>{

	@Query("select (sum(jumlahPengajuan1)+sum(jmlAmbl)+sum(jmlP3k))as JUMLAH_JAMINAN, "
          + "(count(jumlahPengajuan1)) as COUNT_JAMINAN "
          + "from PlTlR pl "
          + "where kodeKantorJr LIKE :kodeKantor "
          + "and kodeRumahSakit like :kodeKantor "
          + "and noSuratJaminan IS NOT NULL "
          + "and flagBayar NOT IN ('0') ")
	public List<Object[]> findJmlJaminanGLRS(@Param("kodeKantor") String kodeKantor);
	
	@Query("select (sum(jumlahPengajuanSmt)+sum(jmlAmblSmt)+sum(jmlP3kSmt))as JUMLAH_JAMINAN, "
	          + "(count(jumlahPengajuan1)) as COUNT_JAMINAN "
	          + "from PlTlR pl "
	          + "where kodeKantorJr LIKE :kodeKantor "
	          + "and kodeRumahSakit like :kodeKantor "
	          + "and noSuratJaminan IS NOT NULL " 
	          + "and flagBayar in ('1','5')")
	public List<Object[]> findJmlJaminanBayarGLRS(@Param("kodeKantor") String kodeKantor);
	
	@Query("select (sum(jumlahPengajuan1)+sum(jmlAmbl)+sum(jmlP3k))as JUMLAH_JAMINAN, "
	          + "(count(jumlahPengajuan1)) as COUNT_JAMINAN "
	          + "from PlTlR pl "
	          + "where kodeKantorJr LIKE :kodeKantor "
	          + "and kodeRumahSakit like :kodeKantor "
	          + "and noSuratJaminan IS NOT NULL "
	          + "and flagBayar in ('4')")
	public List<Object[]> findJmlJaminanHutangGLRS(@Param("kodeKantor") String kodeKantor);
	
	@Query("select (count(jumlahPengajuan1)) as COUNT_JAMINAN "
			  + "from PlTlR pl "
	          + "where kodeKantorJr LIKE :kodeKantor "
	          + "and kodeRumahSakit like :kodeKantor "
	          + "and noSuratJaminan IS NOT NULL "
	          + "and flagBayar = '2'")
	public List<Long> findJmlJaminanBatalGLRS(@Param("kodeKantor") String kodeKantor);
	
	@Query("select a from PlTlR a where a.noSuratJaminan like :noSuratJaminan")
	public List<PlTlR> getPlTlRsByNoSuratJaminan(@Param("noSuratJaminan")String noSuratJaminan);

	@Query("select a from PlTlR a where a.noRegister like :noRegister order by a.idJaminan")
	public List<PlTlR> getPlTlRsByNoRegister(@Param("noRegister")String noRegister);
	
	@Query("select a from PlTlR a where a.idJaminan like :idJaminan")
	public List<PlTlR> getPlTlRsByIdJaminan(@Param("idJaminan")String idJaminan);
	
	@Query("select MAX(a.noSuratJaminan) from PlTlR a where a.noRegister like :noRegister")
	public List<String> getMaxNoSuratJaminan(@Param("noRegister")String noRegister);

	@Query("select distinct a.noRegister from PlTlR a where a.noRegister like :noRegister")
	public List<String> getSize(@Param("noRegister")String noRegister);

}
