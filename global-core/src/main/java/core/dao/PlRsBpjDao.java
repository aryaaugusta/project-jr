package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import core.model.PlRsBpj;
import core.model.PlRsBpjPK;

public interface PlRsBpjDao extends JpaRepository<PlRsBpj, PlRsBpjPK> {

	
}
