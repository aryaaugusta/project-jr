package core.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import core.model.PlBerkasEcm;

public interface PlBerkasEcmDao extends JpaRepository<PlBerkasEcm, String> {
//	public int saveData(PlBerkasEcm plBerkasEcm); 
//	public int updateData(PlBerkasEcm plBerkasEcm);
//	public int deleteData(PlBerkasEcm plBerkasEcm);
//	public PlBerkasEcm findOne(String id);
}
