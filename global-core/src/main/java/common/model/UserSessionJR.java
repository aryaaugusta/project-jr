package common.model;

import java.io.Serializable;

/**
 * Class for maintain user session
 * 
 *
 */
public class UserSessionJR implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String loginID = "1";
	private String userID = "1";
	private String userName = "otorisator";
	private Boolean expired = false;
	private String loginDesc = "otorisator";
	private String userLocale = "ID"; //Attribute1
	private String kantor = "JR Pusat"; //Attribute1
	private String phoneNumber = "6281380294164"; //attrubure2
	private String groupCode = "1"; //groupFromAuthGroupMember
	private String sessionID = "1";
	private String namaKantor = "Kantor JR Pusat";
	private String levelKantor = "Pusat";
	private String perwakilanKantor = "Cabang Jakarta";
	private String userLdap = "1"; //Attribute5
	private String sessionCode = "1";
	
	public UserSessionJR(String loginID, String userID, String userName,
			Boolean expired, String loginDesc, String userLocale,
			String kantor, String phoneNumber, String groupCode,
			String sessionID, String namaKantor, String levelKantor,
			String perwakilanKantor, String userLdap) {
		super();
		this.loginID = loginID;
		this.userID = userID;
		this.userName = userName;
		this.expired = expired;
		this.loginDesc = loginDesc;
		this.userLocale = userLocale;
		this.kantor = kantor;
		this.phoneNumber = phoneNumber;
		this.groupCode = groupCode;
		this.sessionID = sessionID;
		this.namaKantor = namaKantor;
		this.levelKantor = levelKantor;
		this.perwakilanKantor = perwakilanKantor;
		this.userLdap = userLdap;
	}

	public UserSessionJR(UserSessionJR copy){
		this.loginID = copy.getLoginID();
		this.userID = copy.getUserID();
		this.userName = copy.getUserName();
		this.expired = copy.getExpired();
		this.loginDesc = copy.getLoginDesc();
		this.userLocale = copy.getUserLocale();
		this.kantor = copy.getKantor();
		this.phoneNumber = copy.getPhoneNumber();
		this.groupCode = copy.getGroupCode();
		this.sessionID = copy.getSessionID();
		this.namaKantor = copy.getNamaKantor();
		this.levelKantor = copy.getLevelKantor();
		this.perwakilanKantor = copy.getPerwakilanKantor();
		this.userLdap = copy.getUserLdap();
	}
	
	public UserSessionJR(){
		
	}


	public String getLoginID() {
		return loginID;
	}


	public void setLoginID(String loginID) {
		this.loginID = loginID;
	}


	public String getUserID() {
		return userID;
	}


	public void setUserID(String userID) {
		this.userID = userID;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public Boolean getExpired() {
		return expired;
	}


	public void setExpired(Boolean expired) {
		this.expired = expired;
	}


	public String getLoginDesc() {
		return loginDesc;
	}


	public void setLoginDesc(String loginDesc) {
		this.loginDesc = loginDesc;
	}


	public String getUserLocale() {
		return userLocale;
	}


	public void setUserLocale(String userLocale) {
		this.userLocale = userLocale;
	}


	public String getPhoneNumber() {
		return phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getGroupCode() {
		return groupCode;
	}


	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}


	public String getSessionID() {
		return sessionID;
	}


	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public String getKantor() {
		return kantor;
	}

	public void setKantor(String kantor) {
		this.kantor = kantor;
	}

	public String getNamaKantor() {
		return namaKantor;
	}

	public void setNamaKantor(String namaKantor) {
		this.namaKantor = namaKantor;
	}

	public String getLevelKantor() {
		return levelKantor;
	}

	public void setLevelKantor(String levelKantor) {
		this.levelKantor = levelKantor;
	}

	public String getPerwakilanKantor() {
		return perwakilanKantor;
	}

	public void setPerwakilanKantor(String perwakilanKantor) {
		this.perwakilanKantor = perwakilanKantor;
	}

	public String getUserLdap() {
		return userLdap;
	}

	public void setUserLdap(String userLdap) {
		this.userLdap = userLdap;
	}

	public String getSessionCode() {
		return sessionCode;
	}

	public void setSessionCode(String sessionCode) {
		this.sessionCode = sessionCode;
	}
	
	
}
