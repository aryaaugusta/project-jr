package common.model;

import java.io.Serializable;
import java.util.List;

public class RestResponseDukcapil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Object content;
	private boolean lastPage;
	private int numberOfElements;
	private Object sort;
	private int totalElements;
	private boolean firstPage;
	private int number;
	private int size;
	
	public RestResponseDukcapil(){
		
	}
	
	public RestResponseDukcapil(Object content, int numberOfElements, int size) {
		super();
		this.content = content;
		this.numberOfElements = numberOfElements;
		this.size = size;
	}

	public RestResponseDukcapil(Object content, int numberOfElements,
			int totalElements, int size) {
		super();
		this.content = content;
		this.numberOfElements = numberOfElements;
		this.totalElements = totalElements;
		this.size = size;
	}

	public RestResponseDukcapil(Object content, boolean lastPage,
			int numberOfElements, Object sort, int totalElements,
			boolean firstPage, int number, int size) {
		super();
		this.content = content;
		this.lastPage = lastPage;
		this.numberOfElements = numberOfElements;
		this.sort = sort;
		this.totalElements = totalElements;
		this.firstPage = firstPage;
		this.number = number;
		this.size = size;
	}

	public Object getContent() {
		return content;
	}
	public void setContent(Object content) {
		this.content = content;
	}
	public boolean isLastPage() {
		return lastPage;
	}
	public void setLastPage(boolean lastPage) {
		this.lastPage = lastPage;
	}
	public int getNumberOfElements() {
		return numberOfElements;
	}
	public void setNumberOfElements(int numberOfElements) {
		this.numberOfElements = numberOfElements;
	}
	public Object getSort() {
		return sort;
	}
	public void setSort(Object sort) {
		this.sort = sort;
	}
	public int getTotalElements() {
		return totalElements;
	}
	public void setTotalElements(int totalElements) {
		this.totalElements = totalElements;
	}
	public boolean isFirstPage() {
		return firstPage;
	}
	public void setFirstPage(boolean firstPage) {
		this.firstPage = firstPage;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getSize() {
		return size;
	}
	public void setSize(int size) {
		this.size = size;
	}
	
	
}
