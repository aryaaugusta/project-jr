package share;

import java.io.Serializable;


public class PlMappingPoldaDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String deskripsi;
	private String idDistricts;
	private String kodeInstansi;
	private String kodePolda;
	private String namaPolda;
	private String wilayahKpjr;
	
	//added by luthfi
	private String kodeNoLaporan;
	
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getIdDistricts() {
		return idDistricts;
	}
	public void setIdDistricts(String idDistricts) {
		this.idDistricts = idDistricts;
	}
	public String getKodeInstansi() {
		return kodeInstansi;
	}
	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}
	public String getKodePolda() {
		return kodePolda;
	}
	public void setKodePolda(String kodePolda) {
		this.kodePolda = kodePolda;
	}
	public String getNamaPolda() {
		return namaPolda;
	}
	public void setNamaPolda(String namaPolda) {
		this.namaPolda = namaPolda;
	}
	public String getWilayahKpjr() {
		return wilayahKpjr;
	}
	public void setWilayahKpjr(String wilayahKpjr) {
		this.wilayahKpjr = wilayahKpjr;
	}
	public String getKodeNoLaporan() {
		return kodeNoLaporan;
	}
	public void setKodeNoLaporan(String kodeNoLaporan) {
		this.kodeNoLaporan = kodeNoLaporan;
	}
	

}
