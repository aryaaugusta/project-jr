package share;

import java.io.Serializable;

public class PlBerkasEcmsContainDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String dDocname;
	private String kodeBerkas;
	private String noRegEcms;
	public String getdDocname() {
		return dDocname;
	}
	public void setdDocname(String dDocname) {
		this.dDocname = dDocname;
	}
	public String getKodeBerkas() {
		return kodeBerkas;
	}
	public void setKodeBerkas(String kodeBerkas) {
		this.kodeBerkas = kodeBerkas;
	}
	public String getNoRegEcms() {
		return noRegEcms;
	}
	public void setNoRegEcms(String noRegEcms) {
		this.noRegEcms = noRegEcms;
	}
	
	
}
