package share;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class LaporanDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String tipeLaporanN = "N";
	public static final String tipeLaporanC = "C";
	public static final String tipeLaporanL = "L";
	public static final String tipeLaporanP = "P";
	
	
	private String namaLaporan;
	private String kodeLaporan;
	private String reportCode;
	private String tanggalLaporan;
	private String no;
	private Map<String, Object> param = new HashMap<>();
	public String getNamaLaporan() {
		return namaLaporan;
	}
	public void setNamaLaporan(String namaLaporan) {
		this.namaLaporan = namaLaporan;
	}
	public String getKodeLaporan() {
		return kodeLaporan;
	}
	public void setKodeLaporan(String kodeLaporan) {
		this.kodeLaporan = kodeLaporan;
	}
	public String getTanggalLaporan() {
		return tanggalLaporan;
	}
	public void setTanggalLaporan(String tanggalLaporan) {
		this.tanggalLaporan = tanggalLaporan;
	}
	public Map<String, Object> getParam() {
		return param;
	}
	public void setParam(Map<String, Object> param) {
		this.param = param;
	}
	public LaporanDto(String namaLaporan, String kodeLaporan, String reportCode) {
		super();
		this.namaLaporan = namaLaporan;
		this.kodeLaporan = kodeLaporan;
		this.reportCode = reportCode;
	}
	public LaporanDto(String namaLaporan, String kodeLaporan, String reportCode, String no) {
		super();
		this.namaLaporan = namaLaporan;
		this.kodeLaporan = kodeLaporan;
		this.reportCode = reportCode;
		this.no = no;
	}
	public String getReportCode() {
		return reportCode;
	}
	public void setReportCode(String reportCode) {
		this.reportCode = reportCode;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	
	
}
