package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PlKorbanRsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String alamat;
	private BigDecimal biaya;
	private String createdBy;
	private Date createdDate;
	private String diagnosa;
	private String dokterBerwenang;
	private String flagDobel;
	private String flagMultiJaminan;
	private String jenisKelamin;
	private String jenisPelayanan;
	private String jenisTindakan;
	private BigDecimal jumlahDibayarkan;
	private String keterangan;
	private String kodeInstansi;
	private String kodeKejadian;
	private String kodeRumahSakit;
	private Date lastUpdatedDate;
	private String lokasiKejadian;
	private String namaKorban;
	private String nik;
	private String noBerkas;
	private String noKartu;
	private String noTelp;
	private String rekamMedis;
	private String ruangan;
	private String sifatCidera;
	private String statusJaminan;
	private String statusKlaim;
	private Date tglKejadian;
	private Date tglMasukRs;
	private Date tglProses;
	private String tipeId;
	private String umur;
	
	//monitoring data laka rs
	private String namaRs;
	private String batasKunjungan;
	private String batasKesimpulan;
	private String status;
	private String namaInstansi;
	private String pic;
	private Date tglResponse;
	private String petugas;
	
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public BigDecimal getBiaya() {
		return biaya;
	}
	public void setBiaya(BigDecimal biaya) {
		this.biaya = biaya;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getDiagnosa() {
		return diagnosa;
	}
	public void setDiagnosa(String diagnosa) {
		this.diagnosa = diagnosa;
	}
	public String getDokterBerwenang() {
		return dokterBerwenang;
	}
	public void setDokterBerwenang(String dokterBerwenang) {
		this.dokterBerwenang = dokterBerwenang;
	}
	public String getFlagDobel() {
		return flagDobel;
	}
	public void setFlagDobel(String flagDobel) {
		this.flagDobel = flagDobel;
	}
	public String getFlagMultiJaminan() {
		return flagMultiJaminan;
	}
	public void setFlagMultiJaminan(String flagMultiJaminan) {
		this.flagMultiJaminan = flagMultiJaminan;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public String getJenisPelayanan() {
		return jenisPelayanan;
	}
	public void setJenisPelayanan(String jenisPelayanan) {
		this.jenisPelayanan = jenisPelayanan;
	}
	public String getJenisTindakan() {
		return jenisTindakan;
	}
	public void setJenisTindakan(String jenisTindakan) {
		this.jenisTindakan = jenisTindakan;
	}
	public BigDecimal getJumlahDibayarkan() {
		return jumlahDibayarkan;
	}
	public void setJumlahDibayarkan(BigDecimal jumlahDibayarkan) {
		this.jumlahDibayarkan = jumlahDibayarkan;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getKodeInstansi() {
		return kodeInstansi;
	}
	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}
	public String getKodeKejadian() {
		return kodeKejadian;
	}
	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}
	public String getKodeRumahSakit() {
		return kodeRumahSakit;
	}
	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getLokasiKejadian() {
		return lokasiKejadian;
	}
	public void setLokasiKejadian(String lokasiKejadian) {
		this.lokasiKejadian = lokasiKejadian;
	}
	public String getNamaKorban() {
		return namaKorban;
	}
	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}
	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getNoKartu() {
		return noKartu;
	}
	public void setNoKartu(String noKartu) {
		this.noKartu = noKartu;
	}
	public String getNoTelp() {
		return noTelp;
	}
	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
	public String getRekamMedis() {
		return rekamMedis;
	}
	public void setRekamMedis(String rekamMedis) {
		this.rekamMedis = rekamMedis;
	}
	public String getRuangan() {
		return ruangan;
	}
	public void setRuangan(String ruangan) {
		this.ruangan = ruangan;
	}
	public String getSifatCidera() {
		return sifatCidera;
	}
	public void setSifatCidera(String sifatCidera) {
		this.sifatCidera = sifatCidera;
	}
	public String getStatusJaminan() {
		return statusJaminan;
	}
	public void setStatusJaminan(String statusJaminan) {
		this.statusJaminan = statusJaminan;
	}
	public String getStatusKlaim() {
		return statusKlaim;
	}
	public void setStatusKlaim(String statusKlaim) {
		this.statusKlaim = statusKlaim;
	}
	public Date getTglKejadian() {
		return tglKejadian;
	}
	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}
	public Date getTglMasukRs() {
		return tglMasukRs;
	}
	public void setTglMasukRs(Date tglMasukRs) {
		this.tglMasukRs = tglMasukRs;
	}
	public Date getTglProses() {
		return tglProses;
	}
	public void setTglProses(Date tglProses) {
		this.tglProses = tglProses;
	}
	public String getTipeId() {
		return tipeId;
	}
	public void setTipeId(String tipeId) {
		this.tipeId = tipeId;
	}
	public String getUmur() {
		return umur;
	}
	public void setUmur(String umur) {
		this.umur = umur;
	}
	public String getNamaRs() {
		return namaRs;
	}
	public void setNamaRs(String namaRs) {
		this.namaRs = namaRs;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getBatasKunjungan() {
		return batasKunjungan;
	}
	public void setBatasKunjungan(String batasKunjungan) {
		this.batasKunjungan = batasKunjungan;
	}
	public String getBatasKesimpulan() {
		return batasKesimpulan;
	}
	public void setBatasKesimpulan(String batasKesimpulan) {
		this.batasKesimpulan = batasKesimpulan;
	}
	public String getNamaInstansi() {
		return namaInstansi;
	}
	public void setNamaInstansi(String namaInstansi) {
		this.namaInstansi = namaInstansi;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public Date getTglResponse() {
		return tglResponse;
	}
	public void setTglResponse(Date tglResponse) {
		this.tglResponse = tglResponse;
	}
	public String getPetugas() {
		return petugas;
	}
	public void setPetugas(String petugas) {
		this.petugas = petugas;
	}
	
	

}
