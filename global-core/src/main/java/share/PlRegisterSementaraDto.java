package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class PlRegisterSementaraDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String alamatPemohon;
	private String asalBerkasFlag;
	private String bpkCreator;
	private String cideraKorban;
	private String createdBy;
	private Date createdDate;
	private String dilimpahkanKe;
	private String idKorbanKecelakaan;
	private String jenisIdentitas;
	private BigDecimal jmlAmbl;
	private BigDecimal jmlP3k;
	private BigDecimal jumlahPengajuan1;
	private BigDecimal jumlahPengajuan2;
	private String kodeHubunganKorban;
	private String kodeKantorJr;
	private String kodeRumahSakit;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String namaPemohon;
	private String noBpk;
	private String noIdentitas;
	private String noRegister;
	private String statusKrbnRs;
	private String telpPemohon;
	private Date tglBpk;
	private Date tglMasukRs;
	private Date tglMdKorban;
	private Date tglRegister;
	
	private Date tglKecelakaan;
	private String namaKorban;
	private String kodeSifatCidera;
	private String TindakLanjut;
	private String tlTerakhir;
	private String cideraDesc;
	
	//pl tindak lanjut
	private String alasanNoclaimTL;
	private String catatanTindakLanjutTL;
	private String createdByTL;
	private Date createdDateTL;
	private String dilimpahkanKeTL;
	private String idTlRegisterTL;
	private BigDecimal jmlKlaimTL;
	private String lastUpdatedByTL;
	private Date lastUpdatedDateTL;
	private String noRegisterTL;
	private String noSuratJaminanTL;
	private String noSuratPanggilanTL;
	private String petugasSurveyTL;
	private Date tglTindakLanjutTL;
	private String tindakLanjutFlagTL;
	
	//PL PENGAJUAN SANTUNAN
	private String noBerkasPS;
	private String transisiFlagPS;
	private String absahFlagPS;
	private String alamatPemohonPS;
	private String bebanPusatFlagPS;
	private String cideraKorbanPS;
	private String createdByPS;
	private Date creationDatePS;
	private String diajukanDiPS;
	private String dilimpahkanKePS;
	private String flagHasilSurveyPS;
	private String idGuidPS;
	private String idKecelakaanPS;
	private String idKorbanKecelakaanPS;
	private String jaminanGandaFlagPS;
	private String jaminanPembayaranPS;
	private String jenisIdentitasPS;
	private BigDecimal jmlPengajuanAmblPS;
	private BigDecimal jmlPengajuanP3kPS;
	private BigDecimal jmlRekomenAmblPS;
	private BigDecimal jmlRekomenP3kPS;
	private BigDecimal jumlahPengajuanLukalukaPS;
	private BigDecimal jumlahPengajuanMeninggalPS;
	private BigDecimal jumlahPengajuanPenguburanPS;
	private String kesimpulanSementaraPS;
	private String kodeHubunganKorbanPS;
	private String kodeJaminanPS;
	private String kodeJenisSurveyPS;
	private String kodePengajuanPS;
	private String lastUpdatedByPS;
	private Date lastUpdatedDatePS;
	private String lengkapFlagPS;
	private String namaPemohonPS;
	private String noIdentitasPS;
	private String noPengajuanPS;
	private String noSuratSurveyPS;
	private String otorisasiFlagPS;
	private String rekomendasiFlagPS;
	private String rekomendasiJaminanPS;
	private BigDecimal rekomendasiJmlLukalukaPS;
	private BigDecimal rekomendasiJmlMeninggalPS;
	private BigDecimal rekomendasiJmlPenguburanPS;
	private String rekomendasiSementaraPS;
	private BigDecimal seqNoPS;
	private String statusProsesPS;
	private Date tglPenerimaanPS;
	private Date tglPengajuanPS;
	private Date tglPenyelesaianPS;
	private Date tglSuratSurveyPS;
	
	private String asalBerkas;
	private String hubunganKorban;
	
	private String lingkupJaminan;
	private List<PlTindakLanjutDto> lisTindakLanjut = new ArrayList<>();
	
	private String jenisSave;

	//GL RS
	private String idJaminan;
	private String tglKejadian;
	private String noSuratJaminan;
	private BigDecimal jmlPengajuan;
	private String berkasSS;
	private String namaRS;
	private String tglRegisterGL;	
	private String alamatRS;
	private String flagBayar; 
	
	//GL RS
	private String idJaminanTlRs;
	private String createdByTlRs;
	private Date createdDateTlRs;
	private String flagBayarTlRs;
	private String flagBayarAwalTlRs;
	private String flagLanjutanTlRs;
	private String flagLimpahanTlRs;
	private String jenisTagihanTlRs;
	private BigDecimal jmlAmblTlRs;
	private BigDecimal jmlAmblSementaraTlRs;
	private BigDecimal jmlAmblSmtTlRs;
	private BigDecimal jmlP3kTlRs;
	private BigDecimal jmlP3kSementaraTlRs;
	private BigDecimal jmlP3kSmtTlRs;
	private BigDecimal jmlPengajuanSementaraTlRs;
	private BigDecimal jumlahPengajuan1TlRs;
	private BigDecimal jumlahPengajuanSmtTlRs;
	private String kodeKantorAsalLimpTlRs;
	private String kodeKantorJrTlRs;
	private String kodeRsSementaraTlRs;
	private String kodeRumahSakitTlRs;
	private String lastUpdatedByTlRs;
	private Date lastUpdatedDateTlRs;
	private String noBerkasTlRs;
	private String noRegisterTlRs;
	private String noSuratJaminanTlRs;
	private String otorisasiAjuSmtTlRs;
	private String otorisasiAwalTlRs;
	private Date tglMasukRsTlRs;
	private Date tglSuratJaminanTlRs;
	private String keteranganOtorisasiTlRs;

	//luthfi
	private Date tglKejadianLaka;
	private Date tglLaporanPolisiLaka;
	private String deskripsiLokasiLaka;
	private String noLaporanPolisiLaka;
	private String namaInstansi;
	private String kasusLaka;
	private String statusKorbanLaka;
	private String penjamin;
	

	
	public String getAlamatPemohon() {
		return alamatPemohon;
	}
	public void setAlamatPemohon(String alamatPemohon) {
		this.alamatPemohon = alamatPemohon;
	}
	public String getAsalBerkasFlag() {
		return asalBerkasFlag;
	}
	public void setAsalBerkasFlag(String asalBerkasFlag) {
		this.asalBerkasFlag = asalBerkasFlag;
	}
	public String getBpkCreator() {
		return bpkCreator;
	}
	public void setBpkCreator(String bpkCreator) {
		this.bpkCreator = bpkCreator;
	}
	public String getCideraKorban() {
		return cideraKorban;
	}
	public void setCideraKorban(String cideraKorban) {
		this.cideraKorban = cideraKorban;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getDilimpahkanKe() {
		return dilimpahkanKe;
	}
	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}
	public String getIdKorbanKecelakaan() {
		return idKorbanKecelakaan;
	}
	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}
	public String getJenisIdentitas() {
		return jenisIdentitas;
	}
	public void setJenisIdentitas(String jenisIdentitas) {
		this.jenisIdentitas = jenisIdentitas;
	}
	public BigDecimal getJmlAmbl() {
		return jmlAmbl;
	}
	public void setJmlAmbl(BigDecimal jmlAmbl) {
		this.jmlAmbl = jmlAmbl;
	}
	public BigDecimal getJmlP3k() {
		return jmlP3k;
	}
	public void setJmlP3k(BigDecimal jmlP3k) {
		this.jmlP3k = jmlP3k;
	}
	public BigDecimal getJumlahPengajuan1() {
		return jumlahPengajuan1;
	}
	public void setJumlahPengajuan1(BigDecimal jumlahPengajuan1) {
		this.jumlahPengajuan1 = jumlahPengajuan1;
	}
	public BigDecimal getJumlahPengajuan2() {
		return jumlahPengajuan2;
	}
	public void setJumlahPengajuan2(BigDecimal jumlahPengajuan2) {
		this.jumlahPengajuan2 = jumlahPengajuan2;
	}
	public String getKodeHubunganKorban() {
		return kodeHubunganKorban;
	}
	public void setKodeHubunganKorban(String kodeHubunganKorban) {
		this.kodeHubunganKorban = kodeHubunganKorban;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getKodeRumahSakit() {
		return kodeRumahSakit;
	}
	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNamaPemohon() {
		return namaPemohon;
	}
	public void setNamaPemohon(String namaPemohon) {
		this.namaPemohon = namaPemohon;
	}
	public String getNoBpk() {
		return noBpk;
	}
	public void setNoBpk(String noBpk) {
		this.noBpk = noBpk;
	}
	public String getNoIdentitas() {
		return noIdentitas;
	}
	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}
	public String getNoRegister() {
		return noRegister;
	}
	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}
	public String getStatusKrbnRs() {
		return statusKrbnRs;
	}
	public void setStatusKrbnRs(String statusKrbnRs) {
		this.statusKrbnRs = statusKrbnRs;
	}
	public String getTelpPemohon() {
		return telpPemohon;
	}
	public void setTelpPemohon(String telpPemohon) {
		this.telpPemohon = telpPemohon;
	}
	public Date getTglBpk() {
		return tglBpk;
	}
	public void setTglBpk(Date tglBpk) {
		this.tglBpk = tglBpk;
	}
	public Date getTglMasukRs() {
		return tglMasukRs;
	}
	public void setTglMasukRs(Date tglMasukRs) {
		this.tglMasukRs = tglMasukRs;
	}
	public Date getTglMdKorban() {
		return tglMdKorban;
	}
	public void setTglMdKorban(Date tglMdKorban) {
		this.tglMdKorban = tglMdKorban;
	}
	public Date getTglRegister() {
		return tglRegister;
	}
	public void setTglRegister(Date tglRegister) {
		this.tglRegister = tglRegister;
	}
	public Date getTglKecelakaan() {
		return tglKecelakaan;
	}
	public void setTglKecelakaan(Date tglKecelakaan) {
		this.tglKecelakaan = tglKecelakaan;
	}
	public String getNamaKorban() {
		return namaKorban;
	}
	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}
	public String getKodeSifatCidera() {
		return kodeSifatCidera;
	}
	public void setKodeSifatCidera(String kodeSifatCidera) {
		this.kodeSifatCidera = kodeSifatCidera;
	}
	public String getTindakLanjut() {
		return TindakLanjut;
	}
	public void setTindakLanjut(String tindakLanjut) {
		TindakLanjut = tindakLanjut;
	}
	public String getTlTerakhir() {
		return tlTerakhir;
	}
	public void setTlTerakhir(String tlTerakhir) {
		this.tlTerakhir = tlTerakhir;
	}
	public String getAlasanNoclaimTL() {
		return alasanNoclaimTL;
	}
	public void setAlasanNoclaimTL(String alasanNoclaimTL) {
		this.alasanNoclaimTL = alasanNoclaimTL;
	}
	public String getCatatanTindakLanjutTL() {
		return catatanTindakLanjutTL;
	}
	public void setCatatanTindakLanjutTL(String catatanTindakLanjutTL) {
		this.catatanTindakLanjutTL = catatanTindakLanjutTL;
	}
	public String getCreatedByTL() {
		return createdByTL;
	}
	public void setCreatedByTL(String createdByTL) {
		this.createdByTL = createdByTL;
	}
	public Date getCreatedDateTL() {
		return createdDateTL;
	}
	public void setCreatedDateTL(Date createdDateTL) {
		this.createdDateTL = createdDateTL;
	}
	public String getDilimpahkanKeTL() {
		return dilimpahkanKeTL;
	}
	public void setDilimpahkanKeTL(String dilimpahkanKeTL) {
		this.dilimpahkanKeTL = dilimpahkanKeTL;
	}
	public String getIdTlRegisterTL() {
		return idTlRegisterTL;
	}
	public void setIdTlRegisterTL(String idTlRegisterTL) {
		this.idTlRegisterTL = idTlRegisterTL;
	}
	public BigDecimal getJmlKlaimTL() {
		return jmlKlaimTL;
	}
	public void setJmlKlaimTL(BigDecimal jmlKlaimTL) {
		this.jmlKlaimTL = jmlKlaimTL;
	}
	public String getLastUpdatedByTL() {
		return lastUpdatedByTL;
	}
	public void setLastUpdatedByTL(String lastUpdatedByTL) {
		this.lastUpdatedByTL = lastUpdatedByTL;
	}
	public Date getLastUpdatedDateTL() {
		return lastUpdatedDateTL;
	}
	public void setLastUpdatedDateTL(Date lastUpdatedDateTL) {
		this.lastUpdatedDateTL = lastUpdatedDateTL;
	}
	public String getNoRegisterTL() {
		return noRegisterTL;
	}
	public void setNoRegisterTL(String noRegisterTL) {
		this.noRegisterTL = noRegisterTL;
	}
	public String getNoSuratJaminanTL() {
		return noSuratJaminanTL;
	}
	public void setNoSuratJaminanTL(String noSuratJaminanTL) {
		this.noSuratJaminanTL = noSuratJaminanTL;
	}
	public String getNoSuratPanggilanTL() {
		return noSuratPanggilanTL;
	}
	public void setNoSuratPanggilanTL(String noSuratPanggilanTL) {
		this.noSuratPanggilanTL = noSuratPanggilanTL;
	}
	public String getPetugasSurveyTL() {
		return petugasSurveyTL;
	}
	public void setPetugasSurveyTL(String petugasSurveyTL) {
		this.petugasSurveyTL = petugasSurveyTL;
	}
	public Date getTglTindakLanjutTL() {
		return tglTindakLanjutTL;
	}
	public void setTglTindakLanjutTL(Date tglTindakLanjutTL) {
		this.tglTindakLanjutTL = tglTindakLanjutTL;
	}
	public String getTindakLanjutFlagTL() {
		return tindakLanjutFlagTL;
	}
	public void setTindakLanjutFlagTL(String tindakLanjutFlagTL) {
		this.tindakLanjutFlagTL = tindakLanjutFlagTL;
	}
	public String getNoBerkasPS() {
		return noBerkasPS;
	}
	public void setNoBerkasPS(String noBerkasPS) {
		this.noBerkasPS = noBerkasPS;
	}
	public String getTransisiFlagPS() {
		return transisiFlagPS;
	}
	public void setTransisiFlagPS(String transisiFlagPS) {
		this.transisiFlagPS = transisiFlagPS;
	}
	public String getAbsahFlagPS() {
		return absahFlagPS;
	}
	public void setAbsahFlagPS(String absahFlagPS) {
		this.absahFlagPS = absahFlagPS;
	}
	public String getAlamatPemohonPS() {
		return alamatPemohonPS;
	}
	public void setAlamatPemohonPS(String alamatPemohonPS) {
		this.alamatPemohonPS = alamatPemohonPS;
	}
	public String getBebanPusatFlagPS() {
		return bebanPusatFlagPS;
	}
	public void setBebanPusatFlagPS(String bebanPusatFlagPS) {
		this.bebanPusatFlagPS = bebanPusatFlagPS;
	}
	public String getCideraKorbanPS() {
		return cideraKorbanPS;
	}
	public void setCideraKorbanPS(String cideraKorbanPS) {
		this.cideraKorbanPS = cideraKorbanPS;
	}
	public String getCreatedByPS() {
		return createdByPS;
	}
	public void setCreatedByPS(String createdByPS) {
		this.createdByPS = createdByPS;
	}
	public Date getCreationDatePS() {
		return creationDatePS;
	}
	public void setCreationDatePS(Date creationDatePS) {
		this.creationDatePS = creationDatePS;
	}
	public String getDiajukanDiPS() {
		return diajukanDiPS;
	}
	public void setDiajukanDiPS(String diajukanDiPS) {
		this.diajukanDiPS = diajukanDiPS;
	}
	public String getDilimpahkanKePS() {
		return dilimpahkanKePS;
	}
	public void setDilimpahkanKePS(String dilimpahkanKePS) {
		this.dilimpahkanKePS = dilimpahkanKePS;
	}
	public String getFlagHasilSurveyPS() {
		return flagHasilSurveyPS;
	}
	public void setFlagHasilSurveyPS(String flagHasilSurveyPS) {
		this.flagHasilSurveyPS = flagHasilSurveyPS;
	}
	public String getIdGuidPS() {
		return idGuidPS;
	}
	public void setIdGuidPS(String idGuidPS) {
		this.idGuidPS = idGuidPS;
	}
	public String getIdKecelakaanPS() {
		return idKecelakaanPS;
	}
	public void setIdKecelakaanPS(String idKecelakaanPS) {
		this.idKecelakaanPS = idKecelakaanPS;
	}
	public String getIdKorbanKecelakaanPS() {
		return idKorbanKecelakaanPS;
	}
	public void setIdKorbanKecelakaanPS(String idKorbanKecelakaanPS) {
		this.idKorbanKecelakaanPS = idKorbanKecelakaanPS;
	}
	public String getJaminanGandaFlagPS() {
		return jaminanGandaFlagPS;
	}
	public void setJaminanGandaFlagPS(String jaminanGandaFlagPS) {
		this.jaminanGandaFlagPS = jaminanGandaFlagPS;
	}
	public String getJaminanPembayaranPS() {
		return jaminanPembayaranPS;
	}
	public void setJaminanPembayaranPS(String jaminanPembayaranPS) {
		this.jaminanPembayaranPS = jaminanPembayaranPS;
	}
	public String getJenisIdentitasPS() {
		return jenisIdentitasPS;
	}
	public void setJenisIdentitasPS(String jenisIdentitasPS) {
		this.jenisIdentitasPS = jenisIdentitasPS;
	}
	public BigDecimal getJmlPengajuanAmblPS() {
		return jmlPengajuanAmblPS;
	}
	public void setJmlPengajuanAmblPS(BigDecimal jmlPengajuanAmblPS) {
		this.jmlPengajuanAmblPS = jmlPengajuanAmblPS;
	}
	public BigDecimal getJmlPengajuanP3kPS() {
		return jmlPengajuanP3kPS;
	}
	public void setJmlPengajuanP3kPS(BigDecimal jmlPengajuanP3kPS) {
		this.jmlPengajuanP3kPS = jmlPengajuanP3kPS;
	}
	public BigDecimal getJmlRekomenAmblPS() {
		return jmlRekomenAmblPS;
	}
	public void setJmlRekomenAmblPS(BigDecimal jmlRekomenAmblPS) {
		this.jmlRekomenAmblPS = jmlRekomenAmblPS;
	}
	public BigDecimal getJmlRekomenP3kPS() {
		return jmlRekomenP3kPS;
	}
	public void setJmlRekomenP3kPS(BigDecimal jmlRekomenP3kPS) {
		this.jmlRekomenP3kPS = jmlRekomenP3kPS;
	}
	public BigDecimal getJumlahPengajuanLukalukaPS() {
		return jumlahPengajuanLukalukaPS;
	}
	public void setJumlahPengajuanLukalukaPS(BigDecimal jumlahPengajuanLukalukaPS) {
		this.jumlahPengajuanLukalukaPS = jumlahPengajuanLukalukaPS;
	}
	public BigDecimal getJumlahPengajuanMeninggalPS() {
		return jumlahPengajuanMeninggalPS;
	}
	public void setJumlahPengajuanMeninggalPS(BigDecimal jumlahPengajuanMeninggalPS) {
		this.jumlahPengajuanMeninggalPS = jumlahPengajuanMeninggalPS;
	}
	public BigDecimal getJumlahPengajuanPenguburanPS() {
		return jumlahPengajuanPenguburanPS;
	}
	public void setJumlahPengajuanPenguburanPS(
			BigDecimal jumlahPengajuanPenguburanPS) {
		this.jumlahPengajuanPenguburanPS = jumlahPengajuanPenguburanPS;
	}
	public String getKesimpulanSementaraPS() {
		return kesimpulanSementaraPS;
	}
	public void setKesimpulanSementaraPS(String kesimpulanSementaraPS) {
		this.kesimpulanSementaraPS = kesimpulanSementaraPS;
	}
	public String getKodeHubunganKorbanPS() {
		return kodeHubunganKorbanPS;
	}
	public void setKodeHubunganKorbanPS(String kodeHubunganKorbanPS) {
		this.kodeHubunganKorbanPS = kodeHubunganKorbanPS;
	}
	public String getKodeJaminanPS() {
		return kodeJaminanPS;
	}
	public void setKodeJaminanPS(String kodeJaminanPS) {
		this.kodeJaminanPS = kodeJaminanPS;
	}
	public String getKodeJenisSurveyPS() {
		return kodeJenisSurveyPS;
	}
	public void setKodeJenisSurveyPS(String kodeJenisSurveyPS) {
		this.kodeJenisSurveyPS = kodeJenisSurveyPS;
	}
	public String getKodePengajuanPS() {
		return kodePengajuanPS;
	}
	public void setKodePengajuanPS(String kodePengajuanPS) {
		this.kodePengajuanPS = kodePengajuanPS;
	}
	public String getLastUpdatedByPS() {
		return lastUpdatedByPS;
	}
	public void setLastUpdatedByPS(String lastUpdatedByPS) {
		this.lastUpdatedByPS = lastUpdatedByPS;
	}
	public Date getLastUpdatedDatePS() {
		return lastUpdatedDatePS;
	}
	public void setLastUpdatedDatePS(Date lastUpdatedDatePS) {
		this.lastUpdatedDatePS = lastUpdatedDatePS;
	}
	public String getLengkapFlagPS() {
		return lengkapFlagPS;
	}
	public void setLengkapFlagPS(String lengkapFlagPS) {
		this.lengkapFlagPS = lengkapFlagPS;
	}
	public String getNamaPemohonPS() {
		return namaPemohonPS;
	}
	public void setNamaPemohonPS(String namaPemohonPS) {
		this.namaPemohonPS = namaPemohonPS;
	}
	public String getNoIdentitasPS() {
		return noIdentitasPS;
	}
	public void setNoIdentitasPS(String noIdentitasPS) {
		this.noIdentitasPS = noIdentitasPS;
	}
	public String getNoPengajuanPS() {
		return noPengajuanPS;
	}
	public void setNoPengajuanPS(String noPengajuanPS) {
		this.noPengajuanPS = noPengajuanPS;
	}
	public String getNoSuratSurveyPS() {
		return noSuratSurveyPS;
	}
	public void setNoSuratSurveyPS(String noSuratSurveyPS) {
		this.noSuratSurveyPS = noSuratSurveyPS;
	}
	public String getOtorisasiFlagPS() {
		return otorisasiFlagPS;
	}
	public void setOtorisasiFlagPS(String otorisasiFlagPS) {
		this.otorisasiFlagPS = otorisasiFlagPS;
	}
	public String getRekomendasiFlagPS() {
		return rekomendasiFlagPS;
	}
	public void setRekomendasiFlagPS(String rekomendasiFlagPS) {
		this.rekomendasiFlagPS = rekomendasiFlagPS;
	}
	public String getRekomendasiJaminanPS() {
		return rekomendasiJaminanPS;
	}
	public void setRekomendasiJaminanPS(String rekomendasiJaminanPS) {
		this.rekomendasiJaminanPS = rekomendasiJaminanPS;
	}
	public BigDecimal getRekomendasiJmlLukalukaPS() {
		return rekomendasiJmlLukalukaPS;
	}
	public void setRekomendasiJmlLukalukaPS(BigDecimal rekomendasiJmlLukalukaPS) {
		this.rekomendasiJmlLukalukaPS = rekomendasiJmlLukalukaPS;
	}
	public BigDecimal getRekomendasiJmlMeninggalPS() {
		return rekomendasiJmlMeninggalPS;
	}
	public void setRekomendasiJmlMeninggalPS(BigDecimal rekomendasiJmlMeninggalPS) {
		this.rekomendasiJmlMeninggalPS = rekomendasiJmlMeninggalPS;
	}
	public BigDecimal getRekomendasiJmlPenguburanPS() {
		return rekomendasiJmlPenguburanPS;
	}
	public void setRekomendasiJmlPenguburanPS(BigDecimal rekomendasiJmlPenguburanPS) {
		this.rekomendasiJmlPenguburanPS = rekomendasiJmlPenguburanPS;
	}
	public String getRekomendasiSementaraPS() {
		return rekomendasiSementaraPS;
	}
	public void setRekomendasiSementaraPS(String rekomendasiSementaraPS) {
		this.rekomendasiSementaraPS = rekomendasiSementaraPS;
	}
	public BigDecimal getSeqNoPS() {
		return seqNoPS;
	}
	public void setSeqNoPS(BigDecimal seqNoPS) {
		this.seqNoPS = seqNoPS;
	}
	public String getStatusProsesPS() {
		return statusProsesPS;
	}
	public void setStatusProsesPS(String statusProsesPS) {
		this.statusProsesPS = statusProsesPS;
	}
	public Date getTglPenerimaanPS() {
		return tglPenerimaanPS;
	}
	public void setTglPenerimaanPS(Date tglPenerimaanPS) {
		this.tglPenerimaanPS = tglPenerimaanPS;
	}
	public Date getTglPengajuanPS() {
		return tglPengajuanPS;
	}
	public void setTglPengajuanPS(Date tglPengajuanPS) {
		this.tglPengajuanPS = tglPengajuanPS;
	}
	public Date getTglPenyelesaianPS() {
		return tglPenyelesaianPS;
	}
	public void setTglPenyelesaianPS(Date tglPenyelesaianPS) {
		this.tglPenyelesaianPS = tglPenyelesaianPS;
	}
	public Date getTglSuratSurveyPS() {
		return tglSuratSurveyPS;
	}
	public void setTglSuratSurveyPS(Date tglSuratSurveyPS) {
		this.tglSuratSurveyPS = tglSuratSurveyPS;
	}
	public String getLingkupJaminan() {
		return lingkupJaminan;
	}
	public void setLingkupJaminan(String lingkupJaminan) {
		this.lingkupJaminan = lingkupJaminan;
	}
	public String getAsalBerkas() {
		return asalBerkas;
	}
	public void setAsalBerkas(String asalBerkas) {
		this.asalBerkas = asalBerkas;
	}
	public List<PlTindakLanjutDto> getLisTindakLanjut() {
		return lisTindakLanjut;
	}
	public void setLisTindakLanjut(List<PlTindakLanjutDto> lisTindakLanjut) {
		this.lisTindakLanjut = lisTindakLanjut;
	}
	public String getJenisSave() {
		return jenisSave;
	}
	public void setJenisSave(String jenisSave) {
		this.jenisSave = jenisSave;
	}
	public String getHubunganKorban() {
		return hubunganKorban;
	}
	public void setHubunganKorban(String hubunganKorban) {
		this.hubunganKorban = hubunganKorban;
	}
	public String getCideraDesc() {
		return cideraDesc;
	}
	public void setCideraDesc(String cideraDesc) {
		this.cideraDesc = cideraDesc;
	}
	public String getTglKejadian() {
		return tglKejadian;
	}
	public void setTglKejadian(String tglKejadian) {
		this.tglKejadian = tglKejadian;
	}
	public String getNoSuratJaminan() {
		return noSuratJaminan;
	}
	public void setNoSuratJaminan(String noSuratJaminan) {
		this.noSuratJaminan = noSuratJaminan;
	}
	public BigDecimal getJmlPengajuan() {
		return jmlPengajuan;
	}
	public void setJmlPengajuan(BigDecimal jmlPengajuan) {
		this.jmlPengajuan = jmlPengajuan;
	}
	public String getBerkasSS() {
		return berkasSS;
	}
	public void setBerkasSS(String berkasSS) {
		this.berkasSS = berkasSS;
	}
	public String getNamaRS() {
		return namaRS;
	}
	public void setNamaRS(String namaRS) {
		this.namaRS = namaRS;
	}
	public String getTglRegisterGL() {
		return tglRegisterGL;
	}
	public void setTglRegisterGL(String tglRegisterGL) {
		this.tglRegisterGL = tglRegisterGL;
	}
	public String getAlamatRS() {
		return alamatRS;
	}
	public void setAlamatRS(String alamatRS) {
		this.alamatRS = alamatRS;
	}
	public String getIdJaminan() {
		return idJaminan;
	}
	public void setIdJaminan(String idJaminan) {
		this.idJaminan = idJaminan;
	}
	public String getFlagBayar() {
		return flagBayar;
	}
	public void setFlagBayar(String flagBayar) {
		this.flagBayar = flagBayar;
	}
	public String getIdJaminanTlRs() {
		return idJaminanTlRs;
	}
	public void setIdJaminanTlRs(String idJaminanTlRs) {
		this.idJaminanTlRs = idJaminanTlRs;
	}
	public String getCreatedByTlRs() {
		return createdByTlRs;
	}
	public void setCreatedByTlRs(String createdByTlRs) {
		this.createdByTlRs = createdByTlRs;
	}
	public Date getCreatedDateTlRs() {
		return createdDateTlRs;
	}
	public void setCreatedDateTlRs(Date createdDateTlRs) {
		this.createdDateTlRs = createdDateTlRs;
	}
	public String getFlagBayarTlRs() {
		return flagBayarTlRs;
	}
	public void setFlagBayarTlRs(String flagBayarTlRs) {
		this.flagBayarTlRs = flagBayarTlRs;
	}
	public String getFlagBayarAwalTlRs() {
		return flagBayarAwalTlRs;
	}
	public void setFlagBayarAwalTlRs(String flagBayarAwalTlRs) {
		this.flagBayarAwalTlRs = flagBayarAwalTlRs;
	}
	public String getFlagLanjutanTlRs() {
		return flagLanjutanTlRs;
	}
	public void setFlagLanjutanTlRs(String flagLanjutanTlRs) {
		this.flagLanjutanTlRs = flagLanjutanTlRs;
	}
	public String getFlagLimpahanTlRs() {
		return flagLimpahanTlRs;
	}
	public void setFlagLimpahanTlRs(String flagLimpahanTlRs) {
		this.flagLimpahanTlRs = flagLimpahanTlRs;
	}
	public String getJenisTagihanTlRs() {
		return jenisTagihanTlRs;
	}
	public void setJenisTagihanTlRs(String jenisTagihanTlRs) {
		this.jenisTagihanTlRs = jenisTagihanTlRs;
	}
	public BigDecimal getJmlAmblTlRs() {
		return jmlAmblTlRs;
	}
	public void setJmlAmblTlRs(BigDecimal jmlAmblTlRs) {
		this.jmlAmblTlRs = jmlAmblTlRs;
	}
	public BigDecimal getJmlAmblSementaraTlRs() {
		return jmlAmblSementaraTlRs;
	}
	public void setJmlAmblSementaraTlRs(BigDecimal jmlAmblSementaraTlRs) {
		this.jmlAmblSementaraTlRs = jmlAmblSementaraTlRs;
	}
	public BigDecimal getJmlAmblSmtTlRs() {
		return jmlAmblSmtTlRs;
	}
	public void setJmlAmblSmtTlRs(BigDecimal jmlAmblSmtTlRs) {
		this.jmlAmblSmtTlRs = jmlAmblSmtTlRs;
	}
	public BigDecimal getJmlP3kTlRs() {
		return jmlP3kTlRs;
	}
	public void setJmlP3kTlRs(BigDecimal jmlP3kTlRs) {
		this.jmlP3kTlRs = jmlP3kTlRs;
	}
	public BigDecimal getJmlP3kSementaraTlRs() {
		return jmlP3kSementaraTlRs;
	}
	public void setJmlP3kSementaraTlRs(BigDecimal jmlP3kSementaraTlRs) {
		this.jmlP3kSementaraTlRs = jmlP3kSementaraTlRs;
	}
	public BigDecimal getJmlP3kSmtTlRs() {
		return jmlP3kSmtTlRs;
	}
	public void setJmlP3kSmtTlRs(BigDecimal jmlP3kSmtTlRs) {
		this.jmlP3kSmtTlRs = jmlP3kSmtTlRs;
	}
	public BigDecimal getJmlPengajuanSementaraTlRs() {
		return jmlPengajuanSementaraTlRs;
	}
	public void setJmlPengajuanSementaraTlRs(BigDecimal jmlPengajuanSementaraTlRs) {
		this.jmlPengajuanSementaraTlRs = jmlPengajuanSementaraTlRs;
	}
	public BigDecimal getJumlahPengajuan1TlRs() {
		return jumlahPengajuan1TlRs;
	}
	public void setJumlahPengajuan1TlRs(BigDecimal jumlahPengajuan1TlRs) {
		this.jumlahPengajuan1TlRs = jumlahPengajuan1TlRs;
	}
	public BigDecimal getJumlahPengajuanSmtTlRs() {
		return jumlahPengajuanSmtTlRs;
	}
	public void setJumlahPengajuanSmtTlRs(BigDecimal jumlahPengajuanSmtTlRs) {
		this.jumlahPengajuanSmtTlRs = jumlahPengajuanSmtTlRs;
	}
	public String getKodeKantorAsalLimpTlRs() {
		return kodeKantorAsalLimpTlRs;
	}
	public void setKodeKantorAsalLimpTlRs(String kodeKantorAsalLimpTlRs) {
		this.kodeKantorAsalLimpTlRs = kodeKantorAsalLimpTlRs;
	}
	public String getKodeKantorJrTlRs() {
		return kodeKantorJrTlRs;
	}
	public void setKodeKantorJrTlRs(String kodeKantorJrTlRs) {
		this.kodeKantorJrTlRs = kodeKantorJrTlRs;
	}
	public String getKodeRsSementaraTlRs() {
		return kodeRsSementaraTlRs;
	}
	public void setKodeRsSementaraTlRs(String kodeRsSementaraTlRs) {
		this.kodeRsSementaraTlRs = kodeRsSementaraTlRs;
	}
	public String getKodeRumahSakitTlRs() {
		return kodeRumahSakitTlRs;
	}
	public void setKodeRumahSakitTlRs(String kodeRumahSakitTlRs) {
		this.kodeRumahSakitTlRs = kodeRumahSakitTlRs;
	}
	public String getLastUpdatedByTlRs() {
		return lastUpdatedByTlRs;
	}
	public void setLastUpdatedByTlRs(String lastUpdatedByTlRs) {
		this.lastUpdatedByTlRs = lastUpdatedByTlRs;
	}
	public Date getLastUpdatedDateTlRs() {
		return lastUpdatedDateTlRs;
	}
	public void setLastUpdatedDateTlRs(Date lastUpdatedDateTlRs) {
		this.lastUpdatedDateTlRs = lastUpdatedDateTlRs;
	}
	public String getNoBerkasTlRs() {
		return noBerkasTlRs;
	}
	public void setNoBerkasTlRs(String noBerkasTlRs) {
		this.noBerkasTlRs = noBerkasTlRs;
	}
	public String getNoRegisterTlRs() {
		return noRegisterTlRs;
	}
	public void setNoRegisterTlRs(String noRegisterTlRs) {
		this.noRegisterTlRs = noRegisterTlRs;
	}
	public String getNoSuratJaminanTlRs() {
		return noSuratJaminanTlRs;
	}
	public void setNoSuratJaminanTlRs(String noSuratJaminanTlRs) {
		this.noSuratJaminanTlRs = noSuratJaminanTlRs;
	}
	public String getOtorisasiAjuSmtTlRs() {
		return otorisasiAjuSmtTlRs;
	}
	public void setOtorisasiAjuSmtTlRs(String otorisasiAjuSmtTlRs) {
		this.otorisasiAjuSmtTlRs = otorisasiAjuSmtTlRs;
	}
	public String getOtorisasiAwalTlRs() {
		return otorisasiAwalTlRs;
	}
	public void setOtorisasiAwalTlRs(String otorisasiAwalTlRs) {
		this.otorisasiAwalTlRs = otorisasiAwalTlRs;
	}
	public Date getTglMasukRsTlRs() {
		return tglMasukRsTlRs;
	}
	public void setTglMasukRsTlRs(Date tglMasukRsTlRs) {
		this.tglMasukRsTlRs = tglMasukRsTlRs;
	}
	public Date getTglSuratJaminanTlRs() {
		return tglSuratJaminanTlRs;
	}
	public void setTglSuratJaminanTlRs(Date tglSuratJaminanTlRs) {
		this.tglSuratJaminanTlRs = tglSuratJaminanTlRs;
	}
	public String getKeteranganOtorisasiTlRs() {
		return keteranganOtorisasiTlRs;
	}
	public void setKeteranganOtorisasiTlRs(String keteranganOtorisasiTlRs) {
		this.keteranganOtorisasiTlRs = keteranganOtorisasiTlRs;
	}
	public Date getTglKejadianLaka() {
		return tglKejadianLaka;
	}
	public void setTglKejadianLaka(Date tglKejadianLaka) {
		this.tglKejadianLaka = tglKejadianLaka;
	}
	public Date getTglLaporanPolisiLaka() {
		return tglLaporanPolisiLaka;
	}
	public void setTglLaporanPolisiLaka(Date tglLaporanPolisiLaka) {
		this.tglLaporanPolisiLaka = tglLaporanPolisiLaka;
	}
	public String getDeskripsiLokasiLaka() {
		return deskripsiLokasiLaka;
	}
	public void setDeskripsiLokasiLaka(String deskripsiLokasiLaka) {
		this.deskripsiLokasiLaka = deskripsiLokasiLaka;
	}
	public String getNoLaporanPolisiLaka() {
		return noLaporanPolisiLaka;
	}
	public void setNoLaporanPolisiLaka(String noLaporanPolisiLaka) {
		this.noLaporanPolisiLaka = noLaporanPolisiLaka;
	}
	public String getNamaInstansi() {
		return namaInstansi;
	}
	public void setNamaInstansi(String namaInstansi) {
		this.namaInstansi = namaInstansi;
	}
	public String getKasusLaka() {
		return kasusLaka;
	}
	public void setKasusLaka(String kasusLaka) {
		this.kasusLaka = kasusLaka;
	}
	public String getStatusKorbanLaka() {
		return statusKorbanLaka;
	}
	public void setStatusKorbanLaka(String statusKorbanLaka) {
		this.statusKorbanLaka = statusKorbanLaka;
	}
	public String getPenjamin() {
		return penjamin;
	}
	public void setPenjamin(String penjamin) {
		this.penjamin = penjamin;
	}
}
