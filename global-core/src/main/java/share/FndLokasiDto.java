package share;

import java.io.Serializable;
import java.util.Date;

public class FndLokasiDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kodeLokasi;
	private String createdBy;
	private Date creationDate;
	private String deskripsi;
	private String flagEnable;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	
	//added by luthfi
	private String kodeDeskripsi;
	
	public String getKodeLokasi() {
		return kodeLokasi;
	}
	public void setKodeLokasi(String kodeLokasi) {
		this.kodeLokasi = kodeLokasi;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getFlagEnable() {
		return flagEnable;
	}
	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getKodeDeskripsi() {
		return kodeDeskripsi;
	}
	public void setKodeDeskripsi(String kodeDeskripsi) {
		this.kodeDeskripsi = kodeDeskripsi;
	}
}
