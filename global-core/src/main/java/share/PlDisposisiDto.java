package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class PlDisposisiDto implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idDisposisi;
	private String createdBy;
	private Date creationDate;
	private String dari;
	private String disposisi;
	private String idGuid;
	private BigDecimal jumlahDisetujui;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String levelCabangDisp;
	private String noBerkas;
	private Date tglDisposisi;
	
	private boolean checkedDisposisi;
	private String dariDesc;
	
	private String kodeDisposisi;
	private String createdByDesc;
	private String noBerkasDisposisi;
	
	//added by luthfi
	private boolean editVisible;
	
	public String getIdDisposisi() {
		return idDisposisi;
	}
	public void setIdDisposisi(String idDisposisi) {
		this.idDisposisi = idDisposisi;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getDari() {
		return dari;
	}
	public void setDari(String dari) {
		this.dari = dari;
	}
	public String getDisposisi() {
		return disposisi;
	}
	public void setDisposisi(String disposisi) {
		this.disposisi = disposisi;
	}
	public String getIdGuid() {
		return idGuid;
	}
	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}
	public BigDecimal getJumlahDisetujui() {
		return jumlahDisetujui;
	}
	public void setJumlahDisetujui(BigDecimal jumlahDisetujui) {
		this.jumlahDisetujui = jumlahDisetujui;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getLevelCabangDisp() {
		return levelCabangDisp;
	}
	public void setLevelCabangDisp(String levelCabangDisp) {
		this.levelCabangDisp = levelCabangDisp;
	}
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public Date getTglDisposisi() {
		return tglDisposisi;
	}
	public void setTglDisposisi(Date tglDisposisi) {
		this.tglDisposisi = tglDisposisi;
	}
	public boolean isCheckedDisposisi() {
		return checkedDisposisi;
	}
	public void setCheckedDisposisi(boolean checkedDisposisi) {
		this.checkedDisposisi = checkedDisposisi;
	}
	public boolean isEditVisible() {
		return editVisible;
	}
	public void setEditVisible(boolean editVisible) {
		this.editVisible = editVisible;
	}
	public String getDariDesc() {
		return dariDesc;
	}
	public void setDariDesc(String dariDesc) {
		this.dariDesc = dariDesc;
	}
	public String getKodeDisposisi() {
		return kodeDisposisi;
	}
	public void setKodeDisposisi(String kodeDisposisi) {
		this.kodeDisposisi = kodeDisposisi;
	}
	public String getCreatedByDesc() {
		return createdByDesc;
	}
	public void setCreatedByDesc(String createdByDesc) {
		this.createdByDesc = createdByDesc;
	}
	public String getNoBerkasDisposisi() {
		return noBerkasDisposisi;
	}
	public void setNoBerkasDisposisi(String noBerkasDisposisi) {
		this.noBerkasDisposisi = noBerkasDisposisi;
	}
	
	


}
