package share;


public class AuthRsPicDto {

	private String kodeRumahsakit;
	private String userLogin;
	
	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}
	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}
	public String getUserLogin() {
		return userLogin;
	}
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
}
