package share;

import java.io.Serializable;
import java.util.Date;

public class PlPeriodeLockDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String createdBy;
	private Date creationDate;
	private String kodeKantorJr;
	private String periodeBulan;
	private String periodeTahun;
	private String status;
	private String updatedBy;
	private Date updatedDate;
	
	private String periode;
	private String kantor;
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getPeriodeBulan() {
		return periodeBulan;
	}
	public void setPeriodeBulan(String periodeBulan) {
		this.periodeBulan = periodeBulan;
	}
	public String getPeriodeTahun() {
		return periodeTahun;
	}
	public void setPeriodeTahun(String periodeTahun) {
		this.periodeTahun = periodeTahun;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getPeriode() {
		return periode;
	}
	public void setPeriode(String periode) {
		this.periode = periode;
	}
	public String getKantor() {
		return kantor;
	}
	public void setKantor(String kantor) {
		this.kantor = kantor;
	}

}
