package share;

import java.io.Serializable;
import java.math.BigDecimal;

public class KorlantasDistrictDto implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String address;
	private BigDecimal archived;
	private String districtNumber;
	private BigDecimal geoProvinceId;
	private BigDecimal gmt;
	private BigDecimal gpsLatitude;
	private BigDecimal gpsLongitude;
	private BigDecimal id;
	private String name;
	private BigDecimal provinceId;
	private BigDecimal sortOrder;
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public BigDecimal getArchived() {
		return archived;
	}
	public void setArchived(BigDecimal archived) {
		this.archived = archived;
	}
	public String getDistrictNumber() {
		return districtNumber;
	}
	public void setDistrictNumber(String districtNumber) {
		this.districtNumber = districtNumber;
	}
	public BigDecimal getGeoProvinceId() {
		return geoProvinceId;
	}
	public void setGeoProvinceId(BigDecimal geoProvinceId) {
		this.geoProvinceId = geoProvinceId;
	}
	public BigDecimal getGmt() {
		return gmt;
	}
	public void setGmt(BigDecimal gmt) {
		this.gmt = gmt;
	}
	public BigDecimal getGpsLatitude() {
		return gpsLatitude;
	}
	public void setGpsLatitude(BigDecimal gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}
	public BigDecimal getGpsLongitude() {
		return gpsLongitude;
	}
	public void setGpsLongitude(BigDecimal gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(BigDecimal provinceId) {
		this.provinceId = provinceId;
	}
	public BigDecimal getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}
