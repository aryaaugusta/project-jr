package share;

import java.io.Serializable;
import java.util.Date;

public class FndKantorJasaraharjaDto implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String kodeKantorJr;

	private String alamat1;

	private String alamat2;
	private String berindukKeKodeKantor;

	private String createdBy;

	private Date creationDate;

	private String flagEnable;

	private String golonganKantor;

	private String jenis;

	private String kodeKantor;

	private String lastUpdatedBy;

	private Date lastUpdatedDate;

	private String levelKantor;

	private String nama;
	
	//Added by luthfi
	private String kodeNama;
	private String kodeCabang;
	

	public String getKodeCabang() {
		return kodeCabang;
	}

	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}

	public String getKodeNama() {
		return kodeNama;
	}

	public void setKodeNama(String kodeNama) {
		this.kodeNama = kodeNama;
	}

	public String getKodeKantorJr() {
		return kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getAlamat1() {
		return alamat1;
	}

	public void setAlamat1(String alamat1) {
		this.alamat1 = alamat1;
	}

	public String getAlamat2() {
		return alamat2;
	}

	public void setAlamat2(String alamat2) {
		this.alamat2 = alamat2;
	}

	public String getBerindukKeKodeKantor() {
		return berindukKeKodeKantor;
	}

	public void setBerindukKeKodeKantor(String berindukKeKodeKantor) {
		this.berindukKeKodeKantor = berindukKeKodeKantor;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFlagEnable() {
		return flagEnable;
	}

	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}

	public String getGolonganKantor() {
		return golonganKantor;
	}

	public void setGolonganKantor(String golonganKantor) {
		this.golonganKantor = golonganKantor;
	}

	public String getJenis() {
		return jenis;
	}

	public void setJenis(String jenis) {
		this.jenis = jenis;
	}

	public String getKodeKantor() {
		return kodeKantor;
	}

	public void setKodeKantor(String kodeKantor) {
		this.kodeKantor = kodeKantor;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLevelKantor() {
		return levelKantor;
	}

	public void setLevelKantor(String levelKantor) {
		this.levelKantor = levelKantor;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeKantorJr == null) ? 0 : kodeKantorJr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FndKantorJasaraharjaDto other = (FndKantorJasaraharjaDto) obj;
		if (kodeKantorJr == null) {
			if (other.kodeKantorJr != null)
				return false;
		} else if (!kodeKantorJr.equals(other.kodeKantorJr))
			return false;
		return true;
	}
}
