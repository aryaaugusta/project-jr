package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;



public class PlDataKecelakaanDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String asalBerkas;
	private String createdBy;
	private Date creationDate;
	private String deskripsiKecelakaan;
	private String deskripsiLokasi;
	private String gpsLs;
	private String gpsLu;
	private String idGuid;
	private String idKecelakaan;
	private BigDecimal jumlahLuka;
	private BigDecimal jumlahMeninggal;
	private String kodeInstansi;
	private String kodeKantorJr;
	private String kodeKasusKecelakaan;
	private String kodeLintasan;
	private String kodeLokasi;
	private String kodeWilayah;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String namaPetugas;
	private String noLaporanPolisi;
	private BigDecimal noUrut;
	private String sifatKecelakaan;
	private String statusLaporanPolisi;
	private String statusTransisi;
	private Date tglKejadian;
	private Date tglLaporanPolisi;
	private boolean flagDelete;
	
	//added by luthfi for index data laka
	private String lokasiDesc;
	private String asalBerkasDesc;
	private String instansiDesc;
	private String kasusKecelakaanDesc;
	private String sifatKecelakaanDesc;
	private String samsatDesc;
	
	//pl korban kecelakaan
	private String idKorbanKecelakaan;
	private String alamat;
	private String idAngkutanKecelakaan;
	private String idAngkutanPenanggung;
	private String jenisIdentitas;
	private String jenisKelamin;
	private String kodeJaminan;
	private String kodePekerjaan;
	private String kodeSifatCidera;
	private String kodeStatusKorban;
	private String namaKorban;
	private String noIdentitas;
	private String noTelp;
	private String statusNikah;
	private BigDecimal umur;
	
	private String cideraHighValue;
	private String cidera;
	private String status;
	private String penjamin;
	private String kasus;
	private String jaminan;
	
	private String nama;
	private String deskripsi;
	private String namaInstansi;
	private String lingkupJaminan;
	private String deskripsiJaminan;
	
	private String provinsiDesc;
	private String kabKotaDesc;
	private String camatDesc;
	
	private String kodeProvinsiLokasi;
	private String kodeKabkotaLokasi;
	private String kodeCamatLokasi;
	private String noRegister;
	private String kendaraanPenjamin;
	
	private String kodeNamaAsalBerkas;
	private String kodeNamaInstansi;
	
	//for data irsms
	private Date tglKejadianIrsms;
	private Date tglLaporanPolisiIrsms;
	private String noLaporanPolisiIrsms;
	private String namaKorbanIrsms;
	private String cideraKorbanIrsms;
	
	private boolean visibleKetCoklit;
	
	public String getAsalBerkas() {
		return asalBerkas;
	}
	public void setAsalBerkas(String asalBerkas) {
		this.asalBerkas = asalBerkas;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getDeskripsiKecelakaan() {
		return deskripsiKecelakaan;
	}
	public void setDeskripsiKecelakaan(String deskripsiKecelakaan) {
		this.deskripsiKecelakaan = deskripsiKecelakaan;
	}
	public String getDeskripsiLokasi() {
		return deskripsiLokasi;
	}
	public void setDeskripsiLokasi(String deskripsiLokasi) {
		this.deskripsiLokasi = deskripsiLokasi;
	}
	public String getGpsLs() {
		return gpsLs;
	}
	public void setGpsLs(String gpsLs) {
		this.gpsLs = gpsLs;
	}
	public String getGpsLu() {
		return gpsLu;
	}
	public void setGpsLu(String gpsLu) {
		this.gpsLu = gpsLu;
	}
	public String getIdGuid() {
		return idGuid;
	}
	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}
	public String getIdKecelakaan() {
		return idKecelakaan;
	}
	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}
	public BigDecimal getJumlahLuka() {
		return jumlahLuka;
	}
	public void setJumlahLuka(BigDecimal jumlahLuka) {
		this.jumlahLuka = jumlahLuka;
	}
	public BigDecimal getJumlahMeninggal() {
		return jumlahMeninggal;
	}
	public void setJumlahMeninggal(BigDecimal jumlahMeninggal) {
		this.jumlahMeninggal = jumlahMeninggal;
	}
	public String getKodeInstansi() {
		return kodeInstansi;
	}
	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getKodeKasusKecelakaan() {
		return kodeKasusKecelakaan;
	}
	public void setKodeKasusKecelakaan(String kodeKasusKecelakaan) {
		this.kodeKasusKecelakaan = kodeKasusKecelakaan;
	}
	public String getKodeLintasan() {
		return kodeLintasan;
	}
	public void setKodeLintasan(String kodeLintasan) {
		this.kodeLintasan = kodeLintasan;
	}
	public String getKodeLokasi() {
		return kodeLokasi;
	}
	public void setKodeLokasi(String kodeLokasi) {
		this.kodeLokasi = kodeLokasi;
	}
	public String getKodeWilayah() {
		return kodeWilayah;
	}
	public void setKodeWilayah(String kodeWilayah) {
		this.kodeWilayah = kodeWilayah;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNamaPetugas() {
		return namaPetugas;
	}
	public void setNamaPetugas(String namaPetugas) {
		this.namaPetugas = namaPetugas;
	}
	public String getNoLaporanPolisi() {
		return noLaporanPolisi;
	}
	public void setNoLaporanPolisi(String noLaporanPolisi) {
		this.noLaporanPolisi = noLaporanPolisi;
	}
	public BigDecimal getNoUrut() {
		return noUrut;
	}
	public void setNoUrut(BigDecimal noUrut) {
		this.noUrut = noUrut;
	}
	public String getSifatKecelakaan() {
		return sifatKecelakaan;
	}
	public void setSifatKecelakaan(String sifatKecelakaan) {
		this.sifatKecelakaan = sifatKecelakaan;
	}
	public String getStatusLaporanPolisi() {
		return statusLaporanPolisi;
	}
	public void setStatusLaporanPolisi(String statusLaporanPolisi) {
		this.statusLaporanPolisi = statusLaporanPolisi;
	}
	public String getStatusTransisi() {
		return statusTransisi;
	}
	public void setStatusTransisi(String statusTransisi) {
		this.statusTransisi = statusTransisi;
	}
	public Date getTglKejadian() {
		return tglKejadian;
	}
	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}
	public Date getTglLaporanPolisi() {
		return tglLaporanPolisi;
	}
	public void setTglLaporanPolisi(Date tglLaporanPolisi) {
		this.tglLaporanPolisi = tglLaporanPolisi;
	}
	public String getIdKorbanKecelakaan() {
		return idKorbanKecelakaan;
	}
	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getIdAngkutanKecelakaan() {
		return idAngkutanKecelakaan;
	}
	public void setIdAngkutanKecelakaan(String idAngkutanKecelakaan) {
		this.idAngkutanKecelakaan = idAngkutanKecelakaan;
	}
	public String getIdAngkutanPenanggung() {
		return idAngkutanPenanggung;
	}
	public void setIdAngkutanPenanggung(String idAngkutanPenanggung) {
		this.idAngkutanPenanggung = idAngkutanPenanggung;
	}
	public String getJenisIdentitas() {
		return jenisIdentitas;
	}
	public void setJenisIdentitas(String jenisIdentitas) {
		this.jenisIdentitas = jenisIdentitas;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public String getKodeJaminan() {
		return kodeJaminan;
	}
	public void setKodeJaminan(String kodeJaminan) {
		this.kodeJaminan = kodeJaminan;
	}
	public String getKodePekerjaan() {
		return kodePekerjaan;
	}
	public void setKodePekerjaan(String kodePekerjaan) {
		this.kodePekerjaan = kodePekerjaan;
	}
	public String getKodeSifatCidera() {
		return kodeSifatCidera;
	}
	public void setKodeSifatCidera(String kodeSifatCidera) {
		this.kodeSifatCidera = kodeSifatCidera;
	}
	public String getKodeStatusKorban() {
		return kodeStatusKorban;
	}
	public void setKodeStatusKorban(String kodeStatusKorban) {
		this.kodeStatusKorban = kodeStatusKorban;
	}
	public String getNamaKorban() {
		return namaKorban;
	}
	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}
	public String getNoIdentitas() {
		return noIdentitas;
	}
	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}
	public String getNoTelp() {
		return noTelp;
	}
	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
	public String getStatusNikah() {
		return statusNikah;
	}
	public void setStatusNikah(String statusNikah) {
		this.statusNikah = statusNikah;
	}
	public BigDecimal getUmur() {
		return umur;
	}
	public void setUmur(BigDecimal umur) {
		this.umur = umur;
	}
	public String getCidera() {
		return cidera;
	}
	public void setCidera(String cidera) {
		this.cidera = cidera;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getPenjamin() {
		return penjamin;
	}
	public void setPenjamin(String penjamin) {
		this.penjamin = penjamin;
	}
	public String getKasus() {
		return kasus;
	}
	public void setKasus(String kasus) {
		this.kasus = kasus;
	}
	public String getJaminan() {
		return jaminan;
	}
	public void setJaminan(String jaminan) {
		this.jaminan = jaminan;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getNamaInstansi() {
		return namaInstansi;
	}
	public void setNamaInstansi(String namaInstansi) {
		this.namaInstansi = namaInstansi;
	}
	public String getLokasiDesc() {
		return lokasiDesc;
	}
	public void setLokasiDesc(String lokasiDesc) {
		this.lokasiDesc = lokasiDesc;
	}
	public String getAsalBerkasDesc() {
		return asalBerkasDesc;
	}
	public void setAsalBerkasDesc(String asalBerkasDesc) {
		this.asalBerkasDesc = asalBerkasDesc;
	}
	public String getKasusKecelakaanDesc() {
		return kasusKecelakaanDesc;
	}
	public void setKasusKecelakaanDesc(String kasusKecelakaanDesc) {
		this.kasusKecelakaanDesc = kasusKecelakaanDesc;
	}
	public String getSifatKecelakaanDesc() {
		return sifatKecelakaanDesc;
	}
	public void setSifatKecelakaanDesc(String sifatKecelakaanDesc) {
		this.sifatKecelakaanDesc = sifatKecelakaanDesc;
	}
	public String getInstansiDesc() {
		return instansiDesc;
	}
	public void setInstansiDesc(String instansiDesc) {
		this.instansiDesc = instansiDesc;
	}
	public String getSamsatDesc() {
		return samsatDesc;
	}
	public void setSamsatDesc(String samsatDesc) {
		this.samsatDesc = samsatDesc;
	}
	public String getLingkupJaminan() {
		return lingkupJaminan;
	}
	public void setLingkupJaminan(String lingkupJaminan) {
		this.lingkupJaminan = lingkupJaminan;
	}
	public String getProvinsiDesc() {
		return provinsiDesc;
	}
	public void setProvinsiDesc(String provinsiDesc) {
		this.provinsiDesc = provinsiDesc;
	}
	public String getKabKotaDesc() {
		return kabKotaDesc;
	}
	public void setKabKotaDesc(String kabKotaDesc) {
		this.kabKotaDesc = kabKotaDesc;
	}
	public String getCamatDesc() {
		return camatDesc;
	}
	public void setCamatDesc(String camatDesc) {
		this.camatDesc = camatDesc;
	}
	public String getKodeProvinsiLokasi() {
		return kodeProvinsiLokasi;
	}
	public void setKodeProvinsiLokasi(String kodeProvinsiLokasi) {
		this.kodeProvinsiLokasi = kodeProvinsiLokasi;
	}
	public String getKodeKabkotaLokasi() {
		return kodeKabkotaLokasi;
	}
	public void setKodeKabkotaLokasi(String kodeKabkotaLokasi) {
		this.kodeKabkotaLokasi = kodeKabkotaLokasi;
	}
	public String getKodeCamatLokasi() {
		return kodeCamatLokasi;
	}
	public void setKodeCamatLokasi(String kodeCamatLokasi) {
		this.kodeCamatLokasi = kodeCamatLokasi;
	}
	public String getNoRegister() {
		return noRegister;
	}
	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}
	public String getCideraHighValue() {
		return cideraHighValue;
	}
	public void setCideraHighValue(String cideraHighValue) {
		this.cideraHighValue = cideraHighValue;
	}
	public String getDeskripsiJaminan() {
		return deskripsiJaminan;
	}
	public void setDeskripsiJaminan(String deskripsiJaminan) {
		this.deskripsiJaminan = deskripsiJaminan;
	}
	public String getKendaraanPenjamin() {
		return kendaraanPenjamin;
	}
	public void setKendaraanPenjamin(String kendaraanPenjamin) {
		this.kendaraanPenjamin = kendaraanPenjamin;
	}
	public String getKodeNamaAsalBerkas() {
		return kodeNamaAsalBerkas;
	}
	public void setKodeNamaAsalBerkas(String kodeNamaAsalBerkas) {
		this.kodeNamaAsalBerkas = kodeNamaAsalBerkas;
	}
	public String getKodeNamaInstansi() {
		return kodeNamaInstansi;
	}
	public void setKodeNamaInstansi(String kodeNamaInstansi) {
		this.kodeNamaInstansi = kodeNamaInstansi;
	}
	public Date getTglKejadianIrsms() {
		return tglKejadianIrsms;
	}
	public void setTglKejadianIrsms(Date tglKejadianIrsms) {
		this.tglKejadianIrsms = tglKejadianIrsms;
	}
	public Date getTglLaporanPolisiIrsms() {
		return tglLaporanPolisiIrsms;
	}
	public void setTglLaporanPolisiIrsms(Date tglLaporanPolisiIrsms) {
		this.tglLaporanPolisiIrsms = tglLaporanPolisiIrsms;
	}
	public String getNoLaporanPolisiIrsms() {
		return noLaporanPolisiIrsms;
	}
	public void setNoLaporanPolisiIrsms(String noLaporanPolisiIrsms) {
		this.noLaporanPolisiIrsms = noLaporanPolisiIrsms;
	}
	public String getNamaKorbanIrsms() {
		return namaKorbanIrsms;
	}
	public void setNamaKorbanIrsms(String namaKorbanIrsms) {
		this.namaKorbanIrsms = namaKorbanIrsms;
	}
	public String getCideraKorbanIrsms() {
		return cideraKorbanIrsms;
	}
	public void setCideraKorbanIrsms(String cideraKorbanIrsms) {
		this.cideraKorbanIrsms = cideraKorbanIrsms;
	}
	public boolean isVisibleKetCoklit() {
		return visibleKetCoklit;
	}
	public void setVisibleKetCoklit(boolean visibleKetCoklit) {
		this.visibleKetCoklit = visibleKetCoklit;
	}
	public boolean isFlagDelete() {
		return flagDelete;
	}
	public void setFlagDelete(boolean flagDelete) {
		this.flagDelete = flagDelete;
	}
	
}
