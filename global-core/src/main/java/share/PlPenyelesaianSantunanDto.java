package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PlPenyelesaianSantunanDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String noBerkas;
	private String createdBy;
	private Date creationDate;
	private String idGuid;
	private String jenisPembayaran;
	private BigDecimal jmlByrAmbl;
	private BigDecimal jmlByrP3k;
	private BigDecimal jumlahDibayarLukaluka;
	private BigDecimal jumlahDibayarMeninggal;
	private BigDecimal jumlahDibayarPenguburan;
	private BigDecimal kelebihanBayar;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String noBpk;
	private String noRekening;
	private String noSuratPenyelesaian;
	private Date tglKelebihanBayar;
	private Date tglPembuatanBpk;
	private Date tglProses;
	
	private String pembayaranDesc;
	private String namaBank;
	private String namaRekening;
	
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getIdGuid() {
		return idGuid;
	}
	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}
	public String getJenisPembayaran() {
		return jenisPembayaran;
	}
	public void setJenisPembayaran(String jenisPembayaran) {
		this.jenisPembayaran = jenisPembayaran;
	}
	public BigDecimal getJmlByrAmbl() {
		return jmlByrAmbl;
	}
	public void setJmlByrAmbl(BigDecimal jmlByrAmbl) {
		this.jmlByrAmbl = jmlByrAmbl;
	}
	public BigDecimal getJmlByrP3k() {
		return jmlByrP3k;
	}
	public void setJmlByrP3k(BigDecimal jmlByrP3k) {
		this.jmlByrP3k = jmlByrP3k;
	}
	public BigDecimal getJumlahDibayarLukaluka() {
		return jumlahDibayarLukaluka;
	}
	public void setJumlahDibayarLukaluka(BigDecimal jumlahDibayarLukaluka) {
		this.jumlahDibayarLukaluka = jumlahDibayarLukaluka;
	}
	public BigDecimal getJumlahDibayarMeninggal() {
		return jumlahDibayarMeninggal;
	}
	public void setJumlahDibayarMeninggal(BigDecimal jumlahDibayarMeninggal) {
		this.jumlahDibayarMeninggal = jumlahDibayarMeninggal;
	}
	public BigDecimal getJumlahDibayarPenguburan() {
		return jumlahDibayarPenguburan;
	}
	public void setJumlahDibayarPenguburan(BigDecimal jumlahDibayarPenguburan) {
		this.jumlahDibayarPenguburan = jumlahDibayarPenguburan;
	}
	public BigDecimal getKelebihanBayar() {
		return kelebihanBayar;
	}
	public void setKelebihanBayar(BigDecimal kelebihanBayar) {
		this.kelebihanBayar = kelebihanBayar;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNoBpk() {
		return noBpk;
	}
	public void setNoBpk(String noBpk) {
		this.noBpk = noBpk;
	}
	public String getNoRekening() {
		return noRekening;
	}
	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}
	public String getNoSuratPenyelesaian() {
		return noSuratPenyelesaian;
	}
	public void setNoSuratPenyelesaian(String noSuratPenyelesaian) {
		this.noSuratPenyelesaian = noSuratPenyelesaian;
	}
	public Date getTglKelebihanBayar() {
		return tglKelebihanBayar;
	}
	public void setTglKelebihanBayar(Date tglKelebihanBayar) {
		this.tglKelebihanBayar = tglKelebihanBayar;
	}
	public Date getTglPembuatanBpk() {
		return tglPembuatanBpk;
	}
	public void setTglPembuatanBpk(Date tglPembuatanBpk) {
		this.tglPembuatanBpk = tglPembuatanBpk;
	}
	public Date getTglProses() {
		return tglProses;
	}
	public void setTglProses(Date tglProses) {
		this.tglProses = tglProses;
	}
	public String getPembayaranDesc() {
		return pembayaranDesc;
	}
	public void setPembayaranDesc(String pembayaranDesc) {
		this.pembayaranDesc = pembayaranDesc;
	}
	public String getNamaBank() {
		return namaBank;
	}
	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}
	public String getNamaRekening() {
		return namaRekening;
	}
	public void setNamaRekening(String namaRekening) {
		this.namaRekening = namaRekening;
	}

}
