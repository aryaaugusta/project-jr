package share;

import java.io.Serializable;
import java.util.Date;

public class PlRequestPerubahanDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String createdBy;
	private Date createdDate;
	private String idRequest;
	private String ketStatus;
	private String keterangan;
	private String kodeKantorJr;
	private String noBerkas;
	private String statusRequest;
	private Date tglUpdate;
	private String updatedBy;
	private Date updatedDate;
	
	private String namaKantor;
	private String namaKorban;
	private String kodeNamaKantor;
	private String createdByDesc;
	private String description;
	
	private boolean visibleAlert;
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getIdRequest() {
		return idRequest;
	}
	public void setIdRequest(String idRequest) {
		this.idRequest = idRequest;
	}
	public String getKetStatus() {
		return ketStatus;
	}
	public void setKetStatus(String ketStatus) {
		this.ketStatus = ketStatus;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getStatusRequest() {
		return statusRequest;
	}
	public void setStatusRequest(String statusRequest) {
		this.statusRequest = statusRequest;
	}
	public Date getTglUpdate() {
		return tglUpdate;
	}
	public void setTglUpdate(Date tglUpdate) {
		this.tglUpdate = tglUpdate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getNamaKantor() {
		return namaKantor;
	}
	public void setNamaKantor(String namaKantor) {
		this.namaKantor = namaKantor;
	}
	public String getNamaKorban() {
		return namaKorban;
	}
	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}
	public String getKodeNamaKantor() {
		return kodeNamaKantor;
	}
	public void setKodeNamaKantor(String kodeNamaKantor) {
		this.kodeNamaKantor = kodeNamaKantor;
	}
	public String getCreatedByDesc() {
		return createdByDesc;
	}
	public void setCreatedByDesc(String createdByDesc) {
		this.createdByDesc = createdByDesc;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public boolean isVisibleAlert() {
		return visibleAlert;
	}
	public void setVisibleAlert(boolean visibleAlert) {
		this.visibleAlert = visibleAlert;
	}
	
	

}
