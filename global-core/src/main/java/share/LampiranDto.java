package share;

import java.io.Serializable;
import java.util.Date;

public class LampiranDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date tglUpload;
	private String namaFile;
	private String idContent;
	private String absah;
	private boolean checked;
	private String completeXcomments;
	private int did;
	
	public String getNamaFile() {
		return namaFile;
	}

	public void setNamaFile(String namaFile) {
		this.namaFile = namaFile;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDeskripsi() {
		return deskripsi;
	}

	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}

	public Date getTglUpload() {
		return tglUpload;
	}

	public void setTglUpload(Date tglUpload) {
		this.tglUpload = tglUpload;
	}

	public String getIdContent() {
		return idContent;
	}

	public void setIdContent(String idContent) {
		this.idContent = idContent;
	}

	private String url;
	private String deskripsi;

	public String getAbsah() {
		return absah;
	}

	public void setAbsah(String absah) {
		this.absah = absah;
	}

	public boolean isChecked() {
		if(getAbsah()!= null){
			if(getAbsah()=="Y" || getAbsah().equalsIgnoreCase("Y")){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getCompleteXcomments() {
		return completeXcomments;
	}

	public void setCompleteXcomments(String completeXcomments) {
		this.completeXcomments = completeXcomments;
	}

	public int getDid() {
		return did;
	}

	public void setDid(int did) {
		this.did = did;
	}

}
