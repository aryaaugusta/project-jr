package share;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KKDukcapilDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("EKTP_STATUS")
	private String eKTPSTATUS;
	@JsonProperty("NAMA_LGKP")
	private String nAMALGKP;
	@JsonProperty("STAT_HBKEL")
	private String sTATHBKEL;
	@JsonProperty("AGAMA")
	private String aGAMA;
	@JsonProperty("JENIS_PKRJN")
	private String jENISPKRJN;
	@JsonProperty("PDDK_AKH")
	private String pDDKAKH;
	@JsonProperty("TMPT_LHR")
	private String tMPTLHR;
	@JsonProperty("STATUS_KAWIN")
	private String sTATUSKAWIN;
	@JsonProperty("GOL_DARAH")
	private String gOLDARAH;
	@JsonProperty("NIK_IBU")
	private String nIKIBU;
	@JsonProperty("JENIS_KLMIN")
	private String jENISKLMIN;
	@JsonProperty("NO_KK")
	private String nOKK;
	@JsonProperty("NIK")
	private String nIK;
	@JsonProperty("KAB_NAME")
	private String kABNAME;
	@JsonProperty("NAMA_LGKP_AYAH")
	private String nAMALGKPAYAH;
	@JsonProperty("NO_RW")
	private String nORW;
	@JsonProperty("KEC_NAME")
	private String kECNAME;
	@JsonProperty("NO_RT")
	private String nORT;
	@JsonProperty("NO_KEL")
	private String nOKEL;
	@JsonProperty("ALAMAT")
	private String aLAMAT;
	@JsonProperty("NO_KEC")
	private String nOKEC;
	@JsonProperty("NIK_AYAH")
	private String nIKAYAH;
	@JsonProperty("NO_PROP")
	private String nOPROP;
	@JsonProperty("NAMA_LGKP_IBU")
	private String nAMALGKPIBU;
	@JsonProperty("PROP_NAME")
	private String pROPNAME;
	@JsonProperty("NO_KAB")
	private String nOKAB;
	@JsonProperty("TGL_LHR")
	private String tGLLHR;
	@JsonProperty("KEL_NAME")
	private String kELNAME;
	@JsonProperty("RESPON")
	private String respon;
	private Boolean isCall = false;
	
	public KKDukcapilDto (KKDukcapilDto in){
		this.aGAMA=in.getaGAMA();
		this.aLAMAT=in.getaLAMAT();
		this.eKTPSTATUS=in.geteKTPSTATUS();
		this.nAMALGKP=in.getnAMALGKP();
		this.sTATHBKEL=in.getsTATHBKEL();
		this.jENISPKRJN=in.getjENISPKRJN();
		this.pDDKAKH=in.getpDDKAKH();
		this.tMPTLHR=in.gettMPTLHR();
		this.sTATUSKAWIN=in.getsTATUSKAWIN();
		this.gOLDARAH=in.getgOLDARAH();
		this.nIKIBU=in.getnIKIBU();
		this.jENISKLMIN=in.getjENISKLMIN();
		this.nOKK=in.getnOKK();
		this.nIK=in.getnIK();
		this.kABNAME=in.getkABNAME();
		this.nAMALGKPAYAH=in.getnAMALGKPAYAH();
		this.nORW=in.getnORW();
		this.kECNAME=in.getkECNAME();
		this.nORT=in.getnORT();
		this.nOKEL=in.getnOKEL();
		this.aLAMAT=in.getaLAMAT();
		this.nOKEC=in.getnOKEC();
		this.nIKAYAH=in.getnIKAYAH();
		this.nOPROP=in.getnOPROP();
		this.nAMALGKPIBU=in.getnAMALGKPIBU();
		this.pROPNAME=in.getpROPNAME();
		this.nOKAB=in.getnOKAB();
		this.tGLLHR=in.gettGLLHR();
		this.kELNAME=in.getkELNAME();
//		 new KKDukcapilDto(
//				in.geteKTPSTATUS(), in.getnAMALGKP(), in.getsTATHBKEL(),
//				in.getaGAMA(), in.getjENISPKRJN(), in.getpDDKAKH(), in.gettMPTLHR(),
//				in.getsTATUSKAWIN(), in.getgOLDARAH(), in.getnIKIBU(),
//				in.getjENISKLMIN(),in.getnOKK(),in.getnIK(),in.getkABNAME(),
//				in.getnAMALGKPAYAH(),in.getnORW(),in.getkECNAME(),in.getnORT(),
//				in.getnOKEL(),in.getaLAMAT(),in.getnOKEC(),in.getnIKAYAH(),
//				in.getnOPROP(),in.getnAMALGKPIBU(),in.getpROPNAME(),in.getnOKAB(),
//				in.gettGLLHR(),in.getkELNAME());
	}
	
	public KKDukcapilDto(){}
	
	public KKDukcapilDto(String eKTPSTATUS, String nAMALGKP, String sTATHBKEL,
			String aGAMA, String jENISPKRJN, String pDDKAKH, String tMPTLHR,
			String sTATUSKAWIN, String gOLDARAH, String nIKIBU,
			String jENISKLMIN, String nOKK, String nIK, String kABNAME,
			String nAMALGKPAYAH, String nORW, String kECNAME, String nORT,
			String nOKEL, String aLAMAT, String nOKEC, String nIKAYAH,
			String nOPROP, String nAMALGKPIBU, String pROPNAME, String nOKAB,
			String tGLLHR, String kELNAME) {
		super();
		this.eKTPSTATUS = eKTPSTATUS;
		this.nAMALGKP = nAMALGKP;
		this.sTATHBKEL = sTATHBKEL;
		this.aGAMA = aGAMA;
		this.jENISPKRJN = jENISPKRJN;
		this.pDDKAKH = pDDKAKH;
		this.tMPTLHR = tMPTLHR;
		this.sTATUSKAWIN = sTATUSKAWIN;
		this.gOLDARAH = gOLDARAH;
		this.nIKIBU = nIKIBU;
		this.jENISKLMIN = jENISKLMIN;
		this.nOKK = nOKK;
		this.nIK = nIK;
		this.kABNAME = kABNAME;
		this.nAMALGKPAYAH = nAMALGKPAYAH;
		this.nORW = nORW;
		this.kECNAME = kECNAME;
		this.nORT = nORT;
		this.nOKEL = nOKEL;
		this.aLAMAT = aLAMAT;
		this.nOKEC = nOKEC;
		this.nIKAYAH = nIKAYAH;
		this.nOPROP = nOPROP;
		this.nAMALGKPIBU = nAMALGKPIBU;
		this.pROPNAME = pROPNAME;
		this.nOKAB = nOKAB;
		this.tGLLHR = tGLLHR;
		this.kELNAME = kELNAME;
	}
	public String geteKTPSTATUS() {
		return eKTPSTATUS;
	}
	public void seteKTPSTATUS(String eKTPSTATUS) {
		this.eKTPSTATUS = eKTPSTATUS;
	}
	public String getnAMALGKP() {
		return nAMALGKP;
	}
	public void setnAMALGKP(String nAMALGKP) {
		this.nAMALGKP = nAMALGKP;
	}
	public String getsTATHBKEL() {
		return sTATHBKEL;
	}
	public void setsTATHBKEL(String sTATHBKEL) {
		this.sTATHBKEL = sTATHBKEL;
	}
	public String getaGAMA() {
		return aGAMA;
	}
	public void setaGAMA(String aGAMA) {
		this.aGAMA = aGAMA;
	}
	public String getjENISPKRJN() {
		return jENISPKRJN;
	}
	public void setjENISPKRJN(String jENISPKRJN) {
		this.jENISPKRJN = jENISPKRJN;
	}
	public String getpDDKAKH() {
		return pDDKAKH;
	}
	public void setpDDKAKH(String pDDKAKH) {
		this.pDDKAKH = pDDKAKH;
	}
	public String gettMPTLHR() {
		return tMPTLHR;
	}
	public void settMPTLHR(String tMPTLHR) {
		this.tMPTLHR = tMPTLHR;
	}
	public String getsTATUSKAWIN() {
		return sTATUSKAWIN;
	}
	public void setsTATUSKAWIN(String sTATUSKAWIN) {
		this.sTATUSKAWIN = sTATUSKAWIN;
	}
	public String getgOLDARAH() {
		return gOLDARAH;
	}
	public void setgOLDARAH(String gOLDARAH) {
		this.gOLDARAH = gOLDARAH;
	}
	public String getnIKIBU() {
		return nIKIBU;
	}
	public void setnIKIBU(String nIKIBU) {
		this.nIKIBU = nIKIBU;
	}
	public String getjENISKLMIN() {
		return jENISKLMIN;
	}
	public void setjENISKLMIN(String jENISKLMIN) {
		this.jENISKLMIN = jENISKLMIN;
	}
	public String getnOKK() {
		return nOKK;
	}
	public void setnOKK(String nOKK) {
		this.nOKK = nOKK;
	}
	public String getnIK() {
		return nIK;
	}
	public void setnIK(String nIK) {
		this.nIK = nIK;
	}
	public String getkABNAME() {
		return kABNAME;
	}
	public void setkABNAME(String kABNAME) {
		this.kABNAME = kABNAME;
	}
	public String getnAMALGKPAYAH() {
		return nAMALGKPAYAH;
	}
	public void setnAMALGKPAYAH(String nAMALGKPAYAH) {
		this.nAMALGKPAYAH = nAMALGKPAYAH;
	}
	public String getnORW() {
		return nORW;
	}
	public void setnORW(String nORW) {
		this.nORW = nORW;
	}
	public String getkECNAME() {
		return kECNAME;
	}
	public void setkECNAME(String kECNAME) {
		this.kECNAME = kECNAME;
	}
	public String getnORT() {
		return nORT;
	}
	public void setnORT(String nORT) {
		this.nORT = nORT;
	}
	public String getnOKEL() {
		return nOKEL;
	}
	public void setnOKEL(String nOKEL) {
		this.nOKEL = nOKEL;
	}
	public String getaLAMAT() {
		return aLAMAT;
	}
	public void setaLAMAT(String aLAMAT) {
		this.aLAMAT = aLAMAT;
	}
	public String getnOKEC() {
		return nOKEC;
	}
	public void setnOKEC(String nOKEC) {
		this.nOKEC = nOKEC;
	}
	public String getnIKAYAH() {
		return nIKAYAH;
	}
	public void setnIKAYAH(String nIKAYAH) {
		this.nIKAYAH = nIKAYAH;
	}
	public String getnOPROP() {
		return nOPROP;
	}
	public void setnOPROP(String nOPROP) {
		this.nOPROP = nOPROP;
	}
	public String getnAMALGKPIBU() {
		return nAMALGKPIBU;
	}
	public void setnAMALGKPIBU(String nAMALGKPIBU) {
		this.nAMALGKPIBU = nAMALGKPIBU;
	}
	public String getpROPNAME() {
		return pROPNAME;
	}
	public void setpROPNAME(String pROPNAME) {
		this.pROPNAME = pROPNAME;
	}
	public String getnOKAB() {
		return nOKAB;
	}
	public void setnOKAB(String nOKAB) {
		this.nOKAB = nOKAB;
	}
	public String gettGLLHR() {
		return tGLLHR;
	}
	public void settGLLHR(String tGLLHR) {
		this.tGLLHR = tGLLHR;
	}
	public String getkELNAME() {
		return kELNAME;
	}
	public void setkELNAME(String kELNAME) {
		this.kELNAME = kELNAME;
	}
	public String getRespon() {
		return respon;
	}
	public void setRespon(String respon) {
		this.respon = respon;
	}

	public Boolean getIsCall() {
		return isCall;
	}

	public void setIsCall(Boolean isCall) {
		this.isCall = isCall;
	}
	
	
	

}
