package share;

import java.io.Serializable;
import java.util.Date;

public class PlBerkasEcmDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String noRegEcms;
	private String namafile;
	private String filepath;
	private String createdBy;
	private Date createdDate;
	private String ddocname;
	
	
	public String getNoRegEcms() {
		return noRegEcms;
	}
	public void setNoRegEcms(String noRegEcms) {
		this.noRegEcms = noRegEcms;
	}
	public String getNamafile() {
		return namafile;
	}
	public void setNamafile(String namafile) {
		this.namafile = namafile;
	}
	public String getFilepath() {
		return filepath;
	}
	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getDdocname() {
		return ddocname;
	}
	public void setDdocname(String ddocname) {
		this.ddocname = ddocname;
	}
	
	
	
	
}
