package share.common;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


public class AuthElementDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String elementCode;
	private String createdBy;
	private Date createdDate;
	private String description;
	private String eStatusTrf;
	private String elementHeader;
	private String elementId;
	private String elementLink;
	private String elementLinkR;
	private BigDecimal elementOrder;
	private String flagEnable;
	
	public String getElementCode() {
		return elementCode;
	}
	public void setElementCode(String elementCode) {
		this.elementCode = elementCode;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String geteStatusTrf() {
		return eStatusTrf;
	}
	public void seteStatusTrf(String eStatusTrf) {
		this.eStatusTrf = eStatusTrf;
	}
	public String getElementHeader() {
		return elementHeader;
	}
	public void setElementHeader(String elementHeader) {
		this.elementHeader = elementHeader;
	}
	public String getElementId() {
		return elementId;
	}
	public void setElementId(String elementId) {
		this.elementId = elementId;
	}
	public String getElementLink() {
		return elementLink;
	}
	public void setElementLink(String elementLink) {
		this.elementLink = elementLink;
	}
	public String getElementLinkR() {
		return elementLinkR;
	}
	public void setElementLinkR(String elementLinkR) {
		this.elementLinkR = elementLinkR;
	}
	public BigDecimal getElementOrder() {
		return elementOrder;
	}
	public void setElementOrder(BigDecimal elementOrder) {
		this.elementOrder = elementOrder;
	}
	public String getFlagEnable() {
		return flagEnable;
	}
	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}
}
