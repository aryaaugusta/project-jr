package share.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
/**
 * For use as menu translator 
 * in DASI db to be implemented in java UI
 *
 */
public class UserMenuDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserMenuDto parent;
	private List<UserMenuDto> child = new ArrayList<>();
	private String menuName;
	private String menuID;
	private String parentID;
	private String menuLoc;
	
	
	public UserMenuDto getParent() {
		return parent;
	}
	public void setParent(UserMenuDto parent) {
		this.parent = parent;
	}
	public List<UserMenuDto> getChild() {
		return child;
	}
	public void setChild(List<UserMenuDto> child) {
		this.child = child;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getMenuID() {
		return menuID;
	}
	public void setMenuID(String menuID) {
		this.menuID = menuID;
	}
	public String getParentID() {
		return parentID;
	}
	public void setParentID(String parentID) {
		this.parentID = parentID;
	}
	public String getMenuLoc() {
		return menuLoc;
	}
	public void setMenuLoc(String menuLoc) {
		this.menuLoc = menuLoc;
	}
	
	
	
}
