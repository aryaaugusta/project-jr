package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class PlPengajuanSantunanDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String noBerkas;
	private String absahFlag;
	private String alamatPemohon;
	private String bebanPusatFlag;
	private String cideraKorban;
	private String createdBy;
	private Date creationDate;
	private String diajukanDi;
	private String dilimpahkanKe;
	private String flagHasilSurvey;
	private String idGuid;
	private String idKecelakaan;
	private String idKorbanKecelakaan;
	private String jaminanGandaFlag;
	private String jaminanPembayaran;
	private String jenisIdentitas;
	private BigDecimal jmlPengajuanAmbl;
	private BigDecimal jmlPengajuanP3k;
	private BigDecimal jmlRekomenAmbl;
	private BigDecimal jmlRekomenP3k;
	private BigDecimal jumlahPengajuanLukaluka;
	private BigDecimal jumlahPengajuanMeninggal;
	private BigDecimal jumlahPengajuanPenguburan;
	private String kesimpulanSementara;
	private String kodeHubunganKorban;
	private String kodeJaminan;
	private String kodeJenisSurvey;
	private String kodePengajuan;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String lengkapFlag;
	private String namaPemohon;
	private String noIdentitas;
	private String noPengajuan;
	private String noSuratSurvey;
	private String otorisasiFlag;
	private String rekomendasiFlag;
	private String rekomendasiJaminan;
	private BigDecimal rekomendasiJmlLukaluka;
	private BigDecimal rekomendasiJmlMeninggal;
	private BigDecimal rekomendasiJmlPenguburan;
	private String rekomendasiSementara;
	private BigDecimal seqNo;
	private String statusProses;
	private Date tglPenerimaan;
	private Date tglPengajuan;
	private Date tglPenyelesaian;
	private Date tglSuratSurvey;
	private String tipePengajuan;
	private String transisiFlag;

	// exstension
	private String hubKorbanDesc;
	private String lingkupJaminan;

	// data laka
	private String nama;
	private String no;
	private String noLaporanPolisi;
	private String kodeSifatCidera;

	private String cidera2;
	private String penyelesaian;
	private String tglPengajuanToString;
	private Date tglKejadian;
	private String instansi;
	private String deskripsiRs;
	private String deskripsiInstansi;

	private String cideraDes;
	private String kodeCideraDes;
	private String kodeCidera;
	private String statusHubunganDes;
	private String kodeRs;
	private String kodeDesRs;
	private String pembayaranDes;

	private String kodeNamaKantor;
	private String namaKantor;

	private String sifatLakaDesc;

	// ADDED BY LUTHFI
	private String provinsiPemohonDesc;
	private String kabKotaPemohonDesc;
	private String camatPemohonDesc;

	private String kodeKantorDilimpahkan;
	private String namaKantorDilimpahkan;

	private String deskripsiKecelakaan;
	private String deskripsiLokasi;

	// angkutan
	private String noPolisi;
	private String jenisKendaraanDesc;

	// korban
	private String umurKorban;
	private BigDecimal umur;

	// penyelesaian santunan
	private String statusProsesMeaning;
	private String namaKorban;
	private String kesimpulan;

	private String noBerkasPenyelesaian;
	private String createdByPenyelesaian;
	private Date creationDatePenyelesaian;
	private String idGuidPenyelesaian;
	private String jenisPembayaranPenyelesaian;
	private BigDecimal jmlByrAmblPenyelesaian;
	private BigDecimal jmlByrP3kPenyelesaian;
	private BigDecimal jumlahDibayarLukalukaPenyelesaian;
	private BigDecimal jumlahDibayarMeninggalPenyelesaian;
	private BigDecimal jumlahDibayarPenguburanPenyelesaian;
	private BigDecimal kelebihanBayarPenyelesaian;
	private String lastUpdatedByPenyelesaian;
	private Date lastUpdatedDatePenyelesaian;
	private String noBpkPenyelesaian;
	private String noRekeningPenyelesaian;
	private String noSuratPenyelesaianPenyelesaian;
	private Date tglKelebihanBayarPenyelesaian;
	private Date tglPembuatanBpkPenyelesaian;
	private Date tglProsesPenyelesaian;

	private String statusProsesDesc;
	private String alamatKorban;
	private Date tglProses;
	private boolean berkasSelesai;

	// pl additional desc
	private String noBerkasAdd;
	private String createdByAdd;
	private Date createdDateAdd;
	private String idRekRsAdd;
	private String jnsRekeningAdd;
	private String lastUpdatedByAdd;
	private Date lastUpdatedDateAdd;
	private String namaRekeningAdd;
	private Date tglMdAdd;
	private Date tglRawatAkhirAdd;
	private Date tglRawatAwalAdd;
	private Date tglTerimaLimpahAdd;
	private BigDecimal totalPembayaran;
	private String kodeLokasiPemohonAdd;
	
	//pl pengajuan rs
	
	private String idPengajuanRs;
	private String noBerkasPengajuanRs;
	private String createdByPengajuanRs;
	private Date creationDatePengajuanRs;
	private String idGuidPengajuanRs;
	private BigDecimal jumlahPengajuanLukalukaPengajuanRs;
	private String kodeRumahsakitPengajuanRs;
	private String lastUpdatedByPengajuanRs;
	private Date lastUpdatedDatePengajuanRs;

	//pl berkas pengajuan 
	private String diterimaFlagBerkasPengajuan;
	
	private String kodeJaminanLaporan;

	public String getIdPengajuanRs() {
		return idPengajuanRs;
	}

	public void setIdPengajuanRs(String idPengajuanRs) {
		this.idPengajuanRs = idPengajuanRs;
	}

	public String getNoBerkasPengajuanRs() {
		return noBerkasPengajuanRs;
	}

	public void setNoBerkasPengajuanRs(String noBerkasPengajuanRs) {
		this.noBerkasPengajuanRs = noBerkasPengajuanRs;
	}

	public String getCreatedByPengajuanRs() {
		return createdByPengajuanRs;
	}

	public void setCreatedByPengajuanRs(String createdByPengajuanRs) {
		this.createdByPengajuanRs = createdByPengajuanRs;
	}

	public Date getCreationDatePengajuanRs() {
		return creationDatePengajuanRs;
	}

	public void setCreationDatePengajuanRs(Date creationDatePengajuanRs) {
		this.creationDatePengajuanRs = creationDatePengajuanRs;
	}

	public String getIdGuidPengajuanRs() {
		return idGuidPengajuanRs;
	}

	public void setIdGuidPengajuanRs(String idGuidPengajuanRs) {
		this.idGuidPengajuanRs = idGuidPengajuanRs;
	}

	public BigDecimal getJumlahPengajuanLukalukaPengajuanRs() {
		return jumlahPengajuanLukalukaPengajuanRs;
	}

	public void setJumlahPengajuanLukalukaPengajuanRs(
			BigDecimal jumlahPengajuanLukalukaPengajuanRs) {
		this.jumlahPengajuanLukalukaPengajuanRs = jumlahPengajuanLukalukaPengajuanRs;
	}

	public String getKodeRumahsakitPengajuanRs() {
		return kodeRumahsakitPengajuanRs;
	}

	public void setKodeRumahsakitPengajuanRs(String kodeRumahsakitPengajuanRs) {
		this.kodeRumahsakitPengajuanRs = kodeRumahsakitPengajuanRs;
	}

	public String getLastUpdatedByPengajuanRs() {
		return lastUpdatedByPengajuanRs;
	}

	public void setLastUpdatedByPengajuanRs(String lastUpdatedByPengajuanRs) {
		this.lastUpdatedByPengajuanRs = lastUpdatedByPengajuanRs;
	}

	public Date getLastUpdatedDatePengajuanRs() {
		return lastUpdatedDatePengajuanRs;
	}

	public void setLastUpdatedDatePengajuanRs(Date lastUpdatedDatePengajuanRs) {
		this.lastUpdatedDatePengajuanRs = lastUpdatedDatePengajuanRs;
	}

	private String jaminanDesc;
	private Date tglLaporanPolisi;
	private Date tglCetak;
	private String loketMenangani;
	private String namaInstansi;
	private String kasusKecelakaan;
	private String statusKroban;
	private String golonganKendaraan;
	private String noTelpPemohon;
	private String noIdentitasKorban;
	private String noIdentitasPemohon;
	private String noTelpKorban;
	private BigDecimal jumlahPengajuanLuka2RS;
	private String namaRS;

	// MONITORING PELIMPAHAN
	private String namaKantorDiajukanDi;
	private BigDecimal jeda;
	private String tglPelimpahan;
	private String tglTerjadi;

	// Laka belum tuntas
	private Date tglTindakLanjut;
	private String tindakLanjutDesc;

	// FLAG MENU
	private boolean menuPengajuan;
	private boolean menuIdentifikasi;
	private boolean menuOtorisasi;
	
	private String tglPenyelesaianStr;
	private String tglKejadianStr;
	private String statusLaporanPolisi;
	private String kesimpulanSementaraDesc;
	private String jaminaanPembayaranDesc; 
	private String otorisasiDesc;
	private String urutProses;
	private String kodeKantorJr;
	private String kodeBank;
	private String jenisRek;
	private String pemohon;
	private BigDecimal noBpk;
	
	private String namaKantorAsal;
	
	private String alamatRS;

	public String getTglPengajuanToString() {
		return tglPengajuanToString;
	}

	public void setTglPengajuanToString(String tglPengajuanToString) {
		this.tglPengajuanToString = tglPengajuanToString;
	}

	public String getCidera2() {
		return cidera2;
	}

	public void setCidera2(String cidera2) {
		this.cidera2 = cidera2;
	}

	public String getPenyelesaian() {
		return penyelesaian;
	}

	public void setPenyelesaian(String penyelesaian) {
		this.penyelesaian = penyelesaian;
	}

	public String getKodeSifatCidera() {
		return kodeSifatCidera;
	}

	public void setKodeSifatCidera(String kodeSifatCidera) {
		this.kodeSifatCidera = kodeSifatCidera;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getNoBerkas() {
		return noBerkas;
	}

	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}

	public String getAbsahFlag() {
		return absahFlag;
	}

	public void setAbsahFlag(String absahFlag) {
		this.absahFlag = absahFlag;
	}

	public String getAlamatPemohon() {
		return alamatPemohon;
	}

	public void setAlamatPemohon(String alamatPemohon) {
		this.alamatPemohon = alamatPemohon;
	}

	public String getBebanPusatFlag() {
		return bebanPusatFlag;
	}

	public void setBebanPusatFlag(String bebanPusatFlag) {
		this.bebanPusatFlag = bebanPusatFlag;
	}

	public String getCideraKorban() {
		return cideraKorban;
	}

	public void setCideraKorban(String cideraKorban) {
		this.cideraKorban = cideraKorban;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getDiajukanDi() {
		return diajukanDi;
	}

	public void setDiajukanDi(String diajukanDi) {
		this.diajukanDi = diajukanDi;
	}

	public String getDilimpahkanKe() {
		return dilimpahkanKe;
	}

	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}

	public String getFlagHasilSurvey() {
		return flagHasilSurvey;
	}

	public void setFlagHasilSurvey(String flagHasilSurvey) {
		this.flagHasilSurvey = flagHasilSurvey;
	}

	public String getIdGuid() {
		return idGuid;
	}

	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}

	public String getIdKecelakaan() {
		return idKecelakaan;
	}

	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}

	public String getIdKorbanKecelakaan() {
		return idKorbanKecelakaan;
	}

	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}

	public String getJaminanGandaFlag() {
		return jaminanGandaFlag;
	}

	public void setJaminanGandaFlag(String jaminanGandaFlag) {
		this.jaminanGandaFlag = jaminanGandaFlag;
	}

	public String getJaminanPembayaran() {
		return jaminanPembayaran;
	}

	public void setJaminanPembayaran(String jaminanPembayaran) {
		this.jaminanPembayaran = jaminanPembayaran;
	}

	public String getJenisIdentitas() {
		return jenisIdentitas;
	}

	public void setJenisIdentitas(String jenisIdentitas) {
		this.jenisIdentitas = jenisIdentitas;
	}

	public BigDecimal getJmlPengajuanAmbl() {
		return jmlPengajuanAmbl;
	}

	public void setJmlPengajuanAmbl(BigDecimal jmlPengajuanAmbl) {
		this.jmlPengajuanAmbl = jmlPengajuanAmbl;
	}

	public BigDecimal getJmlPengajuanP3k() {
		return jmlPengajuanP3k;
	}

	public void setJmlPengajuanP3k(BigDecimal jmlPengajuanP3k) {
		this.jmlPengajuanP3k = jmlPengajuanP3k;
	}

	public BigDecimal getJmlRekomenAmbl() {
		return jmlRekomenAmbl;
	}

	public void setJmlRekomenAmbl(BigDecimal jmlRekomenAmbl) {
		this.jmlRekomenAmbl = jmlRekomenAmbl;
	}

	public BigDecimal getJmlRekomenP3k() {
		return jmlRekomenP3k;
	}

	public void setJmlRekomenP3k(BigDecimal jmlRekomenP3k) {
		this.jmlRekomenP3k = jmlRekomenP3k;
	}

	public BigDecimal getJumlahPengajuanLukaluka() {
		return jumlahPengajuanLukaluka;
	}

	public void setJumlahPengajuanLukaluka(BigDecimal jumlahPengajuanLukaluka) {
		this.jumlahPengajuanLukaluka = jumlahPengajuanLukaluka;
	}

	public BigDecimal getJumlahPengajuanMeninggal() {
		return jumlahPengajuanMeninggal;
	}

	public void setJumlahPengajuanMeninggal(BigDecimal jumlahPengajuanMeninggal) {
		this.jumlahPengajuanMeninggal = jumlahPengajuanMeninggal;
	}

	public BigDecimal getJumlahPengajuanPenguburan() {
		return jumlahPengajuanPenguburan;
	}

	public void setJumlahPengajuanPenguburan(
			BigDecimal jumlahPengajuanPenguburan) {
		this.jumlahPengajuanPenguburan = jumlahPengajuanPenguburan;
	}

	public String getKesimpulanSementara() {
		return kesimpulanSementara;
	}

	public void setKesimpulanSementara(String kesimpulanSementara) {
		this.kesimpulanSementara = kesimpulanSementara;
	}

	public String getKodeHubunganKorban() {
		return kodeHubunganKorban;
	}

	public void setKodeHubunganKorban(String kodeHubunganKorban) {
		this.kodeHubunganKorban = kodeHubunganKorban;
	}

	public String getKodeJaminan() {
		return kodeJaminan;
	}

	public void setKodeJaminan(String kodeJaminan) {
		this.kodeJaminan = kodeJaminan;
	}

	public String getKodeJenisSurvey() {
		return kodeJenisSurvey;
	}

	public void setKodeJenisSurvey(String kodeJenisSurvey) {
		this.kodeJenisSurvey = kodeJenisSurvey;
	}

	public String getKodePengajuan() {
		return kodePengajuan;
	}

	public void setKodePengajuan(String kodePengajuan) {
		this.kodePengajuan = kodePengajuan;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLengkapFlag() {
		return lengkapFlag;
	}

	public void setLengkapFlag(String lengkapFlag) {
		this.lengkapFlag = lengkapFlag;
	}

	public String getNamaPemohon() {
		return namaPemohon;
	}

	public void setNamaPemohon(String namaPemohon) {
		this.namaPemohon = namaPemohon;
	}

	public String getNoIdentitas() {
		return noIdentitas;
	}

	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}

	public String getNoPengajuan() {
		return noPengajuan;
	}

	public void setNoPengajuan(String noPengajuan) {
		this.noPengajuan = noPengajuan;
	}

	public String getNoSuratSurvey() {
		return noSuratSurvey;
	}

	public void setNoSuratSurvey(String noSuratSurvey) {
		this.noSuratSurvey = noSuratSurvey;
	}

	public String getOtorisasiFlag() {
		return otorisasiFlag;
	}

	public void setOtorisasiFlag(String otorisasiFlag) {
		this.otorisasiFlag = otorisasiFlag;
	}

	public String getRekomendasiFlag() {
		return rekomendasiFlag;
	}

	public void setRekomendasiFlag(String rekomendasiFlag) {
		this.rekomendasiFlag = rekomendasiFlag;
	}

	public String getRekomendasiJaminan() {
		return rekomendasiJaminan;
	}

	public void setRekomendasiJaminan(String rekomendasiJaminan) {
		this.rekomendasiJaminan = rekomendasiJaminan;
	}

	public BigDecimal getRekomendasiJmlLukaluka() {
		return rekomendasiJmlLukaluka;
	}

	public void setRekomendasiJmlLukaluka(BigDecimal rekomendasiJmlLukaluka) {
		this.rekomendasiJmlLukaluka = rekomendasiJmlLukaluka;
	}

	public BigDecimal getRekomendasiJmlMeninggal() {
		return rekomendasiJmlMeninggal;
	}

	public void setRekomendasiJmlMeninggal(BigDecimal rekomendasiJmlMeninggal) {
		this.rekomendasiJmlMeninggal = rekomendasiJmlMeninggal;
	}

	public BigDecimal getRekomendasiJmlPenguburan() {
		return rekomendasiJmlPenguburan;
	}

	public void setRekomendasiJmlPenguburan(BigDecimal rekomendasiJmlPenguburan) {
		this.rekomendasiJmlPenguburan = rekomendasiJmlPenguburan;
	}

	public String getRekomendasiSementara() {
		return rekomendasiSementara;
	}

	public void setRekomendasiSementara(String rekomendasiSementara) {
		this.rekomendasiSementara = rekomendasiSementara;
	}

	public BigDecimal getSeqNo() {
		return seqNo;
	}

	public void setSeqNo(BigDecimal seqNo) {
		this.seqNo = seqNo;
	}

	public String getStatusProses() {
		return statusProses;
	}

	public void setStatusProses(String statusProses) {
		this.statusProses = statusProses;
	}

	public Date getTglPenerimaan() {
		return tglPenerimaan;
	}

	public void setTglPenerimaan(Date tglPenerimaan) {
		this.tglPenerimaan = tglPenerimaan;
	}

	public Date getTglPengajuan() {
		return tglPengajuan;
	}

	public void setTglPengajuan(Date tglPengajuan) {
		this.tglPengajuan = tglPengajuan;
	}

	public Date getTglPenyelesaian() {
		return tglPenyelesaian;
	}

	public void setTglPenyelesaian(Date tglPenyelesaian) {
		this.tglPenyelesaian = tglPenyelesaian;
	}

	public Date getTglSuratSurvey() {
		return tglSuratSurvey;
	}

	public void setTglSuratSurvey(Date tglSuratSurvey) {
		this.tglSuratSurvey = tglSuratSurvey;
	}

	public String getTipePengajuan() {
		return tipePengajuan;
	}

	public void setTipePengajuan(String tipePengajuan) {
		this.tipePengajuan = tipePengajuan;
	}

	public String getTransisiFlag() {
		return transisiFlag;
	}

	public void setTransisiFlag(String transisiFlag) {
		this.transisiFlag = transisiFlag;
	}

	public String getDeskripsiRs() {
		return deskripsiRs;
	}

	public void setDeskripsiRs(String deskripsiRs) {
		this.deskripsiRs = deskripsiRs;
	}

	public String getDeskripsiInstansi() {
		return deskripsiInstansi;
	}

	public void setDeskripsiInstansi(String deskripsiInstansi) {
		this.deskripsiInstansi = deskripsiInstansi;
	}

	public Date getTglKejadian() {
		return tglKejadian;
	}

	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

	public String getStatusHubunganDes() {
		return statusHubunganDes;
	}

	public void setStatusHubunganDes(String statusHubunganDes) {
		this.statusHubunganDes = statusHubunganDes;
	}

	public String getCideraDes() {
		return cideraDes;
	}

	public void setCideraDes(String cideraDes) {
		this.cideraDes = cideraDes;
	}

	public String getKodeCideraDes() {
		return kodeCideraDes;
	}

	public void setKodeCideraDes(String kodeCideraDes) {
		this.kodeCideraDes = kodeCideraDes;
	}

	public String getKodeCidera() {
		return kodeCidera;
	}

	public void setKodeCidera(String kodeCidera) {
		this.kodeCidera = kodeCidera;
	}

	public String getKodeRs() {
		return kodeRs;
	}

	public void setKodeRs(String kodeRs) {
		this.kodeRs = kodeRs;
	}

	public String getKodeDesRs() {
		return kodeDesRs;
	}

	public void setKodeDesRs(String kodeDesRs) {
		this.kodeDesRs = kodeDesRs;
	}

	public String getPembayaranDes() {
		return pembayaranDes;
	}

	public void setPembayaranDes(String pembayaranDes) {
		this.pembayaranDes = pembayaranDes;
	}

	public String getKodeNamaKantor() {
		return kodeNamaKantor;
	}

	public void setKodeNamaKantor(String kodeNamaKantor) {
		this.kodeNamaKantor = kodeNamaKantor;
	}

	public String getNamaKantor() {
		return namaKantor;
	}

	public void setNamaKantor(String namaKantor) {
		this.namaKantor = namaKantor;
	}

	public String getProvinsiPemohonDesc() {
		return provinsiPemohonDesc;
	}

	public void setProvinsiPemohonDesc(String provinsiPemohonDesc) {
		this.provinsiPemohonDesc = provinsiPemohonDesc;
	}

	public String getKabKotaPemohonDesc() {
		return kabKotaPemohonDesc;
	}

	public void setKabKotaPemohonDesc(String kabKotaPemohonDesc) {
		this.kabKotaPemohonDesc = kabKotaPemohonDesc;
	}

	public String getCamatPemohonDesc() {
		return camatPemohonDesc;
	}

	public void setCamatPemohonDesc(String camatPemohonDesc) {
		this.camatPemohonDesc = camatPemohonDesc;
	}

	public String getNoLaporanPolisi() {
		return noLaporanPolisi;
	}

	public void setNoLaporanPolisi(String noLaporanPolisi) {
		this.noLaporanPolisi = noLaporanPolisi;
	}

	public String getInstansi() {
		return instansi;
	}

	public void setInstansi(String instansi) {
		this.instansi = instansi;
	}

	public String getStatusProsesMeaning() {
		return statusProsesMeaning;
	}

	public void setStatusProsesMeaning(String statusProsesMeaning) {
		this.statusProsesMeaning = statusProsesMeaning;
	}

	public String getNamaKorban() {
		return namaKorban;
	}

	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}

	public String getAlamatKorban() {
		return alamatKorban;
	}

	public void setAlamatKorban(String alamatKorban) {
		this.alamatKorban = alamatKorban;
	}

	public String getStatusProsesDesc() {
		return statusProsesDesc;
	}

	public void setStatusProsesDesc(String statusProsesDesc) {
		this.statusProsesDesc = statusProsesDesc;
	}

	public String getKesimpulan() {
		return kesimpulan;
	}

	public void setKesimpulan(String kesimpulan) {
		this.kesimpulan = kesimpulan;
	}

	public Date getTglProses() {
		return tglProses;
	}

	public void setTglProses(Date tglProses) {
		this.tglProses = tglProses;
	}

	public String getNoBerkasPenyelesaian() {
		return noBerkasPenyelesaian;
	}

	public void setNoBerkasPenyelesaian(String noBerkasPenyelesaian) {
		this.noBerkasPenyelesaian = noBerkasPenyelesaian;
	}

	public String getCreatedByPenyelesaian() {
		return createdByPenyelesaian;
	}

	public void setCreatedByPenyelesaian(String createdByPenyelesaian) {
		this.createdByPenyelesaian = createdByPenyelesaian;
	}

	public Date getCreationDatePenyelesaian() {
		return creationDatePenyelesaian;
	}

	public void setCreationDatePenyelesaian(Date creationDatePenyelesaian) {
		this.creationDatePenyelesaian = creationDatePenyelesaian;
	}

	public String getIdGuidPenyelesaian() {
		return idGuidPenyelesaian;
	}

	public void setIdGuidPenyelesaian(String idGuidPenyelesaian) {
		this.idGuidPenyelesaian = idGuidPenyelesaian;
	}

	public String getJenisPembayaranPenyelesaian() {
		return jenisPembayaranPenyelesaian;
	}

	public void setJenisPembayaranPenyelesaian(
			String jenisPembayaranPenyelesaian) {
		this.jenisPembayaranPenyelesaian = jenisPembayaranPenyelesaian;
	}

	public BigDecimal getJmlByrAmblPenyelesaian() {
		return jmlByrAmblPenyelesaian;
	}

	public void setJmlByrAmblPenyelesaian(BigDecimal jmlByrAmblPenyelesaian) {
		this.jmlByrAmblPenyelesaian = jmlByrAmblPenyelesaian;
	}

	public BigDecimal getJmlByrP3kPenyelesaian() {
		return jmlByrP3kPenyelesaian;
	}

	public void setJmlByrP3kPenyelesaian(BigDecimal jmlByrP3kPenyelesaian) {
		this.jmlByrP3kPenyelesaian = jmlByrP3kPenyelesaian;
	}

	public BigDecimal getJumlahDibayarLukalukaPenyelesaian() {
		return jumlahDibayarLukalukaPenyelesaian;
	}

	public void setJumlahDibayarLukalukaPenyelesaian(
			BigDecimal jumlahDibayarLukalukaPenyelesaian) {
		this.jumlahDibayarLukalukaPenyelesaian = jumlahDibayarLukalukaPenyelesaian;
	}

	public BigDecimal getJumlahDibayarMeninggalPenyelesaian() {
		return jumlahDibayarMeninggalPenyelesaian;
	}

	public void setJumlahDibayarMeninggalPenyelesaian(
			BigDecimal jumlahDibayarMeninggalPenyelesaian) {
		this.jumlahDibayarMeninggalPenyelesaian = jumlahDibayarMeninggalPenyelesaian;
	}

	public BigDecimal getJumlahDibayarPenguburanPenyelesaian() {
		return jumlahDibayarPenguburanPenyelesaian;
	}

	public void setJumlahDibayarPenguburanPenyelesaian(
			BigDecimal jumlahDibayarPenguburanPenyelesaian) {
		this.jumlahDibayarPenguburanPenyelesaian = jumlahDibayarPenguburanPenyelesaian;
	}

	public BigDecimal getKelebihanBayarPenyelesaian() {
		return kelebihanBayarPenyelesaian;
	}

	public void setKelebihanBayarPenyelesaian(
			BigDecimal kelebihanBayarPenyelesaian) {
		this.kelebihanBayarPenyelesaian = kelebihanBayarPenyelesaian;
	}

	public String getLastUpdatedByPenyelesaian() {
		return lastUpdatedByPenyelesaian;
	}

	public void setLastUpdatedByPenyelesaian(String lastUpdatedByPenyelesaian) {
		this.lastUpdatedByPenyelesaian = lastUpdatedByPenyelesaian;
	}

	public Date getLastUpdatedDatePenyelesaian() {
		return lastUpdatedDatePenyelesaian;
	}

	public void setLastUpdatedDatePenyelesaian(Date lastUpdatedDatePenyelesaian) {
		this.lastUpdatedDatePenyelesaian = lastUpdatedDatePenyelesaian;
	}

	public String getNoBpkPenyelesaian() {
		return noBpkPenyelesaian;
	}

	public void setNoBpkPenyelesaian(String noBpkPenyelesaian) {
		this.noBpkPenyelesaian = noBpkPenyelesaian;
	}

	public String getNoRekeningPenyelesaian() {
		return noRekeningPenyelesaian;
	}

	public void setNoRekeningPenyelesaian(String noRekeningPenyelesaian) {
		this.noRekeningPenyelesaian = noRekeningPenyelesaian;
	}

	public String getNoSuratPenyelesaianPenyelesaian() {
		return noSuratPenyelesaianPenyelesaian;
	}

	public void setNoSuratPenyelesaianPenyelesaian(
			String noSuratPenyelesaianPenyelesaian) {
		this.noSuratPenyelesaianPenyelesaian = noSuratPenyelesaianPenyelesaian;
	}

	public Date getTglKelebihanBayarPenyelesaian() {
		return tglKelebihanBayarPenyelesaian;
	}

	public void setTglKelebihanBayarPenyelesaian(
			Date tglKelebihanBayarPenyelesaian) {
		this.tglKelebihanBayarPenyelesaian = tglKelebihanBayarPenyelesaian;
	}

	public Date getTglPembuatanBpkPenyelesaian() {
		return tglPembuatanBpkPenyelesaian;
	}

	public void setTglPembuatanBpkPenyelesaian(Date tglPembuatanBpkPenyelesaian) {
		this.tglPembuatanBpkPenyelesaian = tglPembuatanBpkPenyelesaian;
	}

	public Date getTglProsesPenyelesaian() {
		return tglProsesPenyelesaian;
	}

	public void setTglProsesPenyelesaian(Date tglProsesPenyelesaian) {
		this.tglProsesPenyelesaian = tglProsesPenyelesaian;
	}

	public boolean isBerkasSelesai() {
		return berkasSelesai;
	}

	public void setBerkasSelesai(boolean berkasSelesai) {
		this.berkasSelesai = berkasSelesai;
	}

	public String getKodeKantorDilimpahkan() {
		return kodeKantorDilimpahkan;
	}

	public void setKodeKantorDilimpahkan(String kodeKantorDilimpahkan) {
		this.kodeKantorDilimpahkan = kodeKantorDilimpahkan;
	}

	public String getNamaKantorDilimpahkan() {
		return namaKantorDilimpahkan;
	}

	public void setNamaKantorDilimpahkan(String namaKantorDilimpahkan) {
		this.namaKantorDilimpahkan = namaKantorDilimpahkan;
	}

	public String getDeskripsiKecelakaan() {
		return deskripsiKecelakaan;
	}

	public void setDeskripsiKecelakaan(String deskripsiKecelakaan) {
		this.deskripsiKecelakaan = deskripsiKecelakaan;
	}

	public BigDecimal getTotalPembayaran() {
		return totalPembayaran;
	}

	public void setTotalPembayaran(BigDecimal totalPembayaran) {
		this.totalPembayaran = totalPembayaran;
	}

	public String getNoBerkasAdd() {
		return noBerkasAdd;
	}

	public void setNoBerkasAdd(String noBerkasAdd) {
		this.noBerkasAdd = noBerkasAdd;
	}

	public String getCreatedByAdd() {
		return createdByAdd;
	}

	public void setCreatedByAdd(String createdByAdd) {
		this.createdByAdd = createdByAdd;
	}

	public Date getCreatedDateAdd() {
		return createdDateAdd;
	}

	public void setCreatedDateAdd(Date createdDateAdd) {
		this.createdDateAdd = createdDateAdd;
	}

	public String getIdRekRsAdd() {
		return idRekRsAdd;
	}

	public void setIdRekRsAdd(String idRekRsAdd) {
		this.idRekRsAdd = idRekRsAdd;
	}

	public String getJnsRekeningAdd() {
		return jnsRekeningAdd;
	}

	public void setJnsRekeningAdd(String jnsRekeningAdd) {
		this.jnsRekeningAdd = jnsRekeningAdd;
	}

	public String getLastUpdatedByAdd() {
		return lastUpdatedByAdd;
	}

	public void setLastUpdatedByAdd(String lastUpdatedByAdd) {
		this.lastUpdatedByAdd = lastUpdatedByAdd;
	}

	public Date getLastUpdatedDateAdd() {
		return lastUpdatedDateAdd;
	}

	public void setLastUpdatedDateAdd(Date lastUpdatedDateAdd) {
		this.lastUpdatedDateAdd = lastUpdatedDateAdd;
	}

	public String getNamaRekeningAdd() {
		return namaRekeningAdd;
	}

	public void setNamaRekeningAdd(String namaRekeningAdd) {
		this.namaRekeningAdd = namaRekeningAdd;
	}

	public Date getTglMdAdd() {
		return tglMdAdd;
	}

	public void setTglMdAdd(Date tglMdAdd) {
		this.tglMdAdd = tglMdAdd;
	}

	public Date getTglRawatAkhirAdd() {
		return tglRawatAkhirAdd;
	}

	public void setTglRawatAkhirAdd(Date tglRawatAkhirAdd) {
		this.tglRawatAkhirAdd = tglRawatAkhirAdd;
	}

	public Date getTglRawatAwalAdd() {
		return tglRawatAwalAdd;
	}

	public void setTglRawatAwalAdd(Date tglRawatAwalAdd) {
		this.tglRawatAwalAdd = tglRawatAwalAdd;
	}

	public Date getTglTerimaLimpahAdd() {
		return tglTerimaLimpahAdd;
	}

	public void setTglTerimaLimpahAdd(Date tglTerimaLimpahAdd) {
		this.tglTerimaLimpahAdd = tglTerimaLimpahAdd;
	}

	public String getDeskripsiLokasi() {
		return deskripsiLokasi;
	}

	public void setDeskripsiLokasi(String deskripsiLokasi) {
		this.deskripsiLokasi = deskripsiLokasi;
	}

	public String getNoPolisi() {
		return noPolisi;
	}

	public void setNoPolisi(String noPolisi) {
		this.noPolisi = noPolisi;
	}

	public String getSifatLakaDesc() {
		return sifatLakaDesc;
	}

	public void setSifatLakaDesc(String sifatLakaDesc) {
		this.sifatLakaDesc = sifatLakaDesc;
	}

	public String getJenisKendaraanDesc() {
		return jenisKendaraanDesc;
	}

	public void setJenisKendaraanDesc(String jenisKendaraanDesc) {
		this.jenisKendaraanDesc = jenisKendaraanDesc;
	}

	public String getUmurKorban() {
		return umurKorban;
	}

	public void setUmurKorban(String umurKorban) {
		this.umurKorban = umurKorban;
	}

	public String getHubKorbanDesc() {
		return hubKorbanDesc;
	}

	public void setHubKorbanDesc(String hubKorbanDesc) {
		this.hubKorbanDesc = hubKorbanDesc;
	}

	public String getLingkupJaminan() {
		return lingkupJaminan;
	}

	public void setLingkupJaminan(String lingkupJaminan) {
		this.lingkupJaminan = lingkupJaminan;
	}

	public BigDecimal getUmur() {
		return umur;
	}

	public void setUmur(BigDecimal umur) {
		this.umur = umur;
	}

	public String getJaminanDesc() {
		return jaminanDesc;
	}

	public void setJaminanDesc(String jaminanDesc) {
		this.jaminanDesc = jaminanDesc;
	}

	public Date getTglLaporanPolisi() {
		return tglLaporanPolisi;
	}

	public void setTglLaporanPolisi(Date tglLaporanPolisi) {
		this.tglLaporanPolisi = tglLaporanPolisi;
	}

	public Date getTglCetak() {
		return tglCetak;
	}

	public void setTglCetak(Date tglCetak) {
		this.tglCetak = tglCetak;
	}

	public String getLoketMenangani() {
		return loketMenangani;
	}

	public void setLoketMenangani(String loketMenangani) {
		this.loketMenangani = loketMenangani;
	}

	public String getNamaInstansi() {
		return namaInstansi;
	}

	public void setNamaInstansi(String namaInstansi) {
		this.namaInstansi = namaInstansi;
	}

	public String getKasusKecelakaan() {
		return kasusKecelakaan;
	}

	public void setKasusKecelakaan(String kasusKecelakaan) {
		this.kasusKecelakaan = kasusKecelakaan;
	}

	public String getStatusKroban() {
		return statusKroban;
	}

	public void setStatusKroban(String statusKroban) {
		this.statusKroban = statusKroban;
	}

	public String getGolonganKendaraan() {
		return golonganKendaraan;
	}

	public void setGolonganKendaraan(String golonganKendaraan) {
		this.golonganKendaraan = golonganKendaraan;
	}

	public String getNoTelpPemohon() {
		return noTelpPemohon;
	}

	public void setNoTelpPemohon(String noTelpPemohon) {
		this.noTelpPemohon = noTelpPemohon;
	}

	public String getNoIdentitasKorban() {
		return noIdentitasKorban;
	}

	public void setNoIdentitasKorban(String noIdentitasKorban) {
		this.noIdentitasKorban = noIdentitasKorban;
	}

	public String getNoIdentitasPemohon() {
		return noIdentitasPemohon;
	}

	public void setNoIdentitasPemohon(String noIdentitasPemohon) {
		this.noIdentitasPemohon = noIdentitasPemohon;
	}

	public String getNamaKantorDiajukanDi() {
		return namaKantorDiajukanDi;
	}

	public void setNamaKantorDiajukanDi(String namaKantorDiajukanDi) {
		this.namaKantorDiajukanDi = namaKantorDiajukanDi;
	}

	public BigDecimal getJeda() {
		return jeda;
	}

	public void setJeda(BigDecimal jeda) {
		this.jeda = jeda;
	}

	public String getNoTelpKorban() {
		return noTelpKorban;
	}

	public void setNoTelpKorban(String noTelpKorban) {
		this.noTelpKorban = noTelpKorban;
	}

	public BigDecimal getJumlahPengajuanLuka2RS() {
		return jumlahPengajuanLuka2RS;
	}

	public void setJumlahPengajuanLuka2RS(BigDecimal jumlahPengajuanLuka2RS) {
		this.jumlahPengajuanLuka2RS = jumlahPengajuanLuka2RS;
	}

	public String getNamaRS() {
		return namaRS;
	}

	public void setNamaRS(String namaRS) {
		this.namaRS = namaRS;
	}

	public Date getTglTindakLanjut() {
		return tglTindakLanjut;
	}

	public void setTglTindakLanjut(Date tglTindakLanjut) {
		this.tglTindakLanjut = tglTindakLanjut;
	}

	public String getTindakLanjutDesc() {
		return tindakLanjutDesc;
	}

	public void setTindakLanjutDesc(String tindakLanjutDesc) {
		this.tindakLanjutDesc = tindakLanjutDesc;
	}

	public boolean isMenuPengajuan() {
		return menuPengajuan;
	}

	public void setMenuPengajuan(boolean menuPengajuan) {
		this.menuPengajuan = menuPengajuan;
	}

	public boolean isMenuIdentifikasi() {
		return menuIdentifikasi;
	}

	public void setMenuIdentifikasi(boolean menuIdentifikasi) {
		this.menuIdentifikasi = menuIdentifikasi;
	}

	public boolean isMenuOtorisasi() {
		return menuOtorisasi;
	}

	public void setMenuOtorisasi(boolean menuOtorisasi) {
		this.menuOtorisasi = menuOtorisasi;
	}

	public String getTglPelimpahan() {
		return tglPelimpahan;
	}

	public void setTglPelimpahan(String tglPelimpahan) {
		this.tglPelimpahan = tglPelimpahan;
	}

	public String getTglTerjadi() {
		return tglTerjadi;
	}

	public void setTglTerjadi(String tglTerjadi) {
		this.tglTerjadi = tglTerjadi;
	}

	public String getAlamatRS() {
		return alamatRS;
	}

	public void setAlamatRS(String alamatRS) {
		this.alamatRS = alamatRS;
	}

	public String getTglPenyelesaianStr() {
		return tglPenyelesaianStr;
	}

	public void setTglPenyelesaianStr(String tglPenyelesaianStr) {
		this.tglPenyelesaianStr = tglPenyelesaianStr;
	}

	public String getTglKejadianStr() {
		return tglKejadianStr;
	}

	public void setTglKejadianStr(String tglKejadianStr) {
		this.tglKejadianStr = tglKejadianStr;
	}

	public String getStatusLaporanPolisi() {
		return statusLaporanPolisi;
	}

	public void setStatusLaporanPolisi(String statusLaporanPolisi) {
		this.statusLaporanPolisi = statusLaporanPolisi;
	}

	public String getKesimpulanSementaraDesc() {
		return kesimpulanSementaraDesc;
	}

	public void setKesimpulanSementaraDesc(String kesimpulanSementaraDesc) {
		this.kesimpulanSementaraDesc = kesimpulanSementaraDesc;
	}

	public String getJaminaanPembayaranDesc() {
		return jaminaanPembayaranDesc;
	}

	public void setJaminaanPembayaranDesc(String jaminaanPembayaranDesc) {
		this.jaminaanPembayaranDesc = jaminaanPembayaranDesc;
	}

	public String getOtorisasiDesc() {
		return otorisasiDesc;
	}

	public void setOtorisasiDesc(String otorisasiDesc) {
		this.otorisasiDesc = otorisasiDesc;
	}

	public String getUrutProses() {
		return urutProses;
	}

	public void setUrutProses(String urutProses) {
		this.urutProses = urutProses;
	}

	public String getKodeKantorJr() {
		return kodeKantorJr;
	}

	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

	public String getKodeBank() {
		return kodeBank;
	}

	public void setKodeBank(String kodeBank) {
		this.kodeBank = kodeBank;
	}

	public String getJenisRek() {
		return jenisRek;
	}

	public void setJenisRek(String jenisRek) {
		this.jenisRek = jenisRek;
	}

	public String getPemohon() {
		return pemohon;
	}

	public void setPemohon(String pemohon) {
		this.pemohon = pemohon;
	}

	public BigDecimal getNoBpk() {
		return noBpk;
	}

	public void setNoBpk(BigDecimal noBpk) {
		this.noBpk = noBpk;
	}

	public String getNamaKantorAsal() {
		return namaKantorAsal;
	}

	public void setNamaKantorAsal(String namaKantorAsal) {
		this.namaKantorAsal = namaKantorAsal;
	}

	public String getKodeLokasiPemohonAdd() {
		return kodeLokasiPemohonAdd;
	}

	public void setKodeLokasiPemohonAdd(String kodeLokasiPemohonAdd) {
		this.kodeLokasiPemohonAdd = kodeLokasiPemohonAdd;
	}

	public String getDiterimaFlagBerkasPengajuan() {
		return diterimaFlagBerkasPengajuan;
	}

	public void setDiterimaFlagBerkasPengajuan(String diterimaFlagBerkasPengajuan) {
		this.diterimaFlagBerkasPengajuan = diterimaFlagBerkasPengajuan;
	}

	public String getKodeJaminanLaporan() {
		return kodeJaminanLaporan;
	}

	public void setKodeJaminanLaporan(String kodeJaminanLaporan) {
		this.kodeJaminanLaporan = kodeJaminanLaporan;
	}

}
