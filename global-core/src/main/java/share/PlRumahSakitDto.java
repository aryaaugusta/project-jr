package share;

import java.io.Serializable;
import java.util.Date;

public class PlRumahSakitDto implements Serializable{
	private static final long serialVersionUID = 1L;

	private String kodeRumahsakit;
	private String alamat;
	private String createdBy;
	private Date creationDate;
	private String deskripsi;
	private String flagEnable;
	private String jenisRs;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String noTelp;
	private String pic;
	
	//added by Luthfi
	private int no;
	private String kodeNamaRs;
	private String loketKantor;
	private String loketKantorKode;
	
	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}
	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getFlagEnable() {
		return flagEnable;
	}
	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}
	public String getJenisRs() {
		return jenisRs;
	}
	public void setJenisRs(String jenisRs) {
		this.jenisRs = jenisRs;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNoTelp() {
		return noTelp;
	}
	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
	public String getPic() {
		return pic;
	}
	public void setPic(String pic) {
		this.pic = pic;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getKodeNamaRs() {
		return kodeNamaRs;
	}
	public void setKodeNamaRs(String kodeNamaRs) {
		this.kodeNamaRs = kodeNamaRs;
	}
	public String getLoketKantor() {
		return loketKantor;
	}
	public void setLoketKantor(String loketKantor) {
		this.loketKantor = loketKantor;
	}
	public String getLoketKantorKode() {
		return loketKantorKode;
	}
	public void setLoketKantorKode(String loketKantorKode) {
		this.loketKantorKode = loketKantorKode;
	}
}
