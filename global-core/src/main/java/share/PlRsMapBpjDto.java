package share;

import java.io.Serializable;
import java.util.Date;

public class PlRsMapBpjDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String alamatRsBpjs;
	private String createdBy;
	private Date creationDate;
	private String flagKodeBpjsDobel;
	private String kabKotaRsBpjs;
	private String kodeBpjs;
	private String kodeInstansi;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String loketPenanggungjawab;
	private String namaRsBpjs;
	private String propinsiRsBpjs;
	private String kodeRumahsakit;
	
	//added by Luthfi
	private String kodeNama;

	public String getAlamatRsBpjs() {
		return alamatRsBpjs;
	}

	public void setAlamatRsBpjs(String alamatRsBpjs) {
		this.alamatRsBpjs = alamatRsBpjs;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getFlagKodeBpjsDobel() {
		return flagKodeBpjsDobel;
	}

	public void setFlagKodeBpjsDobel(String flagKodeBpjsDobel) {
		this.flagKodeBpjsDobel = flagKodeBpjsDobel;
	}

	public String getKabKotaRsBpjs() {
		return kabKotaRsBpjs;
	}

	public void setKabKotaRsBpjs(String kabKotaRsBpjs) {
		this.kabKotaRsBpjs = kabKotaRsBpjs;
	}

	public String getKodeBpjs() {
		return kodeBpjs;
	}

	public void setKodeBpjs(String kodeBpjs) {
		this.kodeBpjs = kodeBpjs;
	}

	public String getKodeInstansi() {
		return kodeInstansi;
	}

	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getLoketPenanggungjawab() {
		return loketPenanggungjawab;
	}

	public void setLoketPenanggungjawab(String loketPenanggungjawab) {
		this.loketPenanggungjawab = loketPenanggungjawab;
	}

	public String getNamaRsBpjs() {
		return namaRsBpjs;
	}

	public void setNamaRsBpjs(String namaRsBpjs) {
		this.namaRsBpjs = namaRsBpjs;
	}

	public String getPropinsiRsBpjs() {
		return propinsiRsBpjs;
	}

	public void setPropinsiRsBpjs(String propinsiRsBpjs) {
		this.propinsiRsBpjs = propinsiRsBpjs;
	}

	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}

	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}

	public String getKodeNama() {
		return kodeNama;
	}

	public void setKodeNama(String kodeNama) {
		this.kodeNama = kodeNama;
	}
	
}
