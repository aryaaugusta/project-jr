package share;

import java.io.Serializable;

public class GeneralDto implements Serializable{

	public GeneralDto(){
		
	}
	
	public GeneralDto(String kode, String name) {
		super();
		this.kode = kode;
		this.name = name;
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String kode;
	private String name;
	private String codeName;
	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCodeName() {
		return codeName;
	}
	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}
	
	

	
}
