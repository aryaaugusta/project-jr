package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class FndBankDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kodeBank;
	private String kodeBank2;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String namaBank;
	private String status;
	private BigDecimal jumlahRekening;
	private int no;
	private String statusFlag;
	private String mode;
	
	
	public String getStatusFlag() {
		return statusFlag;
	}
	public void setStatusFlag(String statusFlag) {
		this.statusFlag = statusFlag;
	}
	public BigDecimal getJumlahRekening() {
		return jumlahRekening;
	}
	public void setJumlahRekening(BigDecimal jumlahRekening) {
		this.jumlahRekening = jumlahRekening;
	}
	public String getKodeBank() {
		return kodeBank;
	}
	public void setKodeBank(String kodeBank) {
		this.kodeBank = kodeBank;
	}
	public String getKodeBank2() {
		return kodeBank2;
	}
	public void setKodeBank2(String kodeBank2) {
		this.kodeBank2 = kodeBank2;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNamaBank() {
		return namaBank;
	}
	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}

}
