package share;

import java.io.Serializable;
import java.util.Date;

public class PlPksRDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idRsPks;
	private String createdBy;
	private Date creationDate;
	private String flagAktif;
	private String keterangan;
	private String kodeKantorJr;
	private String kodeRumahsakit;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String masaLaku;
	private String noPksJr;
	private String noPksRs;
	private Date tglLakuAkhir;
	private Date tglPks;
	private String flagAktifDesc;
	
	private String kantor;
	
	public String getIdRsPks() {
		return idRsPks;
	}
	public void setIdRsPks(String idRsPks) {
		this.idRsPks = idRsPks;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getFlagAktif() {
		return flagAktif;
	}
	public void setFlagAktif(String flagAktif) {
		this.flagAktif = flagAktif;
	}
	public String getKeterangan() {
		return keterangan;
	}
	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}
	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getMasaLaku() {
		return masaLaku;
	}
	public void setMasaLaku(String masaLaku) {
		this.masaLaku = masaLaku;
	}
	public String getNoPksJr() {
		return noPksJr;
	}
	public void setNoPksJr(String noPksJr) {
		this.noPksJr = noPksJr;
	}
	public String getNoPksRs() {
		return noPksRs;
	}
	public void setNoPksRs(String noPksRs) {
		this.noPksRs = noPksRs;
	}
	public Date getTglLakuAkhir() {
		return tglLakuAkhir;
	}
	public void setTglLakuAkhir(Date tglLakuAkhir) {
		this.tglLakuAkhir = tglLakuAkhir;
	}
	public Date getTglPks() {
		return tglPks;
	}
	public void setTglPks(Date tglPks) {
		this.tglPks = tglPks;
	}
	public String getFlagAktifDesc() {
		return flagAktifDesc;
	}
	public void setFlagAktifDesc(String flagAktifDesc) {
		this.flagAktifDesc = flagAktifDesc;
	}
	public String getKantor() {
		return kantor;
	}
	public void setKantor(String kantor) {
		this.kantor = kantor;
	}
	
}
