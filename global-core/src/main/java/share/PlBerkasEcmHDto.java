package share;

import java.io.Serializable;

public class PlBerkasEcmHDto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private String noRegEcms;
	private String idKorbanKecelakaan;
	public String getNoRegEcms() {
		return noRegEcms;
	}
	public void setNoRegEcms(String noRegEcms) {
		this.noRegEcms = noRegEcms;
	}
	public String getIdKorbanKecelakaan() {
		return idKorbanKecelakaan;
	}
	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}
	
	

}
