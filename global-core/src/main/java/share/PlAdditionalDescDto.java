package share;

import java.io.Serializable;
import java.util.Date;


public class PlAdditionalDescDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String noBerkas;
	private String createdBy;
	private Date createdDate;
	private String idRekRs;
	private String jnsRekening;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String namaRekening;
	private Date tglMd;
	private Date tglRawatAkhir;
	private Date tglRawatAwal;
	private Date tglTerimaLimpah;
	
	private String lastUpdateByPenyelesaian;
	private Date lastUpdateDatePenyelesaian;
	private String jenisPembayaranPenyelesaian;
	private String noRekPenyelesaian;
	private String idGUIDPenyelesaian;
	
	private String kodeLokasiPemohon;

	
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getIdRekRs() {
		return idRekRs;
	}
	public void setIdRekRs(String idRekRs) {
		this.idRekRs = idRekRs;
	}
	public String getJnsRekening() {
		return jnsRekening;
	}
	public void setJnsRekening(String jnsRekening) {
		this.jnsRekening = jnsRekening;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNamaRekening() {
		return namaRekening;
	}
	public void setNamaRekening(String namaRekening) {
		this.namaRekening = namaRekening;
	}
	public Date getTglMd() {
		return tglMd;
	}
	public void setTglMd(Date tglMd) {
		this.tglMd = tglMd;
	}
	public Date getTglRawatAkhir() {
		return tglRawatAkhir;
	}
	public void setTglRawatAkhir(Date tglRawatAkhir) {
		this.tglRawatAkhir = tglRawatAkhir;
	}
	public Date getTglRawatAwal() {
		return tglRawatAwal;
	}
	public void setTglRawatAwal(Date tglRawatAwal) {
		this.tglRawatAwal = tglRawatAwal;
	}
	public Date getTglTerimaLimpah() {
		return tglTerimaLimpah;
	}
	public void setTglTerimaLimpah(Date tglTerimaLimpah) {
		this.tglTerimaLimpah = tglTerimaLimpah;
	}
	public String getLastUpdateByPenyelesaian() {
		return lastUpdateByPenyelesaian;
	}
	public void setLastUpdateByPenyelesaian(String lastUpdateByPenyelesaian) {
		this.lastUpdateByPenyelesaian = lastUpdateByPenyelesaian;
	}
	public Date getLastUpdateDatePenyelesaian() {
		return lastUpdateDatePenyelesaian;
	}
	public void setLastUpdateDatePenyelesaian(Date lastUpdateDatePenyelesaian) {
		this.lastUpdateDatePenyelesaian = lastUpdateDatePenyelesaian;
	}
	public String getJenisPembayaranPenyelesaian() {
		return jenisPembayaranPenyelesaian;
	}
	public void setJenisPembayaranPenyelesaian(String jenisPembayaranPenyelesaian) {
		this.jenisPembayaranPenyelesaian = jenisPembayaranPenyelesaian;
	}
	public String getNoRekPenyelesaian() {
		return noRekPenyelesaian;
	}
	public void setNoRekPenyelesaian(String noRekPenyelesaian) {
		this.noRekPenyelesaian = noRekPenyelesaian;
	}
	public String getIdGUIDPenyelesaian() {
		return idGUIDPenyelesaian;
	}
	public void setIdGUIDPenyelesaian(String idGUIDPenyelesaian) {
		this.idGUIDPenyelesaian = idGUIDPenyelesaian;
	}
	public String getKodeLokasiPemohon() {
		return kodeLokasiPemohon;
	}
	public void setKodeLokasiPemohon(String kodeLokasiPemohon) {
		this.kodeLokasiPemohon = kodeLokasiPemohon;
	}

}
