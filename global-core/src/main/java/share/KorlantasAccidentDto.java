package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


public class KorlantasAccidentDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal id;
	private String accNoN;
	private Timestamp accidentDate;
	private String accidentDiagramId;
	private String accidentNo;
	private String accidentSyncNum;
	private BigDecimal closedId;
	private Timestamp createdAt;
	private BigDecimal devSerial;
	private BigDecimal diagramDir;
	private String directionToAccident;
	private String distanceToAccident;
	private BigDecimal districtId;
	private BigDecimal estCostMaterialDamage;
	private BigDecimal estCostVehiclesDamage;
	private BigDecimal extraOrd;
	private Timestamp finishedDate;
	private BigDecimal gpsLatitude;
	private BigDecimal gpsLatitudeCorrected;
	private BigDecimal gpsLongitude;
	private BigDecimal gpsLongitudeCorrected;
	private BigDecimal heavyInjuredPersonsNumber;
	private BigDecimal hitAndRun;
	private BigDecimal injuredPersonsNumber;
	private BigDecimal killedPersonsNumber;
	private BigDecimal lightId;
	private Timestamp lpDate;
	private String mobileVersion;
	private BigDecimal officerId;
	private String policeDetails;
	private String referenceDesc;
	private BigDecimal referenceTypeId;
	private BigDecimal reportingOfficerId;
	private BigDecimal roadCharacterId;
	private BigDecimal roadClassId;
	private BigDecimal roadFunctionId;
	private BigDecimal roadGradientId;
	private BigDecimal roadKm;
	private String roadName;
	private String roadNumber;
	private BigDecimal roadStateId;
	private BigDecimal roadTypeId;
	private BigDecimal rtaNo;
	private BigDecimal severityId;
	private BigDecimal speedId;
	private BigDecimal state;
	private BigDecimal surfaceConditionId;
	private BigDecimal valid;
	private Timestamp validDate;
	private BigDecimal weatherTypeId;
	
	//index monitoring data laka irsms
	private String instansiDesc;
	private Date tglKejadian;
	private Date tglLaporanPolisi;
	private String noLaporan;
	private String lokasi;
	private String korban;
	private boolean mappingVisible;
	private boolean unMappingVisible;
	private String mappingStatus;
	private String kodeInstansi;
	
	//idkorban
	private String idLakaJr;
	private String idKorbanJr;
	
	private List<PlKorbanKecelakaanDto> listKorbanDasiDto;
	private PlKorbanKecelakaanDto korbanDasiDto;
	private String namaCidera;
	
	public String getAccNoN() {
		return accNoN;
	}
	public void setAccNoN(String accNoN) {
		this.accNoN = accNoN;
	}
	public Timestamp getAccidentDate() {
		return accidentDate;
	}
	public void setAccidentDate(Timestamp accidentDate) {
		this.accidentDate = accidentDate;
	}
	public String getAccidentDiagramId() {
		return accidentDiagramId;
	}
	public void setAccidentDiagramId(String accidentDiagramId) {
		this.accidentDiagramId = accidentDiagramId;
	}
	public String getAccidentNo() {
		return accidentNo;
	}
	public void setAccidentNo(String accidentNo) {
		this.accidentNo = accidentNo;
	}
	public String getAccidentSyncNum() {
		return accidentSyncNum;
	}
	public void setAccidentSyncNum(String accidentSyncNum) {
		this.accidentSyncNum = accidentSyncNum;
	}
	public BigDecimal getClosedId() {
		return closedId;
	}
	public void setClosedId(BigDecimal closedId) {
		this.closedId = closedId;
	}
	public Timestamp getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}
	public BigDecimal getDevSerial() {
		return devSerial;
	}
	public void setDevSerial(BigDecimal devSerial) {
		this.devSerial = devSerial;
	}
	public BigDecimal getDiagramDir() {
		return diagramDir;
	}
	public void setDiagramDir(BigDecimal diagramDir) {
		this.diagramDir = diagramDir;
	}
	public String getDirectionToAccident() {
		return directionToAccident;
	}
	public void setDirectionToAccident(String directionToAccident) {
		this.directionToAccident = directionToAccident;
	}
	public String getDistanceToAccident() {
		return distanceToAccident;
	}
	public void setDistanceToAccident(String distanceToAccident) {
		this.distanceToAccident = distanceToAccident;
	}
	public BigDecimal getDistrictId() {
		return districtId;
	}
	public void setDistrictId(BigDecimal districtId) {
		this.districtId = districtId;
	}
	public BigDecimal getEstCostMaterialDamage() {
		return estCostMaterialDamage;
	}
	public void setEstCostMaterialDamage(BigDecimal estCostMaterialDamage) {
		this.estCostMaterialDamage = estCostMaterialDamage;
	}
	public BigDecimal getEstCostVehiclesDamage() {
		return estCostVehiclesDamage;
	}
	public void setEstCostVehiclesDamage(BigDecimal estCostVehiclesDamage) {
		this.estCostVehiclesDamage = estCostVehiclesDamage;
	}
	public BigDecimal getExtraOrd() {
		return extraOrd;
	}
	public void setExtraOrd(BigDecimal extraOrd) {
		this.extraOrd = extraOrd;
	}
	public Timestamp getFinishedDate() {
		return finishedDate;
	}
	public void setFinishedDate(Timestamp finishedDate) {
		this.finishedDate = finishedDate;
	}
	public BigDecimal getGpsLatitude() {
		return gpsLatitude;
	}
	public void setGpsLatitude(BigDecimal gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}
	public BigDecimal getGpsLatitudeCorrected() {
		return gpsLatitudeCorrected;
	}
	public void setGpsLatitudeCorrected(BigDecimal gpsLatitudeCorrected) {
		this.gpsLatitudeCorrected = gpsLatitudeCorrected;
	}
	public BigDecimal getGpsLongitude() {
		return gpsLongitude;
	}
	public void setGpsLongitude(BigDecimal gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}
	public BigDecimal getGpsLongitudeCorrected() {
		return gpsLongitudeCorrected;
	}
	public void setGpsLongitudeCorrected(BigDecimal gpsLongitudeCorrected) {
		this.gpsLongitudeCorrected = gpsLongitudeCorrected;
	}
	public BigDecimal getHeavyInjuredPersonsNumber() {
		return heavyInjuredPersonsNumber;
	}
	public void setHeavyInjuredPersonsNumber(BigDecimal heavyInjuredPersonsNumber) {
		this.heavyInjuredPersonsNumber = heavyInjuredPersonsNumber;
	}
	public BigDecimal getHitAndRun() {
		return hitAndRun;
	}
	public void setHitAndRun(BigDecimal hitAndRun) {
		this.hitAndRun = hitAndRun;
	}
	public BigDecimal getInjuredPersonsNumber() {
		return injuredPersonsNumber;
	}
	public void setInjuredPersonsNumber(BigDecimal injuredPersonsNumber) {
		this.injuredPersonsNumber = injuredPersonsNumber;
	}
	public BigDecimal getKilledPersonsNumber() {
		return killedPersonsNumber;
	}
	public void setKilledPersonsNumber(BigDecimal killedPersonsNumber) {
		this.killedPersonsNumber = killedPersonsNumber;
	}
	public BigDecimal getLightId() {
		return lightId;
	}
	public void setLightId(BigDecimal lightId) {
		this.lightId = lightId;
	}
	public Timestamp getLpDate() {
		return lpDate;
	}
	public void setLpDate(Timestamp lpDate) {
		this.lpDate = lpDate;
	}
	public String getMobileVersion() {
		return mobileVersion;
	}
	public void setMobileVersion(String mobileVersion) {
		this.mobileVersion = mobileVersion;
	}
	public BigDecimal getOfficerId() {
		return officerId;
	}
	public void setOfficerId(BigDecimal officerId) {
		this.officerId = officerId;
	}
	public String getPoliceDetails() {
		return policeDetails;
	}
	public void setPoliceDetails(String policeDetails) {
		this.policeDetails = policeDetails;
	}
	public String getReferenceDesc() {
		return referenceDesc;
	}
	public void setReferenceDesc(String referenceDesc) {
		this.referenceDesc = referenceDesc;
	}
	public BigDecimal getReferenceTypeId() {
		return referenceTypeId;
	}
	public void setReferenceTypeId(BigDecimal referenceTypeId) {
		this.referenceTypeId = referenceTypeId;
	}
	public BigDecimal getReportingOfficerId() {
		return reportingOfficerId;
	}
	public void setReportingOfficerId(BigDecimal reportingOfficerId) {
		this.reportingOfficerId = reportingOfficerId;
	}
	public BigDecimal getRoadCharacterId() {
		return roadCharacterId;
	}
	public void setRoadCharacterId(BigDecimal roadCharacterId) {
		this.roadCharacterId = roadCharacterId;
	}
	public BigDecimal getRoadClassId() {
		return roadClassId;
	}
	public void setRoadClassId(BigDecimal roadClassId) {
		this.roadClassId = roadClassId;
	}
	public BigDecimal getRoadFunctionId() {
		return roadFunctionId;
	}
	public void setRoadFunctionId(BigDecimal roadFunctionId) {
		this.roadFunctionId = roadFunctionId;
	}
	public BigDecimal getRoadGradientId() {
		return roadGradientId;
	}
	public void setRoadGradientId(BigDecimal roadGradientId) {
		this.roadGradientId = roadGradientId;
	}
	public BigDecimal getRoadKm() {
		return roadKm;
	}
	public void setRoadKm(BigDecimal roadKm) {
		this.roadKm = roadKm;
	}
	public String getRoadName() {
		return roadName;
	}
	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}
	public String getRoadNumber() {
		return roadNumber;
	}
	public void setRoadNumber(String roadNumber) {
		this.roadNumber = roadNumber;
	}
	public BigDecimal getRoadStateId() {
		return roadStateId;
	}
	public void setRoadStateId(BigDecimal roadStateId) {
		this.roadStateId = roadStateId;
	}
	public BigDecimal getRoadTypeId() {
		return roadTypeId;
	}
	public void setRoadTypeId(BigDecimal roadTypeId) {
		this.roadTypeId = roadTypeId;
	}
	public BigDecimal getRtaNo() {
		return rtaNo;
	}
	public void setRtaNo(BigDecimal rtaNo) {
		this.rtaNo = rtaNo;
	}
	public BigDecimal getSeverityId() {
		return severityId;
	}
	public void setSeverityId(BigDecimal severityId) {
		this.severityId = severityId;
	}
	public BigDecimal getSpeedId() {
		return speedId;
	}
	public void setSpeedId(BigDecimal speedId) {
		this.speedId = speedId;
	}
	public BigDecimal getState() {
		return state;
	}
	public void setState(BigDecimal state) {
		this.state = state;
	}
	public BigDecimal getSurfaceConditionId() {
		return surfaceConditionId;
	}
	public void setSurfaceConditionId(BigDecimal surfaceConditionId) {
		this.surfaceConditionId = surfaceConditionId;
	}
	public BigDecimal getValid() {
		return valid;
	}
	public void setValid(BigDecimal valid) {
		this.valid = valid;
	}
	public Timestamp getValidDate() {
		return validDate;
	}
	public void setValidDate(Timestamp validDate) {
		this.validDate = validDate;
	}
	public BigDecimal getWeatherTypeId() {
		return weatherTypeId;
	}
	public void setWeatherTypeId(BigDecimal weatherTypeId) {
		this.weatherTypeId = weatherTypeId;
	}
	public String getInstansiDesc() {
		return instansiDesc;
	}
	public void setInstansiDesc(String instansiDesc) {
		this.instansiDesc = instansiDesc;
	}
	public Date getTglKejadian() {
		return tglKejadian;
	}
	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}
	public String getNoLaporan() {
		return noLaporan;
	}
	public void setNoLaporan(String noLaporan) {
		this.noLaporan = noLaporan;
	}
	public String getLokasi() {
		return lokasi;
	}
	public void setLokasi(String lokasi) {
		this.lokasi = lokasi;
	}
	public String getKorban() {
		return korban;
	}
	public void setKorban(String korban) {
		this.korban = korban;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public boolean isMappingVisible() {
		return mappingVisible;
	}
	public void setMappingVisible(boolean mappingVisible) {
		this.mappingVisible = mappingVisible;
	}
	public boolean isUnMappingVisible() {
		return unMappingVisible;
	}
	public void setUnMappingVisible(boolean unMappingVisible) {
		this.unMappingVisible = unMappingVisible;
	}
	public Date getTglLaporanPolisi() {
		return tglLaporanPolisi;
	}
	public void setTglLaporanPolisi(Date tglLaporanPolisi) {
		this.tglLaporanPolisi = tglLaporanPolisi;
	}
	public String getMappingStatus() {
		return mappingStatus;
	}
	public void setMappingStatus(String mappingStatus) {
		this.mappingStatus = mappingStatus;
	}
	public String getIdLakaJr() {
		return idLakaJr;
	}
	public void setIdLakaJr(String idLakaJr) {
		this.idLakaJr = idLakaJr;
	}
	public String getIdKorbanJr() {
		return idKorbanJr;
	}
	public void setIdKorbanJr(String idKorbanJr) {
		this.idKorbanJr = idKorbanJr;
	}
	public String getKodeInstansi() {
		return kodeInstansi;
	}
	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}
	public List<PlKorbanKecelakaanDto> getListKorbanDasiDto() {
		return listKorbanDasiDto;
	}
	public void setListKorbanDasiDto(List<PlKorbanKecelakaanDto> listKorbanDasiDto) {
		this.listKorbanDasiDto = listKorbanDasiDto;
	}
	public PlKorbanKecelakaanDto getKorbanDasiDto() {
		return korbanDasiDto;
	}
	public void setKorbanDasiDto(PlKorbanKecelakaanDto korbanDasiDto) {
		this.korbanDasiDto = korbanDasiDto;
	}
	public String getNamaCidera() {
		return namaCidera;
	}
	public void setNamaCidera(String namaCidera) {
		this.namaCidera = namaCidera;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((idKorbanJr == null) ? 0 : idKorbanJr.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KorlantasAccidentDto other = (KorlantasAccidentDto) obj;
		if (idKorbanJr == null) {
			if (other.idKorbanJr != null)
				return false;
		} else if (!idKorbanJr.equals(other.idKorbanJr))
			return false;
		return true;
	}


}
