package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PlKorbanKecelakaanDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idKorbanKecelakaan;
	private String alamat;
	private String createdBy;
	private Date creationDate;
	private String idGuid;
	private String jenisIdentitas;
	private String jenisKelamin;
	private String kodeJaminan;
	private String kodePekerjaan;
	private String kodeSifatCidera;
	private String kodeStatusKorban;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String nama;
	private String noIdentitas;
	private String noTelp;
	private String statusNikah;
	private BigDecimal umur;
	
	private String idAngkutanKecelakaan;
	private String idAngkutanPenanggung;
	private String idKecelakaan;
	
	//added by luthfi
	private String pekerjaanDesc;
	private String statusKorbanDesc;
	private String cideraHighDesc;
	private String pertanggunganDesc;
	private String cideraDesc;
	private String jenisIdentitasDesc;
	private String statusNikahDesc;
	private String penjaminDesc;
	private String posisiDesc;
	private String kodeProvinsi;
	private String namaProvinsi;
	private String kodeKabkota;
	private String namaKabkota;
	private String kodeCamat;
	private String namaCamat;
	//untuk cetak
	private String namaJkUmur;
	private String angkutanKecelakaanDesc;
	private String angkutanPenanggungKecelakaanDesc;
	private String jaminanDesc;
	
	private String namaCidera;
	
	//data laka
	private String noLaporanPolisi;
	private Date tglKejadian;
	private Date tglLaporanPolisi;
	private String deskripsiJaminan;
	private String statusLaporanPolisi;
	private String kodeInstansi;
	private String instansiDesc;
	
	public String getIdKorbanKecelakaan() {
		return idKorbanKecelakaan;
	}
	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getIdGuid() {
		return idGuid;
	}
	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}
	public String getJenisIdentitas() {
		return jenisIdentitas;
	}
	public void setJenisIdentitas(String jenisIdentitas) {
		this.jenisIdentitas = jenisIdentitas;
	}
	public String getJenisKelamin() {
		return jenisKelamin;
	}
	public void setJenisKelamin(String jenisKelamin) {
		this.jenisKelamin = jenisKelamin;
	}
	public String getKodeJaminan() {
		return kodeJaminan;
	}
	public void setKodeJaminan(String kodeJaminan) {
		this.kodeJaminan = kodeJaminan;
	}
	public String getKodePekerjaan() {
		return kodePekerjaan;
	}
	public void setKodePekerjaan(String kodePekerjaan) {
		this.kodePekerjaan = kodePekerjaan;
	}
	public String getKodeSifatCidera() {
		return kodeSifatCidera;
	}
	public void setKodeSifatCidera(String kodeSifatCidera) {
		this.kodeSifatCidera = kodeSifatCidera;
	}
	public String getKodeStatusKorban() {
		return kodeStatusKorban;
	}
	public void setKodeStatusKorban(String kodeStatusKorban) {
		this.kodeStatusKorban = kodeStatusKorban;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getNoIdentitas() {
		return noIdentitas;
	}
	public void setNoIdentitas(String noIdentitas) {
		this.noIdentitas = noIdentitas;
	}
	public String getNoTelp() {
		return noTelp;
	}
	public void setNoTelp(String noTelp) {
		this.noTelp = noTelp;
	}
	public String getStatusNikah() {
		return statusNikah;
	}
	public void setStatusNikah(String statusNikah) {
		this.statusNikah = statusNikah;
	}
	public BigDecimal getUmur() {
		return umur;
	}
	public void setUmur(BigDecimal umur) {
		this.umur = umur;
	}
	public String getIdAngkutanKecelakaan() {
		return idAngkutanKecelakaan;
	}
	public void setIdAngkutanKecelakaan(String idAngkutanKecelakaan) {
		this.idAngkutanKecelakaan = idAngkutanKecelakaan;
	}
	public String getIdAngkutanPenanggung() {
		return idAngkutanPenanggung;
	}
	public void setIdAngkutanPenanggung(String idAngkutanPenanggung) {
		this.idAngkutanPenanggung = idAngkutanPenanggung;
	}
	public String getIdKecelakaan() {
		return idKecelakaan;
	}
	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}
	public String getPekerjaanDesc() {
		return pekerjaanDesc;
	}
	public void setPekerjaanDesc(String pekerjaanDesc) {
		this.pekerjaanDesc = pekerjaanDesc;
	}
	public String getStatusKorbanDesc() {
		return statusKorbanDesc;
	}
	public void setStatusKorbanDesc(String statusKorbanDesc) {
		this.statusKorbanDesc = statusKorbanDesc;
	}
	public String getCideraHighDesc() {
		return cideraHighDesc;
	}
	public void setCideraHighDesc(String cideraHighDesc) {
		this.cideraHighDesc = cideraHighDesc;
	}
	public String getPertanggunganDesc() {
		return pertanggunganDesc;
	}
	public void setPertanggunganDesc(String pertanggunganDesc) {
		this.pertanggunganDesc = pertanggunganDesc;
	}
	public String getCideraDesc() {
		return cideraDesc;
	}
	public void setCideraDesc(String cideraDesc) {
		this.cideraDesc = cideraDesc;
	}
	public String getJenisIdentitasDesc() {
		return jenisIdentitasDesc;
	}
	public void setJenisIdentitasDesc(String jenisIdentitasDesc) {
		this.jenisIdentitasDesc = jenisIdentitasDesc;
	}
	public String getPenjaminDesc() {
		return penjaminDesc;
	}
	public void setPenjaminDesc(String penjaminDesc) {
		this.penjaminDesc = penjaminDesc;
	}
	public String getPosisiDesc() {
		return posisiDesc;
	}
	public void setPosisiDesc(String posisiDesc) {
		this.posisiDesc = posisiDesc;
	}
	public String getStatusNikahDesc() {
		return statusNikahDesc;
	}
	public void setStatusNikahDesc(String statusNikahDesc) {
		this.statusNikahDesc = statusNikahDesc;
	}
	public String getKodeProvinsi() {
		return kodeProvinsi;
	}
	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}
	public String getNamaProvinsi() {
		return namaProvinsi;
	}
	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}
	public String getKodeKabkota() {
		return kodeKabkota;
	}
	public void setKodeKabkota(String kodeKabkota) {
		this.kodeKabkota = kodeKabkota;
	}
	public String getNamaKabkota() {
		return namaKabkota;
	}
	public void setNamaKabkota(String namaKabkota) {
		this.namaKabkota = namaKabkota;
	}
	public String getKodeCamat() {
		return kodeCamat;
	}
	public void setKodeCamat(String kodeCamat) {
		this.kodeCamat = kodeCamat;
	}
	public String getNamaCamat() {
		return namaCamat;
	}
	public void setNamaCamat(String namaCamat) {
		this.namaCamat = namaCamat;
	}
	public String getNamaJkUmur() {
		return namaJkUmur;
	}
	public void setNamaJkUmur(String namaJkUmur) {
		this.namaJkUmur = namaJkUmur;
	}
	public String getAngkutanKecelakaanDesc() {
		return angkutanKecelakaanDesc;
	}
	public void setAngkutanKecelakaanDesc(String angkutanKecelakaanDesc) {
		this.angkutanKecelakaanDesc = angkutanKecelakaanDesc;
	}
	public String getAngkutanPenanggungKecelakaanDesc() {
		return angkutanPenanggungKecelakaanDesc;
	}
	public void setAngkutanPenanggungKecelakaanDesc(
			String angkutanPenanggungKecelakaanDesc) {
		this.angkutanPenanggungKecelakaanDesc = angkutanPenanggungKecelakaanDesc;
	}
	public String getJaminanDesc() {
		return jaminanDesc;
	}
	public void setJaminanDesc(String jaminanDesc) {
		this.jaminanDesc = jaminanDesc;
	}
	public String getNamaCidera() {
		return namaCidera;
	}
	public void setNamaCidera(String namaCidera) {
		this.namaCidera = namaCidera;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((idKorbanKecelakaan == null) ? 0 : idKorbanKecelakaan
						.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PlKorbanKecelakaanDto other = (PlKorbanKecelakaanDto) obj;
		if (idKorbanKecelakaan == null) {
			if (other.idKorbanKecelakaan != null)
				return false;
		} else if (!idKorbanKecelakaan.equals(other.idKorbanKecelakaan))
			return false;
		return true;
	}
	public String getNoLaporanPolisi() {
		return noLaporanPolisi;
	}
	public void setNoLaporanPolisi(String noLaporanPolisi) {
		this.noLaporanPolisi = noLaporanPolisi;
	}
	public Date getTglKejadian() {
		return tglKejadian;
	}
	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}
	public Date getTglLaporanPolisi() {
		return tglLaporanPolisi;
	}
	public void setTglLaporanPolisi(Date tglLaporanPolisi) {
		this.tglLaporanPolisi = tglLaporanPolisi;
	}
	public String getDeskripsiJaminan() {
		return deskripsiJaminan;
	}
	public void setDeskripsiJaminan(String deskripsiJaminan) {
		this.deskripsiJaminan = deskripsiJaminan;
	}
	public String getStatusLaporanPolisi() {
		return statusLaporanPolisi;
	}
	public void setStatusLaporanPolisi(String statusLaporanPolisi) {
		this.statusLaporanPolisi = statusLaporanPolisi;
	}
	public String getKodeInstansi() {
		return kodeInstansi;
	}
	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}
	public String getInstansiDesc() {
		return instansiDesc;
	}
	public void setInstansiDesc(String instansiDesc) {
		this.instansiDesc = instansiDesc;
	}
	
	
}
