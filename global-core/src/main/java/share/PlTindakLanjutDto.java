package share;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class PlTindakLanjutDto {
	
	private String alasanNoclaim;
	private String catatanTindakLanjut;
	private String createdBy;
	private Date createdDate;
	private String dilimpahkanKe;
	private String idTlRegister;
	private BigDecimal jmlKlaim;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String noRegister;
	private String noSuratJaminan;
	private String noSuratPanggilan;
	private String petugasSurvey;
	private Date tglTindakLanjut;
	private String tindakLanjutFlag;
	private String deskripsiTL;
	
	private int noUrut;
	private boolean editButtonVisible;
	
	//GL RS
	private String idJaminanTlRs;
	private String createdByTlRs;
	private Date createdDateTlRs;
	private String flagBayarTlRs;
	private String flagBayarAwalTlRs;
	private String flagLanjutanTlRs;
	private String flagLimpahanTlRs;
	private String jenisTagihanTlRs;
	private BigDecimal jmlAmblTlRs;
	private BigDecimal jmlAmblSementaraTlRs;
	private BigDecimal jmlAmblSmtTlRs;
	private BigDecimal jmlP3kTlRs;
	private BigDecimal jmlP3kSementaraTlRs;
	private BigDecimal jmlP3kSmtTlRs;
	private BigDecimal jmlPengajuanSementaraTlRs;
	private BigDecimal jumlahPengajuan1TlRs;
	private BigDecimal jumlahPengajuanSmtTlRs;
	private String kodeKantorAsalLimpTlRs;
	private String kodeKantorJrTlRs;
	private String kodeRsSementaraTlRs;
	private String kodeRumahSakitTlRs;
	private String lastUpdatedByTlRs;
	private Date lastUpdatedDateTlRs;
	private String noBerkasTlRs;
	private String noRegisterTlRs;
	private String noSuratJaminanTlRs;
	private String otorisasiAjuSmtTlRs;
	private String otorisasiAwalTlRs;
	private Date tglMasukRsTlRs;
	private Date tglSuratJaminanTlRs;
	private String keteranganOtorisasiTlRs;

	
	public String getAlasanNoclaim() {
		return alasanNoclaim;
	}
	public void setAlasanNoclaim(String alasanNoclaim) {
		this.alasanNoclaim = alasanNoclaim;
	}
	public String getCatatanTindakLanjut() {
		return catatanTindakLanjut;
	}
	public void setCatatanTindakLanjut(String catatanTindakLanjut) {
		this.catatanTindakLanjut = catatanTindakLanjut;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getDilimpahkanKe() {
		return dilimpahkanKe;
	}
	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}
	public String getIdTlRegister() {
		return idTlRegister;
	}
	public void setIdTlRegister(String idTlRegister) {
		this.idTlRegister = idTlRegister;
	}
	public BigDecimal getJmlKlaim() {
		return jmlKlaim;
	}
	public void setJmlKlaim(BigDecimal jmlKlaim) {
		this.jmlKlaim = jmlKlaim;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNoRegister() {
		return noRegister;
	}
	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}
	public String getNoSuratJaminan() {
		return noSuratJaminan;
	}
	public void setNoSuratJaminan(String noSuratJaminan) {
		this.noSuratJaminan = noSuratJaminan;
	}
	public String getNoSuratPanggilan() {
		return noSuratPanggilan;
	}
	public void setNoSuratPanggilan(String noSuratPanggilan) {
		this.noSuratPanggilan = noSuratPanggilan;
	}
	public String getPetugasSurvey() {
		return petugasSurvey;
	}
	public void setPetugasSurvey(String petugasSurvey) {
		this.petugasSurvey = petugasSurvey;
	}
	public Date getTglTindakLanjut() {
		return tglTindakLanjut;
	}
	public void setTglTindakLanjut(Date tglTindakLanjut) {
		this.tglTindakLanjut = tglTindakLanjut;
	}
	public String getTindakLanjutFlag() {
		return tindakLanjutFlag;
	}
	public void setTindakLanjutFlag(String tindakLanjutFlag) {
		this.tindakLanjutFlag = tindakLanjutFlag;
	}
	public int getNoUrut() {
		return noUrut;
	}
	public void setNoUrut(int noUrut) {
		this.noUrut = noUrut;
	}
	public String getDeskripsiTL() {
		return deskripsiTL;
	}
	public void setDeskripsiTL(String deskripsiTL) {
		this.deskripsiTL = deskripsiTL;
	}
	public boolean isEditButtonVisible() {
		return editButtonVisible;
	}
	public void setEditButtonVisible(boolean editButtonVisible) {
		this.editButtonVisible = editButtonVisible;
	}
	public String getIdJaminanTlRs() {
		return idJaminanTlRs;
	}
	public void setIdJaminanTlRs(String idJaminanTlRs) {
		this.idJaminanTlRs = idJaminanTlRs;
	}
	public String getCreatedByTlRs() {
		return createdByTlRs;
	}
	public void setCreatedByTlRs(String createdByTlRs) {
		this.createdByTlRs = createdByTlRs;
	}
	public Date getCreatedDateTlRs() {
		return createdDateTlRs;
	}
	public void setCreatedDateTlRs(Date createdDateTlRs) {
		this.createdDateTlRs = createdDateTlRs;
	}
	public String getFlagBayarTlRs() {
		return flagBayarTlRs;
	}
	public void setFlagBayarTlRs(String flagBayarTlRs) {
		this.flagBayarTlRs = flagBayarTlRs;
	}
	public String getFlagBayarAwalTlRs() {
		return flagBayarAwalTlRs;
	}
	public void setFlagBayarAwalTlRs(String flagBayarAwalTlRs) {
		this.flagBayarAwalTlRs = flagBayarAwalTlRs;
	}
	public String getFlagLanjutanTlRs() {
		return flagLanjutanTlRs;
	}
	public void setFlagLanjutanTlRs(String flagLanjutanTlRs) {
		this.flagLanjutanTlRs = flagLanjutanTlRs;
	}
	public String getFlagLimpahanTlRs() {
		return flagLimpahanTlRs;
	}
	public void setFlagLimpahanTlRs(String flagLimpahanTlRs) {
		this.flagLimpahanTlRs = flagLimpahanTlRs;
	}
	public String getJenisTagihanTlRs() {
		return jenisTagihanTlRs;
	}
	public void setJenisTagihanTlRs(String jenisTagihanTlRs) {
		this.jenisTagihanTlRs = jenisTagihanTlRs;
	}
	public BigDecimal getJmlAmblTlRs() {
		return jmlAmblTlRs;
	}
	public void setJmlAmblTlRs(BigDecimal jmlAmblTlRs) {
		this.jmlAmblTlRs = jmlAmblTlRs;
	}
	public BigDecimal getJmlAmblSementaraTlRs() {
		return jmlAmblSementaraTlRs;
	}
	public void setJmlAmblSementaraTlRs(BigDecimal jmlAmblSementaraTlRs) {
		this.jmlAmblSementaraTlRs = jmlAmblSementaraTlRs;
	}
	public BigDecimal getJmlAmblSmtTlRs() {
		return jmlAmblSmtTlRs;
	}
	public void setJmlAmblSmtTlRs(BigDecimal jmlAmblSmtTlRs) {
		this.jmlAmblSmtTlRs = jmlAmblSmtTlRs;
	}
	public BigDecimal getJmlP3kTlRs() {
		return jmlP3kTlRs;
	}
	public void setJmlP3kTlRs(BigDecimal jmlP3kTlRs) {
		this.jmlP3kTlRs = jmlP3kTlRs;
	}
	public BigDecimal getJmlP3kSementaraTlRs() {
		return jmlP3kSementaraTlRs;
	}
	public void setJmlP3kSementaraTlRs(BigDecimal jmlP3kSementaraTlRs) {
		this.jmlP3kSementaraTlRs = jmlP3kSementaraTlRs;
	}
	public BigDecimal getJmlP3kSmtTlRs() {
		return jmlP3kSmtTlRs;
	}
	public void setJmlP3kSmtTlRs(BigDecimal jmlP3kSmtTlRs) {
		this.jmlP3kSmtTlRs = jmlP3kSmtTlRs;
	}
	public BigDecimal getJmlPengajuanSementaraTlRs() {
		return jmlPengajuanSementaraTlRs;
	}
	public void setJmlPengajuanSementaraTlRs(BigDecimal jmlPengajuanSementaraTlRs) {
		this.jmlPengajuanSementaraTlRs = jmlPengajuanSementaraTlRs;
	}
	public BigDecimal getJumlahPengajuan1TlRs() {
		return jumlahPengajuan1TlRs;
	}
	public void setJumlahPengajuan1TlRs(BigDecimal jumlahPengajuan1TlRs) {
		this.jumlahPengajuan1TlRs = jumlahPengajuan1TlRs;
	}
	public BigDecimal getJumlahPengajuanSmtTlRs() {
		return jumlahPengajuanSmtTlRs;
	}
	public void setJumlahPengajuanSmtTlRs(BigDecimal jumlahPengajuanSmtTlRs) {
		this.jumlahPengajuanSmtTlRs = jumlahPengajuanSmtTlRs;
	}
	public String getKodeKantorAsalLimpTlRs() {
		return kodeKantorAsalLimpTlRs;
	}
	public void setKodeKantorAsalLimpTlRs(String kodeKantorAsalLimpTlRs) {
		this.kodeKantorAsalLimpTlRs = kodeKantorAsalLimpTlRs;
	}
	public String getKodeKantorJrTlRs() {
		return kodeKantorJrTlRs;
	}
	public void setKodeKantorJrTlRs(String kodeKantorJrTlRs) {
		this.kodeKantorJrTlRs = kodeKantorJrTlRs;
	}
	public String getKodeRsSementaraTlRs() {
		return kodeRsSementaraTlRs;
	}
	public void setKodeRsSementaraTlRs(String kodeRsSementaraTlRs) {
		this.kodeRsSementaraTlRs = kodeRsSementaraTlRs;
	}
	public String getKodeRumahSakitTlRs() {
		return kodeRumahSakitTlRs;
	}
	public void setKodeRumahSakitTlRs(String kodeRumahSakitTlRs) {
		this.kodeRumahSakitTlRs = kodeRumahSakitTlRs;
	}
	public String getLastUpdatedByTlRs() {
		return lastUpdatedByTlRs;
	}
	public void setLastUpdatedByTlRs(String lastUpdatedByTlRs) {
		this.lastUpdatedByTlRs = lastUpdatedByTlRs;
	}
	public Date getLastUpdatedDateTlRs() {
		return lastUpdatedDateTlRs;
	}
	public void setLastUpdatedDateTlRs(Date lastUpdatedDateTlRs) {
		this.lastUpdatedDateTlRs = lastUpdatedDateTlRs;
	}
	public String getNoBerkasTlRs() {
		return noBerkasTlRs;
	}
	public void setNoBerkasTlRs(String noBerkasTlRs) {
		this.noBerkasTlRs = noBerkasTlRs;
	}
	public String getNoRegisterTlRs() {
		return noRegisterTlRs;
	}
	public void setNoRegisterTlRs(String noRegisterTlRs) {
		this.noRegisterTlRs = noRegisterTlRs;
	}
	public String getNoSuratJaminanTlRs() {
		return noSuratJaminanTlRs;
	}
	public void setNoSuratJaminanTlRs(String noSuratJaminanTlRs) {
		this.noSuratJaminanTlRs = noSuratJaminanTlRs;
	}
	public String getOtorisasiAjuSmtTlRs() {
		return otorisasiAjuSmtTlRs;
	}
	public void setOtorisasiAjuSmtTlRs(String otorisasiAjuSmtTlRs) {
		this.otorisasiAjuSmtTlRs = otorisasiAjuSmtTlRs;
	}
	public String getOtorisasiAwalTlRs() {
		return otorisasiAwalTlRs;
	}
	public void setOtorisasiAwalTlRs(String otorisasiAwalTlRs) {
		this.otorisasiAwalTlRs = otorisasiAwalTlRs;
	}
	public Date getTglMasukRsTlRs() {
		return tglMasukRsTlRs;
	}
	public void setTglMasukRsTlRs(Date tglMasukRsTlRs) {
		this.tglMasukRsTlRs = tglMasukRsTlRs;
	}
	public Date getTglSuratJaminanTlRs() {
		return tglSuratJaminanTlRs;
	}
	public void setTglSuratJaminanTlRs(Date tglSuratJaminanTlRs) {
		this.tglSuratJaminanTlRs = tglSuratJaminanTlRs;
	}
	public String getKeteranganOtorisasiTlRs() {
		return keteranganOtorisasiTlRs;
	}
	public void setKeteranganOtorisasiTlRs(String keteranganOtorisasiTlRs) {
		this.keteranganOtorisasiTlRs = keteranganOtorisasiTlRs;
	}
}
