package share;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DukcapilWinDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nik;
	private String agama;
	private String alamat;
	private String alamatDusun;
	private String alamatKodepos;
	private String alamatRt;
	private String alamatRw;
	private Date ektpCreated;
	private String ektpLocalid;
	private String ektpStatus;
	private String golDarah;
	private String jnsKelamin;
	private String kodeKabupaten;
	private String kodeKecamatan;
	private String kodeKelurahan;
	private String kodePekerjaan;
	private String kodePropinsi;
	private Date lastSyncDate;
	private String namaAyah;
	private String namaIbu;
	private String namaLengkap;
	private String noKk;
	private String pendAkhir;
	private String statusNikah;
	private Date tglLahir;
	private Date tglNikah;
	private String tmpLahir;
	private String namaProvinsi;
	private String namaKabupaten;
	private String namaKecamatan;
	private String namaKelurahan;
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	public DukcapilWinDto(){}
	
	
	public DukcapilWinDto(KKDukcapilDto a){
		this.setAgama(a.getaGAMA());
		this.setAlamat(a.getaLAMAT());
		this.setAlamatDusun(null);
		this.setAlamatKodepos(null);
		this.setAlamatRt(a.getnORT());
		this.setAlamatRw(a.getnORW());
		this.setGolDarah(a.getgOLDARAH());
		this.setJnsKelamin(a.getjENISKLMIN());
		this.setKodeKabupaten(a.getnOKAB());
		this.setKodeKecamatan(a.getnOKEC());
		this.setKodeKelurahan(a.getnOKEL());
		this.setKodePropinsi(a.getnOPROP());
		this.setKodePekerjaan(null);
		this.setNamaAyah(a.getnAMALGKPAYAH());
		this.setNamaIbu(a.getnAMALGKPIBU());
		this.setNamaLengkap(a.getnAMALGKP());
		this.setNik(a.getnIK());
		this.setNoKk(a.getnOKK());
		this.setPendAkhir(a.getpDDKAKH());
		this.setStatusNikah(a.getsTATUSKAWIN());
		try {
			this.setTglLahir(sdf.parse(a.gettGLLHR()));
		} catch (ParseException e) {
			this.setTglLahir(null);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.setTmpLahir(a.gettMPTLHR());
		this.setNamaKabupaten(a.getkABNAME());
		this.setNamaKecamatan(a.getkECNAME());
		this.setNamaKelurahan(a.getkELNAME());
		this.setNamaProvinsi(a.getpROPNAME());
	}
	
	public DukcapilWinDto(String nik, String agama, String alamat,
			String alamatDusun, String alamatKodepos, String alamatRt,
			String alamatRw, Date ektpCreated, String ektpLocalid,
			String ektpStatus, String golDarah, String jnsKelamin,
			String kodeKabupaten, String kodeKecamatan, String kodeKelurahan,
			String kodePekerjaan, String kodePropinsi, Date lastSyncDate,
			String namaAyah, String namaIbu, String namaLengkap, String noKk,
			String pendAkhir, String statusNikah, Date tglLahir, Date tglNikah,
			String tmpLahir) {
		super();
		this.nik = nik;
		this.agama = agama;
		this.alamat = alamat;
		this.alamatDusun = alamatDusun;
		this.alamatKodepos = alamatKodepos;
		this.alamatRt = alamatRt;
		this.alamatRw = alamatRw;
		this.ektpCreated = ektpCreated;
		this.ektpLocalid = ektpLocalid;
		this.ektpStatus = ektpStatus;
		this.golDarah = golDarah;
		this.jnsKelamin = jnsKelamin;
		this.kodeKabupaten = kodeKabupaten;
		this.kodeKecamatan = kodeKecamatan;
		this.kodeKelurahan = kodeKelurahan;
		this.kodePekerjaan = kodePekerjaan;
		this.kodePropinsi = kodePropinsi;
		this.lastSyncDate = lastSyncDate;
		this.namaAyah = namaAyah;
		this.namaIbu = namaIbu;
		this.namaLengkap = namaLengkap;
		this.noKk = noKk;
		this.pendAkhir = pendAkhir;
		this.statusNikah = statusNikah;
		this.tglLahir = tglLahir;
		this.tglNikah = tglNikah;
		this.tmpLahir = tmpLahir;
	}

	

	public String getNik() {
		return nik;
	}
	public void setNik(String nik) {
		this.nik = nik;
	}
	public String getAgama() {
		return agama;
	}
	public void setAgama(String agama) {
		this.agama = agama;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getAlamatDusun() {
		return alamatDusun;
	}
	public void setAlamatDusun(String alamatDusun) {
		this.alamatDusun = alamatDusun;
	}
	public String getAlamatKodepos() {
		return alamatKodepos;
	}
	public void setAlamatKodepos(String alamatKodepos) {
		this.alamatKodepos = alamatKodepos;
	}
	public String getAlamatRt() {
		return alamatRt;
	}
	public void setAlamatRt(String alamatRt) {
		this.alamatRt = alamatRt;
	}
	public String getAlamatRw() {
		return alamatRw;
	}
	public void setAlamatRw(String alamatRw) {
		this.alamatRw = alamatRw;
	}
	public Date getEktpCreated() {
		return ektpCreated;
	}
	public void setEktpCreated(Date ektpCreated) {
		this.ektpCreated = ektpCreated;
	}
	public String getEktpLocalid() {
		return ektpLocalid;
	}
	public void setEktpLocalid(String ektpLocalid) {
		this.ektpLocalid = ektpLocalid;
	}
	public String getEktpStatus() {
		return ektpStatus;
	}
	public void setEktpStatus(String ektpStatus) {
		this.ektpStatus = ektpStatus;
	}
	public String getGolDarah() {
		return golDarah;
	}
	public void setGolDarah(String golDarah) {
		this.golDarah = golDarah;
	}
	public String getJnsKelamin() {
		return jnsKelamin;
	}
	public void setJnsKelamin(String jnsKelamin) {
		this.jnsKelamin = jnsKelamin;
	}
	public String getKodeKabupaten() {
		return kodeKabupaten;
	}
	public void setKodeKabupaten(String kodeKabupaten) {
		this.kodeKabupaten = kodeKabupaten;
	}
	public String getKodeKecamatan() {
		return kodeKecamatan;
	}
	public void setKodeKecamatan(String kodeKecamatan) {
		this.kodeKecamatan = kodeKecamatan;
	}
	public String getKodeKelurahan() {
		return kodeKelurahan;
	}
	public void setKodeKelurahan(String kodeKelurahan) {
		this.kodeKelurahan = kodeKelurahan;
	}
	public String getKodePekerjaan() {
		return kodePekerjaan;
	}
	public void setKodePekerjaan(String kodePekerjaan) {
		this.kodePekerjaan = kodePekerjaan;
	}
	public String getKodePropinsi() {
		return kodePropinsi;
	}
	public void setKodePropinsi(String kodePropinsi) {
		this.kodePropinsi = kodePropinsi;
	}
	public Date getLastSyncDate() {
		return lastSyncDate;
	}
	public void setLastSyncDate(Date lastSyncDate) {
		this.lastSyncDate = lastSyncDate;
	}
	public String getNamaAyah() {
		return namaAyah;
	}
	public void setNamaAyah(String namaAyah) {
		this.namaAyah = namaAyah;
	}
	public String getNamaIbu() {
		return namaIbu;
	}
	public void setNamaIbu(String namaIbu) {
		this.namaIbu = namaIbu;
	}
	public String getNamaLengkap() {
		return namaLengkap;
	}
	public void setNamaLengkap(String namaLengkap) {
		this.namaLengkap = namaLengkap;
	}
	public String getNoKk() {
		return noKk;
	}
	public void setNoKk(String noKk) {
		this.noKk = noKk;
	}
	public String getPendAkhir() {
		return pendAkhir;
	}
	public void setPendAkhir(String pendAkhir) {
		this.pendAkhir = pendAkhir;
	}
	public String getStatusNikah() {
		return statusNikah;
	}
	public void setStatusNikah(String statusNikah) {
		this.statusNikah = statusNikah;
	}
	public Date getTglLahir() {
		return tglLahir;
	}
	public void setTglLahir(Date tglLahir) {
		this.tglLahir = tglLahir;
	}
	public Date getTglNikah() {
		return tglNikah;
	}
	public void setTglNikah(Date tglNikah) {
		this.tglNikah = tglNikah;
	}
	public String getTmpLahir() {
		return tmpLahir;
	}
	public void setTmpLahir(String tmpLahir) {
		this.tmpLahir = tmpLahir;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nik == null) ? 0 : nik.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DukcapilWinDto other = (DukcapilWinDto) obj;
		if (nik == null) {
			if (other.nik != null)
				return false;
		} else if (!nik.equals(other.nik))
			return false;
		return true;
	}

	public String getNamaProvinsi() {
		return namaProvinsi;
	}

	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}

	public String getNamaKabupaten() {
		return namaKabupaten;
	}

	public void setNamaKabupaten(String namaKabupaten) {
		this.namaKabupaten = namaKabupaten;
	}

	public String getNamaKecamatan() {
		return namaKecamatan;
	}

	public void setNamaKecamatan(String namaKecamatan) {
		this.namaKecamatan = namaKecamatan;
	}

	public String getNamaKelurahan() {
		return namaKelurahan;
	}

	public void setNamaKelurahan(String namaKelurahan) {
		this.namaKelurahan = namaKelurahan;
	}
	
}
