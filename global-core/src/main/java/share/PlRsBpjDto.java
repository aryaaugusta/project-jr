package share;

public class PlRsBpjDto {

	private String kabKota;
	private String kodeKantorJr;
	private String kodeRsBpjs;
	private String namaRs;
	private String provinsi;
	
	public String getKabKota() {
		return kabKota;
	}
	public void setKabKota(String kabKota) {
		this.kabKota = kabKota;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getKodeRsBpjs() {
		return kodeRsBpjs;
	}
	public void setKodeRsBpjs(String kodeRsBpjs) {
		this.kodeRsBpjs = kodeRsBpjs;
	}
	public String getNamaRs() {
		return namaRs;
	}
	public void setNamaRs(String namaRs) {
		this.namaRs = namaRs;
	}
	public String getProvinsi() {
		return provinsi;
	}
	public void setProvinsi(String provinsi) {
		this.provinsi = provinsi;
	}
	
}
