package share;

import java.math.BigDecimal;
import java.util.Date;

public class PlTlRDto {

	private String idJaminan;
	private String createdBy;
	private Date createdDate;
	private String flagBayar;
	private String flagBayarAwal;
	private String flagLanjutan;
	private String flagLimpahan;
	private String jenisTagihan;
	private BigDecimal jmlAmbl;
	private BigDecimal jmlAmblSementara;
	private BigDecimal jmlAmblSmt;
	private BigDecimal jmlP3k;
	private BigDecimal jmlP3kSementara;
	private BigDecimal jmlP3kSmt;
	private BigDecimal jmlPengajuanSementara;
	private BigDecimal jumlahPengajuan1;
	private BigDecimal jumlahPengajuanSmt;
	private String kodeKantorAsalLimp;
	private String kodeKantorJr;
	private String kodeRsSementara;
	private String kodeRumahSakit;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String noBerkas;
	private String noRegister;
	private String noSuratJaminan;
	private String otorisasiAjuSmt;
	private String otorisasiAwal;
	private Date tglMasukRs;
	private Date tglSuratJaminan;
	private String keteranganOtorisasi;

	
	private BigDecimal jmlJaminan;
	private Long countJaminan;
	private Double jml;
	
	private String namaRs;
	private Date tglLaka;
	private String namaKorban;
	private BigDecimal jmlPengajuan;
	private BigDecimal jmlDigunakan;
	private String statusJaminan;
	
	private String idKorbanKecelakaan;
	private String idLaka;
	
	private String asalBerkasDesc;
	private String lokasiPengajuanDesc;
	private Date tglRegister;
	
	private boolean cetakVisible;
	private boolean otorisasiVisible;
	private String jaminanOption;
	private boolean jaminanVisible;
	
	public String getIdJaminan() {
		return idJaminan;
	}
	public void setIdJaminan(String idJaminan) {
		this.idJaminan = idJaminan;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getFlagBayar() {
		return flagBayar;
	}
	public void setFlagBayar(String flagBayar) {
		this.flagBayar = flagBayar;
	}
	public String getFlagBayarAwal() {
		return flagBayarAwal;
	}
	public void setFlagBayarAwal(String flagBayarAwal) {
		this.flagBayarAwal = flagBayarAwal;
	}
	public String getFlagLanjutan() {
		return flagLanjutan;
	}
	public void setFlagLanjutan(String flagLanjutan) {
		this.flagLanjutan = flagLanjutan;
	}
	public String getFlagLimpahan() {
		return flagLimpahan;
	}
	public void setFlagLimpahan(String flagLimpahan) {
		this.flagLimpahan = flagLimpahan;
	}
	public String getJenisTagihan() {
		return jenisTagihan;
	}
	public void setJenisTagihan(String jenisTagihan) {
		this.jenisTagihan = jenisTagihan;
	}
	public BigDecimal getJmlAmbl() {
		return jmlAmbl;
	}
	public void setJmlAmbl(BigDecimal jmlAmbl) {
		this.jmlAmbl = jmlAmbl;
	}
	public BigDecimal getJmlAmblSementara() {
		return jmlAmblSementara;
	}
	public void setJmlAmblSementara(BigDecimal jmlAmblSementara) {
		this.jmlAmblSementara = jmlAmblSementara;
	}
	public BigDecimal getJmlAmblSmt() {
		return jmlAmblSmt;
	}
	public void setJmlAmblSmt(BigDecimal jmlAmblSmt) {
		this.jmlAmblSmt = jmlAmblSmt;
	}
	public BigDecimal getJmlP3k() {
		return jmlP3k;
	}
	public void setJmlP3k(BigDecimal jmlP3k) {
		this.jmlP3k = jmlP3k;
	}
	public BigDecimal getJmlP3kSementara() {
		return jmlP3kSementara;
	}
	public void setJmlP3kSementara(BigDecimal jmlP3kSementara) {
		this.jmlP3kSementara = jmlP3kSementara;
	}
	public BigDecimal getJmlP3kSmt() {
		return jmlP3kSmt;
	}
	public void setJmlP3kSmt(BigDecimal jmlP3kSmt) {
		this.jmlP3kSmt = jmlP3kSmt;
	}
	public BigDecimal getJmlPengajuanSementara() {
		return jmlPengajuanSementara;
	}
	public void setJmlPengajuanSementara(BigDecimal jmlPengajuanSementara) {
		this.jmlPengajuanSementara = jmlPengajuanSementara;
	}
	public BigDecimal getJumlahPengajuan1() {
		return jumlahPengajuan1;
	}
	public void setJumlahPengajuan1(BigDecimal jumlahPengajuan1) {
		this.jumlahPengajuan1 = jumlahPengajuan1;
	}
	public BigDecimal getJumlahPengajuanSmt() {
		return jumlahPengajuanSmt;
	}
	public void setJumlahPengajuanSmt(BigDecimal jumlahPengajuanSmt) {
		this.jumlahPengajuanSmt = jumlahPengajuanSmt;
	}
	public String getKodeKantorAsalLimp() {
		return kodeKantorAsalLimp;
	}
	public void setKodeKantorAsalLimp(String kodeKantorAsalLimp) {
		this.kodeKantorAsalLimp = kodeKantorAsalLimp;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getKodeRsSementara() {
		return kodeRsSementara;
	}
	public void setKodeRsSementara(String kodeRsSementara) {
		this.kodeRsSementara = kodeRsSementara;
	}
	public String getKodeRumahSakit() {
		return kodeRumahSakit;
	}
	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getNoRegister() {
		return noRegister;
	}
	public void setNoRegister(String noRegister) {
		this.noRegister = noRegister;
	}
	public String getNoSuratJaminan() {
		return noSuratJaminan;
	}
	public void setNoSuratJaminan(String noSuratJaminan) {
		this.noSuratJaminan = noSuratJaminan;
	}
	public String getOtorisasiAjuSmt() {
		return otorisasiAjuSmt;
	}
	public void setOtorisasiAjuSmt(String otorisasiAjuSmt) {
		this.otorisasiAjuSmt = otorisasiAjuSmt;
	}
	public String getOtorisasiAwal() {
		return otorisasiAwal;
	}
	public void setOtorisasiAwal(String otorisasiAwal) {
		this.otorisasiAwal = otorisasiAwal;
	}
	public Date getTglMasukRs() {
		return tglMasukRs;
	}
	public void setTglMasukRs(Date tglMasukRs) {
		this.tglMasukRs = tglMasukRs;
	}
	public Date getTglSuratJaminan() {
		return tglSuratJaminan;
	}
	public void setTglSuratJaminan(Date tglSuratJaminan) {
		this.tglSuratJaminan = tglSuratJaminan;
	}
	public BigDecimal getJmlJaminan() {
		return jmlJaminan;
	}
	public void setJmlJaminan(BigDecimal jmlJaminan) {
		this.jmlJaminan = jmlJaminan;
	}
	public Long getCountJaminan() {
		return countJaminan;
	}
	public void setCountJaminan(Long countJaminan) {
		this.countJaminan = countJaminan;
	}
	public Double getJml() {
		return jml;
	}
	public void setJml(Double jml) {
		this.jml = jml;
	}
	public String getKeteranganOtorisasi() {
		return keteranganOtorisasi;
	}
	public void setKeteranganOtorisasi(String keteranganOtorisasi) {
		this.keteranganOtorisasi = keteranganOtorisasi;
	}
	public String getNamaRs() {
		return namaRs;
	}
	public void setNamaRs(String namaRs) {
		this.namaRs = namaRs;
	}
	public Date getTglLaka() {
		return tglLaka;
	}
	public void setTglLaka(Date tglLaka) {
		this.tglLaka = tglLaka;
	}
	public String getNamaKorban() {
		return namaKorban;
	}
	public void setNamaKorban(String namaKorban) {
		this.namaKorban = namaKorban;
	}
	public BigDecimal getJmlPengajuan() {
		return jmlPengajuan;
	}
	public void setJmlPengajuan(BigDecimal jmlPengajuan) {
		this.jmlPengajuan = jmlPengajuan;
	}
	public BigDecimal getJmlDigunakan() {
		return jmlDigunakan;
	}
	public void setJmlDigunakan(BigDecimal jmlDigunakan) {
		this.jmlDigunakan = jmlDigunakan;
	}
	public String getStatusJaminan() {
		return statusJaminan;
	}
	public void setStatusJaminan(String statusJaminan) {
		this.statusJaminan = statusJaminan;
	}
	public String getIdLaka() {
		return idLaka;
	}
	public void setIdLaka(String idLaka) {
		this.idLaka = idLaka;
	}
	public String getIdKorbanKecelakaan() {
		return idKorbanKecelakaan;
	}
	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}
	public String getAsalBerkasDesc() {
		return asalBerkasDesc;
	}
	public void setAsalBerkasDesc(String asalBerkasDesc) {
		this.asalBerkasDesc = asalBerkasDesc;
	}
	public String getLokasiPengajuanDesc() {
		return lokasiPengajuanDesc;
	}
	public void setLokasiPengajuanDesc(String lokasiPengajuanDesc) {
		this.lokasiPengajuanDesc = lokasiPengajuanDesc;
	}
	public Date getTglRegister() {
		return tglRegister;
	}
	public void setTglRegister(Date tglRegister) {
		this.tglRegister = tglRegister;
	}
	public boolean isCetakVisible() {
		return cetakVisible;
	}
	public void setCetakVisible(boolean cetakVisible) {
		this.cetakVisible = cetakVisible;
	}
	public String getJaminanOption() {
		return jaminanOption;
	}
	public void setJaminanOption(String jaminanOption) {
		this.jaminanOption = jaminanOption;
	}
	public boolean isJaminanVisible() {
		return jaminanVisible;
	}
	public void setJaminanVisible(boolean jaminanVisible) {
		this.jaminanVisible = jaminanVisible;
	}
	public boolean isOtorisasiVisible() {
		return otorisasiVisible;
	}
	public void setOtorisasiVisible(boolean otorisasiVisible) {
		this.otorisasiVisible = otorisasiVisible;
	}
}
