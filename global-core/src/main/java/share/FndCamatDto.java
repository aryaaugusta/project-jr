package share;

import java.io.Serializable;
import java.util.Date;

public class FndCamatDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String catatan;
	private String flagEnable;
	private String kodeCamat;
	private String kodeKabkota;
	private String kodeProvinsi;
	private String namaCamat;
	private String namaKabkota;
	private String namaProvinsi;
	
	//added by Meilona
	private String kodeNamaProv;
	private String kodeNamaCamat;
	private String kodeNamaKabKota;
	
	private String createdBy;
	private Date createdDate;
	private String updatedBy;
	private Date updatedDate;
	private String mode;
	
	public String getCatatan() {
		return catatan;
	}
	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}
	public String getFlagEnable() {
		return flagEnable;
	}
	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}
	public String getKodeCamat() {
		return kodeCamat;
	}
	public void setKodeCamat(String kodeCamat) {
		this.kodeCamat = kodeCamat;
	}
	public String getKodeKabkota() {
		return kodeKabkota;
	}
	public void setKodeKabkota(String kodeKabkota) {
		this.kodeKabkota = kodeKabkota;
	}
	public String getKodeProvinsi() {
		return kodeProvinsi;
	}
	public void setKodeProvinsi(String kodeProvinsi) {
		this.kodeProvinsi = kodeProvinsi;
	}
	public String getNamaCamat() {
		return namaCamat;
	}
	public void setNamaCamat(String namaCamat) {
		this.namaCamat = namaCamat;
	}
	public String getNamaKabkota() {
		return namaKabkota;
	}
	public void setNamaKabkota(String namaKabkota) {
		this.namaKabkota = namaKabkota;
	}
	public String getNamaProvinsi() {
		return namaProvinsi;
	}
	public void setNamaProvinsi(String namaProvinsi) {
		this.namaProvinsi = namaProvinsi;
	}
	public String getKodeNamaProv() {
		return kodeNamaProv;
	}
	public void setKodeNamaProv(String kodeNamaProv) {
		this.kodeNamaProv = kodeNamaProv;
	}
	public String getKodeNamaCamat() {
		return kodeNamaCamat;
	}
	public void setKodeNamaCamat(String kodeNamaCamat) {
		this.kodeNamaCamat = kodeNamaCamat;
	}
	public String getKodeNamaKabKota() {
		return kodeNamaKabKota;
	}
	public void setKodeNamaKabKota(String kodeNamaKabKota) {
		this.kodeNamaKabKota = kodeNamaKabKota;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
}
