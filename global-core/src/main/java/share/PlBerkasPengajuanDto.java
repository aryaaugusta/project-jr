package share;

import java.io.Serializable;
import java.util.Date;

public class PlBerkasPengajuanDto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String noBerkas;
	private String kodeBerkas;
	private String absahFlag;
	private String createdBy;
	private Date creationDate;
	private String diterimaFlag;
	private String idGuid;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private Date tglTerimaBerkas;
	
	private String kodeBerkasDesc;
	
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getKodeBerkas() {
		return kodeBerkas;
	}
	public void setKodeBerkas(String kodeBerkas) {
		this.kodeBerkas = kodeBerkas;
	}
	public String getAbsahFlag() {
		return absahFlag;
	}
	public void setAbsahFlag(String absahFlag) {
		this.absahFlag = absahFlag;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getDiterimaFlag() {
		return diterimaFlag;
	}
	public void setDiterimaFlag(String diterimaFlag) {
		this.diterimaFlag = diterimaFlag;
	}
	public String getIdGuid() {
		return idGuid;
	}
	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public Date getTglTerimaBerkas() {
		return tglTerimaBerkas;
	}
	public void setTglTerimaBerkas(Date tglTerimaBerkas) {
		this.tglTerimaBerkas = tglTerimaBerkas;
	}
	public String getKodeBerkasDesc() {
		return kodeBerkasDesc;
	}
	public void setKodeBerkasDesc(String kodeBerkasDesc) {
		this.kodeBerkasDesc = kodeBerkasDesc;
	}
	
	
	

}
