package share;

import java.io.Serializable;
import java.util.Date;


public class PlRsResponseDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String createdBy;
	private Date createdDate;
	private String dilimpahkanKe;
	private String diprosesDi;
	private String idResponse;
	private String kodeKejadian;
	private String kodeKesimpulan;
	private String kodeRekomendasi;
	private String kodeRs;
	private String lastUpdateBy;
	private Date lastUpdateDate;
	private String petugas;
	private String uraian;
	
	
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getDilimpahkanKe() {
		return dilimpahkanKe;
	}
	public void setDilimpahkanKe(String dilimpahkanKe) {
		this.dilimpahkanKe = dilimpahkanKe;
	}
	public String getDiprosesDi() {
		return diprosesDi;
	}
	public void setDiprosesDi(String diprosesDi) {
		this.diprosesDi = diprosesDi;
	}
	public String getIdResponse() {
		return idResponse;
	}
	public void setIdResponse(String idResponse) {
		this.idResponse = idResponse;
	}
	public String getKodeKejadian() {
		return kodeKejadian;
	}
	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}
	public String getKodeKesimpulan() {
		return kodeKesimpulan;
	}
	public void setKodeKesimpulan(String kodeKesimpulan) {
		this.kodeKesimpulan = kodeKesimpulan;
	}
	public String getKodeRekomendasi() {
		return kodeRekomendasi;
	}
	public void setKodeRekomendasi(String kodeRekomendasi) {
		this.kodeRekomendasi = kodeRekomendasi;
	}
	public String getKodeRs() {
		return kodeRs;
	}
	public void setKodeRs(String kodeRs) {
		this.kodeRs = kodeRs;
	}
	public String getLastUpdateBy() {
		return lastUpdateBy;
	}
	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getPetugas() {
		return petugas;
	}
	public void setPetugas(String petugas) {
		this.petugas = petugas;
	}
	public String getUraian() {
		return uraian;
	}
	public void setUraian(String uraian) {
		this.uraian = uraian;
	}
	

}
