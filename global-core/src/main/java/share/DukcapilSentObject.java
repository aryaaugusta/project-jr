package share;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DukcapilSentObject implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("NIK")
	private String nik;
	@JsonProperty("IP_User")
	private String ipUser;
	@JsonProperty("user_id")
	private String userId;
	@JsonProperty("password")
	private String password;
	
	public DukcapilSentObject(){}
	
	public DukcapilSentObject(String nik, String ipUser, String userId,
			String password) {
		super();
		this.nik = nik;
		this.ipUser = ipUser;
		this.userId = userId;
		this.password = password;
	}
	public String getNik() {
		return nik;
	}

	public void setNik(String nik) {
		this.nik = nik;
	}

	public String getIpUser() {
		return ipUser;
	}

	public void setIpUser(String ipUser) {
		this.ipUser = ipUser;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	
}
