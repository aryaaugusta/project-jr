package share;

import java.io.Serializable;
import java.util.Date;

public class PlInstansiDto implements Serializable{
	 
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kodeInstansi;
	private String createdBy;
	private Date creationDate;
	private String deskripsi;
	private String flagEnable;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	
	private String namaPolda;
	private String kodePolda;
	private String idDistrict;
	private String wilayahKPJR;
	
	private String namaPolres;
	private int number;
	
	//added by luthfi
	private String kodeNama;
	private String kodeNamaInstansi;
	
	public String getKodeInstansi() {
		return kodeInstansi;
	}
	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getFlagEnable() {
		return flagEnable;
	}
	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNamaPolda() {
		return namaPolda;
	}
	public void setNamaPolda(String namaPolda) {
		this.namaPolda = namaPolda;
	}
	
	public String getWilayahKPJR() {
		return wilayahKPJR;
	}
	public void setWilayahKPJR(String wilayahKPJR) {
		this.wilayahKPJR = wilayahKPJR;
	}
	public String getKodePolda() {
		return kodePolda;
	}
	public void setKodePolda(String kodePolda) {
		this.kodePolda = kodePolda;
	}
	public String getIdDistrict() {
		return idDistrict;
	}
	public void setIdDistrict(String idDistrict) {
		this.idDistrict = idDistrict;
	}
	public String getNamaPolres() {
		return namaPolres;
	}
	public void setNamaPolres(String namaPolres) {
		this.namaPolres = namaPolres;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getKodeNama() {
		return kodeNama;
	}
	public void setKodeNama(String kodeNama) {
		this.kodeNama = kodeNama;
	}
	public String getKodeNamaInstansi() {
		return kodeNamaInstansi;
	}
	public void setKodeNamaInstansi(String kodeNamaInstansi) {
		this.kodeNamaInstansi = kodeNamaInstansi;
	}
	
}
