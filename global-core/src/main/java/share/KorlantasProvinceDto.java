package share;

import java.io.Serializable;
import java.math.BigDecimal;

public class KorlantasProvinceDto implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigDecimal archived;
	private String dasiId;
	private BigDecimal gpsLatitude;
	private BigDecimal gpsLongitude;
	private BigDecimal id;
	private String name;
	private String provinceNumber;
	private BigDecimal sortOrder;
	public BigDecimal getArchived() {
		return archived;
	}
	public void setArchived(BigDecimal archived) {
		this.archived = archived;
	}
	public String getDasiId() {
		return dasiId;
	}
	public void setDasiId(String dasiId) {
		this.dasiId = dasiId;
	}
	public BigDecimal getGpsLatitude() {
		return gpsLatitude;
	}
	public void setGpsLatitude(BigDecimal gpsLatitude) {
		this.gpsLatitude = gpsLatitude;
	}
	public BigDecimal getGpsLongitude() {
		return gpsLongitude;
	}
	public void setGpsLongitude(BigDecimal gpsLongitude) {
		this.gpsLongitude = gpsLongitude;
	}
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProvinceNumber() {
		return provinceNumber;
	}
	public void setProvinceNumber(String provinceNumber) {
		this.provinceNumber = provinceNumber;
	}
	public BigDecimal getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(BigDecimal sortOrder) {
		this.sortOrder = sortOrder;
	}

}
