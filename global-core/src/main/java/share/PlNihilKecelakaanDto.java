package share;

import java.io.Serializable;
import java.util.Date;

public class PlNihilKecelakaanDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String createdBy;
	private Date creationDate;
	private String idLakaNhl;
	private String kodeInstansi;
	private String kodeKantorJr;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String statusNhl;
	private Date tglKejadian;
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getIdLakaNhl() {
		return idLakaNhl;
	}
	public void setIdLakaNhl(String idLakaNhl) {
		this.idLakaNhl = idLakaNhl;
	}
	public String getKodeInstansi() {
		return kodeInstansi;
	}
	public void setKodeInstansi(String kodeInstansi) {
		this.kodeInstansi = kodeInstansi;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getStatusNhl() {
		return statusNhl;
	}
	public void setStatusNhl(String statusNhl) {
		this.statusNhl = statusNhl;
	}
	public Date getTglKejadian() {
		return tglKejadian;
	}
	public void setTglKejadian(Date tglKejadian) {
		this.tglKejadian = tglKejadian;
	}

}
