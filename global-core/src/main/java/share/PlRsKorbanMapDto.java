package share;

import java.io.Serializable;
import java.util.Date;

public class PlRsKorbanMapDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String createdBy;
	private Date createdDate;
	private String idKecelakaan;
	private String idKorbanKecelakaan;
	private String kodeKejadian;
	private String kodeRs;
	private String mapCond;
	
	
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getIdKecelakaan() {
		return idKecelakaan;
	}
	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}
	public String getIdKorbanKecelakaan() {
		return idKorbanKecelakaan;
	}
	public void setIdKorbanKecelakaan(String idKorbanKecelakaan) {
		this.idKorbanKecelakaan = idKorbanKecelakaan;
	}
	public String getKodeKejadian() {
		return kodeKejadian;
	}
	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}
	public String getKodeRs() {
		return kodeRs;
	}
	public void setKodeRs(String kodeRs) {
		this.kodeRs = kodeRs;
	}
	public String getMapCond() {
		return mapCond;
	}
	public void setMapCond(String mapCond) {
		this.mapCond = mapCond;
	}


}
