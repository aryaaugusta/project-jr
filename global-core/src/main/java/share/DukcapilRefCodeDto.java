package share;

import java.io.Serializable;
import java.util.Date;


public class DukcapilRefCodeDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String rfDomain;
	private String rfLowValue;
	private String rfHighValue;
	private String rvMeaning;
	private Date lastSyncDate;
	public String getRfDomain() {
		return rfDomain;
	}
	public void setRfDomain(String rfDomain) {
		this.rfDomain = rfDomain;
	}
	public String getRfLowValue() {
		return rfLowValue;
	}
	public void setRfLowValue(String rfLowValue) {
		this.rfLowValue = rfLowValue;
	}
	public String getRfHighValue() {
		return rfHighValue;
	}
	public void setRfHighValue(String rfHighValue) {
		this.rfHighValue = rfHighValue;
	}
	public String getRvMeaning() {
		return rvMeaning;
	}
	public void setRvMeaning(String rvMeaning) {
		this.rvMeaning = rvMeaning;
	}
	public Date getLastSyncDate() {
		return lastSyncDate;
	}
	public void setLastSyncDate(Date lastSyncDate) {
		this.lastSyncDate = lastSyncDate;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((rfDomain == null) ? 0 : rfDomain.hashCode());
		result = prime * result
				+ ((rfHighValue == null) ? 0 : rfHighValue.hashCode());
		result = prime * result
				+ ((rfLowValue == null) ? 0 : rfLowValue.hashCode());
		result = prime * result
				+ ((rvMeaning == null) ? 0 : rvMeaning.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DukcapilRefCodeDto other = (DukcapilRefCodeDto) obj;
		if (rfDomain == null) {
			if (other.rfDomain != null)
				return false;
		} else if (!rfDomain.equals(other.rfDomain))
			return false;
		if (rfHighValue == null) {
			if (other.rfHighValue != null)
				return false;
		} else if (!rfHighValue.equals(other.rfHighValue))
			return false;
		if (rfLowValue == null) {
			if (other.rfLowValue != null)
				return false;
		} else if (!rfLowValue.equals(other.rfLowValue))
			return false;
		if (rvMeaning == null) {
			if (other.rvMeaning != null)
				return false;
		} else if (!rvMeaning.equals(other.rvMeaning))
			return false;
		return true;
	}
	
}
