package share;

import java.io.Serializable;
import java.util.Date;

public class PlRekeningRDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idRsRek;
	private String createdBy;
	private Date creationDate;
	private String flagEnable;
	private String kodeBank;
	private String kodeKantorJr;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String namaPemilik;
	private String noRekening;
	private String kodeRumahsakit;
	
	//added by Luthfi
	private String namaBank;
	private String namaRumahSakit;
	private int no;
	
	private String mode;
	
	public String getIdRsRek() {
		return idRsRek;
	}
	public void setIdRsRek(String idRsRek) {
		this.idRsRek = idRsRek;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getFlagEnable() {
		return flagEnable;
	}
	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}
	public String getKodeBank() {
		return kodeBank;
	}
	public void setKodeBank(String kodeBank) {
		this.kodeBank = kodeBank;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getNamaPemilik() {
		return namaPemilik;
	}
	public void setNamaPemilik(String namaPemilik) {
		this.namaPemilik = namaPemilik;
	}
	public String getNoRekening() {
		return noRekening;
	}
	public void setNoRekening(String noRekening) {
		this.noRekening = noRekening;
	}
	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}
	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}
	public String getNamaBank() {
		return namaBank;
	}
	public void setNamaBank(String namaBank) {
		this.namaBank = namaBank;
	}
	public String getNamaRumahSakit() {
		return namaRumahSakit;
	}
	public void setNamaRumahSakit(String namaRumahSakit) {
		this.namaRumahSakit = namaRumahSakit;
	}
	public int getNo() {
		return no;
	}
	public void setNo(int no) {
		this.no = no;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
}
