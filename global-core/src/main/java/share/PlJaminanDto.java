package share;

import java.io.Serializable;
import java.util.Date;



public class PlJaminanDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kodeJaminan;
	private String createdBy;
	private Date creationDate;
	private String kodeLaporan;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private String lingkupJaminan;
	private String pertanggungan;
	private String deskripsi;
	
	private String kodeNama;
	private String kodeNamaJaminan;

	public String getKodeJaminan() {
		return kodeJaminan;
	}
	public void setKodeJaminan(String kodeJaminan) {
		this.kodeJaminan = kodeJaminan;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getKodeLaporan() {
		return kodeLaporan;
	}
	public void setKodeLaporan(String kodeLaporan) {
		this.kodeLaporan = kodeLaporan;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getLingkupJaminan() {
		return lingkupJaminan;
	}
	public void setLingkupJaminan(String lingkupJaminan) {
		this.lingkupJaminan = lingkupJaminan;
	}
	public String getPertanggungan() {
		return pertanggungan;
	}
	public void setPertanggungan(String pertanggungan) {
		this.pertanggungan = pertanggungan;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getKodeNama() {
		return kodeNama;
	}
	public void setKodeNama(String kodeNama) {
		this.kodeNama = kodeNama;
	}
	public String getKodeNamaJaminan() {
		return kodeNamaJaminan;
	}
	public void setKodeNamaJaminan(String kodeNamaJaminan) {
		this.kodeNamaJaminan = kodeNamaJaminan;
	}
	
	
}
