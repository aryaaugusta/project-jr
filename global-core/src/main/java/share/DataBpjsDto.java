package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DataBpjsDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String jnsData;
	private String noBerkas;
	private String kodeKejadian;
	private String kodeRumahSakit;
	private String namaRsBpjs;
	private Date tanggalProses;
	private BigDecimal rupiahPembayaran;
	public String getJnsData() {
		return jnsData;
	}
	public void setJnsData(String jnsData) {
		this.jnsData = jnsData;
	}
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getKodeKejadian() {
		return kodeKejadian;
	}
	public void setKodeKejadian(String kodeKejadian) {
		this.kodeKejadian = kodeKejadian;
	}
	public String getKodeRumahSakit() {
		return kodeRumahSakit;
	}
	public void setKodeRumahSakit(String kodeRumahSakit) {
		this.kodeRumahSakit = kodeRumahSakit;
	}
	public String getNamaRsBpjs() {
		return namaRsBpjs;
	}
	public void setNamaRsBpjs(String namaRsBpjs) {
		this.namaRsBpjs = namaRsBpjs;
	}
	public Date getTanggalProses() {
		return tanggalProses;
	}
	public void setTanggalProses(Date tanggalProses) {
		this.tanggalProses = tanggalProses;
	}
	public BigDecimal getRupiahPembayaran() {
		return rupiahPembayaran;
	}
	public void setRupiahPembayaran(BigDecimal rupiahPembayaran) {
		this.rupiahPembayaran = rupiahPembayaran;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((kodeKejadian == null) ? 0 : kodeKejadian.hashCode());
		result = prime * result
				+ ((kodeRumahSakit == null) ? 0 : kodeRumahSakit.hashCode());
		result = prime * result
				+ ((noBerkas == null) ? 0 : noBerkas.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataBpjsDto other = (DataBpjsDto) obj;
		if (kodeKejadian == null) {
			if (other.kodeKejadian != null)
				return false;
		} else if (!kodeKejadian.equals(other.kodeKejadian))
			return false;
		if (kodeRumahSakit == null) {
			if (other.kodeRumahSakit != null)
				return false;
		} else if (!kodeRumahSakit.equals(other.kodeRumahSakit))
			return false;
		if (noBerkas == null) {
			if (other.noBerkas != null)
				return false;
		} else if (!noBerkas.equals(other.noBerkas))
			return false;
		return true;
	}
	
	
	
}
