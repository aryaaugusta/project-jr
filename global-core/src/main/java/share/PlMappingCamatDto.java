package share;

import java.io.Serializable;


public class PlMappingCamatDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String kodeLokasi;
	private String catatan;
	private String flagEnable;
	private String kodeCamat;
	private String kodeKantorJr;

	public String getKodeLokasi() {
		return kodeLokasi;
	}
	public void setKodeLokasi(String kodeLokasi) {
		this.kodeLokasi = kodeLokasi;
	}
	public String getCatatan() {
		return catatan;
	}
	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}
	public String getFlagEnable() {
		return flagEnable;
	}
	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}
	public String getKodeCamat() {
		return kodeCamat;
	}
	public void setKodeCamat(String kodeCamat) {
		this.kodeCamat = kodeCamat;
	}
	public String getKodeKantorJr() {
		return kodeKantorJr;
	}
	public void setKodeKantorJr(String kodeKantorJr) {
		this.kodeKantorJr = kodeKantorJr;
	}

}
