package share;

import java.util.Date;

public class FndJenisKendaraanDto {

	private String kodeJenis;
	private String createdBy;
	private Date creationDate;
	private String deskripsi;
	private String jnsKend;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	
	private String kodeNama;
	
	public String getKodeJenis() {
		return kodeJenis;
	}
	public void setKodeJenis(String kodeJenis) {
		this.kodeJenis = kodeJenis;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getDeskripsi() {
		return deskripsi;
	}
	public void setDeskripsi(String deskripsi) {
		this.deskripsi = deskripsi;
	}
	public String getJnsKend() {
		return jnsKend;
	}
	public void setJnsKend(String jnsKend) {
		this.jnsKend = jnsKend;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getKodeNama() {
		return kodeNama;
	}
	public void setKodeNama(String kodeNama) {
		this.kodeNama = kodeNama;
	}
}
