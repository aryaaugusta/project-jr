package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PlAngkutanKecelakaanDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String idAngkutanKecelakaan;
	private String alamatPemilik;
	private String alamatPengemudi;
	private String createdBy;
	private Date creationDate;
	private String idGuid;
	private String kode;
	private String kodeGolongan;
	private String kodeJenisSim;
	private String kodeMerk;
	private String kodePo;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	private Date masaBerlakuSim;
	private String namaPemilik;
	private String namaPengemudi;
	private String noPolisi;
	private String noSimPengemudi;
	private String statusKendaraan;
	private BigDecimal tahunPembuatan;
	private String kodeJenis;
	private String idKecelakaan;
	
	//added by luthfi 
	private String jenisDesc;
	private String statusDesc;
	private String simDesc;
	private String merkDesc;
	private String noPlatNamaPengemudiDesc;
	
	private boolean flagUsed;
	
	public boolean isFlagUsed() {
		return flagUsed;
	}
	public void setFlagUsed(boolean flagUsed) {
		this.flagUsed = flagUsed;
	}
	public String getIdAngkutanKecelakaan() {
		return idAngkutanKecelakaan;
	}
	public void setIdAngkutanKecelakaan(String idAngkutanKecelakaan) {
		this.idAngkutanKecelakaan = idAngkutanKecelakaan;
	}
	public String getAlamatPemilik() {
		return alamatPemilik;
	}
	public void setAlamatPemilik(String alamatPemilik) {
		this.alamatPemilik = alamatPemilik;
	}
	public String getAlamatPengemudi() {
		return alamatPengemudi;
	}
	public void setAlamatPengemudi(String alamatPengemudi) {
		this.alamatPengemudi = alamatPengemudi;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getIdGuid() {
		return idGuid;
	}
	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}
	public String getKode() {
		return kode;
	}
	public void setKode(String kode) {
		this.kode = kode;
	}
	public String getKodeGolongan() {
		return kodeGolongan;
	}
	public void setKodeGolongan(String kodeGolongan) {
		this.kodeGolongan = kodeGolongan;
	}
	public String getKodeJenisSim() {
		return kodeJenisSim;
	}
	public void setKodeJenisSim(String kodeJenisSim) {
		this.kodeJenisSim = kodeJenisSim;
	}
	public String getKodeMerk() {
		return kodeMerk;
	}
	public void setKodeMerk(String kodeMerk) {
		this.kodeMerk = kodeMerk;
	}
	public String getKodePo() {
		return kodePo;
	}
	public void setKodePo(String kodePo) {
		this.kodePo = kodePo;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public Date getMasaBerlakuSim() {
		return masaBerlakuSim;
	}
	public void setMasaBerlakuSim(Date masaBerlakuSim) {
		this.masaBerlakuSim = masaBerlakuSim;
	}
	public String getNamaPemilik() {
		return namaPemilik;
	}
	public void setNamaPemilik(String namaPemilik) {
		this.namaPemilik = namaPemilik;
	}
	public String getNamaPengemudi() {
		return namaPengemudi;
	}
	public void setNamaPengemudi(String namaPengemudi) {
		this.namaPengemudi = namaPengemudi;
	}
	public String getNoPolisi() {
		return noPolisi;
	}
	public void setNoPolisi(String noPolisi) {
		this.noPolisi = noPolisi;
	}
	public String getNoSimPengemudi() {
		return noSimPengemudi;
	}
	public void setNoSimPengemudi(String noSimPengemudi) {
		this.noSimPengemudi = noSimPengemudi;
	}
	public String getStatusKendaraan() {
		return statusKendaraan;
	}
	public void setStatusKendaraan(String statusKendaraan) {
		this.statusKendaraan = statusKendaraan;
	}
	public BigDecimal getTahunPembuatan() {
		return tahunPembuatan;
	}
	public void setTahunPembuatan(BigDecimal tahunPembuatan) {
		this.tahunPembuatan = tahunPembuatan;
	}
	public String getKodeJenis() {
		return kodeJenis;
	}
	public void setKodeJenis(String kodeJenis) {
		this.kodeJenis = kodeJenis;
	}
	public String getIdKecelakaan() {
		return idKecelakaan;
	}
	public void setIdKecelakaan(String idKecelakaan) {
		this.idKecelakaan = idKecelakaan;
	}
	public String getJenisDesc() {
		return jenisDesc;
	}
	public void setJenisDesc(String jenisDesc) {
		this.jenisDesc = jenisDesc;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getSimDesc() {
		return simDesc;
	}
	public void setSimDesc(String simDesc) {
		this.simDesc = simDesc;
	}
	public String getMerkDesc() {
		return merkDesc;
	}
	public void setMerkDesc(String merkDesc) {
		this.merkDesc = merkDesc;
	}
	public String getNoPlatNamaPengemudiDesc() {
		return noPlatNamaPengemudiDesc;
	}
	public void setNoPlatNamaPengemudiDesc(String noPlatNamaPengemudiDesc) {
		this.noPlatNamaPengemudiDesc = noPlatNamaPengemudiDesc;
	}
}
