package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class DasiJrRefCodeDto implements Serializable{
	private static final long serialVersionUID = 1L;

	private String flagEnable;
	private Date lastUpdatedDate;
	private BigDecimal orderSeq;
	private String rvAbbreviation;
	private String rvDomain;
	private String rvHighValue;
	private String rvLowValue;
	private String rvMeaning;
	
	//added by luthfi asoy
	private String sifatCidera;
	private String sifatKecelakaan;
	private boolean checkedJenisDomumen;
	private String lowMeaning;
	
	private boolean disabledItem;
	
	public String getFlagEnable() {
		return flagEnable;
	}
	public void setFlagEnable(String flagEnable) {
		this.flagEnable = flagEnable;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public BigDecimal getOrderSeq() {
		return orderSeq;
	}
	public void setOrderSeq(BigDecimal orderSeq) {
		this.orderSeq = orderSeq;
	}
	public String getRvAbbreviation() {
		return rvAbbreviation;
	}
	public void setRvAbbreviation(String rvAbbreviation) {
		this.rvAbbreviation = rvAbbreviation;
	}
	public String getRvDomain() {
		return rvDomain;
	}
	public void setRvDomain(String rvDomain) {
		this.rvDomain = rvDomain;
	}
	public String getRvHighValue() {
		return rvHighValue;
	}
	public void setRvHighValue(String rvHighValue) {
		this.rvHighValue = rvHighValue;
	}
	public String getRvLowValue() {
		return rvLowValue;
	}
	public void setRvLowValue(String rvLowValue) {
		this.rvLowValue = rvLowValue;
	}
	public String getRvMeaning() {
		return rvMeaning;
	}
	public void setRvMeaning(String rvMeaning) {
		this.rvMeaning = rvMeaning;
	}
	public String getSifatCidera() {
		return sifatCidera;
	}
	public void setSifatCidera(String sifatCidera) {
		this.sifatCidera = sifatCidera;
	}
	public String getSifatKecelakaan() {
		return sifatKecelakaan;
	}
	public void setSifatKecelakaan(String sifatKecelakaan) {
		this.sifatKecelakaan = sifatKecelakaan;
	}
	public boolean isCheckedJenisDomumen() {
		return checkedJenisDomumen;
	}
	public void setCheckedJenisDomumen(boolean checkedJenisDomumen) {
		this.checkedJenisDomumen = checkedJenisDomumen;
	}
	public String getLowMeaning() {
		return lowMeaning;
	}
	public void setLowMeaning(String lowMeaning) {
		this.lowMeaning = lowMeaning;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((rvDomain == null) ? 0 : rvDomain.hashCode());
		result = prime * result
				+ ((rvLowValue == null) ? 0 : rvLowValue.hashCode());
		result = prime * result
				+ ((rvMeaning == null) ? 0 : rvMeaning.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DasiJrRefCodeDto other = (DasiJrRefCodeDto) obj;
		if (rvDomain == null) {
			if (other.rvDomain != null)
				return false;
		} else if (!rvDomain.equals(other.rvDomain))
			return false;
		if (rvLowValue == null) {
			if (other.rvLowValue != null)
				return false;
		} else if (!rvLowValue.equals(other.rvLowValue))
			return false;
		if (rvMeaning == null) {
			if (other.rvMeaning != null)
				return false;
		} else if (!rvMeaning.equals(other.rvMeaning))
			return false;
		return true;
	}
	public boolean isDisabledItem() {
		return disabledItem;
	}
	public void setDisabledItem(boolean disabledItem) {
		this.disabledItem = disabledItem;
	}

}
