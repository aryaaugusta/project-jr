package share;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class PlPengajuanRsDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String idPengajuanRs;
	private String noBerkas;
	private String createdBy;
	private Date creationDate;
	private String idGuid;
	private BigDecimal jumlahPengajuanLukaluka;
	private String kodeRumahsakit;
	private String lastUpdatedBy;
	private Date lastUpdatedDate;
	
	
	public String getIdPengajuanRs() {
		return idPengajuanRs;
	}
	public void setIdPengajuanRs(String idPengajuanRs) {
		this.idPengajuanRs = idPengajuanRs;
	}
	public String getNoBerkas() {
		return noBerkas;
	}
	public void setNoBerkas(String noBerkas) {
		this.noBerkas = noBerkas;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	public String getIdGuid() {
		return idGuid;
	}
	public void setIdGuid(String idGuid) {
		this.idGuid = idGuid;
	}
	public BigDecimal getJumlahPengajuanLukaluka() {
		return jumlahPengajuanLukaluka;
	}
	public void setJumlahPengajuanLukaluka(BigDecimal jumlahPengajuanLukaluka) {
		this.jumlahPengajuanLukaluka = jumlahPengajuanLukaluka;
	}
	public String getKodeRumahsakit() {
		return kodeRumahsakit;
	}
	public void setKodeRumahsakit(String kodeRumahsakit) {
		this.kodeRumahsakit = kodeRumahsakit;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	

}
