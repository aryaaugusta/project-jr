/**
 * BpmInstanceInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectInstanceIdCreateSchema;

public class BpmInstanceInfo  implements java.io.Serializable {
    private java.lang.String nomorPermohonan;

    private java.lang.String kodeCabang;

    private java.lang.String column1;

    private java.lang.String creationDate;

    private java.lang.String lastUpdatedBy;

    private java.lang.String lastUpdatedDate;

    private java.lang.String idGuid;

    private java.lang.String column2;

    private java.lang.String column3;

    private java.lang.String otherInfo1;

    private java.lang.String otherInfo2;

    private java.lang.String otherInfo3;

    private java.lang.String otherInfo4;

    private java.lang.String otherInfo5;

    public BpmInstanceInfo() {
    }

    public BpmInstanceInfo(
           java.lang.String nomorPermohonan,
           java.lang.String kodeCabang,
           java.lang.String column1,
           java.lang.String creationDate,
           java.lang.String lastUpdatedBy,
           java.lang.String lastUpdatedDate,
           java.lang.String idGuid,
           java.lang.String column2,
           java.lang.String column3,
           java.lang.String otherInfo1,
           java.lang.String otherInfo2,
           java.lang.String otherInfo3,
           java.lang.String otherInfo4,
           java.lang.String otherInfo5) {
           this.nomorPermohonan = nomorPermohonan;
           this.kodeCabang = kodeCabang;
           this.column1 = column1;
           this.creationDate = creationDate;
           this.lastUpdatedBy = lastUpdatedBy;
           this.lastUpdatedDate = lastUpdatedDate;
           this.idGuid = idGuid;
           this.column2 = column2;
           this.column3 = column3;
           this.otherInfo1 = otherInfo1;
           this.otherInfo2 = otherInfo2;
           this.otherInfo3 = otherInfo3;
           this.otherInfo4 = otherInfo4;
           this.otherInfo5 = otherInfo5;
    }


    /**
     * Gets the nomorPermohonan value for this BpmInstanceInfo.
     * 
     * @return nomorPermohonan
     */
    public java.lang.String getNomorPermohonan() {
        return nomorPermohonan;
    }


    /**
     * Sets the nomorPermohonan value for this BpmInstanceInfo.
     * 
     * @param nomorPermohonan
     */
    public void setNomorPermohonan(java.lang.String nomorPermohonan) {
        this.nomorPermohonan = nomorPermohonan;
    }


    /**
     * Gets the kodeCabang value for this BpmInstanceInfo.
     * 
     * @return kodeCabang
     */
    public java.lang.String getKodeCabang() {
        return kodeCabang;
    }


    /**
     * Sets the kodeCabang value for this BpmInstanceInfo.
     * 
     * @param kodeCabang
     */
    public void setKodeCabang(java.lang.String kodeCabang) {
        this.kodeCabang = kodeCabang;
    }


    /**
     * Gets the column1 value for this BpmInstanceInfo.
     * 
     * @return column1
     */
    public java.lang.String getColumn1() {
        return column1;
    }


    /**
     * Sets the column1 value for this BpmInstanceInfo.
     * 
     * @param column1
     */
    public void setColumn1(java.lang.String column1) {
        this.column1 = column1;
    }


    /**
     * Gets the creationDate value for this BpmInstanceInfo.
     * 
     * @return creationDate
     */
    public java.lang.String getCreationDate() {
        return creationDate;
    }


    /**
     * Sets the creationDate value for this BpmInstanceInfo.
     * 
     * @param creationDate
     */
    public void setCreationDate(java.lang.String creationDate) {
        this.creationDate = creationDate;
    }


    /**
     * Gets the lastUpdatedBy value for this BpmInstanceInfo.
     * 
     * @return lastUpdatedBy
     */
    public java.lang.String getLastUpdatedBy() {
        return lastUpdatedBy;
    }


    /**
     * Sets the lastUpdatedBy value for this BpmInstanceInfo.
     * 
     * @param lastUpdatedBy
     */
    public void setLastUpdatedBy(java.lang.String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }


    /**
     * Gets the lastUpdatedDate value for this BpmInstanceInfo.
     * 
     * @return lastUpdatedDate
     */
    public java.lang.String getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    /**
     * Sets the lastUpdatedDate value for this BpmInstanceInfo.
     * 
     * @param lastUpdatedDate
     */
    public void setLastUpdatedDate(java.lang.String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }


    /**
     * Gets the idGuid value for this BpmInstanceInfo.
     * 
     * @return idGuid
     */
    public java.lang.String getIdGuid() {
        return idGuid;
    }


    /**
     * Sets the idGuid value for this BpmInstanceInfo.
     * 
     * @param idGuid
     */
    public void setIdGuid(java.lang.String idGuid) {
        this.idGuid = idGuid;
    }


    /**
     * Gets the column2 value for this BpmInstanceInfo.
     * 
     * @return column2
     */
    public java.lang.String getColumn2() {
        return column2;
    }


    /**
     * Sets the column2 value for this BpmInstanceInfo.
     * 
     * @param column2
     */
    public void setColumn2(java.lang.String column2) {
        this.column2 = column2;
    }


    /**
     * Gets the column3 value for this BpmInstanceInfo.
     * 
     * @return column3
     */
    public java.lang.String getColumn3() {
        return column3;
    }


    /**
     * Sets the column3 value for this BpmInstanceInfo.
     * 
     * @param column3
     */
    public void setColumn3(java.lang.String column3) {
        this.column3 = column3;
    }


    /**
     * Gets the otherInfo1 value for this BpmInstanceInfo.
     * 
     * @return otherInfo1
     */
    public java.lang.String getOtherInfo1() {
        return otherInfo1;
    }


    /**
     * Sets the otherInfo1 value for this BpmInstanceInfo.
     * 
     * @param otherInfo1
     */
    public void setOtherInfo1(java.lang.String otherInfo1) {
        this.otherInfo1 = otherInfo1;
    }


    /**
     * Gets the otherInfo2 value for this BpmInstanceInfo.
     * 
     * @return otherInfo2
     */
    public java.lang.String getOtherInfo2() {
        return otherInfo2;
    }


    /**
     * Sets the otherInfo2 value for this BpmInstanceInfo.
     * 
     * @param otherInfo2
     */
    public void setOtherInfo2(java.lang.String otherInfo2) {
        this.otherInfo2 = otherInfo2;
    }


    /**
     * Gets the otherInfo3 value for this BpmInstanceInfo.
     * 
     * @return otherInfo3
     */
    public java.lang.String getOtherInfo3() {
        return otherInfo3;
    }


    /**
     * Sets the otherInfo3 value for this BpmInstanceInfo.
     * 
     * @param otherInfo3
     */
    public void setOtherInfo3(java.lang.String otherInfo3) {
        this.otherInfo3 = otherInfo3;
    }


    /**
     * Gets the otherInfo4 value for this BpmInstanceInfo.
     * 
     * @return otherInfo4
     */
    public java.lang.String getOtherInfo4() {
        return otherInfo4;
    }


    /**
     * Sets the otherInfo4 value for this BpmInstanceInfo.
     * 
     * @param otherInfo4
     */
    public void setOtherInfo4(java.lang.String otherInfo4) {
        this.otherInfo4 = otherInfo4;
    }


    /**
     * Gets the otherInfo5 value for this BpmInstanceInfo.
     * 
     * @return otherInfo5
     */
    public java.lang.String getOtherInfo5() {
        return otherInfo5;
    }


    /**
     * Sets the otherInfo5 value for this BpmInstanceInfo.
     * 
     * @param otherInfo5
     */
    public void setOtherInfo5(java.lang.String otherInfo5) {
        this.otherInfo5 = otherInfo5;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BpmInstanceInfo)) return false;
        BpmInstanceInfo other = (BpmInstanceInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nomorPermohonan==null && other.getNomorPermohonan()==null) || 
             (this.nomorPermohonan!=null &&
              this.nomorPermohonan.equals(other.getNomorPermohonan()))) &&
            ((this.kodeCabang==null && other.getKodeCabang()==null) || 
             (this.kodeCabang!=null &&
              this.kodeCabang.equals(other.getKodeCabang()))) &&
            ((this.column1==null && other.getColumn1()==null) || 
             (this.column1!=null &&
              this.column1.equals(other.getColumn1()))) &&
            ((this.creationDate==null && other.getCreationDate()==null) || 
             (this.creationDate!=null &&
              this.creationDate.equals(other.getCreationDate()))) &&
            ((this.lastUpdatedBy==null && other.getLastUpdatedBy()==null) || 
             (this.lastUpdatedBy!=null &&
              this.lastUpdatedBy.equals(other.getLastUpdatedBy()))) &&
            ((this.lastUpdatedDate==null && other.getLastUpdatedDate()==null) || 
             (this.lastUpdatedDate!=null &&
              this.lastUpdatedDate.equals(other.getLastUpdatedDate()))) &&
            ((this.idGuid==null && other.getIdGuid()==null) || 
             (this.idGuid!=null &&
              this.idGuid.equals(other.getIdGuid()))) &&
            ((this.column2==null && other.getColumn2()==null) || 
             (this.column2!=null &&
              this.column2.equals(other.getColumn2()))) &&
            ((this.column3==null && other.getColumn3()==null) || 
             (this.column3!=null &&
              this.column3.equals(other.getColumn3()))) &&
            ((this.otherInfo1==null && other.getOtherInfo1()==null) || 
             (this.otherInfo1!=null &&
              this.otherInfo1.equals(other.getOtherInfo1()))) &&
            ((this.otherInfo2==null && other.getOtherInfo2()==null) || 
             (this.otherInfo2!=null &&
              this.otherInfo2.equals(other.getOtherInfo2()))) &&
            ((this.otherInfo3==null && other.getOtherInfo3()==null) || 
             (this.otherInfo3!=null &&
              this.otherInfo3.equals(other.getOtherInfo3()))) &&
            ((this.otherInfo4==null && other.getOtherInfo4()==null) || 
             (this.otherInfo4!=null &&
              this.otherInfo4.equals(other.getOtherInfo4()))) &&
            ((this.otherInfo5==null && other.getOtherInfo5()==null) || 
             (this.otherInfo5!=null &&
              this.otherInfo5.equals(other.getOtherInfo5())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNomorPermohonan() != null) {
            _hashCode += getNomorPermohonan().hashCode();
        }
        if (getKodeCabang() != null) {
            _hashCode += getKodeCabang().hashCode();
        }
        if (getColumn1() != null) {
            _hashCode += getColumn1().hashCode();
        }
        if (getCreationDate() != null) {
            _hashCode += getCreationDate().hashCode();
        }
        if (getLastUpdatedBy() != null) {
            _hashCode += getLastUpdatedBy().hashCode();
        }
        if (getLastUpdatedDate() != null) {
            _hashCode += getLastUpdatedDate().hashCode();
        }
        if (getIdGuid() != null) {
            _hashCode += getIdGuid().hashCode();
        }
        if (getColumn2() != null) {
            _hashCode += getColumn2().hashCode();
        }
        if (getColumn3() != null) {
            _hashCode += getColumn3().hashCode();
        }
        if (getOtherInfo1() != null) {
            _hashCode += getOtherInfo1().hashCode();
        }
        if (getOtherInfo2() != null) {
            _hashCode += getOtherInfo2().hashCode();
        }
        if (getOtherInfo3() != null) {
            _hashCode += getOtherInfo3().hashCode();
        }
        if (getOtherInfo4() != null) {
            _hashCode += getOtherInfo4().hashCode();
        }
        if (getOtherInfo5() != null) {
            _hashCode += getOtherInfo5().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BpmInstanceInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "BpmInstanceInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomorPermohonan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "nomorPermohonan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kodeCabang");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "kodeCabang"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("column1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "column1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "creationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdatedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "lastUpdatedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdatedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "lastUpdatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idGuid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "idGuid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("column2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "column2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("column3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "column3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "otherInfo1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "otherInfo2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "otherInfo3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "otherInfo4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo5");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "otherInfo5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
