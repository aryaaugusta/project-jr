/**
 * Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectInstanceIdCreateSchema;

public class Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid  implements java.io.Serializable {
    private java.lang.String nomorPermohonan;

    private java.lang.String creationDate;

    private java.lang.String instanceid;

    public Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid() {
    }

    public Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid(
           java.lang.String nomorPermohonan,
           java.lang.String creationDate,
           java.lang.String instanceid) {
           this.nomorPermohonan = nomorPermohonan;
           this.creationDate = creationDate;
           this.instanceid = instanceid;
    }


    /**
     * Gets the nomorPermohonan value for this Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid.
     * 
     * @return nomorPermohonan
     */
    public java.lang.String getNomorPermohonan() {
        return nomorPermohonan;
    }


    /**
     * Sets the nomorPermohonan value for this Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid.
     * 
     * @param nomorPermohonan
     */
    public void setNomorPermohonan(java.lang.String nomorPermohonan) {
        this.nomorPermohonan = nomorPermohonan;
    }


    /**
     * Gets the creationDate value for this Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid.
     * 
     * @return creationDate
     */
    public java.lang.String getCreationDate() {
        return creationDate;
    }


    /**
     * Sets the creationDate value for this Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid.
     * 
     * @param creationDate
     */
    public void setCreationDate(java.lang.String creationDate) {
        this.creationDate = creationDate;
    }


    /**
     * Gets the instanceid value for this Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid.
     * 
     * @return instanceid
     */
    public java.lang.String getInstanceid() {
        return instanceid;
    }


    /**
     * Sets the instanceid value for this Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid.
     * 
     * @param instanceid
     */
    public void setInstanceid(java.lang.String instanceid) {
        this.instanceid = instanceid;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid)) return false;
        Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid other = (Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nomorPermohonan==null && other.getNomorPermohonan()==null) || 
             (this.nomorPermohonan!=null &&
              this.nomorPermohonan.equals(other.getNomorPermohonan()))) &&
            ((this.creationDate==null && other.getCreationDate()==null) || 
             (this.creationDate!=null &&
              this.creationDate.equals(other.getCreationDate()))) &&
            ((this.instanceid==null && other.getInstanceid()==null) || 
             (this.instanceid!=null &&
              this.instanceid.equals(other.getInstanceid())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNomorPermohonan() != null) {
            _hashCode += getNomorPermohonan().hashCode();
        }
        if (getCreationDate() != null) {
            _hashCode += getCreationDate().hashCode();
        }
        if (getInstanceid() != null) {
            _hashCode += getInstanceid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomorPermohonan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "nomorPermohonan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "creationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instanceid");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "instanceid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
