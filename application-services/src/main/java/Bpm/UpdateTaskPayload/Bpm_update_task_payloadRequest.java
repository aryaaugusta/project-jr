/**
 * Bpm_update_task_payloadRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.UpdateTaskPayload;

public class Bpm_update_task_payloadRequest  implements java.io.Serializable {
    private Bpm.UpdateTaskPayload.CredentialType credential;

    private Bpm.UpdateTaskPayload.PayloadType payload;

    private java.lang.String outcome;

    private java.math.BigInteger taskNumber;

    public Bpm_update_task_payloadRequest() {
    }

    public Bpm_update_task_payloadRequest(
           Bpm.UpdateTaskPayload.CredentialType credential,
           Bpm.UpdateTaskPayload.PayloadType payload,
           java.lang.String outcome,
           java.math.BigInteger taskNumber) {
           this.credential = credential;
           this.payload = payload;
           this.outcome = outcome;
           this.taskNumber = taskNumber;
    }


    /**
     * Gets the credential value for this Bpm_update_task_payloadRequest.
     * 
     * @return credential
     */
    public Bpm.UpdateTaskPayload.CredentialType getCredential() {
        return credential;
    }


    /**
     * Sets the credential value for this Bpm_update_task_payloadRequest.
     * 
     * @param credential
     */
    public void setCredential(Bpm.UpdateTaskPayload.CredentialType credential) {
        this.credential = credential;
    }


    /**
     * Gets the payload value for this Bpm_update_task_payloadRequest.
     * 
     * @return payload
     */
    public Bpm.UpdateTaskPayload.PayloadType getPayload() {
        return payload;
    }


    /**
     * Sets the payload value for this Bpm_update_task_payloadRequest.
     * 
     * @param payload
     */
    public void setPayload(Bpm.UpdateTaskPayload.PayloadType payload) {
        this.payload = payload;
    }


    /**
     * Gets the outcome value for this Bpm_update_task_payloadRequest.
     * 
     * @return outcome
     */
    public java.lang.String getOutcome() {
        return outcome;
    }


    /**
     * Sets the outcome value for this Bpm_update_task_payloadRequest.
     * 
     * @param outcome
     */
    public void setOutcome(java.lang.String outcome) {
        this.outcome = outcome;
    }


    /**
     * Gets the taskNumber value for this Bpm_update_task_payloadRequest.
     * 
     * @return taskNumber
     */
    public java.math.BigInteger getTaskNumber() {
        return taskNumber;
    }


    /**
     * Sets the taskNumber value for this Bpm_update_task_payloadRequest.
     * 
     * @param taskNumber
     */
    public void setTaskNumber(java.math.BigInteger taskNumber) {
        this.taskNumber = taskNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_update_task_payloadRequest)) return false;
        Bpm_update_task_payloadRequest other = (Bpm_update_task_payloadRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.credential==null && other.getCredential()==null) || 
             (this.credential!=null &&
              this.credential.equals(other.getCredential()))) &&
            ((this.payload==null && other.getPayload()==null) || 
             (this.payload!=null &&
              this.payload.equals(other.getPayload()))) &&
            ((this.outcome==null && other.getOutcome()==null) || 
             (this.outcome!=null &&
              this.outcome.equals(other.getOutcome()))) &&
            ((this.taskNumber==null && other.getTaskNumber()==null) || 
             (this.taskNumber!=null &&
              this.taskNumber.equals(other.getTaskNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCredential() != null) {
            _hashCode += getCredential().hashCode();
        }
        if (getPayload() != null) {
            _hashCode += getPayload().hashCode();
        }
        if (getOutcome() != null) {
            _hashCode += getOutcome().hashCode();
        }
        if (getTaskNumber() != null) {
            _hashCode += getTaskNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_update_task_payloadRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_update_task_payload_wsdl/", ">bpm_update_task_payloadRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credential");
        elemField.setXmlName(new javax.xml.namespace.QName("", "credential"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_update_task_payload_wsdl/", "credentialType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payload");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payload"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_update_task_payload_wsdl/", "payloadType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("outcome");
        elemField.setXmlName(new javax.xml.namespace.QName("", "outcome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taskNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
