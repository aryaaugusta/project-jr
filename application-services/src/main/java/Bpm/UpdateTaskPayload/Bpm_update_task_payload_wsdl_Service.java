/**
 * Bpm_update_task_payload_wsdl_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.UpdateTaskPayload;

public interface Bpm_update_task_payload_wsdl_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_update_task_payload_wsdlSOAPAddress();

    public Bpm.UpdateTaskPayload.Bpm_update_task_payload_wsdl_PortType getbpm_update_task_payload_wsdlSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.UpdateTaskPayload.Bpm_update_task_payload_wsdl_PortType getbpm_update_task_payload_wsdlSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
