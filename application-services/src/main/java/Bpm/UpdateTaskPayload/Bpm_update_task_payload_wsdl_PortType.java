/**
 * Bpm_update_task_payload_wsdl_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.UpdateTaskPayload;

public interface Bpm_update_task_payload_wsdl_PortType extends java.rmi.Remote {
    public Bpm.UpdateTaskPayload.Bpm_update_task_payloadResponse bpm_update_task_payload(Bpm.UpdateTaskPayload.Bpm_update_task_payloadRequest parameters) throws java.rmi.RemoteException;
}
