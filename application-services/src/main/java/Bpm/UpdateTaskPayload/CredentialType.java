/**
 * CredentialType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.UpdateTaskPayload;

public class CredentialType  implements java.io.Serializable {
    private java.lang.String login;

    private java.lang.String password;

    private java.lang.String identityContext;

    private java.lang.String onBehalfOfUser;

    public CredentialType() {
    }

    public CredentialType(
           java.lang.String login,
           java.lang.String password,
           java.lang.String identityContext,
           java.lang.String onBehalfOfUser) {
           this.login = login;
           this.password = password;
           this.identityContext = identityContext;
           this.onBehalfOfUser = onBehalfOfUser;
    }


    /**
     * Gets the login value for this CredentialType.
     * 
     * @return login
     */
    public java.lang.String getLogin() {
        return login;
    }


    /**
     * Sets the login value for this CredentialType.
     * 
     * @param login
     */
    public void setLogin(java.lang.String login) {
        this.login = login;
    }


    /**
     * Gets the password value for this CredentialType.
     * 
     * @return password
     */
    public java.lang.String getPassword() {
        return password;
    }


    /**
     * Sets the password value for this CredentialType.
     * 
     * @param password
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }


    /**
     * Gets the identityContext value for this CredentialType.
     * 
     * @return identityContext
     */
    public java.lang.String getIdentityContext() {
        return identityContext;
    }


    /**
     * Sets the identityContext value for this CredentialType.
     * 
     * @param identityContext
     */
    public void setIdentityContext(java.lang.String identityContext) {
        this.identityContext = identityContext;
    }


    /**
     * Gets the onBehalfOfUser value for this CredentialType.
     * 
     * @return onBehalfOfUser
     */
    public java.lang.String getOnBehalfOfUser() {
        return onBehalfOfUser;
    }


    /**
     * Sets the onBehalfOfUser value for this CredentialType.
     * 
     * @param onBehalfOfUser
     */
    public void setOnBehalfOfUser(java.lang.String onBehalfOfUser) {
        this.onBehalfOfUser = onBehalfOfUser;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CredentialType)) return false;
        CredentialType other = (CredentialType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.login==null && other.getLogin()==null) || 
             (this.login!=null &&
              this.login.equals(other.getLogin()))) &&
            ((this.password==null && other.getPassword()==null) || 
             (this.password!=null &&
              this.password.equals(other.getPassword()))) &&
            ((this.identityContext==null && other.getIdentityContext()==null) || 
             (this.identityContext!=null &&
              this.identityContext.equals(other.getIdentityContext()))) &&
            ((this.onBehalfOfUser==null && other.getOnBehalfOfUser()==null) || 
             (this.onBehalfOfUser!=null &&
              this.onBehalfOfUser.equals(other.getOnBehalfOfUser())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getLogin() != null) {
            _hashCode += getLogin().hashCode();
        }
        if (getPassword() != null) {
            _hashCode += getPassword().hashCode();
        }
        if (getIdentityContext() != null) {
            _hashCode += getIdentityContext().hashCode();
        }
        if (getOnBehalfOfUser() != null) {
            _hashCode += getOnBehalfOfUser().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CredentialType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_update_task_payload_wsdl/", "credentialType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("login");
        elemField.setXmlName(new javax.xml.namespace.QName("", "login"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("password");
        elemField.setXmlName(new javax.xml.namespace.QName("", "password"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identityContext");
        elemField.setXmlName(new javax.xml.namespace.QName("", "identityContext"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("onBehalfOfUser");
        elemField.setXmlName(new javax.xml.namespace.QName("", "onBehalfOfUser"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
