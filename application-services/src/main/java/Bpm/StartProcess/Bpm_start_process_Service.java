/**
 * Bpm_start_process_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.StartProcess;

public interface Bpm_start_process_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_start_processSOAPAddress();

    public Bpm.StartProcess.Bpm_start_process_PortType getbpm_start_processSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.StartProcess.Bpm_start_process_PortType getbpm_start_processSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
