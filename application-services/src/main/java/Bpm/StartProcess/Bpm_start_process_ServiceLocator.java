/**
 * Bpm_start_process_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.StartProcess;

public class Bpm_start_process_ServiceLocator extends org.apache.axis.client.Service implements Bpm.StartProcess.Bpm_start_process_Service {

    public Bpm_start_process_ServiceLocator() {
    }


    public Bpm_start_process_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_start_process_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for bpm_start_processSOAP
    private java.lang.String bpm_start_processSOAP_address = "http://192.168.1.136:8011/JR-BPM/BPM/ProxyServices/bpm_start_process_ps";

    public java.lang.String getbpm_start_processSOAPAddress() {
        return bpm_start_processSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String bpm_start_processSOAPWSDDServiceName = "bpm_start_processSOAP";

    public java.lang.String getbpm_start_processSOAPWSDDServiceName() {
        return bpm_start_processSOAPWSDDServiceName;
    }

    public void setbpm_start_processSOAPWSDDServiceName(java.lang.String name) {
        bpm_start_processSOAPWSDDServiceName = name;
    }

    public Bpm.StartProcess.Bpm_start_process_PortType getbpm_start_processSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(bpm_start_processSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getbpm_start_processSOAP(endpoint);
    }

    public Bpm.StartProcess.Bpm_start_process_PortType getbpm_start_processSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.StartProcess.Bpm_start_processSOAPStub _stub = new Bpm.StartProcess.Bpm_start_processSOAPStub(portAddress, this);
            _stub.setPortName(getbpm_start_processSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setbpm_start_processSOAPEndpointAddress(java.lang.String address) {
        bpm_start_processSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.StartProcess.Bpm_start_process_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.StartProcess.Bpm_start_processSOAPStub _stub = new Bpm.StartProcess.Bpm_start_processSOAPStub(new java.net.URL(bpm_start_processSOAP_address), this);
                _stub.setPortName(getbpm_start_processSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_start_processSOAP".equals(inputPortName)) {
            return getbpm_start_processSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.example.org/bpm_start_process/", "bpm_start_process");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.example.org/bpm_start_process/", "bpm_start_processSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("bpm_start_processSOAP".equals(portName)) {
            setbpm_start_processSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
