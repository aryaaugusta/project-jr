package Bpm.StartProcess;

public class Bpm_start_processProxy implements Bpm.StartProcess.Bpm_start_process_PortType {
  private String _endpoint = null;
  private Bpm.StartProcess.Bpm_start_process_PortType bpm_start_process_PortType = null;
  
  public Bpm_start_processProxy() {
    _initBpm_start_processProxy();
  }
  
  public Bpm_start_processProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_start_processProxy();
  }
  
  private void _initBpm_start_processProxy() {
    try {
      bpm_start_process_PortType = (new Bpm.StartProcess.Bpm_start_process_ServiceLocator()).getbpm_start_processSOAP();
      if (bpm_start_process_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_start_process_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_start_process_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_start_process_PortType != null)
      ((javax.xml.rpc.Stub)bpm_start_process_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.StartProcess.Bpm_start_process_PortType getBpm_start_process_PortType() {
    if (bpm_start_process_PortType == null)
      _initBpm_start_processProxy();
    return bpm_start_process_PortType;
  }
  
  public Bpm.StartProcess.Bpm_start_processResponse bpm_start_process(Bpm.StartProcess.Bpm_start_processRequest parameters) throws java.rmi.RemoteException{
    if (bpm_start_process_PortType == null)
      _initBpm_start_processProxy();
    return bpm_start_process_PortType.bpm_start_process(parameters);
  }
  
  
}