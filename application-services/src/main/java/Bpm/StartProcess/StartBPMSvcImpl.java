package Bpm.StartProcess;

import java.rmi.RemoteException;

public class StartBPMSvcImpl {
public static Bpm_start_processResponse bpmStartProcess(Bpm_start_processRequest parameters) throws RemoteException {
		
		Bpm_start_processProxy service = new Bpm_start_processProxy();
		Bpm_start_process_PortType port = service.getBpm_start_process_PortType();
		return port.bpm_start_process(parameters);
	}

}
