/**
 * Bpm_start_process_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.StartProcess;

public interface Bpm_start_process_PortType extends java.rmi.Remote {
    public Bpm.StartProcess.Bpm_start_processResponse bpm_start_process(Bpm.StartProcess.Bpm_start_processRequest parameters) throws java.rmi.RemoteException;
}
