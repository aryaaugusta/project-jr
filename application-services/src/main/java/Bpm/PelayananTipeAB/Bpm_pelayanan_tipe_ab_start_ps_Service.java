/**
 * Bpm_pelayanan_tipe_ab_start_ps_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.PelayananTipeAB;

public interface Bpm_pelayanan_tipe_ab_start_ps_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_pelayanan_tipe_ab_start_psSOAPAddress();

    public Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_start_ps_PortType getbpm_pelayanan_tipe_ab_start_psSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_start_ps_PortType getbpm_pelayanan_tipe_ab_start_psSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
