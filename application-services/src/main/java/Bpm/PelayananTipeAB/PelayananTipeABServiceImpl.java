package Bpm.PelayananTipeAB;

import java.rmi.RemoteException;



public class PelayananTipeABServiceImpl {
public static Bpm_pelayanan_tipe_ab_startResponse bpmPelTipeAB(Bpm_pelayanan_tipe_ab_startRequest parameters) throws RemoteException {
		
		Bpm_pelayanan_tipe_ab_start_psProxy service = new Bpm_pelayanan_tipe_ab_start_psProxy();
		Bpm_pelayanan_tipe_ab_start_ps_PortType port = service.getBpm_pelayanan_tipe_ab_start_ps_PortType();
		return port.bpm_pelayanan_tipe_ab_start(parameters);
	}

}
