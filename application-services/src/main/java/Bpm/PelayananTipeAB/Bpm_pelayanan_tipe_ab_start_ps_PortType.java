/**
 * Bpm_pelayanan_tipe_ab_start_ps_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.PelayananTipeAB;

public interface Bpm_pelayanan_tipe_ab_start_ps_PortType extends java.rmi.Remote {
    public Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_startResponse bpm_pelayanan_tipe_ab_start(Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_startRequest parameters) throws java.rmi.RemoteException;
}
