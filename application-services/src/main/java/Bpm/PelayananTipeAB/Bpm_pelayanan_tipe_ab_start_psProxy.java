package Bpm.PelayananTipeAB;

public class Bpm_pelayanan_tipe_ab_start_psProxy implements Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_start_ps_PortType {
  private String _endpoint = null;
  private Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_start_ps_PortType bpm_pelayanan_tipe_ab_start_ps_PortType = null;
  
  public Bpm_pelayanan_tipe_ab_start_psProxy() {
    _initBpm_pelayanan_tipe_ab_start_psProxy();
  }
  
  public Bpm_pelayanan_tipe_ab_start_psProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_pelayanan_tipe_ab_start_psProxy();
  }
  
  private void _initBpm_pelayanan_tipe_ab_start_psProxy() {
    try {
      bpm_pelayanan_tipe_ab_start_ps_PortType = (new Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_start_ps_ServiceLocator()).getbpm_pelayanan_tipe_ab_start_psSOAP();
      if (bpm_pelayanan_tipe_ab_start_ps_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_pelayanan_tipe_ab_start_ps_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_pelayanan_tipe_ab_start_ps_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_pelayanan_tipe_ab_start_ps_PortType != null)
      ((javax.xml.rpc.Stub)bpm_pelayanan_tipe_ab_start_ps_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_start_ps_PortType getBpm_pelayanan_tipe_ab_start_ps_PortType() {
    if (bpm_pelayanan_tipe_ab_start_ps_PortType == null)
      _initBpm_pelayanan_tipe_ab_start_psProxy();
    return bpm_pelayanan_tipe_ab_start_ps_PortType;
  }
  
  public Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_startResponse bpm_pelayanan_tipe_ab_start(Bpm.PelayananTipeAB.Bpm_pelayanan_tipe_ab_startRequest parameters) throws java.rmi.RemoteException{
    if (bpm_pelayanan_tipe_ab_start_ps_PortType == null)
      _initBpm_pelayanan_tipe_ab_start_psProxy();
    return bpm_pelayanan_tipe_ab_start_ps_PortType.bpm_pelayanan_tipe_ab_start(parameters);
  }
  
  
}