/**
 * Bpm_ecms_update_doc_info_ps_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.UpdateDocInfo;

public interface Bpm_ecms_update_doc_info_ps_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_ecms_update_doc_info_psSOAPAddress();

    public Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType getbpm_ecms_update_doc_info_psSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType getbpm_ecms_update_doc_info_psSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
