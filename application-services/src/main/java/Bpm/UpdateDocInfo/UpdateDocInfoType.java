/**
 * UpdateDocInfoType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.UpdateDocInfo;

public class UpdateDocInfoType  implements java.io.Serializable {
    private int dID;

    private java.lang.String dRevLabel;

    private java.lang.String dDocName;

    private Bpm.UpdateDocInfo.PropertyType[] customDocMetadata;

    public UpdateDocInfoType() {
    }

    public UpdateDocInfoType(
           int dID,
           java.lang.String dRevLabel,
           java.lang.String dDocName,
           Bpm.UpdateDocInfo.PropertyType[] customDocMetadata) {
           this.dID = dID;
           this.dRevLabel = dRevLabel;
           this.dDocName = dDocName;
           this.customDocMetadata = customDocMetadata;
    }


    /**
     * Gets the dID value for this UpdateDocInfoType.
     * 
     * @return dID
     */
    public int getDID() {
        return dID;
    }


    /**
     * Sets the dID value for this UpdateDocInfoType.
     * 
     * @param dID
     */
    public void setDID(int dID) {
        this.dID = dID;
    }


    /**
     * Gets the dRevLabel value for this UpdateDocInfoType.
     * 
     * @return dRevLabel
     */
    public java.lang.String getDRevLabel() {
        return dRevLabel;
    }


    /**
     * Sets the dRevLabel value for this UpdateDocInfoType.
     * 
     * @param dRevLabel
     */
    public void setDRevLabel(java.lang.String dRevLabel) {
        this.dRevLabel = dRevLabel;
    }


    /**
     * Gets the dDocName value for this UpdateDocInfoType.
     * 
     * @return dDocName
     */
    public java.lang.String getDDocName() {
        return dDocName;
    }


    /**
     * Sets the dDocName value for this UpdateDocInfoType.
     * 
     * @param dDocName
     */
    public void setDDocName(java.lang.String dDocName) {
        this.dDocName = dDocName;
    }


    /**
     * Gets the customDocMetadata value for this UpdateDocInfoType.
     * 
     * @return customDocMetadata
     */
    public Bpm.UpdateDocInfo.PropertyType[] getCustomDocMetadata() {
        return customDocMetadata;
    }


    /**
     * Sets the customDocMetadata value for this UpdateDocInfoType.
     * 
     * @param customDocMetadata
     */
    public void setCustomDocMetadata(Bpm.UpdateDocInfo.PropertyType[] customDocMetadata) {
        this.customDocMetadata = customDocMetadata;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UpdateDocInfoType)) return false;
        UpdateDocInfoType other = (UpdateDocInfoType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.dID == other.getDID() &&
            ((this.dRevLabel==null && other.getDRevLabel()==null) || 
             (this.dRevLabel!=null &&
              this.dRevLabel.equals(other.getDRevLabel()))) &&
            ((this.dDocName==null && other.getDDocName()==null) || 
             (this.dDocName!=null &&
              this.dDocName.equals(other.getDDocName()))) &&
            ((this.customDocMetadata==null && other.getCustomDocMetadata()==null) || 
             (this.customDocMetadata!=null &&
              java.util.Arrays.equals(this.customDocMetadata, other.getCustomDocMetadata())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getDID();
        if (getDRevLabel() != null) {
            _hashCode += getDRevLabel().hashCode();
        }
        if (getDDocName() != null) {
            _hashCode += getDDocName().hashCode();
        }
        if (getCustomDocMetadata() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomDocMetadata());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomDocMetadata(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UpdateDocInfoType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_update_doc_info_ps/", "updateDocInfoType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DRevLabel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dRevLabel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDocName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dDocName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customDocMetadata");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customDocMetadata"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_update_doc_info_ps/", "propertyType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "property"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
