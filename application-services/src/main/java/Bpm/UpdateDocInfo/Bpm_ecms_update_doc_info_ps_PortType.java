/**
 * Bpm_ecms_update_doc_info_ps_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.UpdateDocInfo;

public interface Bpm_ecms_update_doc_info_ps_PortType extends java.rmi.Remote {
    public Bpm.UpdateDocInfo.UpdateDocInfoResultType update_doc_info(Bpm.UpdateDocInfo.UpdateDocInfoType parameters) throws java.rmi.RemoteException;
}
