/**
 * Bpm_ecms_update_doc_info_ps_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.UpdateDocInfo;

public class Bpm_ecms_update_doc_info_ps_ServiceLocator extends org.apache.axis.client.Service implements Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_Service {

    public Bpm_ecms_update_doc_info_ps_ServiceLocator() {
    }


    public Bpm_ecms_update_doc_info_ps_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_ecms_update_doc_info_ps_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for bpm_ecms_update_doc_info_psSOAP
    private java.lang.String bpm_ecms_update_doc_info_psSOAP_address = "http://192.168.1.136:8011/JR-BPM/ECMS/Enterprise_Services/Proxy_Services/bpm_ecms_update_doc_info_ps";

    public java.lang.String getbpm_ecms_update_doc_info_psSOAPAddress() {
        return bpm_ecms_update_doc_info_psSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String bpm_ecms_update_doc_info_psSOAPWSDDServiceName = "bpm_ecms_update_doc_info_psSOAP";

    public java.lang.String getbpm_ecms_update_doc_info_psSOAPWSDDServiceName() {
        return bpm_ecms_update_doc_info_psSOAPWSDDServiceName;
    }

    public void setbpm_ecms_update_doc_info_psSOAPWSDDServiceName(java.lang.String name) {
        bpm_ecms_update_doc_info_psSOAPWSDDServiceName = name;
    }

    public Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType getbpm_ecms_update_doc_info_psSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(bpm_ecms_update_doc_info_psSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getbpm_ecms_update_doc_info_psSOAP(endpoint);
    }

    public Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType getbpm_ecms_update_doc_info_psSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_psSOAPStub _stub = new Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_psSOAPStub(portAddress, this);
            _stub.setPortName(getbpm_ecms_update_doc_info_psSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setbpm_ecms_update_doc_info_psSOAPEndpointAddress(java.lang.String address) {
        bpm_ecms_update_doc_info_psSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_psSOAPStub _stub = new Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_psSOAPStub(new java.net.URL(bpm_ecms_update_doc_info_psSOAP_address), this);
                _stub.setPortName(getbpm_ecms_update_doc_info_psSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_ecms_update_doc_info_psSOAP".equals(inputPortName)) {
            return getbpm_ecms_update_doc_info_psSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_update_doc_info_ps/", "bpm_ecms_update_doc_info_ps");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_update_doc_info_ps/", "bpm_ecms_update_doc_info_psSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("bpm_ecms_update_doc_info_psSOAP".equals(portName)) {
            setbpm_ecms_update_doc_info_psSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
