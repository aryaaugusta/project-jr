package Bpm.UpdateDocInfo;

public class Bpm_ecms_update_doc_info_psProxy implements Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType {
  private String _endpoint = null;
  private Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType bpm_ecms_update_doc_info_ps_PortType = null;
  
  public Bpm_ecms_update_doc_info_psProxy() {
    _initBpm_ecms_update_doc_info_psProxy();
  }
  
  public Bpm_ecms_update_doc_info_psProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_ecms_update_doc_info_psProxy();
  }
  
  private void _initBpm_ecms_update_doc_info_psProxy() {
    try {
      bpm_ecms_update_doc_info_ps_PortType = (new Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_ServiceLocator()).getbpm_ecms_update_doc_info_psSOAP();
      if (bpm_ecms_update_doc_info_ps_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_ecms_update_doc_info_ps_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_ecms_update_doc_info_ps_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_ecms_update_doc_info_ps_PortType != null)
      ((javax.xml.rpc.Stub)bpm_ecms_update_doc_info_ps_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.UpdateDocInfo.Bpm_ecms_update_doc_info_ps_PortType getBpm_ecms_update_doc_info_ps_PortType() {
    if (bpm_ecms_update_doc_info_ps_PortType == null)
      _initBpm_ecms_update_doc_info_psProxy();
    return bpm_ecms_update_doc_info_ps_PortType;
  }
  
  public Bpm.UpdateDocInfo.UpdateDocInfoResultType update_doc_info(Bpm.UpdateDocInfo.UpdateDocInfoType parameters) throws java.rmi.RemoteException{
    if (bpm_ecms_update_doc_info_ps_PortType == null)
      _initBpm_ecms_update_doc_info_psProxy();
    return bpm_ecms_update_doc_info_ps_PortType.update_doc_info(parameters);
  }
  
  
}