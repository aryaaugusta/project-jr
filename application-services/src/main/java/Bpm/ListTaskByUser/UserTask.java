package Bpm.ListTaskByUser;

import java.io.Serializable;
import java.math.BigInteger;

public class UserTask implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nomorPermohonan;
	private String kodeCabang;
	private String createdBy;
	private String creationDate;
	private String lastUpdatedBy;
	private String lastUpdateDate;
	private String idGUID;
	private String pengajuanType;
	private String kodeKantor;
	private String otherInfo1;
	private String otherInfo2;
	private String otherInfo3;
	private String otherInfo4;
	private String otherInfo5;
	private String role;
	private String taskTitle;
	private BigInteger taskNumber;
	
	public UserTask() {
		// TODO Auto-generated constructor stub
	}
	
	
	public String getNomorPermohonan() {
		return nomorPermohonan;
	}
	public void setNomorPermohonan(String nomorPermohonan) {
		this.nomorPermohonan = nomorPermohonan;
	}
	public String getKodeCabang() {
		return kodeCabang;
	}
	public void setKodeCabang(String kodeCabang) {
		this.kodeCabang = kodeCabang;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getIdGUID() {
		return idGUID;
	}
	public void setIdGUID(String idGUID) {
		this.idGUID = idGUID;
	}
	public String getPengajuanType() {
		return pengajuanType;
	}
	public void setPengajuanType(String pengajuanType) {
		this.pengajuanType = pengajuanType;
	}
	public String getKodeKantor() {
		return kodeKantor;
	}
	public void setKodeKantor(String kodeKantor) {
		this.kodeKantor = kodeKantor;
	}
	public String getOtherInfo1() {
		return otherInfo1;
	}
	public void setOtherInfo1(String otherInfo1) {
		this.otherInfo1 = otherInfo1;
	}
	public String getOtherInfo2() {
		return otherInfo2;
	}
	public void setOtherInfo2(String otherInfo2) {
		this.otherInfo2 = otherInfo2;
	}
	public String getOtherInfo3() {
		return otherInfo3;
	}
	public void setOtherInfo3(String otherInfo3) {
		this.otherInfo3 = otherInfo3;
	}
	public String getOtherInfo4() {
		return otherInfo4;
	}
	public void setOtherInfo4(String otherInfo4) {
		this.otherInfo4 = otherInfo4;
	}
	public String getOtherInfo5() {
		return otherInfo5;
	}
	public void setOtherInfo5(String otherInfo5) {
		this.otherInfo5 = otherInfo5;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getTaskTitle() {
		return taskTitle;
	}
	public void setTaskTitle(String taskTitle) {
		this.taskTitle = taskTitle;
	}
	public BigInteger getTaskNumber() {
		return taskNumber;
	}
	public void setTaskNumber(BigInteger taskNumber) {
		this.taskNumber = taskNumber;
	}
	
	
	

}
