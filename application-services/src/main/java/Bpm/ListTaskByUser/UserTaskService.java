package Bpm.ListTaskByUser;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis.types.PositiveInteger;

import Bpm.InstanceInfo.InstanceInfo;
import Bpm.QueryTaskBpm.Bpm_query_taskResponseType;
import Bpm.QueryTaskBpm.ClauseType;
import Bpm.QueryTaskBpm.ColumnType;
import Bpm.QueryTaskBpm.CredentialType;
import Bpm.QueryTaskBpm.PredicateType;
import Bpm.QueryTaskBpm.QueryTaskServiceImpl;
import Bpm.QueryTaskBpm.TaskType;
import Bpm.QueryTaskByUser.QueryTaskByUserSvcImpl;
import Bpm.SelectInstanceId.SelectInstanceIdSvcImpl;
import Bpm.SelectInstanceIdCreate.SelectInstanceIdCreateSvcImpl;
import Bpm.SelectInstanceIdCreateSchema.Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid;
import Bpm.SelectInstanceIdSchema.Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID;
import Bpm.SelectTask.SelectTaskSvcImpl;
import Bpm.SelectTaskPayload.BpmInstanceInfo;
import Bpm.SelectTaskPayload.Bpm_select_taskSelect_idKecelakaan_task;
import Bpm.UpdateAndInsertTask.Bpm_update_task_payloadRequest;
import Bpm.UpdateAndInsertTask.Bpm_update_task_payloadResponse;
import Bpm.UpdateAndInsertTask.PayloadType;
import Bpm.UpdateAndInsertTask.UpdateAndInsertTaskServiceImpl;




public class UserTaskService {
	
private static String processInstanceId;



public static List<UserTask> getUserTasks(String login, String password) throws RemoteException{
		
		Bpm.QueryTaskBpm.Bpm_query_taskRequest req = new Bpm.QueryTaskBpm.Bpm_query_taskRequest();
		String[] valueList = new String[1];
		valueList[0] = "ASSIGNED";
		
		CredentialType credentialType = new CredentialType();
		credentialType.setLogin(login);
		credentialType.setPassword(password);
		//---------------------------------
		
		ColumnType col = new ColumnType();
		ClauseType[] clause = new ClauseType[1];
		ClauseType clauseItem = new ClauseType();

		
		col.setColumnName("state");
		System.out.println("Column Name : " + col.getColumnName());
		clauseItem.setColumn(col);

		clauseItem.setOperator("in");
		clauseItem.setValueList(valueList);
		
		clause[0] = clauseItem;
  		
		PredicateType predicateType = new PredicateType();
		predicateType.setAssignmentFilter("GROUP");
		predicateType.setPredicate(clause);
		
		req.setCredential(credentialType);
		req.setPredicate(predicateType);
		req.setOptionalInfo("Payload");
		
		Bpm_query_taskResponseType res = new Bpm_query_taskResponseType();
		res = QueryTaskServiceImpl.bpmQueryAllAsigned(req);
		
		List<TaskType> taskList = new ArrayList<TaskType>();
		
		for (TaskType task : res.getTaskList()) {
			taskList.add(task);
			System.out.println(task.getTitle());
		}
		
		
	
		
		List<UserTask> listTask = new ArrayList<>();
		
		try{
						
			for (TaskType o : res.getTaskList()){
				UserTask userTask = new UserTask();
				
				userTask.setNomorPermohonan(o.getPayload().getInstanceInfo().getNomorPermohonan());
				userTask.setKodeCabang(o.getPayload().getInstanceInfo().getKodeCabang());
				userTask.setCreatedBy(o.getPayload().getInstanceInfo().getCreatedBy());
				userTask.setCreationDate(o.getPayload().getInstanceInfo().getCreationDate());
				userTask.setLastUpdatedBy(o.getPayload().getInstanceInfo().getLastUpdatedBy());
				userTask.setLastUpdateDate(o.getPayload().getInstanceInfo().getLastUpdatedDate());
				userTask.setIdGUID(o.getPayload().getInstanceInfo().getIdGUID());
				userTask.setPengajuanType(o.getPayload().getInstanceInfo().getPengajuanType());
				userTask.setKodeKantor(o.getPayload().getInstanceInfo().getKodeKantor());
				userTask.setOtherInfo1(o.getPayload().getInstanceInfo().getOtherInfo1());
				userTask.setOtherInfo2(o.getPayload().getInstanceInfo().getOtherInfo2());
				userTask.setOtherInfo3(o.getPayload().getInstanceInfo().getOtherInfo3());
				userTask.setOtherInfo4(o.getPayload().getInstanceInfo().getOtherInfo4());
				userTask.setOtherInfo5(o.getPayload().getInstanceInfo().getOtherInfo5());
				userTask.setRole(o.getSystemAttributes().getSwimlaneRole());
				userTask.setTaskTitle(o.getTitle());
				userTask.setTaskNumber(o.getSystemAttributes().getTaskNumber());
				
				System.out.println("ID Kecelakaan : " + userTask.getNomorPermohonan());
				System.out.println("Task Number : " + userTask.getTaskNumber());
				
				
				listTask.add(userTask);
			}
			
			System.out.println("JumlahTask : " + listTask.size());
			
	
			} catch (Exception e){
				e.printStackTrace();
				System.out.println("Data belum di proses BPM atau tidak termasuk task Anda");
			}
		
	
		return listTask;
		
	}

public static List<UserTask> getUserTasksByUser(String login, String password, String user) throws RemoteException{
	
	Bpm.QueryTaskByUser.Bpm_query_taskRequest req = new Bpm.QueryTaskByUser.Bpm_query_taskRequest();
	String[] valueList = new String[1];
	valueList[0] = "ASSIGNED";
	
	Bpm.QueryTaskByUser.CredentialType credentialType = new Bpm.QueryTaskByUser.CredentialType();
	credentialType.setLogin(login);
	credentialType.setPassword(password);
	credentialType.setOnBehalfOfUser(user);
	//---------------------------------
	
	Bpm.QueryTaskByUser.ColumnType col = new Bpm.QueryTaskByUser.ColumnType();
	Bpm.QueryTaskByUser.ClauseType[] clause = new Bpm.QueryTaskByUser.ClauseType[1];
	Bpm.QueryTaskByUser.ClauseType clauseItem = new Bpm.QueryTaskByUser.ClauseType();

	
	col.setColumnName("state");
	System.out.println("Column Name : " + col.getColumnName());
	clauseItem.setColumn(col);

	clauseItem.setOperator("in");
	clauseItem.setValueList(valueList);
	
	clause[0] = clauseItem;
		
	Bpm.QueryTaskByUser.PredicateType predicateType = new Bpm.QueryTaskByUser.PredicateType();
	predicateType.setAssignmentFilter("GROUP");
	predicateType.setPredicate(clause);
	
	req.setCredential(credentialType);
	req.setPredicate(predicateType);
	req.setOptionalInfo("Payload");
	
	Bpm.QueryTaskByUser.Bpm_query_taskResponseType res = new Bpm.QueryTaskByUser.Bpm_query_taskResponseType();
	res = QueryTaskByUserSvcImpl.bpmQueryAllAsigned(req);
	
	List<Bpm.QueryTaskByUser.TaskType> taskList = new ArrayList<Bpm.QueryTaskByUser.TaskType>();
	
	for (Bpm.QueryTaskByUser.TaskType task : res.getTaskList()) {
		taskList.add(task);
		System.out.println(task.getTitle());
	}
	
	

	
	List<UserTask> listTask = new ArrayList<>();
	
	try{
					
		for (Bpm.QueryTaskByUser.TaskType o : res.getTaskList()){
			UserTask userTask = new UserTask();
			
			userTask.setNomorPermohonan(o.getPayload().getInstanceInfo().getNomorPermohonan());
			userTask.setKodeCabang(o.getPayload().getInstanceInfo().getKodeCabang());
			userTask.setCreatedBy(o.getPayload().getInstanceInfo().getCreatedBy());
			userTask.setCreationDate(o.getPayload().getInstanceInfo().getCreationDate());
			userTask.setLastUpdatedBy(o.getPayload().getInstanceInfo().getLastUpdatedBy());
			userTask.setLastUpdateDate(o.getPayload().getInstanceInfo().getLastUpdatedDate());
			userTask.setIdGUID(o.getPayload().getInstanceInfo().getIdGUID());
			userTask.setPengajuanType(o.getPayload().getInstanceInfo().getPengajuanType());
			userTask.setKodeKantor(o.getPayload().getInstanceInfo().getKodeKantor());
			userTask.setOtherInfo1(o.getPayload().getInstanceInfo().getOtherInfo1());
			userTask.setOtherInfo2(o.getPayload().getInstanceInfo().getOtherInfo2());
			userTask.setOtherInfo3(o.getPayload().getInstanceInfo().getOtherInfo3());
			userTask.setOtherInfo4(o.getPayload().getInstanceInfo().getOtherInfo4());
			userTask.setOtherInfo5(o.getPayload().getInstanceInfo().getOtherInfo5());
			userTask.setRole(o.getSystemAttributes().getSwimlaneRole());
			userTask.setTaskTitle(o.getTitle());
			userTask.setTaskNumber(o.getSystemAttributes().getTaskNumber());
			
			System.out.println("ID Kecelakaan : " + userTask.getNomorPermohonan());
			System.out.println("Task Number : " + userTask.getTaskNumber());
			
			
			listTask.add(userTask);
		}
		
		System.out.println("JumlahTask : " + listTask.size());
		

		} catch (Exception e){
			e.printStackTrace();
			System.out.println("Data belum di proses BPM atau tidak termasuk task Anda");
		}
	

	return listTask;
	
}

/*	public static UserTask getUserTaskByIdKecelakaan(String login, String password, String idKecelakaan) throws RemoteException {
		
		List<UserTask> listTask = UserTaskService.getUserTasks(login, password);

		UserTask userTask = new UserTask();
		
		for(UserTask u : listTask){
			
			if(u.getNomorPermohonan().equalsIgnoreCase(idKecelakaan)){
				userTask = u;
			} else {
				System.out.println("Data belum di proses BPM atau ID Kecelakaan tidak ditemukan");
			}
			
		}
		
		return userTask;
	}*/
	
	public static void updateUserTask(String login, String password, String noBerkas, String taskTitle, String outcome, String action, String kodeCabang, String lastUpdatedBy, String lastUpdatedDate, String idKecelakaan, String user) throws RemoteException{
		
	
		UserTask uTask = new UserTask();
		String titleSearch = null;
		if(taskTitle.equalsIgnoreCase("Entry Data Pengajuan")){
			titleSearch = "Entry Data Pengajuan";
		}else if(taskTitle.equalsIgnoreCase("Identifikasi Kelengkapan dan Keabsahan Berkas")){
			titleSearch = "Entry Data Pengajuan";
		}else if(taskTitle.equalsIgnoreCase("Entry Otorisasi Pengajuan Berkas")){
			titleSearch = "Identifikasi Kelengkapan dan Keabsahan Berkas";
		}else if(taskTitle.equalsIgnoreCase("Entry Penyelesaian Berkas")){
			titleSearch = "Entry Otorisasi Pengajuan Berkas";
		}else if(taskTitle.equalsIgnoreCase("Entry Verifikasi Berkas")){
			titleSearch = "Entry Penyelesaian Berkas";
		}else if(taskTitle.equalsIgnoreCase("Entry Pengesahan Berkas")){
			titleSearch = "Entry Verifikasi Berkas";
		}else if(taskTitle.equalsIgnoreCase("Entry Jurnal Pelayanan Pembayaran")){
			titleSearch = "Entry Pengesahan Berkas";
		}
		
		String instanceId = getInstanceId(titleSearch, noBerkas);
		if (action.equalsIgnoreCase("edit")){
			uTask = getTaskByInstanceId(login, password, taskTitle, instanceId);
			uTask.setKodeCabang(kodeCabang);
			uTask.setCreatedBy(lastUpdatedBy);
			uTask.setCreationDate(uTask.getLastUpdateDate());
			uTask.setLastUpdatedBy(lastUpdatedBy);
			uTask.setLastUpdateDate(lastUpdatedDate);
            uTask.setOtherInfo1(taskTitle);
            uTask.setOtherInfo2(processInstanceId);
            uTask.setOtherInfo3(idKecelakaan);
		}else{
			uTask = getTaskByInstanceId(login, password, taskTitle, instanceId);
			String title = null;
//			if(taskTitle.equalsIgnoreCase("Entry Data Pengajuan")){
//				title = "Identifikasi Kelengkapan dan Keabsahan Berkas";
//			}else if(taskTitle.equalsIgnoreCase("Identifikasi Kelengkapan dan Keabsahan Berkas")){
//				title = "Entry Otorisasi Pengajuan Berkas";
//			}else if(taskTitle.equalsIgnoreCase("Entry Otorisasi Pengajuan Berkas")){
//				title = "Entry Penyelesaian Berkas";
//			}else if(taskTitle.equalsIgnoreCase("Entry Penyelesaian Berkas")){
//				title = "Entry Verifikasi Berkas";
//			}else if(taskTitle.equalsIgnoreCase("Entry Verifikasi Berkas")){
//				title = "Entry Pengesahan Berkas";
//			}else if(taskTitle.equalsIgnoreCase("Entry Pengesahan Berkas")){
//				title = "Entry Jurnal Pelayanan Pembayaran";
//			}else if(taskTitle.equalsIgnoreCase("Entry Jurnal Pelayanan Pembayaran")){
//				title = "End";
//			}
			uTask.setCreatedBy(lastUpdatedBy);
			uTask.setCreationDate(uTask.getLastUpdateDate());
			uTask.setLastUpdatedBy(lastUpdatedBy);
			uTask.setLastUpdateDate(lastUpdatedDate);
			uTask.setOtherInfo1(taskTitle);
			uTask.setOtherInfo2(processInstanceId);
			uTask.setOtherInfo3(idKecelakaan);
		}
		
		updateTask(login, password, user, outcome, uTask, action);
		
	}
	
	public static String getInstanceId(String taskTitle, String idKecelakaan) throws RemoteException{
		
		Bpm_select_taskSelect_idKecelakaan_task param = new Bpm_select_taskSelect_idKecelakaan_task();
		param.setIdKecelakaan(idKecelakaan);
		param.setTask(taskTitle);
		BpmInstanceInfo[] instanceInfo = SelectTaskSvcImpl.bpm_select_taskSelect(param);
		UserTask uTask = new UserTask();
		List<UserTask> tasks = new ArrayList<UserTask>();
		
		for (BpmInstanceInfo instance : instanceInfo){
			UserTask userTask = new UserTask();
			userTask.setNomorPermohonan(instance.getNomorPermohonan());
			userTask.setKodeCabang(instance.getKodeCabang());
			userTask.setCreatedBy(instance.getColumn1());
			userTask.setCreationDate(instance.getCreationDate());
			userTask.setLastUpdatedBy(instance.getLastUpdatedBy());
			userTask.setLastUpdateDate(instance.getLastUpdatedDate());
			userTask.setIdGUID(instance.getIdGuid());
			userTask.setPengajuanType(instance.getColumn2());
			userTask.setKodeKantor(instance.getColumn3());
			userTask.setOtherInfo1(instance.getOtherInfo1());
			userTask.setOtherInfo2(instance.getOtherInfo2());
			userTask.setOtherInfo3(instance.getOtherInfo3());
			userTask.setOtherInfo4(instance.getOtherInfo4());
			userTask.setOtherInfo5(instance.getOtherInfo5());
			tasks.add(userTask);
		}
		processInstanceId = tasks.get(0).getOtherInfo2();
		String instanceId = tasks.get(0).getOtherInfo2().substring(5);
		return instanceId;
		
	}
	
	public static UserTask getTaskByInstanceId(String login, String password, String taskTitle, String instanceId){
		UserTask userTask = new UserTask();
		Bpm.QueryTaskBpm.Bpm_query_taskRequest req = new Bpm.QueryTaskBpm.Bpm_query_taskRequest();
		
		
		CredentialType credentialType = new CredentialType();
		credentialType.setLogin(login);
		credentialType.setPassword(password);
		//---------------------------------
		

		ClauseType[] clause = new ClauseType[2];
		ClauseType clauseItem = new ClauseType();
		ClauseType clauseItem2 = new ClauseType();
		
		ColumnType col1 = new ColumnType();
		ColumnType col2 = new ColumnType();
		col1.setColumnName("instanceId");
		col2.setColumnName("title");
		
		String[] valueList1 = new String[1];
		valueList1[0] = instanceId;
		String[] valueList2 = new String[1];
		valueList2[0] = taskTitle;

		clauseItem.setColumn(col1);
		clauseItem.setOperator("in");
		clauseItem.setValueList(valueList1);
		
		clauseItem2.setColumn(col2);
		clauseItem2.setOperator("in");
		clauseItem2.setValueList(valueList2);
		
		clause[0] = clauseItem;
		clause[1] = clauseItem2;
  		
		PredicateType predicateType = new PredicateType();
		predicateType.setAssignmentFilter("All");
		predicateType.setPredicate(clause);
		
		req.setCredential(credentialType);
		req.setPredicate(predicateType);
		req.setOptionalInfo("Payload");
		
		Bpm_query_taskResponseType res = new Bpm_query_taskResponseType();
		try {
			res = QueryTaskServiceImpl.bpmQueryAllAsigned(req);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		List<TaskType> taskList = new ArrayList<TaskType>();
		
		for (TaskType task : res.getTaskList()) {
			taskList.add(task);
			System.out.println(task.getTitle());
		}
		
		
	
		
		List<UserTask> listTask = new ArrayList<>();
		
		try{
						
			for (TaskType o : res.getTaskList()){
				UserTask task = new UserTask();
				
				task.setNomorPermohonan(o.getPayload().getInstanceInfo().getNomorPermohonan());
				task.setKodeCabang(o.getPayload().getInstanceInfo().getKodeCabang());
				task.setCreatedBy(o.getPayload().getInstanceInfo().getCreatedBy());
				task.setCreationDate(o.getPayload().getInstanceInfo().getCreationDate());
				task.setLastUpdatedBy(o.getPayload().getInstanceInfo().getLastUpdatedBy());
				task.setLastUpdateDate(o.getPayload().getInstanceInfo().getLastUpdatedDate());
				task.setIdGUID(o.getPayload().getInstanceInfo().getIdGUID());
				task.setPengajuanType(o.getPayload().getInstanceInfo().getPengajuanType());
				task.setKodeKantor(o.getPayload().getInstanceInfo().getKodeKantor());
				task.setOtherInfo1(o.getPayload().getInstanceInfo().getOtherInfo1());
				task.setOtherInfo2(o.getPayload().getInstanceInfo().getOtherInfo2());
				task.setOtherInfo3(o.getPayload().getInstanceInfo().getOtherInfo3());
				task.setOtherInfo4(o.getPayload().getInstanceInfo().getOtherInfo4());
				task.setOtherInfo5(o.getPayload().getInstanceInfo().getOtherInfo5());
				task.setRole(o.getSystemAttributes().getSwimlaneRole());
				task.setTaskTitle(o.getTitle());
				task.setTaskNumber(o.getSystemAttributes().getTaskNumber());
				
				System.out.println("ID Kecelakaan : " + task.getNomorPermohonan());
				System.out.println("Task Number : " + task.getTaskNumber());
				
				
				listTask.add(task);
			}
			
			System.out.println("JumlahTask : " + listTask.size());
			System.out.println("User Task Number Index 0 : " + listTask.get(0).getTaskNumber());
			
	
			} catch (Exception e){
				e.printStackTrace();
				System.out.println("Data belum di proses BPM atau tidak termasuk task Anda");
			}
		
		userTask = listTask.get(0);
		System.out.println("User Task Number By Id : " + userTask.getTaskNumber());
		
		return userTask;
		
		
	}
	
	public static void updateTask(String login, String password, String user, String outcome, UserTask userTask, String action){
		Bpm_update_task_payloadRequest req = new Bpm_update_task_payloadRequest();
		
		Bpm.UpdateAndInsertTask.CredentialType credential = new Bpm.UpdateAndInsertTask.CredentialType();
		credential.setLogin(login);
		credential.setPassword(password);
		credential.setIdentityContext("");
		credential.setOnBehalfOfUser(user);
		
		String taskTitle = new String();
		
//		if(userTask.getOtherInfo1().equalsIgnoreCase("Entry Data Pengajuan")){
//			taskTitle = "Identifikasi Kelengkapan dan Keabsahan Berkas";
//		}else if(userTask.getOtherInfo1().equalsIgnoreCase("Identifikasi Kelengkapan dan Keabsahan Berkas")){
//			taskTitle = "Entry Otorisasi Pengajuan Berkas";
//		}else if(userTask.getOtherInfo1().equalsIgnoreCase("Entry Otorisasi Pengajuan Berkas")){
//			taskTitle = "Entry Penyelesaian Berkas";
//		}else if(userTask.getOtherInfo1().equalsIgnoreCase("Entry Penyelesaian Berkas")){
//			taskTitle = "Entry Verifikasi Berkas";
//		}else if(userTask.getOtherInfo1().equalsIgnoreCase("Entry Verifikasi Berkas")){
//			taskTitle = "Entry Pengesahan Berkas";
//		}else if(userTask.getOtherInfo1().equalsIgnoreCase("Entry Pengesahan Berkas")){
//			taskTitle = "Entry Jurnal Pelayanan Pembayaran";
//		}
		
		
		PayloadType payload = new PayloadType();
		InstanceInfo instance = new InstanceInfo();
		instance.setNomorPermohonan(userTask.getNomorPermohonan());
		instance.setKodeCabang(userTask.getKodeCabang());
		instance.setCreatedBy(userTask.getCreatedBy());
		instance.setCreationDate(userTask.getCreationDate());
		instance.setLastUpdatedBy(userTask.getLastUpdatedBy());
		instance.setLastUpdatedDate(userTask.getLastUpdateDate());
		instance.setIdGUID(userTask.getIdGUID());
		instance.setPengajuanType(userTask.getPengajuanType());
		instance.setKodeKantor(userTask.getKodeKantor());
//		if(action.equalsIgnoreCase("edit")){
//			instance.setOtherInfo1(userTask.getOtherInfo1());
//		}else{
		instance.setOtherInfo1(userTask.getOtherInfo1());
//		}
		instance.setOtherInfo2(userTask.getOtherInfo2());
		instance.setOtherInfo3(userTask.getOtherInfo3());
		instance.setOtherInfo4(userTask.getOtherInfo4());
		instance.setOtherInfo5(userTask.getOtherInfo5());
		
		payload.setInstanceInfo(instance);
		
		req.setCredential(credential);
		if(action.equalsIgnoreCase("edit")){
			req.setOutcome("");
		}else{
		req.setOutcome(outcome);
		}
		req.setTaskNumber(userTask.getTaskNumber());
		req.setPayload(payload);
		
		try {
			Bpm_update_task_payloadResponse res = UpdateAndInsertTaskServiceImpl.bpmUpdateInsertTask(req);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
public static String getInstanceIdPelimpahan(String noBerkas, String lastInstanceId) throws RemoteException{
		
		Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID param = new Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID();
		param.setNOMOR_PERMOHONAN(noBerkas);
		param.setINSTANCE_ID(lastInstanceId);
		Bpm.SelectInstanceIdSchema.BpmInstanceInfo[] instanceInfo = SelectInstanceIdSvcImpl.bpm_select_instanceidSelect(param);
		UserTask uTask = new UserTask();
		List<UserTask> tasks = new ArrayList<UserTask>();
		
		for (Bpm.SelectInstanceIdSchema.BpmInstanceInfo instance : instanceInfo){
			UserTask userTask = new UserTask();
			userTask.setNomorPermohonan(instance.getNomorPermohonan());
			userTask.setKodeCabang(instance.getKodeCabang());
			userTask.setCreatedBy(instance.getColumn1());
			userTask.setCreationDate(instance.getCreationDate());
			userTask.setLastUpdatedBy(instance.getLastUpdatedBy());
			userTask.setLastUpdateDate(instance.getLastUpdatedDate());
			userTask.setIdGUID(instance.getIdGuid());
			userTask.setPengajuanType(instance.getColumn2());
			userTask.setKodeKantor(instance.getColumn3());
			userTask.setOtherInfo1(instance.getOtherInfo1());
			userTask.setOtherInfo2(instance.getOtherInfo2());
			userTask.setOtherInfo3(instance.getOtherInfo3());
			userTask.setOtherInfo4(instance.getOtherInfo4());
			userTask.setOtherInfo5(instance.getOtherInfo5());
			tasks.add(userTask);
		}
		processInstanceId = tasks.get(0).getOtherInfo2();
		String instanceId = tasks.get(0).getOtherInfo2();
		return instanceId;
		
	}

public static String getInstanceIdCreatePelimpahan(String noBerkas, String creationDate, String lastInstanceId) throws RemoteException{
	
	Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid param = new Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid();
	param.setNomorPermohonan(noBerkas);
	param.setCreationDate(creationDate);
	param.setInstanceid(lastInstanceId);
	Bpm.SelectInstanceIdCreateSchema.BpmInstanceInfo[] instanceInfo = SelectInstanceIdCreateSvcImpl.bpm_select_instanceidSelect(param);
	UserTask uTask = new UserTask();
	List<UserTask> tasks = new ArrayList<UserTask>();
	
	for (Bpm.SelectInstanceIdCreateSchema.BpmInstanceInfo instance : instanceInfo){
		UserTask userTask = new UserTask();
		userTask.setNomorPermohonan(instance.getNomorPermohonan());
		userTask.setKodeCabang(instance.getKodeCabang());
		userTask.setCreatedBy(instance.getColumn1());
		userTask.setCreationDate(instance.getCreationDate());
		userTask.setLastUpdatedBy(instance.getLastUpdatedBy());
		userTask.setLastUpdateDate(instance.getLastUpdatedDate());
		userTask.setIdGUID(instance.getIdGuid());
		userTask.setPengajuanType(instance.getColumn2());
		userTask.setKodeKantor(instance.getColumn3());
		userTask.setOtherInfo1(instance.getOtherInfo1());
		userTask.setOtherInfo2(instance.getOtherInfo2());
		userTask.setOtherInfo3(instance.getOtherInfo3());
		userTask.setOtherInfo4(instance.getOtherInfo4());
		userTask.setOtherInfo5(instance.getOtherInfo5());
		tasks.add(userTask);
	}
	processInstanceId = tasks.get(0).getOtherInfo2();
	String instanceId = tasks.get(0).getOtherInfo2();
	return instanceId;
	
}
	
	

	public static String getProcessInstanceId() {
		return processInstanceId;
	}

	public static void setProcessInstanceId(String processInstanceId) {
		UserTaskService.processInstanceId = processInstanceId;
	}
	

}
