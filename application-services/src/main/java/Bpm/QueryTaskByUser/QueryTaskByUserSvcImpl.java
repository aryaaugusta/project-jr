package Bpm.QueryTaskByUser;

import java.rmi.RemoteException;

import Bpm.QueryTaskByUser.Bpm_query_taskRequest;
import Bpm.QueryTaskByUser.Bpm_query_taskResponseType;
import Bpm.QueryTaskByUser.Bpm_query_task_wsdlProxy;
import Bpm.QueryTaskByUser.Bpm_query_task_wsdl_PortType;

public class QueryTaskByUserSvcImpl {
	public static Bpm_query_taskResponseType bpmQueryAllAsigned(
			Bpm_query_taskRequest parameters) throws RemoteException {

		Bpm_query_task_wsdlProxy service = new Bpm_query_task_wsdlProxy();
		Bpm_query_task_wsdl_PortType port = service
				.getBpm_query_task_wsdl_PortType();
		return port.bpm_query_task(parameters);
	}

}
