/**
 * TaskType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.QueryTaskByUser;

public class TaskType  implements java.io.Serializable {
    private java.lang.String title;

    private java.lang.String ownerRole;

    private java.math.BigInteger priority;

    private java.lang.String processInfo;

    private Bpm.QueryTaskByUser.SystemAttributesType systemAttributes;

    private java.lang.String systemMessageAttributes;

    private java.lang.String callback;

    private Bpm.QueryTaskByUser.ScaType sca;

    private java.lang.String applicationContext;

    private java.lang.String taskDefinitionId;

    private java.lang.String mdsLabel;

    private java.lang.String customAttributes;

    private Bpm.QueryTaskByUser.PayloadType payload;

    public TaskType() {
    }

    public TaskType(
           java.lang.String title,
           java.lang.String ownerRole,
           java.math.BigInteger priority,
           java.lang.String processInfo,
           Bpm.QueryTaskByUser.SystemAttributesType systemAttributes,
           java.lang.String systemMessageAttributes,
           java.lang.String callback,
           Bpm.QueryTaskByUser.ScaType sca,
           java.lang.String applicationContext,
           java.lang.String taskDefinitionId,
           java.lang.String mdsLabel,
           java.lang.String customAttributes,
           Bpm.QueryTaskByUser.PayloadType payload) {
           this.title = title;
           this.ownerRole = ownerRole;
           this.priority = priority;
           this.processInfo = processInfo;
           this.systemAttributes = systemAttributes;
           this.systemMessageAttributes = systemMessageAttributes;
           this.callback = callback;
           this.sca = sca;
           this.applicationContext = applicationContext;
           this.taskDefinitionId = taskDefinitionId;
           this.mdsLabel = mdsLabel;
           this.customAttributes = customAttributes;
           this.payload = payload;
    }


    /**
     * Gets the title value for this TaskType.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this TaskType.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the ownerRole value for this TaskType.
     * 
     * @return ownerRole
     */
    public java.lang.String getOwnerRole() {
        return ownerRole;
    }


    /**
     * Sets the ownerRole value for this TaskType.
     * 
     * @param ownerRole
     */
    public void setOwnerRole(java.lang.String ownerRole) {
        this.ownerRole = ownerRole;
    }


    /**
     * Gets the priority value for this TaskType.
     * 
     * @return priority
     */
    public java.math.BigInteger getPriority() {
        return priority;
    }


    /**
     * Sets the priority value for this TaskType.
     * 
     * @param priority
     */
    public void setPriority(java.math.BigInteger priority) {
        this.priority = priority;
    }


    /**
     * Gets the processInfo value for this TaskType.
     * 
     * @return processInfo
     */
    public java.lang.String getProcessInfo() {
        return processInfo;
    }


    /**
     * Sets the processInfo value for this TaskType.
     * 
     * @param processInfo
     */
    public void setProcessInfo(java.lang.String processInfo) {
        this.processInfo = processInfo;
    }


    /**
     * Gets the systemAttributes value for this TaskType.
     * 
     * @return systemAttributes
     */
    public Bpm.QueryTaskByUser.SystemAttributesType getSystemAttributes() {
        return systemAttributes;
    }


    /**
     * Sets the systemAttributes value for this TaskType.
     * 
     * @param systemAttributes
     */
    public void setSystemAttributes(Bpm.QueryTaskByUser.SystemAttributesType systemAttributes) {
        this.systemAttributes = systemAttributes;
    }


    /**
     * Gets the systemMessageAttributes value for this TaskType.
     * 
     * @return systemMessageAttributes
     */
    public java.lang.String getSystemMessageAttributes() {
        return systemMessageAttributes;
    }


    /**
     * Sets the systemMessageAttributes value for this TaskType.
     * 
     * @param systemMessageAttributes
     */
    public void setSystemMessageAttributes(java.lang.String systemMessageAttributes) {
        this.systemMessageAttributes = systemMessageAttributes;
    }


    /**
     * Gets the callback value for this TaskType.
     * 
     * @return callback
     */
    public java.lang.String getCallback() {
        return callback;
    }


    /**
     * Sets the callback value for this TaskType.
     * 
     * @param callback
     */
    public void setCallback(java.lang.String callback) {
        this.callback = callback;
    }


    /**
     * Gets the sca value for this TaskType.
     * 
     * @return sca
     */
    public Bpm.QueryTaskByUser.ScaType getSca() {
        return sca;
    }


    /**
     * Sets the sca value for this TaskType.
     * 
     * @param sca
     */
    public void setSca(Bpm.QueryTaskByUser.ScaType sca) {
        this.sca = sca;
    }


    /**
     * Gets the applicationContext value for this TaskType.
     * 
     * @return applicationContext
     */
    public java.lang.String getApplicationContext() {
        return applicationContext;
    }


    /**
     * Sets the applicationContext value for this TaskType.
     * 
     * @param applicationContext
     */
    public void setApplicationContext(java.lang.String applicationContext) {
        this.applicationContext = applicationContext;
    }


    /**
     * Gets the taskDefinitionId value for this TaskType.
     * 
     * @return taskDefinitionId
     */
    public java.lang.String getTaskDefinitionId() {
        return taskDefinitionId;
    }


    /**
     * Sets the taskDefinitionId value for this TaskType.
     * 
     * @param taskDefinitionId
     */
    public void setTaskDefinitionId(java.lang.String taskDefinitionId) {
        this.taskDefinitionId = taskDefinitionId;
    }


    /**
     * Gets the mdsLabel value for this TaskType.
     * 
     * @return mdsLabel
     */
    public java.lang.String getMdsLabel() {
        return mdsLabel;
    }


    /**
     * Sets the mdsLabel value for this TaskType.
     * 
     * @param mdsLabel
     */
    public void setMdsLabel(java.lang.String mdsLabel) {
        this.mdsLabel = mdsLabel;
    }


    /**
     * Gets the customAttributes value for this TaskType.
     * 
     * @return customAttributes
     */
    public java.lang.String getCustomAttributes() {
        return customAttributes;
    }


    /**
     * Sets the customAttributes value for this TaskType.
     * 
     * @param customAttributes
     */
    public void setCustomAttributes(java.lang.String customAttributes) {
        this.customAttributes = customAttributes;
    }


    /**
     * Gets the payload value for this TaskType.
     * 
     * @return payload
     */
    public Bpm.QueryTaskByUser.PayloadType getPayload() {
        return payload;
    }


    /**
     * Sets the payload value for this TaskType.
     * 
     * @param payload
     */
    public void setPayload(Bpm.QueryTaskByUser.PayloadType payload) {
        this.payload = payload;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TaskType)) return false;
        TaskType other = (TaskType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.ownerRole==null && other.getOwnerRole()==null) || 
             (this.ownerRole!=null &&
              this.ownerRole.equals(other.getOwnerRole()))) &&
            ((this.priority==null && other.getPriority()==null) || 
             (this.priority!=null &&
              this.priority.equals(other.getPriority()))) &&
            ((this.processInfo==null && other.getProcessInfo()==null) || 
             (this.processInfo!=null &&
              this.processInfo.equals(other.getProcessInfo()))) &&
            ((this.systemAttributes==null && other.getSystemAttributes()==null) || 
             (this.systemAttributes!=null &&
              this.systemAttributes.equals(other.getSystemAttributes()))) &&
            ((this.systemMessageAttributes==null && other.getSystemMessageAttributes()==null) || 
             (this.systemMessageAttributes!=null &&
              this.systemMessageAttributes.equals(other.getSystemMessageAttributes()))) &&
            ((this.callback==null && other.getCallback()==null) || 
             (this.callback!=null &&
              this.callback.equals(other.getCallback()))) &&
            ((this.sca==null && other.getSca()==null) || 
             (this.sca!=null &&
              this.sca.equals(other.getSca()))) &&
            ((this.applicationContext==null && other.getApplicationContext()==null) || 
             (this.applicationContext!=null &&
              this.applicationContext.equals(other.getApplicationContext()))) &&
            ((this.taskDefinitionId==null && other.getTaskDefinitionId()==null) || 
             (this.taskDefinitionId!=null &&
              this.taskDefinitionId.equals(other.getTaskDefinitionId()))) &&
            ((this.mdsLabel==null && other.getMdsLabel()==null) || 
             (this.mdsLabel!=null &&
              this.mdsLabel.equals(other.getMdsLabel()))) &&
            ((this.customAttributes==null && other.getCustomAttributes()==null) || 
             (this.customAttributes!=null &&
              this.customAttributes.equals(other.getCustomAttributes()))) &&
            ((this.payload==null && other.getPayload()==null) || 
             (this.payload!=null &&
              this.payload.equals(other.getPayload())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getOwnerRole() != null) {
            _hashCode += getOwnerRole().hashCode();
        }
        if (getPriority() != null) {
            _hashCode += getPriority().hashCode();
        }
        if (getProcessInfo() != null) {
            _hashCode += getProcessInfo().hashCode();
        }
        if (getSystemAttributes() != null) {
            _hashCode += getSystemAttributes().hashCode();
        }
        if (getSystemMessageAttributes() != null) {
            _hashCode += getSystemMessageAttributes().hashCode();
        }
        if (getCallback() != null) {
            _hashCode += getCallback().hashCode();
        }
        if (getSca() != null) {
            _hashCode += getSca().hashCode();
        }
        if (getApplicationContext() != null) {
            _hashCode += getApplicationContext().hashCode();
        }
        if (getTaskDefinitionId() != null) {
            _hashCode += getTaskDefinitionId().hashCode();
        }
        if (getMdsLabel() != null) {
            _hashCode += getMdsLabel().hashCode();
        }
        if (getCustomAttributes() != null) {
            _hashCode += getCustomAttributes().hashCode();
        }
        if (getPayload() != null) {
            _hashCode += getPayload().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TaskType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "taskType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ownerRole");
        elemField.setXmlName(new javax.xml.namespace.QName("", "ownerRole"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("priority");
        elemField.setXmlName(new javax.xml.namespace.QName("", "priority"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "processInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "systemAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "systemAttributesType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemMessageAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "systemMessageAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("callback");
        elemField.setXmlName(new javax.xml.namespace.QName("", "callback"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sca");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "scaType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("applicationContext");
        elemField.setXmlName(new javax.xml.namespace.QName("", "applicationContext"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskDefinitionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taskDefinitionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mdsLabel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "mdsLabel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("", "customAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payload");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payload"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "payloadType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
