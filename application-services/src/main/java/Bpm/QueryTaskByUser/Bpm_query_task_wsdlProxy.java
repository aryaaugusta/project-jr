package Bpm.QueryTaskByUser;

public class Bpm_query_task_wsdlProxy implements Bpm.QueryTaskByUser.Bpm_query_task_wsdl_PortType {
  private String _endpoint = null;
  private Bpm.QueryTaskByUser.Bpm_query_task_wsdl_PortType bpm_query_task_wsdl_PortType = null;
  
  public Bpm_query_task_wsdlProxy() {
    _initBpm_query_task_wsdlProxy();
  }
  
  public Bpm_query_task_wsdlProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_query_task_wsdlProxy();
  }
  
  private void _initBpm_query_task_wsdlProxy() {
    try {
      bpm_query_task_wsdl_PortType = (new Bpm.QueryTaskByUser.Bpm_query_task_wsdl_ServiceLocator()).getbpm_query_task_wsdlSOAP();
      if (bpm_query_task_wsdl_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_query_task_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_query_task_wsdl_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_query_task_wsdl_PortType != null)
      ((javax.xml.rpc.Stub)bpm_query_task_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.QueryTaskByUser.Bpm_query_task_wsdl_PortType getBpm_query_task_wsdl_PortType() {
    if (bpm_query_task_wsdl_PortType == null)
      _initBpm_query_task_wsdlProxy();
    return bpm_query_task_wsdl_PortType;
  }
  
  public Bpm.QueryTaskByUser.Bpm_query_taskResponseType bpm_query_task(Bpm.QueryTaskByUser.Bpm_query_taskRequest parameters) throws java.rmi.RemoteException{
    if (bpm_query_task_wsdl_PortType == null)
      _initBpm_query_task_wsdlProxy();
    return bpm_query_task_wsdl_PortType.bpm_query_task(parameters);
  }
  
  
}