/**
 * PredicateType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.QueryTaskByUser;

public class PredicateType  implements java.io.Serializable {
    private java.lang.String assignmentFilter;

    private Bpm.QueryTaskByUser.ClauseType[] predicate;

    public PredicateType() {
    }

    public PredicateType(
           java.lang.String assignmentFilter,
           Bpm.QueryTaskByUser.ClauseType[] predicate) {
           this.assignmentFilter = assignmentFilter;
           this.predicate = predicate;
    }


    /**
     * Gets the assignmentFilter value for this PredicateType.
     * 
     * @return assignmentFilter
     */
    public java.lang.String getAssignmentFilter() {
        return assignmentFilter;
    }


    /**
     * Sets the assignmentFilter value for this PredicateType.
     * 
     * @param assignmentFilter
     */
    public void setAssignmentFilter(java.lang.String assignmentFilter) {
        this.assignmentFilter = assignmentFilter;
    }


    /**
     * Gets the predicate value for this PredicateType.
     * 
     * @return predicate
     */
    public Bpm.QueryTaskByUser.ClauseType[] getPredicate() {
        return predicate;
    }


    /**
     * Sets the predicate value for this PredicateType.
     * 
     * @param predicate
     */
    public void setPredicate(Bpm.QueryTaskByUser.ClauseType[] predicate) {
        this.predicate = predicate;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PredicateType)) return false;
        PredicateType other = (PredicateType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.assignmentFilter==null && other.getAssignmentFilter()==null) || 
             (this.assignmentFilter!=null &&
              this.assignmentFilter.equals(other.getAssignmentFilter()))) &&
            ((this.predicate==null && other.getPredicate()==null) || 
             (this.predicate!=null &&
              java.util.Arrays.equals(this.predicate, other.getPredicate())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAssignmentFilter() != null) {
            _hashCode += getAssignmentFilter().hashCode();
        }
        if (getPredicate() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getPredicate());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getPredicate(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PredicateType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "predicateType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assignmentFilter");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assignmentFilter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("predicate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "predicate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "clauseType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "clause"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
