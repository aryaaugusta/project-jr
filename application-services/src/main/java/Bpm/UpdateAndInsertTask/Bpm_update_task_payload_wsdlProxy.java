package Bpm.UpdateAndInsertTask;

public class Bpm_update_task_payload_wsdlProxy implements Bpm.UpdateAndInsertTask.Bpm_update_task_payload_wsdl_PortType {
  private String _endpoint = null;
  private Bpm.UpdateAndInsertTask.Bpm_update_task_payload_wsdl_PortType bpm_update_task_payload_wsdl_PortType = null;
  
  public Bpm_update_task_payload_wsdlProxy() {
    _initBpm_update_task_payload_wsdlProxy();
  }
  
  public Bpm_update_task_payload_wsdlProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_update_task_payload_wsdlProxy();
  }
  
  private void _initBpm_update_task_payload_wsdlProxy() {
    try {
      bpm_update_task_payload_wsdl_PortType = (new Bpm.UpdateAndInsertTask.Bpm_update_task_payload_wsdl_ServiceLocator()).getbpm_update_task_payload_wsdlSOAP();
      if (bpm_update_task_payload_wsdl_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_update_task_payload_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_update_task_payload_wsdl_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_update_task_payload_wsdl_PortType != null)
      ((javax.xml.rpc.Stub)bpm_update_task_payload_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.UpdateAndInsertTask.Bpm_update_task_payload_wsdl_PortType getBpm_update_task_payload_wsdl_PortType() {
    if (bpm_update_task_payload_wsdl_PortType == null)
      _initBpm_update_task_payload_wsdlProxy();
    return bpm_update_task_payload_wsdl_PortType;
  }
  
  public Bpm.UpdateAndInsertTask.Bpm_update_task_payloadResponse bpm_update_task_payload(Bpm.UpdateAndInsertTask.Bpm_update_task_payloadRequest parameters) throws java.rmi.RemoteException{
    if (bpm_update_task_payload_wsdl_PortType == null)
      _initBpm_update_task_payload_wsdlProxy();
    return bpm_update_task_payload_wsdl_PortType.bpm_update_task_payload(parameters);
  }
  
  
}