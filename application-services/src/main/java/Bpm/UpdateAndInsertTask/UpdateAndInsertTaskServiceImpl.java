package Bpm.UpdateAndInsertTask;

import java.rmi.RemoteException;

public class UpdateAndInsertTaskServiceImpl {
public static Bpm_update_task_payloadResponse bpmUpdateInsertTask(Bpm_update_task_payloadRequest parameters) throws RemoteException {
		
		Bpm_update_task_payload_wsdlProxy service = new Bpm_update_task_payload_wsdlProxy();
		Bpm_update_task_payload_wsdl_PortType port = service.getBpm_update_task_payload_wsdl_PortType();
		return port.bpm_update_task_payload(parameters);
	}

}
