/**
 * Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectInstanceIdSchema;

public class Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID  implements java.io.Serializable {
    private java.lang.String NOMOR_PERMOHONAN;

    private java.lang.String INSTANCE_ID;

    public Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID() {
    }

    public Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID(
           java.lang.String NOMOR_PERMOHONAN,
           java.lang.String INSTANCE_ID) {
           this.NOMOR_PERMOHONAN = NOMOR_PERMOHONAN;
           this.INSTANCE_ID = INSTANCE_ID;
    }


    /**
     * Gets the NOMOR_PERMOHONAN value for this Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID.
     * 
     * @return NOMOR_PERMOHONAN
     */
    public java.lang.String getNOMOR_PERMOHONAN() {
        return NOMOR_PERMOHONAN;
    }


    /**
     * Sets the NOMOR_PERMOHONAN value for this Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID.
     * 
     * @param NOMOR_PERMOHONAN
     */
    public void setNOMOR_PERMOHONAN(java.lang.String NOMOR_PERMOHONAN) {
        this.NOMOR_PERMOHONAN = NOMOR_PERMOHONAN;
    }


    /**
     * Gets the INSTANCE_ID value for this Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID.
     * 
     * @return INSTANCE_ID
     */
    public java.lang.String getINSTANCE_ID() {
        return INSTANCE_ID;
    }


    /**
     * Sets the INSTANCE_ID value for this Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID.
     * 
     * @param INSTANCE_ID
     */
    public void setINSTANCE_ID(java.lang.String INSTANCE_ID) {
        this.INSTANCE_ID = INSTANCE_ID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID)) return false;
        Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID other = (Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.NOMOR_PERMOHONAN==null && other.getNOMOR_PERMOHONAN()==null) || 
             (this.NOMOR_PERMOHONAN!=null &&
              this.NOMOR_PERMOHONAN.equals(other.getNOMOR_PERMOHONAN()))) &&
            ((this.INSTANCE_ID==null && other.getINSTANCE_ID()==null) || 
             (this.INSTANCE_ID!=null &&
              this.INSTANCE_ID.equals(other.getINSTANCE_ID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNOMOR_PERMOHONAN() != null) {
            _hashCode += getNOMOR_PERMOHONAN().hashCode();
        }
        if (getINSTANCE_ID() != null) {
            _hashCode += getINSTANCE_ID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NOMOR_PERMOHONAN");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "NOMOR_PERMOHONAN"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("INSTANCE_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_instanceid", "INSTANCE_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
