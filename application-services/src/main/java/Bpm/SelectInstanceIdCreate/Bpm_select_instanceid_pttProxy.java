package Bpm.SelectInstanceIdCreate;

public class Bpm_select_instanceid_pttProxy implements Bpm.SelectInstanceIdCreate.Bpm_select_instanceid_ptt {
  private String _endpoint = null;
  private Bpm.SelectInstanceIdCreate.Bpm_select_instanceid_ptt bpm_select_instanceid_ptt = null;
  
  public Bpm_select_instanceid_pttProxy() {
    _initBpm_select_instanceid_pttProxy();
  }
  
  public Bpm_select_instanceid_pttProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_select_instanceid_pttProxy();
  }
  
  private void _initBpm_select_instanceid_pttProxy() {
    try {
      bpm_select_instanceid_ptt = (new Bpm.SelectInstanceIdCreate.Bpm_select_instanceidServiceLocator()).getBpm_select_instanceidPort();
      if (bpm_select_instanceid_ptt != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_select_instanceid_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_select_instanceid_ptt)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_select_instanceid_ptt != null)
      ((javax.xml.rpc.Stub)bpm_select_instanceid_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.SelectInstanceIdCreate.Bpm_select_instanceid_ptt getBpm_select_instanceid_ptt() {
    if (bpm_select_instanceid_ptt == null)
      _initBpm_select_instanceid_pttProxy();
    return bpm_select_instanceid_ptt;
  }
  
  public Bpm.SelectInstanceIdCreateSchema.BpmInstanceInfo[] bpm_select_instanceidSelect(Bpm.SelectInstanceIdCreateSchema.Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid bpm_select_instanceidSelect_inputParameters) throws java.rmi.RemoteException{
    if (bpm_select_instanceid_ptt == null)
      _initBpm_select_instanceid_pttProxy();
    return bpm_select_instanceid_ptt.bpm_select_instanceidSelect(bpm_select_instanceidSelect_inputParameters);
  }
  
  
}