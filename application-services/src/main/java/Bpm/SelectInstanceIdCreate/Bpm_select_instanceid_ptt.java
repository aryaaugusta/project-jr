/**
 * Bpm_select_instanceid_ptt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectInstanceIdCreate;

public interface Bpm_select_instanceid_ptt extends java.rmi.Remote {
    public Bpm.SelectInstanceIdCreateSchema.BpmInstanceInfo[] bpm_select_instanceidSelect(Bpm.SelectInstanceIdCreateSchema.Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid bpm_select_instanceidSelect_inputParameters) throws java.rmi.RemoteException;
}
