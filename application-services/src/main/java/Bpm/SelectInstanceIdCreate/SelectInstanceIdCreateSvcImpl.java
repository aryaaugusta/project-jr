package Bpm.SelectInstanceIdCreate;

import Bpm.SelectInstanceIdCreate.Bpm_select_instanceid_ptt;
import Bpm.SelectInstanceIdCreate.Bpm_select_instanceid_pttProxy;

public class SelectInstanceIdCreateSvcImpl {
	public static Bpm.SelectInstanceIdCreateSchema.BpmInstanceInfo[] bpm_select_instanceidSelect(Bpm.SelectInstanceIdCreateSchema.Bpm_select_instanceidSelect_nomorPermohonan_creationDate_instanceid param) throws java.rmi.RemoteException{
		Bpm_select_instanceid_pttProxy service = new Bpm_select_instanceid_pttProxy();
		Bpm_select_instanceid_ptt port = service.getBpm_select_instanceid_ptt();
		return port.bpm_select_instanceidSelect(param);
		
	}
}
