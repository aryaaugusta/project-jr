/**
 * Bpm_pelayanan_tipe_perwakilan_start_ps_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.PelayananTipePerwakilan;

public class Bpm_pelayanan_tipe_perwakilan_start_ps_ServiceLocator extends org.apache.axis.client.Service implements Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilan_start_ps_Service {

    public Bpm_pelayanan_tipe_perwakilan_start_ps_ServiceLocator() {
    }


    public Bpm_pelayanan_tipe_perwakilan_start_ps_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_pelayanan_tipe_perwakilan_start_ps_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for bpm_pelayanan_tipe_perwakilan_start_psSOAP
    private java.lang.String bpm_pelayanan_tipe_perwakilan_start_psSOAP_address = "http://192.168.1.136:8011/JR-BPM/BPM/ProxyServices/bpm_pelayanan_tipe_perwakilan_start_ps";

    public java.lang.String getbpm_pelayanan_tipe_perwakilan_start_psSOAPAddress() {
        return bpm_pelayanan_tipe_perwakilan_start_psSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String bpm_pelayanan_tipe_perwakilan_start_psSOAPWSDDServiceName = "bpm_pelayanan_tipe_perwakilan_start_psSOAP";

    public java.lang.String getbpm_pelayanan_tipe_perwakilan_start_psSOAPWSDDServiceName() {
        return bpm_pelayanan_tipe_perwakilan_start_psSOAPWSDDServiceName;
    }

    public void setbpm_pelayanan_tipe_perwakilan_start_psSOAPWSDDServiceName(java.lang.String name) {
        bpm_pelayanan_tipe_perwakilan_start_psSOAPWSDDServiceName = name;
    }

    public Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilan_start_ps_PortType getbpm_pelayanan_tipe_perwakilan_start_psSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(bpm_pelayanan_tipe_perwakilan_start_psSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getbpm_pelayanan_tipe_perwakilan_start_psSOAP(endpoint);
    }

    public Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilan_start_ps_PortType getbpm_pelayanan_tipe_perwakilan_start_psSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilan_start_psSOAPStub _stub = new Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilan_start_psSOAPStub(portAddress, this);
            _stub.setPortName(getbpm_pelayanan_tipe_perwakilan_start_psSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setbpm_pelayanan_tipe_perwakilan_start_psSOAPEndpointAddress(java.lang.String address) {
        bpm_pelayanan_tipe_perwakilan_start_psSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilan_start_ps_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilan_start_psSOAPStub _stub = new Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilan_start_psSOAPStub(new java.net.URL(bpm_pelayanan_tipe_perwakilan_start_psSOAP_address), this);
                _stub.setPortName(getbpm_pelayanan_tipe_perwakilan_start_psSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_pelayanan_tipe_perwakilan_start_psSOAP".equals(inputPortName)) {
            return getbpm_pelayanan_tipe_perwakilan_start_psSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.example.org/bpm_pelayanan_tipe_perwakilan_start_ps/", "bpm_pelayanan_tipe_perwakilan_start_ps");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.example.org/bpm_pelayanan_tipe_perwakilan_start_ps/", "bpm_pelayanan_tipe_perwakilan_start_psSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("bpm_pelayanan_tipe_perwakilan_start_psSOAP".equals(portName)) {
            setbpm_pelayanan_tipe_perwakilan_start_psSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
