package Bpm.PelayananTipePerwakilan;

import java.rmi.RemoteException;


public class PelayananPerwakilanSvcImpl {
	

public static Bpm_pelayanan_tipe_perwakilanResponse bpmPerwakilan(Bpm_pelayanan_tipe_perwakilanRequest parameters) throws RemoteException {
	
	Bpm_pelayanan_tipe_perwakilan_start_psProxy service = new Bpm_pelayanan_tipe_perwakilan_start_psProxy();
	Bpm_pelayanan_tipe_perwakilan_start_ps_PortType port = service.getBpm_pelayanan_tipe_perwakilan_start_ps_PortType();
	return port.bpm_pelayanan_tipe_perwakilan_start(parameters);
	}
}