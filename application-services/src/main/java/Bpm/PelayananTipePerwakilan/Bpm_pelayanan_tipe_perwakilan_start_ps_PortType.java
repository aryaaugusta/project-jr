/**
 * Bpm_pelayanan_tipe_perwakilan_start_ps_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.PelayananTipePerwakilan;

public interface Bpm_pelayanan_tipe_perwakilan_start_ps_PortType extends java.rmi.Remote {
    public Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilanResponse bpm_pelayanan_tipe_perwakilan_start(Bpm.PelayananTipePerwakilan.Bpm_pelayanan_tipe_perwakilanRequest parameters) throws java.rmi.RemoteException;
}
