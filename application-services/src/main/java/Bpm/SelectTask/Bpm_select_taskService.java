/**
 * Bpm_select_taskService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectTask;

public interface Bpm_select_taskService extends javax.xml.rpc.Service {
    public java.lang.String getBpm_select_taskPortAddress();

    public Bpm.SelectTask.Bpm_select_task_ptt getBpm_select_taskPort() throws javax.xml.rpc.ServiceException;

    public Bpm.SelectTask.Bpm_select_task_ptt getBpm_select_taskPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
