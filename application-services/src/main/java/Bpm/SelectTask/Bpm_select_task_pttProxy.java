package Bpm.SelectTask;

public class Bpm_select_task_pttProxy implements Bpm.SelectTask.Bpm_select_task_ptt {
  private String _endpoint = null;
  private Bpm.SelectTask.Bpm_select_task_ptt bpm_select_task_ptt = null;
  
  public Bpm_select_task_pttProxy() {
    _initBpm_select_task_pttProxy();
  }
  
  public Bpm_select_task_pttProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_select_task_pttProxy();
  }
  
  private void _initBpm_select_task_pttProxy() {
    try {
      bpm_select_task_ptt = (new Bpm.SelectTask.Bpm_select_taskServiceLocator()).getBpm_select_taskPort();
      if (bpm_select_task_ptt != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_select_task_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_select_task_ptt)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_select_task_ptt != null)
      ((javax.xml.rpc.Stub)bpm_select_task_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.SelectTask.Bpm_select_task_ptt getBpm_select_task_ptt() {
    if (bpm_select_task_ptt == null)
      _initBpm_select_task_pttProxy();
    return bpm_select_task_ptt;
  }
  
  public Bpm.SelectTaskPayload.BpmInstanceInfo[] bpm_select_taskSelect(Bpm.SelectTaskPayload.Bpm_select_taskSelect_idKecelakaan_task bpm_select_taskSelect_inputParameters) throws java.rmi.RemoteException{
    if (bpm_select_task_ptt == null)
      _initBpm_select_task_pttProxy();
    return bpm_select_task_ptt.bpm_select_taskSelect(bpm_select_taskSelect_inputParameters);
  }
  
  
}