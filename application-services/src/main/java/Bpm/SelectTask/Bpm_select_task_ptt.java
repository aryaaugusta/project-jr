/**
 * Bpm_select_task_ptt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectTask;

public interface Bpm_select_task_ptt extends java.rmi.Remote {
    public Bpm.SelectTaskPayload.BpmInstanceInfo[] bpm_select_taskSelect(Bpm.SelectTaskPayload.Bpm_select_taskSelect_idKecelakaan_task bpm_select_taskSelect_inputParameters) throws java.rmi.RemoteException;
}
