/**
 * Bpm_select_taskServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectTask;

public class Bpm_select_taskServiceLocator extends org.apache.axis.client.Service implements Bpm.SelectTask.Bpm_select_taskService {

    public Bpm_select_taskServiceLocator() {
    }


    public Bpm_select_taskServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_select_taskServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for Bpm_select_taskPort
    private java.lang.String Bpm_select_taskPort_address = "http://192.168.1.136:8011/JR-BPM/BPM/ProxyServices/bpm_select_task_ps";

    public java.lang.String getBpm_select_taskPortAddress() {
        return Bpm_select_taskPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String Bpm_select_taskPortWSDDServiceName = "bpm_select_task-port";

    public java.lang.String getBpm_select_taskPortWSDDServiceName() {
        return Bpm_select_taskPortWSDDServiceName;
    }

    public void setBpm_select_taskPortWSDDServiceName(java.lang.String name) {
        Bpm_select_taskPortWSDDServiceName = name;
    }

    public Bpm.SelectTask.Bpm_select_task_ptt getBpm_select_taskPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Bpm_select_taskPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBpm_select_taskPort(endpoint);
    }

    public Bpm.SelectTask.Bpm_select_task_ptt getBpm_select_taskPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.SelectTask.Bpm_select_task_pttBindingStub _stub = new Bpm.SelectTask.Bpm_select_task_pttBindingStub(portAddress, this);
            _stub.setPortName(getBpm_select_taskPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBpm_select_taskPortEndpointAddress(java.lang.String address) {
        Bpm_select_taskPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.SelectTask.Bpm_select_task_ptt.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.SelectTask.Bpm_select_task_pttBindingStub _stub = new Bpm.SelectTask.Bpm_select_task_pttBindingStub(new java.net.URL(Bpm_select_taskPort_address), this);
                _stub.setPortName(getBpm_select_taskPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_select_task-port".equals(inputPortName)) {
            return getBpm_select_taskPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/JRBPMJCA/JCABPM/bpm_select_task", "bpm_select_task-service");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/JRBPMJCA/JCABPM/bpm_select_task", "bpm_select_task-port"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("Bpm_select_taskPort".equals(portName)) {
            setBpm_select_taskPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
