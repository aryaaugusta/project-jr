package Bpm.SelectTask;

import java.rmi.RemoteException;

import Bpm.InsertTaskBpm.Bpm_insert_task_instance_psProxy;
import Bpm.InsertTaskBpm.Bpm_insert_task_instance_ps_PortType;
import Bpm.SelectTaskPayload.BpmInstanceInfo;

public class SelectTaskSvcImpl {
	
	public static BpmInstanceInfo[] bpm_select_taskSelect(Bpm.SelectTaskPayload.Bpm_select_taskSelect_idKecelakaan_task param) throws RemoteException{
		Bpm_select_task_pttProxy service = new Bpm_select_task_pttProxy();
		Bpm_select_task_ptt port = service.getBpm_select_task_ptt();
		return port.bpm_select_taskSelect(param);
	}

}
