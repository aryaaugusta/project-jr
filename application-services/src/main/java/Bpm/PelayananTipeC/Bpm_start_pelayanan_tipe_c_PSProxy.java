package Bpm.PelayananTipeC;

public class Bpm_start_pelayanan_tipe_c_PSProxy implements Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_PortType {
  private String _endpoint = null;
  private Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_PortType bpm_start_pelayanan_tipe_c_PS_PortType = null;
  
  public Bpm_start_pelayanan_tipe_c_PSProxy() {
    _initBpm_start_pelayanan_tipe_c_PSProxy();
  }
  
  public Bpm_start_pelayanan_tipe_c_PSProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_start_pelayanan_tipe_c_PSProxy();
  }
  
  private void _initBpm_start_pelayanan_tipe_c_PSProxy() {
    try {
      bpm_start_pelayanan_tipe_c_PS_PortType = (new Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_ServiceLocator()).getbpm_start_pelayanan_tipe_c_PSSOAP();
      if (bpm_start_pelayanan_tipe_c_PS_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_start_pelayanan_tipe_c_PS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_start_pelayanan_tipe_c_PS_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_start_pelayanan_tipe_c_PS_PortType != null)
      ((javax.xml.rpc.Stub)bpm_start_pelayanan_tipe_c_PS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_PortType getBpm_start_pelayanan_tipe_c_PS_PortType() {
    if (bpm_start_pelayanan_tipe_c_PS_PortType == null)
      _initBpm_start_pelayanan_tipe_c_PSProxy();
    return bpm_start_pelayanan_tipe_c_PS_PortType;
  }
  
  public Bpm.PelayananTipeC.Response start_pelayanan_tipe_c(Bpm.PelayananTipeC.Instanceinfo parameters) throws java.rmi.RemoteException{
    if (bpm_start_pelayanan_tipe_c_PS_PortType == null)
      _initBpm_start_pelayanan_tipe_c_PSProxy();
    return bpm_start_pelayanan_tipe_c_PS_PortType.start_pelayanan_tipe_c(parameters);
  }
  
  
}