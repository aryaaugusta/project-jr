/**
 * Instanceinfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.PelayananTipeC;

public class Instanceinfo  implements java.io.Serializable {
    private Bpm.PelayananTipeC.InstanceType instanceInfo;

    public Instanceinfo() {
    }

    public Instanceinfo(
           Bpm.PelayananTipeC.InstanceType instanceInfo) {
           this.instanceInfo = instanceInfo;
    }


    /**
     * Gets the instanceInfo value for this Instanceinfo.
     * 
     * @return instanceInfo
     */
    public Bpm.PelayananTipeC.InstanceType getInstanceInfo() {
        return instanceInfo;
    }


    /**
     * Sets the instanceInfo value for this Instanceinfo.
     * 
     * @param instanceInfo
     */
    public void setInstanceInfo(Bpm.PelayananTipeC.InstanceType instanceInfo) {
        this.instanceInfo = instanceInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Instanceinfo)) return false;
        Instanceinfo other = (Instanceinfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.instanceInfo==null && other.getInstanceInfo()==null) || 
             (this.instanceInfo!=null &&
              this.instanceInfo.equals(other.getInstanceInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getInstanceInfo() != null) {
            _hashCode += getInstanceInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Instanceinfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_start_pelayanan_tipe_c_PS/", "instanceinfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("instanceInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "instanceInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_start_pelayanan_tipe_c_PS/", "instanceType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
