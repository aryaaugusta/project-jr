package Bpm.PelayananTipeC;

import java.rmi.RemoteException;


public class PelayananTipeCServiceImpl {
public static Response bpmPelTipeC(Instanceinfo parameters) throws RemoteException {
		
		Bpm_start_pelayanan_tipe_c_PSProxy service = new Bpm_start_pelayanan_tipe_c_PSProxy();
		Bpm_start_pelayanan_tipe_c_PS_PortType port = service.getBpm_start_pelayanan_tipe_c_PS_PortType();
		return port.start_pelayanan_tipe_c(parameters);
	}

}
