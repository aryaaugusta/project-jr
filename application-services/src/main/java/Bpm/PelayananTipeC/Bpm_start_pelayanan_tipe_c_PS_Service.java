/**
 * Bpm_start_pelayanan_tipe_c_PS_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.PelayananTipeC;

public interface Bpm_start_pelayanan_tipe_c_PS_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_start_pelayanan_tipe_c_PSSOAPAddress();

    public Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_PortType getbpm_start_pelayanan_tipe_c_PSSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_PortType getbpm_start_pelayanan_tipe_c_PSSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
