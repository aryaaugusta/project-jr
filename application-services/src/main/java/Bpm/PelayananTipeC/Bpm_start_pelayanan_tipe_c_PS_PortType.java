/**
 * Bpm_start_pelayanan_tipe_c_PS_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.PelayananTipeC;

public interface Bpm_start_pelayanan_tipe_c_PS_PortType extends java.rmi.Remote {
    public Bpm.PelayananTipeC.Response start_pelayanan_tipe_c(Bpm.PelayananTipeC.Instanceinfo parameters) throws java.rmi.RemoteException;
}
