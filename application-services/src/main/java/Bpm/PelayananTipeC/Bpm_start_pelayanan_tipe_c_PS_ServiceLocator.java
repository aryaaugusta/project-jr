/**
 * Bpm_start_pelayanan_tipe_c_PS_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.PelayananTipeC;

public class Bpm_start_pelayanan_tipe_c_PS_ServiceLocator extends org.apache.axis.client.Service implements Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_Service {

    public Bpm_start_pelayanan_tipe_c_PS_ServiceLocator() {
    }


    public Bpm_start_pelayanan_tipe_c_PS_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_start_pelayanan_tipe_c_PS_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for bpm_start_pelayanan_tipe_c_PSSOAP
    private java.lang.String bpm_start_pelayanan_tipe_c_PSSOAP_address = "http://192.168.1.136:8011/JR-BPM/BPM/ProxyServices/bpm_pelayanan_tipe_c_start_PS";

    public java.lang.String getbpm_start_pelayanan_tipe_c_PSSOAPAddress() {
        return bpm_start_pelayanan_tipe_c_PSSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String bpm_start_pelayanan_tipe_c_PSSOAPWSDDServiceName = "bpm_start_pelayanan_tipe_c_PSSOAP";

    public java.lang.String getbpm_start_pelayanan_tipe_c_PSSOAPWSDDServiceName() {
        return bpm_start_pelayanan_tipe_c_PSSOAPWSDDServiceName;
    }

    public void setbpm_start_pelayanan_tipe_c_PSSOAPWSDDServiceName(java.lang.String name) {
        bpm_start_pelayanan_tipe_c_PSSOAPWSDDServiceName = name;
    }

    public Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_PortType getbpm_start_pelayanan_tipe_c_PSSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(bpm_start_pelayanan_tipe_c_PSSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getbpm_start_pelayanan_tipe_c_PSSOAP(endpoint);
    }

    public Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_PortType getbpm_start_pelayanan_tipe_c_PSSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PSSOAPStub _stub = new Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PSSOAPStub(portAddress, this);
            _stub.setPortName(getbpm_start_pelayanan_tipe_c_PSSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setbpm_start_pelayanan_tipe_c_PSSOAPEndpointAddress(java.lang.String address) {
        bpm_start_pelayanan_tipe_c_PSSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PS_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PSSOAPStub _stub = new Bpm.PelayananTipeC.Bpm_start_pelayanan_tipe_c_PSSOAPStub(new java.net.URL(bpm_start_pelayanan_tipe_c_PSSOAP_address), this);
                _stub.setPortName(getbpm_start_pelayanan_tipe_c_PSSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_start_pelayanan_tipe_c_PSSOAP".equals(inputPortName)) {
            return getbpm_start_pelayanan_tipe_c_PSSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.example.org/bpm_start_pelayanan_tipe_c_PS/", "bpm_start_pelayanan_tipe_c_PS");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.example.org/bpm_start_pelayanan_tipe_c_PS/", "bpm_start_pelayanan_tipe_c_PSSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("bpm_start_pelayanan_tipe_c_PSSOAP".equals(portName)) {
            setbpm_start_pelayanan_tipe_c_PSSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
