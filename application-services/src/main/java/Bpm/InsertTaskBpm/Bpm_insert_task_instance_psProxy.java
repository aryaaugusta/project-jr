package Bpm.InsertTaskBpm;

public class Bpm_insert_task_instance_psProxy implements Bpm.InsertTaskBpm.Bpm_insert_task_instance_ps_PortType {
  private String _endpoint = null;
  private Bpm.InsertTaskBpm.Bpm_insert_task_instance_ps_PortType bpm_insert_task_instance_ps_PortType = null;
  
  public Bpm_insert_task_instance_psProxy() {
    _initBpm_insert_task_instance_psProxy();
  }
  
  public Bpm_insert_task_instance_psProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_insert_task_instance_psProxy();
  }
  
  private void _initBpm_insert_task_instance_psProxy() {
    try {
      bpm_insert_task_instance_ps_PortType = (new Bpm.InsertTaskBpm.Bpm_insert_task_instance_ps_ServiceLocator()).getbpm_insert_task_instance_psSOAP();
      if (bpm_insert_task_instance_ps_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_insert_task_instance_ps_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_insert_task_instance_ps_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_insert_task_instance_ps_PortType != null)
      ((javax.xml.rpc.Stub)bpm_insert_task_instance_ps_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.InsertTaskBpm.Bpm_insert_task_instance_ps_PortType getBpm_insert_task_instance_ps_PortType() {
    if (bpm_insert_task_instance_ps_PortType == null)
      _initBpm_insert_task_instance_psProxy();
    return bpm_insert_task_instance_ps_PortType;
  }
  
  public java.lang.String insert(java.lang.String NOMOR_PERMOHONAN, java.lang.String KODE_CABANG, java.lang.String CREATEDBY, java.lang.String CREATIONDATE, java.lang.String LASTUPDATEDBY, java.lang.String LASTUPDATEDDATE, java.lang.String IDGUID, java.lang.String PENGAJUANTYPE, java.lang.String KODEKANTOR, java.lang.String OTHERINFO1, java.lang.String OTHERINFO2, java.lang.String OTHERINFO3, java.lang.String OTHERINFO4, java.lang.String OTHERINFO5) throws java.rmi.RemoteException{
    if (bpm_insert_task_instance_ps_PortType == null)
      _initBpm_insert_task_instance_psProxy();
    return bpm_insert_task_instance_ps_PortType.insert(NOMOR_PERMOHONAN, KODE_CABANG, CREATEDBY, CREATIONDATE, LASTUPDATEDBY, LASTUPDATEDDATE, IDGUID, PENGAJUANTYPE, KODEKANTOR, OTHERINFO1, OTHERINFO2, OTHERINFO3, OTHERINFO4, OTHERINFO5);
  }
  
  
}