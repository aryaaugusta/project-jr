/**
 * Bpm_insert_task_instance_ps_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.InsertTaskBpm;

public class Bpm_insert_task_instance_ps_ServiceLocator extends org.apache.axis.client.Service implements Bpm.InsertTaskBpm.Bpm_insert_task_instance_ps_Service {

    public Bpm_insert_task_instance_ps_ServiceLocator() {
    }


    public Bpm_insert_task_instance_ps_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_insert_task_instance_ps_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for bpm_insert_task_instance_psSOAP
    private java.lang.String bpm_insert_task_instance_psSOAP_address = "http://192.168.1.136:8011/JR-BPM/BPM/ProxyServices/bpm_insert_task_instance_ps";

    public java.lang.String getbpm_insert_task_instance_psSOAPAddress() {
        return bpm_insert_task_instance_psSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String bpm_insert_task_instance_psSOAPWSDDServiceName = "bpm_insert_task_instance_psSOAP";

    public java.lang.String getbpm_insert_task_instance_psSOAPWSDDServiceName() {
        return bpm_insert_task_instance_psSOAPWSDDServiceName;
    }

    public void setbpm_insert_task_instance_psSOAPWSDDServiceName(java.lang.String name) {
        bpm_insert_task_instance_psSOAPWSDDServiceName = name;
    }

    public Bpm.InsertTaskBpm.Bpm_insert_task_instance_ps_PortType getbpm_insert_task_instance_psSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(bpm_insert_task_instance_psSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getbpm_insert_task_instance_psSOAP(endpoint);
    }

    public Bpm.InsertTaskBpm.Bpm_insert_task_instance_ps_PortType getbpm_insert_task_instance_psSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.InsertTaskBpm.Bpm_insert_task_instance_psSOAPStub _stub = new Bpm.InsertTaskBpm.Bpm_insert_task_instance_psSOAPStub(portAddress, this);
            _stub.setPortName(getbpm_insert_task_instance_psSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setbpm_insert_task_instance_psSOAPEndpointAddress(java.lang.String address) {
        bpm_insert_task_instance_psSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.InsertTaskBpm.Bpm_insert_task_instance_ps_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.InsertTaskBpm.Bpm_insert_task_instance_psSOAPStub _stub = new Bpm.InsertTaskBpm.Bpm_insert_task_instance_psSOAPStub(new java.net.URL(bpm_insert_task_instance_psSOAP_address), this);
                _stub.setPortName(getbpm_insert_task_instance_psSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_insert_task_instance_psSOAP".equals(inputPortName)) {
            return getbpm_insert_task_instance_psSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.example.org/bpm_insert_task_instance_ps/", "bpm_insert_task_instance_ps");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.example.org/bpm_insert_task_instance_ps/", "bpm_insert_task_instance_psSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("bpm_insert_task_instance_psSOAP".equals(portName)) {
            setbpm_insert_task_instance_psSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
