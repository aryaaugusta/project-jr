package Bpm.InsertTaskBpm;

import java.rmi.RemoteException;



public class InsertTaskBpmSvcImpl {
	
	public static String insert(String noPermohonan, String kodeCabang, String createdBy, String creationDate, String lastUpdatedBy, String lastUpdatedDate, String idGuid, String pengajuanType, String kodeKantor, String otherInfo1, String otherInfo2, String otherInfo3, String otherInfo4, String otherInfo5) throws RemoteException {

		Bpm_insert_task_instance_psProxy service = new Bpm_insert_task_instance_psProxy();
		Bpm_insert_task_instance_ps_PortType port = service.getBpm_insert_task_instance_ps_PortType();
		return port.insert(noPermohonan, kodeCabang, createdBy, creationDate, lastUpdatedBy, lastUpdatedDate, idGuid, pengajuanType, kodeKantor, otherInfo1, otherInfo2, otherInfo3, otherInfo4, otherInfo5);
	}

}
