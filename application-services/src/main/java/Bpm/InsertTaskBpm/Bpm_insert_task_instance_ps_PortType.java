/**
 * Bpm_insert_task_instance_ps_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.InsertTaskBpm;

public interface Bpm_insert_task_instance_ps_PortType extends java.rmi.Remote {
    public java.lang.String insert(java.lang.String NOMOR_PERMOHONAN, java.lang.String KODE_CABANG, java.lang.String CREATEDBY, java.lang.String CREATIONDATE, java.lang.String LASTUPDATEDBY, java.lang.String LASTUPDATEDDATE, java.lang.String IDGUID, java.lang.String PENGAJUANTYPE, java.lang.String KODEKANTOR, java.lang.String OTHERINFO1, java.lang.String OTHERINFO2, java.lang.String OTHERINFO3, java.lang.String OTHERINFO4, java.lang.String OTHERINFO5) throws java.rmi.RemoteException;
}
