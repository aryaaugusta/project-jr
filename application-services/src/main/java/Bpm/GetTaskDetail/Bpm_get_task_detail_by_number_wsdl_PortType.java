/**
 * Bpm_get_task_detail_by_number_wsdl_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.GetTaskDetail;

public interface Bpm_get_task_detail_by_number_wsdl_PortType extends java.rmi.Remote {
    public Bpm.GetTaskDetail.Bpm_get_task_detail_by_numberResponse bpm_get_task_detail_by_number(Bpm.GetTaskDetail.Bpm_get_task_detail_by_numberRequest parameters) throws java.rmi.RemoteException;
}
