/**
 * Bpm_get_task_detail_by_numberRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.GetTaskDetail;

public class Bpm_get_task_detail_by_numberRequest  implements java.io.Serializable {
    private Bpm.GetTaskDetail.CredentialType credential;

    private org.apache.axis.types.PositiveInteger taskNumber;

    public Bpm_get_task_detail_by_numberRequest() {
    }

    public Bpm_get_task_detail_by_numberRequest(
           Bpm.GetTaskDetail.CredentialType credential,
           org.apache.axis.types.PositiveInteger taskNumber) {
           this.credential = credential;
           this.taskNumber = taskNumber;
    }


    /**
     * Gets the credential value for this Bpm_get_task_detail_by_numberRequest.
     * 
     * @return credential
     */
    public Bpm.GetTaskDetail.CredentialType getCredential() {
        return credential;
    }


    /**
     * Sets the credential value for this Bpm_get_task_detail_by_numberRequest.
     * 
     * @param credential
     */
    public void setCredential(Bpm.GetTaskDetail.CredentialType credential) {
        this.credential = credential;
    }


    /**
     * Gets the taskNumber value for this Bpm_get_task_detail_by_numberRequest.
     * 
     * @return taskNumber
     */
    public org.apache.axis.types.PositiveInteger getTaskNumber() {
        return taskNumber;
    }


    /**
     * Sets the taskNumber value for this Bpm_get_task_detail_by_numberRequest.
     * 
     * @param taskNumber
     */
    public void setTaskNumber(org.apache.axis.types.PositiveInteger taskNumber) {
        this.taskNumber = taskNumber;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_get_task_detail_by_numberRequest)) return false;
        Bpm_get_task_detail_by_numberRequest other = (Bpm_get_task_detail_by_numberRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.credential==null && other.getCredential()==null) || 
             (this.credential!=null &&
              this.credential.equals(other.getCredential()))) &&
            ((this.taskNumber==null && other.getTaskNumber()==null) || 
             (this.taskNumber!=null &&
              this.taskNumber.equals(other.getTaskNumber())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCredential() != null) {
            _hashCode += getCredential().hashCode();
        }
        if (getTaskNumber() != null) {
            _hashCode += getTaskNumber().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_get_task_detail_by_numberRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_get_task_detail_by_number_wsdl/", ">bpm_get_task_detail_by_numberRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credential");
        elemField.setXmlName(new javax.xml.namespace.QName("", "credential"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_get_task_detail_by_number_wsdl/", "credentialType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taskNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "positiveInteger"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
