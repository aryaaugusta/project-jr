/**
 * Bpm_get_task_detail_by_number_wsdl_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.GetTaskDetail;

public interface Bpm_get_task_detail_by_number_wsdl_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_get_task_detail_by_number_wsdlSOAPAddress();

    public Bpm.GetTaskDetail.Bpm_get_task_detail_by_number_wsdl_PortType getbpm_get_task_detail_by_number_wsdlSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.GetTaskDetail.Bpm_get_task_detail_by_number_wsdl_PortType getbpm_get_task_detail_by_number_wsdlSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
