package Bpm.GetTaskDetail;

public class Bpm_get_task_detail_by_number_wsdlProxy implements Bpm.GetTaskDetail.Bpm_get_task_detail_by_number_wsdl_PortType {
  private String _endpoint = null;
  private Bpm.GetTaskDetail.Bpm_get_task_detail_by_number_wsdl_PortType bpm_get_task_detail_by_number_wsdl_PortType = null;
  
  public Bpm_get_task_detail_by_number_wsdlProxy() {
    _initBpm_get_task_detail_by_number_wsdlProxy();
  }
  
  public Bpm_get_task_detail_by_number_wsdlProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_get_task_detail_by_number_wsdlProxy();
  }
  
  private void _initBpm_get_task_detail_by_number_wsdlProxy() {
    try {
      bpm_get_task_detail_by_number_wsdl_PortType = (new Bpm.GetTaskDetail.Bpm_get_task_detail_by_number_wsdl_ServiceLocator()).getbpm_get_task_detail_by_number_wsdlSOAP();
      if (bpm_get_task_detail_by_number_wsdl_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_get_task_detail_by_number_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_get_task_detail_by_number_wsdl_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_get_task_detail_by_number_wsdl_PortType != null)
      ((javax.xml.rpc.Stub)bpm_get_task_detail_by_number_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.GetTaskDetail.Bpm_get_task_detail_by_number_wsdl_PortType getBpm_get_task_detail_by_number_wsdl_PortType() {
    if (bpm_get_task_detail_by_number_wsdl_PortType == null)
      _initBpm_get_task_detail_by_number_wsdlProxy();
    return bpm_get_task_detail_by_number_wsdl_PortType;
  }
  
  public Bpm.GetTaskDetail.Bpm_get_task_detail_by_numberResponse bpm_get_task_detail_by_number(Bpm.GetTaskDetail.Bpm_get_task_detail_by_numberRequest parameters) throws java.rmi.RemoteException{
    if (bpm_get_task_detail_by_number_wsdl_PortType == null)
      _initBpm_get_task_detail_by_number_wsdlProxy();
    return bpm_get_task_detail_by_number_wsdl_PortType.bpm_get_task_detail_by_number(parameters);
  }
  
  
}