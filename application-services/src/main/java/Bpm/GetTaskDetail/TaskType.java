/**
 * TaskType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.GetTaskDetail;

public class TaskType  implements java.io.Serializable {
    private java.lang.String title;

    private Bpm.GetTaskDetail.PayloadType payload;

    private Bpm.GetTaskDetail.ProcessInfoType processInfo;

    private java.lang.String taskId;

    public TaskType() {
    }

    public TaskType(
           java.lang.String title,
           Bpm.GetTaskDetail.PayloadType payload,
           Bpm.GetTaskDetail.ProcessInfoType processInfo,
           java.lang.String taskId) {
           this.title = title;
           this.payload = payload;
           this.processInfo = processInfo;
           this.taskId = taskId;
    }


    /**
     * Gets the title value for this TaskType.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this TaskType.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the payload value for this TaskType.
     * 
     * @return payload
     */
    public Bpm.GetTaskDetail.PayloadType getPayload() {
        return payload;
    }


    /**
     * Sets the payload value for this TaskType.
     * 
     * @param payload
     */
    public void setPayload(Bpm.GetTaskDetail.PayloadType payload) {
        this.payload = payload;
    }


    /**
     * Gets the processInfo value for this TaskType.
     * 
     * @return processInfo
     */
    public Bpm.GetTaskDetail.ProcessInfoType getProcessInfo() {
        return processInfo;
    }


    /**
     * Sets the processInfo value for this TaskType.
     * 
     * @param processInfo
     */
    public void setProcessInfo(Bpm.GetTaskDetail.ProcessInfoType processInfo) {
        this.processInfo = processInfo;
    }


    /**
     * Gets the taskId value for this TaskType.
     * 
     * @return taskId
     */
    public java.lang.String getTaskId() {
        return taskId;
    }


    /**
     * Sets the taskId value for this TaskType.
     * 
     * @param taskId
     */
    public void setTaskId(java.lang.String taskId) {
        this.taskId = taskId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof TaskType)) return false;
        TaskType other = (TaskType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.payload==null && other.getPayload()==null) || 
             (this.payload!=null &&
              this.payload.equals(other.getPayload()))) &&
            ((this.processInfo==null && other.getProcessInfo()==null) || 
             (this.processInfo!=null &&
              this.processInfo.equals(other.getProcessInfo()))) &&
            ((this.taskId==null && other.getTaskId()==null) || 
             (this.taskId!=null &&
              this.taskId.equals(other.getTaskId())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getPayload() != null) {
            _hashCode += getPayload().hashCode();
        }
        if (getProcessInfo() != null) {
            _hashCode += getProcessInfo().hashCode();
        }
        if (getTaskId() != null) {
            _hashCode += getTaskId().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(TaskType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_get_task_detail_by_number_wsdl/", "taskType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("payload");
        elemField.setXmlName(new javax.xml.namespace.QName("", "payload"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_get_task_detail_by_number_wsdl/", "payloadType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("processInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "processInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_get_task_detail_by_number_wsdl/", "processInfoType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taskId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
