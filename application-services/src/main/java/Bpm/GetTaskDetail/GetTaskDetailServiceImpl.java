package Bpm.GetTaskDetail;

import java.rmi.RemoteException;




public class GetTaskDetailServiceImpl {
	
public static Bpm_get_task_detail_by_numberResponse bpmGetTaskDetail(Bpm_get_task_detail_by_numberRequest parameters) throws RemoteException {
		
		Bpm_get_task_detail_by_number_wsdlProxy service = new Bpm_get_task_detail_by_number_wsdlProxy();
		Bpm_get_task_detail_by_number_wsdl_PortType port = service.getBpm_get_task_detail_by_number_wsdl_PortType();
		return port.bpm_get_task_detail_by_number(parameters);
	}

}
