/**
 * Bpm_select_taskSelect_idKecelakaan_task.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectTaskPayload;

public class Bpm_select_taskSelect_idKecelakaan_task  implements java.io.Serializable {
    private java.lang.String idKecelakaan;

    private java.lang.String task;

    public Bpm_select_taskSelect_idKecelakaan_task() {
    }

    public Bpm_select_taskSelect_idKecelakaan_task(
           java.lang.String idKecelakaan,
           java.lang.String task) {
           this.idKecelakaan = idKecelakaan;
           this.task = task;
    }


    /**
     * Gets the idKecelakaan value for this Bpm_select_taskSelect_idKecelakaan_task.
     * 
     * @return idKecelakaan
     */
    public java.lang.String getIdKecelakaan() {
        return idKecelakaan;
    }


    /**
     * Sets the idKecelakaan value for this Bpm_select_taskSelect_idKecelakaan_task.
     * 
     * @param idKecelakaan
     */
    public void setIdKecelakaan(java.lang.String idKecelakaan) {
        this.idKecelakaan = idKecelakaan;
    }


    /**
     * Gets the task value for this Bpm_select_taskSelect_idKecelakaan_task.
     * 
     * @return task
     */
    public java.lang.String getTask() {
        return task;
    }


    /**
     * Sets the task value for this Bpm_select_taskSelect_idKecelakaan_task.
     * 
     * @param task
     */
    public void setTask(java.lang.String task) {
        this.task = task;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_select_taskSelect_idKecelakaan_task)) return false;
        Bpm_select_taskSelect_idKecelakaan_task other = (Bpm_select_taskSelect_idKecelakaan_task) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idKecelakaan==null && other.getIdKecelakaan()==null) || 
             (this.idKecelakaan!=null &&
              this.idKecelakaan.equals(other.getIdKecelakaan()))) &&
            ((this.task==null && other.getTask()==null) || 
             (this.task!=null &&
              this.task.equals(other.getTask())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdKecelakaan() != null) {
            _hashCode += getIdKecelakaan().hashCode();
        }
        if (getTask() != null) {
            _hashCode += getTask().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_select_taskSelect_idKecelakaan_task.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_task", "bpm_select_taskSelect_idKecelakaan_task"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idKecelakaan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_task", "idKecelakaan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("task");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_select_task", "task"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
