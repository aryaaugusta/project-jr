/**
 * Bpm_select_instanceidService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectInstanceId;

public interface Bpm_select_instanceidService extends javax.xml.rpc.Service {
    public java.lang.String getBpm_select_instanceidPortAddress();

    public Bpm.SelectInstanceId.Bpm_select_instanceid_ptt getBpm_select_instanceidPort() throws javax.xml.rpc.ServiceException;

    public Bpm.SelectInstanceId.Bpm_select_instanceid_ptt getBpm_select_instanceidPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
