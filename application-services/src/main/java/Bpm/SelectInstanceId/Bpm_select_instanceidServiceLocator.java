/**
 * Bpm_select_instanceidServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.SelectInstanceId;

public class Bpm_select_instanceidServiceLocator extends org.apache.axis.client.Service implements Bpm.SelectInstanceId.Bpm_select_instanceidService {

    public Bpm_select_instanceidServiceLocator() {
    }


    public Bpm_select_instanceidServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_select_instanceidServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for Bpm_select_instanceidPort
    private java.lang.String Bpm_select_instanceidPort_address = "http://192.168.1.136:8011/JR-BPM/BPM/ProxyServices/bpm_select_instanceid";

    public java.lang.String getBpm_select_instanceidPortAddress() {
        return Bpm_select_instanceidPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String Bpm_select_instanceidPortWSDDServiceName = "bpm_select_instanceid-port";

    public java.lang.String getBpm_select_instanceidPortWSDDServiceName() {
        return Bpm_select_instanceidPortWSDDServiceName;
    }

    public void setBpm_select_instanceidPortWSDDServiceName(java.lang.String name) {
        Bpm_select_instanceidPortWSDDServiceName = name;
    }

    public Bpm.SelectInstanceId.Bpm_select_instanceid_ptt getBpm_select_instanceidPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Bpm_select_instanceidPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBpm_select_instanceidPort(endpoint);
    }

    public Bpm.SelectInstanceId.Bpm_select_instanceid_ptt getBpm_select_instanceidPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.SelectInstanceId.Bpm_select_instanceid_pttBindingStub _stub = new Bpm.SelectInstanceId.Bpm_select_instanceid_pttBindingStub(portAddress, this);
            _stub.setPortName(getBpm_select_instanceidPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBpm_select_instanceidPortEndpointAddress(java.lang.String address) {
        Bpm_select_instanceidPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.SelectInstanceId.Bpm_select_instanceid_ptt.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.SelectInstanceId.Bpm_select_instanceid_pttBindingStub _stub = new Bpm.SelectInstanceId.Bpm_select_instanceid_pttBindingStub(new java.net.URL(Bpm_select_instanceidPort_address), this);
                _stub.setPortName(getBpm_select_instanceidPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_select_instanceid-port".equals(inputPortName)) {
            return getBpm_select_instanceidPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/JRBPMJCA/JCABPM/bpm_select_instanceid", "bpm_select_instanceid-service");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/JRBPMJCA/JCABPM/bpm_select_instanceid", "bpm_select_instanceid-port"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("Bpm_select_instanceidPort".equals(portName)) {
            setBpm_select_instanceidPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
