package Bpm.SelectInstanceId;

public class SelectInstanceIdSvcImpl {
	public static Bpm.SelectInstanceIdSchema.BpmInstanceInfo[] bpm_select_instanceidSelect(Bpm.SelectInstanceIdSchema.Bpm_select_instanceidSelect_NOMOR_PERMOHONAN_INSTANCE_ID param) throws java.rmi.RemoteException{
		Bpm_select_instanceid_pttProxy service = new Bpm_select_instanceid_pttProxy();
		Bpm_select_instanceid_ptt port = service.getBpm_select_instanceid_ptt();
		return port.bpm_select_instanceidSelect(param);
	}

}
