package Bpm.Ecms.uploadByDirectory;

public class Bpm_ecms_upload_wsdlProxy implements Bpm.Ecms.uploadByDirectory.Bpm_ecms_upload_wsdl_PortType {
  private String _endpoint = null;
  private Bpm.Ecms.uploadByDirectory.Bpm_ecms_upload_wsdl_PortType bpm_ecms_upload_wsdl_PortType = null;
  
  public Bpm_ecms_upload_wsdlProxy() {
    _initBpm_ecms_upload_wsdlProxy();
  }
  
  public Bpm_ecms_upload_wsdlProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_ecms_upload_wsdlProxy();
  }
  
  private void _initBpm_ecms_upload_wsdlProxy() {
    try {
      bpm_ecms_upload_wsdl_PortType = (new Bpm.Ecms.uploadByDirectory.Bpm_ecms_upload_wsdl_ServiceLocator()).getbpm_ecms_upload_wsdlSOAP();
      if (bpm_ecms_upload_wsdl_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_ecms_upload_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_ecms_upload_wsdl_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_ecms_upload_wsdl_PortType != null)
      ((javax.xml.rpc.Stub)bpm_ecms_upload_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.Ecms.uploadByDirectory.Bpm_ecms_upload_wsdl_PortType getBpm_ecms_upload_wsdl_PortType() {
    if (bpm_ecms_upload_wsdl_PortType == null)
      _initBpm_ecms_upload_wsdlProxy();
    return bpm_ecms_upload_wsdl_PortType;
  }
  
  public Bpm.Ecms.uploadByDirectory.AttachmentResultType[] bpm_ecms_upload(java.lang.String noBerkas, java.lang.String docPath, Bpm.Ecms.uploadByDirectory.AttachmentsType[] file_attachments) throws java.rmi.RemoteException{
    if (bpm_ecms_upload_wsdl_PortType == null)
      _initBpm_ecms_upload_wsdlProxy();
    return bpm_ecms_upload_wsdl_PortType.bpm_ecms_upload(noBerkas, docPath, file_attachments);
  }
  
  
}