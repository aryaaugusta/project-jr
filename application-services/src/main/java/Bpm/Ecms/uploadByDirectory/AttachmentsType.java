/**
 * AttachmentsType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.uploadByDirectory;

public class AttachmentsType  implements java.io.Serializable {
    private java.lang.String namaFile;

    private java.lang.String deskripsi;

    private byte[] attachments;

    public AttachmentsType() {
    }

    public AttachmentsType(
           java.lang.String namaFile,
           java.lang.String deskripsi,
           byte[] attachments) {
           this.namaFile = namaFile;
           this.deskripsi = deskripsi;
           this.attachments = attachments;
    }


    /**
     * Gets the namaFile value for this AttachmentsType.
     * 
     * @return namaFile
     */
    public java.lang.String getNamaFile() {
        return namaFile;
    }


    /**
     * Sets the namaFile value for this AttachmentsType.
     * 
     * @param namaFile
     */
    public void setNamaFile(java.lang.String namaFile) {
        this.namaFile = namaFile;
    }


    /**
     * Gets the deskripsi value for this AttachmentsType.
     * 
     * @return deskripsi
     */
    public java.lang.String getDeskripsi() {
        return deskripsi;
    }


    /**
     * Sets the deskripsi value for this AttachmentsType.
     * 
     * @param deskripsi
     */
    public void setDeskripsi(java.lang.String deskripsi) {
        this.deskripsi = deskripsi;
    }


    /**
     * Gets the attachments value for this AttachmentsType.
     * 
     * @return attachments
     */
    public byte[] getAttachments() {
        return attachments;
    }


    /**
     * Sets the attachments value for this AttachmentsType.
     * 
     * @param attachments
     */
    public void setAttachments(byte[] attachments) {
        this.attachments = attachments;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AttachmentsType)) return false;
        AttachmentsType other = (AttachmentsType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.namaFile==null && other.getNamaFile()==null) || 
             (this.namaFile!=null &&
              this.namaFile.equals(other.getNamaFile()))) &&
            ((this.deskripsi==null && other.getDeskripsi()==null) || 
             (this.deskripsi!=null &&
              this.deskripsi.equals(other.getDeskripsi()))) &&
            ((this.attachments==null && other.getAttachments()==null) || 
             (this.attachments!=null &&
              java.util.Arrays.equals(this.attachments, other.getAttachments())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNamaFile() != null) {
            _hashCode += getNamaFile().hashCode();
        }
        if (getDeskripsi() != null) {
            _hashCode += getDeskripsi().hashCode();
        }
        if (getAttachments() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttachments());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttachments(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AttachmentsType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_upload_wsdl/", "attachmentsType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("namaFile");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NamaFile"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deskripsi");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Deskripsi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attachments");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Attachments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "base64Binary"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
