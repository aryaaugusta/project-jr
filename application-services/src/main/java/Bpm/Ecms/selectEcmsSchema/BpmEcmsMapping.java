/**
 * BpmEcmsMapping.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.selectEcmsSchema;

public class BpmEcmsMapping  implements java.io.Serializable {
    private java.lang.String directoryId;

    private java.lang.String ecmsId;

    private java.lang.String deskripsi;

    public BpmEcmsMapping() {
    }

    public BpmEcmsMapping(
           java.lang.String directoryId,
           java.lang.String ecmsId,
           java.lang.String deskripsi) {
           this.directoryId = directoryId;
           this.ecmsId = ecmsId;
           this.deskripsi = deskripsi;
    }


    /**
     * Gets the directoryId value for this BpmEcmsMapping.
     * 
     * @return directoryId
     */
    public java.lang.String getDirectoryId() {
        return directoryId;
    }


    /**
     * Sets the directoryId value for this BpmEcmsMapping.
     * 
     * @param directoryId
     */
    public void setDirectoryId(java.lang.String directoryId) {
        this.directoryId = directoryId;
    }


    /**
     * Gets the ecmsId value for this BpmEcmsMapping.
     * 
     * @return ecmsId
     */
    public java.lang.String getEcmsId() {
        return ecmsId;
    }


    /**
     * Sets the ecmsId value for this BpmEcmsMapping.
     * 
     * @param ecmsId
     */
    public void setEcmsId(java.lang.String ecmsId) {
        this.ecmsId = ecmsId;
    }


    /**
     * Gets the deskripsi value for this BpmEcmsMapping.
     * 
     * @return deskripsi
     */
    public java.lang.String getDeskripsi() {
        return deskripsi;
    }


    /**
     * Sets the deskripsi value for this BpmEcmsMapping.
     * 
     * @param deskripsi
     */
    public void setDeskripsi(java.lang.String deskripsi) {
        this.deskripsi = deskripsi;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BpmEcmsMapping)) return false;
        BpmEcmsMapping other = (BpmEcmsMapping) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.directoryId==null && other.getDirectoryId()==null) || 
             (this.directoryId!=null &&
              this.directoryId.equals(other.getDirectoryId()))) &&
            ((this.ecmsId==null && other.getEcmsId()==null) || 
             (this.ecmsId!=null &&
              this.ecmsId.equals(other.getEcmsId()))) &&
            ((this.deskripsi==null && other.getDeskripsi()==null) || 
             (this.deskripsi!=null &&
              this.deskripsi.equals(other.getDeskripsi())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDirectoryId() != null) {
            _hashCode += getDirectoryId().hashCode();
        }
        if (getEcmsId() != null) {
            _hashCode += getEcmsId().hashCode();
        }
        if (getDeskripsi() != null) {
            _hashCode += getDeskripsi().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BpmEcmsMapping.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_ecms_mapping_query", "BpmEcmsMapping"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("directoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_ecms_mapping_query", "directoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ecmsId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_ecms_mapping_query", "ecmsId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deskripsi");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_ecms_mapping_query", "deskripsi"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
