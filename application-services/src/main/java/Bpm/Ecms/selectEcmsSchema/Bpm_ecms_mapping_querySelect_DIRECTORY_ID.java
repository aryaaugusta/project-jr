/**
 * Bpm_ecms_mapping_querySelect_DIRECTORY_ID.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.selectEcmsSchema;

public class Bpm_ecms_mapping_querySelect_DIRECTORY_ID  implements java.io.Serializable {
    private java.lang.String DIRECTORY_ID;

    public Bpm_ecms_mapping_querySelect_DIRECTORY_ID() {
    }

    public Bpm_ecms_mapping_querySelect_DIRECTORY_ID(
           java.lang.String DIRECTORY_ID) {
           this.DIRECTORY_ID = DIRECTORY_ID;
    }


    /**
     * Gets the DIRECTORY_ID value for this Bpm_ecms_mapping_querySelect_DIRECTORY_ID.
     * 
     * @return DIRECTORY_ID
     */
    public java.lang.String getDIRECTORY_ID() {
        return DIRECTORY_ID;
    }


    /**
     * Sets the DIRECTORY_ID value for this Bpm_ecms_mapping_querySelect_DIRECTORY_ID.
     * 
     * @param DIRECTORY_ID
     */
    public void setDIRECTORY_ID(java.lang.String DIRECTORY_ID) {
        this.DIRECTORY_ID = DIRECTORY_ID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_ecms_mapping_querySelect_DIRECTORY_ID)) return false;
        Bpm_ecms_mapping_querySelect_DIRECTORY_ID other = (Bpm_ecms_mapping_querySelect_DIRECTORY_ID) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.DIRECTORY_ID==null && other.getDIRECTORY_ID()==null) || 
             (this.DIRECTORY_ID!=null &&
              this.DIRECTORY_ID.equals(other.getDIRECTORY_ID())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDIRECTORY_ID() != null) {
            _hashCode += getDIRECTORY_ID().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_ecms_mapping_querySelect_DIRECTORY_ID.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_ecms_mapping_query", "bpm_ecms_mapping_querySelect_DIRECTORY_ID"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DIRECTORY_ID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/top/bpm_ecms_mapping_query", "DIRECTORY_ID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
