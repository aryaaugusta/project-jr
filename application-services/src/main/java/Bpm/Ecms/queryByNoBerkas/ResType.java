/**
 * ResType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.queryByNoBerkas;

public class ResType  implements java.io.Serializable {
    private Bpm.Ecms.queryByNoBerkas.ContentResult[] content;

    private Bpm.Ecms.queryByNoBerkas.StatusResult status;

    public ResType() {
    }

    public ResType(
           Bpm.Ecms.queryByNoBerkas.ContentResult[] content,
           Bpm.Ecms.queryByNoBerkas.StatusResult status) {
           this.content = content;
           this.status = status;
    }


    /**
     * Gets the content value for this ResType.
     * 
     * @return content
     */
    public Bpm.Ecms.queryByNoBerkas.ContentResult[] getContent() {
        return content;
    }


    /**
     * Sets the content value for this ResType.
     * 
     * @param content
     */
    public void setContent(Bpm.Ecms.queryByNoBerkas.ContentResult[] content) {
        this.content = content;
    }

    public Bpm.Ecms.queryByNoBerkas.ContentResult getContent(int i) {
        return this.content[i];
    }

    public void setContent(int i, Bpm.Ecms.queryByNoBerkas.ContentResult _value) {
        this.content[i] = _value;
    }


    /**
     * Gets the status value for this ResType.
     * 
     * @return status
     */
    public Bpm.Ecms.queryByNoBerkas.StatusResult getStatus() {
        return status;
    }


    /**
     * Sets the status value for this ResType.
     * 
     * @param status
     */
    public void setStatus(Bpm.Ecms.queryByNoBerkas.StatusResult status) {
        this.status = status;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ResType)) return false;
        ResType other = (ResType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.content==null && other.getContent()==null) || 
             (this.content!=null &&
              java.util.Arrays.equals(this.content, other.getContent()))) &&
            ((this.status==null && other.getStatus()==null) || 
             (this.status!=null &&
              this.status.equals(other.getStatus())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getContent() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getContent());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getContent(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStatus() != null) {
            _hashCode += getStatus().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ResType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_query_bynoberkas_ps_wsdl/", "resType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("content");
        elemField.setXmlName(new javax.xml.namespace.QName("", "content"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_query_bynoberkas_ps_wsdl/", "contentResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("status");
        elemField.setXmlName(new javax.xml.namespace.QName("", "status"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_query_bynoberkas_ps_wsdl/", "statusResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
