/**
 * Bpm_ecms_query_bynoberkas_ps_wsdl_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.queryByNoBerkas;

public interface Bpm_ecms_query_bynoberkas_ps_wsdl_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_ecms_query_bynoberkas_ps_wsdlSOAPAddress();

    public Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdl_PortType getbpm_ecms_query_bynoberkas_ps_wsdlSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdl_PortType getbpm_ecms_query_bynoberkas_ps_wsdlSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
