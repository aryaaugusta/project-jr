/**
 * Bpm_ecms_query_bynoberkas_ps_wsdl_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.queryByNoBerkas;

public interface Bpm_ecms_query_bynoberkas_ps_wsdl_PortType extends java.rmi.Remote {
    public Bpm.Ecms.queryByNoBerkas.Response get_ecms_id(Bpm.Ecms.queryByNoBerkas.DirectoryName parameters) throws java.rmi.RemoteException;
}
