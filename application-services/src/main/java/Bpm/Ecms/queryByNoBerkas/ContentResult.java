/**
 * ContentResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.queryByNoBerkas;

public class ContentResult  implements java.io.Serializable {
    private java.lang.String dDocName;

    private java.lang.String dDocTitle;

    private java.lang.String dOriginalName;

    private java.lang.String dFormat;

    private java.lang.String dExtension;

    private int dFileSize;

    private java.lang.String xComments;

    private java.lang.String dWebURL;

    private int dID;

    public ContentResult() {
    }

    public ContentResult(
           java.lang.String dDocName,
           java.lang.String dDocTitle,
           java.lang.String dOriginalName,
           java.lang.String dFormat,
           java.lang.String dExtension,
           int dFileSize,
           java.lang.String xComments,
           java.lang.String dWebURL,
           int dID) {
           this.dDocName = dDocName;
           this.dDocTitle = dDocTitle;
           this.dOriginalName = dOriginalName;
           this.dFormat = dFormat;
           this.dExtension = dExtension;
           this.dFileSize = dFileSize;
           this.xComments = xComments;
           this.dWebURL = dWebURL;
           this.dID = dID;
    }


    /**
     * Gets the dDocName value for this ContentResult.
     * 
     * @return dDocName
     */
    public java.lang.String getDDocName() {
        return dDocName;
    }


    /**
     * Sets the dDocName value for this ContentResult.
     * 
     * @param dDocName
     */
    public void setDDocName(java.lang.String dDocName) {
        this.dDocName = dDocName;
    }


    /**
     * Gets the dDocTitle value for this ContentResult.
     * 
     * @return dDocTitle
     */
    public java.lang.String getDDocTitle() {
        return dDocTitle;
    }


    /**
     * Sets the dDocTitle value for this ContentResult.
     * 
     * @param dDocTitle
     */
    public void setDDocTitle(java.lang.String dDocTitle) {
        this.dDocTitle = dDocTitle;
    }


    /**
     * Gets the dOriginalName value for this ContentResult.
     * 
     * @return dOriginalName
     */
    public java.lang.String getDOriginalName() {
        return dOriginalName;
    }


    /**
     * Sets the dOriginalName value for this ContentResult.
     * 
     * @param dOriginalName
     */
    public void setDOriginalName(java.lang.String dOriginalName) {
        this.dOriginalName = dOriginalName;
    }


    /**
     * Gets the dFormat value for this ContentResult.
     * 
     * @return dFormat
     */
    public java.lang.String getDFormat() {
        return dFormat;
    }


    /**
     * Sets the dFormat value for this ContentResult.
     * 
     * @param dFormat
     */
    public void setDFormat(java.lang.String dFormat) {
        this.dFormat = dFormat;
    }


    /**
     * Gets the dExtension value for this ContentResult.
     * 
     * @return dExtension
     */
    public java.lang.String getDExtension() {
        return dExtension;
    }


    /**
     * Sets the dExtension value for this ContentResult.
     * 
     * @param dExtension
     */
    public void setDExtension(java.lang.String dExtension) {
        this.dExtension = dExtension;
    }


    /**
     * Gets the dFileSize value for this ContentResult.
     * 
     * @return dFileSize
     */
    public int getDFileSize() {
        return dFileSize;
    }


    /**
     * Sets the dFileSize value for this ContentResult.
     * 
     * @param dFileSize
     */
    public void setDFileSize(int dFileSize) {
        this.dFileSize = dFileSize;
    }


    /**
     * Gets the xComments value for this ContentResult.
     * 
     * @return xComments
     */
    public java.lang.String getXComments() {
        return xComments;
    }


    /**
     * Sets the xComments value for this ContentResult.
     * 
     * @param xComments
     */
    public void setXComments(java.lang.String xComments) {
        this.xComments = xComments;
    }


    /**
     * Gets the dWebURL value for this ContentResult.
     * 
     * @return dWebURL
     */
    public java.lang.String getDWebURL() {
        return dWebURL;
    }


    /**
     * Sets the dWebURL value for this ContentResult.
     * 
     * @param dWebURL
     */
    public void setDWebURL(java.lang.String dWebURL) {
        this.dWebURL = dWebURL;
    }


    /**
     * Gets the dID value for this ContentResult.
     * 
     * @return dID
     */
    public int getDID() {
        return dID;
    }


    /**
     * Sets the dID value for this ContentResult.
     * 
     * @param dID
     */
    public void setDID(int dID) {
        this.dID = dID;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ContentResult)) return false;
        ContentResult other = (ContentResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dDocName==null && other.getDDocName()==null) || 
             (this.dDocName!=null &&
              this.dDocName.equals(other.getDDocName()))) &&
            ((this.dDocTitle==null && other.getDDocTitle()==null) || 
             (this.dDocTitle!=null &&
              this.dDocTitle.equals(other.getDDocTitle()))) &&
            ((this.dOriginalName==null && other.getDOriginalName()==null) || 
             (this.dOriginalName!=null &&
              this.dOriginalName.equals(other.getDOriginalName()))) &&
            ((this.dFormat==null && other.getDFormat()==null) || 
             (this.dFormat!=null &&
              this.dFormat.equals(other.getDFormat()))) &&
            ((this.dExtension==null && other.getDExtension()==null) || 
             (this.dExtension!=null &&
              this.dExtension.equals(other.getDExtension()))) &&
            this.dFileSize == other.getDFileSize() &&
            ((this.xComments==null && other.getXComments()==null) || 
             (this.xComments!=null &&
              this.xComments.equals(other.getXComments()))) &&
            ((this.dWebURL==null && other.getDWebURL()==null) || 
             (this.dWebURL!=null &&
              this.dWebURL.equals(other.getDWebURL()))) &&
            this.dID == other.getDID();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDDocName() != null) {
            _hashCode += getDDocName().hashCode();
        }
        if (getDDocTitle() != null) {
            _hashCode += getDDocTitle().hashCode();
        }
        if (getDOriginalName() != null) {
            _hashCode += getDOriginalName().hashCode();
        }
        if (getDFormat() != null) {
            _hashCode += getDFormat().hashCode();
        }
        if (getDExtension() != null) {
            _hashCode += getDExtension().hashCode();
        }
        _hashCode += getDFileSize();
        if (getXComments() != null) {
            _hashCode += getXComments().hashCode();
        }
        if (getDWebURL() != null) {
            _hashCode += getDWebURL().hashCode();
        }
        _hashCode += getDID();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ContentResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_query_bynoberkas_ps_wsdl/", "contentResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDocName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dDocName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDocTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dDocTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOriginalName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dOriginalName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DExtension");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dExtension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DFileSize");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dFileSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("XComments");
        elemField.setXmlName(new javax.xml.namespace.QName("", "xComments"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DWebURL");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dWebURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
