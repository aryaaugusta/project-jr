package Bpm.Ecms.queryByNoBerkas;

public class Bpm_ecms_query_bynoberkas_ps_wsdlProxy implements Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdl_PortType {
  private String _endpoint = null;
  private Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdl_PortType bpm_ecms_query_bynoberkas_ps_wsdl_PortType = null;
  
  public Bpm_ecms_query_bynoberkas_ps_wsdlProxy() {
    _initBpm_ecms_query_bynoberkas_ps_wsdlProxy();
  }
  
  public Bpm_ecms_query_bynoberkas_ps_wsdlProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_ecms_query_bynoberkas_ps_wsdlProxy();
  }
  
  private void _initBpm_ecms_query_bynoberkas_ps_wsdlProxy() {
    try {
      bpm_ecms_query_bynoberkas_ps_wsdl_PortType = (new Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdl_ServiceLocator()).getbpm_ecms_query_bynoberkas_ps_wsdlSOAP();
      if (bpm_ecms_query_bynoberkas_ps_wsdl_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_ecms_query_bynoberkas_ps_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_ecms_query_bynoberkas_ps_wsdl_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_ecms_query_bynoberkas_ps_wsdl_PortType != null)
      ((javax.xml.rpc.Stub)bpm_ecms_query_bynoberkas_ps_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.Ecms.queryByNoBerkas.Bpm_ecms_query_bynoberkas_ps_wsdl_PortType getBpm_ecms_query_bynoberkas_ps_wsdl_PortType() {
    if (bpm_ecms_query_bynoberkas_ps_wsdl_PortType == null)
      _initBpm_ecms_query_bynoberkas_ps_wsdlProxy();
    return bpm_ecms_query_bynoberkas_ps_wsdl_PortType;
  }
  
  public Bpm.Ecms.queryByNoBerkas.Response get_ecms_id(Bpm.Ecms.queryByNoBerkas.DirectoryName parameters) throws java.rmi.RemoteException{
    if (bpm_ecms_query_bynoberkas_ps_wsdl_PortType == null)
      _initBpm_ecms_query_bynoberkas_ps_wsdlProxy();
    return bpm_ecms_query_bynoberkas_ps_wsdl_PortType.get_ecms_id(parameters);
  }
  
  
}