/**
 * Bpm_ecms_folder_create_wsdl_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.createFolder;

public class Bpm_ecms_folder_create_wsdl_ServiceLocator extends org.apache.axis.client.Service implements Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdl_Service {

    public Bpm_ecms_folder_create_wsdl_ServiceLocator() {
    }


    public Bpm_ecms_folder_create_wsdl_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_ecms_folder_create_wsdl_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for bpm_ecms_folder_create_wsdlSOAP
    private java.lang.String bpm_ecms_folder_create_wsdlSOAP_address = "http://soaserver:8011/JR-BPM/ECMS/Enterprise_Services/Proxy_Services/bpm_ecms_folder_create_ps";

    public java.lang.String getbpm_ecms_folder_create_wsdlSOAPAddress() {
        return bpm_ecms_folder_create_wsdlSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String bpm_ecms_folder_create_wsdlSOAPWSDDServiceName = "bpm_ecms_folder_create_wsdlSOAP";

    public java.lang.String getbpm_ecms_folder_create_wsdlSOAPWSDDServiceName() {
        return bpm_ecms_folder_create_wsdlSOAPWSDDServiceName;
    }

    public void setbpm_ecms_folder_create_wsdlSOAPWSDDServiceName(java.lang.String name) {
        bpm_ecms_folder_create_wsdlSOAPWSDDServiceName = name;
    }

    public Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdl_PortType getbpm_ecms_folder_create_wsdlSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(bpm_ecms_folder_create_wsdlSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getbpm_ecms_folder_create_wsdlSOAP(endpoint);
    }

    public Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdl_PortType getbpm_ecms_folder_create_wsdlSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdlSOAPStub _stub = new Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdlSOAPStub(portAddress, this);
            _stub.setPortName(getbpm_ecms_folder_create_wsdlSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setbpm_ecms_folder_create_wsdlSOAPEndpointAddress(java.lang.String address) {
        bpm_ecms_folder_create_wsdlSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdl_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdlSOAPStub _stub = new Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdlSOAPStub(new java.net.URL(bpm_ecms_folder_create_wsdlSOAP_address), this);
                _stub.setPortName(getbpm_ecms_folder_create_wsdlSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_ecms_folder_create_wsdlSOAP".equals(inputPortName)) {
            return getbpm_ecms_folder_create_wsdlSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_folder_create_wsdl/", "bpm_ecms_folder_create_wsdl");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_folder_create_wsdl/", "bpm_ecms_folder_create_wsdlSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("bpm_ecms_folder_create_wsdlSOAP".equals(portName)) {
            setbpm_ecms_folder_create_wsdlSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
