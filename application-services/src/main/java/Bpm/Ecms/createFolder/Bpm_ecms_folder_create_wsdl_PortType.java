/**
 * Bpm_ecms_folder_create_wsdl_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.createFolder;

public interface Bpm_ecms_folder_create_wsdl_PortType extends java.rmi.Remote {
    public void bpm_ecms_folder_create(java.lang.String hasParentCollectionPath, java.lang.String dParentCollectionPath, java.lang.String dCollectionName, java.lang.String dCollectionOwner, java.lang.String dSecurityGroup, javax.xml.rpc.holders.StringHolder statusCode, javax.xml.rpc.holders.StringHolder statusMessage, javax.xml.rpc.holders.StringHolder dCollectionID) throws java.rmi.RemoteException;
}
