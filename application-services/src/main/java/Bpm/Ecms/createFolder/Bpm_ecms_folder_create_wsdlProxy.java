package Bpm.Ecms.createFolder;

public class Bpm_ecms_folder_create_wsdlProxy implements Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdl_PortType {
  private String _endpoint = null;
  private Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdl_PortType bpm_ecms_folder_create_wsdl_PortType = null;
  
  public Bpm_ecms_folder_create_wsdlProxy() {
    _initBpm_ecms_folder_create_wsdlProxy();
  }
  
  public Bpm_ecms_folder_create_wsdlProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_ecms_folder_create_wsdlProxy();
  }
  
  private void _initBpm_ecms_folder_create_wsdlProxy() {
    try {
      bpm_ecms_folder_create_wsdl_PortType = (new Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdl_ServiceLocator()).getbpm_ecms_folder_create_wsdlSOAP();
      if (bpm_ecms_folder_create_wsdl_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_ecms_folder_create_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_ecms_folder_create_wsdl_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_ecms_folder_create_wsdl_PortType != null)
      ((javax.xml.rpc.Stub)bpm_ecms_folder_create_wsdl_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.Ecms.createFolder.Bpm_ecms_folder_create_wsdl_PortType getBpm_ecms_folder_create_wsdl_PortType() {
    if (bpm_ecms_folder_create_wsdl_PortType == null)
      _initBpm_ecms_folder_create_wsdlProxy();
    return bpm_ecms_folder_create_wsdl_PortType;
  }
  
  public void bpm_ecms_folder_create(java.lang.String hasParentCollectionPath, java.lang.String dParentCollectionPath, java.lang.String dCollectionName, java.lang.String dCollectionOwner, java.lang.String dSecurityGroup, javax.xml.rpc.holders.StringHolder statusCode, javax.xml.rpc.holders.StringHolder statusMessage, javax.xml.rpc.holders.StringHolder dCollectionID) throws java.rmi.RemoteException{
    if (bpm_ecms_folder_create_wsdl_PortType == null)
      _initBpm_ecms_folder_create_wsdlProxy();
    bpm_ecms_folder_create_wsdl_PortType.bpm_ecms_folder_create(hasParentCollectionPath, dParentCollectionPath, dCollectionName, dCollectionOwner, dSecurityGroup, statusCode, statusMessage, dCollectionID);
  }
  
  
}