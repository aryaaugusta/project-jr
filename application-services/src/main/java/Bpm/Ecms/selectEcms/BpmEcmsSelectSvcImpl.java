package Bpm.Ecms.selectEcms;

public class BpmEcmsSelectSvcImpl {
	
	public static Bpm.Ecms.selectEcmsSchema.BpmEcmsMapping[] bpm_ecms_mapping_querySelect(Bpm.Ecms.selectEcmsSchema.Bpm_ecms_mapping_querySelect_DIRECTORY_ID parameters) throws java.rmi.RemoteException{
		Bpm_ecms_mapping_query_pttProxy service = new Bpm_ecms_mapping_query_pttProxy();
		Bpm_ecms_mapping_query_ptt port = service.getBpm_ecms_mapping_query_ptt();
		return port.bpm_ecms_mapping_querySelect(parameters);
		
	}

}
