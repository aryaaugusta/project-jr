/**
 * Bpm_ecms_mapping_query_pttBindingQSServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.selectEcms;

public class Bpm_ecms_mapping_query_pttBindingQSServiceLocator extends org.apache.axis.client.Service implements Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_pttBindingQSService {

    public Bpm_ecms_mapping_query_pttBindingQSServiceLocator() {
    }


    public Bpm_ecms_mapping_query_pttBindingQSServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_ecms_mapping_query_pttBindingQSServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for Bpm_ecms_mapping_query_pttBindingQSPort
    private java.lang.String Bpm_ecms_mapping_query_pttBindingQSPort_address = "http://192.168.1.136:8011/JR-BPM/ECMS/Enterprise_Services/Proxy_Services/bpm_ecms_mapping_query_ps";

    public java.lang.String getBpm_ecms_mapping_query_pttBindingQSPortAddress() {
        return Bpm_ecms_mapping_query_pttBindingQSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String Bpm_ecms_mapping_query_pttBindingQSPortWSDDServiceName = "bpm_ecms_mapping_query_ptt-bindingQSPort";

    public java.lang.String getBpm_ecms_mapping_query_pttBindingQSPortWSDDServiceName() {
        return Bpm_ecms_mapping_query_pttBindingQSPortWSDDServiceName;
    }

    public void setBpm_ecms_mapping_query_pttBindingQSPortWSDDServiceName(java.lang.String name) {
        Bpm_ecms_mapping_query_pttBindingQSPortWSDDServiceName = name;
    }

    public Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_ptt getBpm_ecms_mapping_query_pttBindingQSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(Bpm_ecms_mapping_query_pttBindingQSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBpm_ecms_mapping_query_pttBindingQSPort(endpoint);
    }

    public Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_ptt getBpm_ecms_mapping_query_pttBindingQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_pttBindingStub _stub = new Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_pttBindingStub(portAddress, this);
            _stub.setPortName(getBpm_ecms_mapping_query_pttBindingQSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBpm_ecms_mapping_query_pttBindingQSPortEndpointAddress(java.lang.String address) {
        Bpm_ecms_mapping_query_pttBindingQSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_ptt.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_pttBindingStub _stub = new Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_pttBindingStub(new java.net.URL(Bpm_ecms_mapping_query_pttBindingQSPort_address), this);
                _stub.setPortName(getBpm_ecms_mapping_query_pttBindingQSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_ecms_mapping_query_ptt-bindingQSPort".equals(inputPortName)) {
            return getBpm_ecms_mapping_query_pttBindingQSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/JRBPMJCA/JCABPM/bpm_ecms_mapping_query", "bpm_ecms_mapping_query_ptt-bindingQSService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://xmlns.oracle.com/pcbpel/adapter/db/JRBPMJCA/JCABPM/bpm_ecms_mapping_query", "bpm_ecms_mapping_query_ptt-bindingQSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("Bpm_ecms_mapping_query_pttBindingQSPort".equals(portName)) {
            setBpm_ecms_mapping_query_pttBindingQSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
