/**
 * Bpm_ecms_mapping_query_ptt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.selectEcms;

public interface Bpm_ecms_mapping_query_ptt extends java.rmi.Remote {
    public Bpm.Ecms.selectEcmsSchema.BpmEcmsMapping[] bpm_ecms_mapping_querySelect(Bpm.Ecms.selectEcmsSchema.Bpm_ecms_mapping_querySelect_DIRECTORY_ID bpm_ecms_mapping_querySelect_inputParameters) throws java.rmi.RemoteException;
}
