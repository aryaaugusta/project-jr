package Bpm.Ecms.selectEcms;

public class Bpm_ecms_mapping_query_pttProxy implements Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_ptt {
  private String _endpoint = null;
  private Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_ptt bpm_ecms_mapping_query_ptt = null;
  
  public Bpm_ecms_mapping_query_pttProxy() {
    _initBpm_ecms_mapping_query_pttProxy();
  }
  
  public Bpm_ecms_mapping_query_pttProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_ecms_mapping_query_pttProxy();
  }
  
  private void _initBpm_ecms_mapping_query_pttProxy() {
    try {
      bpm_ecms_mapping_query_ptt = (new Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_pttBindingQSServiceLocator()).getBpm_ecms_mapping_query_pttBindingQSPort();
      if (bpm_ecms_mapping_query_ptt != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_ecms_mapping_query_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_ecms_mapping_query_ptt)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_ecms_mapping_query_ptt != null)
      ((javax.xml.rpc.Stub)bpm_ecms_mapping_query_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.Ecms.selectEcms.Bpm_ecms_mapping_query_ptt getBpm_ecms_mapping_query_ptt() {
    if (bpm_ecms_mapping_query_ptt == null)
      _initBpm_ecms_mapping_query_pttProxy();
    return bpm_ecms_mapping_query_ptt;
  }
  
  public Bpm.Ecms.selectEcmsSchema.BpmEcmsMapping[] bpm_ecms_mapping_querySelect(Bpm.Ecms.selectEcmsSchema.Bpm_ecms_mapping_querySelect_DIRECTORY_ID bpm_ecms_mapping_querySelect_inputParameters) throws java.rmi.RemoteException{
    if (bpm_ecms_mapping_query_ptt == null)
      _initBpm_ecms_mapping_query_pttProxy();
    return bpm_ecms_mapping_query_ptt.bpm_ecms_mapping_querySelect(bpm_ecms_mapping_querySelect_inputParameters);
  }
  
  
}