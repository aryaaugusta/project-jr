/**
 * Bpm_ecms_upload_wsdl_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.upload;

public interface Bpm_ecms_upload_wsdl_PortType extends java.rmi.Remote {
    public Bpm.Ecms.upload.AttachmentResultType[] bpm_ecms_upload(java.lang.String noBerkas, Bpm.Ecms.upload.AttachmentsType[] file_attachments) throws java.rmi.RemoteException;
}
