/**
 * Bpm_ecms_upload_wsdl_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.upload;

public interface Bpm_ecms_upload_wsdl_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_ecms_upload_wsdlSOAPAddress();

    public Bpm.Ecms.upload.Bpm_ecms_upload_wsdl_PortType getbpm_ecms_upload_wsdlSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.Ecms.upload.Bpm_ecms_upload_wsdl_PortType getbpm_ecms_upload_wsdlSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
