/**
 * Bpm_ecms_mapping_insert_ptt.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.insertEcmsInfo;

public interface Bpm_ecms_mapping_insert_ptt extends java.rmi.Remote {
    public void insert(Bpm.Ecms.insertEcmsInfoSchema.BpmEcmsMapping[] bpmEcmsMappingCollection) throws java.rmi.RemoteException;
}
