package Bpm.Ecms.insertEcmsInfo;

public class Bpm_ecms_mapping_insert_pttProxy implements Bpm.Ecms.insertEcmsInfo.Bpm_ecms_mapping_insert_ptt {
  private String _endpoint = null;
  private Bpm.Ecms.insertEcmsInfo.Bpm_ecms_mapping_insert_ptt bpm_ecms_mapping_insert_ptt = null;
  
  public Bpm_ecms_mapping_insert_pttProxy() {
    _initBpm_ecms_mapping_insert_pttProxy();
  }
  
  public Bpm_ecms_mapping_insert_pttProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_ecms_mapping_insert_pttProxy();
  }
  
  private void _initBpm_ecms_mapping_insert_pttProxy() {
    try {
      bpm_ecms_mapping_insert_ptt = (new Bpm.Ecms.insertEcmsInfo.Bpm_ecms_mapping_insert_pttBindingQSServiceLocator()).getBpm_ecms_mapping_insert_pttBindingQSPort();
      if (bpm_ecms_mapping_insert_ptt != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_ecms_mapping_insert_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_ecms_mapping_insert_ptt)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_ecms_mapping_insert_ptt != null)
      ((javax.xml.rpc.Stub)bpm_ecms_mapping_insert_ptt)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.Ecms.insertEcmsInfo.Bpm_ecms_mapping_insert_ptt getBpm_ecms_mapping_insert_ptt() {
    if (bpm_ecms_mapping_insert_ptt == null)
      _initBpm_ecms_mapping_insert_pttProxy();
    return bpm_ecms_mapping_insert_ptt;
  }
  
  public void insert(Bpm.Ecms.insertEcmsInfoSchema.BpmEcmsMapping[] bpmEcmsMappingCollection) throws java.rmi.RemoteException{
    if (bpm_ecms_mapping_insert_ptt == null)
      _initBpm_ecms_mapping_insert_pttProxy();
    bpm_ecms_mapping_insert_ptt.insert(bpmEcmsMappingCollection);
  }
  
  
}