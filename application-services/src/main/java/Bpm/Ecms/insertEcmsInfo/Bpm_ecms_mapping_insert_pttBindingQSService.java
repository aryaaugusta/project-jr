/**
 * Bpm_ecms_mapping_insert_pttBindingQSService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.insertEcmsInfo;

public interface Bpm_ecms_mapping_insert_pttBindingQSService extends javax.xml.rpc.Service {
    public java.lang.String getBpm_ecms_mapping_insert_pttBindingQSPortAddress();

    public Bpm.Ecms.insertEcmsInfo.Bpm_ecms_mapping_insert_ptt getBpm_ecms_mapping_insert_pttBindingQSPort() throws javax.xml.rpc.ServiceException;

    public Bpm.Ecms.insertEcmsInfo.Bpm_ecms_mapping_insert_ptt getBpm_ecms_mapping_insert_pttBindingQSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
