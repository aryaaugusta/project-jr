/**
 * Bpm_ecms_document_delete_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.documentDelete;

public interface Bpm_ecms_document_delete_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_ecms_document_deleteSOAPAddress();

    public Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_PortType getbpm_ecms_document_deleteSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_PortType getbpm_ecms_document_deleteSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
