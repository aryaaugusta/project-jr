/**
 * Bpm_ecms_document_delete_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.documentDelete;

public interface Bpm_ecms_document_delete_PortType extends java.rmi.Remote {
    public Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteResponse bpm_ecms_document_delete(Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteRequest parameters) throws java.rmi.RemoteException;
}
