/**
 * Bpm_ecms_document_delete_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.documentDelete;

public class Bpm_ecms_document_delete_ServiceLocator extends org.apache.axis.client.Service implements Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_Service {

    public Bpm_ecms_document_delete_ServiceLocator() {
    }


    public Bpm_ecms_document_delete_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public Bpm_ecms_document_delete_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for bpm_ecms_document_deleteSOAP
    private java.lang.String bpm_ecms_document_deleteSOAP_address = "http://192.168.1.136:8011/JR-BPM/ECMS/Enterprise_Services/Proxy_Services/bpm_ecms_document_delete_ps";

    public java.lang.String getbpm_ecms_document_deleteSOAPAddress() {
        return bpm_ecms_document_deleteSOAP_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String bpm_ecms_document_deleteSOAPWSDDServiceName = "bpm_ecms_document_deleteSOAP";

    public java.lang.String getbpm_ecms_document_deleteSOAPWSDDServiceName() {
        return bpm_ecms_document_deleteSOAPWSDDServiceName;
    }

    public void setbpm_ecms_document_deleteSOAPWSDDServiceName(java.lang.String name) {
        bpm_ecms_document_deleteSOAPWSDDServiceName = name;
    }

    public Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_PortType getbpm_ecms_document_deleteSOAP() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(bpm_ecms_document_deleteSOAP_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getbpm_ecms_document_deleteSOAP(endpoint);
    }

    public Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_PortType getbpm_ecms_document_deleteSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteSOAPStub _stub = new Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteSOAPStub(portAddress, this);
            _stub.setPortName(getbpm_ecms_document_deleteSOAPWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setbpm_ecms_document_deleteSOAPEndpointAddress(java.lang.String address) {
        bpm_ecms_document_deleteSOAP_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteSOAPStub _stub = new Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteSOAPStub(new java.net.URL(bpm_ecms_document_deleteSOAP_address), this);
                _stub.setPortName(getbpm_ecms_document_deleteSOAPWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("bpm_ecms_document_deleteSOAP".equals(inputPortName)) {
            return getbpm_ecms_document_deleteSOAP();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_document_delete/", "bpm_ecms_document_delete");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_document_delete/", "bpm_ecms_document_deleteSOAP"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("bpm_ecms_document_deleteSOAP".equals(portName)) {
            setbpm_ecms_document_deleteSOAPEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
