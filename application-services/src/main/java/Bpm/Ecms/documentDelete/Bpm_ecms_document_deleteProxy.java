package Bpm.Ecms.documentDelete;

public class Bpm_ecms_document_deleteProxy implements Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_PortType {
  private String _endpoint = null;
  private Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_PortType bpm_ecms_document_delete_PortType = null;
  
  public Bpm_ecms_document_deleteProxy() {
    _initBpm_ecms_document_deleteProxy();
  }
  
  public Bpm_ecms_document_deleteProxy(String endpoint) {
    _endpoint = endpoint;
    _initBpm_ecms_document_deleteProxy();
  }
  
  private void _initBpm_ecms_document_deleteProxy() {
    try {
      bpm_ecms_document_delete_PortType = (new Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_ServiceLocator()).getbpm_ecms_document_deleteSOAP();
      if (bpm_ecms_document_delete_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)bpm_ecms_document_delete_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)bpm_ecms_document_delete_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (bpm_ecms_document_delete_PortType != null)
      ((javax.xml.rpc.Stub)bpm_ecms_document_delete_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public Bpm.Ecms.documentDelete.Bpm_ecms_document_delete_PortType getBpm_ecms_document_delete_PortType() {
    if (bpm_ecms_document_delete_PortType == null)
      _initBpm_ecms_document_deleteProxy();
    return bpm_ecms_document_delete_PortType;
  }
  
  public Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteResponse bpm_ecms_document_delete(Bpm.Ecms.documentDelete.Bpm_ecms_document_deleteRequest parameters) throws java.rmi.RemoteException{
    if (bpm_ecms_document_delete_PortType == null)
      _initBpm_ecms_document_deleteProxy();
    return bpm_ecms_document_delete_PortType.bpm_ecms_document_delete(parameters);
  }
  
  
}