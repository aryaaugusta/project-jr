/**
 * QuickSearchResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.quickSearch;

public class QuickSearchResult  implements java.io.Serializable {
    private Bpm.Ecms.quickSearch.SearchResults[] searchResults;

    private Bpm.Ecms.quickSearch.SearchInfo searchInfo;

    private Bpm.Ecms.quickSearch.NavigationPages[] navigationPages;

    private Bpm.Ecms.quickSearch.StatusInfo statusInfo;

    public QuickSearchResult() {
    }

    public QuickSearchResult(
           Bpm.Ecms.quickSearch.SearchResults[] searchResults,
           Bpm.Ecms.quickSearch.SearchInfo searchInfo,
           Bpm.Ecms.quickSearch.NavigationPages[] navigationPages,
           Bpm.Ecms.quickSearch.StatusInfo statusInfo) {
           this.searchResults = searchResults;
           this.searchInfo = searchInfo;
           this.navigationPages = navigationPages;
           this.statusInfo = statusInfo;
    }


    /**
     * Gets the searchResults value for this QuickSearchResult.
     * 
     * @return searchResults
     */
    public Bpm.Ecms.quickSearch.SearchResults[] getSearchResults() {
        return searchResults;
    }


    /**
     * Sets the searchResults value for this QuickSearchResult.
     * 
     * @param searchResults
     */
    public void setSearchResults(Bpm.Ecms.quickSearch.SearchResults[] searchResults) {
        this.searchResults = searchResults;
    }

    public Bpm.Ecms.quickSearch.SearchResults getSearchResults(int i) {
        return this.searchResults[i];
    }

    public void setSearchResults(int i, Bpm.Ecms.quickSearch.SearchResults _value) {
        this.searchResults[i] = _value;
    }


    /**
     * Gets the searchInfo value for this QuickSearchResult.
     * 
     * @return searchInfo
     */
    public Bpm.Ecms.quickSearch.SearchInfo getSearchInfo() {
        return searchInfo;
    }


    /**
     * Sets the searchInfo value for this QuickSearchResult.
     * 
     * @param searchInfo
     */
    public void setSearchInfo(Bpm.Ecms.quickSearch.SearchInfo searchInfo) {
        this.searchInfo = searchInfo;
    }


    /**
     * Gets the navigationPages value for this QuickSearchResult.
     * 
     * @return navigationPages
     */
    public Bpm.Ecms.quickSearch.NavigationPages[] getNavigationPages() {
        return navigationPages;
    }


    /**
     * Sets the navigationPages value for this QuickSearchResult.
     * 
     * @param navigationPages
     */
    public void setNavigationPages(Bpm.Ecms.quickSearch.NavigationPages[] navigationPages) {
        this.navigationPages = navigationPages;
    }

    public Bpm.Ecms.quickSearch.NavigationPages getNavigationPages(int i) {
        return this.navigationPages[i];
    }

    public void setNavigationPages(int i, Bpm.Ecms.quickSearch.NavigationPages _value) {
        this.navigationPages[i] = _value;
    }


    /**
     * Gets the statusInfo value for this QuickSearchResult.
     * 
     * @return statusInfo
     */
    public Bpm.Ecms.quickSearch.StatusInfo getStatusInfo() {
        return statusInfo;
    }


    /**
     * Sets the statusInfo value for this QuickSearchResult.
     * 
     * @param statusInfo
     */
    public void setStatusInfo(Bpm.Ecms.quickSearch.StatusInfo statusInfo) {
        this.statusInfo = statusInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QuickSearchResult)) return false;
        QuickSearchResult other = (QuickSearchResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.searchResults==null && other.getSearchResults()==null) || 
             (this.searchResults!=null &&
              java.util.Arrays.equals(this.searchResults, other.getSearchResults()))) &&
            ((this.searchInfo==null && other.getSearchInfo()==null) || 
             (this.searchInfo!=null &&
              this.searchInfo.equals(other.getSearchInfo()))) &&
            ((this.navigationPages==null && other.getNavigationPages()==null) || 
             (this.navigationPages!=null &&
              java.util.Arrays.equals(this.navigationPages, other.getNavigationPages()))) &&
            ((this.statusInfo==null && other.getStatusInfo()==null) || 
             (this.statusInfo!=null &&
              this.statusInfo.equals(other.getStatusInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSearchResults() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSearchResults());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSearchResults(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSearchInfo() != null) {
            _hashCode += getSearchInfo().hashCode();
        }
        if (getNavigationPages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getNavigationPages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getNavigationPages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStatusInfo() != null) {
            _hashCode += getStatusInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QuickSearchResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "QuickSearchResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchResults");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SearchResults"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "SearchResults"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "SearchInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "SearchInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("navigationPages");
        elemField.setXmlName(new javax.xml.namespace.QName("", "NavigationPages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "NavigationPages"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "StatusInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "StatusInfo"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
