/**
 * Bpm_ecms_quick_search_wsdl_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.quickSearch;

public interface Bpm_ecms_quick_search_wsdl_PortType extends java.rmi.Remote {
    public Bpm.Ecms.quickSearch.Bpm_ecms_quick_searchResponse bpm_ecms_quick_search(Bpm.Ecms.quickSearch.Bpm_ecms_quick_searchRequest parameters) throws java.rmi.RemoteException;
}
