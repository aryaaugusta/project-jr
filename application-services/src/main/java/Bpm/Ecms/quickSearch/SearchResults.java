/**
 * SearchResults.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.quickSearch;

public class SearchResults  implements java.io.Serializable {
    private java.lang.Integer dID;

    private java.lang.Integer dRevisionID;

    private java.lang.String dDocName;

    private java.lang.String dDocTitle;

    private java.lang.String dDocType;

    private java.lang.String dDocAuthor;

    private java.lang.String dSecurityGroup;

    private java.lang.String dDocAccount;

    private java.lang.String dExtension;

    private java.lang.String dWebExtension;

    private java.lang.String dRevLabel;

    private java.lang.String dInDate;

    private java.lang.String dOutDate;

    private java.lang.String dFormat;

    private java.lang.String dOriginalName;

    private java.lang.String url;

    private java.lang.String dGif;

    private java.lang.Integer webFileSize;

    private java.lang.Integer vaultFileSize;

    private java.lang.Integer alternateFileSize;

    private java.lang.String alternateFormat;

    private java.lang.String dPublishType;

    private java.lang.String dRendition1;

    private java.lang.String dRendition2;

    private Bpm.Ecms.quickSearch.IdcProperty[] customDocMetaData;

    public SearchResults() {
    }

    public SearchResults(
           java.lang.Integer dID,
           java.lang.Integer dRevisionID,
           java.lang.String dDocName,
           java.lang.String dDocTitle,
           java.lang.String dDocType,
           java.lang.String dDocAuthor,
           java.lang.String dSecurityGroup,
           java.lang.String dDocAccount,
           java.lang.String dExtension,
           java.lang.String dWebExtension,
           java.lang.String dRevLabel,
           java.lang.String dInDate,
           java.lang.String dOutDate,
           java.lang.String dFormat,
           java.lang.String dOriginalName,
           java.lang.String url,
           java.lang.String dGif,
           java.lang.Integer webFileSize,
           java.lang.Integer vaultFileSize,
           java.lang.Integer alternateFileSize,
           java.lang.String alternateFormat,
           java.lang.String dPublishType,
           java.lang.String dRendition1,
           java.lang.String dRendition2,
           Bpm.Ecms.quickSearch.IdcProperty[] customDocMetaData) {
           this.dID = dID;
           this.dRevisionID = dRevisionID;
           this.dDocName = dDocName;
           this.dDocTitle = dDocTitle;
           this.dDocType = dDocType;
           this.dDocAuthor = dDocAuthor;
           this.dSecurityGroup = dSecurityGroup;
           this.dDocAccount = dDocAccount;
           this.dExtension = dExtension;
           this.dWebExtension = dWebExtension;
           this.dRevLabel = dRevLabel;
           this.dInDate = dInDate;
           this.dOutDate = dOutDate;
           this.dFormat = dFormat;
           this.dOriginalName = dOriginalName;
           this.url = url;
           this.dGif = dGif;
           this.webFileSize = webFileSize;
           this.vaultFileSize = vaultFileSize;
           this.alternateFileSize = alternateFileSize;
           this.alternateFormat = alternateFormat;
           this.dPublishType = dPublishType;
           this.dRendition1 = dRendition1;
           this.dRendition2 = dRendition2;
           this.customDocMetaData = customDocMetaData;
    }


    /**
     * Gets the dID value for this SearchResults.
     * 
     * @return dID
     */
    public java.lang.Integer getDID() {
        return dID;
    }


    /**
     * Sets the dID value for this SearchResults.
     * 
     * @param dID
     */
    public void setDID(java.lang.Integer dID) {
        this.dID = dID;
    }


    /**
     * Gets the dRevisionID value for this SearchResults.
     * 
     * @return dRevisionID
     */
    public java.lang.Integer getDRevisionID() {
        return dRevisionID;
    }


    /**
     * Sets the dRevisionID value for this SearchResults.
     * 
     * @param dRevisionID
     */
    public void setDRevisionID(java.lang.Integer dRevisionID) {
        this.dRevisionID = dRevisionID;
    }


    /**
     * Gets the dDocName value for this SearchResults.
     * 
     * @return dDocName
     */
    public java.lang.String getDDocName() {
        return dDocName;
    }


    /**
     * Sets the dDocName value for this SearchResults.
     * 
     * @param dDocName
     */
    public void setDDocName(java.lang.String dDocName) {
        this.dDocName = dDocName;
    }


    /**
     * Gets the dDocTitle value for this SearchResults.
     * 
     * @return dDocTitle
     */
    public java.lang.String getDDocTitle() {
        return dDocTitle;
    }


    /**
     * Sets the dDocTitle value for this SearchResults.
     * 
     * @param dDocTitle
     */
    public void setDDocTitle(java.lang.String dDocTitle) {
        this.dDocTitle = dDocTitle;
    }


    /**
     * Gets the dDocType value for this SearchResults.
     * 
     * @return dDocType
     */
    public java.lang.String getDDocType() {
        return dDocType;
    }


    /**
     * Sets the dDocType value for this SearchResults.
     * 
     * @param dDocType
     */
    public void setDDocType(java.lang.String dDocType) {
        this.dDocType = dDocType;
    }


    /**
     * Gets the dDocAuthor value for this SearchResults.
     * 
     * @return dDocAuthor
     */
    public java.lang.String getDDocAuthor() {
        return dDocAuthor;
    }


    /**
     * Sets the dDocAuthor value for this SearchResults.
     * 
     * @param dDocAuthor
     */
    public void setDDocAuthor(java.lang.String dDocAuthor) {
        this.dDocAuthor = dDocAuthor;
    }


    /**
     * Gets the dSecurityGroup value for this SearchResults.
     * 
     * @return dSecurityGroup
     */
    public java.lang.String getDSecurityGroup() {
        return dSecurityGroup;
    }


    /**
     * Sets the dSecurityGroup value for this SearchResults.
     * 
     * @param dSecurityGroup
     */
    public void setDSecurityGroup(java.lang.String dSecurityGroup) {
        this.dSecurityGroup = dSecurityGroup;
    }


    /**
     * Gets the dDocAccount value for this SearchResults.
     * 
     * @return dDocAccount
     */
    public java.lang.String getDDocAccount() {
        return dDocAccount;
    }


    /**
     * Sets the dDocAccount value for this SearchResults.
     * 
     * @param dDocAccount
     */
    public void setDDocAccount(java.lang.String dDocAccount) {
        this.dDocAccount = dDocAccount;
    }


    /**
     * Gets the dExtension value for this SearchResults.
     * 
     * @return dExtension
     */
    public java.lang.String getDExtension() {
        return dExtension;
    }


    /**
     * Sets the dExtension value for this SearchResults.
     * 
     * @param dExtension
     */
    public void setDExtension(java.lang.String dExtension) {
        this.dExtension = dExtension;
    }


    /**
     * Gets the dWebExtension value for this SearchResults.
     * 
     * @return dWebExtension
     */
    public java.lang.String getDWebExtension() {
        return dWebExtension;
    }


    /**
     * Sets the dWebExtension value for this SearchResults.
     * 
     * @param dWebExtension
     */
    public void setDWebExtension(java.lang.String dWebExtension) {
        this.dWebExtension = dWebExtension;
    }


    /**
     * Gets the dRevLabel value for this SearchResults.
     * 
     * @return dRevLabel
     */
    public java.lang.String getDRevLabel() {
        return dRevLabel;
    }


    /**
     * Sets the dRevLabel value for this SearchResults.
     * 
     * @param dRevLabel
     */
    public void setDRevLabel(java.lang.String dRevLabel) {
        this.dRevLabel = dRevLabel;
    }


    /**
     * Gets the dInDate value for this SearchResults.
     * 
     * @return dInDate
     */
    public java.lang.String getDInDate() {
        return dInDate;
    }


    /**
     * Sets the dInDate value for this SearchResults.
     * 
     * @param dInDate
     */
    public void setDInDate(java.lang.String dInDate) {
        this.dInDate = dInDate;
    }


    /**
     * Gets the dOutDate value for this SearchResults.
     * 
     * @return dOutDate
     */
    public java.lang.String getDOutDate() {
        return dOutDate;
    }


    /**
     * Sets the dOutDate value for this SearchResults.
     * 
     * @param dOutDate
     */
    public void setDOutDate(java.lang.String dOutDate) {
        this.dOutDate = dOutDate;
    }


    /**
     * Gets the dFormat value for this SearchResults.
     * 
     * @return dFormat
     */
    public java.lang.String getDFormat() {
        return dFormat;
    }


    /**
     * Sets the dFormat value for this SearchResults.
     * 
     * @param dFormat
     */
    public void setDFormat(java.lang.String dFormat) {
        this.dFormat = dFormat;
    }


    /**
     * Gets the dOriginalName value for this SearchResults.
     * 
     * @return dOriginalName
     */
    public java.lang.String getDOriginalName() {
        return dOriginalName;
    }


    /**
     * Sets the dOriginalName value for this SearchResults.
     * 
     * @param dOriginalName
     */
    public void setDOriginalName(java.lang.String dOriginalName) {
        this.dOriginalName = dOriginalName;
    }


    /**
     * Gets the url value for this SearchResults.
     * 
     * @return url
     */
    public java.lang.String getUrl() {
        return url;
    }


    /**
     * Sets the url value for this SearchResults.
     * 
     * @param url
     */
    public void setUrl(java.lang.String url) {
        this.url = url;
    }


    /**
     * Gets the dGif value for this SearchResults.
     * 
     * @return dGif
     */
    public java.lang.String getDGif() {
        return dGif;
    }


    /**
     * Sets the dGif value for this SearchResults.
     * 
     * @param dGif
     */
    public void setDGif(java.lang.String dGif) {
        this.dGif = dGif;
    }


    /**
     * Gets the webFileSize value for this SearchResults.
     * 
     * @return webFileSize
     */
    public java.lang.Integer getWebFileSize() {
        return webFileSize;
    }


    /**
     * Sets the webFileSize value for this SearchResults.
     * 
     * @param webFileSize
     */
    public void setWebFileSize(java.lang.Integer webFileSize) {
        this.webFileSize = webFileSize;
    }


    /**
     * Gets the vaultFileSize value for this SearchResults.
     * 
     * @return vaultFileSize
     */
    public java.lang.Integer getVaultFileSize() {
        return vaultFileSize;
    }


    /**
     * Sets the vaultFileSize value for this SearchResults.
     * 
     * @param vaultFileSize
     */
    public void setVaultFileSize(java.lang.Integer vaultFileSize) {
        this.vaultFileSize = vaultFileSize;
    }


    /**
     * Gets the alternateFileSize value for this SearchResults.
     * 
     * @return alternateFileSize
     */
    public java.lang.Integer getAlternateFileSize() {
        return alternateFileSize;
    }


    /**
     * Sets the alternateFileSize value for this SearchResults.
     * 
     * @param alternateFileSize
     */
    public void setAlternateFileSize(java.lang.Integer alternateFileSize) {
        this.alternateFileSize = alternateFileSize;
    }


    /**
     * Gets the alternateFormat value for this SearchResults.
     * 
     * @return alternateFormat
     */
    public java.lang.String getAlternateFormat() {
        return alternateFormat;
    }


    /**
     * Sets the alternateFormat value for this SearchResults.
     * 
     * @param alternateFormat
     */
    public void setAlternateFormat(java.lang.String alternateFormat) {
        this.alternateFormat = alternateFormat;
    }


    /**
     * Gets the dPublishType value for this SearchResults.
     * 
     * @return dPublishType
     */
    public java.lang.String getDPublishType() {
        return dPublishType;
    }


    /**
     * Sets the dPublishType value for this SearchResults.
     * 
     * @param dPublishType
     */
    public void setDPublishType(java.lang.String dPublishType) {
        this.dPublishType = dPublishType;
    }


    /**
     * Gets the dRendition1 value for this SearchResults.
     * 
     * @return dRendition1
     */
    public java.lang.String getDRendition1() {
        return dRendition1;
    }


    /**
     * Sets the dRendition1 value for this SearchResults.
     * 
     * @param dRendition1
     */
    public void setDRendition1(java.lang.String dRendition1) {
        this.dRendition1 = dRendition1;
    }


    /**
     * Gets the dRendition2 value for this SearchResults.
     * 
     * @return dRendition2
     */
    public java.lang.String getDRendition2() {
        return dRendition2;
    }


    /**
     * Sets the dRendition2 value for this SearchResults.
     * 
     * @param dRendition2
     */
    public void setDRendition2(java.lang.String dRendition2) {
        this.dRendition2 = dRendition2;
    }


    /**
     * Gets the customDocMetaData value for this SearchResults.
     * 
     * @return customDocMetaData
     */
    public Bpm.Ecms.quickSearch.IdcProperty[] getCustomDocMetaData() {
        return customDocMetaData;
    }


    /**
     * Sets the customDocMetaData value for this SearchResults.
     * 
     * @param customDocMetaData
     */
    public void setCustomDocMetaData(Bpm.Ecms.quickSearch.IdcProperty[] customDocMetaData) {
        this.customDocMetaData = customDocMetaData;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SearchResults)) return false;
        SearchResults other = (SearchResults) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.dID==null && other.getDID()==null) || 
             (this.dID!=null &&
              this.dID.equals(other.getDID()))) &&
            ((this.dRevisionID==null && other.getDRevisionID()==null) || 
             (this.dRevisionID!=null &&
              this.dRevisionID.equals(other.getDRevisionID()))) &&
            ((this.dDocName==null && other.getDDocName()==null) || 
             (this.dDocName!=null &&
              this.dDocName.equals(other.getDDocName()))) &&
            ((this.dDocTitle==null && other.getDDocTitle()==null) || 
             (this.dDocTitle!=null &&
              this.dDocTitle.equals(other.getDDocTitle()))) &&
            ((this.dDocType==null && other.getDDocType()==null) || 
             (this.dDocType!=null &&
              this.dDocType.equals(other.getDDocType()))) &&
            ((this.dDocAuthor==null && other.getDDocAuthor()==null) || 
             (this.dDocAuthor!=null &&
              this.dDocAuthor.equals(other.getDDocAuthor()))) &&
            ((this.dSecurityGroup==null && other.getDSecurityGroup()==null) || 
             (this.dSecurityGroup!=null &&
              this.dSecurityGroup.equals(other.getDSecurityGroup()))) &&
            ((this.dDocAccount==null && other.getDDocAccount()==null) || 
             (this.dDocAccount!=null &&
              this.dDocAccount.equals(other.getDDocAccount()))) &&
            ((this.dExtension==null && other.getDExtension()==null) || 
             (this.dExtension!=null &&
              this.dExtension.equals(other.getDExtension()))) &&
            ((this.dWebExtension==null && other.getDWebExtension()==null) || 
             (this.dWebExtension!=null &&
              this.dWebExtension.equals(other.getDWebExtension()))) &&
            ((this.dRevLabel==null && other.getDRevLabel()==null) || 
             (this.dRevLabel!=null &&
              this.dRevLabel.equals(other.getDRevLabel()))) &&
            ((this.dInDate==null && other.getDInDate()==null) || 
             (this.dInDate!=null &&
              this.dInDate.equals(other.getDInDate()))) &&
            ((this.dOutDate==null && other.getDOutDate()==null) || 
             (this.dOutDate!=null &&
              this.dOutDate.equals(other.getDOutDate()))) &&
            ((this.dFormat==null && other.getDFormat()==null) || 
             (this.dFormat!=null &&
              this.dFormat.equals(other.getDFormat()))) &&
            ((this.dOriginalName==null && other.getDOriginalName()==null) || 
             (this.dOriginalName!=null &&
              this.dOriginalName.equals(other.getDOriginalName()))) &&
            ((this.url==null && other.getUrl()==null) || 
             (this.url!=null &&
              this.url.equals(other.getUrl()))) &&
            ((this.dGif==null && other.getDGif()==null) || 
             (this.dGif!=null &&
              this.dGif.equals(other.getDGif()))) &&
            ((this.webFileSize==null && other.getWebFileSize()==null) || 
             (this.webFileSize!=null &&
              this.webFileSize.equals(other.getWebFileSize()))) &&
            ((this.vaultFileSize==null && other.getVaultFileSize()==null) || 
             (this.vaultFileSize!=null &&
              this.vaultFileSize.equals(other.getVaultFileSize()))) &&
            ((this.alternateFileSize==null && other.getAlternateFileSize()==null) || 
             (this.alternateFileSize!=null &&
              this.alternateFileSize.equals(other.getAlternateFileSize()))) &&
            ((this.alternateFormat==null && other.getAlternateFormat()==null) || 
             (this.alternateFormat!=null &&
              this.alternateFormat.equals(other.getAlternateFormat()))) &&
            ((this.dPublishType==null && other.getDPublishType()==null) || 
             (this.dPublishType!=null &&
              this.dPublishType.equals(other.getDPublishType()))) &&
            ((this.dRendition1==null && other.getDRendition1()==null) || 
             (this.dRendition1!=null &&
              this.dRendition1.equals(other.getDRendition1()))) &&
            ((this.dRendition2==null && other.getDRendition2()==null) || 
             (this.dRendition2!=null &&
              this.dRendition2.equals(other.getDRendition2()))) &&
            ((this.customDocMetaData==null && other.getCustomDocMetaData()==null) || 
             (this.customDocMetaData!=null &&
              java.util.Arrays.equals(this.customDocMetaData, other.getCustomDocMetaData())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDID() != null) {
            _hashCode += getDID().hashCode();
        }
        if (getDRevisionID() != null) {
            _hashCode += getDRevisionID().hashCode();
        }
        if (getDDocName() != null) {
            _hashCode += getDDocName().hashCode();
        }
        if (getDDocTitle() != null) {
            _hashCode += getDDocTitle().hashCode();
        }
        if (getDDocType() != null) {
            _hashCode += getDDocType().hashCode();
        }
        if (getDDocAuthor() != null) {
            _hashCode += getDDocAuthor().hashCode();
        }
        if (getDSecurityGroup() != null) {
            _hashCode += getDSecurityGroup().hashCode();
        }
        if (getDDocAccount() != null) {
            _hashCode += getDDocAccount().hashCode();
        }
        if (getDExtension() != null) {
            _hashCode += getDExtension().hashCode();
        }
        if (getDWebExtension() != null) {
            _hashCode += getDWebExtension().hashCode();
        }
        if (getDRevLabel() != null) {
            _hashCode += getDRevLabel().hashCode();
        }
        if (getDInDate() != null) {
            _hashCode += getDInDate().hashCode();
        }
        if (getDOutDate() != null) {
            _hashCode += getDOutDate().hashCode();
        }
        if (getDFormat() != null) {
            _hashCode += getDFormat().hashCode();
        }
        if (getDOriginalName() != null) {
            _hashCode += getDOriginalName().hashCode();
        }
        if (getUrl() != null) {
            _hashCode += getUrl().hashCode();
        }
        if (getDGif() != null) {
            _hashCode += getDGif().hashCode();
        }
        if (getWebFileSize() != null) {
            _hashCode += getWebFileSize().hashCode();
        }
        if (getVaultFileSize() != null) {
            _hashCode += getVaultFileSize().hashCode();
        }
        if (getAlternateFileSize() != null) {
            _hashCode += getAlternateFileSize().hashCode();
        }
        if (getAlternateFormat() != null) {
            _hashCode += getAlternateFormat().hashCode();
        }
        if (getDPublishType() != null) {
            _hashCode += getDPublishType().hashCode();
        }
        if (getDRendition1() != null) {
            _hashCode += getDRendition1().hashCode();
        }
        if (getDRendition2() != null) {
            _hashCode += getDRendition2().hashCode();
        }
        if (getCustomDocMetaData() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCustomDocMetaData());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCustomDocMetaData(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SearchResults.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "SearchResults"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DRevisionID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dRevisionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDocName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dDocName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDocTitle");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dDocTitle"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDocType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dDocType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDocAuthor");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dDocAuthor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DSecurityGroup");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dSecurityGroup"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DDocAccount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dDocAccount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DExtension");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dExtension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DWebExtension");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dWebExtension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DRevLabel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dRevLabel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DInDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dInDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOutDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dOutDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DOriginalName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dOriginalName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("url");
        elemField.setXmlName(new javax.xml.namespace.QName("", "url"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DGif");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dGif"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("webFileSize");
        elemField.setXmlName(new javax.xml.namespace.QName("", "webFileSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vaultFileSize");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vaultFileSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternateFileSize");
        elemField.setXmlName(new javax.xml.namespace.QName("", "alternateFileSize"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("alternateFormat");
        elemField.setXmlName(new javax.xml.namespace.QName("", "alternateFormat"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DPublishType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dPublishType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DRendition1");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dRendition1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("DRendition2");
        elemField.setXmlName(new javax.xml.namespace.QName("", "dRendition2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("customDocMetaData");
        elemField.setXmlName(new javax.xml.namespace.QName("", "CustomDocMetaData"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "IdcProperty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "property"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
