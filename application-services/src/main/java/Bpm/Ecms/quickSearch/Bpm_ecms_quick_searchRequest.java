/**
 * Bpm_ecms_quick_searchRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.quickSearch;

public class Bpm_ecms_quick_searchRequest  implements java.io.Serializable {
    private java.lang.String queryText;

    private Bpm.Ecms.quickSearch.IdcProperty[] extraProps;

    public Bpm_ecms_quick_searchRequest() {
    }

    public Bpm_ecms_quick_searchRequest(
           java.lang.String queryText,
           Bpm.Ecms.quickSearch.IdcProperty[] extraProps) {
           this.queryText = queryText;
           this.extraProps = extraProps;
    }


    /**
     * Gets the queryText value for this Bpm_ecms_quick_searchRequest.
     * 
     * @return queryText
     */
    public java.lang.String getQueryText() {
        return queryText;
    }


    /**
     * Sets the queryText value for this Bpm_ecms_quick_searchRequest.
     * 
     * @param queryText
     */
    public void setQueryText(java.lang.String queryText) {
        this.queryText = queryText;
    }


    /**
     * Gets the extraProps value for this Bpm_ecms_quick_searchRequest.
     * 
     * @return extraProps
     */
    public Bpm.Ecms.quickSearch.IdcProperty[] getExtraProps() {
        return extraProps;
    }


    /**
     * Sets the extraProps value for this Bpm_ecms_quick_searchRequest.
     * 
     * @param extraProps
     */
    public void setExtraProps(Bpm.Ecms.quickSearch.IdcProperty[] extraProps) {
        this.extraProps = extraProps;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_ecms_quick_searchRequest)) return false;
        Bpm_ecms_quick_searchRequest other = (Bpm_ecms_quick_searchRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.queryText==null && other.getQueryText()==null) || 
             (this.queryText!=null &&
              this.queryText.equals(other.getQueryText()))) &&
            ((this.extraProps==null && other.getExtraProps()==null) || 
             (this.extraProps!=null &&
              java.util.Arrays.equals(this.extraProps, other.getExtraProps())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getQueryText() != null) {
            _hashCode += getQueryText().hashCode();
        }
        if (getExtraProps() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getExtraProps());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getExtraProps(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_ecms_quick_searchRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", ">bpm_ecms_quick_searchRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queryText");
        elemField.setXmlName(new javax.xml.namespace.QName("", "queryText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extraProps");
        elemField.setXmlName(new javax.xml.namespace.QName("", "extraProps"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "IdcProperty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "property"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
