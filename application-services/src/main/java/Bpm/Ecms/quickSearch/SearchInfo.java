/**
 * SearchInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.quickSearch;

public class SearchInfo  implements java.io.Serializable {
    private java.lang.Integer startRow;

    private java.lang.Integer endRow;

    private java.lang.Integer pageNumber;

    private java.lang.Integer numPages;

    private java.lang.Integer totalRows;

    private java.lang.Integer totalDocsProcessed;

    public SearchInfo() {
    }

    public SearchInfo(
           java.lang.Integer startRow,
           java.lang.Integer endRow,
           java.lang.Integer pageNumber,
           java.lang.Integer numPages,
           java.lang.Integer totalRows,
           java.lang.Integer totalDocsProcessed) {
           this.startRow = startRow;
           this.endRow = endRow;
           this.pageNumber = pageNumber;
           this.numPages = numPages;
           this.totalRows = totalRows;
           this.totalDocsProcessed = totalDocsProcessed;
    }


    /**
     * Gets the startRow value for this SearchInfo.
     * 
     * @return startRow
     */
    public java.lang.Integer getStartRow() {
        return startRow;
    }


    /**
     * Sets the startRow value for this SearchInfo.
     * 
     * @param startRow
     */
    public void setStartRow(java.lang.Integer startRow) {
        this.startRow = startRow;
    }


    /**
     * Gets the endRow value for this SearchInfo.
     * 
     * @return endRow
     */
    public java.lang.Integer getEndRow() {
        return endRow;
    }


    /**
     * Sets the endRow value for this SearchInfo.
     * 
     * @param endRow
     */
    public void setEndRow(java.lang.Integer endRow) {
        this.endRow = endRow;
    }


    /**
     * Gets the pageNumber value for this SearchInfo.
     * 
     * @return pageNumber
     */
    public java.lang.Integer getPageNumber() {
        return pageNumber;
    }


    /**
     * Sets the pageNumber value for this SearchInfo.
     * 
     * @param pageNumber
     */
    public void setPageNumber(java.lang.Integer pageNumber) {
        this.pageNumber = pageNumber;
    }


    /**
     * Gets the numPages value for this SearchInfo.
     * 
     * @return numPages
     */
    public java.lang.Integer getNumPages() {
        return numPages;
    }


    /**
     * Sets the numPages value for this SearchInfo.
     * 
     * @param numPages
     */
    public void setNumPages(java.lang.Integer numPages) {
        this.numPages = numPages;
    }


    /**
     * Gets the totalRows value for this SearchInfo.
     * 
     * @return totalRows
     */
    public java.lang.Integer getTotalRows() {
        return totalRows;
    }


    /**
     * Sets the totalRows value for this SearchInfo.
     * 
     * @param totalRows
     */
    public void setTotalRows(java.lang.Integer totalRows) {
        this.totalRows = totalRows;
    }


    /**
     * Gets the totalDocsProcessed value for this SearchInfo.
     * 
     * @return totalDocsProcessed
     */
    public java.lang.Integer getTotalDocsProcessed() {
        return totalDocsProcessed;
    }


    /**
     * Sets the totalDocsProcessed value for this SearchInfo.
     * 
     * @param totalDocsProcessed
     */
    public void setTotalDocsProcessed(java.lang.Integer totalDocsProcessed) {
        this.totalDocsProcessed = totalDocsProcessed;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SearchInfo)) return false;
        SearchInfo other = (SearchInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.startRow==null && other.getStartRow()==null) || 
             (this.startRow!=null &&
              this.startRow.equals(other.getStartRow()))) &&
            ((this.endRow==null && other.getEndRow()==null) || 
             (this.endRow!=null &&
              this.endRow.equals(other.getEndRow()))) &&
            ((this.pageNumber==null && other.getPageNumber()==null) || 
             (this.pageNumber!=null &&
              this.pageNumber.equals(other.getPageNumber()))) &&
            ((this.numPages==null && other.getNumPages()==null) || 
             (this.numPages!=null &&
              this.numPages.equals(other.getNumPages()))) &&
            ((this.totalRows==null && other.getTotalRows()==null) || 
             (this.totalRows!=null &&
              this.totalRows.equals(other.getTotalRows()))) &&
            ((this.totalDocsProcessed==null && other.getTotalDocsProcessed()==null) || 
             (this.totalDocsProcessed!=null &&
              this.totalDocsProcessed.equals(other.getTotalDocsProcessed())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStartRow() != null) {
            _hashCode += getStartRow().hashCode();
        }
        if (getEndRow() != null) {
            _hashCode += getEndRow().hashCode();
        }
        if (getPageNumber() != null) {
            _hashCode += getPageNumber().hashCode();
        }
        if (getNumPages() != null) {
            _hashCode += getNumPages().hashCode();
        }
        if (getTotalRows() != null) {
            _hashCode += getTotalRows().hashCode();
        }
        if (getTotalDocsProcessed() != null) {
            _hashCode += getTotalDocsProcessed().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SearchInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "SearchInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startRow");
        elemField.setXmlName(new javax.xml.namespace.QName("", "startRow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endRow");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endRow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numPages");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numPages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalRows");
        elemField.setXmlName(new javax.xml.namespace.QName("", "totalRows"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("totalDocsProcessed");
        elemField.setXmlName(new javax.xml.namespace.QName("", "totalDocsProcessed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
