/**
 * NavigationPages.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.quickSearch;

public class NavigationPages  implements java.io.Serializable {
    private java.lang.Integer headerPageNumber;

    private java.lang.Integer pageReference;

    private java.lang.Integer pageNumber;

    private java.lang.Integer startRow;

    private java.lang.Integer endRow;

    public NavigationPages() {
    }

    public NavigationPages(
           java.lang.Integer headerPageNumber,
           java.lang.Integer pageReference,
           java.lang.Integer pageNumber,
           java.lang.Integer startRow,
           java.lang.Integer endRow) {
           this.headerPageNumber = headerPageNumber;
           this.pageReference = pageReference;
           this.pageNumber = pageNumber;
           this.startRow = startRow;
           this.endRow = endRow;
    }


    /**
     * Gets the headerPageNumber value for this NavigationPages.
     * 
     * @return headerPageNumber
     */
    public java.lang.Integer getHeaderPageNumber() {
        return headerPageNumber;
    }


    /**
     * Sets the headerPageNumber value for this NavigationPages.
     * 
     * @param headerPageNumber
     */
    public void setHeaderPageNumber(java.lang.Integer headerPageNumber) {
        this.headerPageNumber = headerPageNumber;
    }


    /**
     * Gets the pageReference value for this NavigationPages.
     * 
     * @return pageReference
     */
    public java.lang.Integer getPageReference() {
        return pageReference;
    }


    /**
     * Sets the pageReference value for this NavigationPages.
     * 
     * @param pageReference
     */
    public void setPageReference(java.lang.Integer pageReference) {
        this.pageReference = pageReference;
    }


    /**
     * Gets the pageNumber value for this NavigationPages.
     * 
     * @return pageNumber
     */
    public java.lang.Integer getPageNumber() {
        return pageNumber;
    }


    /**
     * Sets the pageNumber value for this NavigationPages.
     * 
     * @param pageNumber
     */
    public void setPageNumber(java.lang.Integer pageNumber) {
        this.pageNumber = pageNumber;
    }


    /**
     * Gets the startRow value for this NavigationPages.
     * 
     * @return startRow
     */
    public java.lang.Integer getStartRow() {
        return startRow;
    }


    /**
     * Sets the startRow value for this NavigationPages.
     * 
     * @param startRow
     */
    public void setStartRow(java.lang.Integer startRow) {
        this.startRow = startRow;
    }


    /**
     * Gets the endRow value for this NavigationPages.
     * 
     * @return endRow
     */
    public java.lang.Integer getEndRow() {
        return endRow;
    }


    /**
     * Sets the endRow value for this NavigationPages.
     * 
     * @param endRow
     */
    public void setEndRow(java.lang.Integer endRow) {
        this.endRow = endRow;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof NavigationPages)) return false;
        NavigationPages other = (NavigationPages) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.headerPageNumber==null && other.getHeaderPageNumber()==null) || 
             (this.headerPageNumber!=null &&
              this.headerPageNumber.equals(other.getHeaderPageNumber()))) &&
            ((this.pageReference==null && other.getPageReference()==null) || 
             (this.pageReference!=null &&
              this.pageReference.equals(other.getPageReference()))) &&
            ((this.pageNumber==null && other.getPageNumber()==null) || 
             (this.pageNumber!=null &&
              this.pageNumber.equals(other.getPageNumber()))) &&
            ((this.startRow==null && other.getStartRow()==null) || 
             (this.startRow!=null &&
              this.startRow.equals(other.getStartRow()))) &&
            ((this.endRow==null && other.getEndRow()==null) || 
             (this.endRow!=null &&
              this.endRow.equals(other.getEndRow())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getHeaderPageNumber() != null) {
            _hashCode += getHeaderPageNumber().hashCode();
        }
        if (getPageReference() != null) {
            _hashCode += getPageReference().hashCode();
        }
        if (getPageNumber() != null) {
            _hashCode += getPageNumber().hashCode();
        }
        if (getStartRow() != null) {
            _hashCode += getStartRow().hashCode();
        }
        if (getEndRow() != null) {
            _hashCode += getEndRow().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(NavigationPages.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "NavigationPages"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("headerPageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "headerPageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageReference");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pageReference"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "pageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("startRow");
        elemField.setXmlName(new javax.xml.namespace.QName("", "startRow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("endRow");
        elemField.setXmlName(new javax.xml.namespace.QName("", "endRow"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
