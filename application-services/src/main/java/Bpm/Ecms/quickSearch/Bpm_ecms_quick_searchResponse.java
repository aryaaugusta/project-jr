/**
 * Bpm_ecms_quick_searchResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.Ecms.quickSearch;

public class Bpm_ecms_quick_searchResponse  implements java.io.Serializable {
    private Bpm.Ecms.quickSearch.QuickSearchResult quickSearchResult;

    public Bpm_ecms_quick_searchResponse() {
    }

    public Bpm_ecms_quick_searchResponse(
           Bpm.Ecms.quickSearch.QuickSearchResult quickSearchResult) {
           this.quickSearchResult = quickSearchResult;
    }


    /**
     * Gets the quickSearchResult value for this Bpm_ecms_quick_searchResponse.
     * 
     * @return quickSearchResult
     */
    public Bpm.Ecms.quickSearch.QuickSearchResult getQuickSearchResult() {
        return quickSearchResult;
    }


    /**
     * Sets the quickSearchResult value for this Bpm_ecms_quick_searchResponse.
     * 
     * @param quickSearchResult
     */
    public void setQuickSearchResult(Bpm.Ecms.quickSearch.QuickSearchResult quickSearchResult) {
        this.quickSearchResult = quickSearchResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_ecms_quick_searchResponse)) return false;
        Bpm_ecms_quick_searchResponse other = (Bpm_ecms_quick_searchResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.quickSearchResult==null && other.getQuickSearchResult()==null) || 
             (this.quickSearchResult!=null &&
              this.quickSearchResult.equals(other.getQuickSearchResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getQuickSearchResult() != null) {
            _hashCode += getQuickSearchResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_ecms_quick_searchResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", ">bpm_ecms_quick_searchResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("quickSearchResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "QuickSearchResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_ecms_quick_search_wsdl/", "QuickSearchResult"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
