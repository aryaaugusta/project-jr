/**
 * Bpm_query_taskRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.QueryTaskBpm;

public class Bpm_query_taskRequest  implements java.io.Serializable {
    private Bpm.QueryTaskBpm.CredentialType credential;

    private Bpm.QueryTaskBpm.PredicateType predicate;

    private java.lang.String optionalInfo;

    public Bpm_query_taskRequest() {
    }

    public Bpm_query_taskRequest(
           Bpm.QueryTaskBpm.CredentialType credential,
           Bpm.QueryTaskBpm.PredicateType predicate,
           java.lang.String optionalInfo) {
           this.credential = credential;
           this.predicate = predicate;
           this.optionalInfo = optionalInfo;
    }


    /**
     * Gets the credential value for this Bpm_query_taskRequest.
     * 
     * @return credential
     */
    public Bpm.QueryTaskBpm.CredentialType getCredential() {
        return credential;
    }


    /**
     * Sets the credential value for this Bpm_query_taskRequest.
     * 
     * @param credential
     */
    public void setCredential(Bpm.QueryTaskBpm.CredentialType credential) {
        this.credential = credential;
    }


    /**
     * Gets the predicate value for this Bpm_query_taskRequest.
     * 
     * @return predicate
     */
    public Bpm.QueryTaskBpm.PredicateType getPredicate() {
        return predicate;
    }


    /**
     * Sets the predicate value for this Bpm_query_taskRequest.
     * 
     * @param predicate
     */
    public void setPredicate(Bpm.QueryTaskBpm.PredicateType predicate) {
        this.predicate = predicate;
    }


    /**
     * Gets the optionalInfo value for this Bpm_query_taskRequest.
     * 
     * @return optionalInfo
     */
    public java.lang.String getOptionalInfo() {
        return optionalInfo;
    }


    /**
     * Sets the optionalInfo value for this Bpm_query_taskRequest.
     * 
     * @param optionalInfo
     */
    public void setOptionalInfo(java.lang.String optionalInfo) {
        this.optionalInfo = optionalInfo;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_query_taskRequest)) return false;
        Bpm_query_taskRequest other = (Bpm_query_taskRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.credential==null && other.getCredential()==null) || 
             (this.credential!=null &&
              this.credential.equals(other.getCredential()))) &&
            ((this.predicate==null && other.getPredicate()==null) || 
             (this.predicate!=null &&
              this.predicate.equals(other.getPredicate()))) &&
            ((this.optionalInfo==null && other.getOptionalInfo()==null) || 
             (this.optionalInfo!=null &&
              this.optionalInfo.equals(other.getOptionalInfo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCredential() != null) {
            _hashCode += getCredential().hashCode();
        }
        if (getPredicate() != null) {
            _hashCode += getPredicate().hashCode();
        }
        if (getOptionalInfo() != null) {
            _hashCode += getOptionalInfo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_query_taskRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", ">bpm_query_taskRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("credential");
        elemField.setXmlName(new javax.xml.namespace.QName("", "credential"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "credentialType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("predicate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "predicate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "predicateType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("optionalInfo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "optionalInfo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
