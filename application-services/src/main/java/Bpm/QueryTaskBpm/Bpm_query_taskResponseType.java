/**
 * Bpm_query_taskResponseType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.QueryTaskBpm;

public class Bpm_query_taskResponseType  implements java.io.Serializable {
    private Bpm.QueryTaskBpm.TaskType[] taskList;

    public Bpm_query_taskResponseType() {
    }

    public Bpm_query_taskResponseType(
           Bpm.QueryTaskBpm.TaskType[] taskList) {
           this.taskList = taskList;
    }


    /**
     * Gets the taskList value for this Bpm_query_taskResponseType.
     * 
     * @return taskList
     */
    public Bpm.QueryTaskBpm.TaskType[] getTaskList() {
        return taskList;
    }


    /**
     * Sets the taskList value for this Bpm_query_taskResponseType.
     * 
     * @param taskList
     */
    public void setTaskList(Bpm.QueryTaskBpm.TaskType[] taskList) {
        this.taskList = taskList;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Bpm_query_taskResponseType)) return false;
        Bpm_query_taskResponseType other = (Bpm_query_taskResponseType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.taskList==null && other.getTaskList()==null) || 
             (this.taskList!=null &&
              java.util.Arrays.equals(this.taskList, other.getTaskList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTaskList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTaskList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTaskList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Bpm_query_taskResponseType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "bpm_query_taskResponseType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taskList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "taskType"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("", "task"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
