/**
 * SystemAttributesType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.QueryTaskBpm;

public class SystemAttributesType  implements java.io.Serializable {
    private java.util.Calendar assignedDate;

    private java.util.Calendar createdDate;

    private boolean digitalSignatureRequired;

    private Bpm.QueryTaskBpm.FromUserType fromUser;

    private java.math.BigInteger numberOfTimesModified;

    private java.lang.String state;

    private Bpm.QueryTaskBpm.SystemActionsType[] systemActions;

    private java.lang.String taskId;

    private java.math.BigInteger taskNumber;

    private Bpm.QueryTaskBpm.UpdatedByType updatedBy;

    private java.util.Calendar updatedDate;

    private java.math.BigInteger version;

    private java.lang.String taskDefinitionId;

    private java.lang.String workflowPattern;

    private java.lang.String participantName;

    private Bpm.QueryTaskBpm.ReviewersType[] reviewers;

    private Bpm.QueryTaskBpm.AssigneesType[] assignees;

    private boolean isPartialTask;

    private java.lang.String organizationalUnitId;

    private java.lang.String swimlaneRole;

    private boolean isDecomposedTask;

    private java.lang.String formName;

    public SystemAttributesType() {
    }

    public SystemAttributesType(
           java.util.Calendar assignedDate,
           java.util.Calendar createdDate,
           boolean digitalSignatureRequired,
           Bpm.QueryTaskBpm.FromUserType fromUser,
           java.math.BigInteger numberOfTimesModified,
           java.lang.String state,
           Bpm.QueryTaskBpm.SystemActionsType[] systemActions,
           java.lang.String taskId,
           java.math.BigInteger taskNumber,
           Bpm.QueryTaskBpm.UpdatedByType updatedBy,
           java.util.Calendar updatedDate,
           java.math.BigInteger version,
           java.lang.String taskDefinitionId,
           java.lang.String workflowPattern,
           java.lang.String participantName,
           Bpm.QueryTaskBpm.ReviewersType[] reviewers,
           Bpm.QueryTaskBpm.AssigneesType[] assignees,
           boolean isPartialTask,
           java.lang.String organizationalUnitId,
           java.lang.String swimlaneRole,
           boolean isDecomposedTask,
           java.lang.String formName) {
           this.assignedDate = assignedDate;
           this.createdDate = createdDate;
           this.digitalSignatureRequired = digitalSignatureRequired;
           this.fromUser = fromUser;
           this.numberOfTimesModified = numberOfTimesModified;
           this.state = state;
           this.systemActions = systemActions;
           this.taskId = taskId;
           this.taskNumber = taskNumber;
           this.updatedBy = updatedBy;
           this.updatedDate = updatedDate;
           this.version = version;
           this.taskDefinitionId = taskDefinitionId;
           this.workflowPattern = workflowPattern;
           this.participantName = participantName;
           this.reviewers = reviewers;
           this.assignees = assignees;
           this.isPartialTask = isPartialTask;
           this.organizationalUnitId = organizationalUnitId;
           this.swimlaneRole = swimlaneRole;
           this.isDecomposedTask = isDecomposedTask;
           this.formName = formName;
    }


    /**
     * Gets the assignedDate value for this SystemAttributesType.
     * 
     * @return assignedDate
     */
    public java.util.Calendar getAssignedDate() {
        return assignedDate;
    }


    /**
     * Sets the assignedDate value for this SystemAttributesType.
     * 
     * @param assignedDate
     */
    public void setAssignedDate(java.util.Calendar assignedDate) {
        this.assignedDate = assignedDate;
    }


    /**
     * Gets the createdDate value for this SystemAttributesType.
     * 
     * @return createdDate
     */
    public java.util.Calendar getCreatedDate() {
        return createdDate;
    }


    /**
     * Sets the createdDate value for this SystemAttributesType.
     * 
     * @param createdDate
     */
    public void setCreatedDate(java.util.Calendar createdDate) {
        this.createdDate = createdDate;
    }


    /**
     * Gets the digitalSignatureRequired value for this SystemAttributesType.
     * 
     * @return digitalSignatureRequired
     */
    public boolean isDigitalSignatureRequired() {
        return digitalSignatureRequired;
    }


    /**
     * Sets the digitalSignatureRequired value for this SystemAttributesType.
     * 
     * @param digitalSignatureRequired
     */
    public void setDigitalSignatureRequired(boolean digitalSignatureRequired) {
        this.digitalSignatureRequired = digitalSignatureRequired;
    }


    /**
     * Gets the fromUser value for this SystemAttributesType.
     * 
     * @return fromUser
     */
    public Bpm.QueryTaskBpm.FromUserType getFromUser() {
        return fromUser;
    }


    /**
     * Sets the fromUser value for this SystemAttributesType.
     * 
     * @param fromUser
     */
    public void setFromUser(Bpm.QueryTaskBpm.FromUserType fromUser) {
        this.fromUser = fromUser;
    }


    /**
     * Gets the numberOfTimesModified value for this SystemAttributesType.
     * 
     * @return numberOfTimesModified
     */
    public java.math.BigInteger getNumberOfTimesModified() {
        return numberOfTimesModified;
    }


    /**
     * Sets the numberOfTimesModified value for this SystemAttributesType.
     * 
     * @param numberOfTimesModified
     */
    public void setNumberOfTimesModified(java.math.BigInteger numberOfTimesModified) {
        this.numberOfTimesModified = numberOfTimesModified;
    }


    /**
     * Gets the state value for this SystemAttributesType.
     * 
     * @return state
     */
    public java.lang.String getState() {
        return state;
    }


    /**
     * Sets the state value for this SystemAttributesType.
     * 
     * @param state
     */
    public void setState(java.lang.String state) {
        this.state = state;
    }


    /**
     * Gets the systemActions value for this SystemAttributesType.
     * 
     * @return systemActions
     */
    public Bpm.QueryTaskBpm.SystemActionsType[] getSystemActions() {
        return systemActions;
    }


    /**
     * Sets the systemActions value for this SystemAttributesType.
     * 
     * @param systemActions
     */
    public void setSystemActions(Bpm.QueryTaskBpm.SystemActionsType[] systemActions) {
        this.systemActions = systemActions;
    }

    public Bpm.QueryTaskBpm.SystemActionsType getSystemActions(int i) {
        return this.systemActions[i];
    }

    public void setSystemActions(int i, Bpm.QueryTaskBpm.SystemActionsType _value) {
        this.systemActions[i] = _value;
    }


    /**
     * Gets the taskId value for this SystemAttributesType.
     * 
     * @return taskId
     */
    public java.lang.String getTaskId() {
        return taskId;
    }


    /**
     * Sets the taskId value for this SystemAttributesType.
     * 
     * @param taskId
     */
    public void setTaskId(java.lang.String taskId) {
        this.taskId = taskId;
    }


    /**
     * Gets the taskNumber value for this SystemAttributesType.
     * 
     * @return taskNumber
     */
    public java.math.BigInteger getTaskNumber() {
        return taskNumber;
    }


    /**
     * Sets the taskNumber value for this SystemAttributesType.
     * 
     * @param taskNumber
     */
    public void setTaskNumber(java.math.BigInteger taskNumber) {
        this.taskNumber = taskNumber;
    }


    /**
     * Gets the updatedBy value for this SystemAttributesType.
     * 
     * @return updatedBy
     */
    public Bpm.QueryTaskBpm.UpdatedByType getUpdatedBy() {
        return updatedBy;
    }


    /**
     * Sets the updatedBy value for this SystemAttributesType.
     * 
     * @param updatedBy
     */
    public void setUpdatedBy(Bpm.QueryTaskBpm.UpdatedByType updatedBy) {
        this.updatedBy = updatedBy;
    }


    /**
     * Gets the updatedDate value for this SystemAttributesType.
     * 
     * @return updatedDate
     */
    public java.util.Calendar getUpdatedDate() {
        return updatedDate;
    }


    /**
     * Sets the updatedDate value for this SystemAttributesType.
     * 
     * @param updatedDate
     */
    public void setUpdatedDate(java.util.Calendar updatedDate) {
        this.updatedDate = updatedDate;
    }


    /**
     * Gets the version value for this SystemAttributesType.
     * 
     * @return version
     */
    public java.math.BigInteger getVersion() {
        return version;
    }


    /**
     * Sets the version value for this SystemAttributesType.
     * 
     * @param version
     */
    public void setVersion(java.math.BigInteger version) {
        this.version = version;
    }


    /**
     * Gets the taskDefinitionId value for this SystemAttributesType.
     * 
     * @return taskDefinitionId
     */
    public java.lang.String getTaskDefinitionId() {
        return taskDefinitionId;
    }


    /**
     * Sets the taskDefinitionId value for this SystemAttributesType.
     * 
     * @param taskDefinitionId
     */
    public void setTaskDefinitionId(java.lang.String taskDefinitionId) {
        this.taskDefinitionId = taskDefinitionId;
    }


    /**
     * Gets the workflowPattern value for this SystemAttributesType.
     * 
     * @return workflowPattern
     */
    public java.lang.String getWorkflowPattern() {
        return workflowPattern;
    }


    /**
     * Sets the workflowPattern value for this SystemAttributesType.
     * 
     * @param workflowPattern
     */
    public void setWorkflowPattern(java.lang.String workflowPattern) {
        this.workflowPattern = workflowPattern;
    }


    /**
     * Gets the participantName value for this SystemAttributesType.
     * 
     * @return participantName
     */
    public java.lang.String getParticipantName() {
        return participantName;
    }


    /**
     * Sets the participantName value for this SystemAttributesType.
     * 
     * @param participantName
     */
    public void setParticipantName(java.lang.String participantName) {
        this.participantName = participantName;
    }


    /**
     * Gets the reviewers value for this SystemAttributesType.
     * 
     * @return reviewers
     */
    public Bpm.QueryTaskBpm.ReviewersType[] getReviewers() {
        return reviewers;
    }


    /**
     * Sets the reviewers value for this SystemAttributesType.
     * 
     * @param reviewers
     */
    public void setReviewers(Bpm.QueryTaskBpm.ReviewersType[] reviewers) {
        this.reviewers = reviewers;
    }

    public Bpm.QueryTaskBpm.ReviewersType getReviewers(int i) {
        return this.reviewers[i];
    }

    public void setReviewers(int i, Bpm.QueryTaskBpm.ReviewersType _value) {
        this.reviewers[i] = _value;
    }


    /**
     * Gets the assignees value for this SystemAttributesType.
     * 
     * @return assignees
     */
    public Bpm.QueryTaskBpm.AssigneesType[] getAssignees() {
        return assignees;
    }


    /**
     * Sets the assignees value for this SystemAttributesType.
     * 
     * @param assignees
     */
    public void setAssignees(Bpm.QueryTaskBpm.AssigneesType[] assignees) {
        this.assignees = assignees;
    }

    public Bpm.QueryTaskBpm.AssigneesType getAssignees(int i) {
        return this.assignees[i];
    }

    public void setAssignees(int i, Bpm.QueryTaskBpm.AssigneesType _value) {
        this.assignees[i] = _value;
    }


    /**
     * Gets the isPartialTask value for this SystemAttributesType.
     * 
     * @return isPartialTask
     */
    public boolean isIsPartialTask() {
        return isPartialTask;
    }


    /**
     * Sets the isPartialTask value for this SystemAttributesType.
     * 
     * @param isPartialTask
     */
    public void setIsPartialTask(boolean isPartialTask) {
        this.isPartialTask = isPartialTask;
    }


    /**
     * Gets the organizationalUnitId value for this SystemAttributesType.
     * 
     * @return organizationalUnitId
     */
    public java.lang.String getOrganizationalUnitId() {
        return organizationalUnitId;
    }


    /**
     * Sets the organizationalUnitId value for this SystemAttributesType.
     * 
     * @param organizationalUnitId
     */
    public void setOrganizationalUnitId(java.lang.String organizationalUnitId) {
        this.organizationalUnitId = organizationalUnitId;
    }


    /**
     * Gets the swimlaneRole value for this SystemAttributesType.
     * 
     * @return swimlaneRole
     */
    public java.lang.String getSwimlaneRole() {
        return swimlaneRole;
    }


    /**
     * Sets the swimlaneRole value for this SystemAttributesType.
     * 
     * @param swimlaneRole
     */
    public void setSwimlaneRole(java.lang.String swimlaneRole) {
        this.swimlaneRole = swimlaneRole;
    }


    /**
     * Gets the isDecomposedTask value for this SystemAttributesType.
     * 
     * @return isDecomposedTask
     */
    public boolean isIsDecomposedTask() {
        return isDecomposedTask;
    }


    /**
     * Sets the isDecomposedTask value for this SystemAttributesType.
     * 
     * @param isDecomposedTask
     */
    public void setIsDecomposedTask(boolean isDecomposedTask) {
        this.isDecomposedTask = isDecomposedTask;
    }


    /**
     * Gets the formName value for this SystemAttributesType.
     * 
     * @return formName
     */
    public java.lang.String getFormName() {
        return formName;
    }


    /**
     * Sets the formName value for this SystemAttributesType.
     * 
     * @param formName
     */
    public void setFormName(java.lang.String formName) {
        this.formName = formName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SystemAttributesType)) return false;
        SystemAttributesType other = (SystemAttributesType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.assignedDate==null && other.getAssignedDate()==null) || 
             (this.assignedDate!=null &&
              this.assignedDate.equals(other.getAssignedDate()))) &&
            ((this.createdDate==null && other.getCreatedDate()==null) || 
             (this.createdDate!=null &&
              this.createdDate.equals(other.getCreatedDate()))) &&
            this.digitalSignatureRequired == other.isDigitalSignatureRequired() &&
            ((this.fromUser==null && other.getFromUser()==null) || 
             (this.fromUser!=null &&
              this.fromUser.equals(other.getFromUser()))) &&
            ((this.numberOfTimesModified==null && other.getNumberOfTimesModified()==null) || 
             (this.numberOfTimesModified!=null &&
              this.numberOfTimesModified.equals(other.getNumberOfTimesModified()))) &&
            ((this.state==null && other.getState()==null) || 
             (this.state!=null &&
              this.state.equals(other.getState()))) &&
            ((this.systemActions==null && other.getSystemActions()==null) || 
             (this.systemActions!=null &&
              java.util.Arrays.equals(this.systemActions, other.getSystemActions()))) &&
            ((this.taskId==null && other.getTaskId()==null) || 
             (this.taskId!=null &&
              this.taskId.equals(other.getTaskId()))) &&
            ((this.taskNumber==null && other.getTaskNumber()==null) || 
             (this.taskNumber!=null &&
              this.taskNumber.equals(other.getTaskNumber()))) &&
            ((this.updatedBy==null && other.getUpdatedBy()==null) || 
             (this.updatedBy!=null &&
              this.updatedBy.equals(other.getUpdatedBy()))) &&
            ((this.updatedDate==null && other.getUpdatedDate()==null) || 
             (this.updatedDate!=null &&
              this.updatedDate.equals(other.getUpdatedDate()))) &&
            ((this.version==null && other.getVersion()==null) || 
             (this.version!=null &&
              this.version.equals(other.getVersion()))) &&
            ((this.taskDefinitionId==null && other.getTaskDefinitionId()==null) || 
             (this.taskDefinitionId!=null &&
              this.taskDefinitionId.equals(other.getTaskDefinitionId()))) &&
            ((this.workflowPattern==null && other.getWorkflowPattern()==null) || 
             (this.workflowPattern!=null &&
              this.workflowPattern.equals(other.getWorkflowPattern()))) &&
            ((this.participantName==null && other.getParticipantName()==null) || 
             (this.participantName!=null &&
              this.participantName.equals(other.getParticipantName()))) &&
            ((this.reviewers==null && other.getReviewers()==null) || 
             (this.reviewers!=null &&
              java.util.Arrays.equals(this.reviewers, other.getReviewers()))) &&
            ((this.assignees==null && other.getAssignees()==null) || 
             (this.assignees!=null &&
              java.util.Arrays.equals(this.assignees, other.getAssignees()))) &&
            this.isPartialTask == other.isIsPartialTask() &&
            ((this.organizationalUnitId==null && other.getOrganizationalUnitId()==null) || 
             (this.organizationalUnitId!=null &&
              this.organizationalUnitId.equals(other.getOrganizationalUnitId()))) &&
            ((this.swimlaneRole==null && other.getSwimlaneRole()==null) || 
             (this.swimlaneRole!=null &&
              this.swimlaneRole.equals(other.getSwimlaneRole()))) &&
            this.isDecomposedTask == other.isIsDecomposedTask() &&
            ((this.formName==null && other.getFormName()==null) || 
             (this.formName!=null &&
              this.formName.equals(other.getFormName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAssignedDate() != null) {
            _hashCode += getAssignedDate().hashCode();
        }
        if (getCreatedDate() != null) {
            _hashCode += getCreatedDate().hashCode();
        }
        _hashCode += (isDigitalSignatureRequired() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getFromUser() != null) {
            _hashCode += getFromUser().hashCode();
        }
        if (getNumberOfTimesModified() != null) {
            _hashCode += getNumberOfTimesModified().hashCode();
        }
        if (getState() != null) {
            _hashCode += getState().hashCode();
        }
        if (getSystemActions() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSystemActions());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSystemActions(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTaskId() != null) {
            _hashCode += getTaskId().hashCode();
        }
        if (getTaskNumber() != null) {
            _hashCode += getTaskNumber().hashCode();
        }
        if (getUpdatedBy() != null) {
            _hashCode += getUpdatedBy().hashCode();
        }
        if (getUpdatedDate() != null) {
            _hashCode += getUpdatedDate().hashCode();
        }
        if (getVersion() != null) {
            _hashCode += getVersion().hashCode();
        }
        if (getTaskDefinitionId() != null) {
            _hashCode += getTaskDefinitionId().hashCode();
        }
        if (getWorkflowPattern() != null) {
            _hashCode += getWorkflowPattern().hashCode();
        }
        if (getParticipantName() != null) {
            _hashCode += getParticipantName().hashCode();
        }
        if (getReviewers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getReviewers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getReviewers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAssignees() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAssignees());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAssignees(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += (isIsPartialTask() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getOrganizationalUnitId() != null) {
            _hashCode += getOrganizationalUnitId().hashCode();
        }
        if (getSwimlaneRole() != null) {
            _hashCode += getSwimlaneRole().hashCode();
        }
        _hashCode += (isIsDecomposedTask() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getFormName() != null) {
            _hashCode += getFormName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SystemAttributesType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "systemAttributesType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assignedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assignedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "createdDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("digitalSignatureRequired");
        elemField.setXmlName(new javax.xml.namespace.QName("", "digitalSignatureRequired"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fromUser");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fromUser"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "fromUserType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberOfTimesModified");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numberOfTimesModified"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("state");
        elemField.setXmlName(new javax.xml.namespace.QName("", "state"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("systemActions");
        elemField.setXmlName(new javax.xml.namespace.QName("", "systemActions"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "systemActionsType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taskId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taskNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("", "updatedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "updatedByType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("updatedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("", "updatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("version");
        elemField.setXmlName(new javax.xml.namespace.QName("", "version"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "integer"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("taskDefinitionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "taskDefinitionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("workflowPattern");
        elemField.setXmlName(new javax.xml.namespace.QName("", "workflowPattern"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("participantName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "participantName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("reviewers");
        elemField.setXmlName(new javax.xml.namespace.QName("", "reviewers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "reviewersType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("assignees");
        elemField.setXmlName(new javax.xml.namespace.QName("", "assignees"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.example.org/bpm_query_task_wsdl/", "assigneesType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isPartialTask");
        elemField.setXmlName(new javax.xml.namespace.QName("", "isPartialTask"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("organizationalUnitId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "organizationalUnitId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("swimlaneRole");
        elemField.setXmlName(new javax.xml.namespace.QName("", "swimlaneRole"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isDecomposedTask");
        elemField.setXmlName(new javax.xml.namespace.QName("", "isDecomposedTask"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formName");
        elemField.setXmlName(new javax.xml.namespace.QName("", "formName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
