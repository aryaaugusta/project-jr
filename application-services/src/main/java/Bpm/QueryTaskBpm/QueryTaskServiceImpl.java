package Bpm.QueryTaskBpm;

import java.rmi.RemoteException;

public class QueryTaskServiceImpl {
	public static Bpm_query_taskResponseType bpmQueryAllAsigned(
			Bpm_query_taskRequest parameters) throws RemoteException {

		Bpm_query_task_wsdlProxy service = new Bpm_query_task_wsdlProxy();
		Bpm_query_task_wsdl_PortType port = service
				.getBpm_query_task_wsdl_PortType();
		return port.bpm_query_task(parameters);
	}

}
