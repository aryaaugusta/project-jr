/**
 * Bpm_query_task_wsdl_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package Bpm.QueryTaskBpm;

public interface Bpm_query_task_wsdl_Service extends javax.xml.rpc.Service {
    public java.lang.String getbpm_query_task_wsdlSOAPAddress();

    public Bpm.QueryTaskBpm.Bpm_query_task_wsdl_PortType getbpm_query_task_wsdlSOAP() throws javax.xml.rpc.ServiceException;

    public Bpm.QueryTaskBpm.Bpm_query_task_wsdl_PortType getbpm_query_task_wsdlSOAP(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
