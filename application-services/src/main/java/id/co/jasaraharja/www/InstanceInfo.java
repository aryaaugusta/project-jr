/**
 * InstanceInfo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package id.co.jasaraharja.www;

public class InstanceInfo  implements java.io.Serializable {
    private java.lang.String nomorPermohonan;

    private java.lang.String kodeCabang;

    private java.lang.String createdBy;

    private java.lang.String creationDate;

    private java.lang.String lastUpdatedBy;

    private java.lang.String lastUpdatedDate;

    private java.lang.String idGUID;

    private java.lang.String pengajuanType;

    private java.lang.String kodeKantor;

    private java.lang.String otherInfo1;

    private java.lang.String otherInfo2;

    private java.lang.String otherInfo3;

    private java.lang.String otherInfo4;

    private java.lang.String otherInfo5;

    public InstanceInfo() {
    }

    public InstanceInfo(
           java.lang.String nomorPermohonan,
           java.lang.String kodeCabang,
           java.lang.String createdBy,
           java.lang.String creationDate,
           java.lang.String lastUpdatedBy,
           java.lang.String lastUpdatedDate,
           java.lang.String idGUID,
           java.lang.String pengajuanType,
           java.lang.String kodeKantor,
           java.lang.String otherInfo1,
           java.lang.String otherInfo2,
           java.lang.String otherInfo3,
           java.lang.String otherInfo4,
           java.lang.String otherInfo5) {
           this.nomorPermohonan = nomorPermohonan;
           this.kodeCabang = kodeCabang;
           this.createdBy = createdBy;
           this.creationDate = creationDate;
           this.lastUpdatedBy = lastUpdatedBy;
           this.lastUpdatedDate = lastUpdatedDate;
           this.idGUID = idGUID;
           this.pengajuanType = pengajuanType;
           this.kodeKantor = kodeKantor;
           this.otherInfo1 = otherInfo1;
           this.otherInfo2 = otherInfo2;
           this.otherInfo3 = otherInfo3;
           this.otherInfo4 = otherInfo4;
           this.otherInfo5 = otherInfo5;
    }


    /**
     * Gets the nomorPermohonan value for this InstanceInfo.
     * 
     * @return nomorPermohonan
     */
    public java.lang.String getNomorPermohonan() {
        return nomorPermohonan;
    }


    /**
     * Sets the nomorPermohonan value for this InstanceInfo.
     * 
     * @param nomorPermohonan
     */
    public void setNomorPermohonan(java.lang.String nomorPermohonan) {
        this.nomorPermohonan = nomorPermohonan;
    }


    /**
     * Gets the kodeCabang value for this InstanceInfo.
     * 
     * @return kodeCabang
     */
    public java.lang.String getKodeCabang() {
        return kodeCabang;
    }


    /**
     * Sets the kodeCabang value for this InstanceInfo.
     * 
     * @param kodeCabang
     */
    public void setKodeCabang(java.lang.String kodeCabang) {
        this.kodeCabang = kodeCabang;
    }


    /**
     * Gets the createdBy value for this InstanceInfo.
     * 
     * @return createdBy
     */
    public java.lang.String getCreatedBy() {
        return createdBy;
    }


    /**
     * Sets the createdBy value for this InstanceInfo.
     * 
     * @param createdBy
     */
    public void setCreatedBy(java.lang.String createdBy) {
        this.createdBy = createdBy;
    }


    /**
     * Gets the creationDate value for this InstanceInfo.
     * 
     * @return creationDate
     */
    public java.lang.String getCreationDate() {
        return creationDate;
    }


    /**
     * Sets the creationDate value for this InstanceInfo.
     * 
     * @param creationDate
     */
    public void setCreationDate(java.lang.String creationDate) {
        this.creationDate = creationDate;
    }


    /**
     * Gets the lastUpdatedBy value for this InstanceInfo.
     * 
     * @return lastUpdatedBy
     */
    public java.lang.String getLastUpdatedBy() {
        return lastUpdatedBy;
    }


    /**
     * Sets the lastUpdatedBy value for this InstanceInfo.
     * 
     * @param lastUpdatedBy
     */
    public void setLastUpdatedBy(java.lang.String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }


    /**
     * Gets the lastUpdatedDate value for this InstanceInfo.
     * 
     * @return lastUpdatedDate
     */
    public java.lang.String getLastUpdatedDate() {
        return lastUpdatedDate;
    }


    /**
     * Sets the lastUpdatedDate value for this InstanceInfo.
     * 
     * @param lastUpdatedDate
     */
    public void setLastUpdatedDate(java.lang.String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }


    /**
     * Gets the idGUID value for this InstanceInfo.
     * 
     * @return idGUID
     */
    public java.lang.String getIdGUID() {
        return idGUID;
    }


    /**
     * Sets the idGUID value for this InstanceInfo.
     * 
     * @param idGUID
     */
    public void setIdGUID(java.lang.String idGUID) {
        this.idGUID = idGUID;
    }


    /**
     * Gets the pengajuanType value for this InstanceInfo.
     * 
     * @return pengajuanType
     */
    public java.lang.String getPengajuanType() {
        return pengajuanType;
    }


    /**
     * Sets the pengajuanType value for this InstanceInfo.
     * 
     * @param pengajuanType
     */
    public void setPengajuanType(java.lang.String pengajuanType) {
        this.pengajuanType = pengajuanType;
    }


    /**
     * Gets the kodeKantor value for this InstanceInfo.
     * 
     * @return kodeKantor
     */
    public java.lang.String getKodeKantor() {
        return kodeKantor;
    }


    /**
     * Sets the kodeKantor value for this InstanceInfo.
     * 
     * @param kodeKantor
     */
    public void setKodeKantor(java.lang.String kodeKantor) {
        this.kodeKantor = kodeKantor;
    }


    /**
     * Gets the otherInfo1 value for this InstanceInfo.
     * 
     * @return otherInfo1
     */
    public java.lang.String getOtherInfo1() {
        return otherInfo1;
    }


    /**
     * Sets the otherInfo1 value for this InstanceInfo.
     * 
     * @param otherInfo1
     */
    public void setOtherInfo1(java.lang.String otherInfo1) {
        this.otherInfo1 = otherInfo1;
    }


    /**
     * Gets the otherInfo2 value for this InstanceInfo.
     * 
     * @return otherInfo2
     */
    public java.lang.String getOtherInfo2() {
        return otherInfo2;
    }


    /**
     * Sets the otherInfo2 value for this InstanceInfo.
     * 
     * @param otherInfo2
     */
    public void setOtherInfo2(java.lang.String otherInfo2) {
        this.otherInfo2 = otherInfo2;
    }


    /**
     * Gets the otherInfo3 value for this InstanceInfo.
     * 
     * @return otherInfo3
     */
    public java.lang.String getOtherInfo3() {
        return otherInfo3;
    }


    /**
     * Sets the otherInfo3 value for this InstanceInfo.
     * 
     * @param otherInfo3
     */
    public void setOtherInfo3(java.lang.String otherInfo3) {
        this.otherInfo3 = otherInfo3;
    }


    /**
     * Gets the otherInfo4 value for this InstanceInfo.
     * 
     * @return otherInfo4
     */
    public java.lang.String getOtherInfo4() {
        return otherInfo4;
    }


    /**
     * Sets the otherInfo4 value for this InstanceInfo.
     * 
     * @param otherInfo4
     */
    public void setOtherInfo4(java.lang.String otherInfo4) {
        this.otherInfo4 = otherInfo4;
    }


    /**
     * Gets the otherInfo5 value for this InstanceInfo.
     * 
     * @return otherInfo5
     */
    public java.lang.String getOtherInfo5() {
        return otherInfo5;
    }


    /**
     * Sets the otherInfo5 value for this InstanceInfo.
     * 
     * @param otherInfo5
     */
    public void setOtherInfo5(java.lang.String otherInfo5) {
        this.otherInfo5 = otherInfo5;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof InstanceInfo)) return false;
        InstanceInfo other = (InstanceInfo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.nomorPermohonan==null && other.getNomorPermohonan()==null) || 
             (this.nomorPermohonan!=null &&
              this.nomorPermohonan.equals(other.getNomorPermohonan()))) &&
            ((this.kodeCabang==null && other.getKodeCabang()==null) || 
             (this.kodeCabang!=null &&
              this.kodeCabang.equals(other.getKodeCabang()))) &&
            ((this.createdBy==null && other.getCreatedBy()==null) || 
             (this.createdBy!=null &&
              this.createdBy.equals(other.getCreatedBy()))) &&
            ((this.creationDate==null && other.getCreationDate()==null) || 
             (this.creationDate!=null &&
              this.creationDate.equals(other.getCreationDate()))) &&
            ((this.lastUpdatedBy==null && other.getLastUpdatedBy()==null) || 
             (this.lastUpdatedBy!=null &&
              this.lastUpdatedBy.equals(other.getLastUpdatedBy()))) &&
            ((this.lastUpdatedDate==null && other.getLastUpdatedDate()==null) || 
             (this.lastUpdatedDate!=null &&
              this.lastUpdatedDate.equals(other.getLastUpdatedDate()))) &&
            ((this.idGUID==null && other.getIdGUID()==null) || 
             (this.idGUID!=null &&
              this.idGUID.equals(other.getIdGUID()))) &&
            ((this.pengajuanType==null && other.getPengajuanType()==null) || 
             (this.pengajuanType!=null &&
              this.pengajuanType.equals(other.getPengajuanType()))) &&
            ((this.kodeKantor==null && other.getKodeKantor()==null) || 
             (this.kodeKantor!=null &&
              this.kodeKantor.equals(other.getKodeKantor()))) &&
            ((this.otherInfo1==null && other.getOtherInfo1()==null) || 
             (this.otherInfo1!=null &&
              this.otherInfo1.equals(other.getOtherInfo1()))) &&
            ((this.otherInfo2==null && other.getOtherInfo2()==null) || 
             (this.otherInfo2!=null &&
              this.otherInfo2.equals(other.getOtherInfo2()))) &&
            ((this.otherInfo3==null && other.getOtherInfo3()==null) || 
             (this.otherInfo3!=null &&
              this.otherInfo3.equals(other.getOtherInfo3()))) &&
            ((this.otherInfo4==null && other.getOtherInfo4()==null) || 
             (this.otherInfo4!=null &&
              this.otherInfo4.equals(other.getOtherInfo4()))) &&
            ((this.otherInfo5==null && other.getOtherInfo5()==null) || 
             (this.otherInfo5!=null &&
              this.otherInfo5.equals(other.getOtherInfo5())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getNomorPermohonan() != null) {
            _hashCode += getNomorPermohonan().hashCode();
        }
        if (getKodeCabang() != null) {
            _hashCode += getKodeCabang().hashCode();
        }
        if (getCreatedBy() != null) {
            _hashCode += getCreatedBy().hashCode();
        }
        if (getCreationDate() != null) {
            _hashCode += getCreationDate().hashCode();
        }
        if (getLastUpdatedBy() != null) {
            _hashCode += getLastUpdatedBy().hashCode();
        }
        if (getLastUpdatedDate() != null) {
            _hashCode += getLastUpdatedDate().hashCode();
        }
        if (getIdGUID() != null) {
            _hashCode += getIdGUID().hashCode();
        }
        if (getPengajuanType() != null) {
            _hashCode += getPengajuanType().hashCode();
        }
        if (getKodeKantor() != null) {
            _hashCode += getKodeKantor().hashCode();
        }
        if (getOtherInfo1() != null) {
            _hashCode += getOtherInfo1().hashCode();
        }
        if (getOtherInfo2() != null) {
            _hashCode += getOtherInfo2().hashCode();
        }
        if (getOtherInfo3() != null) {
            _hashCode += getOtherInfo3().hashCode();
        }
        if (getOtherInfo4() != null) {
            _hashCode += getOtherInfo4().hashCode();
        }
        if (getOtherInfo5() != null) {
            _hashCode += getOtherInfo5().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(InstanceInfo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", ">InstanceInfo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nomorPermohonan");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "nomorPermohonan"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kodeCabang");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "kodeCabang"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("createdBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "createdBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("creationDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "creationDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdatedBy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "lastUpdatedBy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("lastUpdatedDate");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "lastUpdatedDate"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idGUID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "idGUID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pengajuanType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "pengajuanType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("kodeKantor");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "kodeKantor"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "otherInfo1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "otherInfo2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "otherInfo3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "otherInfo4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("otherInfo5");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.jasaraharja.co.id", "otherInfo5"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
