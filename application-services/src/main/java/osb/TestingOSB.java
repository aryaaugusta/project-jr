package osb;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import Bpm.QueryTaskBpm.Bpm_query_taskRequest;
import Bpm.QueryTaskBpm.Bpm_query_taskResponseType;
import Bpm.QueryTaskBpm.Bpm_query_task_wsdlProxy;
import Bpm.QueryTaskBpm.Bpm_query_task_wsdl_PortType;
import Bpm.StartProcess.Bpm_start_processProxy;
import Bpm.StartProcess.Bpm_start_processRequest;
import Bpm.StartProcess.Bpm_start_processResponse;
import Bpm.StartProcess.Bpm_start_process_PortType;


public class TestingOSB implements Serializable {
		//public static String hallo = new String("hello ini osb");
	
	private static Bpm_query_taskResponseType bpmQueryAllAsigned(Bpm_query_taskRequest parameters) throws RemoteException {
		
		Bpm_query_task_wsdlProxy service = new Bpm_query_task_wsdlProxy();
		Bpm_query_task_wsdl_PortType port = service.getBpm_query_task_wsdl_PortType();
		return port.bpm_query_task(parameters);
	}
	
private static Bpm_start_processResponse bpmStartProcess(Bpm_start_processRequest parameters) throws RemoteException {
		
		Bpm_start_processProxy service = new Bpm_start_processProxy();
		Bpm_start_process_PortType port = service.getBpm_start_process_PortType();
		return port.bpm_start_process(parameters);
	}
	
	public static void main(String[] args) throws RemoteException {
//		Bpm_query_taskRequest req = new Bpm_query_taskRequest();
//		String[] valueList = new String[1];
//		valueList[0] = "ASSIGNED";
//		
//		CredentialType credentialType = new CredentialType();
//		credentialType.setLogin("weblogic");
//		credentialType.setPassword("welcome1");
//		//---------------------------------
//		
//		ColumnType col = new ColumnType();
//		ClauseType clause = new ClauseType();
//		OnPredicateType onPredicateType = new OnPredicateType();
//		
//		col.setColumnName("state");
//		clause.setColumn(col);
//		clause.setOperator("in");
//		clause.setValueList(valueList);
//		
//		onPredicateType.setClause(clause);
//  		
//		PredicateType predicateType = new PredicateType();
//		predicateType.setAssignmentFilter("all");
//		predicateType.setPredicate(onPredicateType);
//		
//		req.setCredential(credentialType);
//		req.setPredicate(predicateType);
//		
//		Bpm_query_taskResponseType res = new Bpm_query_taskResponseType();
//		res = bpmQueryAllAsigned(req);
//		
//		for (TaskType tasklist : res.getTaskList()) {
//			System.out.println(tasklist.getTitle());
//		}
		
//		String pattern = "dd-MM-yyyy";
//		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//
//		String dateFormat = simpleDateFormat.format(new Date());
//		System.out.println(dateFormat);
//		
//		Bpm_start_processRequest req = new Bpm_start_processRequest();
//		req.setTipeCabang("AB");
//		req.setNomorPermohonan("91289829");
//		req.setKodeCabang("0200001");
//		req.setCreatedBy("userTest03");
//		req.setCreationDate(dateFormat);
//		req.setLastUpdatedBy("userTest03");
//		req.setLastUpdatedDate(dateFormat);
//		req.setIdGUID("N");
//		req.setPengajuanType("0200001");
//		req.setKodeKantor("02");
//		req.setOtherInfo1("a");
//		req.setOtherInfo2("a");
//		req.setOtherInfo3("a");
//		req.setOtherInfo4("a");
//		req.setOtherInfo5("a");
//		
//		//StartProcessSvcImpl startProcess = new StartProcessSvcImpl();
//		try {
//			
//			Bpm_start_processResponse resp = bpmStartProcess(req);
//			System.out.println("Response Service BPM : " + resp.getResponse().getResponseCode());
//		} catch (RemoteException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
		
}
