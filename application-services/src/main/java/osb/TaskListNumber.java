package osb;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.apache.axis.types.PositiveInteger;

import Bpm.GetTaskDetail.Bpm_get_task_detail_by_numberRequest;
import Bpm.GetTaskDetail.Bpm_get_task_detail_by_numberResponse;
import Bpm.GetTaskDetail.GetTaskDetailServiceImpl;
import Bpm.InstanceInfo.InstanceInfo;
import Bpm.ListTaskByUser.UserTask;
import Bpm.ListTaskByUser.UserTaskService;
import Bpm.QueryTaskBpm.Bpm_query_taskRequest;
import Bpm.QueryTaskBpm.Bpm_query_taskResponseType;
import Bpm.QueryTaskBpm.ClauseType;
import Bpm.QueryTaskBpm.ColumnType;
import Bpm.QueryTaskBpm.CredentialType;
import Bpm.QueryTaskBpm.PredicateType;
import Bpm.QueryTaskBpm.QueryTaskServiceImpl;
import Bpm.QueryTaskBpm.TaskType;


public class TaskListNumber {

	public static void main(String[] args) throws RemoteException {
		// TODO Auto-generated method stub
		try{
		
		List<UserTask> userTask = UserTaskService.getUserTasks("userTest02", "welcome1");
		for(UserTask u : userTask){
			System.out.println("Task Number : " + u.getTaskNumber().toString());
		}
		}catch(Exception e){
			System.out.println("Data tidak ada di BPM");
			e.printStackTrace();
		}
	}
	
public static List<UserTask> getUserTasks(String login, String password) throws RemoteException{
		
		Bpm_query_taskRequest req = new Bpm_query_taskRequest();
		String[] valueList = new String[1];
		valueList[0] = "ASSIGNED";
		
		CredentialType credentialType = new CredentialType();
		credentialType.setLogin(login);
		credentialType.setPassword(password);
		//---------------------------------
		
		ColumnType col = new ColumnType();
		ClauseType[] clause = new ClauseType[1];

		
		col.setColumnName("state");
		clause[0].setColumn(col);
		clause[0].setOperator("in");
		clause[0].setValueList(valueList);
		
  		
		PredicateType predicateType = new PredicateType();
		predicateType.setAssignmentFilter("GROUP");
		predicateType.setPredicate(clause);
		
		req.setCredential(credentialType);
		req.setPredicate(predicateType);
		req.setOptionalInfo("Payload");
		
		Bpm_query_taskResponseType res = new Bpm_query_taskResponseType();
		res = QueryTaskServiceImpl.bpmQueryAllAsigned(req);
		
		List<TaskType> taskList = new ArrayList<TaskType>();
		
		for (TaskType task : res.getTaskList()) {
			taskList.add(task);
			System.out.println(task.getTitle());
		}
		
		
		
		Bpm_get_task_detail_by_numberRequest reqTask = new Bpm_get_task_detail_by_numberRequest();
		Bpm.GetTaskDetail.CredentialType credential = new Bpm.GetTaskDetail.CredentialType();
		
		credential.setLogin(login);
		credential.setPassword(password);
		credential.setIdentityContext("");
		credential.setOnBehalfOfUser("");
		
		List<UserTask> listTask = new ArrayList<>();
		
		try{
						
			for (TaskType o : res.getTaskList()){
				UserTask userTask = new UserTask();

				
//				BigInteger taskNum = o.getSystemAttributes().getTaskNumber();
//				
//				reqTask.setTaskNumber(new PositiveInteger(taskNum.toString()));
//				reqTask.setCredential(credential);
//				
//				Bpm_get_task_detail_by_numberResponse resTask = GetTaskDetailServiceImpl.bpmGetTaskDetail(reqTask);
				
				userTask.setNomorPermohonan(o.getPayload().getInstanceInfo().getNomorPermohonan());
				userTask.setKodeCabang(o.getPayload().getInstanceInfo().getKodeCabang());
				userTask.setCreatedBy(o.getPayload().getInstanceInfo().getCreatedBy());
				userTask.setCreationDate(o.getPayload().getInstanceInfo().getCreationDate());
				userTask.setLastUpdatedBy(o.getPayload().getInstanceInfo().getLastUpdatedBy());
				userTask.setLastUpdateDate(o.getPayload().getInstanceInfo().getLastUpdatedDate());
				userTask.setIdGUID(o.getPayload().getInstanceInfo().getIdGUID());
				userTask.setPengajuanType(o.getPayload().getInstanceInfo().getPengajuanType());
				userTask.setKodeKantor(o.getPayload().getInstanceInfo().getKodeKantor());
				userTask.setOtherInfo1(o.getPayload().getInstanceInfo().getOtherInfo1());
				userTask.setOtherInfo2(o.getPayload().getInstanceInfo().getOtherInfo2());
				userTask.setOtherInfo3(o.getPayload().getInstanceInfo().getOtherInfo3());
				userTask.setOtherInfo4(o.getPayload().getInstanceInfo().getOtherInfo4());
				userTask.setOtherInfo5(o.getPayload().getInstanceInfo().getOtherInfo5());
				userTask.setRole(o.getSystemAttributes().getSwimlaneRole());
				userTask.setTaskTitle(o.getTitle());
				userTask.setTaskNumber(o.getSystemAttributes().getTaskNumber());
				
				System.out.println("ID Kecelakaan : " + userTask.getNomorPermohonan());
				System.out.println("Task Number : " + userTask.getTaskNumber());
				
				
				listTask.add(userTask);
			}
			
			System.out.println("JumlahTask : " + listTask.size());
			
	
			} catch (Exception e){
				e.printStackTrace();
				System.out.println("Data belum di proses BPM atau tidak termasuk task Anda");
			}
		
	
		return listTask;
		
	}

}
