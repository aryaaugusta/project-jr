# README

## Install on local

```
mvn -f global-core/pom.xml clean install -DcreateChecksum=true && \
mvn -f application-services/pom.xml clean install -DcreateChecksum=true && \
mvn -f application-core/pom.xml clean install -DcreateChecksum=true && \
mvn -f application-ui/pom.xml clean install -DcreateChecksum=true && \
sudo docker container prune -f && \
sudo docker-compose up
```

## Deploy to dev server (weblogic)

1. Change `application-ui/src/main/webapp/WEB-INF/zk.xml` from `<label-location>/WEB-INF/application.local.properties</label-location>` to `<label-location>/WEB-INF/application.properties</label-location>`
2. Rebuild: `mvn -f application-ui/pom.xml clean install -DcreateChecksum=true`
3. Deploy file `application-ui/target/application-ui.war` and `application-core/target/application-core.war`

## Restart Server

```
cd /u01/Oracle/Middleware/Oracle_Home/user_projects/domains/DEVBPM_domain/bin/
./stopManagedWebLogic.sh soa_server1
./stopWebLogic.sh
nohup sh ./startWebLogic.sh &
nohup sh ./startManagedWebLogic.sh soa_server1 &
```